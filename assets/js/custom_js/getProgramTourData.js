function getProgramTourData(programTour_id)
{
    var host_selected = (type_page == "html") ? host : host_entrepreneur+"/business"
    $.ajax({
        type: "POST",
        url: host_selected+'/ProgramTour/GetProgramTourData',
        data: {programTour_id: programTour_id},
        success: function (response) {
            
            $(".box_data").remove();

            var header_view_modal = document.getElementById("header_view_modal")
            var box_btn_programTour_approve = document.getElementById("box_btn_programTour_approve");

            if(box_btn_programTour_approve !=null)
            {
                if(parseInt(response[0].PublishStatus_publishStatus_id) == 1)
                    box_btn_programTour_approve.style.visibility = "hidden";
                else
                    box_btn_programTour_approve.style.visibility = "visible";
            }

            if(header_view_modal !=null)
                header_view_modal.textContent= response[0].programTour_nameThai

            document.getElementById("coverItem_paths_Thai").src = "/dasta_thailand/assets/img/uploadfile/"+response[0].coverItem_paths;
            document.getElementById("coverItem_paths_English").src = "/dasta_thailand/assets/img/uploadfile/"+response[0].coverItem_paths;
            document.getElementById("coverItem_paths_Chinese").src = "/dasta_thailand/assets/img/uploadfile/"+response[0].coverItem_paths;
            document.getElementById("coverItem_paths_Laos").src = "/dasta_thailand/assets/img/uploadfile/"+response[0].coverItem_paths;

            document.getElementById("programTour_nameThai").textContent= response[0].programTour_nameThai
            document.getElementById("programTour_nameEnglish").textContent= response[0].programTour_nameEnglish
            document.getElementById("programTour_nameChinese").textContent= response[0].programTour_nameChinese
            document.getElementById("programTour_nameLaos").textContent= response[0].programTour_nameLaos
            
            document.getElementById("programTour_interestingThingsThai").textContent= response[0].programTour_interestingThingsThai
            document.getElementById("programTour_interestingThingsEnglish").textContent= response[0].programTour_interestingThingsEnglish
            document.getElementById("programTour_interestingThingsChinese").textContent= response[0].programTour_interestingThingsChinese
            document.getElementById("programTour_interestingThingsLaos").textContent= response[0].programTour_interestingThingsLaos

            document.getElementById("category_Thai1").textContent= response[0].category_thai +" : "
            document.getElementById("category_English1").textContent= response[0].category_english +" : "
            document.getElementById("category_Chinese1").textContent= response[0].category_chinese +" : "
            document.getElementById("category_Laos1").textContent= response[0].category_laos +" : "

            document.getElementById("category_Thai2").textContent= response[0].subcategory_thai
            document.getElementById("category_English2").textContent= response[0].subcategory_english
            document.getElementById("category_Chinese2").textContent= response[0].subcategory_chinese
            document.getElementById("category_Laos2").textContent= response[0].subcategory_laos

            for(i=0;i<Object.keys(response[0].DatesTrip).length;i++)
            {
                new_data_date_trip_box ="";
                new_data_date_trip_box_English ="";
                new_data_date_trip_box_Chinese ="";
                new_data_date_trip_box_Laos ="";  

                new_data_trip_scope_box ="";
                new_data_trip_scope_box_English ="";
                new_data_trip_scope_box_Chinese ="";
                new_data_trip_scope_box_Laos ="";  

                TouristAttractionsThai = [];
                TouristAttractionsEnglish = [];
                TouristAttractionsChinese = [];
                TouristAttractionsLaos = [];

                new_data_date_trip_item_box="";
                new_data_date_trip_item_box_English="";
                new_data_date_trip_item_box_Chinese="";
                new_data_date_trip_item_box_Laos="";

                for(j=0;j<Object.keys(response[0].DatesTrip[i].TouristAttractions).length;j++)
                {
                    TouristAttractionsThai.push("<i class=\"fa fa-map-marker\" aria-hidden=\"true\"> "+response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].itmes_topicThai+"</i>")
                    TouristAttractionsEnglish.push("<i class=\"fa fa-map-marker\" aria-hidden=\"true\"> "+response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].itmes_topicEnglish+"</i>")
                    TouristAttractionsChinese.push("<i class=\"fa fa-map-marker\" aria-hidden=\"true\"> "+response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].itmes_topicChinese+"</i>")
                    TouristAttractionsLaos.push("<i class=\"fa fa-map-marker\" aria-hidden=\"true\"> "+response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].itmes_topicLaos+"</i>")

                    new_data_date_trip_photo_box="";
                    new_data_date_trip_photo_box_English="";
                    new_data_date_trip_photo_box_Chinese="";
                    new_data_date_trip_photo_box_Laos="";

                    var time = response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].items_timeOpen   +" - " + response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].items_timeClose;
                    var TimePeriodThai = "";
                    var TimePeriodEnglish = "";
                    var TimePeriodChinese = "";
                    var TimePeriodLaos = "";

                    if(response[0].item_dayOpen == response[0].item_dayClose)
                    {
                        TimePeriodThai = response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].item_dayOpen_data[0].dayofweek_thai + " " + time;
                        TimePeriodEnglish = response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].item_dayOpen_data[0].dayofweek_English + " " + time;
                        TimePeriodChinese = response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].item_dayOpen_data[0].dayofweek_Chinese + " " + time;
                        TimePeriodLaos = response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].item_dayOpen_data[0].dayofweek_Laos + " " + time;
                    }
                    else
                    {
                        TimePeriodThai = response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].item_dayOpen_data[0].dayofweek_thai +" "+response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].item_dayClose_data[0].dayofweek_thai + " " + time;
                        TimePeriodEnglish = response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].item_dayOpen_data[0].dayofweek_English +" "+response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].item_dayClose_data[0].dayofweek_English + " " + time;
                        TimePeriodChinese = response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].item_dayOpen_data[0].dayofweek_Chinese +" "+response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].item_dayClose_data[0].dayofweek_Chinese + " " + time;
                        TimePeriodLaos = response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].item_dayOpen_data[0].dayofweek_Laos +" "+response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].item_dayClose_data[0].dayofweek_Laos + " " + time;
                        
                    }
                
                    new_data_date_trip_item_box += "<br>";
                    new_data_date_trip_item_box += "<div class=\"ItemDetailThai\">";
                    
                    new_data_date_trip_item_box_English += "<br>";
                    new_data_date_trip_item_box_English += "<div class=\"ItemDetailEnglish\">"; 
                    
                    new_data_date_trip_item_box_Chinese += "<br>";
                    new_data_date_trip_item_box_Chinese += "<div class=\"ItemDetailChinese\">";
                    
                    new_data_date_trip_item_box_Laos += "<br>";
                    new_data_date_trip_item_box_Laos += "<div class=\"ItemDetailLaos\">";
                    
                    new_data_date_trip_item_box += "<div class=\"ibox-content-head2\">";
                    new_data_date_trip_item_box += "<div class=\"row\">";
                    new_data_date_trip_item_box += "<div class=\"col-md-5\">";
                    new_data_date_trip_item_box += "<div class=\"\">";
                    new_data_date_trip_item_box += "<img class=\"d-block w-100\" src=\"/dasta_thailand/assets/img/uploadfile/"+response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].coverItem_paths+"\" alt=\"First slide\">";
                    new_data_date_trip_item_box += "</div>";
                    new_data_date_trip_item_box += "</div>";
                    new_data_date_trip_item_box += "<div class=\"col-md-7\">";
                    new_data_date_trip_item_box += "<h3>"+response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].itmes_topicThai+"</h3>";
                    new_data_date_trip_item_box += "<p>ที่อยู่ : "+response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].items_contactThai+"</p>";
                    new_data_date_trip_item_box += "<h4>รายละเอียดการเปิด-ปิด</h4>";
                    new_data_date_trip_item_box += "<p>"+TimePeriodThai+"</p>";
                    new_data_date_trip_item_box += "<p>เบอร์ติดต่อ : "+response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].items_phone+"";
                    new_data_date_trip_item_box += "</div>";
                    new_data_date_trip_item_box += "</div>";
                    new_data_date_trip_item_box += "</div>";
                    new_data_date_trip_item_box += "<hr class=\"hr-line-solid\">";

                    new_data_date_trip_item_box_English += "<div class=\"ibox-content-head2\">";
                    new_data_date_trip_item_box_English += "<div class=\"row\">";
                    new_data_date_trip_item_box_English += "<div class=\"col-md-5\">";
                    new_data_date_trip_item_box_English += "<div class=\"\">";
                    new_data_date_trip_item_box_English += "<img class=\"d-block w-100\" src=\"/dasta_thailand/assets/img/uploadfile/"+response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].coverItem_paths+"\" alt=\"First slide\">";
                    new_data_date_trip_item_box_English += "</div>";
                    new_data_date_trip_item_box_English += "</div>";
                    new_data_date_trip_item_box_English += "<div class=\"col-md-7\">";
                    new_data_date_trip_item_box_English += "<h3>"+response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].itmes_topicEnglish+"</h3>";
                    new_data_date_trip_item_box_English += "<p>ที่อยู่ : "+response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].items_contactEnglish+"</p>";
                    new_data_date_trip_item_box_English += "<h4>รายละเอียดการเปิด-ปิด</h4>";
                    new_data_date_trip_item_box_English += "<p>"+TimePeriodEnglish+"</p>";
                    new_data_date_trip_item_box_English += "<p>เบอร์ติดต่อ : "+response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].items_phone+"";
                    new_data_date_trip_item_box_English += "</div>";
                    new_data_date_trip_item_box_English += "</div>";
                    new_data_date_trip_item_box_English += "</div>";
                    new_data_date_trip_item_box_English += "<hr class=\"hr-line-solid\">";

                    new_data_date_trip_item_box_Chinese += "<div class=\"ibox-content-head2\">";
                    new_data_date_trip_item_box_Chinese += "<div class=\"row\">";
                    new_data_date_trip_item_box_Chinese += "<div class=\"col-md-5\">";
                    new_data_date_trip_item_box_Chinese += "<div class=\"\">";
                    new_data_date_trip_item_box_Chinese += "<img class=\"d-block w-100\" src=\"/dasta_thailand/assets/img/uploadfile/"+response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].coverItem_paths+"\" alt=\"First slide\">";
                    new_data_date_trip_item_box_Chinese += "</div>";
                    new_data_date_trip_item_box_Chinese += "</div>";
                    new_data_date_trip_item_box_Chinese += "<div class=\"col-md-7\">";
                    new_data_date_trip_item_box_Chinese += "<h3>"+response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].itmes_topicChinese+"</h3>";
                    new_data_date_trip_item_box_Chinese += "<p>ที่อยู่ : "+response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].items_contactChinese+"</p>";
                    new_data_date_trip_item_box_Chinese += "<h4>รายละเอียดการเปิด-ปิด</h4>";
                    new_data_date_trip_item_box_Chinese += "<p>"+TimePeriodChinese+"</p>";
                    new_data_date_trip_item_box_Chinese += "<p>เบอร์ติดต่อ : "+response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].items_phone+"";
                    new_data_date_trip_item_box_Chinese += "</div>";
                    new_data_date_trip_item_box_Chinese += "</div>";
                    new_data_date_trip_item_box_Chinese += "</div>";
                    new_data_date_trip_item_box_Chinese += "<hr class=\"hr-line-solid\">";

                    new_data_date_trip_item_box_Laos += "<div class=\"ibox-content-head2\">";
                    new_data_date_trip_item_box_Laos += "<div class=\"row\">";
                    new_data_date_trip_item_box_Laos += "<div class=\"col-md-5\">";
                    new_data_date_trip_item_box_Laos += "<div class=\"\">";
                    new_data_date_trip_item_box_Laos += "<img class=\"d-block w-100\" src=\"/dasta_thailand/assets/img/uploadfile/"+response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].coverItem_paths+"\" alt=\"First slide\">";
                    new_data_date_trip_item_box_Laos += "</div>";
                    new_data_date_trip_item_box_Laos += "</div>";
                    new_data_date_trip_item_box_Laos += "<div class=\"col-md-7\">";
                    new_data_date_trip_item_box_Laos += "<h3>"+response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].itmes_topicLaos+"</h3>";
                    new_data_date_trip_item_box_Laos += "<p>ที่อยู่ : "+response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].items_contactLaos+"</p>";
                    new_data_date_trip_item_box_Laos += "<h4>รายละเอียดการเปิด-ปิด</h4>";
                    new_data_date_trip_item_box_Laos += "<p>"+TimePeriodLaos+"</p>";
                    new_data_date_trip_item_box_Laos += "<p>เบอร์ติดต่อ : "+response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].items_phone+"";
                    new_data_date_trip_item_box_Laos += "</div>";
                    new_data_date_trip_item_box_Laos += "</div>";
                    new_data_date_trip_item_box_Laos += "</div>";
                    new_data_date_trip_item_box_Laos += "<hr class=\"hr-line-solid\">";

                    TouristAttractions_PhotoTourist_Thai="";
                    TouristAttractions_PhotoTourist_English="";
                    TouristAttractions_PhotoTourist_Chinese="";
                    TouristAttractions_PhotoTourist_Laos="";

                    for(k=0;k<Object.keys(response[0].DatesTrip[i].TouristAttractions[j].PhotoTourist).length;k++)
                    {
                        TouristAttractions_PhotoTourist_Thai += "<div class=\"box_data\">";
                        TouristAttractions_PhotoTourist_Thai += "<p><span id=\"\">"+response[0].DatesTrip[i].TouristAttractions[j].PhotoTourist[k].photoTourist_topicThai+"</span></p> ";
                        TouristAttractions_PhotoTourist_Thai += "<img class=\"d-block w-100\" src=\"/dasta_thailand/assets/img/uploadfile/"+response[0].DatesTrip[i].TouristAttractions[j].PhotoTourist[k].photoTourist_paths+"\" alt=\"\">  ";
                        TouristAttractions_PhotoTourist_Thai += " </div> ";
    
                        TouristAttractions_PhotoTourist_English += "<div class=\"box_data\">";
                        TouristAttractions_PhotoTourist_English += "<img class=\"d-block w-100\" src=\"/dasta_thailand/assets/img/uploadfile/"+response[0].DatesTrip[i].TouristAttractions[j].PhotoTourist[k].photoTourist_paths+"\" alt=\"\">  ";
                        TouristAttractions_PhotoTourist_English += "<p><span id=\"\">"+response[0].DatesTrip[i].TouristAttractions[j].PhotoTourist[k].photoTourist_topicEnglish+"</span></p> ";
                        TouristAttractions_PhotoTourist_English += " </div> ";
    
                        TouristAttractions_PhotoTourist_Chinese += "<div class=\"box_data\">";
                        TouristAttractions_PhotoTourist_Chinese += "<p><span id=\"\">"+response[0].DatesTrip[i].TouristAttractions[j].PhotoTourist[k].photoTourist_topicChinese+"</span></p> ";
                        TouristAttractions_PhotoTourist_Chinese += "<img class=\"d-block w-100\" src=\"/dasta_thailand/assets/img/uploadfile/"+response[0].DatesTrip[i].TouristAttractions[j].PhotoTourist[k].photoTourist_paths+"\" alt=\"\">  ";
                        TouristAttractions_PhotoTourist_Chinese += " </div> ";
    
                        TouristAttractions_PhotoTourist_Laos += "<div class=\"box_data\">";
                        TouristAttractions_PhotoTourist_Laos += "<p><span id=\"\">"+response[0].DatesTrip[i].TouristAttractions[j].PhotoTourist[k].photoTourist_topicLaos+"</span></p> ";
                        TouristAttractions_PhotoTourist_Laos += "<img class=\"d-block w-100\" src=\"/dasta_thailand/assets/img/uploadfile/"+response[0].DatesTrip[i].TouristAttractions[j].PhotoTourist[k].photoTourist_paths+"\" alt=\"\">  ";
                        TouristAttractions_PhotoTourist_Laos += " </div> ";
                    }

                    new_data_date_trip_photo_box += "<div class=\"PhotoTouristThai\">";
                    new_data_date_trip_photo_box += TouristAttractions_PhotoTourist_Thai;
                    new_data_date_trip_photo_box += " </div> ";
                    
                    new_data_date_trip_photo_box_English += "<div class=\"PhotoTouristEnglish\">";
                    new_data_date_trip_photo_box_English += TouristAttractions_PhotoTourist_English;
                    new_data_date_trip_photo_box_English += " </div> ";
                    
                    new_data_date_trip_photo_box_Chinese += "<div class=\"PhotoTouristChinese\">";
                    new_data_date_trip_photo_box_Chinese += TouristAttractions_PhotoTourist_Chinese;
                    new_data_date_trip_photo_box_Chinese += " </div> ";
                    
                    new_data_date_trip_photo_box_Laos += "<div class=\"PhotoTouristLaos\">";
                    new_data_date_trip_photo_box_Laos += TouristAttractions_PhotoTourist_Thai;
                    new_data_date_trip_photo_box_Laos += " </div> ";


                   
                    new_data_date_trip_item_box += new_data_date_trip_photo_box;
                    new_data_date_trip_item_box += " </div> ";
                    
                    new_data_date_trip_item_box_English += new_data_date_trip_photo_box_English;
                    new_data_date_trip_item_box_English += " </div> ";

                    new_data_date_trip_item_box_Chinese += new_data_date_trip_photo_box_Chinese;
                    new_data_date_trip_item_box_Chinese += " </div> ";

                    new_data_date_trip_item_box_Laos += new_data_date_trip_photo_box_Laos;
                    new_data_date_trip_item_box_Laos += " </div> ";

                }

                new_data_trip_scope_box += "<div class=\"box_data\">";
                new_data_trip_scope_box += "<p><span id=\"\"> "+response[0].DatesTrip[i].datesTrip_topicThai+" : "+TouristAttractionsThai.join(' - ')+"</span></p> ";
                new_data_trip_scope_box += " </div> ";

                new_data_trip_scope_box_English += "<div class=\"box_data\">";
                new_data_trip_scope_box_English += "<p><span id=\"\"> "+response[0].DatesTrip[i].datesTrip_topicEnglish+" : "+TouristAttractionsEnglish.join(' - ')+"</span></p> ";
                new_data_trip_scope_box_English += " </div> ";

                new_data_trip_scope_box_Chinese += "<div class=\"box_data\">";
                new_data_trip_scope_box_Chinese += "<p><span id=\"\"> "+response[0].DatesTrip[i].datesTrip_topicChinese+" : "+TouristAttractionsChinese.join(' - ')+"</span></p> ";
                new_data_trip_scope_box_Chinese += " </div> ";

                new_data_trip_scope_box_Laos += "<div class=\"box_data\">";
                new_data_trip_scope_box_Laos += "<p><span id=\"\"> "+response[0].DatesTrip[i].datesTrip_topicLaos+" : "+TouristAttractionsLaos.join(' - ')+"</span></p> ";
                new_data_trip_scope_box_Laos += " </div> ";
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
                new_data_date_trip_box += "<div class=\"box_data\">";
                new_data_date_trip_box += "<p><h3><span id=\"\"> "+response[0].DatesTrip[i].datesTrip_topicThai+"</span></h3></p> ";
                new_data_date_trip_box += new_data_date_trip_item_box;
                new_data_date_trip_box += "<hr class=\"hr-line-solid\">";
                new_data_date_trip_box += " </div> ";

                new_data_date_trip_box_English += "<div class=\"box_data\">";
                new_data_date_trip_box_English += "<p><h3><span id=\"\"> "+response[0].DatesTrip[i].datesTrip_topicEnglish+"</span></h3></p> ";
                new_data_date_trip_box_English += new_data_date_trip_item_box_English;
                new_data_date_trip_box_English += "<hr class=\"hr-line-solid\">";
                new_data_date_trip_box_English += " </div> ";

                new_data_date_trip_box_Chinese += "<div class=\"box_data\">";
                new_data_date_trip_box_Chinese += "<p><h3><span id=\"\"> "+response[0].DatesTrip[i].datesTrip_topicChinese+"</span></h3></p> ";
                new_data_date_trip_box_Chinese += new_data_date_trip_item_box_Chinese;
                new_data_date_trip_box_Chinese += "<hr class=\"hr-line-solid\">";
                new_data_date_trip_box_Chinese += " </div> ";

                new_data_date_trip_box_Laos += "<div class=\"box_data\">";
                new_data_date_trip_box_Laos += "<p><h3><span id=\"\"> "+response[0].DatesTrip[i].datesTrip_topicLaos+"</span></h3></p> ";
                new_data_date_trip_box_Laos += new_data_date_trip_item_box_Laos;
                new_data_date_trip_box_Laos += "<hr class=\"hr-line-solid\">";
                new_data_date_trip_box_Laos += " </div> ";

                $("#DatesTripScopeThai").append(new_data_trip_scope_box);
                $("#DatesTripScopeEnglish").append(new_data_trip_scope_box_English);
                $("#DatesTripScopeChinese").append(new_data_trip_scope_box_Chinese);
                $("#DatesTripScopeLaos").append(new_data_trip_scope_box_Laos);

                $("#DatesTripThai").append(new_data_date_trip_box);
                $("#DatesTripEnglish").append(new_data_date_trip_box_English);
                $("#DatesTripChinese").append(new_data_date_trip_box_Chinese);
                $("#DatesTripLaos").append(new_data_date_trip_box_Laos);


            }

            var btn_items_approve = document.getElementById("btn_programTour_approve")
            var btn_items_disapprove = document.getElementById("btn_programTour_disapprove")
           
            if(btn_items_approve != null)
                btn_items_approve.value= response[0].programTour_id
            if(btn_items_disapprove != null)
                btn_items_disapprove.value= response[0].programTour_id
        }
    });
}