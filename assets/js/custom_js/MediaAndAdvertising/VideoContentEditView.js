$(document).ready(function(){
    $(".link-youtube").map(function() {return $(this).attr('id');}).get().forEach((element, index) => {
        cheackElementYoutubeLink(document.getElementById(element))
    });

    $.ajax({
        type: 'post',
        url: host+'/MediaAndAdvertising/'+MenuName+'/getNameCategory',
        success: function (response) {
          $("#namecategory").empty();
          $("#namecategory").append(response);
          $("#namecategory").val(category_id);
          $('#namecategory').trigger("chosen:updated");
        }
    });

    $(document).on("click", "#save", function () {
        resetNotification();
        regex = /http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?‌​[\w\?‌​=]*)?/;
        var formData = new FormData();
        formData.append('videoContent_textThai',$("#videoContent_textThai").val());
        formData.append('videoContent_textEnglish',$("#videoContent_textEnglish").val());
        formData.append('videoContent_textLaos',$("#videoContent_textLaos").val());
        formData.append('videoContent_textChinese',$("#videoContent_textChinese").val());
        formData.append('link_youtube',$("#link_youtube").val());
        formData.append('country',$("#country").val());
        formData.append('provinces',$("#provinces").val());
        formData.append('districts',$("#districts").val());
        formData.append('namecategory',$("#namecategory").val());
        formData.append('videoContent_id',videoContent_id);
        
        if(!regex.test($("#link_youtube").val()))
        {
            var element = document.getElementById("notifi_link_youtube");
            element.style.color = "red";
            $("#notifi_link_youtube").css('display', 'inline', 'important');
            $('#notifi_link_youtube').text('ลิ้งค์ youtube ไม่ถูกต้อง');
            document.getElementById('link_youtube').focus();
        }
        else
        {
            $.ajax({
                type: "POST",
                url: host+'/MediaAndAdvertising/'+MenuName+'/saveEditVideoContent',
                data: formData,
                contentType: false,
                processData:false,
                success: function (response) {
                   
                    if(response.state)
                    {
                        var element = document.getElementById("notifi_error_save");
                        element.style.color = "green";
                        $("#notifi_error_save").css('display', 'inline', 'important');
                        element.innerHTML = response.msg;
                        window.location.href ='/dasta_thailand/html/MediaAndAdvertising/'+MenuName;
                    }
                    else
                    {
                        if(response.error == 'validation')
                        { 
                            msg = response.msg.replace(/<p>The /g, "");  
                            msg = msg.replace(/ field is required.<\/p>/g,"");
                            msg =msg.split('\n');
                            msg.forEach((element, index) => {
                                    var ids = element.split('|');
                                    if(ids[0] == "required")
                                    {
                                        SetNavLinkTabMainActive();
                                        var element = document.getElementById("notifi_"+ids[2]);
                                        element.style.color = "red";
                                        $("#notifi_"+ids[2]).css('display', 'inline', 'important');
                                        $('#notifi_'+ids[2]).text(ids[3]);
                                        document.getElementById(ids[1]).focus();
                                    }
                            });
                        }
                        else
                        {
                            var element = document.getElementById("notifi_error_save");
                            element.style.color = "red";
                            $("#notifi_error_save").css('display', 'inline', 'important');
                            element.innerHTML = response.msg;
                        }
                    }
                }
            });
        }
    });
    setTimeout(function () {SetSelectLocation();}, 1000);
    SetData();
});
async function SetData()
{
    var element =  document.getElementById('link_youtube')
    regex = /http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?‌​[\w\?‌​=]*)?/;
    var videoId ="";
    if(element.value != "" && regex.test(element.value))
    {
        if(element.value.match(regex)[1].length > 0)
        {
            videoId = "https://www.youtube-nocookie.com/embed/"+element.value.match(regex)[1]+"?controls=0";
        }
        else
        {
            videoId = "";
        }
    }
    else
        videoId= "";

    document.getElementById('player').src = videoId;
}
function cheackElementYoutubeLink(element)
{
    ["input", "keydown", "keyup", "mousedown", "mouseup", "select"].forEach(function(event) {
        element.addEventListener(event, function() {
            regex = /http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?‌​[\w\?‌​=]*)?/;
            var videoId ="";
            if(this.value != "" && regex.test(this.value))
            {
                if(this.value.match(regex)[1].length > 0)
                {
                    videoId = "https://www.youtube-nocookie.com/embed/"+this.value.match(regex)[1]+"?controls=0";
                }
                else
                {
                    videoId = "";
                }
            }
            else
                videoId= "";

            document.getElementById('player').src = videoId;
            
        });
    });
}