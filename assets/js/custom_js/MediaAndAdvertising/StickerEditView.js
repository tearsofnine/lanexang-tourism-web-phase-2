$(document).ready(function(){
    $.ajax({
        type: 'post',
        url: '/dasta_thailand/html/MediaAndAdvertising/Sticker/getNameCategory',
        success: function (response) {
          $("#sticker_type").empty();
          $("#sticker_type").append(response);
          $("#sticker_type").val(sticker_type);
          $('#sticker_type').trigger("chosen:updated");
        }
    });
    $(document).on("click", "#save", function () {
     // $("#save").prop("disabled", true);
      resetNotification();
      var formData = new FormData();
      formData.append('sticker_Thai',$("#sticker_Thai").val());
      formData.append('sticker_English',$("#sticker_English").val());
      formData.append('sticker_Chinese',$("#sticker_Chinese").val());
      formData.append('sticker_Laos',$("#sticker_Laos").val());
      formData.append('sticker_type',$("#sticker_type").val());
      formData.append('sticker_id',val_sticker_id);

      for (var pair of formData.entries()) {
          console.log(pair[0]+ ', ' + pair[1]); 
          
      }

      if($('#fileSticker').fileinput('getFilesCount') >= 1)
      {
          $.ajax({
              type: "POST",
              url: host+'/'+typepage+'/'+MenuName+'/'+type,
              data: formData,
              contentType: false,
              processData:false,
              success: function (response) {
                  if(response.state)
                  {
                      var element = document.getElementById("notifi_error_save");
                      element.style.color = "green";
                      $("#notifi_error_save").css('display', 'inline', 'important');
                      element.innerHTML = response.msg;

                      var sticker_id = response.sticker_id
                      eval("val_sticker_id = sticker_id");
                      $('#fileSticker').fileinput('upload');
                  }
                  else
                  {
                      $("#save").prop("disabled", false);
                      if(response.error == 'validation')
                      { 
                          msg = response.msg.replace(/<p>The /g, "");  
                          msg = msg.replace(/ field is required.<\/p>/g,"");
                          msg =msg.split('\n');
                          msg.forEach((element, index) => {
                                  var ids = element.split('|');
                                  if(ids[0] == "required")
                                  {
                                      document.getElementById("status").style.display = "none";
                                      document.getElementById("preloader").style.display = "none";
                                      SetNavLinkTabMainActive();
                                      var element = document.getElementById("notifi_"+ids[2]);
                                      element.style.color = "red";
                                      $("#notifi_"+ids[2]).css('display', 'inline', 'important');
                                      $('#notifi_'+ids[2]).text(ids[3]);
                                      document.getElementById(ids[1]).focus();
                                      
                                  }
                          });
                      }
                      else
                      {
                          document.getElementById("status").style.display = "none";
                          document.getElementById("preloader").style.display = "none";
                          var element = document.getElementById("notifi_error_save");
                          element.style.color = "red";
                          $("#notifi_error_save").css('display', 'inline', 'important');
                          element.innerHTML = response.msg;
                      }
                  }
              }
          });
      }
      else
      {
        $.ajax({
          type: "POST",
          url: host+'/'+typepage+'/'+MenuName+'/'+type,
          data: formData,
          contentType: false,
          processData:false,
          success: function (response) {
              if(response.state)
              {
                  var element = document.getElementById("notifi_error_save");
                  element.style.color = "green";
                  $("#notifi_error_save").css('display', 'inline', 'important');
                  element.innerHTML = response.msg;
                  window.location.href ='/dasta_thailand/html/'+typepage+'/'+MenuName;
                  // var sticker_id = response.sticker_id.insert_id
                  // eval("val_sticker_id = sticker_id");
                  // $('#fileSticker').fileinput('upload');
              }
              else
              {
                  $("#save").prop("disabled", false);
                  if(response.error == 'validation')
                  { 
                      msg = response.msg.replace(/<p>The /g, "");  
                      msg = msg.replace(/ field is required.<\/p>/g,"");
                      msg =msg.split('\n');
                      msg.forEach((element, index) => {
                              var ids = element.split('|');
                              if(ids[0] == "required")
                              {
                                  document.getElementById("status").style.display = "none";
                                  document.getElementById("preloader").style.display = "none";
                                  SetNavLinkTabMainActive();
                                  var element = document.getElementById("notifi_"+ids[2]);
                                  element.style.color = "red";
                                  $("#notifi_"+ids[2]).css('display', 'inline', 'important');
                                  $('#notifi_'+ids[2]).text(ids[3]);
                                  document.getElementById(ids[1]).focus();
                                  
                              }
                      });
                  }
                  else
                  {
                      document.getElementById("status").style.display = "none";
                      document.getElementById("preloader").style.display = "none";
                      var element = document.getElementById("notifi_error_save");
                      element.style.color = "red";
                      $("#notifi_error_save").css('display', 'inline', 'important');
                      element.innerHTML = response.msg;
                  }
              }
          }
      });
          
      }

    })
});