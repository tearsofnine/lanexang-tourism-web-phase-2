$(document).ready(function(){
    
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });

    $.ajax({
        type: 'post',
        url: host+'/MediaAndAdvertising/'+MenuName+'/getNameCategory',
        success: function (response) {
          $("#namecategory").empty();
          $("#namecategory").append(response);
          $("#namecategory").val(menuItem_id);
          $('#namecategory').trigger("chosen:updated");
        }
    });

    document.getElementById('image_crop_img').addEventListener('ready', (event) => {      
        if(!crop_img)
        {
            cropper.setData(CropData);
            cropper.setCropBoxData(CropBoxData);
            cropper.setCanvasData(CanvasData);
            crop_img = true;
        }   
    });
    
    document.getElementById('image_crop_img').addEventListener('crop', (event) => {
        if(crop_img)
        {
            CropData = cropper.getData()
            CropBoxData = cropper.getCropBoxData()
            CanvasData = cropper.getCanvasData()
        }
    });  
    SetData();
    setTimeout(function () {SetSelectLocation();}, 1000);
    $(document).on("click", "#save", function () {
        resetNotification();
        cropper.getCroppedCanvas().toBlob((blob) =>{
            var formData = new FormData();

            formData.append('cropperImage', blob,'cropperImage.png');
            formData.append('mediaAndAdvertising_id', mediaAndAdvertising_id);
            
            if(document.getElementById("inputImage").files[0] != null)
                formData.append('originalImage', document.getElementById("inputImage").files[0]);
            else
            {
                formData.append("originalImage",dataURLtoFile(document.getElementById("temp_inputImage").value,'originalImage'))
            }

            formData.append('CropData', JSON.stringify(CropData));
            formData.append('CropBoxData', JSON.stringify(CropBoxData));
            formData.append('CanvasData', JSON.stringify(CanvasData));

            formData.append('mediaAndAdvertising_nameThai',$("#mediaAndAdvertising_nameThai").val());
            formData.append('mediaAndAdvertising_nameEnglish',$("#mediaAndAdvertising_nameEnglish").val());
            formData.append('mediaAndAdvertising_nameLaos',$("#mediaAndAdvertising_nameLaos").val());
            formData.append('mediaAndAdvertising_nameChinese',$("#mediaAndAdvertising_nameChinese").val());

            formData.append('mediaAndAdvertising_urlLink',$("#mediaAndAdvertising_urlLink").val());
            
            formData.append('country',$("#country").val());
            formData.append('provinces',$("#provinces").val());
            formData.append('districts',$("#districts").val());

            formData.append('namecategory',$("#namecategory").val());
           

            formData.append('mediaAndAdvertising_time_period_start',$("#start").val());
            formData.append('mediaAndAdvertising_time_period_end',$("#end").val());
            

            for (var pair of formData.entries()) {
                console.log(pair[0]+ ', ' + pair[1]); 
            }

            if(checktypefile() == 0)
            {
                ajaxSaveItem(formData,"saveEditNews","MediaAndAdvertising")
            }
            else
            {
                var element = document.getElementById("notifi_error_save");
                element.style.color = "red";
                $("#notifi_error_save").css('display', 'inline', 'important');
                element.innerHTML = "Please select only jpg/png file.";
$("#save").prop("disabled", false);
            }
        });
    });
});
async function SetData()
{
    var typeImage = document.getElementById('image_crop_img').src.split('.')
    var file = await srcToFile(document.getElementById('image_crop_img').src,"temp_image_crop_img",'image/'+typeImage[1])
    handleFileSelect(file,document.getElementById('temp_inputImage')) 

    

    if((mediaAndAdvertising_time_period_start != "1970-01-01") && 
        (mediaAndAdvertising_time_period_end != "1970-01-01")
    )
    {
        var time_period =  document.getElementsByClassName("time_period")

        $('#set_time_period').iCheck('check'); 
        $(".time_period").prop("disabled", false);
        time_period[0].value = moment(mediaAndAdvertising_time_period_start).format('MM/DD/YYYY');
        time_period[1].value = moment(mediaAndAdvertising_time_period_end).format('MM/DD/YYYY');
        $('#data_5 .input-daterange').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,                       
        });
    }
}