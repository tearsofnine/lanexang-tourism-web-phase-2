$(document).ready(function(){
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });

    $.ajax({
        type: 'post',
        data: { getOption: 1},
        url: host+'/MediaAndAdvertising/CoverProgramTourOfficial/getNameCategorySubCategory',
        success: function (response) {
            $("#namecategory").empty();
            $("#namecategory").append(response);
            $("#namecategory").trigger("chosen:updated");
        }
    }); 
    $(document).on('change', '#namecategory', function (e) {
        var select = $('option:selected', this);
        if(select.val() != ""){
            var data = {
                subcategory_id: select.val()
              }
            $.ajax({
                type: 'post',
                data: data,
                url: host+'/MediaAndAdvertising/CoverProgramTourOfficial/getProgramTourData',
                success: function (response) {
                    $("#program_tour").prop("disabled", false);
                    $("#program_tour").empty();
                    $("#program_tour").append(response);
                    $('#program_tour').trigger("chosen:updated");
                }
            });
        }
        else{
            $("#program_tour").prop("disabled", false);
            $("#program_tour").empty();
            $('#program_tour').trigger("chosen:updated");
        }
    });
    document.getElementById('image_crop_img').addEventListener('crop', (event) => {

        CropData = cropper.getData()
        CropBoxData = cropper.getCropBoxData()
        CanvasData = cropper.getCanvasData()
    });

    $(document).on("click", "#save", function () {
        resetNotification();
        cropper.getCroppedCanvas().toBlob((blob) =>{
            var formData = new FormData();
            formData.append('cropperImage', blob,'cropperImage.png');
            
            formData.append('originalImage', document.getElementById("inputImage").files[0]);

            formData.append('CropData', JSON.stringify(CropData));
            formData.append('CropBoxData', JSON.stringify(CropBoxData));
            formData.append('CanvasData', JSON.stringify(CanvasData));

            formData.append('mediaAndAdvertising_nameThai',$("#mediaAndAdvertising_nameThai").val());
            formData.append('mediaAndAdvertising_nameEnglish',$("#mediaAndAdvertising_nameEnglish").val());
            formData.append('mediaAndAdvertising_nameLaos',$("#mediaAndAdvertising_nameLaos").val());
            formData.append('mediaAndAdvertising_nameChinese',$("#mediaAndAdvertising_nameChinese").val());
            
            // formData.append('country',$("#country").val());
            // formData.append('provinces',$("#provinces").val());
            // formData.append('districts',$("#districts").val());

            formData.append('namecategory',$("#namecategory").val());
            formData.append('program_tour',$("#program_tour").val());

            formData.append('mediaAndAdvertising_time_period_start',$("#start").val());
            formData.append('mediaAndAdvertising_time_period_end',$("#end").val());
            

            for (var pair of formData.entries()) {
                console.log(pair[0]+ ', ' + pair[1]); 
            }
            
            if(checktypefile() == 0)
            {
                ajaxSaveItem(formData,"saveNewCoverProgramTourOfficial","MediaAndAdvertising")
            }
            else
            {
                var element = document.getElementById("notifi_error_save");
                element.style.color = "red";
                $("#notifi_error_save").css('display', 'inline', 'important');
                element.innerHTML = "Please select only jpg/png file.";
$("#save").prop("disabled", false);
            }
        });
    });
});