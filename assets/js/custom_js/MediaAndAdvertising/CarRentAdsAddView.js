$(document).ready(function(){
    
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });

    $.ajax({
        type: 'post',
        url: host+'/MediaAndAdvertising/'+MenuName+'/getNameBusiness',
        success: function (response) {
          $("#business_id").empty();
          $("#business_id").append(response);
          $('#business_id').trigger("chosen:updated");
        }
    });

    document.getElementById('image_crop_img').addEventListener('crop', (event) => {

        CropData = cropper.getData()
        CropBoxData = cropper.getCropBoxData()
        CanvasData = cropper.getCanvasData()
    });
    $(document).on("click", "#save", function () {
        $("#save").prop("disabled", true);
        resetNotification();
        cropper.getCroppedCanvas().toBlob((blob) =>{
            var formData = new FormData();
            formData.append('cropperImage', blob,'cropperImage.png');
            
            formData.append('originalImage', document.getElementById("inputImage").files[0]);

            formData.append('CropData', JSON.stringify(CropData));
            formData.append('CropBoxData', JSON.stringify(CropBoxData));
            formData.append('CanvasData', JSON.stringify(CanvasData));

            formData.append('mediaAndAdvertising_nameThai',$("#mediaAndAdvertising_nameThai").val());
            formData.append('mediaAndAdvertising_nameEnglish',$("#mediaAndAdvertising_nameEnglish").val());
            formData.append('mediaAndAdvertising_nameLaos',$("#mediaAndAdvertising_nameLaos").val());
            formData.append('mediaAndAdvertising_nameChinese',$("#mediaAndAdvertising_nameChinese").val());

            formData.append('business_id',$("#business_id").val());
           
            formData.append('mediaAndAdvertising_time_period_start',$("#start").val());
            formData.append('mediaAndAdvertising_time_period_end',$("#end").val());

            for (var pair of formData.entries()) {
                console.log(pair[0]+ ', ' + pair[1]); 
            }
            
            if(checktypefile() == 0)
            {
                ajaxSaveItem(formData,"saveNewCarRentAds","MediaAndAdvertising")
            }
            else
            {
                var element = document.getElementById("notifi_error_save");
                element.style.color = "red";
                $("#notifi_error_save").css('display', 'inline', 'important');
                element.innerHTML = "Please select only jpg/png file.";
                $("#save").prop("disabled", false);
                
            }
        })
    });
});