
// $(document).ready(function(){
//     updateNotifications()
// });
function updateNotifications()
{
    $.ajax({
        type: 'POST',
        url: '/dasta_thailand/html/management/UserEvent/updateUnReadMessages',
        success: function (response) {

            //var UnReadMessages = document.getElementsByClassName("UnReadMessages");
            if(parseInt(response[0].count_read) == 0 )
            {
                $(".messages-box").remove();
                $("dropdown-divider").remove();
                $('.UnReadMessages').each(function (idx, element) {

                    element.classList.remove('label')
                    element.classList.remove('label-warning')
                    element.textContent =""
                    
                });
            }
            else
            {
                $('.UnReadMessages').addClass('label')
                $('.UnReadMessages').addClass('label-warning')
                $('.UnReadMessages').each(function (idx, element) {
                    element.textContent = response[0].count_read
                });

                $(".messages-box").remove();
                $(".dropdown-divider").remove();
                //$("#main_notifications_table").append("<ul class=\"dropdown-menu dropdown-messages dropdown-menu-right\" id=\"notifications_table\"></ul>");
                
                var Notifications ="";
                $.ajax({
                    type: 'POST',
                    url: '/dasta_thailand/html/management/UserEvent/updateNotificationsMessages',
                    success: function (response) {
                        var main_notifications_table = document.getElementById("main_notifications_table")
                        if(main_notifications_table)
                        {
                            for (i = 0; i < response.length; i++) 
                            {
                                Notifications ="";
                                Notifications +="<a class=\"text-muted messages-box\" href=\"/dasta_thailand/html/Notifications\">";
                                Notifications +="<li class=\"\">";
                                Notifications +="<div class=\"dropdown-messages-box\">";
                                Notifications +="<div class=\"dropdown-item float-left\"><img alt=\"image\" class=\"rounded-circle\" src=\""+response[i].user_profile_pic_url+"\"></div>";
                                Notifications +="<div class=\"media-body\">";
                                Notifications +="<small class=\"float-right\">"+response[i].message_created+"</small>";
                                Notifications +="<strong>"+response[i].UserSend[0].user_firstName+" "+response[i].UserSend[0].user_lastName+"</strong><br> send "+response[i].count_read+" messages.";
                                Notifications +="<br>";
                                Notifications +="<small class=\"text-muted\">"+response[i].message_created_at+"</small>";
                                Notifications +="</div>";
                                Notifications +="</div>";
                                Notifications +="</li>";
                                Notifications +="</a>";
                                Notifications +="<li class=\"dropdown-divider\"></li>";
                                $("#notifications_table").append(Notifications); 
                            }
                        }
                        
                    }
                })
            }
        }
    });
}