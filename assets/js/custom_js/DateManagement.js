$(document).ready(function(){
    $.ajax({
        type: 'post',
        url: host+'/'+TypeMenu+'/'+MenuName+'/getDayOfWeek',
        success: function (response) {
          $("#dayofweek").empty();
          $("#dayofweek").append(response);
          if(str_array != null)
            $('#dayofweek').val(str_array);
          $('#dayofweek').trigger('chosen:updated');
        }
    });

    $('.timepicker').pickatime({
      format: 'HH:i',
      formatLabel: 'HH:i',
	    formatSubmit: 'HH:i',
      interval: 30
    }).pickatime('picker');

    $(".chosen-select-dayofweek").chosen().change(function(e, params){
      values = $(".chosen-select-dayofweek").chosen().val();
      
      if(values[0] == "add")
      {
        //document.getElementById("dayofweek").value = 0;
        $('#dayofweek').trigger("chosen:updated");
      }
      else if(values.length > 1)
      {
        if(parseInt(values[1]) > 7)
        {
          //document.getElementById("dayofweek").value = 0;
          $('#dayofweek').trigger("chosen:updated");
        }
      }
    });
    
  
    //$("#dayofweek").val(str_array).trigger("chosen:updated");
});