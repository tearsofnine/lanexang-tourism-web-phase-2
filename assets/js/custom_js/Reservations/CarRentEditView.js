$(document).ready(function(){
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });
    $.ajax({
        type: 'post',
        url: host+'/Reservations/'+MenuName+'/getNameGearSystem',
        success: function (response) {
          $("#GearSystem_gearSystem_id").empty();
          $("#GearSystem_gearSystem_id").append(response);
          $("#GearSystem_gearSystem_id").val(GearSystem_gearSystem_id);
          $('#GearSystem_gearSystem_id').trigger("chosen:updated");
        }
      });
    $.ajax({
        type: 'post',
        url: host+'/Reservations/'+MenuName+'/getNameTypesofCars',
        success: function (response) {
          $("#TypesofCars_typesofCars_id").empty();
          $("#TypesofCars_typesofCars_id").append(response);
          $("#TypesofCars_typesofCars_id").val(TypesofCars_typesofCars_id);
          $('#TypesofCars_typesofCars_id').trigger("chosen:updated");
        }
      });

      document.getElementById('image_crop_img').addEventListener('ready', (event) => {      
        if(!crop_img)
        {
            cropper.setData(CropData);
            cropper.setCropBoxData(CropBoxData);
            cropper.setCanvasData(CanvasData);
            crop_img = true;
        }   
    });
    
    document.getElementById('image_crop_img').addEventListener('crop', (event) => {
        if(crop_img)
        {
            CropData = cropper.getData()
            CropBoxData = cropper.getCropBoxData()
            CanvasData = cropper.getCanvasData()
        }
    }); 
    SetData();

    $(document).on("click", "#save", function () {
        $("#save").prop("disabled", true);
        resetNotification();
        cropper.getCroppedCanvas().toBlob((blob) =>{
            var formData = new FormData();
            var BookingCondition_14 = [];

            formData.append('cropperImage', blob,'cropperImage.png');
            
            if(document.getElementById("inputImage").files[0] != null)
                formData.append('originalImage', document.getElementById("inputImage").files[0]);
            else
            {
                formData.append("originalImage",dataURLtoFile(document.getElementById("temp_inputImage").value,'originalImage'))
            }

            formData.append('CropData', JSON.stringify(CropData));
            formData.append('CropBoxData', JSON.stringify(CropBoxData));
            formData.append('CanvasData', JSON.stringify(CanvasData));


            formData.append('items_id',items_id);

            formData.append('itmes_topicThai',$("#itmes_topicThai").val());
            formData.append('itmes_topicEnglish',$("#itmes_topicEnglish").val());
            formData.append('itmes_topicLaos',$("#itmes_topicLaos").val());
            formData.append('itmes_topicChinese',$("#itmes_topicChinese").val());

            formData.append('TypesofCars_typesofCars_id',$("#TypesofCars_typesofCars_id").val());
            formData.append('GearSystem_gearSystem_id',$("#GearSystem_gearSystem_id").val());
            formData.append('items_carSeats',$("#items_carSeats").val());

            formData.append('items_PricePerDay',$("#items_PricePerDay").val());
            formData.append('items_CarDeliveryPrice',$("#items_CarDeliveryPrice").val());
            formData.append('items_PickUpPrice',$("#items_PickUpPrice").val());

            formData.append('items_DepositPrice',$("#items_DepositPrice").val());
            formData.append("items_isBasicInsurance",document.querySelector('input[name="items_isBasicInsurance"]:checked').value);

            formData.append('items_BissicInsurance_detailThai',$("#items_BissicInsurance_detailThai").val());
            formData.append('items_BissicInsurance_detailEnglish',$("#items_BissicInsurance_detailEnglish").val());
            formData.append('items_BissicInsurance_detailLaos',$("#items_BissicInsurance_detailLaos").val());
            formData.append('items_BissicInsurance_detailChinese',$("#items_BissicInsurance_detailChinese").val());

            formData.append('bookingConditioncol_detailThai_13',$("#bookingConditioncol_detailThai_id_13").val());
            formData.append('bookingConditioncol_detailEnglish_13',$("#bookingConditioncol_detailEnglish_id_13").val());
            formData.append('bookingConditioncol_detailLaos_13',$("#bookingConditioncol_detailLaos_id_13").val());
            formData.append('bookingConditioncol_detailChinese_13',$("#bookingConditioncol_detailChinese_id_13").val());

            for(var i=0;i<box_field_documents;i++)
            {
                var BookingCondition_14_detail= {
                bookingConditioncol_14_detailThai :$("#bookingConditioncol_detailThai_id_14_"+(i+1)).val(),
                bookingConditioncol_14_detailEnglish :$("#bookingConditioncol_detailEnglish_id_14_"+(i+1)).val(),
                bookingConditioncol_14_detailChinese :$("#bookingConditioncol_detailChinese_id_14_"+(i+1)).val(),
                bookingConditioncol_14_detailLaos :$("#bookingConditioncol_detailLaos_id_14_"+(i+1)).val(),
                }
                BookingCondition_14.push(BookingCondition_14_detail)
            }
            formData.append('BookingCondition_14',JSON.stringify(BookingCondition_14));

            for (var pair of formData.entries()) {
                console.log(pair[0]+ ', ' + pair[1]); 
            }
            if(checktypefile() == 0)
            {
                ajaxSaveItem(formData,"saveEditItem","Reservations")
            }
            else
            {
                var element = document.getElementById("notifi_error_save");
                element.style.color = "red";
                $("#notifi_error_save").css('display', 'inline', 'important');
                element.innerHTML = "Please select only jpg/png file.";
                $("#save").prop("disabled", false);
            }

        });
    });

    
});
async function SetData()
{
    if(items_isBasicInsurance == "1")
    {
        $('#set_items_isBasicInsurance').iCheck('check'); 
    }
    var typeImage = document.getElementById('image_crop_img').src.split('.')
    var file = await srcToFile(document.getElementById('image_crop_img').src,"temp_image_crop_img",'image/'+typeImage[1])
    handleFileSelect(file,document.getElementById('temp_inputImage')) 

    
    
    for (i = 0; i < Object.keys(BookingCondition).length; i++) 
    {
        if(BookingCondition[i].bookingConditionCategory_id == "13")
        {
            document.getElementById('bookingConditioncol_detailThai_id_13').value = BookingCondition[i].bookingConditioncol_detailThai;
            document.getElementById('bookingConditioncol_detailThai_id_13').innerHTML = BookingCondition[i].bookingConditioncol_detailThai;
            document.getElementById('bookingConditioncol_detailEnglish_id_13').value = BookingCondition[i].bookingConditioncol_detailEnglish;
            document.getElementById('bookingConditioncol_detailEnglish_id_13').innerHTML = BookingCondition[i].bookingConditioncol_detailEnglish;
            document.getElementById('bookingConditioncol_detailChinese_id_13').value = BookingCondition[i].bookingConditioncol_detailChinese;
            document.getElementById('bookingConditioncol_detailChinese_id_13').innerHTML = BookingCondition[i].bookingConditioncol_detailChinese;
            document.getElementById('bookingConditioncol_detailLaos_id_13').value = BookingCondition[i].bookingConditioncol_detailLaos;
            document.getElementById('bookingConditioncol_detailLaos_id_13').innerHTML = BookingCondition[i].bookingConditioncol_detailLaos;
            
        }
        else if(BookingCondition[i].bookingConditionCategory_id == "14")
        {
            setTimeout(function () {
                box_field_documents++;
                adddocuments();
                
            }, 100); 

        }

    }
    var k = 1;
    setTimeout(function () {
        for (i = 0; i < Object.keys(BookingCondition).length; i++) 
        {
            if(BookingCondition[i].bookingConditionCategory_id == "14")
            {
                document.getElementById('bookingConditioncol_detailThai_id_14_'+k).value = BookingCondition[i].bookingConditioncol_detailThai;
                document.getElementById('bookingConditioncol_detailThai_id_14_'+k).innerHTML = BookingCondition[i].bookingConditioncol_detailThai;
                document.getElementById('bookingConditioncol_detailEnglish_id_14_'+k).value = BookingCondition[i].bookingConditioncol_detailEnglish;
                document.getElementById('bookingConditioncol_detailEnglish_id_14_'+k).innerHTML = BookingCondition[i].bookingConditioncol_detailEnglish;
                document.getElementById('bookingConditioncol_detailChinese_id_14_'+k).value = BookingCondition[i].bookingConditioncol_detailChinese;
                document.getElementById('bookingConditioncol_detailChinese_id_14_'+k).innerHTML = BookingCondition[i].bookingConditioncol_detailChinese;
                document.getElementById('bookingConditioncol_detailLaos_id_14_'+k).value = BookingCondition[i].bookingConditioncol_detailLaos;
                document.getElementById('bookingConditioncol_detailLaos_id_14_'+k).innerHTML = BookingCondition[i].bookingConditioncol_detailLaos;
                k++
            }
            
        }
    }, 1200); 
}