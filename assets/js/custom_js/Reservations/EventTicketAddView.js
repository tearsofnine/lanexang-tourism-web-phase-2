$(document).ready(function(){
    $.ajax({
        type: 'post',
        url: host+'/Reservations/'+MenuName+'/getNameCategorySubCategory',
        success: function (response) {
          $("#namesubcategory").empty();
          $("#namesubcategory").append(response);
          $('#namesubcategory').trigger("chosen:updated");
        }
      });
    
    var $owl = $(".owl-carousel");
    $owl.owlCarousel({
        loop:false,
        margin:10,
        nav:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }   
    });
    

    $(document).on("click", "#save", function () {
        $("#save").prop("disabled", true);
        resetNotification();
        var formData = new FormData();
        var BookingCondition = [];
        var PhotoText = [];

        formData.append('box_cover_image_event_ticket',box_cover_image_event_ticket);
        for (var i = 0; i < box_cover_image_event_ticket; i++) { 
          formData.append("cropperImage_"+(i+1),document.getElementById("img_event_ticket_fname_"+(i+1)).files[0]);
        }
        formData.append('coverItem_url',$("#coverItem_url").val());

        formData.append('items_priceguestAdult',$("#items_priceguestAdult").val());
        formData.append('items_priceguestChild',$("#items_priceguestChild").val());

        formData.append('itmes_topicThai',$("#itmes_topicThai").val());
        formData.append('itmes_topicEnglish',$("#itmes_topicEnglish").val());
        formData.append('itmes_topicLaos',$("#itmes_topicLaos").val());
        formData.append('itmes_topicChinese',$("#itmes_topicChinese").val());
        
        formData.append('namesubcategory',$("#namesubcategory").val());

        formData.append('items_highlightsThai',$("#items_highlightsThai").val());
        formData.append('items_highlightsEnglish',$("#items_highlightsEnglish").val());
        formData.append('items_highlightsLaos',$("#items_highlightsLaos").val());
        formData.append('items_highlightsChinese',$("#items_highlightsChinese").val());

        formData.append('country',$("#country").val());
        formData.append('provinces',$("#provinces").val());
        formData.append('districts',$("#districts").val());
        formData.append('subdistricts',$("#subdistricts").val());

        formData.append('items_contactThai',$("#items_contactThai").val());
        formData.append('items_contactEnglish',$("#items_contactEnglish").val());
        formData.append('items_contactLaos',$("#items_contactLaos").val());
        formData.append('items_contactChinese',$("#items_contactChinese").val());
        formData.append('items_latitude',$("#items_latitude").val());
        formData.append('items_longitude',$("#items_longitude").val());

        formData.append('dayofweek',$("#dayofweek").val());
        formData.append('box_field_timepicker',box_field_timepicker);
        for (var i = 0; i < box_field_timepicker; i++) { 
            formData.append('items_timeOpen_'+(i+1),$("#items_timeOpen_"+(i+1)).val());
            formData.append('items_timeClose_'+(i+1),$("#items_timeClose_"+(i+1)).val());
        }

        formData.append('box_cover_image_location_illustrations',box_cover_image_location_illustrations);

        formData.append('topicdetail_id_1_Thai',$("#topicdetail_id_1_Thai").val());
        formData.append('topicdetail_id_1_English',$("#topicdetail_id_1_English").val());
        formData.append('topicdetail_id_1_Laos',$("#topicdetail_id_1_Laos").val());
        formData.append('topicdetail_id_1_Chinese',$("#topicdetail_id_1_Chinese").val());
        
        for (var i = 0; i < box_cover_image_location_illustrations; i++) { 
            formData.append("img_location_illustrations_fname_"+(i+1),document.getElementById("img_location_illustrations_fname_"+(i+1)).files[0]);
            PhotoText_detail={
                photo_textThai:$("#photo_text_location_illustrations_"+(i+1)).val(),
                photo_textEnglish:$("#photo_text_location_illustrations_"+(i+1)+"_English").val(),
                photo_textChinese:$("#photo_text_location_illustrations_"+(i+1)+"_Chinese").val(),
                photo_textLaos:$("#photo_text_location_illustrations_"+(i+1)+"_Laos").val()
            }
            PhotoText.push(PhotoText_detail)
        }
        formData.append('PhotoText',JSON.stringify(PhotoText));
  
        for(var i=8;i<13;i++)
        {
            var BookingCondition_detail= {
            bookingConditioncol_detailThai :$("#bookingConditioncol_detailThai_id_"+(i)).val(),
            bookingConditioncol_detailEnglish :$("#bookingConditioncol_detailEnglish_id_"+i).val(),
            bookingConditioncol_detailChinese :$("#bookingConditioncol_detailChinese_id_"+i).val(),
            bookingConditioncol_detailLaos :$("#bookingConditioncol_detailLaos_id_"+i).val(),
            }
            BookingCondition.push(BookingCondition_detail)
        }
        formData.append('bookingConditioncol',JSON.stringify(BookingCondition));

        // Display the values
        for (var pair of formData.entries()) {
            console.log(pair[0]+ ', ' + pair[1]); 
        }
        if(checktypefile() == 0)
        {
            ajaxSaveItem(formData,"saveNewItem","Reservations")
        }
        else
        {
            var element = document.getElementById("notifi_error_save");
            element.style.color = "red";
            $("#notifi_error_save").css('display', 'inline', 'important');
            element.innerHTML = "Please select only jpg/png file.";
$("#save").prop("disabled", false);
        }
    });

});