/* ------- Preloader ------ */
var btnAddElementById;

$(document).ready(function(){

   
   
    $(window).on('load', function(){ 
        $(".status").fadeOut();
        $(".preloader").fadeOut("slow");
    });

    if(document.getElementById("sidebar_"+MenuName))
        document.getElementById("sidebar_"+MenuName).classList.add("active");
    
    
    if(MenuName.includes("CarRentAds") ||
        MenuName.includes("Coupon") ||
        MenuName.includes("CoverProgramTourOfficial") ||
        MenuName.includes("EventTicketAds") ||
        MenuName.includes("HotelAds") ||
        MenuName.includes("News") ||
        MenuName.includes("PackageToursAds") ||
        MenuName.includes("Sticker") ||
        MenuName.includes("VideoContent") 
    )
    {
        $("#sidebar_MediaAndAdvertising_a").click();    
    }
    if(MenuName.includes("CarRent") ||
        MenuName.includes("EventTicket") ||
        MenuName.includes("PackageTours") ||
        MenuName.includes("ApprovalAttractions") ||
        MenuName.includes("ApprovalHotel") ||
        MenuName.includes("ApprovalRestaurant") ||
        MenuName.includes("ApprovalShopping") ||
        MenuName.includes("ApprovalCarRent") ||
        MenuName.includes("ApprovalEventTicket") ||
        MenuName.includes("ApprovalPackageTours") ||
        MenuName.includes("ApprovalProgramTour")||
        MenuName.includes("EntrepreneurApprovalAttractions")
    )
    {
        if(MenuName.includes("Approval" ))
        {
            $("#sidebar_Approval_a").click();
            if(MenuName.includes("EntrepreneurApproval"))
            {
                reNameMenu = MenuName.replace("Entrepreneur", "")
                document.getElementById("sidebar_"+reNameMenu).classList.add("active");
            }
        }
        else
        {
            if(MenuName.includes("CarRent") ||
                MenuName.includes("EventTicket") ||
                MenuName.includes("PackageTours") )
            {
                if(MenuName.includes("Booking" ))
                {
                    $("#sidebar_Booking_a").click();
                }
                else
                    $("#sidebar_Reservations_a").click();
            }
        }
    }

    $(document).on("click", ".btn_information", function () {
        
        $.ajax({
            type: 'post',
            url: host_entrepreneur+'/Main/get_information_type_page',
                success: function (response) {
                    window.location.href ='/dasta_thailand/entrepreneur/'+response.typepage;
                }
            });
    });
     
    $(".chosen-select").chosen({
        disable_search_threshold: 10,
        search_contains: true,
        width: "100%"
    });
    $(".chosen-select-province-boundary").chosen({
        
        search_contains: true,
        width: "100%"
    });
    $(".chosen-select-dayofweek").chosen({
        disable_search_threshold: 10,
        max_selected_options: 2,
        search_contains: true,
        width: "100%"
    });
    $(".chosen-select-delicious").chosen({
        disable_search_threshold: 10,
        search_contains: true,
        width: "100%"
    });
    $(".chosen-select-facilities").chosen({
        disable_search_threshold: 10,
        search_contains: true,
        width: "100%"
    });

    var image = document.getElementById('image_crop_img');
    if(image != null)
    {
        var options = {
            //aspectRatio:16/9,
            preview: '.img-preview',
            //dragMode: 'move',
            viewMode:2,
        };
        cropper = new Cropper(image, options);
    }
    $(document).on("click", "#btn_reset_crop", function () {
        cropper.reset();
    });
    var inputImage = $("#inputImage");

    inputImage.change(function () {
        var fileReader = new FileReader(),
            files = this.files,
            file;
        if (!files.length) {
            return;
        }
        file = files[0];
        
        if (/^image\/\w+$/.test(file.type)) 
        {
            fileReader.onload = function () {
                image.src = fileReader.result;
                cropper.destroy();
                var options = {
                    //aspectRatio:16/9,
                    preview: '.img-preview',
                    //dragMode: 'move',
                    viewMode:2,
                };
                cropper = new Cropper(image, options);
            };
            fileReader.readAsDataURL(file);
        }
        else if(file == null)
        {
            console.log("Please choose an image file.");
        }
        else {
            console.log("Please choose an image file.");
        }
    });

    $(".telephone").map(function() {return $(this).attr('id');}).get().forEach((element, index) => {
        setInputFilter(document.getElementById(element), function(value) {return /^-?\d*[-]?\d*[,]?\d*$/.test(value);});
    });
    
    $(".latitude").map(function() {return $(this).attr('id');}).get().forEach((element, index) => {
        setInputFilter(document.getElementById(element), function(value) {return /^-?\d*[.]?\d*$/.test(value);});
        setInputLatLng(document.getElementById(element));
        setInputLatLng(document.getElementById(element));
    });

    $(".longitude").map(function() {return $(this).attr('id');}).get().forEach((element, index) => {
        setInputFilter(document.getElementById(element), function(value) {return /^-?\d*[.]?\d*$/.test(value);});
        setInputLatLng(document.getElementById(element));
    });

    $(".typenumber").keypress(function (evt) {
        evt.preventDefault();
    });

    $(".typenumberEventTicketPrice").map(function() {return $(this).attr('id');}).get().forEach((element, index) => {
        setInputFilter2(document.getElementById(element), function(value) {
            return /^\d*\.?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
          });
    });

    $(document).on("click", ".btn-program-tour-del", function () {
        var confirms = confirm("Select Yes below if you are ready to delete your data");
        if (confirms == true) {
            $.ajax({
                type: "POST",
                url: host+'/management/ItemsEvent/DeleteProgramTour',
                data: {programTour_id: this.value},
                success: function (response) {
                    table.ajax.reload(null, false);
                }
            });
        }
        //console.log(this.value)
    }); 
    $('#EditInfomationModal').on('hidden.bs.modal', function () {
        location.reload();
        
      })
    $(document).on("click", "#btn_save_info", function () {
        resetNotification();
        var formData = new FormData();
        formData.append("info_user_firstName", $("#info_user_firstName").val());
        formData.append("info_user_lastName", $("#info_user_lastName").val());
        formData.append("info_user_age", $("#info_user_age").val());
        formData.append("info_user_gender", $("#info_user_gender").val());
        formData.append("info_user_phone", $("#info_user_phone").val());
        for (var value of formData.values()) {
			console.log(value);
		}
        $.ajax({
            type: "POST",
            url: host+'/management/UserEvent/updateUserInfo',
            data: formData,
			contentType: false,
			processData: false,
        
            success: function (response) {
                if(response.state)
                {
                    $('#EditInfomationModal').modal('hide');
                }
                else
                {
                    if(response.error == 'validation')
                    { 
                        msg = response.msg.replace(/<p>The /g, "");  
                        msg = msg.replace(/ field is required.<\/p>/g,"");
                        msg =msg.split('\n');
                        msg.forEach((element, index) => {
                                var ids = element.split('|');
                                if(ids[0] == "required")
                                {
                                    var element = document.getElementById("notifi_"+ids[2]);
                                    element.style.color = "red";
                                    $("#notifi_"+ids[2]).css('display', 'inline', 'important');
                                    $('#notifi_'+ids[2]).text(ids[3]);
                                    document.getElementById(ids[1]).focus();
                                }
                        });
                    }
                    else
                    {
                        var element = document.getElementById("notifi_info_error_save_info");
                        element.style.color = "red";
                        $("#notifi_info_error_save_info").css('display', 'inline', 'important');
                        element.innerHTML = response.msg;
                    }
                }
            } 
        });
        
    });
    $(document).on("click", "#btn_save_ChangePassword", function () {
        resetNotification();
        var oldpass = document.getElementById("oldpass").value;
        var newpass = document.getElementById("newpass").value;
        var conpass = document.getElementById("conpass").value;
        $.ajax({
            type: "POST",
            url: host+'/management/UserEvent/ChangePassword',
            data: {oldpass: oldpass,newpass: newpass,conpass: conpass},
        
            success: function (response) {
                if(response.state)
                {
                    $('#ChangePasswordModal').modal('hide');
                }
                else
                {
                    if(response.error == 'validation')
                    { 
                        msg = response.msg.replace(/<p>The /g, "");  
                        msg = msg.replace(/ field is required.<\/p>/g,"");
                        msg =msg.split('\n');
                        msg.forEach((element, index) => {
                                var ids = element.split('|');
                                if(ids[0] == "required")
                                {
                                    var element = document.getElementById("notifi_"+ids[2]);
                                    element.style.color = "red";
                                    $("#notifi_"+ids[2]).css('display', 'inline', 'important');
                                    $('#notifi_'+ids[2]).text(ids[3]);
                                    document.getElementById(ids[1]).focus();
                                }
                        });
                    }
                    else
                    {
                        var element = document.getElementById("notifi_error_save_ChangePassword");
                        element.style.color = "red";
                        $("#notifi_error_save_ChangePassword").css('display', 'inline', 'important');
                        element.innerHTML = response.msg;
                    }
                }
            } 
        });
    });
    $(document).on("click", "#btn_save_ChangePassword_entrepreneur", function () {
        resetNotification();
        var oldpass = document.getElementById("oldpass").value;
        var newpass = document.getElementById("newpass").value;
        var conpass = document.getElementById("conpass").value;
        $.ajax({
            type: "POST",
            url: host_entrepreneur+'/management/UserEvent/ChangePassword',
            data: {oldpass: oldpass,newpass: newpass,conpass: conpass},
        
            success: function (response) {
                if(response.state)
                {
                    $('#ChangePasswordModal').modal('hide');
                }
                else
                {
                    if(response.error == 'validation')
                    { 
                        msg = response.msg.replace(/<p>The /g, "");  
                        msg = msg.replace(/ field is required.<\/p>/g,"");
                        msg =msg.split('\n');
                        msg.forEach((element, index) => {
                                var ids = element.split('|');
                                if(ids[0] == "required")
                                {
                                    var element = document.getElementById("notifi_"+ids[2]);
                                    element.style.color = "red";
                                    $("#notifi_"+ids[2]).css('display', 'inline', 'important');
                                    $('#notifi_'+ids[2]).text(ids[3]);
                                    document.getElementById(ids[1]).focus();
                                }
                        });
                    }
                    else
                    {
                        var element = document.getElementById("notifi_error_save_ChangePassword");
                        element.style.color = "red";
                        $("#notifi_error_save_ChangePassword").css('display', 'inline', 'important');
                        element.innerHTML = response.msg;
                    }
                }
            } 
        });
    });
    $(document).on("click", "#btn_refresh", function () {
        table.ajax.reload();
        resetData();
        UpdateApprovalData()
    });
    $(document).on("change", "#find_approval", function () {

        table.ajax.reload();
        resetData();
        UpdateApprovalData()
        
    }); 
    $(document).on("click", ".ViewItemsModal", function () {
        if(TypeMenu.includes("Items") )
            getItemsData(this.value)
        else if(TypeMenu.includes("Reservations"))
            getReservationsData(this.value)
        else if(TypeMenu.includes("ProgramTour"))
            getProgramTourData(this.value)
    });
    $(document).on("click", ".btn-product-del", function () {
        var confirms = confirm("Select Yes below if you are ready to delete your data");
        if (confirms == true) 
        {
            switch (MenuName) {
                case 'Restaurant':
                case 'Shopping':
                    $.ajax({
                        type: "POST",
                        url: host_entrepreneur+'/management/ItemsEvent/DeleteProduct',
                        data: {product_id: this.value},
                    
                        success: function (response) {
                            table.ajax.reload(null, false);
                            setTimeout( function () {
                                var $owl_old = $(".owl-carousel");
                                $owl_old.trigger('destroy.owl.carousel');
                                $owl_old.html($owl_old.find('.owl-stage-outer').html()).removeClass('owl-loaded');
                                var $owl = $(".owl-carousel");
                                $owl.owlCarousel({
                                    items : 1, 
                                    loop:false,
                                    margin:5,
                                    nav:false
                                });
                            }, 500 );
                    
                            $(".gallery-product").remove();
                           
                        } 
                    });
                break;
                case 'Hotel':
                    $.ajax({
                        type: "POST",
                        url: host_entrepreneur+'/management/ItemsEvent/DeleteRoom',
                        data: {room_id: this.value},
                    
                        success: function (response) {
                            table.ajax.reload(null, false);
                            setTimeout( function () {
                                 var $owl_old = $(".owl-carousel");
                                $owl_old.trigger('destroy.owl.carousel');
                                $owl_old.html($owl_old.find('.owl-stage-outer').html()).removeClass('owl-loaded');
                                var $owl = $(".owl-carousel");
                                $owl.owlCarousel({
                                    items : 1, 
                                    loop:false,
                                    margin:5,
                                    nav:false
                                });
                            }, 500 );
                    
                            $(".gallery-product").remove();
                           
                        } 
                    });
                break;
            }
            
           
        }
    })
    $(document).on("click", ".btn-item-del", function () {
        var confirms = confirm("Select Yes below if you are ready to delete your data");
        if (confirms == true) {
            if(MenuName.includes("EntrepreneurApproval"))
            {
                $.ajax({
                    type: "POST",
                    url: host+'/management/UserEvent/DeleteUser',
                    data: {user_id: this.value},
                
                    success: function (response) {
                        table.ajax.reload();
                        resetData();
                        UpdateApprovalData()
                    } 
                });
            }
            else
            {
                if(TypeMenu.includes("MediaAndAdvertising") )
                {
                    $.ajax({
                        type: "POST",
                        url: host+'/management/MediaAndAdvertisingEvent/DeleteMediaAndAdvertising',
                        data: {mediaAndAdvertising_id: this.value},
                        success: function (response) {
                            table.ajax.reload();
                            resetData();
                            UpdateApprovalData()
                        }
                    });
                }
                else  if(TypeMenu.includes("VideoContent") )
                {
                    $.ajax({
                        type: "POST",
                        url: host+'/management/MediaAndAdvertisingEvent/DeleteVideoContent',
                        data: {videoContent_id: this.value},
                        success: function (response) {
                            table.ajax.reload();
                            resetData();
                            UpdateApprovalData()
                        }
                    });
                }
                else
                {
                    $.ajax({
                        type: "POST",
                        url: host+'/management/ItemsEvent/DeleteItem',
                        data: {items_id: this.value},
                        success: function (response) {
                            table.ajax.reload();
                            resetData();
                            UpdateApprovalData()
                        }
                    });
                }
            }
        }
       
    }); 
    $(document).on("click", ".btn-item-approve", function () {
        var confirms = confirm("Select Yes below if you are ready to Approve this data");
        if (confirms == true) {
            if(MenuName.includes("EntrepreneurApproval"))
            {
                $.ajax({
                    type: "POST",
                    url: host+'/management/UserEvent/ApprovUser',
                    data: {user_id: this.value},
                
                    success: function (response) {
                        table.ajax.reload();
                        resetData();
                        UpdateApprovalData()
                    }
                });
            }
            else if(MenuName.includes("ApprovalProgramTour"))
            {
                $.ajax({
                    type: "POST",
                    url: host+'/management/ItemsEvent/ApproveProgramTour',
                    data: {programTour_id: this.value},
                
                    success: function (response) {
                        table.ajax.reload();
                        resetData();
                        UpdateApprovalData()
                    }
                });
            }
            else
            {
                $.ajax({
                    type: "POST",
                    url: host+'/management/ItemsEvent/ApproveItem',
                    data: {items_id: this.value},
                
                    success: function (response) {
                        table.ajax.reload();
                        resetData();
                        UpdateApprovalData()
                    }
                });
            }
            
        }
    }); 
    $(document).on("click", ".btn-item-disapproval", function () {
        var confirms = confirm("Select Yes below if you are ready to Disapproval this data");
        if (confirms == true) {
            if(MenuName.includes("EntrepreneurApproval"))
            {
                $.ajax({
                    type: "POST",
                    url: host+'/management/UserEvent/DisapprovalUser',
                    data: {user_id: this.value},
                
                    success: function (response) {
                        table.ajax.reload();
                        resetData();
                        UpdateApprovalData()
                    }
                });
            }
            else if(MenuName.includes("ApprovalProgramTour"))
            {
                $.ajax({
                    type: "POST",
                    url: host+'/management/ItemsEvent/DisapprovalProgramTour',
                    data: {programTour_id: this.value},
                
                    success: function (response) {
                        table.ajax.reload();
                        resetData();
                        UpdateApprovalData()
                    }
                });
            }
            else
            {
                $.ajax({
                    type: "POST",
                    url: host+'/management/ItemsEvent/DisapprovalItem',
                    data: {items_id: this.value},
                
                    success: function (response) {
                        table.ajax.reload();
                        resetData();
                        UpdateApprovalData()
                    }
                });
            }
           
        }
    }); 
   
    $(document).on("click", ".btn-item-public", function () {
        // var confirms = confirm("Select Yes below if you are ready to delete your data");
        var data = this.value.split('_');

            $.ajax({
                type: "POST",
                url: host+'/management/ItemsEvent/PublicItem',
                data: {
                    items_id: data[0],
                    public_status : data[1]
                },
            
                success: function (response) {
                    table.ajax.reload(null, false);
                    setTimeout( function () {
                        document.getElementById("countData").textContent= table.data().length
                    }, 500 );
                }
            });
        
        //console.log(this.value)
    }); 
    
    $(document).on("click", ".nav-link", function () {
        setTimeout( function () {
            $('#TravelPeriodThai').DataTable({
                destroy: true,
                responsive: true, 
                searching: false,
                paging: false,
            }); 
            $('#TravelPeriodEnglish').DataTable({
                destroy: true,
                responsive: true, 
                searching: false,
                paging: false,
            }); 
            $('#TravelPeriodChinese').DataTable({
                destroy: true,
                responsive: true, 
                searching: false,
                paging: false,
            }); 
            $('#TravelPeriodLaos').DataTable({
                destroy: true,
                responsive: true, 
                searching: false,
                paging: false,
            }); 
    
            
         }, 500 );
    });
   
    $(document).on("click", ".btn-item-view", function () {
        getItemsData(this.value)
    });
    $(document).on("click", ".btn-item-reservations-view", function () {
        getReservationsData(this.value)
    });
    $(document).on("click", ".btn-program-tour-view", function () {
        getProgramTourData(this.value)
    });
    $(document).on("click", ".btn-programtour-view", function () {
        console.log("ProgramTour")
        //getReservationsData(this.value)
    });

    $(document).on("click", ".add_PackageTours_field", function () {
        addPackageToursField(this.name)  
    });

    $(document).on("click", ".add_image_field-Reservations", function () 
    {
        addImageFieldReservations(this.name)
    });
    $(document).on("click", ".btn-del-box-documents", function () 
    {
        var from = this.name.split('_')
        $("#form_group_documents_" + from[2]).remove();
        $("#form_group_documents_English_" + from[2]).remove();
        $("#form_group_documents_Laos_" + from[2]).remove();
        $("#form_group_documents_Chinese_" + from[2]).remove();

        box_field_documents--;

        $('.form-group-documents').each(function (idx, element) {
            element.id = "form_group_documents_"+(idx+1)
        });
        $('.form-group-documents-English').each(function (idx, element) {
            element.id = "form_group_documents_English_"+(idx+1)
        });
        $('.form-group-documents-Laos').each(function (idx, element) {
            element.id = "form_group_documents_Laos_"+(idx+1)
        });
        $('.form-group-documents-Chinese').each(function (idx, element) {
            element.id = "form_group_documents_Chinese_"+(idx+1)
        });

        $('.name_label').each(function (idx, element) {
            element.innerHTML  =""+(idx+1)
        });
        $('.name_label_English').each(function (idx, element) {
            element.innerHTML  =""+(idx+1)
        });
        $('.name_label_Laos').each(function (idx, element) {
            element.innerHTML  =""+(idx+1)
        });
        $('.name_label_Chinese').each(function (idx, element) {
            element.innerHTML  =""+(idx+1)
        });

        $('.bookingConditioncol').each(function (idx, element) {
            element.id  ="bookingConditioncol_detailThai_id_14_"+(idx+1)
        });
        $('.bookingConditioncol_English').each(function (idx, element) {
            element.id  ="bookingConditioncol_detailEnglish_id_14_"+(idx+1)
        });
        $('.bookingConditioncol_Laos').each(function (idx, element) {
            element.id  ="bookingConditioncol_detailLaos_id_14_"+(idx+1)
        });
        $('.bookingConditioncol_Chinese').each(function (idx, element) {
            element.id  ="bookingConditioncol_detailChinese_id_14_"+(idx+1)
        });

        $('.btn-del-box-documents').each(function (idx, element) {
            element.name = "del_boxdocuments_"+(idx+1)
        });
        $('.notifi_items_documents').each(function (idx, element) {
            element.id = "notifi_bookingConditioncol_detailThai_id_14_"+(idx+1)
        });
    });
    $(document).on("click", ".btn-del-box-timepicker", function () 
    {
        var from = this.name.split('_')
        $("#form_group_timepicker_" + from[2]).remove();
        box_field_timepicker--;

        $('.form-group-timepicker').each(function (idx, element) {
            element.id = "form_group_timepicker_"+(idx+1)
        });
        $('.items_timeOpen').each(function (idx, element) {
            element.id = "items_timeOpen_"+(idx+1)
            if(parseInt(from[2]) > 1 && idx !=0 )
                $("#items_timeOpen_"+(idx+1)).pickatime('stop');
        });
        $('.items_timeClose').each(function (idx, element) {
            element.id = "items_timeClose_"+(idx+1)
            if(parseInt(from[2]) > 1 && idx !=0 )
                $("#items_timeClose_"+(idx+1)).pickatime().pickatime('stop');

        });
        $('.notifi_items_timeOpen').each(function (idx, element) {
            element.id = "notifi_items_timeOpen_"+(idx+1)
        });
        $('.notifi_items_timeClose').each(function (idx, element) {
            element.id = "notifi_items_timeClose_"+(idx+1)
        });
        $('.btn-del-box-timepicker').each(function (idx, element) {
            element.name = "del_boxtimepicker_"+(idx+1)
        });

        $('.timepicker').pickatime({
            format: 'HH:i',
            formatLabel: 'HH:i',
              formatSubmit: 'HH:i',
            interval: 30
          }).pickatime('picker');
    });
    $(document).on("click", ".add_box_field_documents", function () 
    {
        box_field_documents++;
        adddocuments();
    });
    $(document).on("click", ".add_box_field_timepicker", function () 
    {
        addBoxFieldTimepicker();
    });

    $(document).on("click", ".add_image_field", function () {
        resetNotification();
        var name = this.name.split('_')
        if(name.length === 1 && !this.name.includes("ProgramTour"))
        {
            eval("image_"+this.name+" = " + "image_"+this.name + " + 1");
            NewImageBox(eval("image_"+this.name+""),this.name);
        }
       
        else if(this.name.includes("image_foodmenu") || this.name.includes("image_shoppingmenu") || this.name.includes("image_hotelroom") )
        {
            index = parseInt(name[2]);
            if(eval("array_image_"+name[1]+"["+(index-1)+"]") == null)
                eval("array_image_"+name[1]+"["+(index-1)+"] = 0");
            eval("array_image_"+name[1]+"["+(index-1)+"] = " + "array_image_"+name[1]+"["+(index-1)+"] + 1");
            NewImageBox(eval("array_image_"+name[1]+"["+(index-1)+"]"),this.name);

            
        }
        else if(this.name === "ProgramTour")
        {
            array_dates_ProgramTour.push(new ProgramTourModels());
            NewImageBox(array_dates_ProgramTour.length,this.name);
            
        }
        else if(this.name.includes("detail_item_ProgramTour"))
        {
            indexProgramTour = parseInt(name[3]);
            indexItem = parseInt(name[4]);
            array_dates_ProgramTour[(indexProgramTour-1)].Itemid[(indexItem-1)].numImgItem += 1;
            NewImageBox( array_dates_ProgramTour[(indexProgramTour-1)].Itemid[(indexItem-1)].numImgItem,this.name);
        }
        else if(this.name.includes("item_ProgramTour") && !this.name.includes("detail_item_ProgramTour"))
        {
            index = parseInt(name[2]);
            if(array_dates_ProgramTour[(index-1)].Itemid.length == 0)
            {
                array_dates_ProgramTour[(index-1)].Itemid.push(new ItemArray());
                NewImageBox( array_dates_ProgramTour[(index-1)].Itemid.length,this.name);
            }
            else
            {
                if(array_dates_ProgramTour[(index-1)].Itemid[(array_dates_ProgramTour[(index-1)].Itemid.length-1)].place != 0)
                {
                    array_dates_ProgramTour[(index-1)].Itemid.push(new ItemArray());
                    NewImageBox( array_dates_ProgramTour[(index-1)].Itemid.length,this.name);
                }
                else{
                    var element = document.getElementById("notifi_item_ProgramTour_"+index)
                    element.style.color = "red";
                    $("#notifi_item_ProgramTour_"+index).css('display', 'inline', 'important');
                    $('#notifi_item_ProgramTour_'+index).text("กรุณาเลือกสถานที่ก่อนหน้าให้แล้วเสร็จ");
                }
            }
        }

        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
    });

    $(document).on("click", ".btn-del", function () {
        var from = this.name.split('_')
        if(!this.name.includes("image_foodmenu") && 
            !this.name.includes("image_shoppingmenu") && 
            !this.name.includes("image_hotelroom") && 
            !this.name.includes("package_tours") && 
            !this.name.includes("event_ticket") && 
            !this.name.includes("location_illustrations") && 
            !this.name.includes("detail_item_ProgramTour")
        )
        {
            var poi_box_id = $(this).closest('.card-poi-'+from[1]+'-box').attr('id');
            var pos_id = getCardPosition(poi_box_id);

            if(!from[1].includes("thingstomeet")){
                $("#form_group_"+from[1]+"_image_" + pos_id).remove();

                $("#form_group_Eng_"+from[1]+"_image_" + pos_id).remove();
                $("#form_group_Laos_"+from[1]+"_image_" + pos_id).remove();
                $("#form_group_Chinese_"+from[1]+"_image_" + pos_id).remove();
            }
            else{
                $("#form_group_"+from[1]+"_image_" + pos_id).remove();
            }
            eval("image_"+from[1]+" = " + "image_"+from[1] + " - 1");
            orderAllPoiBox(from[1]); 
        }
        else if(this.name.includes("detail_item_ProgramTour") )
        {
            var poi_box_id = $(this).closest('.card-poi-'+from[1]+'_'+from[2]+'_'+from[3]+'_'+from[4]+'_'+from[5]+'-box').attr('id');
            var pos_id = getCardPositionProgramTour(poi_box_id);
            $("#form_group_"+from[1]+'_'+from[2]+'_'+from[3]+'_'+from[4]+'_'+from[5]+"_image_" + pos_id).remove();
            $("#form_group_Eng_"+from[1]+'_'+from[2]+'_'+from[3]+'_'+from[4]+'_'+from[5]+"_image_" + pos_id).remove();
            $("#form_group_Laos_"+from[1]+'_'+from[2]+'_'+from[3]+'_'+from[4]+'_'+from[5]+"_image_" + pos_id).remove();
            $("#form_group_Chinese_"+from[1]+'_'+from[2]+'_'+from[3]+'_'+from[4]+'_'+from[5]+"_image_" + pos_id).remove();
            array_dates_ProgramTour[(from[4]-1)].Itemid[(from[5]-1)].numImgItem -= 1;
            orderAllPoiBoxImageProgramTour("ProgramTour");
            
        }
        else if(this.name.includes("image_foodmenu") || this.name.includes("image_shoppingmenu") || this.name.includes("image_hotelroom"))
        {
            var poi_box_id = $(this).closest('.card-poi-'+from[1]+'_'+from[2]+'_'+from[3]+'-box').attr('id');
            var pos_id = getCardPositionProduct(poi_box_id);
            $("#image_box_"+from[1]+'_'+from[2]+'_'+from[3]).trigger('remove.owl.carousel', [(pos_id-1)]).trigger('refresh.owl.carousel');
            $("#image_box_Eng_"+from[1]+'_'+from[2]+'_'+from[3]).trigger('remove.owl.carousel', [(pos_id-1)]).trigger('refresh.owl.carousel');
            $("#image_box_Laos_"+from[1]+'_'+from[2]+'_'+from[3]).trigger('remove.owl.carousel', [(pos_id-1)]).trigger('refresh.owl.carousel');
            $("#image_box_Chinese_"+from[1]+'_'+from[2]+'_'+from[3]).trigger('remove.owl.carousel', [(pos_id-1)]).trigger('refresh.owl.carousel');
            $("#form_group_"+from[1]+'_'+from[2]+'_'+from[3]+"_image_" + pos_id).remove();
            $("#form_group_Eng_"+from[1]+'_'+from[2]+'_'+from[3]+"_image_" + pos_id).remove();
            $("#form_group_Laos_"+from[1]+'_'+from[2]+'_'+from[3]+"_image_" + pos_id).remove();
            $("#form_group_Chinese_"+from[1]+'_'+from[2]+'_'+from[3]+"_image_" + pos_id).remove();
            index = parseInt(from[3]);
            eval("array_image_"+from[2]+"["+(index-1)+"] = " + "array_image_"+from[2]+"["+(index-1)+"] - 1");
            orderAllPoiBox(from[1]+"_"+from[2]+"_"+from[3]); 
        }
        else if(this.name.includes("del_box_package_tours") || this.name.includes("del_box_event_ticket") || this.name.includes("del_box_location_illustrations"))
        {
            var page ="";
            if(this.name.includes("package_tours"))
                page ="package_tours";
            else if(this.name.includes("event_ticket"))
                page ="event_ticket";
            else if(this.name.includes("location_illustrations"))
                page ="location_illustrations";

            var poi_box_id = $(this).closest('.card-poi-'+page+'-box').attr('id');
            var pos_id = getCardPosition(poi_box_id);

            $("#image_box_"+page).trigger('remove.owl.carousel', [(parseInt(pos_id)-1)]).trigger('refresh.owl.carousel');
            $("#image_box_"+page+"_English").trigger('remove.owl.carousel', [(parseInt(pos_id)-1)]).trigger('refresh.owl.carousel');
            $("#image_box_"+page+"_Laos").trigger('remove.owl.carousel', [(parseInt(pos_id)-1)]).trigger('refresh.owl.carousel');
            $("#image_box_"+page+"_Chinese").trigger('remove.owl.carousel', [(parseInt(pos_id)-1)]).trigger('refresh.owl.carousel');
            
            eval("box_cover_image_"+page+" = box_cover_image_"+page+" - 1");

            
            orderAllPoiBoxReservations(page);
        }
    });

    $(document).on("click", ".btn-del-product", function () {
        var from = this.name.split('_')
        var poi_box_id = $(this).closest('.card-poi-'+from[1]+'-box').attr('id');
        var pos_id = getCardPosition(poi_box_id);
        $("#form_group_"+from[1]+"_image_" + pos_id).remove();
        $("#form_group_Eng_"+from[1]+"_image_" + pos_id).remove();
        $("#form_group_Laos_"+from[1]+"_image_" + pos_id).remove();
        $("#form_group_Chinese_"+from[1]+"_image_" + pos_id).remove();
        eval("array_image_"+from[1]).splice((pos_id-1), 1)
        eval("image_"+from[1]+" = " + "image_"+from[1] + " - 1");
        orderAllPoiBox(from[1]);
        orderAllPoiBoxImageProduct(from[1]);  
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
    });

    $(document).on("click", ".btn-del-programtour", function () {
        var from = this.name.split('_')
        var poi_box_id = $(this).closest('.card-poi-'+from[1]+'-box').attr('id');
        var pos_id = getCardPosition(poi_box_id);
        $("#form_group_"+from[1]+"_image_" + pos_id).remove();
        array_dates_ProgramTour.splice((pos_id-1), 1)
        orderAllPoiBox(from[1]);
        orderAllPoiBoxImageProgramTour(from[1]); 
    });

    $(document).on("click", ".btn-edit", function () {
        var from = this.name.split('_')
        if(!this.name.includes("image_foodmenu") && 
        !this.name.includes("image_shoppingmenu") && 
        !this.name.includes("image_hotelroom") && 
        !this.name.includes("package_tours") && 
        !this.name.includes("event_ticket") && 
        !this.name.includes("location_illustrations") && 
        !this.name.includes("detail_item_ProgramTour")
        )
        {
            var poi_box_id = $(this).closest('.card-poi-'+from[1]+'-box').attr('id');
            var pos_id = getCardPosition(poi_box_id);
            $('#img_'+from[1]+'_fname_'+pos_id).click();
        }
        else if(this.name.includes("detail_item_ProgramTour") )
        {
            var poi_box_id = $(this).closest('.card-poi-'+from[1]+'_'+from[2]+'_'+from[3]+'_'+from[4]+'_'+from[5]+'-box').attr('id');
            var pos_id = getCardPositionProgramTour(poi_box_id);
            $('#img_'+from[1]+'_'+from[2]+'_'+from[3]+'_'+from[4]+'_'+from[5]+'_fname_'+pos_id).click();
           
        }
        else if(this.name.includes("image_foodmenu") || this.name.includes("image_shoppingmenu") || this.name.includes("image_hotelroom"))
        {
            var poi_box_id = $(this).closest('.card-poi-'+from[1]+'_'+from[2]+'_'+from[3]+'-box').attr('id');
            var pos_id = getCardPositionProduct(poi_box_id);
           $('#img_'+from[1]+'_'+from[2]+'_'+from[3]+'_fname_'+pos_id).click();
        }
        else if(this.name.includes("edit_box_package_tours") || this.name.includes("edit_box_event_ticket") || this.name.includes("edit_box_location_illustrations"))
        {
            var page ="";
            if(this.name.includes("package_tours"))
                page ="package_tours";
            else if(this.name.includes("event_ticket"))
                page ="event_ticket";
            else if(this.name.includes("location_illustrations"))
                page ="location_illustrations";

            var poi_box_id = $(this).closest('.card-poi-'+page+'-box').attr('id');
            var pos_id = getCardPosition(poi_box_id);
            
            $('#img_'+page+'_fname_'+pos_id).click();
        }
       
    });  

    $(document).on('click', '#trip_up_btn', function (e) {
        var position = this.value.split('_')
        var day = parseInt(position[0]);
        var pos = parseInt(position[1]);
        var pos_array = pos-1;
        if (pos > 1) {
           $('#form_group_box_item_ProgramTour_'+day+'_'+(pos-1)).before($(this).closest('.form-group-box-item')[0]);
            var temp_element = array_dates_ProgramTour[(day-1)].Itemid[pos_array];
            array_dates_ProgramTour[(day-1)].Itemid[pos_array] = array_dates_ProgramTour[(day-1)].Itemid[(pos_array-1)];
            array_dates_ProgramTour[(day-1)].Itemid[(pos_array-1)] = temp_element;
            
            orderAllPoiBox("ProgramTour");
            orderAllPoiBoxImageProgramTour("ProgramTour");   
        }
        
    });

    $(document).on('click', '#trip_down_btn', function (e) {
        var position = this.value.split('_')
        var day = parseInt(position[0]);
        var pos = parseInt(position[1]);
        var pos_array = pos-1;
        if (pos < array_dates_ProgramTour[(day-1)].Itemid.length) {
            $('#form_group_box_item_ProgramTour_'+day+'_'+(pos+1)).after($(this).closest('.form-group-box-item')[0]);
            var temp_element = array_dates_ProgramTour[(day-1)].Itemid[pos_array];
            array_dates_ProgramTour[(day-1)].Itemid[pos_array] = array_dates_ProgramTour[(day-1)].Itemid[(pos_array+1)]
            array_dates_ProgramTour[(day-1)].Itemid[(pos_array+1)] = temp_element

            orderAllPoiBox("ProgramTour");
            orderAllPoiBoxImageProgramTour("ProgramTour"); 
        }

    });

//ProgramTour
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $(document).on("click", ".div_content_info_save", function () {
        var host_selected = (type_page == "html") ? host : host_entrepreneur+"/business"
        resetNotification();
        var  index = this.value.split('_');
        var div_content_info = document.getElementById("div_content_info_"+$(this).val());
        var div_content_show_result = document.getElementById("div_content_show_result_"+$(this).val());
        if($("#item_ProgramTour_"+this.value+"_place").val() != "" && $("#item_ProgramTour_"+this.value+"_place").val() != null)
        {
            div_content_info.style.display = 'none';
            div_content_show_result.style.display = '';
            array_dates_ProgramTour[(index[0]-1)].Itemid[(index[1]-1)].place =  parseInt($("#item_ProgramTour_"+this.value+"_place").val());
            array_dates_ProgramTour[(index[0]-1)].Itemid[(index[1]-1)].country =  parseInt($("#item_ProgramTour_"+this.value+"_country").val());
            array_dates_ProgramTour[(index[0]-1)].Itemid[(index[1]-1)].provinces =  parseInt($("#item_ProgramTour_"+this.value+"_provinces").val());
            array_dates_ProgramTour[(index[0]-1)].Itemid[(index[1]-1)].districts =  parseInt($("#item_ProgramTour_"+this.value+"_districts").val());
            array_dates_ProgramTour[(index[0]-1)].Itemid[(index[1]-1)].category =  parseInt($("#item_ProgramTour_"+this.value+"_category").val());
            array_dates_ProgramTour[(index[0]-1)].Itemid[(index[1]-1)].type =  parseInt($("#item_ProgramTour_"+this.value+"_type").val());

            var data = {
                item_id: parseInt($("#item_ProgramTour_"+this.value+"_place").val())
            }
            $.ajax({
                type: 'post',
                data: data,
                url: host_selected+'/ProgramTour/getDataItem',
                success: function (response) {
                    
                    document.getElementById("item_ProgramTour_"+index[0]+"_"+index[1]+"_itmes_topicThai").innerText = response[0].itmes_topicThai
                    document.getElementById('item_ProgramTour_'+index[0]+"_"+index[1]+'_items_contactThai').innerText =response[0].items_contactThai
                    document.getElementById('item_ProgramTour_'+index[0]+"_"+index[1]+'_category_thai').innerText =response[0].category_thai
                    document.getElementById('item_ProgramTour_'+index[0]+"_"+index[1]+'_subcategory_thai').innerText =response[0].subcategory_thai
                    document.getElementById('item_ProgramTour_'+index[0]+"_"+index[1]+'_coverItem_paths').src = "/dasta_thailand/assets/img/uploadfile/"+response[0].coverItem_paths
                }
            });
        }
        else
        {
            var element = document.getElementById("notifi_place_item_ProgramTour_"+this.value);
            element.style.color = "red";
            $("#notifi_place_item_ProgramTour_"+this.value).css('display', 'inline', 'important');
            $('#notifi_place_item_ProgramTour_'+this.value).text("กรุณาเลือกสถานที่");
        }
    });
    $(document).on("click", ".div_content_show_result_edit", function () {
        resetNotification();
        var div_content_info = document.getElementById("div_content_info_"+$(this).val());
        var div_content_show_result = document.getElementById("div_content_show_result_"+$(this).val());
        div_content_info.style.display = '';
        div_content_show_result.style.display = 'none';
    });
    $(document).on("click", ".div_content_show_result_del", function () {
        resetNotification();
        index = this.value.split('_');

        $("#form_group_box_item_ProgramTour_" + this.value).remove();
        array_dates_ProgramTour[(index[0]-1)].Itemid.splice((index[1]-1), 1)

        orderAllPoiBox("ProgramTour");
        orderAllPoiBoxImageProgramTour("ProgramTour"); 

    });
    $(document).on("click", ".div_content_info_cancel", function () {
        resetNotification();
        index = this.value.split('_');
        if(array_dates_ProgramTour[(index[0]-1)].Itemid[(index[1]-1)].place == 0 || $("#item_ProgramTour_"+this.value+"_place").val() == "")
        {
            $("#form_group_box_item_ProgramTour_"+this.value).remove();
            array_dates_ProgramTour[(index[0]-1)].Itemid.splice((index[1]-1), 1)
        }
        else
        {
            var div_content_info = document.getElementById("div_content_info_"+$(this).val());
            var div_content_show_result = document.getElementById("div_content_show_result_"+$(this).val());
            div_content_info.style.display = 'none';
            div_content_show_result.style.display = '';
        }

    });
    $('.set_time_period').on('ifClicked', function (event) {
        var time_period =  document.getElementsByClassName("time_period")
        if(this.value == "1")
        {
            $(".time_period").prop("disabled", false);
            time_period[0].value = moment().format('MM/DD/YYYY');
            time_period[1].value = moment().format('MM/DD/YYYY');

            $('#data_5 .input-daterange').datepicker({
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true,
                
            });
        }
        else
        {
            $(".time_period").prop("disabled", true);
            time_period[0].value = "";
            time_period[1].value = "";
        }
    });
    $(document).on('change', '.country', function (e) {
        var select = $('option:selected', this);
        ElementId = this.id.replace(/country(?:|\S)/g,"provinces")
        mainElementId = this.id.replace(/country(?:|\S)/g,"")
        ResetSelectProgramTour("country",mainElementId)
        if(select.val() != ""){
            var data = {
                getOption: select.val()
            }
            $.ajax({
                type: 'post',
                data: data,
                url: host+'/management/LocationManagement/getProvinces',
                success: function (response) {
                    $("#"+ElementId).prop("disabled", false);
                    $("#"+ElementId).empty();
                    $("#"+ElementId).append(response);
                    $('#'+ElementId).trigger("chosen:updated");
                }
            });
        }
    });
    $(document).on('change', '.provinces', function (e) {
        var select = $('option:selected', this);
        ElementId = this.id.replace(/provinces(?:|\S)/g,"districts")
        mainElementId = this.id.replace(/provinces(?:|\S)/g,"")
        ResetSelectProgramTour("provinces",mainElementId)
        if(select.val() != ""){
            var data = {
                getOption: select.val()
            }
            $.ajax({
                type: 'post',
                data: data,
                url: host+'/management/LocationManagement/getDistricts',
                success: function (response) {
                    $("#"+ElementId).prop("disabled", false);
                    $("#"+ElementId).empty();
                    $("#"+ElementId).append(response);
                    $('#'+ElementId).trigger("chosen:updated");
                }
            });
        }
    });
    $(document).on('change', '.districts', function (e) {
        var select = $('option:selected', this);
        var host_selected = (type_page == "html") ? host : host_entrepreneur+"/business"
        ElementId = this.id.replace(/districts(?:|\S)/g,"category")
        mainElementId = this.id.replace(/districts(?:|\S)/g,"")
        ResetSelectProgramTour("districts",mainElementId)
        if(select.val() != ""){
            $.ajax({
                type: 'post',
                url: host_selected+'/ProgramTour/getNameCategory',
                success: function (response) {
                    $("#"+ElementId).prop("disabled", false);
                    $("#"+ElementId).empty();
                    $("#"+ElementId).append(response);
                    $('#'+ElementId).trigger("chosen:updated");
                }
            });
        }
    });
    $(document).on('change', '.category', function (e) {
        var select = $('option:selected', this);
        var host_selected = (type_page == "html") ? host : host_entrepreneur+"/business"
        ElementId = this.id.replace(/category(?:|\S)/g,"type")
        mainElementId = this.id.replace(/category(?:|\S)/g,"")
        ResetSelectProgramTour("category",mainElementId)
        if(select.val() != ""){
            var data = {
                getOption: select.val()
            }
            $.ajax({
                type: 'post',
                data: data,
                url: host_selected+'/ProgramTour/getNameCategorySubCategory',
                success: function (response) {
                    $("#"+ElementId).prop("disabled", false);
                    $("#"+ElementId).empty();
                    $("#"+ElementId).append(response);
                    $('#'+ElementId).trigger("chosen:updated");
                }
            });
        }
    });
    $(document).on('change', '.type', function (e) {
        var select = $('option:selected', this);
        var host_selected = (type_page == "html") ? host : host_entrepreneur+"/business"
        ElementId = this.id.replace(/type(?:|\S)/g,"place")
        mainElementId = this.id.replace(/type(?:|\S)/g,"")
        ResetSelectProgramTour("type",mainElementId)
        if(select.val() != ""){
            var data = {
                subcategory: select.val(),
                districts: $("#"+mainElementId+"districts").val(),
            }
            $.ajax({
                type: 'post',
                data: data,
                url: host_selected+'/ProgramTour/getNamePlace',
                success: function (response) {
                    $("#"+ElementId).prop("disabled", false);
                    $("#"+ElementId).empty();
                    $("#"+ElementId).append(response);
                    $('#'+ElementId).trigger("chosen:updated");
                }
            });
        }
    });
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
});

function NewPackageToursBox(idForm,form,typeAdd = true)
{
    var new_box = "";
    if(form.includes("TravelSchedule"))
    {
        new_box += '<div class="col-lg-12 form-group-'+form+'-image"  id="form_group_'+form+'_image_'+idForm+'">';
        new_box += '<div class="ibox card-poi-'+form+'-box" id="poi_'+form+'_'+idForm+'">';
        new_box += '<div class="ibox-title  back-change">';
        new_box += '<h5>ช่วงเวลาที่ '+idForm+'</h5>';
        new_box += '<div class="ibox-tools">';
        new_box += '<a class="collapse-link">';
        new_box += '<i class="fa fa-chevron-up"></i>';
        new_box += '</a>';
        new_box += '</div>';
        new_box += '</div>';
        new_box += '<div class="ibox-content">';
        new_box += '<div class="form-group  row">';
        new_box += '<label class="col-sm-2 col-form-label">จำนวนที่สามารถจองได้</label>';
        new_box += '<div class="col-sm-10">';
        new_box += '<input type="number" placeholder="จำนวนความจุ (คน)" class="form-control typenumberTravelSchedule" min="0" step="1" id="'+form+'_amount_'+idForm+'">';
        new_box += '</div>';
        new_box += '</div>';
        new_box += '<div class="form-group  row" id="data_5">';
        new_box += '<label class="col-sm-2 col-form-label">วันเดินทางไป-กลับ</label>';
        new_box += '<div class="col-sm-10">';
        new_box += '<div class="input-daterange input-group" id="datepicker">';
        new_box += '<input type="text" class="form-control-sm form-control" id="'+form+'_start_'+idForm+'" value="05/14/2014" />';
        new_box += '<span class="input-group-addon">ถึง</span>';
        new_box += '<input type="text" class="form-control-sm form-control" id="'+form+'_end_'+idForm+'" value="05/22/2014" />';
        new_box += '</div>';
        new_box += '</div>';
        new_box += '</div>';
        new_box += '<div class="title">';
        new_box += '<h4>รายละเอียดราคา</h4>';
        new_box += '</div>';
        new_box += '<div class="form-group row ">';
        new_box += '<label class="col-sm-2 col-form-label">ผู้ใหญ่ <br><p> พัก 2-3 คน</p></label>';
        new_box += '<div class="col-sm-10"><input type="number" placeholder="ราคา" class="form-control typenumberTravelSchedule" min="0" step="1" id="'+form+'_guestAdult_'+idForm+'"></div>';
        new_box += '</div>';
        new_box += '<div class="form-group row ">';
        new_box += '<label class="col-sm-2 col-form-label">ผู้ใหญ่ <br><p> พักเดี่ยว</p> </label>';
        new_box += '<div class="col-sm-10"><input type="number" placeholder="ราคา" class="form-control typenumberTravelSchedule" min="0" step="1" id="'+form+'_guestAdultSingle_'+idForm+'"></div>';
        new_box += '</div>';
        new_box += '<div class="form-group row ">';
        new_box += '<label class="col-sm-2 col-form-label">เด็ก <br><p> เพิ่มเตียง</p></label>';
        new_box += '<div class="col-sm-10"><input type="number" placeholder="ราคา" class="form-control typenumberTravelSchedule" min="0" step="1" id="'+form+'_guestChildBed_'+idForm+'"></div>';
        new_box += '</div>';
        new_box += '<div class="form-group row ">';
        new_box += '<label class="col-sm-2 col-form-label">เด็ก <br><p> ไม่เพิ่มเตียง</p> </label>';
        new_box += '<div class="col-sm-10"><input type="number" placeholder="ราคา" class="form-control typenumberTravelSchedule" min="0" id="'+form+'_guestChild_'+idForm+'"></div>';
        new_box += '</div>';
        new_box += '</div>';
        new_box += '</div>';
        new_box += '</div>';
    }
    else if(form.includes("TravelDetails"))
    {
        new_box += '<div class="col-lg-12 form-group-'+form+'-image" id="form_group_'+form+'_image_'+idForm+'">';
        new_box += '<div class="ibox card-poi-'+form+'-box" id="poi_'+form+'_'+idForm+'">';
        new_box += '<div class="ibox-title  back-change">';
        new_box += '<h5>รายละเอียดการเดินทาง วันที่ '+idForm+'</h5>';
        new_box += '</div>';
        new_box += '<div class="ibox-content">';
        new_box += '<div class="form-group  row">';
        new_box += '<label class="col-sm-2 col-form-label">ชื่อการเดินทาง วันที่ '+idForm+'</label>';
        new_box += '<div class="col-sm-10">';
        new_box += '<div class="input-group">';
        new_box += '<input type="text" placeholder="ชื่อการเดินทาง" class="form-control-sm form-control" id="'+form+'_dateThai_'+idForm+'">';
        new_box += '</div>';
        new_box += '</div>';
        new_box += '</div>';
        new_box += '<div class="form-group  row">';
        new_box += '<label class="col-sm-2 col-form-label">รายละเอียดการเดินทาง วันที่1</label>';
        new_box += '<div class="col-sm-10">';
        new_box += '<textarea class="form-control" placeholder="รายละเอียดการเดินทาง วันที่ '+idForm+'" rows="4" id="'+form+'_textThai_'+idForm+'"></textarea>';
        new_box += '</div>';
        new_box += '</div>';
        new_box += '<div class="form-group  row">';
        new_box += '<label class="col-sm-2 col-form-label">ที่พักโรงแรม</label>';
        new_box += '<div class="col-sm-10">';
        new_box += '<div class="input-group">';
        new_box += '<input type="text" placeholder="ชื่อที่พัก" class="form-control-sm form-control" id="'+form+'_hotel_nameThai_'+idForm+'">';
        new_box += '</div>';
        new_box += '</div>';
        new_box += '</div>';
        new_box += '<div class="form-group  row">';
        new_box += '<label class="col-sm-2 col-form-label">มื้ออาหาร</label>';
        new_box += '<div class="col-sm-10">';
        new_box += '<div class="input-group">';
        new_box += '<div class="i-checks">';
        new_box += '<label><input type="checkbox" id="'+form+'_food_breakfast_'+idForm+'"><i></i>มื้อเช้า </label>';
        new_box += '<label><input type="checkbox" id="'+form+'_food_lunch_'+idForm+'"><i></i> มื้อกลางวัน </label>';
        new_box += '<label><input type="checkbox" id="'+form+'_food_dinner_'+idForm+'"><i></i> มื้อเย็น </label>';
        new_box += '</div>';
        new_box += '</div>';
        new_box += '</div>';
        new_box += '</div>';
        new_box += '</div>';
        new_box += '</div>';
        new_box += '</div>';

        
        var tab_name = ['English','Laos','Chinese'];
        for(i=0;i<3;i++)
        {
            var new_tab_box ='';
            new_tab_box += '<div class="col-lg-12 form-group-'+tab_name[i]+'-'+form+'-image" id="form_group_'+tab_name[i]+'_'+form+'_image_'+idForm+'">';
            new_tab_box += '<div class="ibox card-poi-'+tab_name[i]+'-'+form+'-box" id="poi_'+tab_name[i]+'-'+form+'_'+idForm+'">';
            new_tab_box += '<div class="ibox-title  back-change">';
            new_tab_box += '<h5>รายละเอียดการเดินทาง วันที่ '+idForm+'</h5>';
            new_tab_box += '</div>';
            new_tab_box += '<div class="ibox-content">';
            new_tab_box += '<div class="form-group  row">';
            new_tab_box += '<label class="col-sm-2 col-form-label">ชื่อการเดินทาง วันที่ '+idForm+'</label>';
            new_tab_box += '<div class="col-sm-10">';
            new_tab_box += '<div class="input-group">';
            new_tab_box += '<input type="text" placeholder="ชื่อการเดินทาง" class="form-control-sm form-control" id="'+form+'_date'+tab_name[i]+'_'+idForm+'">';
            new_tab_box += '</div>';
            new_tab_box += '</div>';
            new_tab_box += '</div>';
            new_tab_box += '<div class="form-group  row">';
            new_tab_box += '<label class="col-sm-2 col-form-label">รายละเอียดการเดินทาง วันที่1</label>';
            new_tab_box += '<div class="col-sm-10">';
            new_tab_box += '<textarea class="form-control" placeholder="รายละเอียดการเดินทาง วันที่ '+idForm+'" rows="4" id="'+form+'_text'+tab_name[i]+'_'+idForm+'"></textarea>';
            new_tab_box += '</div>';
            new_tab_box += '</div>';
            new_tab_box += '<div class="form-group  row">';
            new_tab_box += '<label class="col-sm-2 col-form-label">ที่พักโรงแรม</label>';
            new_tab_box += '<div class="col-sm-10">';
            new_tab_box += '<div class="input-group">';
            new_tab_box += '<input type="text" placeholder="ชื่อที่พัก" class="form-control-sm form-control" id="'+form+'_hotel_name'+tab_name[i]+'_'+idForm+'">';
            new_tab_box += '</div>';
            new_tab_box += '</div>';
            new_tab_box += '</div>';
            new_tab_box += '</div>';
            new_tab_box += '</div>';
            new_tab_box += '</div>';

            $("#box_"+tab_name[i]+"_"+form).append(new_tab_box);
        }
        
    }
    $("#box_"+form).append(new_box);

    if(form.includes("TravelSchedule"))
    {
        document.querySelector(".typenumberTravelSchedule").addEventListener("keypress", function (evt) {
            if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57)
            {
                evt.preventDefault();
            }
        });
        $(".typenumberTravelSchedule").map(function() {return $(this).attr('id');}).get().forEach((element, index) => {
            setInputFilter2(document.getElementById(element), function(value) {
                return /^\d*\.?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
              });
        });
    }
    
}
function NewImageBox(idImage,imageform,typeAdd = true)
{
    var new_image_box = "";
    var new_image_Eng_box = "";
    var new_image_Laos_box = "";
    var new_image_Chinese_box = "";
    if(imageform === "thingstomeet")
    {
        new_image_box += '<div class="gallery container-image-field form-group-'+imageform+'-image" id="form_group_'+imageform+'_image_'+idImage+'">';
        new_image_box += '<div class="card-poi-'+imageform+'-box" id="poi_'+imageform+'_'+idImage+'">';
        new_image_box += '<img src="/dasta_thailand/assets/img/image_placeholder_test.jpg" class="image-'+imageform+'-uploaded" id="output_image_'+imageform+'_'+idImage+'">';
        new_image_box += '<button class="btn btn-del btn-danger btn-circle btn-sm" name="del_'+imageform+'"><span aria-hidden="true" style="color: white;">X</span></button>';
        new_image_box += '<button class="btn btn-edit btn-warning btn-circle btn-sm" name="edit_'+imageform+'"><span aria-hidden="true" style="color: white;"><i class="glyphicon glyphicon-pencil"></i></span></button>';
        new_image_box += '<input type="file" class="img-'+imageform+'-fname" style="display:none" accept="image/jpg,image/png,image/jpeg" id="img_'+imageform+'_fname_'+idImage+'" onchange="preview_image(event,\'output_image_'+imageform+'_'+idImage+'\')"> ';
        new_image_box += '<input type="hidden" class="temp-img-'+imageform+'-fname" id="temp_img_'+imageform+'_fname_'+idImage+'"> ';
        new_image_box += '</div>';
        new_image_box += '</div>';
    }
    else if(imageform.includes("image_foodmenu") || imageform.includes("image_shoppingmenu") || imageform.includes("image_hotelroom"))
    {
        new_image_box += '<div class="gallery-product-item product-item-img  container-image-field-food form-group-'+imageform+'-image item" id="form_group_'+imageform+'_image_'+idImage+'">';
        new_image_box += '<div class="card-poi-product card-poi-'+imageform+'-box" id="poi_'+imageform+'_'+idImage+'">';
        new_image_box += '<img src="/dasta_thailand/assets/img/image_placeholder_test.jpg" class="image-'+imageform+'-uploaded image-uploaded-product" id="output_image_'+imageform+'_'+idImage+'">';
        new_image_box += '<button class="btn btn-del btn-danger btn-circle-food btn-sm" name="del_'+imageform+'"><span aria-hidden="true" style="color: white;">X</span></button>';
        new_image_box += '<button class="btn btn-edit btn-warning btn-circle-food btn-sm" name="edit_'+imageform+'"><span aria-hidden="true" style="color: white;"><i class="glyphicon glyphicon-pencil"></i></span></button>';
        new_image_box += '<input type="file" class="img-'+imageform+'-fname img-fname" style="display:none" accept="image/jpg,image/png,image/jpeg" id="img_'+imageform+'_fname_'+idImage+'" onchange="preview_image(event,\'output_image_'+imageform+'_'+idImage+'\')">';
        new_image_box += '<input type="hidden" class="temp-img-'+imageform+'-fname temp-img-fname" id="temp_img_'+imageform+'_fname_'+idImage+'">';
        new_image_box += '</div>';
        new_image_box += '</div>';

        new_image_Eng_box += '<div class="gallery-product-item product-Eng-item-img container-image-field-food form-group-Eng-'+imageform+'-image item" id="form_group_Eng_'+imageform+'_image_'+idImage+'">';
        new_image_Eng_box += '<div class="card-poi-Eng-product card-poi-Eng-'+imageform+'-image-box" id="poi_Eng_'+imageform+'_'+idImage+'">';
        new_image_Eng_box += '<img src="/dasta_thailand/assets/img/image_placeholder_test.jpg" class="image-Eng-'+imageform+'-uploaded image-Eng-uploaded-product" id="output_image_Eng_'+imageform+'_'+idImage+'">';
        new_image_Eng_box += '</div>';
        new_image_Eng_box += '</div>';

        new_image_Laos_box += '<div class="gallery-product-item product-Laos-item-img container-image-field-food form-group-Laos-'+imageform+'-image item" id="form_group_Laos_'+imageform+'_image_'+idImage+'">';
        new_image_Laos_box += '<div class="card-poi-Laos-product card-poi-Laos'+imageform+'-image-box" id="poi_Laos_'+imageform+'_'+idImage+'">';
        new_image_Laos_box += '<img src="/dasta_thailand/assets/img/image_placeholder_test.jpg" class="image-Laos-'+imageform+'-uploaded image-Laos-uploaded-product" id="output_image_Laos_'+imageform+'_'+idImage+'">';
        new_image_Laos_box += '</div>';
        new_image_Laos_box += '</div>'; 

        new_image_Chinese_box += '<div class="gallery-product-item product-Chinese-item-img container-image-field-food form-group-Chinese-'+imageform+'-image item" id="form_group_Chinese_'+imageform+'_image_'+idImage+'">';
        new_image_Chinese_box += '<div class="card-poi-Chinese-product card-poi-Chinese'+imageform+'-image-box" id="poi_Chinese_'+imageform+'_'+idImage+'">';
        new_image_Chinese_box += '<img src="/dasta_thailand/assets/img/image_placeholder_test.jpg" class="image-Chinese-'+imageform+'-uploaded image-Chinese-uploaded-product" id="output_image_Chinese_'+imageform+'_'+idImage+'">';
        new_image_Chinese_box += '</div>';
        new_image_Chinese_box += '</div>'; 

        $("#image_box_Eng_"+imageform).append(new_image_Eng_box);
        $("#image_box_Laos_"+imageform).append(new_image_Laos_box);
        $("#image_box_Chinese_"+imageform).append(new_image_Chinese_box);
        
    }
    else if(imageform === "foodmenu" || imageform ==="shoppingmenu" || imageform ==="hotelroom")
    {
        var nameProduc;
        if(imageform === "foodmenu")
            nameProduc = 'เมนูอาหาร'
        else if(imageform ==="shoppingmenu")
            nameProduc = 'สินค้า'
        else if(imageform ==="hotelroom")
            nameProduc = 'ห้องพัก'
        // ProductFunction.js
        new_image_box = NewBoxProducTH(imageform,idImage,nameProduc);
        new_image_Eng_box = NewBoxProducENG(imageform,idImage,nameProduc);
        new_image_Laos_box = NewBoxProducLAOS(imageform,idImage,nameProduc);
        new_image_Chinese_box = NewBoxProducCHN(imageform,idImage,nameProduc);
        $("#image_box_Eng_"+imageform).append(new_image_Eng_box);
        $("#image_box_Laos_"+imageform).append(new_image_Laos_box);
        $("#image_box_Chinese_"+imageform).append(new_image_Chinese_box);
    }
    else if(imageform === "delicious")
    {
        new_image_box += '<div class="gallery container-image-field form-group-'+imageform+'-image" id="form_group_'+imageform+'_image_'+idImage+'">';
        new_image_box += '<div class="card-poi-'+imageform+'-box" id="poi_'+imageform+'_'+idImage+'">';
        new_image_box += '<div class="">';
        new_image_box += '<img src="/dasta_thailand/assets/img/image_placeholder_test.jpg" class="image-'+imageform+'-uploaded" id="output_image_'+imageform+'_'+idImage+'">';
        new_image_box += '<label class="col-sm-12 col-form-label" id="name_'+imageform+'_'+idImage+'">เนื้อหา</label>';
        new_image_box += '</div>';
        new_image_box += '</div>';
        new_image_box += '</div>';
    }
    else if(imageform.includes("detail_item_ProgramTour"))
    {
        new_image_box += '<div class="gallery-item-img_program-tour container-image-field form-group-'+imageform+'-image" id="form_group_'+imageform+'_image_'+idImage+'">';
        new_image_box += '<div class="card-poi-'+imageform+'-box" id="poi_'+imageform+'_'+idImage+'">';
        new_image_box += '<img src="/dasta_thailand/assets/img/image_placeholder_test.jpg" class="image-'+imageform+'-uploaded" id="output_image_'+imageform+'_'+idImage+'">';
        new_image_box += '<button class="btn btn-del btn-danger btn-circle btn-sm" name="del_'+imageform+'"><span aria-hidden="true" style="color: white;">X</span></button>';
        new_image_box += '<button class="btn btn-edit btn-warning btn-circle btn-sm" name="edit_'+imageform+'"><span aria-hidden="true" style="color: white;"><i class="glyphicon glyphicon-pencil"></i></span></button>';
        new_image_box += '<input type="file" class="img-'+imageform+'-fname" style="display:none" accept="image/jpg,image/png,image/jpeg" id="img_'+imageform+'_fname_'+idImage+'" onchange="preview_image(event,\'output_image_'+imageform+'_'+idImage+'\')"> ';
        new_image_box += '<input type="hidden" class="temp-img-'+imageform+'-fname" id="temp_img_'+imageform+'_fname_'+idImage+'"> ';
        new_image_box += '<textarea class="form-control form-control-textarea  photoTourist-topicThai-'+imageform+'" placeholder="* เขียนบรรยายใต้ภาพของคุณเล็กน้อย  ( เขียนหรือไม่ก็ได้ ) ภาษาไทย"  id="photoTourist_topicThai_'+imageform+'_'+idImage+'" rows="4"></textarea>';
        new_image_box += '</div>';
        new_image_box += '</div>';

        new_image_Eng_box += '<div class="gallery-item-img_program-tour program-tour-Eng-item-img container-image-field form-group-Eng-'+imageform+'-image" id="form_group_Eng_'+imageform+'_image_'+idImage+'">';
        new_image_Eng_box += '<div class="card-poi-Eng-'+imageform+'-box" id="poi_Eng_'+imageform+'_'+idImage+'">';
        new_image_Eng_box += '<img src="/dasta_thailand/assets/img/image_placeholder_test.jpg" class="image-Eng-'+imageform+'-uploaded" id="output_image_Eng_'+imageform+'_'+idImage+'">';
        new_image_Eng_box += '<textarea class="form-control form-control-textarea photoTourist-topicEng-'+imageform+'" placeholder="* เขียนบรรยายใต้ภาพของคุณเล็กน้อย  ( เขียนหรือไม่ก็ได้ ) ภาษาอังกฤษ"  id="photoTourist_topicEnglish_'+imageform+'_'+idImage+'" rows="4"></textarea>';
        new_image_Eng_box += '</div>';
        new_image_Eng_box += '</div>';

        new_image_Laos_box += '<div class="gallery-item-img_program-tour program-tour-Laos-item-img container-image-field form-group-Laos-'+imageform+'-image" id="form_group_Laos_'+imageform+'_image_'+idImage+'">';
        new_image_Laos_box += '<div class="card-poi-Laos-'+imageform+'-box" id="poi_Laos_'+imageform+'_'+idImage+'">';
        new_image_Laos_box += '<img src="/dasta_thailand/assets/img/image_placeholder_test.jpg" class="image-Laos-'+imageform+'-uploaded" id="output_image_Laos_'+imageform+'_'+idImage+'">';
        new_image_Laos_box += '<textarea class="form-control form-control-textarea photoTourist-topicLaos-'+imageform+'" placeholder="* เขียนบรรยายใต้ภาพของคุณเล็กน้อย  ( เขียนหรือไม่ก็ได้ ) ภาษาลาว"  id="photoTourist_topicLaos_'+imageform+'_'+idImage+'" rows="4"></textarea>';
        new_image_Laos_box += '</div>';
        new_image_Laos_box += '</div>';

        new_image_Chinese_box += '<div class="gallery-item-img_program-tour program-tour-Chinese-item-img container-image-field form-group-Chinese-'+imageform+'-image" id="form_group_Chinese_'+imageform+'_image_'+idImage+'">';
        new_image_Chinese_box += '<div class="card-poi-Chinese-'+imageform+'-box" id="poi_Chinese_'+imageform+'_'+idImage+'">';
        new_image_Chinese_box += '<img src="/dasta_thailand/assets/img/image_placeholder_test.jpg" class="image-Chinese-'+imageform+'-uploaded" id="output_image_Chinese_'+imageform+'_'+idImage+'">';
        new_image_Chinese_box += '<textarea class="form-control form-control-textarea photoTourist-topicChinese-'+imageform+'" placeholder="* เขียนบรรยายใต้ภาพของคุณเล็กน้อย  ( เขียนหรือไม่ก็ได้ ) ภาษาจีน"  id="photoTourist_topicChinese_'+imageform+'_'+idImage+'" rows="4"></textarea>';
        new_image_Chinese_box += '</div>';
        new_image_Chinese_box += '</div>';

        $("#image_box_Eng_"+imageform).append(new_image_Eng_box);
        $("#image_box_Laos_"+imageform).append(new_image_Laos_box);
        $("#image_box_Chinese_"+imageform).append(new_image_Chinese_box);

    }
    else if(imageform.includes("item_ProgramTour") && !imageform.includes("detail_item_ProgramTour"))
    {
        // ProgramTourFunction.js
        new_image_box = NewProgramTourItem(imageform,idImage)
    }
    else if(imageform === "ProgramTour")
    {
        new_image_box += '<div class="ibox-content form-group-'+imageform+'-image" id="form_group_'+imageform+'_image_'+idImage+'">';
        new_image_box += '<div class="ibox-title  back-change card-poi-'+imageform+'-box" style="border-width: 2px;border-style: solid;" id="poi_'+imageform+'_'+idImage+'">';
        new_image_box += '<div class="form-group row">';
        new_image_box += '<div class="col-lg-12">';

        new_image_box += '<div class="tabs-container">';
        new_image_box += '<ul class="nav nav-tabs" role="tablist">';
        new_image_box += '<li>';
        new_image_box += '<a class="nav-link AdatesTrip_topic active" data-toggle="tab" href="#datesTrip_topic_'+imageform+'_'+idImage+'_1" id="AdatesTriptabTH"> ภาษาไทย</a>';
        new_image_box += '</li>';
        new_image_box += '<li>';
        new_image_box += '<a class="nav-link AdatesTrip_topic" data-toggle="tab" href="#datesTrip_topic_'+imageform+'_'+idImage+'_2" id="AdatesTriptabENG"> ภาษาอังกฤษ</a>';
        new_image_box += '</li>';
        new_image_box += '<li>';
        new_image_box += '<a class="nav-link AdatesTrip_topic" data-toggle="tab" href="#datesTrip_topic_'+imageform+'_'+idImage+'_3" id="AdatesTriptabLAOS"> ภาษาลาว</a>';
        new_image_box += '</li>';
        new_image_box += '<li>';
        new_image_box += '<a class="nav-link AdatesTrip_topic" data-toggle="tab" href="#datesTrip_topic_'+imageform+'_'+idImage+'_4" id="AdatesTriptabCHN"> ภาษาจีน</a>';
        new_image_box += '</li>';
        new_image_box += '</ul>';
        new_image_box += '<div class="tab-content">';
        new_image_box += '<div role="tabpanel" id="datesTrip_topic_'+imageform+'_'+idImage+'_1" class="tab-pane TabdatesTrip_topic active">';
        new_image_box += '<input type="text" placeholder="วันที่'+idImage+' ภาษาไทย" class="form-control InputdatesTrip_topic" id="datesTrip_topicThai_'+imageform+'_'+idImage+'">';
        new_image_box += '</div>';
        new_image_box += '<div role="tabpanel" id="datesTrip_topic_'+imageform+'_'+idImage+'_2" class="tab-pane TabdatesTrip_topic">';
        new_image_box += '<input type="text" placeholder="วันที่'+idImage+' ภาษาอังกฤษ" class="form-control InputdatesTrip_topic" id="datesTrip_topicEnglish_'+imageform+'_'+idImage+'">';
        new_image_box += '</div>';
        new_image_box += '<div role="tabpanel" id="datesTrip_topic_'+imageform+'_'+idImage+'_3" class="tab-pane TabdatesTrip_topic">';
        new_image_box += '<input type="text" placeholder="วันที่'+idImage+' ภาษาลาว" class="form-control InputdatesTrip_topic" id="datesTrip_topicLaos_'+imageform+'_'+idImage+'">';
        new_image_box += '</div>';
        new_image_box += '<div role="tabpanel" id="datesTrip_topic_'+imageform+'_'+idImage+'_4" class="tab-pane TabdatesTrip_topic">';
        new_image_box += '<input type="text" placeholder="วันที่'+idImage+' ภาษาจีน" class="form-control InputdatesTrip_topic" id="datesTrip_topicChinese_'+imageform+'_'+idImage+'">';
        new_image_box += '</div>';
        new_image_box += '</div>';
        new_image_box += '</div>';
        new_image_box += '</div>';
        new_image_box += '</div>';
        new_image_box += '<div class="ibox-tools">';
        new_image_box += '<button class="btn btn-outline btn-del-programtour" name="del_'+imageform+'">';
        new_image_box += '<i class="fa fa-times"></i>';
        new_image_box += '</button>';
        new_image_box += '</div>';
        new_image_box += '<div class="ibox item-box-item-'+imageform+'_'+idImage+'" id="image_box_item_'+imageform+'_'+idImage+'">';
        new_image_box += '<!-- javascript2  -->';
        new_image_box += '<!-- end javascript2  -->';
        new_image_box += '</div>';
        new_image_box += '<div class="text-center">';
        new_image_box += '<span class="note notifi" id="notifi_item_'+imageform+'_'+idImage+'"></span>';
        new_image_box += '</div>';
        new_image_box += '<div class="text-center">';
        new_image_box += '<button class="btn btn-outline btn-primary add_image_field add_image_field_ItemProgramTour" name="item_'+imageform+'_'+idImage+'">เลือกสถานที่</button>';
        new_image_box += '</div>';
        new_image_box += '</div>';
        new_image_box += '</div>';
    }
    else
    {
        new_image_box += '<div class="form-group row form-group-'+imageform+'-image" id="form_group_'+imageform+'_image_'+idImage+'">';
        new_image_box += '<div class="col-lg-12">';
        new_image_box += '<div class="ibox-content no-padding border-left-right container-image-field card-poi-'+imageform+'-box" id="poi_'+imageform+'_'+idImage+'">';
        new_image_box += '<img class="d-block w-100 image-'+imageform+'-uploaded" src="/dasta_thailand/assets/img/image_placeholder_test.jpg" alt="First slide" id="output_image_'+imageform+'_'+idImage+'">';
        new_image_box += '<button class="btn btn-del btn-danger btn-circle btn-sm" name="del_'+imageform+'"><span aria-hidden="true" style="color: white;">X</span></button>';
        new_image_box += '<button class="btn btn-edit btn-warning btn-circle btn-sm" name="edit_'+imageform+'"><span aria-hidden="true" style="color: white;"><i class="glyphicon glyphicon-pencil"></i></span></button>';
        new_image_box += '<textarea class="form-control photo_textThai_'+imageform+'" placeholder="* เขียนบรรยายใต้ภาพของคุณ" id ="photo_textThai_'+imageform+'_'+idImage+'"></textarea>';
        new_image_box += '<input type="file" class="img-'+imageform+'-fname" style="display:none" accept="image/jpg,image/png,image/jpeg" id="img_'+imageform+'_fname_'+idImage+'" onchange="preview_image(event,\'output_image_'+imageform+'_'+idImage+'\')"> ';
        new_image_box += '<input type="hidden" class="temp-img-'+imageform+'-fname" id="temp_img_'+imageform+'_fname_'+idImage+'"> ';
        new_image_box += '</div>';
        new_image_box += '</div>';
        new_image_box += '</div>';

        new_image_Eng_box += '<div class="form-group row form-group-Eng-'+imageform+'-image" id="form_group_Eng_'+imageform+'_image_'+idImage+'">';
        new_image_Eng_box += '<div class="col-lg-12">';
        new_image_Eng_box += '<div class="ibox-content no-padding border-left-right container-image-field card-poi-Eng-'+imageform+'-box" id="poi_Eng_'+imageform+'_'+idImage+'">';
        new_image_Eng_box += '<img class="d-block w-100 image-Eng-'+imageform+'-uploaded" src="/dasta_thailand/assets/img/image_placeholder_test.jpg" alt="First slide" id="output_image_Eng_'+imageform+'_'+idImage+'">';
        new_image_Eng_box += '<textarea class="form-control photo_textEnglish_'+imageform+'" placeholder="* เขียนบรรยายใต้ภาพของคุณ" id ="photo_textEnglish_'+imageform+'_'+idImage+'"></textarea>';
        new_image_Eng_box += '</div>';
        new_image_Eng_box += '</div>';
        new_image_Eng_box += '</div>';

        new_image_Laos_box += '<div class="form-group row form-group-Laos-'+imageform+'-image" id="form_group_Laos_'+imageform+'_image_'+idImage+'">';
        new_image_Laos_box += '<div class="col-lg-12">';
        new_image_Laos_box += '<div class="ibox-content no-padding border-left-right container-image-field card-poi-Laos-'+imageform+'-box" id="poi_Laos_'+imageform+'_'+idImage+'">';
        new_image_Laos_box += '<img class="d-block w-100 image-Laos-'+imageform+'-uploaded" src="/dasta_thailand/assets/img/image_placeholder_test.jpg" alt="First slide" id="output_image_Laos_'+imageform+'_'+idImage+'">';
        new_image_Laos_box += '<textarea class="form-control photo_textLaos_'+imageform+'" placeholder="* เขียนบรรยายใต้ภาพของคุณ" id ="photo_textLaos_'+imageform+'_'+idImage+'"></textarea>';
        new_image_Laos_box += '</div>';
        new_image_Laos_box += '</div>';
        new_image_Laos_box += '</div>';

        new_image_Chinese_box += '<div class="form-group row form-group-Chinese-'+imageform+'-image" id="form_group_Chinese_'+imageform+'_image_'+idImage+'">';
        new_image_Chinese_box += '<div class="col-lg-12">';
        new_image_Chinese_box += '<div class="ibox-content no-padding border-left-right container-image-field card-poi-Chinese-'+imageform+'-box" id="poi_Chinese_'+imageform+'_'+idImage+'">';
        new_image_Chinese_box += '<img class="d-block w-100 image-Chinese-'+imageform+'-uploaded" src="/dasta_thailand/assets/img/image_placeholder_test.jpg" alt="First slide" id="output_image_Chinese_'+imageform+'_'+idImage+'">';
        new_image_Chinese_box += '<textarea class="form-control photo_textChinese_'+imageform+'" placeholder="* เขียนบรรยายใต้ภาพของคุณ" id ="photo_textChinese_'+imageform+'_'+idImage+'"></textarea>';
        new_image_Chinese_box += '</div>';
        new_image_Chinese_box += '</div>';
        new_image_Chinese_box += '</div>';
        
        $("#image_box_Eng_"+imageform).append(new_image_Eng_box);
        $("#image_box_Laos_"+imageform).append(new_image_Laos_box);
        $("#image_box_Chinese_"+imageform).append(new_image_Chinese_box);
    }
    
    $("#image_box_"+imageform).append(new_image_box);

    $(".price-foodmenu").map(function() {return $(this).attr('id');}).get().forEach((element, index) => {
        setInputFilter(document.getElementById(element), function(value) {return /^-?\d*$/.test(value);});
        PriceProductChange(document.getElementById(element))
    });
    $(".price-shoppingmenu").map(function() {return $(this).attr('id');}).get().forEach((element, index) => {
        setInputFilter(document.getElementById(element), function(value) {return /^-?\d*$/.test(value);});
        PriceProductChange(document.getElementById(element))
    });
    $(".price-hotelroom").map(function() {return $(this).attr('id');}).get().forEach((element, index) => {
        setInputFilter(document.getElementById(element), function(value) {return /^-?\d*$/.test(value);});
        PriceProductChange(document.getElementById(element))
    });
    $(".form-control-textarea").map(function() {return $(this).attr('id');}).get().forEach((element, index) => { 
        FormControlTextareaChange(document.getElementById(element))
    });
    
    if(typeAdd && imageform !== "delicious" && 
        imageform !== "foodmenu" && 
        imageform !== "shoppingmenu" && 
        imageform !== "hotelroom" && 
        imageform !== "ProgramTour" &&
        !imageform.includes("item_ProgramTour")
    )
    {
        btnAddElementById = document.getElementById('img_'+imageform+'_fname_'+idImage)
        btnAddElementById.onclick = charge
        btnAddElementById.click();
    }
    else if(typeAdd &&imageform.includes("detail_item_ProgramTour"))
    {
        btnAddElementById = document.getElementById('img_'+imageform+'_fname_'+idImage)
        btnAddElementById.onclick = charge
        btnAddElementById.click();
    }


    if(imageform.includes("image_foodmenu") || 
        imageform.includes("image_shoppingmenu") ||
        imageform.includes("image_hotelroom")
    )
    {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
        UpdateOwlCarousel()
    }

    $(".chosen-select").chosen({
        disable_search_threshold: 10,
        search_contains: true,
        width: "100%"
    });
    
    if(imageform.includes("item_ProgramTour"))
    {
        $.ajax({
            type: 'post',
            url: host+'/management/LocationManagement/getCountry',
            success: function (response) {
              $("#"+imageform+"_"+idImage+"_country").empty();
              $("#"+imageform+"_"+idImage+"_country").append(response);
              
              $("#"+imageform+"_"+idImage+"_country").trigger("chosen:updated");
            }
          });

          $(".chosen-select-program-tour").chosen({
            disable_search_threshold: 10,
            search_contains: true,
            width: "100%"
        });
    }
}
function UpdateOwlCarousel()
{
    var $owl = $(".owl-carousel");
        $owl.trigger('destroy.owl.carousel');
        $owl.html($owl.find('.owl-stage-outer').html()).removeClass('owl-loaded');
        $owl.owlCarousel({
            autoWidth:true,
            // margin:-10,
        });  
}
function charge()
{
    if(btnAddElementById != null )
    {
        document.body.onfocus =  function (){
            setTimeout(function () {
                roar()
                btnAddElementById = null
            }, 500);
        } 
        
    }
    
}
function roar()
{
    if(btnAddElementById != null)
    {
        if(btnAddElementById.files[0] == null)
        {
            var from = btnAddElementById.id.split('_')
            if(!btnAddElementById.id.includes("image_foodmenu") && 
                !btnAddElementById.id.includes("image_shoppingmenu") &&
                !btnAddElementById.id.includes("image_hotelroom") &&
                !btnAddElementById.id.includes("package_tours") &&
                !btnAddElementById.id.includes("event_ticket") &&
                !btnAddElementById.id.includes("location_illustrations") &&
                !btnAddElementById.id.includes("detail_item_ProgramTour")
            )
            {
                var pos_id = getCardPosition('poi_'+from[1]+'_'+from[3]);
                if(!from[1].includes("thingstomeet")){
                    $("#form_group_"+from[1]+"_image_" + pos_id).remove();
    
                    $("#form_group_Eng_"+from[1]+"_image_" + pos_id).remove();
                    $("#form_group_Laos_"+from[1]+"_image_" + pos_id).remove();
                    $("#form_group_Chinese_"+from[1]+"_image_" + pos_id).remove();
                }
                else{
                    $("#form_group_"+from[1]+"_image_" + pos_id).remove();
                }
                eval("image_"+from[1]+" = " + "image_"+from[1] + " - 1");
                orderAllPoiBox(from[1]);
            }
            else if(btnAddElementById.id.includes("image_foodmenu") || 
                btnAddElementById.id.includes("image_shoppingmenu") ||
                btnAddElementById.id.includes("image_hotelroom"))
            {
                var pos_id = getCardPositionProduct('poi_'+from[1]+"_"+from[2]+"_"+from[3]+'_'+from[5]);
    
                $("#image_box_"+from[1]+'_'+from[2]+'_'+from[3]).trigger('remove.owl.carousel', [(pos_id-1)]).trigger('refresh.owl.carousel');
                $("#image_box_Eng_"+from[1]+'_'+from[2]+'_'+from[3]).trigger('remove.owl.carousel', [(pos_id-1)]).trigger('refresh.owl.carousel');
                $("#image_box_Laos_"+from[1]+'_'+from[2]+'_'+from[3]).trigger('remove.owl.carousel', [(pos_id-1)]).trigger('refresh.owl.carousel');
                $("#image_box_Chinese_"+from[1]+'_'+from[2]+'_'+from[3]).trigger('remove.owl.carousel', [(pos_id-1)]).trigger('refresh.owl.carousel');
    
                $("#form_group_"+from[1]+'_'+from[2]+'_'+from[3]+"_image_" + pos_id).remove();
                $("#form_group_Eng_"+from[1]+'_'+from[2]+'_'+from[3]+"_image_" + pos_id).remove();
                $("#form_group_Laos_"+from[1]+'_'+from[2]+'_'+from[3]+"_image_" + pos_id).remove();
                $("#form_group_Chinese_"+from[1]+'_'+from[2]+'_'+from[3]+"_image_" + pos_id).remove();
                index = parseInt(from[3]);
                eval("array_image_"+from[2]+"["+(index-1)+"] = " + "array_image_"+from[2]+"["+(index-1)+"] - 1");
                orderAllPoiBox(from[1]+"_"+from[2]+"_"+from[3]); 
            }
            else if(btnAddElementById.id.includes("detail_item_ProgramTour"))
            {
                
                var pos_id = getCardPositionProgramTour('poi_'+from[1]+'_'+from[2]+'_'+from[3]+'_'+from[4]+'_'+from[5]+'_'+from[7]);
                $("#form_group_"+from[1]+'_'+from[2]+'_'+from[3]+'_'+from[4]+'_'+from[5]+"_image_" + pos_id).remove();
                $("#form_group_Eng_"+from[1]+'_'+from[2]+'_'+from[3]+'_'+from[4]+'_'+from[5]+"_image_" + pos_id).remove();
                $("#form_group_Laos_"+from[1]+'_'+from[2]+'_'+from[3]+'_'+from[4]+'_'+from[5]+"_image_" + pos_id).remove();
                $("#form_group_Chinese_"+from[1]+'_'+from[2]+'_'+from[3]+'_'+from[4]+'_'+from[5]+"_image_" + pos_id).remove();
                array_dates_ProgramTour[(from[4]-1)].Itemid[(from[5]-1)].numImgItem -= 1;
                orderAllPoiBoxImageProgramTour("ProgramTour");
            }
            else if(btnAddElementById.id.includes("package_tours") || btnAddElementById.id.includes("event_ticket") || btnAddElementById.id.includes("location_illustrations"))
            {
                var page ="";
                if(btnAddElementById.id.includes("package_tours"))
                    page ="package_tours";
                else if(btnAddElementById.id.includes("event_ticket"))
                    page ="event_ticket";
                else if(btnAddElementById.id.includes("location_illustrations"))
                    page ="location_illustrations";

                $("#image_box_"+page).trigger('remove.owl.carousel', [(parseInt(from[4])-1)]).trigger('refresh.owl.carousel');
                $("#image_box_"+page+"_English").trigger('remove.owl.carousel', [(parseInt(from[4])-1)]).trigger('refresh.owl.carousel');
                $("#image_box_"+page+"_Laos").trigger('remove.owl.carousel', [(parseInt(from[4])-1)]).trigger('refresh.owl.carousel');
                $("#image_box_"+page+"_Chinese").trigger('remove.owl.carousel', [(parseInt(from[4])-1)]).trigger('refresh.owl.carousel');
                eval("box_cover_image_"+page+" = box_cover_image_"+page+" - 1");
            }
            document.body.onfocus =null;
            document.body.onblur =null;
        } 
    }
}
function preview_image(event,ElementById) 
{
    var reader = new FileReader();
    if (event.target.files[0] == null)
    {
        console.log("Event cancel")
    }
    else
    {
        var output = document.getElementById(ElementById);
        if(output != null)
        {
            reader.onload = function()
            {
                if(ElementById.includes("thingstomeet"))
                {
                    
                    output.src = reader.result;
                }
                else if(ElementById.includes("package_tours") || ElementById.includes("event_ticket") || ElementById.includes("location_illustrations"))
                {
                    
                    output.src = reader.result;
                    if(ElementById.includes("location_illustrations"))
                    {
                        document.getElementById(ElementById+'_English').src = reader.result;
                        document.getElementById(ElementById+'_Laos').src = reader.result;
                        document.getElementById(ElementById+'_Chinese').src = reader.result;
                    }
                    
                }
                else
                {
                    var ElementEng = [ElementById.slice(0, 12), "_Eng", ElementById.slice(12)].join('');
                    var ElementLao = [ElementById.slice(0, 12), "_Laos", ElementById.slice(12)].join('');
                    var ElementChinese = [ElementById.slice(0, 12), "_Chinese", ElementById.slice(12)].join('');
                    output.src = reader.result;
                    document.getElementById(ElementEng).src = reader.result;
                    document.getElementById(ElementLao).src = reader.result;
                    document.getElementById(ElementChinese).src = reader.result;
                }  
            }
            reader.readAsDataURL(event.target.files[0]);
        }
        
    }
}
function getCardPosition(raw_position) {
    regex = /^poi_(\w+)_(\d+)$/;
    var pos_id = parseInt(raw_position.match(regex)[2]);
    return pos_id;
}
function getCardPositionProduct(raw_position) {
    regex = /^poi_(\w+)_(\w+)_(\d+)_(\d+)$/;
    var pos_id = parseInt(raw_position.match(regex)[4]);
    return pos_id;
}
function getCardPositionProgramTour(raw_position) {
    regex = /^poi_(\w+)_(\d+)_(\d+)_(\d+)$/;
    var pos_id = parseInt(raw_position.match(regex)[4]);
    return pos_id;
}
function orderAllPoiBoxImageProgramTour(imageform) {
    $('.add_image_field_ItemProgramTour').each(function (idx, element) {
        element.name = 'item_'+imageform+'_' + (idx+1);
    });

    $('.form-group-'+imageform+'-image').each(function (idx, element) {
        var item_name = imageform+'_'+(idx+1);
        var index  = (idx+1);
        var targetDivIbox = element.getElementsByClassName("ibox")[0];

        targetDivIbox.className = 'ibox item-box-item-'+item_name;
        targetDivIbox.id = 'image_box_item_'+item_name;
        var AdatesTrip_topic = element.getElementsByClassName("AdatesTrip_topic");
        var TabdatesTrip_topic = element.getElementsByClassName("TabdatesTrip_topic");
        var InputdatesTrip_topic = element.getElementsByClassName("InputdatesTrip_topic");

        for(var i=0; i< AdatesTrip_topic.length;i++)
        {
            AdatesTrip_topic[i].href = "#datesTrip_topic_"+item_name+'_'+(i+1)
            TabdatesTrip_topic[i].id = "datesTrip_topic_"+item_name+'_'+(i+1)
        }

        InputdatesTrip_topic[0].id = "datesTrip_topicThai_"+item_name
        InputdatesTrip_topic[1].id = "datesTrip_topicEnglish_"+item_name;
        InputdatesTrip_topic[2].id = "datesTrip_topicLaos_"+item_name
        InputdatesTrip_topic[3].id = "datesTrip_topicChinese_"+item_name

        InputdatesTrip_topic[0].placeholder  = "วันที่"+(idx+1)+" ภาษาไทย"
        InputdatesTrip_topic[1].placeholder  = "วันที่"+(idx+1)+" ภาษาอังกฤษ"
        InputdatesTrip_topic[2].placeholder  = "วันที่"+(idx+1)+" ภาษาลาว"
        InputdatesTrip_topic[3].placeholder  = "วันที่"+(idx+1)+" ภาษาจีน"

        $('#image_box_item_'+item_name).each(function (idx, element) {
            
            var content_info_cancel = element.getElementsByClassName("div_content_info_cancel");
            var content_info_save = element.getElementsByClassName("div_content_info_save");

            var div_content_info = element.getElementsByClassName("div_content_info");
            var div_content_show_result = element.getElementsByClassName("div_content_show_result");

            var div_content_show_result_edit = element.getElementsByClassName("div_content_show_result_edit");
            var div_content_show_result_del = element.getElementsByClassName("div_content_show_result_del");

            var trip_down_btn = element.getElementsByClassName("trip_down_btn");
            var trip_up_btn = element.getElementsByClassName("trip_up_btn");

            for(var i=0; i< content_info_save.length;i++)
            {
                content_info_save[i].value = index+"_"+(i+1);
                content_info_cancel[i].value = index+"_"+(i+1);
                div_content_info[i].id = "div_content_info_"+index+"_"+(i+1);
                div_content_show_result[i].id = "div_content_show_result_"+index+"_"+(i+1);
                div_content_show_result_edit[i].value = index+"_"+(i+1);
                div_content_show_result_del[i].value = index+"_"+(i+1);
                trip_down_btn[i].value = index+"_"+(i+1);
                trip_up_btn[i].value = index+"_"+(i+1);
            }

            var form_group_box_item = element.getElementsByClassName("form-group-box-item");

            for(var i=0; i< form_group_box_item.length;i++)
            {
                var form_group_box_item_outerHTMLs = form_group_box_item[i].outerHTML+"";
                var regex = /ProgramTour_\d_[\d+$]+(?:|\S)/g;
                form_group_box_item_outerHTMLs = form_group_box_item_outerHTMLs.replace(regex, item_name+"_"+(i+1))
                form_group_box_item[i].outerHTML = form_group_box_item_outerHTMLs
                //console.log(form_group_box_item[i])
                // $("#"+form_group_box_item[i].id).each(function (idx, element) {
                    
                // });
            }

            var detail_item_id = item_name+"_"+(idx+1)
            $('.form-group-detail_item_'+detail_item_id+'-image').each(function (idx, element) {
                element.id = "form_group_detail_item_"+detail_item_id+"_image_"+(idx+1)
            });
            $('.form-group-Eng-detail_item_'+detail_item_id+'-image').each(function (idx, element) {
                element.id = "form_group_Eng_detail_item_"+detail_item_id+"_image_"+(idx+1)
            });
            $('.form-group-Laos-detail_item_'+detail_item_id+'-image').each(function (idx, element) {
                element.id = "form_group_Laos_detail_item_"+detail_item_id+"_image_"+(idx+1)
            });
            $('.form-group-Chinese-detail_item_'+detail_item_id+'-image').each(function (idx, element) {
                element.id = "form_group_Chinese_detail_item_"+detail_item_id+"_image_"+(idx+1)
            });
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $('.card-poi-detail_item_'+detail_item_id+'-box').each(function (idx, element) {
                element.id = "poi_detail_item_"+detail_item_id+"_"+(idx+1)
            });
            $('.card-poi-Eng-detail_item_'+detail_item_id+'-box').each(function (idx, element) {
                element.id = "poi_Eng_detail_item_"+detail_item_id+"_"+(idx+1)
            });
            $('.card-poi-Laos-detail_item_'+detail_item_id+'-box').each(function (idx, element) {
                element.id = "poi_Laos_detail_item_"+detail_item_id+"_"+(idx+1)
            });
            $('.card-poi-Chinese-detail_item_'+detail_item_id+'-box').each(function (idx, element) {
                element.id = "poi_Chinese_detail_item_"+detail_item_id+"_"+(idx+1)
            });
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $('.image-detail_item_'+detail_item_id+'-uploaded').each(function (idx, element) {
                element.id = "output_image_detail_item_"+detail_item_id+"_"+(idx+1)
            });
            $('.image-Eng-detail_item_'+detail_item_id+'-uploaded').each(function (idx, element) {
                element.id = "output_image_Eng_detail_item_"+detail_item_id+"_"+(idx+1)
            });
            $('.image-Laos-detail_item_'+detail_item_id+'-uploaded').each(function (idx, element) {
                element.id = "output_image_Laos_detail_item_"+detail_item_id+"_"+(idx+1)
            });
            $('.image-Chinese-detail_item_'+detail_item_id+'-uploaded').each(function (idx, element) {
                element.id = "output_image_Chinese_detail_item_"+detail_item_id+"_"+(idx+1)
            });
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////          
            $('.img-detail_item_'+detail_item_id+'-fname').each(function (idx, element) {
                element.id = "img_detail_item_"+detail_item_id+"_fname_"+(idx+1)
                element.setAttribute('onchange',"preview_image(event,\'output_image_detail_item_"+detail_item_id+"_"+(idx+1)+"\')");
            });
            $('.temp-img-detail_item'+detail_item_id+'-fname').each(function (idx, element) {
                element.id = "temp_img_detail_item_"+detail_item_id+"_fname_"+(idx+1)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            });
            $('.photoTourist-topicThai-detail_item_'+detail_item_id).each(function (idx, element) {
                element.id = "photoTourist_topicThai_detail_item_"+detail_item_id+"_"+(idx+1)
            });
            $('.photoTourist-topicEng-detail_item_'+detail_item_id).each(function (idx, element) {
                element.id = "photoTourist_topicEnglish_detail_item_"+detail_item_id+"_"+(idx+1)
            });
            $('.photoTourist-topicLaos-detail_item_'+detail_item_id).each(function (idx, element) {
                element.id = "photoTourist_topicLaos_detail_item_"+detail_item_id+"_"+(idx+1)
            });
            $('.photoTourist-topicChinese-detail_item_'+detail_item_id).each(function (idx, element) {
                element.id = "photoTourist_topicChinese_detail_item_"+detail_item_id+"_"+(idx+1)
            });
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            var outerHTMLs = element.outerHTML+"";
            var regex = /ProgramTour_\d(?:|\S)/g;
            outerHTMLs = outerHTMLs.replace(regex, item_name+"" )
            element.outerHTML = outerHTMLs
            for(var i=0; i< form_group_box_item.length;i++)
            {
                $("#item_ProgramTour_"+index+"_"+(i+1)+"_country_chosen").remove()
                $("#item_ProgramTour_"+index+"_"+(i+1)+"_provinces_chosen").remove()
                $("#item_ProgramTour_"+index+"_"+(i+1)+"_districts_chosen").remove()
                $("#item_ProgramTour_"+index+"_"+(i+1)+"_category_chosen").remove()
                $("#item_ProgramTour_"+index+"_"+(i+1)+"_type_chosen").remove()
                $("#item_ProgramTour_"+index+"_"+(i+1)+"_place_chosen").remove()

                $("#item_ProgramTour_"+index+"_"+(i+1)+"_country").chosen({disable_search_threshold: 10,search_contains: true,width: "100%"});
                $("#item_ProgramTour_"+index+"_"+(i+1)+"_provinces").chosen({disable_search_threshold: 10,search_contains: true,width: "100%"});
                $("#item_ProgramTour_"+index+"_"+(i+1)+"_districts").chosen({disable_search_threshold: 10,search_contains: true,width: "100%"});
                $("#item_ProgramTour_"+index+"_"+(i+1)+"_category").chosen({disable_search_threshold: 10,search_contains: true,width: "100%"});
                $("#item_ProgramTour_"+index+"_"+(i+1)+"_type").chosen({disable_search_threshold: 10,search_contains: true,width: "100%"});
                $("#item_ProgramTour_"+index+"_"+(i+1)+"_place").chosen({disable_search_threshold: 10,search_contains: true,width: "100%"});
                
                RestProgramTourData(index,(i+1))
            }

        });
        
    });
}
function RestProgramTourData(idPosProgramTour,idPosItem)
{
    var host_selected = (type_page == "html") ? host : host_entrepreneur+"/business"
    $.ajax({
        type: 'post',
        url: host+'/management/LocationManagement/getCountry',
        success: function (response) {
            $("#item_ProgramTour_"+idPosProgramTour+"_"+idPosItem+"_country").empty();
            $("#item_ProgramTour_"+idPosProgramTour+"_"+idPosItem+"_country").append(response);
            $("#item_ProgramTour_"+idPosProgramTour+"_"+idPosItem+"_country").val(array_dates_ProgramTour[(idPosProgramTour-1)].Itemid[(idPosItem-1)].country);
            $("#item_ProgramTour_"+idPosProgramTour+"_"+idPosItem+"_country").trigger("chosen:updated");
            var data = {
                getOption: array_dates_ProgramTour[(idPosProgramTour-1)].Itemid[(idPosItem-1)].country
            }
            $.ajax({
                type: 'post',
                data: data,
                url: host+'/management/LocationManagement/getProvinces',
                success: function (response) {
                    $("#item_ProgramTour_"+idPosProgramTour+"_"+idPosItem+"_provinces").prop("disabled", false);
                    $("#item_ProgramTour_"+idPosProgramTour+"_"+idPosItem+"_provinces").empty();
                    $("#item_ProgramTour_"+idPosProgramTour+"_"+idPosItem+"_provinces").append(response);
                    $("#item_ProgramTour_"+idPosProgramTour+"_"+idPosItem+"_provinces").val(array_dates_ProgramTour[(idPosProgramTour-1)].Itemid[(idPosItem-1)].provinces);
                    $("#item_ProgramTour_"+idPosProgramTour+"_"+idPosItem+"_provinces").trigger("chosen:updated");
                    var data = {
                        getOption: array_dates_ProgramTour[(idPosProgramTour-1)].Itemid[(idPosItem-1)].provinces
                      }
                    $.ajax({
                        type: 'post',
                        data: data,
                        url: host+'/management/LocationManagement/getDistricts',
                        success: function (response) {
                            $("#item_ProgramTour_"+idPosProgramTour+"_"+idPosItem+"_districts").prop("disabled", false);
                            $("#item_ProgramTour_"+idPosProgramTour+"_"+idPosItem+"_districts").empty();
                            $("#item_ProgramTour_"+idPosProgramTour+"_"+idPosItem+"_districts").append(response);
                            $("#item_ProgramTour_"+idPosProgramTour+"_"+idPosItem+"_districts").val(array_dates_ProgramTour[(idPosProgramTour-1)].Itemid[(idPosItem-1)].districts);
                            $("#item_ProgramTour_"+idPosProgramTour+"_"+idPosItem+"_districts").trigger("chosen:updated");
                            
                            $.ajax({
                                type: 'post',
                                url: host_selected+'/ProgramTour/getNameCategory',
                                success: function (response) {
                                    $("#item_ProgramTour_"+idPosProgramTour+"_"+idPosItem+"_category").prop("disabled", false);
                                    $("#item_ProgramTour_"+idPosProgramTour+"_"+idPosItem+"_category").empty();
                                    $("#item_ProgramTour_"+idPosProgramTour+"_"+idPosItem+"_category").append(response);
                                    $("#item_ProgramTour_"+idPosProgramTour+"_"+idPosItem+"_category").val(array_dates_ProgramTour[(idPosProgramTour-1)].Itemid[(idPosItem-1)].category);
                                    $("#item_ProgramTour_"+idPosProgramTour+"_"+idPosItem+"_category").trigger("chosen:updated");
                                    var data = {
                                        getOption: array_dates_ProgramTour[(idPosProgramTour-1)].Itemid[(idPosItem-1)].category
                                    }
                                    $.ajax({
                                        type: 'post',
                                        data: data,
                                        url: host_selected+'/ProgramTour/getNameCategorySubCategory',
                                        success: function (response) {
                                            $("#item_ProgramTour_"+idPosProgramTour+"_"+idPosItem+"_type").prop("disabled", false);
                                            $("#item_ProgramTour_"+idPosProgramTour+"_"+idPosItem+"_type").empty();
                                            $("#item_ProgramTour_"+idPosProgramTour+"_"+idPosItem+"_type").append(response);
                                            $("#item_ProgramTour_"+idPosProgramTour+"_"+idPosItem+"_type").val(array_dates_ProgramTour[(idPosProgramTour-1)].Itemid[(idPosItem-1)].type);
                                            $("#item_ProgramTour_"+idPosProgramTour+"_"+idPosItem+"_type").trigger("chosen:updated");
                                            var data = {
                                                subcategory: array_dates_ProgramTour[(idPosProgramTour-1)].Itemid[(idPosItem-1)].type,
                                                category: array_dates_ProgramTour[(idPosProgramTour-1)].Itemid[(idPosItem-1)].category,
                                                districts:array_dates_ProgramTour[(idPosProgramTour-1)].Itemid[(idPosItem-1)].districts
                                            }
                                            $.ajax({
                                                type: 'post',
                                                data: data,
                                                url: host_selected+'/ProgramTour/getNamePlace',
                                                success: function (response) {
                                                    $("#item_ProgramTour_"+idPosProgramTour+"_"+idPosItem+"_place").prop("disabled", false);
                                                    $("#item_ProgramTour_"+idPosProgramTour+"_"+idPosItem+"_place").empty();
                                                    $("#item_ProgramTour_"+idPosProgramTour+"_"+idPosItem+"_place").append(response);
                                                    $("#item_ProgramTour_"+idPosProgramTour+"_"+idPosItem+"_place").val(array_dates_ProgramTour[(idPosProgramTour-1)].Itemid[(idPosItem-1)].place);
                                                    $("#item_ProgramTour_"+idPosProgramTour+"_"+idPosItem+"_place").trigger("chosen:updated");
                                                    
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}
function orderAllPoiBoxImageProduct(imageform) {
    $('.add_image_field-product').each(function (idx, element) {
        element.name = 'image_'+imageform+'_' + (idx+1);
        
    });
    $('.form-group-'+imageform+'-image').each(function (idx, element) {
        var image_name = 'image_'+imageform+'_'+(idx+1);

        var targetDivcarousel = element.getElementsByClassName("owl-carousel")[0];
        targetDivcarousel.className = 'owl-carousel owl-theme image_box_'+image_name;
        targetDivcarousel.id = 'image_box_'+image_name;
        
        $('#'+targetDivcarousel.id).each(function (idx, element) {
            var DivGallery = element.getElementsByClassName("product-item-img");
            var DivPoiProduct = element.getElementsByClassName("card-poi-product");
            var DivImageUploadedProduct = element.getElementsByClassName("image-uploaded-product");
            var DivBtnDel = element.getElementsByClassName("btn-del");
            var DivBtnEdit = element.getElementsByClassName("btn-edit");
            var DivImgFname = element.getElementsByClassName("img-fname");
            var DivTempImgFname = element.getElementsByClassName("temp-img-fname");

            for(var i=0; i< DivGallery.length;i++)
            {
                DivGallery[i].className = 'gallery-product-item product-item-img container-image-field-food form-group-'+image_name+'-image item';
                DivGallery[i].id = 'form_group_'+image_name+'_image_'+(i+1);

                DivPoiProduct[i].className = 'card-poi-product card-poi-'+image_name+'-box';
                DivPoiProduct[i].id = 'poi_'+image_name+'_'+(i+1);

                DivImageUploadedProduct[i].className = 'image-'+image_name+'-uploaded image-uploaded-product';
                DivImageUploadedProduct[i].id = 'output_image_'+image_name+'_'+(i+1);
                
                DivImgFname[i].className = 'img-'+image_name+'-fname img-fname';
                DivImgFname[i].id = 'img_'+image_name+'_fname_'+(i+1);
                DivImgFname[i].setAttribute('onchange', 'preview_image(event,\'output_image_'+image_name+'_'+(i+1)+'\')');
                
                DivTempImgFname[i].className = 'temp-img-'+image_name+'-fname temp-img-fname';
                DivTempImgFname[i].id = 'temp_img_'+image_name+'_fname_'+(i+1);
                
                DivBtnDel[i].name = 'del_'+image_name;
                DivBtnEdit[i].name = 'edit_'+image_name;

            }

        });
      
    });
    $('.form-group-Eng-'+imageform+'-image').each(function (idx, element) {
        var image_name = 'image_'+imageform+'_'+(idx+1);

        var targetDivcarousel = element.getElementsByClassName("owl-carousel")[0];
        targetDivcarousel.className = 'owl-carousel owl-theme image_box_Eng_'+image_name;
        targetDivcarousel.id = 'image_box_Eng_'+image_name;
        
        $('#'+targetDivcarousel.id).each(function (idx, element) {
            var DivGallery = element.getElementsByClassName("product-Eng-item-img");
            var DivPoiProduct = element.getElementsByClassName("card-poi-Eng-product");
            var DivImageUploadedProduct = element.getElementsByClassName("image-Eng-uploaded-product");


            for(var i=0; i< DivGallery.length;i++)
            {
                DivGallery[i].className = 'gallery-product-item product-Eng-item-img container-image-field-food form-group-Eng-'+image_name+'-image item';
                DivGallery[i].id = 'form_group_Eng_'+image_name+'_image_'+(i+1);

                DivPoiProduct[i].className = 'card-poi-Eng-product card-poi-'+image_name+'-box';
                DivPoiProduct[i].id = 'poi_Eng_'+image_name+'_'+(i+1);

                DivImageUploadedProduct[i].className = 'image-'+image_name+'-uploaded image-Eng-uploaded-product';
                DivImageUploadedProduct[i].id = 'output_image_Eng_'+image_name+'_'+(i+1);

            }
        });

    });
    $('.form-group-Laos-'+imageform+'-image').each(function (idx, element) {
        var image_name = 'image_'+imageform+'_'+(idx+1);

        var targetDivcarousel = element.getElementsByClassName("owl-carousel")[0];
        targetDivcarousel.className = 'owl-carousel owl-theme image_box_Laos_'+image_name;
        targetDivcarousel.id = 'image_box_Laos_'+image_name;
        
        $('#'+targetDivcarousel.id).each(function (idx, element) {
            var DivGallery = element.getElementsByClassName("product-Laos-item-img");
            var DivPoiProduct = element.getElementsByClassName("card-poi-Laos-product");
            var DivImageUploadedProduct = element.getElementsByClassName("image-Laos-uploaded-product");


            for(var i=0; i< DivGallery.length;i++)
            {
                DivGallery[i].className = 'gallery-product-item product-Laos-item-img container-image-field-food form-group-Laos-'+image_name+'-image item';
                DivGallery[i].id = 'form_group_Laos_'+image_name+'_image_'+(i+1);

                DivPoiProduct[i].className = 'card-poi-Laos-product card-poi-Laos-'+image_name+'-box';
                DivPoiProduct[i].id = 'poi_Laos_'+image_name+'_'+(i+1);

                DivImageUploadedProduct[i].className = 'image-Laos-'+image_name+'-uploaded image-Laos-uploaded-product';
                DivImageUploadedProduct[i].id = 'output_image_Laos_'+image_name+'_'+(i+1);

            }
        });
    });

    $('.form-group-Chinese-'+imageform+'-image').each(function (idx, element) {
        var image_name = 'image_'+imageform+'_'+(idx+1);

        var targetDivcarousel = element.getElementsByClassName("owl-carousel")[0];
        targetDivcarousel.className = 'owl-carousel owl-theme image_box_Chinese_'+image_name;
        targetDivcarousel.id = 'image_box_Chinese_'+image_name;
        
        $('#'+targetDivcarousel.id).each(function (idx, element) {
            var DivGallery = element.getElementsByClassName("product-Chinese-item-img");
            var DivPoiProduct = element.getElementsByClassName("card-poi-Chinese-product");
            var DivImageUploadedProduct = element.getElementsByClassName("image-Chinese-uploaded-product");


            for(var i=0; i< DivGallery.length;i++)
            {
                DivGallery[i].className = 'gallery-product-item product-Chinese-item-img container-image-field-food form-group-Chinese-'+image_name+'-image item';
                DivGallery[i].id = 'form_group_Chinese_'+image_name+'_image_'+(i+1);

                DivPoiProduct[i].className = 'card-poi-Chinese-product card-poi-Chinese-'+image_name+'-box';
                DivPoiProduct[i].id = 'poi_Chinese_'+image_name+'_'+(i+1);

                DivImageUploadedProduct[i].className = 'image-Chinese-'+image_name+'-uploaded image-Chinese-uploaded-product';
                DivImageUploadedProduct[i].id = 'output_image_Chinese_'+image_name+'_'+(i+1);

            }
        });
    });

    UpdateOwlCarousel()
}
function orderAllPoiBoxReservations(page) {
    $('.form-group-'+page+'-image').each(function (idx, element) {
        element.id = "form_group_"+page+"_image_" + (idx+1); 
    });
    $('.card-poi-'+page+'-box').each(function (idx, element) {
        element.id = "poi_"+page+"_" + (idx+1);
    });
    $('.image-'+page+'-uploaded').each(function (idx, element) {
        element.id = "output_image_"+page+"_" + (idx+1);
    });
    if(page.includes("location_illustrations"))
    {
        
        $('.photo_text_location_illustrations').each(function (idx, element) {
            element.id = "photo_text_location_illustrations_" + (idx+1);
        }); 
        $('.photo_text_location_illustrations_English').each(function (idx, element) {
            element.id = "photo_text_location_illustrations_" + (idx+1)+"_English";
        }); 
        $('.photo_text_location_illustrations_Laos').each(function (idx, element) {
            element.id = "photo_text_location_illustrations_" + (idx+1)+"_Laos";
        }); 
        $('.photo_text_location_illustrations_Chinese').each(function (idx, element) {
            element.id = "photo_text_location_illustrations_" + (idx+1)+"_Chinese";
        });

        $('.temp_cover_img_location_illustrations_fname').each(function (idx, element) {
            element.id = "temp_cover_img_location_illustrations_fname_" + (idx+1);
        }); 
        $('.temp_cover_img_location_illustrations_fname_English').each(function (idx, element) {
            element.id = "temp_cover_img_location_illustrations_fname_" + (idx+1)+"_English";
        }); 
        $('.temp_cover_img_location_illustrations_fname_Laos').each(function (idx, element) {
            element.id = "temp_cover_img_location_illustrations_fname_" + (idx+1) +"_Laos";
        }); 
        $('.temp_cover_img_location_illustrations_fname_Chinese').each(function (idx, element) {
            element.id = "temp_cover_img_location_illustrations_fname_" + (idx+1)+"_Chinese";
        }); 
        
        $('.form-group-'+page+'-image_English').each(function (idx, element) {
            element.id = "form_group_"+page+"_image_" + (idx+1)+"_English"; 
        });$('.form-group-'+page+'-image_Laos').each(function (idx, element) {
            element.id = "form_group_"+page+"_image_" + (idx+1)+"_Laos"; 
        });$('.form-group-'+page+'-image_Chinese').each(function (idx, element) {
            element.id = "form_group_"+page+"_image_" + (idx+1)+"_Chinese"; 
        });
        
        $('.card-poi-'+page+'-box_English').each(function (idx, element) {
            element.id = "poi_"+page+"_" + (idx+1)+"_English";
        });
        $('.card-poi-'+page+'-box_Laos').each(function (idx, element) {
            element.id = "poi_"+page+"_" + (idx+1)+"_Laos";
        });
        $('.card-poi-'+page+'-box_Chinese').each(function (idx, element) {
            element.id = "poi_"+page+"_" + (idx+1)+"_Chinese";
        });
        
        $('.image-'+page+'-uploaded_English').each(function (idx, element) {
            element.id = "output_image_"+page+"_" + (idx+1)+"_English";
        });
        $('.image-'+page+'-uploaded_Laos').each(function (idx, element) {
            element.id = "output_image_"+page+"_" + (idx+1)+"_Laos";
        });
        $('.image-'+page+'-uploaded_Chinese').each(function (idx, element) {
            element.id = "output_image_"+page+"_" + (idx+1)+"_Chinese";
        });
    }
    
    $('.img-'+page+'-fname').each(function (idx, element) {
        element.id = "img_"+page+"_fname_" + (idx+1);
        element.setAttribute('onchange',"preview_image(event,\'output_image_"+page+"_"+(idx+1)+"\')");
    });
    $('.temp_cover_img_'+page+'_fname').each(function (idx, element) {
        element.id = "temp_cover_img_"+page+"_fname_" + (idx+1);
       
    });
}
function orderAllPoiBox(imageform) {
    $('.form-group-'+imageform+'-image').each(function (idx, element) {
        element.id = "form_group_" + imageform + "_image_" + (idx+1); 
    });
    $('.form-group-Eng-'+imageform+'-image').each(function (idx, element) {
        element.id = "form_group_Eng_" + imageform + "_image_" + (idx+1);
    });
    $('.form-group-Laos-'+imageform+'-image').each(function (idx, element) {
        element.id = "form_group_Laos_" + imageform + "_image_" + (idx+1);
    });
    $('.form-group-Chinese-'+imageform+'-image').each(function (idx, element) {
        element.id = "form_group_Chinese_" + imageform + "_image_" + (idx+1);
    });
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $('.card-poi-'+imageform+'-box').each(function (idx, element) {
        element.id = "poi_" + imageform + "_" + (idx+1);
    });
    $('.card-poi-Eng-'+imageform+'-box').each(function (idx, element) {
        element.id = "poi_Eng_" + imageform + "_" + (idx+1);
    });
    $('.card-poi-Laos-'+imageform+'-box').each(function (idx, element) {
        element.id = "poi_Laos_" + imageform + "_" + (idx+1);
    });
    $('.card-poi-Chinese-'+imageform+'-box').each(function (idx, element) {
        element.id = "poi_Chinese_" + imageform + "_" + (idx+1);
    });
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $('.image-'+imageform+'-uploaded').each(function (idx, element) {
        element.id = "output_image_" + imageform + "_" + (idx+1);
    });
    $('.image-Eng-'+imageform+'-uploaded').each(function (idx, element) {
        element.id = "output_image_Eng_" + imageform + "_" + (idx+1);
    });
    $('.image-Laos-'+imageform+'-uploaded').each(function (idx, element) {
        element.id = "output_image_Laos_" + imageform + "_" + (idx+1);
    });
    $('.image-Chinese-'+imageform+'-uploaded').each(function (idx, element) {
        element.id = "output_image_Chinese_" + imageform + "_" + (idx+1);
    });
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $('.photo_textThai_'+imageform).each(function (idx, element) {
        element.id = "photo_textThai_" + imageform + "_" + (idx+1);
    });
    $('.photo_textEnglish_'+imageform).each(function (idx, element) {
        element.id = "photo_textEnglish_" + imageform + "_" + (idx+1);
    });
    $('.photo_textLaos_'+imageform).each(function (idx, element) {
        element.id = "photo_textLaos_" + imageform + "_" + (idx+1);
    });
    $('.photo_textChinese_'+imageform).each(function (idx, element) {
        element.id = "photo_textChinese_" + imageform + "_" + (idx+1);
    });
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $('.temp-img-'+imageform+'-fname').each(function (idx, element) {
        element.id = "temp_img_" + imageform + "_fname_" + (idx+1);
    });
    $('.img-'+imageform+'-fname').each(function (idx, element) {
        element.id = "img_"+imageform+"_fname_" + (idx+1);
        element.setAttribute('onchange',"preview_image(event,\'output_image_"+imageform+"_"+(idx+1)+"\')");
    });
    
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $('.name-'+imageform).each(function (idx, element) {
        element.id = "name_" + imageform + "_" + (idx+1);
    });
    $('.name-wng-'+imageform).each(function (idx, element) {
        element.id = "name_Eng_" + imageform + "_" + (idx+1);
    });
    $('.name-Laos-'+imageform).each(function (idx, element) {
        element.id = "name_Laos_" + imageform + "_" + (idx+1);
    });
    $('.name-Chinese-'+imageform).each(function (idx, element) {
        element.id = "name_Chinese_" + imageform + "_" + (idx+1);
    });
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $('.price-'+imageform).each(function (idx, element) {
        element.id = "price_" + imageform + "_" + (idx+1);
    });
    $('.price-Eng-'+imageform).each(function (idx, element) {
        element.id = "price_Eng_" + imageform + "_" + (idx+1);
    });
    $('.price-Laos-'+imageform).each(function (idx, element) {
        element.id = "price_Laos_" + imageform + "_" + (idx+1);
    });
    $('.price-Chinese-'+imageform).each(function (idx, element) {
        element.id = "price_Chinese_" + imageform + "_" + (idx+1);
    });
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $('.details-'+imageform).each(function (idx, element) {
        element.id = "details_" + imageform + "_" + (idx+1);
    });
    $('.details-Eng-'+imageform).each(function (idx, element) {
        element.id = "details_Eng_" + imageform + "_" + (idx+1);
    });
    $('.details-Laos-'+imageform).each(function (idx, element) {
        element.id = "details_Laos_" + imageform + "_" + (idx+1);
    });
    $('.details-Chinese-'+imageform).each(function (idx, element) {
        element.id = "details_Chinese_" + imageform + "_" + (idx+1);
    });
    $('.temp_id_'+imageform).each(function (idx, element) {
        element.id = "temp_id_" + imageform + "_" + (idx+1);
    });
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $('.breakfast-'+imageform).each(function (idx, element) {
        var radio_breakfast = 'breakfast_hotelroom_'+ (idx+1)
        var radio = element.getElementsByClassName('radio-breakfast-hotelroom');
        for(var i=0; i < radio.length;i++)
        {
            radio[i].name = radio_breakfast;
        }
    });
}
function FormControlTextareaChange (element)
{
    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
        element.addEventListener(event, function() {
            this.innerHTML = this.value;
        });
    });
}
function PriceProductChange (element) {
    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
        element.addEventListener(event, function() {
            var ElementEng = [this.id.slice(0, 5), "_Eng", this.id.slice(5)].join('');
            var ElementLao = [this.id.slice(0, 5), "_Laos", this.id.slice(5)].join('');
            var ElementChinese = [this.id.slice(0, 5), "_Chinese", this.id.slice(5)].join('');
            document.getElementById(ElementEng).innerHTML = this.value;
            document.getElementById(ElementLao).innerHTML = this.value;
            document.getElementById(ElementChinese).innerHTML = this.value;
        });
    });
    
}
function setInputFilter2(textbox, inputFilter) {
    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
      textbox.addEventListener(event, function() {
          
        if (inputFilter(this.value)) {
          this.oldValue = this.value;
          this.oldSelectionStart = this.selectionStart;
          this.oldSelectionEnd = this.selectionEnd;
        } else if (this.hasOwnProperty("oldValue")) {
          this.value = this.oldValue;
          this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
        } else {
          this.value = "";
        }

        var newnumber = new Number(this.value+'').toFixed(parseInt(0));
	    this.value =  parseFloat(newnumber); 
      });
    });
}
function setInputFilter(textbox, inputFilter) {
    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
      textbox.addEventListener(event, function() {
        if (inputFilter(this.value)) {
          this.oldValue = this.value;
          this.oldSelectionStart = this.selectionStart;
          this.oldSelectionEnd = this.selectionEnd;
        } else if (this.hasOwnProperty("oldValue")) {
          this.value = this.oldValue;
          this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
        }
      });
    });
}
function setInputLatLng($input)
{
    $input.onkeyup = function(){
        // When user select text in the document, also abort.
        var selection = window.getSelection().toString();
        if ( selection !== '' ) {
            return;
        }
        var $this = $(this);
        var input = $this.val();
        var split = 1;
        var chunk = [];

        for (var i = 0, len = input.length; i < len; i += split) {
            
            if(input.charAt(2) == ".")
            {
                split = ( i >= 2 && i <= 18 ) ? 18 : 2;
                chunk.push( input.substr( i, split ) );
                
            }  
            else
            {
                input = input.replace(/[\W\s\._\-]+/g, '');
                split = ( i >= 3 && i <= 18 ) ? 18 : 3;
                chunk.push( input.substr( i, split ) );
                
            }
        }
        $this.val(function() {
                if(input.charAt(2) != ".")
                    return chunk.join(".");
                else
                    return $this.val(); 
        });
        if(document.getElementById("items_latitude") || document.getElementById("items_longitude"))
        {
            if(document.getElementById("items_latitude").value != "" && document.getElementById("items_longitude").value != "")
            {
                addOnKeyUpMarker(document.getElementById("items_latitude").value,document.getElementById("items_longitude").value)
            }
        } 
        if(document.getElementById("business_latitude") || document.getElementById("business_longitude"))
        {
            if(document.getElementById("business_latitude").value != "" && document.getElementById("business_longitude").value != "")
            {
                addOnKeyUpMarker(document.getElementById("business_latitude").value,document.getElementById("business_longitude").value)
            }
        }
    }
}
function resetNotification()
{
    $(".notifi").map(function() {return $(this).attr('id');}).get().forEach((element, index) => {
        $("#"+element).css('display', 'none', 'important');
    });     
}
async function srcToFile(src, fileName, mimeType){
    return (fetch(src)
        .then(function(res){return res.arrayBuffer();})
        .then(function(buf){return new File([buf], fileName, {type:mimeType});})
    );
}
function dataURLtoFile(dataurl, filename) {
 
    var arr = dataurl,
        //mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr), 
        n = bstr.length, 
        u8arr = new Uint8Array(n);
        
    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    
    var arrs = u8arr.subarray(0, 4);
    var header = "";
    for(var i = 0; i < arrs.length; i++) {
        header += arrs[i].toString(16);
    }
    switch (header) {
        case "89504e47":
            typeImage = "image/png";
            types = ".png";
            break;
        case "47494638":
            typeImage = "image/gif";
            types = ".gif";
            break;
        case "ffd8ffe0":
        case "ffd8ffe1":
        case "ffd8ffe2":
        case "ffd8ffe3":
        case "ffd8ffe8":
            typeImage = "image/jpeg";
            types = ".jpg";
            break;
        default:
            typeImage = "unknown"; // Or you can use the blob.type as fallback
            types =""
            break;
    }
    return new File([u8arr], filename+""+types, {type:typeImage});
}
function handleFileSelect(file,tempInput) {
    //var f = evt.target.files[0]; // FileList object
    var reader = new FileReader();
    // Closure to capture the file information.
    reader.onload = (function(theFile) {
      return function(e) {
        var binaryData = e.target.result;
        //Converting Binary Data to base 64
        tempInput.value = window.btoa(binaryData);
      };
    })(file);
    // Read in the image file as a data URL.
    reader.readAsBinaryString(file);
}
function SetSelectLocation()
{
    $.ajax({
        type: 'post',
        url: host+'/management/LocationManagement/getCountry',
        success: function (response) {
            $("#country").empty();
            $("#country").append(response);
            $("#country").val(country_id);
            $('#country').trigger("chosen:updated");
            var data = {
                getOption: country_id
            }
            $.ajax({
                type: 'post',
                data: data,
                url: host+'/management/LocationManagement/getProvinces',
                success: function (response) {
                    $("#provinces").prop("disabled", false);
                    $("#provinces").empty();
                    $("#provinces").append(response);
                    $("#provinces").val(provinces_id);
                    $('#provinces').trigger("chosen:updated");
                    var data = {
                        getOption: provinces_id
                      }
                    $.ajax({
                        type: 'post',
                        data: data,
                        url: host+'/management/LocationManagement/getDistricts',
                        success: function (response) {
                            $("#districts").prop("disabled", false);
                            $("#districts").empty();
                            $("#districts").append(response);
                            $("#districts").val(districts_id);
                            $('#districts').trigger("chosen:updated");
                            var data = {
                                getOption: districts_id
                            }
                            var element = document.getElementById("subdistricts");
                            if(element)
                            {
                                $.ajax({
                                    type: 'post',
                                    data: data,
                                    url: host+'/management/LocationManagement/getSubdistricts',
                                    success: function (response) {
                                        $("#subdistricts").prop("disabled", false);
                                        $("#subdistricts").empty();
                                        $("#subdistricts").append(response);
                                        $("#subdistricts").val(subdistricts_id);
                                        $('#subdistricts').trigger("chosen:updated");
                                    }
                                });
                            }
                            
                        }
                    });
                }
            });
        }
    });
}
function SetNavLinkTabMainActive()
{
    $('.nav-link').each(function (idx, element) {
        if(element.id.includes("tabTH"))
        {
            element.click();
        }
        else
        {
            element.classList.remove("active");
            element.classList.remove("show");
        }
    });
}
function ajaxSaveItemBusiness(formData,type,typepage)
{
    $.ajax({
        type: "POST",
        url: host_entrepreneur+'/business/'+typepage+'/'+type,
        data: formData,
        contentType: false,
        processData:false,
        success: function (response) {

            if(response.state)
            {
                var element = document.getElementById("notifi_error_save");
                element.style.color = "green";
                $("#notifi_error_save").css('display', 'inline', 'important');
                element.innerHTML = response.msg;
                window.location.href ='/dasta_thailand/entrepreneur/business/'+typepage;
            }
            else
            {
                $("#save").prop("disabled", false);
                if(response.error == 'validation')
                { 
                    msg = response.msg.replace(/<p>The /g, "");  
                    msg = msg.replace(/ field is required.<\/p>/g,"");
                    msg =msg.split('\n');
                    msg.forEach((element, index) => {
                        var ids = element.split('|');
                        if(ids[0] == "required")
                        {
                        document.getElementById("status").style.display = "none";
                        document.getElementById("preloader").style.display = "none";
                        SetNavLinkTabMainActive();
                        var element = document.getElementById("notifi_"+ids[2]);
                        element.style.color = "red";
                        $("#notifi_"+ids[2]).css('display', 'inline', 'important');
                        $('#notifi_'+ids[2]).text(ids[3]);
                        document.getElementById(ids[1]).focus();
                        }
                    });
                }
                else
                {
                    document.getElementById("status").style.display = "none";
                    document.getElementById("preloader").style.display = "none";
                    var element = document.getElementById("notifi_error_save");
                    element.style.color = "red";
                    $("#notifi_error_save").css('display', 'inline', 'important');
                    element.innerHTML = response.msg;
                }
            }
        },
        beforeSend:function(){
            document.getElementById("status").style.display = "";
            document.getElementById("preloader").style.display = "";
        }
    });
}
function ajaxSaveItem(formData,type,typepage)
{
    $.ajax({
        type: "POST",
        url: host+'/'+typepage+'/'+MenuName+'/'+type,
        data: formData,
        contentType: false,
        processData:false,
        success: function (response) {
           
            if(response.state)
            {
                var element = document.getElementById("notifi_error_save");
                element.style.color = "green";
                $("#notifi_error_save").css('display', 'inline', 'important');
                element.innerHTML = response.msg;
                window.location.href ='/dasta_thailand/html/'+typepage+'/'+MenuName;
            }
            else
            {
                $("#save").prop("disabled", false);
                if(response.error == 'validation')
                { 
                    msg = response.msg.replace(/<p>The /g, "");  
                    msg = msg.replace(/ field is required.<\/p>/g,"");
                    msg =msg.split('\n');
                    msg.forEach((element, index) => {
                            var ids = element.split('|');
                            if(ids[0] == "required")
                            {
                                document.getElementById("status").style.display = "none";
                                document.getElementById("preloader").style.display = "none";
                                SetNavLinkTabMainActive();
                                var element = document.getElementById("notifi_"+ids[2]);
                                element.style.color = "red";
                                $("#notifi_"+ids[2]).css('display', 'inline', 'important');
                                $('#notifi_'+ids[2]).text(ids[3]);
                                document.getElementById(ids[1]).focus();
                                
                            }
                    });
                }
                else
                {
                    document.getElementById("status").style.display = "none";
                    document.getElementById("preloader").style.display = "none";
                    var element = document.getElementById("notifi_error_save");
                    element.style.color = "red";
                    $("#notifi_error_save").css('display', 'inline', 'important');
                    element.innerHTML = response.msg;
                }
            }
        },
        beforeSend:function(){
            document.getElementById("status").style.display = "";
            document.getElementById("preloader").style.display = "";
        }
    });
}

function addBoxFieldTimepicker(isnew = true)
{
    box_field_timepicker++;
    var new_box_timepicker = "";
    new_box_timepicker +='<div class="form-group  row form-group-timepicker" id="form_group_timepicker_'+box_field_timepicker+'">';
    new_box_timepicker +='<label class="col-sm-2 col-form-label">ช่วงเวลา</label>';
    new_box_timepicker +='<div class="col-sm-4">';
    new_box_timepicker +='<input class="form-control timepicker items_timeOpen" type="text" id="items_timeOpen_'+box_field_timepicker+'">';
    new_box_timepicker +='<span class="note notifi notifi_items_timeOpen" id="notifi_items_timeOpen_'+box_field_timepicker+'"></span>';
    new_box_timepicker +='</div>';
    new_box_timepicker +='<div class="col-sm-4">';
    new_box_timepicker +='<input class="form-control timepicker items_timeClose" type="text" id="items_timeClose_'+box_field_timepicker+'">';
    new_box_timepicker +='<span class="note notifi notifi_items_timeClose" id="notifi_items_timeClose_'+box_field_timepicker+'"></span>';
    new_box_timepicker +='</div>';
    new_box_timepicker +='<div class="col-sm">';
    new_box_timepicker +='<button class="btn btn-del-box-timepicker btn-danger btn-circle btn-sm" name="del_boxtimepicker_'+box_field_timepicker+'"><span aria-hidden="true" style="color: white;">X</span></button>';
    new_box_timepicker +='</div>';
    new_box_timepicker +='</div>';
    $("#box_timepicker").append(new_box_timepicker);

    if(isnew)
    {
        $('.timepicker').pickatime({
            format: 'HH:i',
            formatLabel: 'HH:i',
              formatSubmit: 'HH:i',
            interval: 30
          })
    }
    
}
function adddocuments(isnew = true)
{
    var new_box_documents ="";
    new_box_documents +='<div class="form-group  row form-group-documents" id="form_group_documents_'+box_field_documents+'">';
    new_box_documents +='<label class="col-sm-2 col-form-label name_label">'+box_field_documents+'</label>';
    new_box_documents +='<div class="col-sm-8">';
    new_box_documents +='<input type="text" placeholder="" class="form-control bookingConditioncol" id="bookingConditioncol_detailThai_id_14_'+box_field_documents+'">';
    new_box_documents +='<span class="note notifi notifi_items_documents" id="notifi_bookingConditioncol_detailThai_id_14_'+box_field_documents+'"></span>';
    new_box_documents +='</div>';
    new_box_documents +='<div class="col-sm-2">';
    new_box_documents +='<button class="btn btn-del-box-documents btn-danger btn-circle btn-sm" name="del_boxdocuments_'+box_field_documents+'"><span aria-hidden="true" style="color: white;">X</span></button>';
    new_box_documents +='</div>';
    new_box_documents +='</div>';
    $("#box_documents").append(new_box_documents);

    var tab_name = ['English','Laos','Chinese'];
    for(i=0;i<3;i++)
    {
        new_box_documents ="";
        new_box_documents +='<div class="form-group  row form-group-documents-'+tab_name[i]+'" id="form_group_documents_'+tab_name[i]+'_'+box_field_documents+'">';
        new_box_documents +='<label class="col-sm-2 col-form-label name_label_'+tab_name[i]+'">'+box_field_documents+'</label>';
        new_box_documents +='<div class="col-sm-8">';
        new_box_documents +='<input type="text" placeholder="" class="form-control bookingConditioncol_'+tab_name[i]+'" id="bookingConditioncol_detail'+tab_name[i]+'_id_14_'+box_field_documents+'">';
        new_box_documents +='</div>';
        new_box_documents +='</div>';
        $("#box_documents_"+tab_name[i]).append(new_box_documents);
    }
}
function addImageFieldReservations(fieldName,isnew = true)
{
    var new_image_box_Reservations = "";
    var page ="";
    if(fieldName.includes("PackageTours"))
        page ="package_tours";
    else if(fieldName.includes("EventTicket"))
        page ="event_ticket";
    else if(fieldName.includes("LocationIllustrations"))
        page ="location_illustrations";

    eval("box_cover_image_"+page+" = box_cover_image_"+page+" + 1");

    new_image_box_Reservations += '<div class="item gallery-'+page+' container-image-field form-group-'+page+'-image" id="form_group_'+page+'_image_'+eval("box_cover_image_"+page)+'">'
    new_image_box_Reservations += '<div class="card-poi-'+page+'-box" id="poi_'+page+'_'+eval("box_cover_image_"+page)+'">';
    new_image_box_Reservations += '<img width="200" height="200" class="image-'+page+'-uploaded" src="../../../assets/img/image_placeholder.jpg" alt="" id="output_image_'+page+'_'+eval("box_cover_image_"+page)+'">'
    new_image_box_Reservations += '<button class="btn btn-del btn-danger btn-circle btn-sm" name="del_box_'+page+'"><span aria-hidden="true" style="color: white;">X</span></button>';
    new_image_box_Reservations += '<button class="btn btn-edit btn-warning btn-circle btn-sm" name="edit_box_'+page+'"><span aria-hidden="true" style="color: white;"><i class="glyphicon glyphicon-pencil"></i></span></button>';
    new_image_box_Reservations += '<input type="file" class="img-'+page+'-fname" style="display:none" accept="image/jpg,image/png,image/jpeg" id="img_'+page+'_fname_'+eval("box_cover_image_"+page)+'" onchange="preview_image(event,\'output_image_'+page+'_'+eval("box_cover_image_"+page)+'\')"> ';
    
    if(fieldName.includes("LocationIllustrations"))
    {
        new_image_box_Reservations +='<textarea class="form-control photo_text_location_illustrations" placeholder="* เขียนบรรยายใต้ภาพของคุณ" id="photo_text_location_illustrations_'+eval("box_cover_image_"+page)+'"></textarea>';
        new_image_box_Reservations += '<input type="hidden" class="temp_cover_img_'+page+'_fname" id="temp_cover_img_'+page+'_fname_'+eval("box_cover_image_"+page)+'">';
    }
    else
    {
        new_image_box_Reservations += '<input type="hidden" class="temp_cover_img_'+page+'_fname" id="temp_cover_img_'+page+'_fname_'+eval("box_cover_image_"+page)+'">';
    }
    new_image_box_Reservations += '</div>';
    new_image_box_Reservations += '</div>';

    if(fieldName.includes("LocationIllustrations"))
    {
        var languages =["English","Laos","Chinese"]
        for(var i=0;i<3;i++)
        {
            var new_image_box_Reservations_multi ="";
            new_image_box_Reservations_multi += '<div class="item gallery-'+page+' container-image-field form-group-'+page+'-image_'+languages[i]+'" id="form_group_'+page+'_image_'+eval("box_cover_image_"+page)+'_'+languages[i]+'">'
            new_image_box_Reservations_multi += '<div class="card-poi-'+page+'-box_'+languages[i]+'" id="poi_'+page+'_'+eval("box_cover_image_"+page)+'_'+languages[i]+'">';
            new_image_box_Reservations_multi += '<img width="200" height="200" class="image-'+page+'-uploaded_'+languages[i]+'" src="../../../assets/img/image_placeholder.jpg" alt="" id="output_image_'+page+'_'+eval("box_cover_image_"+page)+'_'+languages[i]+'">'
            new_image_box_Reservations_multi += '<input type="hidden" class="temp_cover_img_'+page+'_fname_'+languages[i]+'" id="temp_cover_img_'+page+'_fname_'+eval("box_cover_image_"+page)+'_'+languages[i]+'">';
            new_image_box_Reservations_multi += '<textarea class="form-control photo_text_location_illustrations_'+languages[i]+'" placeholder="* เขียนบรรยายใต้ภาพของคุณ" id="photo_text_location_illustrations_'+eval("box_cover_image_"+page)+'_'+languages[i]+'"></textarea>';
            new_image_box_Reservations_multi += '</div>';
            new_image_box_Reservations_multi += '</div>';

            $('#image_box_'+page+'_'+languages[i]).trigger('add.owl.carousel', [new_image_box_Reservations_multi]).trigger('refresh.owl.carousel')
        }
    }

    $('#image_box_'+page).trigger('add.owl.carousel', [new_image_box_Reservations]).trigger('refresh.owl.carousel')

    if(isnew)
    {
        btnAddElementById = document.getElementById('img_'+page+'_fname_'+eval("box_cover_image_"+page))
        btnAddElementById.onclick = charge
        btnAddElementById.click();
    }
    
}
function addPackageToursField(fieldName,isnew = true)
{
    var num_box = 0;
    
    if(fieldName.includes("TravelSchedule"))
    {
        num_box = parseInt((document.getElementById("num_box_travel_schedule").value ==""?0:document.getElementById("num_box_travel_schedule").value))
        if(box_travel_schedule > num_box && box_travel_schedule != 0)
        {
            // delete box
            for(var i=box_travel_schedule;i > num_box;i--)
            {
                $("#form_group_"+fieldName+"_image_" + i).remove();
            }
            box_travel_schedule = num_box

        }
        else 
        {
            var numloop = 1;
            for(var i=box_travel_schedule;i < num_box;i++)
            {
                if(box_travel_schedule == 0)
                {
                    NewPackageToursBox((i+1),fieldName)
                    document.getElementById(fieldName+"_start_"+(i+1)).value =  moment().format('MM/DD/YYYY');
                    document.getElementById(fieldName+"_end_"+(i+1)).value =  moment().format('MM/DD/YYYY');
                }   
                else
                {
                    NewPackageToursBox(box_travel_schedule+numloop,fieldName)
                    document.getElementById(fieldName+"_start_"+(box_travel_schedule+numloop)).value =  moment().format('MM/DD/YYYY');
                    document.getElementById(fieldName+"_end_"+(box_travel_schedule+numloop)).value =  moment().format('MM/DD/YYYY');
                    numloop++;
                }  
            }
            if(isnew)
            {
                $('#data_5 .input-daterange').datepicker({
                    keyboardNavigation: false,
                    forceParse: false,
                    autoclose: true,                       
                });
            }
           
            box_travel_schedule = num_box
        }
        
    }
    else if(fieldName.includes("TravelDetails"))
    {
        num_box = parseInt((document.getElementById("num_box_travel_details").value ==""?0:document.getElementById("num_box_travel_details").value))
        if(box_travel_details > num_box && box_travel_details != 0)
        {
            // delete box
            for(var i=box_travel_details;i > num_box;i--)
            {
                $("#form_group_"+fieldName+"_image_" + i).remove();
                $("#form_group_English_"+fieldName+"_image_" + i).remove();
                $("#form_group_Laos_"+fieldName+"_image_" + i).remove();
                $("#form_group_Chinese_"+fieldName+"_image_" + i).remove();
            }
            box_travel_details = num_box
        }
        else 
        {
            var numloop = 1;
            for(var i=box_travel_details;i < num_box;i++)
            {
                if(box_travel_details == 0)
                {
                    NewPackageToursBox((i+1),fieldName)
                }   
                else
                {
                    NewPackageToursBox(box_travel_details+numloop,fieldName)
                    numloop++;
                }  
            }
            $('.summernote').summernote({
                placeholder: 'Hello Bootstrap 4',
                tabsize: 2,
                height: 100
                });
                $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
            box_travel_details = num_box
        }
    }
}
function checktypefile()
{
    var status= 0;
    var typefile = ["jpg",'png']
    $("input[type=file]").map(function() {return $(this).attr('id');}).get().forEach((element, index) => {
        var fullPath = document.getElementById(element).value;
        if (fullPath) 
        {
            var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
            var filename = fullPath.substring(startIndex);
            if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0)
            {
                filename = filename.substring(1);
                if(!typefile.includes(filename.toLowerCase().split('.').pop()))
                    status += 1; 
            }
        }             
    });
    return status
}
function resetData(){
    setTimeout( function () {
        table.ajax.reload();
        var rows = table.rows(0).data();
        var user_content =  document.getElementById("user_content");
        var items_content =  document.getElementById("items_content");

        if(rows.length !=0)
        {
            if(user_content != null)
            {
                user_content.style.display = "block"
                document.getElementById("header_count_list").textContent= table.data().length +" รายการ"
                getUserData(rows[0][0])
            }
            else if(items_content != null)
            {
                items_content.style.display = "block"

                if(MenuName.includes("ApprovalEventTicket") ||
                    MenuName.includes("ApprovalCarRent") ||
                    MenuName.includes("ApprovalPackageTours")
                )
                {
                    getReservationsData(rows[0][0])
                }
                else if(MenuName.includes("ApprovalProgramTour"))
                    getProgramTourData(rows[0][0])
                else
                    getItemsData(rows[0][0])

                document.getElementById("header_count_list").textContent= table.data().length +" รายการ"
            }
            else
            {
                document.getElementById("countData").textContent= table.data().length
            }
        }
        else
        {
            if(user_content != null)
            {
                user_content.style.display = "none"
                document.getElementById("header_count_list").textContent= table.data().length +" รายการ"
            }
            else if(items_content != null)
            {
                items_content.style.display = "none"
                document.getElementById("header_count_list").textContent= table.data().length +" รายการ"
            }
            else
            {
                document.getElementById("countData").textContent= table.data().length
            }
        }
    }, 500 );
}
function UpdateApprovalData(){
    $.ajax({
        type: "POST",
        url: host+'/management/ItemsEvent/GetApprovalData',
        success: function (response) {

            if(response.Items.All == 0)
            {
                document.getElementById("ApprovalAllWarning").textContent="";
                document.getElementById("ApprovalAllWarning").removeAttribute("class"); 
            }
            else
            {
                document.getElementById("ApprovalAllWarning").setAttribute("class", "label label-warning float-right");
                document.getElementById("ApprovalAllWarning").textContent= response.Items.All;
            }
            document.getElementById("ApprovalProgramTour").textContent= (response.Items.ProgramTour == 0?"":response.Items.ProgramTour);
            document.getElementById("ApprovalAttractions").textContent= (response.Items.Attractions == 0?"":response.Items.Attractions);
            document.getElementById("ApprovalRestaurant").textContent= (response.Items.Restaurant == 0?"":response.Items.Restaurant);
            document.getElementById("ApprovalShopping").textContent= (response.Items.Shopping == 0?"":response.Items.Shopping);
            document.getElementById("ApprovalHotel").textContent= (response.Items.Hotel == 0?"":response.Items.Hotel);
            document.getElementById("ApprovalPackageTours").textContent= (response.Items.PackageTours == 0?"":response.Items.PackageTours);
            document.getElementById("ApprovalEventTicket").textContent= (response.Items.EventTicket == 0?"":response.Items.EventTicket);
            document.getElementById("ApprovalCarRent").textContent= (response.Items.CarRent == 0?"":response.Items.CarRent);

            var state_all =0;
            var state_approved =0;
            var state_wait =0;
            var state_user_wait =0;
            var state_refuse =0;

            switch (TypeMenu) {
                case "Approval":
                    switch (MenuName) {
                        case "ApprovalProgramTour":
                            state_all = response.ItemState_ProgramTour.All;
                            state_approved = response.ItemState_ProgramTour.Approved;
                            state_wait = response.Items.ProgramTour;
                            state_refuse = response.ItemState_ProgramTour.Refuse;
                            
                            break; 
                        case "ApprovalAttractions":
                            state_all = response.ItemState_Attractions.All;
                            state_approved = response.ItemState_Attractions.Approved;
                            state_wait = response.Items.Attractions - response.UserState_Attractions.Wait;
                            state_refuse = response.ItemState_Attractions.Refuse;
                            state_user_wait = response.UserState_Attractions.Wait;
                           
                            break;
                        case "ApprovalRestaurant":
                            state_all = response.ItemState_Restaurant.All;
                            state_approved = response.ItemState_Restaurant.Approved;
                            state_wait = response.Items.Restaurant - response.UserState_Restaurant.Wait;
                            state_refuse = response.ItemState_Restaurant.Refuse;
                            state_user_wait = response.UserState_Restaurant.Wait;
                            break;
                        case "ApprovalShopping":
                            state_all = response.ItemState_Shopping.All;
                            state_approved = response.ItemState_Shopping.Approved;
                            state_wait = response.Items.Shopping - response.UserState_Shopping.Wait;
                            state_refuse = response.ItemState_Shopping.Refuse;
                            state_user_wait = response.UserState_Shopping.Wait;
                            break;
                        case "ApprovalHotel":
                            state_all = response.ItemState_Hotel.All;
                            state_approved = response.ItemState_Hotel.Approved;
                            state_wait = response.Items.Hotel - response.UserState_Hotel.Wait;
                            state_refuse = response.ItemState_Hotel.Refuse;
                            state_user_wait = response.UserState_Hotel.Wait;
                            break;
                        case "ApprovalPackageTours":
                            state_all = response.ItemState_PackageTours.All;
                            state_approved = response.ItemState_PackageTours.Approved;
                            state_wait = response.Items.PackageTours - response.UserState_PackageTours.Wait;
                            state_refuse = response.ItemState_PackageTours.Refuse;
                            state_user_wait = response.UserState_PackageTours.Wait;
                            break;
                        case "ApprovalEventTicket":
                            state_all = response.ItemState_EventTicket.All;
                            state_approved = response.ItemState_EventTicket.Approved;
                            state_wait = response.Items.EventTicket - response.UserState_EventTicket.Wait;
                            state_refuse = response.ItemState_EventTicket.Refuse;
                            state_user_wait = response.UserState_EventTicket.Wait;
                            break;
                        case "ApprovalCarRent":
                            state_all = response.ItemState_CarRent.All;
                            state_approved = response.ItemState_CarRent.Approved;
                            state_wait = response.Items.CarRent - response.UserState_CarRent.Wait;
                            state_refuse = response.ItemState_CarRent.Refuse;
                            state_user_wait = response.UserState_CarRent.Wait;
                            break;
                        default:
                            break;
                    }
                    break;
                case "EntrepreneurApproval":
                    switch (MenuName) {
                        case "EntrepreneurApprovalProgramTour":
                            
                            break; 
                        case "EntrepreneurApprovalAttractions":
                            state_all = response.UserState_Attractions.All;
                            state_approved = response.UserState_Attractions.Approved;
                            state_wait = response.UserState_Attractions.Wait;
                            state_refuse = response.UserState_Attractions.Refuse;
                            state_user_wait = response.UserState_Attractions.Wait;
                            break;
                        case "EntrepreneurApprovalRestaurant":
                            state_all = response.UserState_Restaurant.All;
                            state_approved = response.UserState_Restaurant.Approved;
                            state_wait = response.UserState_Restaurant.Wait;
                            state_refuse = response.UserState_Restaurant.Refuse;
                            state_user_wait = response.UserState_Restaurant.Wait;
                            break;
                        case "EntrepreneurApprovalShopping":
                            state_all = response.UserState_Shopping.All;
                            state_approved = response.UserState_Shopping.Approved;
                            state_wait = response.UserState_Shopping.Wait;
                            state_refuse = response.UserState_Shopping.Refuse;
                            state_user_wait = response.UserState_Shopping.Wait;
                            break;
                        case "EntrepreneurApprovalHotel":
                            state_all = response.UserState_Hotel.All;
                            state_approved = response.UserState_Hotel.Approved;
                            state_wait = response.UserState_Hotel.Wait;
                            state_refuse = response.UserState_Hotel.Refuse;
                            state_user_wait = response.UserState_Hotel.Wait;
                            break;
                        case "EntrepreneurApprovalPackageTours":
                            state_all = response.UserState_PackageTours.All;
                            state_approved = response.UserState_PackageTours.Approved;
                            state_wait = response.UserState_PackageTours.Wait;
                            state_refuse = response.UserState_PackageTours.Refuse;
                            state_user_wait = response.UserState_PackageTours.Wait;
                            break;
                        case "EntrepreneurApprovalEventTicket":
                            state_all = response.UserState_EventTicket.All;
                            state_approved = response.UserState_EventTicket.Approved;
                            state_wait = response.UserState_EventTicket.Wait;
                            state_refuse = response.UserState_EventTicket.Refuse;
                            state_user_wait = response.UserState_EventTicket.Wait;
                            break;
                        case "EntrepreneurApprovalCarRent":
                            state_all = response.UserState_CarRent.All;
                            state_approved = response.UserState_CarRent.Approved;
                            state_wait = response.UserState_CarRent.Wait;
                            state_refuse = response.UserState_CarRent.Refuse;
                            state_user_wait = response.UserState_CarRent.Wait;
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
           
            var getElementById_state_all = document.getElementById("state_all");
            if(getElementById_state_all)
            {
                document.getElementById("state_all").textContent= state_all;
                document.getElementById("state_approved").textContent= state_approved;
                document.getElementById("state_wait").textContent= state_wait;
                document.getElementById("state_refuse").textContent= state_refuse;
                var ApprovalItem = document.getElementById("ApprovalItem");
                var ApprovalUserState = document.getElementById("ApprovalUserState");
    
                if(ApprovalItem != null)
                {
                    if(state_wait == 0)
                    {
                       ApprovalItem.textContent= "";
                       ApprovalItem.removeAttribute("class"); 
                    }
                    else
                    {
                        ApprovalItem.setAttribute("class", "badge badge-warning");
                        ApprovalItem.textContent= state_wait+" รายการ";
                    }
                }
                if(ApprovalUserState != null)
                {
                    if(state_user_wait == 0)
                    {
                        ApprovalUserState.textContent= "";
                        ApprovalUserState.removeAttribute("class"); 
                    }
                    else
                    {
                        ApprovalUserState.setAttribute("class", "badge badge-warning");
                        ApprovalUserState.textContent= state_user_wait+" รายการ";
                    }
                }
            }
            
            
            
        }
    });
}
function menuItemClickListener(menu_item, parent)
{
    if(menu_item.attr('id').includes("LiEdit"))
    {
        if(MenuName.includes("CarRent") ||
            MenuName.includes("EventTicket") ||
            MenuName.includes("ProgramTour") ||
            MenuName.includes("PackageTours")  )
        {
            window.location.href ='/dasta_thailand/entrepreneur/EditUserData'
        }
        else
        {
            window.location.href ='/dasta_thailand/entrepreneur/business/'+MenuName+'/edit'+MenuName+'?item_id='+items_id;
        }   
    }
}