function getUserData(user_id)
{
    $.ajax({
        type: "POST",
        url: host+'/management/UserEvent/GetUserData',
        data: {user_id: user_id},
    
        success: function (response) {

            var box_btn_approve = document.getElementById("box_btn_approve");
            if(box_btn_approve !=null)
            {
                if(parseInt(response[0].user_isActive) == 1)
                    box_btn_approve.style.visibility = "hidden";
                else
                    box_btn_approve.style.visibility = "visible";
            }
            document.getElementById("crop_img").src = "../../assets/img/uploadfile/"+response[0].user_profile_pic_url;
            document.getElementById("business_nameThai").textContent= response[0].business_nameThai + "|" +response[0].business_nameEnglish + "|" +response[0].business_nameChinese + "|" +response[0].business_nameLaos
            document.getElementById("business_presentAddress").textContent= response[0].business_presentAddress
            document.getElementById("business_type_category_id").textContent= response[0].category_thai
            document.getElementById("business_phone").textContent= response[0].business_phone
            document.getElementById("business_www").textContent= response[0].business_www
            document.getElementById("business_facebook").textContent= response[0].business_facebook
            document.getElementById("user_email").textContent= response[0].user_email
            document.getElementById("user_firstName").textContent= response[0].user_firstName
            document.getElementById("user_lastName").textContent= response[0].user_lastName
            document.getElementById("user_phone").textContent= response[0].user_phone
            document.getElementById("user_account").textContent= response[0].user_account

            document.getElementById("btn_approve").value= response[0].user_id
            document.getElementById("btn_disapprove").value= response[0].user_id
        }
    });
    $('#ApprovalTable tbody').on( 'click', 'tr', function () {
        var rows = table.row( this ).data();
        getUserData(rows[0])
    });
}