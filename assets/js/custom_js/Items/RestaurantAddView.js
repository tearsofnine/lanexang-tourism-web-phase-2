$(document).ready(function(){
      
    $.ajax({
        type: 'post',
        url: host+'/Items/'+MenuName+'/getNameCategorySubCategory',
        success: function (response) {
          $("#namesubcategory").empty();
          $("#namesubcategory").append(response);
          $('#namesubcategory').trigger("chosen:updated");
        }
    });
    $.ajax({
        type: 'post',
        url: host+'/Items/'+MenuName+'/getNameDeliciousGuarantee',
        success: function (response) {
          $("#delicious").empty();
          $("#delicious").append(response);
          $('#delicious').trigger("chosen:updated");
        }
    });

    document.getElementById('image_crop_img').addEventListener('crop', (event) => {

        CropData = cropper.getData()
        CropBoxData = cropper.getCropBoxData()
        CanvasData = cropper.getCanvasData()
    });
    
    $(document).on('change', '#delicious', function (e) {
        if(array_delicious.length > $('#delicious').val().length)
        {
            var temp_array_delicious =[]
            $(this).find('option:selected').each(function(){
                if(array_delicious.includes($(this).val()))
                {
                    temp_array_delicious.push($(this).val());
                }
            });
            array_delicious = temp_array_delicious;
            $('.form-group-delicious-image').each(function (idx, element) {
                var formsplict = element.id.split('_')
                if(!array_delicious.includes(formsplict[4]))
                    $("#"+element.id).remove();
            });
        }
        else
        {
            $(this).find('option:selected').each(function(){
                if(!array_delicious.includes($(this).val()))
                {
                    array_delicious.push($(this).val());
                    var id = $(this).val();
                    NewImageBox(id,"delicious");
                    $.ajax({
                        type: 'post',
                        url: host+'/Items/'+MenuName+'/getDeliciousGuarantee',
                        data:{
                            deliciousGuarantee_id:id,
                            dataTable:'deliciousGuarantee_id'
                        },
                        success: function (response) {
                            document.getElementById("output_image_delicious_"+id).src = "/dasta_thailand/assets/img/"+response[0].deliciousGuarantee_paths
                            document.getElementById("name_delicious_"+id).innerHTML = response[0].deliciousGuarantee_textThai
                        }
                    });
                }    
            });
        }
        // console.log("array_delicious :"+ array_delicious)
        // console.log("$('#delicious').val().length :"+ $('#delicious').val())
    });
    

    $(document).on("click", "#save", function () {
        $("#save").prop("disabled", true);
        resetNotification();
        cropper.getCroppedCanvas().toBlob((blob) =>{
            var formData = new FormData();
            
            
            formData.append('cropperImage', blob,'cropperImage.png');
            
            formData.append('originalImage', document.getElementById("inputImage").files[0]);

            formData.append('CropData', JSON.stringify(CropData));
            formData.append('CropBoxData', JSON.stringify(CropBoxData));
            formData.append('CanvasData', JSON.stringify(CanvasData));

            formData.append('namesubcategory',$("#namesubcategory").val());
            formData.append('country',$("#country").val());
            formData.append('provinces',$("#provinces").val());
            formData.append('districts',$("#districts").val());
            formData.append('subdistricts',$("#subdistricts").val());

            formData.append('coverItem_url',$("#coverItem_url").val());
            formData.append('itmes_topicThai',$("#itmes_topicThai").val());
            formData.append('itmes_topicEnglish',$("#itmes_topicEnglish").val());
            formData.append('itmes_topicLaos',$("#itmes_topicLaos").val());
            formData.append('itmes_topicChinese',$("#itmes_topicChinese").val());
            formData.append('items_contactThai',$("#items_contactThai").val());
            formData.append('items_contactEnglish',$("#items_contactEnglish").val());
            formData.append('items_contactLaos',$("#items_contactLaos").val());
            formData.append('items_contactChinese',$("#items_contactChinese").val());
            formData.append('items_latitude',$("#items_latitude").val());
            formData.append('items_longitude',$("#items_longitude").val());
            formData.append('dayofweek',$("#dayofweek").val());
            formData.append('items_timeOpen',$("#items_timeOpen").val());
            formData.append('items_timeClose',$("#items_timeClose").val());
            formData.append('items_phone',$("#items_phone").val());
            formData.append('items_email',$("#items_email").val());
            formData.append('items_line',$("#items_line").val());
            formData.append('items_facebookPage',$("#items_facebookPage").val());

            formData.append('topicdetail_id_1_Thai',$("#topicdetail_id_1_Thai").val());
            formData.append('topicdetail_id_1_English',$("#topicdetail_id_1_English").val());
            formData.append('topicdetail_id_1_Laos',$("#topicdetail_id_1_Laos").val());
            formData.append('topicdetail_id_1_Chinese',$("#topicdetail_id_1_Chinese").val());

            formData.append('topicdetail_id_2_Thai',$("#topicdetail_id_2_Thai").val());
            formData.append('topicdetail_id_2_English',$("#topicdetail_id_2_English").val());
            formData.append('topicdetail_id_2_Laos',$("#topicdetail_id_2_Laos").val());
            formData.append('topicdetail_id_2_Chinese',$("#topicdetail_id_2_Chinese").val());
            
            formData.append('topicdetail_id_3_Thai',$("#topicdetail_id_3_Thai").val());
            formData.append('topicdetail_id_3_English',$("#topicdetail_id_3_English").val());
            formData.append('topicdetail_id_3_Laos',$("#topicdetail_id_3_Laos").val());
            formData.append('topicdetail_id_3_Chinese',$("#topicdetail_id_3_Chinese").val());
            
            formData.append('item_ar_url',$("#item_ar_url").val());
            formData.append('item_vr_url',$("#item_vr_url").val());

            formData.append('image_info',image_info);
            formData.append('image_navigate',image_navigate);
            formData.append('image_thingstomeet',image_thingstomeet);
            

            for (var i = 0; i < image_thingstomeet; i++) { 
                formData.append("img_thingstomeet_fname_"+(i+1),document.getElementById("img_thingstomeet_fname_"+(i+1)).files[0]);
            }
            for (var i = 0; i < image_info; i++) { 
                formData.append("img_info_fname_"+(i+1),document.getElementById("img_info_fname_"+(i+1)).files[0]);
                formData.append("photo_textThai_info_"+(i+1),$("#photo_textThai_info_"+(i+1)).val());
                formData.append("photo_textEnglish_info_"+(i+1),$("#photo_textEnglish_info_"+(i+1)).val());
                formData.append("photo_textLaos_info_"+(i+1),$("#photo_textLaos_info_"+(i+1)).val());
                formData.append("photo_textChinese_info_"+(i+1),$("#photo_textChinese_info_"+(i+1)).val());
            }
            for (var i = 0; i < image_navigate; i++) { 
                formData.append("img_navigate_fname_"+(i+1),document.getElementById("img_navigate_fname_"+(i+1)).files[0]);
                formData.append("photo_textThai_navigate_"+(i+1),$("#photo_textThai_navigate_"+(i+1)).val());
                formData.append("photo_textEnglish_navigate_"+(i+1),$("#photo_textEnglish_navigate_"+(i+1)).val());
                formData.append("photo_textChinese_navigate_"+(i+1),$("#photo_textChinese_navigate_"+(i+1)).val());
            }
            for (var i = 0; i < image_foodmenu; i++) {
                formData.append("product_namesThai_"+(i+1),$("#name_foodmenu_"+(i+1)).val());
                formData.append("product_namesEnglish_"+(i+1),$("#name_Eng_foodmenu_"+(i+1)).val());
                formData.append("product_namesLaos_"+(i+1),$("#name_Laos_foodmenu_"+(i+1)).val());

                formData.append("product_price_"+(i+1),($("#price_foodmenu_"+(i+1)).val() == ""?"0":$("#price_foodmenu_"+(i+1)).val()));

                formData.append("product_descriptionThai_"+(i+1),$("#details_foodmenu_"+(i+1)).val());
                formData.append("product_descriptionEnglish_"+(i+1),$("#details_Eng_foodmenu_"+(i+1)).val());
                formData.append("product_descriptionLaos_"+(i+1),$("#details_Laos_foodmenu_"+(i+1)).val());
                for (var j = 0; j < array_image_foodmenu[i]; j++) {
                    formData.append("img_image_foodmenu_"+(i+1)+"_fname_"+(j+1),document.getElementById("img_image_foodmenu_"+(i+1)+"_fname_"+(j+1)).files[0]);
                }
            }

            formData.append('image_foodmenu',image_foodmenu);
            formData.append('array_image_foodmenu',array_image_foodmenu);
            formData.append('array_delicious',array_delicious);
           
            if(checktypefile() == 0)
                ajaxSaveItem(formData,"saveNewItem","Items")
            else
            {
                var element = document.getElementById("notifi_error_save");
                element.style.color = "red";
                $("#notifi_error_save").css('display', 'inline', 'important');
                element.innerHTML = "Please select only jpg/png file.";
                $("#save").prop("disabled", false);
            }

        });
    });  
  
});
