$(document).ready(function(){
    document.getElementById("SearchBoxText").textContent= SearchBoxText;
    $("#SearchBoxText").append(' <span class="float-right"><i class="fa fa-chevron-down"></i></span>');
   
    table = $('#'+MenuName+'Table').DataTable({
        processing: true,
        //stateSave: true,
        //serverSide: true,
        columnDefs: [
            {
                "targets": [ 1,2,3,4,5 ],"visible": false,
                
            },
            { orderable: false, targets:[0] },
        ],
        "order": [[ 5, "asc" ]],
        dom: '<"toolbar">frtip',
        fnInitComplete: function (oSettings, response) {
            // here you to do after load data
            //response.recordsTotal is used to get Total count data
            document.getElementById("countData").textContent=(response.recordsTotal);
        },
        "ajax": {
            url : host+'/Items/'+MenuName+'/getData'+MenuName+'ForTable',
            type : 'POST',
            data :  function ( d ) {
                d.namesubcategory = $('#namesubcategory').val();
                d.country = $('#country').val();
                d.provinces =$('#provinces').val();
                d.districts =$('#districts').val();
            }, 
        },
        // "language": {
        //     "lengthMenu": "_MENU_",
        //     // "zeroRecords": "Nothing found - sorry",
        //     // "info": "Showing page _PAGE_ of _PAGES_",
        //     // "infoEmpty": "No records available",
        //     // "infoFiltered": "(filtered from _MAX_ total records)"
        // },
        
    }); 
    $('#DatasTables').DataTable({
        responsive: true
    });
    $(document).on("click", "#btn_search", function () {
        table.ajax.reload();
        setTimeout( function () {
            document.getElementById("countData").textContent= table.data().length
        }, 500 );
        
    });  
    
    $(document).on('click', '#btn_search_clear', function () {
        ResetSelect("country");
        document.getElementById("country").selectedIndex = 0;
        $('#country').trigger("chosen:updated");
        document.getElementById("namesubcategory").selectedIndex = 0;
        $('#namesubcategory').trigger("chosen:updated");
        table.ajax.reload();
        setTimeout( function () {
            document.getElementById("countData").textContent= table.data().length
        }, 500 );
    });
    $.ajax({
        type: 'post',
        url: host+'/Items/'+MenuName+'/getNameCategorySubCategory',
        success: function (response) {
          $("#namesubcategory").empty();
          $("#namesubcategory").append(response);
          $('#namesubcategory').trigger("chosen:updated");
        }
    });
    
});
