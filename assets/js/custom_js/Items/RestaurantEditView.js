$(document).ready(function(){
    $.ajax({
        type: 'post',
        url: host+'/Items/'+MenuName+'/getNameCategorySubCategory',
        success: function (response) {
          $("#namesubcategory").empty();
          $("#namesubcategory").append(response);
          $("#namesubcategory").val(subcategory_id);
          $('#namesubcategory').trigger("chosen:updated");
        }
    });

    $.ajax({
        type: 'post',
        url: host+'/Items/'+MenuName+'/getNameDeliciousGuarantee',
        success: function (response) {
            $("#delicious").empty();
            $("#delicious").append(response);
            if(array_delicious != null)
            $('#delicious').val(array_delicious);
            $('#delicious').trigger("chosen:updated");
            
            for(var i=0; i< array_delicious.length;i++)
            {
                NewImageBox(array_delicious[i],"delicious")
                $.when(ajax1(array_delicious[i])).done(function(data){
                    document.getElementById("output_image_delicious_"+data[0].deliciousGuarantee_id).src = "/dasta_thailand/assets/img/"+data[0].deliciousGuarantee_paths;
                    document.getElementById("name_delicious_"+data[0].deliciousGuarantee_id).innerHTML = data[0].deliciousGuarantee_textThai
                });
            }
 
        }
    });
    
    $(document).on('change', '#delicious', function (e) {
        if(array_delicious.length > $('#delicious').val().length)
        {
            var temp_array_delicious =[]
            $(this).find('option:selected').each(function(){
                if(array_delicious.includes($(this).val()))
                {
                    temp_array_delicious.push($(this).val());
                }
            });
            array_delicious = temp_array_delicious;
            $('.form-group-delicious-image').each(function (idx, element) {
                var formsplict = element.id.split('_')
                if(!array_delicious.includes(formsplict[4]))
                    $("#"+element.id).remove();
            });
        }
        else
        {
            $(this).find('option:selected').each(function(){
                if(!array_delicious.includes($(this).val()))
                {
                    array_delicious.push($(this).val());
                    var id = $(this).val();
                    NewImageBox(id,"delicious");
                    $.ajax({
                        type: 'post',
                        url: host+'/Items/'+MenuName+'/getDeliciousGuarantee',
                        data:{
                            deliciousGuarantee_id:id
                        },
                        success: function (response) {
                            document.getElementById("output_image_delicious_"+id).src = "/dasta_thailand/assets/img/"+response[0].deliciousGuarantee_paths
                            document.getElementById("name_delicious_"+id).innerHTML = response[0].deliciousGuarantee_textThai
                        }
                    });

                }
                
            });
        }
    });

    document.getElementById('image_crop_img').addEventListener('ready', (event) => {      
        if(!crop_img)
        {
            cropper.setData(CropData);
            cropper.setCropBoxData(CropBoxData);
            cropper.setCanvasData(CanvasData);
            crop_img = true;
        }   
    });
    
    document.getElementById('image_crop_img').addEventListener('crop', (event) => {
        if(crop_img)
        {
            CropData = cropper.getData()
            CropBoxData = cropper.getCropBoxData()
            CanvasData = cropper.getCanvasData()
        }
    });  

    setTimeout(function () {SetSelectLocation();}, 1000);
    SetImageBox();
    addOnKeyUpMarker(document.getElementById("items_latitude").value,document.getElementById("items_longitude").value)
    $(document).on("click", "#save", function () {
        $("#save").prop("disabled", true);
        resetNotification();
        cropper.getCroppedCanvas().toBlob((blob) =>{
            var formData = new FormData();

            formData.append('cropperImage', blob,'cropperImage.png');
            
            if(document.getElementById("inputImage").files[0] != null)
                formData.append('originalImage', document.getElementById("inputImage").files[0]);
            else
            {
                
                formData.append("originalImage",dataURLtoFile(document.getElementById("temp_inputImage").value,'originalImage'))
            }

            formData.append('CropData', JSON.stringify(CropData));
            formData.append('CropBoxData', JSON.stringify(CropBoxData));
            formData.append('CanvasData', JSON.stringify(CanvasData));


            formData.append('items_id',items_id);

            formData.append('namesubcategory',$("#namesubcategory").val());
            formData.append('country',$("#country").val());
            formData.append('provinces',$("#provinces").val());
            formData.append('districts',$("#districts").val());
            formData.append('subdistricts',$("#subdistricts").val());

            formData.append('coverItem_url',$("#coverItem_url").val());
            formData.append('itmes_topicThai',$("#itmes_topicThai").val());
            formData.append('itmes_topicEnglish',$("#itmes_topicEnglish").val());
            formData.append('itmes_topicLaos',$("#itmes_topicLaos").val());
            formData.append('itmes_topicChinese',$("#itmes_topicChinese").val());
            formData.append('items_contactThai',$("#items_contactThai").val());
            formData.append('items_contactEnglish',$("#items_contactEnglish").val());
            formData.append('items_contactLaos',$("#items_contactLaos").val());
            formData.append('items_contactChinese',$("#items_contactChinese").val());
            formData.append('items_latitude',$("#items_latitude").val());
            formData.append('items_longitude',$("#items_longitude").val());
            formData.append('dayofweek',$("#dayofweek").val());
            formData.append('items_timeOpen',$("#items_timeOpen").val());
            formData.append('items_timeClose',$("#items_timeClose").val());
            formData.append('items_phone',$("#items_phone").val());
            formData.append('items_email',$("#items_email").val());
            formData.append('items_line',$("#items_line").val());
            formData.append('items_facebookPage',$("#items_facebookPage").val());

            formData.append('topicdetail_id_1_Thai',$("#topicdetail_id_1_Thai").val());
            formData.append('topicdetail_id_1_English',$("#topicdetail_id_1_English").val());
            formData.append('topicdetail_id_1_Laos',$("#topicdetail_id_1_Laos").val());
            formData.append('topicdetail_id_1_Chinese',$("#topicdetail_id_1_Chinese").val());

            formData.append('topicdetail_id_2_Thai',$("#topicdetail_id_2_Thai").val());
            formData.append('topicdetail_id_2_English',$("#topicdetail_id_2_English").val());
            formData.append('topicdetail_id_2_Laos',$("#topicdetail_id_2_Laos").val());
            formData.append('topicdetail_id_2_Chinese',$("#topicdetail_id_2_Chinese").val());
            
            formData.append('topicdetail_id_3_Thai',$("#topicdetail_id_3_Thai").val());
            formData.append('topicdetail_id_3_English',$("#topicdetail_id_3_English").val());
            formData.append('topicdetail_id_3_Laos',$("#topicdetail_id_3_Laos").val());
            formData.append('topicdetail_id_3_Chinese',$("#topicdetail_id_3_Chinese").val());
            
            formData.append('item_ar_url',$("#item_ar_url").val());
            formData.append('item_vr_url',$("#item_vr_url").val());

            formData.append('image_info',image_info);
            formData.append('image_navigate',image_navigate);
            formData.append('image_thingstomeet',image_thingstomeet);

            for (var i = 0; i < image_thingstomeet; i++) { 

                if(document.getElementById("img_thingstomeet_fname_"+(i+1)).files[0] != null)
                    formData.append("img_thingstomeet_fname_"+(i+1),document.getElementById("img_thingstomeet_fname_"+(i+1)).files[0]);
                else
                {
                   
                    formData.append("img_thingstomeet_fname_"+(i+1),dataURLtoFile(document.getElementById("temp_img_thingstomeet_fname_"+(i+1)).value,'thingstomeet'+(i+1)))
                }
            }
            for (var i = 0; i < image_info; i++) { 
                if(document.getElementById("img_info_fname_"+(i+1)).files[0] != null)
                    formData.append("img_info_fname_"+(i+1),document.getElementById("img_info_fname_"+(i+1)).files[0]);
                else
                {
                    
                    formData.append("img_info_fname_"+(i+1),dataURLtoFile(document.getElementById("temp_img_info_fname_"+(i+1)).value,'info'+(i+1)))
                }

                formData.append("photo_textThai_info_"+(i+1),$("#photo_textThai_info_"+(i+1)).val());
                formData.append("photo_textEnglish_info_"+(i+1),$("#photo_textEnglish_info_"+(i+1)).val());
                formData.append("photo_textLaos_info_"+(i+1),$("#photo_textLaos_info_"+(i+1)).val());
                formData.append("photo_textChinese_info_"+(i+1),$("#photo_textChinese_info_"+(i+1)).val());
            }
            for (var i = 0; i < image_navigate; i++) { 
                if(document.getElementById("img_navigate_fname_"+(i+1)).files[0] != null)
                    formData.append("img_navigate_fname_"+(i+1),document.getElementById("img_navigate_fname_"+(i+1)).files[0]);
                else
                {
                    
                    formData.append("img_navigate_fname_"+(i+1),dataURLtoFile(document.getElementById("temp_img_navigate_fname_"+(i+1)).value,'navigate'+(i+1)))
                }
                formData.append("photo_textThai_navigate_"+(i+1),$("#photo_textThai_navigate_"+(i+1)).val());
                formData.append("photo_textEnglish_navigate_"+(i+1),$("#photo_textEnglish_navigate_"+(i+1)).val());
                formData.append("photo_textLaos_navigate_"+(i+1),$("#photo_textLaos_navigate_"+(i+1)).val());
                formData.append("photo_textChinese_navigate_"+(i+1),$("#photo_textChinese_navigate_"+(i+1)).val());
            }

            for (var i = 0; i < image_foodmenu; i++) {
                formData.append("temp_id_foodmenu_"+(i+1),$("#temp_id_foodmenu_"+(i+1)).val());
                formData.append("product_namesThai_"+(i+1),$("#name_foodmenu_"+(i+1)).val());
                formData.append("product_namesEnglish_"+(i+1),$("#name_Eng_foodmenu_"+(i+1)).val());
                formData.append("product_namesLaos_"+(i+1),$("#name_Laos_foodmenu_"+(i+1)).val());
                formData.append("product_namesChinese_"+(i+1),$("#name_Chinese_foodmenu_"+(i+1)).val());

                formData.append("product_price_"+(i+1),($("#price_foodmenu_"+(i+1)).val() == ""?"0":$("#price_foodmenu_"+(i+1)).val()));

                formData.append("product_descriptionThai_"+(i+1),$("#details_foodmenu_"+(i+1)).val());
                formData.append("product_descriptionEnglish_"+(i+1),$("#details_Eng_foodmenu_"+(i+1)).val());
                formData.append("product_descriptionLaos_"+(i+1),$("#details_Laos_foodmenu_"+(i+1)).val());
                formData.append("product_descriptionChinese_"+(i+1),$("#details_Chinese_foodmenu_"+(i+1)).val());
                for (var j = 0; j < array_image_foodmenu[i]; j++) {
                    if(document.getElementById("img_image_foodmenu_"+(i+1)+"_fname_"+(j+1)).files[0] != null)
                        formData.append("img_image_foodmenu_"+(i+1)+"_fname_"+(j+1),document.getElementById("img_image_foodmenu_"+(i+1)+"_fname_"+(j+1)).files[0]);
                    else
                    {
                        var typeImage = document.getElementById('output_image_image_foodmenu_'+(i+1)+'_'+(j+1)).src.split('.')
                        formData.append("img_image_foodmenu_"+(i+1)+"_fname_"+(j+1),dataURLtoFile(document.getElementById('temp_img_image_foodmenu_'+(i+1)+'_fname_'+(j+1)).value,'image_foodmenu_'+(i+1)+'_'+(j+1)))
                    }
                }
            }

            formData.append('image_foodmenu',image_foodmenu);
            formData.append('array_image_foodmenu',array_image_foodmenu);
            formData.append('array_delicious',array_delicious);

        
            if(checktypefile() == 0)
                ajaxSaveItem(formData,'saveEditItem',"Items")
            else
            {
                var element = document.getElementById("notifi_error_save");
                element.style.color = "red";
                $("#notifi_error_save").css('display', 'inline', 'important');
                element.innerHTML = "Please select only jpg/png file.";
                $("#save").prop("disabled", false);
            }

        });
    });  
    

});
function ajax1(delicious_id) {
    // NOTE:  This function must return the value 
    //        from calling the $.ajax() method.
    return  $.ajax({
        type: 'post',
        url: host+'/Items/'+MenuName+'/getDeliciousGuarantee',
        data:{
            deliciousGuarantee_id:delicious_id
        },
        success: function (response) {
        }
    });
}
async function SetImageBox()
{
    var typeImage = document.getElementById('image_crop_img').src.split('.')
    var file = await srcToFile(document.getElementById('image_crop_img').src,"temp_image_crop_img",'image/'+typeImage[1])
    handleFileSelect(file,document.getElementById('temp_inputImage')) 

    for (i = 0; i < image_thingstomeet; i++) {
        NewImageBox((i+1),"thingstomeet",false);
        
        document.getElementById('output_image_thingstomeet_'+(i+1)).src = "/dasta_thailand/assets/img/uploadfile/"+PhotoThingstomeet[i].photo_paths;
        typeImage = PhotoThingstomeet[i].photo_paths.split('.')
        file = await srcToFile(document.getElementById('output_image_thingstomeet_'+(i+1)).src,PhotoThingstomeet[i].photo_paths,'image/'+typeImage[1])
        handleFileSelect(file,document.getElementById('temp_img_thingstomeet_fname_'+(i+1)))       
    }
    for (i = 0; i < image_info; i++) {
        NewImageBox((i+1),"info",false);
        document.getElementById('output_image_info_'+(i+1)).src = "/dasta_thailand/assets/img/uploadfile/"+PhotoInfo[i].photo_paths;
        document.getElementById('output_image_Eng_info_'+(i+1)).src = "/dasta_thailand/assets/img/uploadfile/"+PhotoInfo[i].photo_paths;
        document.getElementById('output_image_Laos_info_'+(i+1)).src = "/dasta_thailand/assets/img/uploadfile/"+PhotoInfo[i].photo_paths;
        document.getElementById('output_image_Chinese_info_'+(i+1)).src = "/dasta_thailand/assets/img/uploadfile/"+PhotoInfo[i].photo_paths;

        typeImage = PhotoInfo[i].photo_paths.split('.')
        file = await srcToFile(document.getElementById('output_image_info_'+(i+1)).src,PhotoInfo[i].photo_paths,'image/'+typeImage[1])
        handleFileSelect(file,document.getElementById('temp_img_info_fname_'+(i+1)))

        document.getElementById('photo_textThai_info_'+(i+1)).value = PhotoInfo[i].photo_textThai;
        document.getElementById('photo_textEnglish_info_'+(i+1)).value = PhotoInfo[i].photo_textEnglish;
        document.getElementById('photo_textLaos_info_'+(i+1)).value = PhotoInfo[i].photo_textLaos;
        document.getElementById('photo_textChinese_info_'+(i+1)).value = PhotoInfo[i].photo_textChinese;
        
    }
    for (i = 0; i < image_navigate; i++) {
        NewImageBox((i+1),"navigate",false);
        document.getElementById('output_image_navigate_'+(i+1)).src = "/dasta_thailand/assets/img/uploadfile/"+PhotoNavigate[i].photo_paths;
        document.getElementById('output_image_Eng_navigate_'+(i+1)).src = "/dasta_thailand/assets/img/uploadfile/"+PhotoNavigate[i].photo_paths;
        document.getElementById('output_image_Laos_navigate_'+(i+1)).src = "/dasta_thailand/assets/img/uploadfile/"+PhotoNavigate[i].photo_paths;
        document.getElementById('output_image_Chinese_navigate_'+(i+1)).src = "/dasta_thailand/assets/img/uploadfile/"+PhotoNavigate[i].photo_paths;

        typeImage = PhotoNavigate[i].photo_paths.split('.')
        file = await srcToFile(document.getElementById('output_image_navigate_'+(i+1)).src,PhotoNavigate[i].photo_paths,'image/'+typeImage[1])
        handleFileSelect(file,document.getElementById('temp_img_navigate_fname_'+(i+1)))

        document.getElementById('photo_textThai_navigate_'+(i+1)).value = PhotoNavigate[i].photo_textThai;
        document.getElementById('photo_textEnglish_navigate_'+(i+1)).value = PhotoNavigate[i].photo_textEnglish;
        document.getElementById('photo_textLaos_navigate_'+(i+1)).value = PhotoNavigate[i].photo_textLaos;
        document.getElementById('photo_textChinese_navigate_'+(i+1)).value = PhotoNavigate[i].photo_textChinese;
    }

    for (i = 0; i < image_foodmenu; i++) {
        NewImageBox((i+1),"foodmenu",false);
        document.getElementById('temp_id_foodmenu_'+(i+1)).value = data_image_foodmenu[i].product_id;
        document.getElementById('name_foodmenu_'+(i+1)).value = data_image_foodmenu[i].product_namesThai;
        document.getElementById('name_Eng_foodmenu_'+(i+1)).value = data_image_foodmenu[i].product_namesEnglish;
        document.getElementById('name_Laos_foodmenu_'+(i+1)).value = data_image_foodmenu[i].product_namesLaos;
        document.getElementById('name_Chinese_foodmenu_'+(i+1)).value = data_image_foodmenu[i].product_namesChinese;

        document.getElementById('price_foodmenu_'+(i+1)).value = data_image_foodmenu[i].product_price;
        document.getElementById('price_Eng_foodmenu_'+(i+1)).innerHTML = data_image_foodmenu[i].product_price;
        document.getElementById('price_Laos_foodmenu_'+(i+1)).innerHTML = data_image_foodmenu[i].product_price;
        document.getElementById('price_Chinese_foodmenu_'+(i+1)).innerHTML = data_image_foodmenu[i].product_price;

        document.getElementById('details_foodmenu_'+(i+1)).value = data_image_foodmenu[i].product_descriptionThai;
        document.getElementById('details_Eng_foodmenu_'+(i+1)).value = data_image_foodmenu[i].product_descriptionEnglish;
        document.getElementById('details_Laos_foodmenu_'+(i+1)).value = data_image_foodmenu[i].product_descriptionLaos;
        document.getElementById('details_Chinese_foodmenu_'+(i+1)).value = data_image_foodmenu[i].product_descriptionChinese;

        for(j = 0; j < data_image_foodmenu[i].PhotoProduct.length; j++)
        {
            NewImageBox((j+1),"image_foodmenu_"+(i+1),false);
            document.getElementById('output_image_image_foodmenu_'+(i+1)+'_'+(j+1)).src = "/dasta_thailand/assets/img/uploadfile/"+data_image_foodmenu[i].PhotoProduct[j].photoProduct_paths;
            document.getElementById('output_image_Eng_image_foodmenu_'+(i+1)+'_'+(j+1)).src = "/dasta_thailand/assets/img/uploadfile/"+data_image_foodmenu[i].PhotoProduct[j].photoProduct_paths;
            document.getElementById('output_image_Laos_image_foodmenu_'+(i+1)+'_'+(j+1)).src = "/dasta_thailand/assets/img/uploadfile/"+data_image_foodmenu[i].PhotoProduct[j].photoProduct_paths;
            document.getElementById('output_image_Chinese_image_foodmenu_'+(i+1)+'_'+(j+1)).src = "/dasta_thailand/assets/img/uploadfile/"+data_image_foodmenu[i].PhotoProduct[j].photoProduct_paths;
            typeImage = data_image_foodmenu[i].PhotoProduct[j].photoProduct_paths.split('.')
            file = await srcToFile(document.getElementById('output_image_image_foodmenu_'+(i+1)+'_'+(j+1)).src,data_image_foodmenu[i].PhotoProduct[j].photoProduct_paths,'image/'+typeImage[1])
            handleFileSelect(file,document.getElementById('temp_img_image_foodmenu_'+(i+1)+'_fname_'+(j+1)))
        }
    }
   
}
