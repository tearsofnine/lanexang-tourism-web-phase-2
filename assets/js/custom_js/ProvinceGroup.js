$(document).ready(function () {
	$("#sidebar_PermissionGroup_a").click();
	document.getElementById("sidebar_ProvinceGroup").classList.add("active");

	var table = $("#ProvinceGroupTable").DataTable({
		responsive: true,
		ajax: {
			url: host + "ProvinceGroup/getDataProvinceGroup",
			type: "GET",
		},
		stateSave: true,
		columnDefs: [
			// { className: 'text-center', targets:"_all" },
			// { width: "5%", targets: 0 },
			// { width: "30%", targets: 1 },
			// { width: "10%", targets: 3 },
			// { width: "15%", targets: 4 },
			// { responsivePriority: 1, targets: 0 },
			// { responsivePriority: 10001, targets: 4 },
			// { responsivePriority: 2, targets: -2 }
		],
		order: [[1, "asc"]],
	});
	table
		.on("order.dt search.dt", function () {
			table
				.column(0, { search: "applied", order: "applied" })
				.nodes()
				.each(function (cell, i) {
					cell.innerHTML = i + 1;
				});
		})
		.draw();
	$(".ProvinceSelection").chosen({
		disable_search_threshold: 10,
		search_contains: true,
		width: "100%",
	});
	$(document).on("click", "#ProvinceGroupAdd", function () {
		resetNotification();
		$.ajax({
			type: "post",
			data: { getOption: 219 },
			url: host + "management/LocationManagement/getProvinces",
			success: function (response) {
				$("#ProvinceSelection").prop("disabled", false);
				$("#ProvinceSelection").empty();
				$("#ProvinceSelection").append(response);
				$("#ProvinceSelection").trigger("chosen:updated");
			},
		});
		document.getElementById("pg_thai").value = "";
		document.getElementById("pg_english").value = "";
		document.getElementById("pg_chinese").value = "";
		document.getElementById("pg_laos").value = "";
	});

	$(document).on("click", "#btn_save_ProvinceGroup", function () {
		resetNotification();
		var formData = new FormData();
		formData.append("pg_thai", $("#pg_thai").val());
		formData.append("pg_english", $("#pg_english").val());
		formData.append("pg_chinese", $("#pg_chinese").val());
		formData.append("pg_laos", $("#pg_laos").val());
		formData.append("ProvinceSelection", $("#ProvinceSelection").val());

		// for (var value of formData.values()) {
		// 	console.log(value);
		// }
		$.ajax({
			type: "POST",
			url: host + "ProvinceGroup/addProvinceGroup",
			data: formData,
			contentType: false,
			processData: false,
			success: function (response) {
				if (response.state) {
					var element = document.getElementById(
						"notifi_error_save_ProvinceGroup"
					);
					element.style.color = "green";
					$("#notifi_error_save_ProvinceGroup").css(
						"display",
						"inline",
						"important"
					);
					element.innerHTML = response.msg;
					table.ajax.reload();
					$("#ProvinceGroupModal").modal("hide");
				} else {
					$("#save").prop("disabled", false);
					if (response.error == "validation") {
						msg = response.msg.replace(/<p>The /g, "");
						msg = msg.replace(/ field is required.<\/p>/g, "");
						msg = msg.split("\n");
						msg.forEach((element, index) => {
							var ids = element.split("|");
							if (ids[0] == "required") {
								SetNavLinkTabMainActive();
								var element = document.getElementById("notifi_" + ids[2]);
								element.style.color = "red";
								$("#notifi_" + ids[2]).css("display", "inline", "important");
								$("#notifi_" + ids[2]).text(ids[3]);
								document.getElementById(ids[1]).focus();
							}
						});
					} else {
						var element = document.getElementById("notifi_error_save_ProvinceGroup");
						element.style.color = "red";
						$("#notifi_error_save_ProvinceGroup").css("display", "inline", "important");
						element.innerHTML = response.msg;
					}
				}
			},
		});
	});
});
