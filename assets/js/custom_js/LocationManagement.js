$(document).ready(function(){
    
    $.ajax({
        type: 'post',
        url: host+'/management/LocationManagement/getCountry',
        success: function (response) {
          $("#country").empty();
          $("#country").append(response);
          $('#country').trigger("chosen:updated");
        }
      });

    $(document).on('change', '#country', function (e) {
        var select = $('option:selected', this);
        ResetSelect("country")
        if(select.val() != ""){
            var data = {
                getOption: select.val()
              }
            $.ajax({
                type: 'post',
                data: data,
                url: host+'/management/LocationManagement/getProvinces',
                success: function (response) {
                    $("#provinces").prop("disabled", false);
                    $("#provinces").empty();
                    $("#provinces").append(response);
                    $('#provinces').trigger("chosen:updated");
                }
            });
        }
    });

    $(document).on('change', '#provinces', function (e) {
        var select = $('option:selected', this);
        ResetSelect("provinces")
        if(select.val() != ""){
            var data = {
                getOption: select.val()
              }
            $.ajax({
                type: 'post',
                data: data,
                url: host+'/management/LocationManagement/getDistricts',
                success: function (response) {
                    $("#districts").prop("disabled", false);
                    $("#districts").empty();
                    $("#districts").append(response);
                    $('#districts').trigger("chosen:updated");
                }
            });
        }
    });
    $(document).on('change', '#districts', function (e) {
        var select = $('option:selected', this);
        ResetSelect("districts")
        if(select.val() != ""){
            var data = {
                getOption: select.val()
              }
            $.ajax({
                type: 'post',
                data: data,
                url: host+'/management/LocationManagement/getSubdistricts',
                success: function (response) {
                    $("#subdistricts").prop("disabled", false);
                    $("#subdistricts").empty();
                    $("#subdistricts").append(response);
                    $('#subdistricts').trigger("chosen:updated");
                }
            });
        }
    });
    $(document).on('change', '#subdistricts', function (e) {
        var select = $('option:selected', this);
        
        if(select.val() != "" && select.text() !="unidentified" ){
            if(document.getElementById("items_contactThai"))
                setContact(select);
        }
        
    });
});

function ResetSelect(select){
    switch(select) {
        case "country":
            $("#provinces").empty();
            $("#districts").empty();
            $("#subdistricts").empty();

            $("#provinces").prop("disabled", true);
            $("#districts").prop("disabled", true);
            $("#subdistricts").prop("disabled", true);

            $('#provinces').trigger("chosen:updated");
            $('#districts').trigger("chosen:updated");
            $('#subdistricts').trigger("chosen:updated");
          break;
        case "provinces":
            $("#districts").empty();
            $("#subdistricts").empty();

            $("#districts").prop("disabled", true);
            $("#subdistricts").prop("disabled", true);

            
            $('#districts').trigger("chosen:updated");
            $('#subdistricts').trigger("chosen:updated");
          break;
        case "districts":
            $("#subdistricts").empty();
            
            $("#subdistricts").prop("disabled", true);
            
            $('#subdistricts').trigger("chosen:updated");
          break;
    }
}
function ResetSelectProgramTour(select,mainid){
    switch(select) {
        case "country":
            $("#"+mainid+"provinces").empty();
            $("#"+mainid+"districts").empty();
            $("#"+mainid+"category").empty();
            $("#"+mainid+"type").empty();
            $("#"+mainid+"place").empty();

            $("#"+mainid+"provinces").prop("disabled", true);
            $("#"+mainid+"districts").prop("disabled", true);
            $("#"+mainid+"category").prop("disabled", true);
            $("#"+mainid+"type").prop("disabled", true);
            $("#"+mainid+"place").prop("disabled", true);

            $("#"+mainid+"provinces").trigger("chosen:updated");
            $("#"+mainid+"districts").trigger("chosen:updated");
            $("#"+mainid+"category").trigger("chosen:updated");
            $("#"+mainid+"type").trigger("chosen:updated");
            $("#"+mainid+"place").trigger("chosen:updated");
          break;
        case "provinces":
            $("#"+mainid+"districts").empty();
            $("#"+mainid+"category").empty();
            $("#"+mainid+"type").empty();
            $("#"+mainid+"place").empty();

            $("#"+mainid+"districts").prop("disabled", true);
            $("#"+mainid+"category").prop("disabled", true);
            $("#"+mainid+"type").prop("disabled", true);
            $("#"+mainid+"place").prop("disabled", true);

            
            $("#"+mainid+"districts").trigger("chosen:updated");
            $("#"+mainid+"category").trigger("chosen:updated");
            $("#"+mainid+"type").trigger("chosen:updated");
            $("#"+mainid+"place").trigger("chosen:updated");
          break;
        case "districts":
            $("#"+mainid+"category").empty();
            $("#"+mainid+"type").empty();
            $("#"+mainid+"place").empty();

            $("#"+mainid+"category").prop("disabled", true);
            $("#"+mainid+"type").prop("disabled", true);
            $("#"+mainid+"place").prop("disabled", true);
            
            $("#"+mainid+"category").trigger("chosen:updated");
            $("#"+mainid+"type").trigger("chosen:updated");
            $("#"+mainid+"place").trigger("chosen:updated");
          break;
        case "category":
            $("#"+mainid+"type").empty();
            $("#"+mainid+"place").empty();
            
            $("#"+mainid+"type").prop("disabled", true);
            $("#"+mainid+"place").prop("disabled", true);

            $("#"+mainid+"type").trigger("chosen:updated");
            $("#"+mainid+"place").trigger("chosen:updated");
          break;
    }
}

function setContact(subdistricts){
    var element_contact_th = document.getElementById("items_contactThai");
    var element_contact_eng = document.getElementById("items_contactEnglish");
    var element_contact_lao = document.getElementById("items_contactLaos");
    var element_contact_chn = document.getElementById("items_contactChinese");

    var data = {
        id: subdistricts.val(),
        types: "subdistricts_id",
        querys: "Subdistricts"
    }
    $.ajax({
        type: 'post',
        url: host+'/management/LocationManagement/getNameLocation',
        data: data,
        success: function (responsesubdistricts) {
            element_contact_th.value = "ตำบล"+responsesubdistricts.subdistricts_thai;
            element_contact_eng.value = responsesubdistricts.subdistricts_english;
            element_contact_lao.value = responsesubdistricts.subdistricts_laos;
            element_contact_chn.value = responsesubdistricts.subdistricts_chinese;
            
            var data = {
                id: responsesubdistricts.Districts_districts_id,
                types: "districts_id",
                querys: "Districts"
            }

            $.ajax({
                type: 'post',
                url: host+'/management/LocationManagement/getNameLocation',
                data: data,
                success: function (responsedistrict) {
                    element_contact_th.value += " อำเภอ"+responsedistrict.districts_thai;
                    element_contact_eng.value += ", "+responsedistrict.districts_english;
                    element_contact_lao.value += ", "+responsedistrict.districts_laos;
                    element_contact_chn.value += ", "+responsedistrict.districts_chinese;
                    
                    var data = {
                        id: responsedistrict.Provinces_provinces_id,
                        types: "provinces_id",
                        querys: "Provinces"
                    }

                    $.ajax({
                        type: 'post',
                        url: host+'/management/LocationManagement/getNameLocation',
                        data: data,
                        success: function (responseprovinces) {
                            element_contact_th.value += " จังหวัด"+responseprovinces.provinces_thai;
                            element_contact_eng.value += ", "+responseprovinces.provinces_english;
                            element_contact_lao.value += ", "+responseprovinces.Provinces_laos;
                            element_contact_chn.value += ", "+responseprovinces.Provinces_chinese;
                            
                            var data = {
                                id: responseprovinces.Country_country_id,
                                types: "country_id",
                                querys: "Country"
                            }

                            $.ajax({
                                type: 'post',
                                url: host+'/management/LocationManagement/getNameLocation',
                                data: data,
                                success: function (responsecountry) {
                                    
                                    element_contact_eng.value += ", "+responsecountry.country_english;
                                    element_contact_lao.value += ", "+responsecountry.country_laos;
                                    element_contact_chn.value += ", "+responsecountry.country_chinese;
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}