function initMap() {
    var ElementIdMap = document.getElementById('map');
    // var ElementIdMapEnglish = document.getElementById('mapEnglish');
    // var ElementIdMapChinese = document.getElementById('mapChinese');
    // var ElementIdMapLaos = document.getElementById('mapLaso');
    if(ElementIdMap != null)
    {
        if(UseGoogleMap.includes('production'))
        {
            map = new google.maps.Map(ElementIdMap, {
                center: {
                    lat: 14.1436398, 
                    lng: 102.5548934
                },
                zoom: 5,
                fullscreenControl:false
            });
            // if(TypeMenu.includes("Approval"))
            // {
            //     mapEnglish = new google.maps.Map(ElementIdMapEnglish, {
            //         center: {
            //             lat: 14.1436398, 
            //             lng: 102.5548934
            //         },
            //         zoom: 5,
            //         fullscreenControl:false
            //     });
            //     mapChinese = new google.maps.Map(ElementIdMapChinese, {
            //         center: {
            //             lat: 14.1436398, 
            //             lng: 102.5548934
            //         },
            //         zoom: 5,
            //         fullscreenControl:false
            //     }); 
            //     mapLaos = new google.maps.Map(ElementIdMapLaos, {
            //         center: {
            //             lat: 14.1436398, 
            //             lng: 102.5548934
            //         },
            //         zoom: 5,
            //         fullscreenControl:false
            //     });
            // }
            if(!TypeMenu.includes("Approval"))
            {
                map.addListener('click', function(e) {
                    removeMarkers()
                    addOnClickMarker(e.latLng);
                });
            }  
        }
    }  
}
// define function to add marker at given lat & lng
function addOnClickMarker(latLng) {
    let marker = new google.maps.Marker({
        map: map,
        position: latLng,
        draggable: true
    });
    var ElementItemMap = document.getElementById('items_latitude');
    var ElementUserMap = document.getElementById('business_latitude');
    if(ElementItemMap)
    {
        document.getElementById("items_latitude").value = marker.getPosition().lat();
        document.getElementById("items_longitude").value = marker.getPosition().lng();
    }
    else if(ElementUserMap)
    {
        document.getElementById("business_latitude").value = marker.getPosition().lat();
        document.getElementById("business_longitude").value = marker.getPosition().lng();
    }
  
    
    gmarkers.push(marker);
}
function addOnKeyUpMarker(Lat,Lng) {
    removeMarkers()
    let marker = new google.maps.Marker({
        map: map,
        position: new google.maps.LatLng(Lat, Lng),
        draggable: true
    });

    gmarkers.push(marker);
}

function removeMarkers(){
    for(i=0; i<gmarkers.length; i++){
        gmarkers[i].setMap(null);
    }
}

function searchMapsBox( searchBox){
    map.addListener("bounds_changed", () => {
        searchBox.setBounds(map.getBounds());
    });
    searchBox.addListener("places_changed", () => {
        const places = searchBox.getPlaces();
    
        if (places.length == 0) {
          return;
        } // Clear out the old markers.
    
        gmarkers.forEach(marker => {
            marker.setMap(null);
        });
        gmarkers = []; // For each place, get the icon, name and location.
    
        const bounds = new google.maps.LatLngBounds();
        places.forEach(place => {
            if (!place.geometry) {
                console.log("Returned place contains no geometry");
                return;
            }
    
            const icon = {
                url: place.icon,
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
            }; // Create a marker for each place.
    
            gmarkers.push(
                new google.maps.Marker({
                    map,
                    icon,
                    title: place.name,
                    //   position: place.geometry.location
                })
            );
    
            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } 
            else {
                bounds.extend(place.geometry.location);
            }
        });
        map.fitBounds(bounds);
    });
}