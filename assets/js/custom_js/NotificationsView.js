$(document).ready(function(){

    table = $('#ListChatTable').DataTable({
        processing: true,
        responsive: false, 
        columnDefs: [
            {
                "targets": [0],"visible": false,
                
            }
        ],
        "ordering": false,
        dom: '<"toolbar">frtip',
        fnInitComplete: function (oSettings, response) {
            // here you to do after load data
            //response.recordsTotal is used to get Total count data
            //document.getElementById("header_count_list").innerHTML =response.recordsTotal+" รายการ"
        },
        "ajax": {
            url :'/dasta_thailand/html/Notifications/getDataForTable',
            type : 'POST',
            data :  function ( d ) {
                d.user_id = user_id;
            }, 
        },
    });
    $(".dataTables_filter").hide();

    setTimeout( function () {
        var rows = table.rows(0).data();
        
        if(rows.length != 0)
        {
            select_rows_data = rows[0][0];
            addChat(select_rows_data,user_id)
            updateNotifications()
            setTimeout( function () {
                table.ajax.reload();
            }, 200 );
            $( table.rows(0).nodes() ).addClass('active-td')
        }

       
       
    }, 500 );

    $("#message").on('keyup', function (event) {
        if (event.keyCode === 13) {
            sendChat(select_rows_data,user_id);
        }
    });
    $(document).on("click", "#send_message", function () {
        sendChat(select_rows_data,user_id)
    })
    $('#ListChatTable tbody').on( 'click', 'tr', function () {
       
        var rows = table.rows(this).data();
        select_rows_data = rows[0][0];
        addChat(select_rows_data,user_id)
        updateNotifications()
        setTimeout( function () {
            table.ajax.reload();
            
            setTimeout( function () {
                indexes = table.column( 0 ).data().indexOf( select_rows_data );
                table.$('tr.active-td').removeClass('active-td');
                $(table.rows(indexes).nodes()).addClass('active-td');

                
            }, 200 );
        }, 200 );
    });
})

messaging.onMessage(function(payload) {
    var indexes = table.column( 0 ).data().indexOf( select_rows_data );
    var rows = table.rows(indexes).data();
    if(rows.length != 0)
    {
       
        addChat(rows[0][0],user_id)
        updateNotifications()
        setTimeout( function () {
            table.ajax.reload();
            
            setTimeout( function () {
                indexes = table.column( 0 ).data().indexOf( select_rows_data );
                table.$('tr.active-td').removeClass('active-td');
                $(table.rows(indexes).nodes()).addClass('active-td');

                
            }, 200 );
        }, 200 );
    }
    else
    {
        table.ajax.reload();
        setTimeout( function () {
            var indexes = table.column( 0 ).data().indexOf( select_rows_data );
            var rows = table.rows(indexes).data();
            
            addChat(rows[0][0],user_id)
            updateNotifications()
            setTimeout( function () {
                indexes = table.column( 0 ).data().indexOf( select_rows_data );
                table.$('tr.active-td').removeClass('active-td');
                $(table.rows(indexes).nodes()).addClass('active-td');

                
            }, 200 );
        }, 200 );
    }
});

function sendChat(chatroom_id,user_id)
{
    $.ajax({
        type: 'POST',
        data: {
            chatroom_id:chatroom_id,
            user_id:user_id
        },
        url: './Notifications/getChatRoom',
        success: function (response) {
            var from_user_id =0;
            var to_user_id =0;
            if(response[0].UserSend_user_id == user_id)
            {
                from_user_id = user_id;
                to_user_id = response[0].UserReceive_user_id;
            }
            else
            {
                from_user_id = user_id;
                to_user_id = response[0].UserSend_user_id;
            }
            var message =  document.getElementById("message").value;
            if(message)
            {
                $.ajax({
                    type: 'POST',
                    data: {
                        from_user_id:from_user_id,
                        to_user_id:to_user_id,
                        message:message
                    },
                    url: '../api/chat/Chat/sendingMessage',
                    success: function (response) {
                        var indexes = table.column( 0 ).data().indexOf( chatroom_id );
                        var rows = table.rows(indexes).data();
                        addChat(rows[0][0],user_id)
                        setTimeout( function () {
                            table.ajax.reload();

                            setTimeout( function () {
                                indexes = table.column( 0 ).data().indexOf( chatroom_id );
                                table.$('tr.active-td').removeClass('active-td');
                                $(table.rows(indexes).nodes()).addClass('active-td');
                            }, 200 );
                        }, 200 );
                    }
                });
                document.getElementById("message").value = "";
            }
                
        }
    });
}
function addChat(chatroom_id,user_id)
{
    var chat ="";
    $.ajax({
        type: 'POST',
        data: {
            chatroom_id:chatroom_id,
            user_id:user_id
        },
        url: './Notifications/setChatDataForTable',
        success: function (response) {
            $("#chat_table").remove();
            $("#main_chat_table").append("<div class=\"chat-activity-list\" id=\"chat_table\"></div>");
            for (i = 0; i < response.length; i++) 
            {
                if(response[i].user_profile_pic_url)
                    image = "/dasta_thailand/assets/img/uploadfile/"+response[i].user_profile_pic_url;
                else
                    image = "/dasta_thailand/assets/img/account_avatar.jpg";

                chat ="";
                chat +="<div class=\"chat-element "+(response[i].User_user_id == user_id? 'right' :'')+"\">";
                chat +="<a href=\"#\" class=\"float-"+(response[i].User_user_id == user_id? 'right' :'left')+"\"><img alt=\"image\" class=\"rounded-circle\" src=\""+image+"\"></a>";
                chat +="<div class=\"media-body "+(response[i].User_user_id == user_id? 'text-right' :'')+"\" style=\"word-wrap: break-word;white-space: pre-wrap;white-space: -pre-wrap;white-space: -o-pre-wrap;word-wrap: break-word;\">";
                chat +="<strong>"+response[i].user_firstName+" "+response[i].user_lastName+"</strong>";
                chat +="<p class=\"m-b-xs \">"+response[i].message+"</p>";
                chat +="</div>";
                chat +="</div>";
                $("#chat_table").append(chat); 
            }

            var scrollTo_val = $('#messagelist').prop('scrollHeight') + 'px';
            $('#messagelist').slimScroll({ 
                scrollTo : scrollTo_val,
                height: '620px',
                start: 'bottom',
                alwaysVisible: true
            });
        }
    });
  
}