$(document).ready(function () {
	$("#sidebar_PermissionGroup_a").click();
	document.getElementById("sidebar_UserManagement").classList.add("active");

	var table = $("#UserManagementTable").DataTable({
		responsive: true,
		ajax: {
			url: host + "UserManagement/getDataUserManagement",
			type: "GET",
		},
		stateSave: true,
		order: [[1, "asc"]],
	});
	table
		.on("order.dt search.dt", function () {
			table
				.column(0, { search: "applied", order: "applied" })
				.nodes()
				.each(function (cell, i) {
					cell.innerHTML = i + 1;
				});
		})
		.draw();

	$(document).on("click", "#UserManagementAdd", function () {
		$("#user_email").prop("disabled", false);
		$("#user_password").prop("disabled", false);
		$("#user_passwordcon").prop("disabled", false);

		$("#btn_save").removeClass("btn_edit_UserManagement");
		$("#btn_save").addClass("btn_add_UserManagement");
		document.getElementById("btn_save").value = "add";
		document.getElementById("UserManagementModalLabel").innerHTML =
			"เพิ่มผู้ดูแลระบบ";
		resetValue();
	});

	$(document).on("change", "#UserRole_userRole_id", function (e) {
		var select = $("option:selected", this);
		switch (select.val()) {
			case "2":
				$("#box_ProvinceGroup").removeClass("invisible");
				$.ajax({
					type: "post",
					url: host + "UserManagement/getProvinceGroup",
					success: function (response) {
						$("#ProvinceGroup_pg_id").empty();
						$("#ProvinceGroup_pg_id").append(response);
						$("#ProvinceGroup_pg_id").trigger("chosen:updated");
					},
				});
				$("#box_province").addClass("invisible");
				$("#province").empty();
				break;
			case "3":
				$("#box_ProvinceGroup").removeClass("invisible");
				$.ajax({
					type: "post",
					url: host + "UserManagement/getProvinceGroup",
					success: function (response) {
						$("#ProvinceGroup_pg_id").empty();
						$("#ProvinceGroup_pg_id").append(response);
						$("#ProvinceGroup_pg_id").trigger("chosen:updated");
					},
				});
				break;

			default:
				$("#box_ProvinceGroup").addClass("invisible");
				$("#box_province").addClass("invisible");
				$("#ProvinceGroup_pg_id").empty();
				$("#province").empty();
				break;
		}
	});

	$(document).on("change", "#ProvinceGroup_pg_id", function (e) {
		var select = $("option:selected", this);
		if (parseInt($("#UserRole_userRole_id").val()) === 3) {
			if (select.val() != "") {
				$("#box_province").removeClass("invisible");

				$.ajax({
					type: "post",
					data: {
						ProvinceGroup_pg_id: select.val(),
					},
					url: host + "UserManagement/getProvince",
					success: function (response) {
						$("#province").empty();
						$("#province").append(response);
						$("#province").trigger("chosen:updated");
					},
				});
			} else {
				$("#box_province").addClass("invisible");

				$("#province").empty();
			}
		}

	});

	$(document).on("click", ".DisabledUser", function () {
		var confirms = confirm(
			"Select Yes below if you are ready to " +
			(this.value.split("_")[1] == 0 ? "Disabled" : "Enabled") +
			" user"
		);
		if (confirms == true) {
			$.ajax({
				type: "POST",
				url: host + "/management/UserEvent/DisabledUser",
				data: {
					user_id: this.value.split("_")[0],
					state: this.value.split("_")[1],
				},

				success: function (response) {
					table.ajax.reload();

				},
			});
		}
	});

	$(document).on("click", ".btn-dal-usermanagement", function () {
		var confirms = confirm(
			"Select Yes below if you are ready to delete your data"
		);
		if (confirms == true) {
			$.ajax({
				type: "POST",
				url: host + "/management/UserEvent/DeleteUser",
				data: { user_id: this.value },

				success: function (response) {
					table.ajax.reload();

				},
			});
		}
	});

	$(document).on("click", ".btn-edit-usermanagement", function () {
		$("#user_email").prop("disabled", true);
		$("#user_password").prop("disabled", true);
		$("#user_passwordcon").prop("disabled", true);

		document.getElementById("UserManagementModalLabel").innerHTML =
			"แก้ไขข้อมูลผู้ดูแลระบบ";
		$("#btn_save").addClass("btn_edit_UserManagement");
		$("#btn_save").removeClass("btn_add_UserManagement");
		document.getElementById("btn_save").value = "edit";
		document.getElementById("user_id").value = this.value;

		$.ajax({
			type: "POST",
			url: host + "UserManagement/getDataUser",
			data: {
				user_id: this.value,
			},
			success: function (response) {
				resetValue(response.UserRole_userRole_id);
				document.getElementById("user_email").value = response.user_email;
				document.getElementById("user_firstName").value =
					response.user_firstName;
				document.getElementById("user_lastName").value = response.user_lastName;
				document.getElementById("user_gender").value = response.user_gender;
				document.getElementById("user_age").value = response.user_age;
				document.getElementById("user_phone").value = response.user_phone;
				if (parseInt(response.UserRole_userRole_id) == 2) {
					$("#box_ProvinceGroup").removeClass("invisible");
					$.ajax({
						type: "post",
						url: host + "UserManagement/getProvinceGroup",
						success: function (res) {
							$("#ProvinceGroup_pg_id").empty();
							$("#ProvinceGroup_pg_id").append(res);
							$("#ProvinceGroup_pg_id").val(response.ProvinceGroup_pg_id);
							$("#ProvinceGroup_pg_id").trigger("chosen:updated");
						},
					});
				}
				if (parseInt(response.UserRole_userRole_id) == 3) {
					$("#box_ProvinceGroup").removeClass("invisible");
					$.ajax({
						type: "post",
						url: host + "UserManagement/getProvinceGroup",
						success: function (res) {
							$("#ProvinceGroup_pg_id").empty();
							$("#ProvinceGroup_pg_id").append(res);
							$("#ProvinceGroup_pg_id").val(response.ProvinceGroup_pg_id);
							$("#ProvinceGroup_pg_id").trigger("chosen:updated");
						},
					});
					$("#box_province").removeClass("invisible");

					$.ajax({
						type: "post",
						data: {
							ProvinceGroup_pg_id: response.ProvinceGroup_pg_id,
						},
						url: host + "UserManagement/getProvince",
						success: function (res) {
							$("#province").empty();
							$("#province").append(res);
							$("#province").val(response.ppg_id);
							$("#province").trigger("chosen:updated");
						},
					});
				}
			},
		});
		$("#UserManagementModal").modal("show");
	});

	$(document).on("click", "#btn_save", function () {
		resetNotification();
		var formData = new FormData();
		var uri = "";
		if (this.value == "add") {
			formData.append("action", "add");
			uri = host + "UserManagement/addNewUser";

			formData.append("user_password", $("#user_password").val());
			formData.append("user_passwordcon", $("#user_passwordcon").val());
		}
		if (this.value == "edit") {
			uri = host + "UserManagement/editNewUser";
			formData.append("user_id", $("#user_id").val());
			formData.append("action", "edit");
		}
		formData.append("user_email", $("#user_email").val());

		formData.append("user_firstName", $("#user_firstName").val());
		formData.append("user_lastName", $("#user_lastName").val());
		formData.append("user_gender", $("#user_gender").val());
		formData.append("user_age", $("#user_age").val());
		formData.append("user_phone", $("#user_phone").val());
		formData.append("UserRole_userRole_id", $("#UserRole_userRole_id").val());
		if ($("#UserRole_userRole_id").val() == "2") {
			formData.append("ProvinceGroup_pg_id", $("#ProvinceGroup_pg_id").val());
		}
		if ($("#UserRole_userRole_id").val() == "3") {
			formData.append("ProvinceGroup_pg_id", $("#ProvinceGroup_pg_id").val());
			formData.append("province", $("#province").val());
		}

		// for (var value of formData.values()) {
		// 	console.log(value);
		// }

		$.ajax({
			type: "POST",
			url: uri,
			data: formData,
			contentType: false,
			processData: false,
			success: function (response) {
				if (response.state) {
					var element = document.getElementById(
						"notifi_error_save_UserManagement"
					);
					element.style.color = "green";
					$("#notifi_error_save_UserManagement").css(
						"display",
						"inline",
						"important"
					);
					element.innerHTML = response.msg;
					table.ajax.reload();
					$("#UserManagementModal").modal("hide");
				} else {
					$("#save").prop("disabled", false);
					if (response.error == "validation") {
						msg = response.msg.replace(/<p>The /g, "");
						msg = msg.replace(/ field is required.<\/p>/g, "");
						msg = msg.replace(/. field does not match the required/g, "");
						msg = msg.replace(/ field must contain a valid email address/g, "");
						msg = msg.replace(
							/ field does not match the user_password field/g,
							""
						);
						msg = msg.replace(/<p>This /g, "");
						// msg = msg.replace(/ has already been used/g,"");
						msg = msg.replace(/.<\/p>/g, "");
						msg = msg.split("\n");
						msg.forEach((element, index) => {
							var ids = element.split("|");
							if (ids[0] == "required") {
								SetNavLinkTabMainActive();
								var element = document.getElementById("notifi_" + ids[2]);
								element.style.color = "red";
								$("#notifi_" + ids[2]).css("display", "inline", "important");
								$("#notifi_" + ids[2]).text(ids[3]);
								document.getElementById(ids[1]).focus();
							}
						});
					} else {
						var element = document.getElementById(
							"notifi_error_save_UserManagement"
						);
						element.style.color = "red";
						$("#notifi_error_save_UserManagement").css(
							"display",
							"inline",
							"important"
						);
						element.innerHTML = response.msg;
					}
				}
			},
		});
	});
});

function resetValue(UserRole = 0) {
	resetNotification();
	$.ajax({
		type: "post",
		data: { getOption: 219 },
		url: host + "UserManagement/getUserRole",
		success: function (response) {
			$("#UserRole_userRole_id").empty();
			$("#UserRole_userRole_id").append(response);
			if (UserRole != 0) $("#UserRole_userRole_id").val(UserRole);
			$("#UserRole_userRole_id").trigger("chosen:updated");
		},
	});
	document.getElementById("user_email").value = "";
	document.getElementById("user_password").value = "";
	document.getElementById("user_passwordcon").value = "";
	document.getElementById("user_firstName").value = "";
	document.getElementById("user_lastName").value = "";
	document.getElementById("user_gender").value = "";
	document.getElementById("user_age").value = "";
	document.getElementById("user_phone").value = "";
	$("#box_ProvinceGroup").addClass("invisible");
	$("#box_province").addClass("invisible");
	$("#ProvinceGroup_pg_id").empty();
	$("#province").empty();
}
