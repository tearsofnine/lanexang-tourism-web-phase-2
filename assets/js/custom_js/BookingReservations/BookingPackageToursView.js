$(document).ready(function(){
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });
    table = $('#BookingPackageToursTable').DataTable({
        processing: true,
        columnDefs: [
            {
                "targets": [ 0],"visible": false,
                
            }
        ],
        dom: '<"toolbar">frtip',
        fnInitComplete: function (oSettings, response) {
            // here you to do after load data
            //response.recordsTotal is used to get Total count data
            // document.getElementById("header_count_list").innerHTML =response.recordsTotal
            
        },
        // "ajax": {
        //     url : host_entrepreneur+'/BookingOrder/getDataForTable',
        //     type : 'POST',
        //     data :  function ( d ) {
        //         d.state_approv = state_approv;
        //         // d.country = $('#country').val();
        //         // d.provinces =$('#provinces').val();
        //         // d.districts =$('#districts').val();
        //     }, 
        // },
    });
    $(".dataTables_filter").hide();
    $('#CustomSearchBox').keyup(function(){
        table.search($(this).val()).draw() ;
    });
});