$(document).ready(function(){
    document.getElementById("header_table_text").innerHTML ="การอนุมัติ/สถานประกอบกิจการบริการร้านอาหาร"
    
    table = $('#ApprovalTable').DataTable({
        processing: true,
        responsive: true, 
        columnDefs: [
            {
                "targets": [ 0],"visible": false,
                
            }
        ],
        dom: '<"toolbar">frtip',
        fnInitComplete: function (oSettings, response) {
            // here you to do after load data
            //response.recordsTotal is used to get Total count data
            document.getElementById("header_count_list").innerHTML =response.recordsTotal+" รายการ"
            
        },
        "ajax": {
            url : host+'/EntrepreneurApproval/'+MenuName+'/getDataForTable',
            type : 'POST',
            data :  function ( d ) {
                d.find_approval = $('#find_approval').val();
            }, 
        },
    });
    $(".dataTables_filter").hide();
    $('#CustomSearchBox').keyup(function(){
        table.search($(this).val()).draw() ;
    })
    setTimeout( function () {
        var rows = table.rows(0).data();
        if(rows.length !=0)
        {
            document.getElementById("user_content").style.display = "block"
            getUserData(rows[0][0])
        }
        else
        {
            document.getElementById("user_content").style.display = "none"
        }
        //
    }, 500 );
});