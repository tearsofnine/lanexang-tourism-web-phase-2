updateNotifications()
// For Firebase JavaScript SDK v7.20.0 and later, `measurementId` is an optional field
 var firebaseConfig = {
    // apiKey: "AIzaSyCV1zm-pN-u_uPX3mU4kS7TQ4_A0UZAdFg",
    // authDomain: "lanexang-info.firebaseapp.com",
    // databaseURL: "https://lanexang-info.firebaseio.com",
    // projectId: "lanexang-info",
    // storageBucket: "lanexang-info.appspot.com",
    // messagingSenderId: "599248162658",
    // appId: "1:599248162658:web:4c5cbbd24654e03f6fa63b",
    // measurementId: "G-SETZPRY9PW"
    apiKey: "AIzaSyBQTsq90u5Yo1GKvzic1ZS3vHcmV9OjKZs",
    authDomain: "lanexang-info-ca0d2.firebaseapp.com",
    databaseURL: "https://lanexang-info-ca0d2.firebaseio.com",
    projectId: "lanexang-info-ca0d2",
    storageBucket: "lanexang-info-ca0d2.appspot.com",
    messagingSenderId: "247075743210",
    appId: "1:247075743210:web:3fb23ca77d7b8da9bc7e93",
    measurementId: "G-MZS281YXT5"
};
// // Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

// // Retrieve Firebase Messaging object.
// const messaging = firebase.messaging();
// // Add the public key generated from the console here.
const messaging = firebase.messaging();

navigator.serviceWorker.register('/dasta_thailand/firebase-messaging-sw.js').then((registration) => {
  messaging.useServiceWorker(registration);
});

// messaging.usePublicVapidKey("BBp2EagNIzfejOk9WIJ_gk_0XC9jyqfooWD5_fALGUG1Pkq2JLrbDY0sVJ17T416JlmlYotSco-dyegL9mk5wRw");
messaging.usePublicVapidKey("BNCVh5pVHk4yRhRbIJsul84PF90LVvDpE53Hscv6OBRzG_kk4HCElcb5nRn8g_RK8CuzdXOle4YRJlCUlbtuVWs");

messaging.requestPermission().then(function () {
  console.log("Have permission");
  if ('serviceWorker' in navigator) 
  {
    navigator.serviceWorker.register('/dasta_thailand/firebase-messaging-sw.js').then(function(registration) {
      console.log('Registration successful, scope is:', registration.scope);
    }).catch(function(err) {
      console.log('Service worker registration failed, error:', err);
    });
  }
});

messaging.onMessage(function(payload) {

  var obj = JSON.parse( payload.data.data);
  const notificationTitle = 'New Message';
  const notificationOptions = {
    body: obj.body,
    icon: '/dasta_thailand/assets/img/firebase-logo.png'
    
  };

  if (Notification.permission == 'granted')
  {
    navigator.serviceWorker.getRegistration().then(function(reg) {reg.showNotification(notificationTitle,notificationOptions);})
    updateNotifications()
  }
});
// [START refresh_token]
  // Callback fired if Instance ID token is updated.
messaging.onTokenRefresh(() => {
    messaging.getToken().then((refreshedToken) => {
        console.log("Message received. ", refreshedToken);

    }).catch((err) => {
      console.log('Unable to retrieve refreshed token ', err);
      //showToken('Unable to retrieve refreshed token ', err);
    });
});
  // [END refresh_token]

