function NewBoxProducTH(imageform,idImage,nameProduc)
{
    var new_image_box = "";
    new_image_box += '<div class="gallery-product container-image-field form-group-'+imageform+'-image" id="form_group_'+imageform+'_image_'+idImage+'">';
    new_image_box += '<div class="card-poi-'+imageform+'-box" id="poi_'+imageform+'_'+idImage+'">';
    new_image_box += '<div class="col-sm-12">';
    new_image_box += '<div class="owl-carousel owl-theme image_box_image_'+imageform+'_'+idImage+'" id="image_box_image_'+imageform+'_'+idImage+'">';
    new_image_box += '</div>';
    new_image_box += '<div class="text-right" style="margin-top: 10px;">';
    new_image_box += '<button class="btn btn-primary btn-xs add_image_field add_image_field-product" name="image_'+imageform+'_'+idImage+'">เพิ่มรูปภาพ'+nameProduc+'</button>';
    new_image_box += '</div>';
    new_image_box += '</div>';
    new_image_box += '<div class="ibox-content">';
    new_image_box += '<div class="form-group  row">';
    new_image_box += '<div class="col-sm-4">';
    new_image_box += '<label class="col-form-label">ชื่อ'+nameProduc+'</label>';
    new_image_box += '</div>';
    new_image_box += '<div class="col-sm-8">';
    new_image_box += '<input type="text" placeholder="ชื่อ'+nameProduc+'" class="form-control name-'+imageform+'" id="name_'+imageform+'_'+idImage+'">';
    new_image_box += '<input type="hidden" class="temp_id_'+imageform+'" id="temp_id_'+imageform+'_'+idImage+'">';
    new_image_box += '</div>';
    new_image_box += '</div>';
    new_image_box += '<div class="form-group  row">';
    new_image_box += '<div class="col-sm-4">';
    new_image_box += '<label class="col-form-label">ราคา</label>';
    new_image_box += '</div>';
    new_image_box += '<div class="col-sm-8">';
    new_image_box += '<input type="number" placeholder="ราคา" class="form-control price-'+imageform+'" id="price_'+imageform+'_'+idImage+'" value="0" min="0">';
    new_image_box += '</div>';
    new_image_box += '</div>';
    new_image_box += '<div class="form-group  row">';
    new_image_box += '<div class="col-sm-4">';
    new_image_box += '<label class="col-form-label">รายละเอียด'+nameProduc+'</label>';
    new_image_box += '</div>';
    new_image_box += '<div class="col-sm-8">';
    new_image_box += '<textarea class="form-control details-'+imageform+'" placeholder="รายละเอียด'+nameProduc+'" rows="5" id="details_'+imageform+'_'+idImage+'"></textarea>';
    new_image_box += '</div>';
    new_image_box += '</div>';
    if(imageform === "hotelroom")
    {
        new_image_box += '<div class="form-group  row">';
        new_image_box += '<div class="col-sm-4">';
        new_image_box += '<label class="col-form-label">อาหารเช้า</label>';
        new_image_box += '</div>';
        new_image_box += '<div class="col-sm-8 breakfast-'+imageform+'" >';
        new_image_box += '<div class="form-check-inline i-checks">';
        new_image_box += '<label class="form-check-label">';
        new_image_box += '<input type="radio" class="form-check-input radio-breakfast-'+imageform+'" value="1" name="breakfast_'+imageform+'_'+idImage+'"><i></i> มี';
        new_image_box += '</label>';
        new_image_box += '</div>';
        new_image_box += '<div class="form-check-inline i-checks">';
        new_image_box += '<label class="form-check-label">';
        new_image_box += '<input type="radio" class="form-check-input radio-breakfast-'+imageform+'" value="0" name="breakfast_'+imageform+'_'+idImage+'" checked><i></i> ไม่มี';
        new_image_box += '</label>';
        new_image_box += '</div>';
        new_image_box += '</div>';
        new_image_box += '</div>';
    }
    new_image_box += '<div class="text-right" style="margin-top: 10px;">';
    new_image_box += '<button class="btn btn-danger btn-xs del_image_field btn-del-product" name="del_'+imageform+'_'+idImage+'" id="del_'+imageform+'">ลบ'+nameProduc+'</button>';
    new_image_box += '</div>';
    new_image_box += '</div>';
    new_image_box += '</div>';
    new_image_box += '</div>';
    return new_image_box;
    
}
function NewBoxProducENG(imageform,idImage,nameProduc)
{
    var new_image_box = "";
    new_image_box += '<div class="gallery-product container-image-field form-group-Eng-'+imageform+'-image" id="form_group_Eng_'+imageform+'_image_'+idImage+'">';
    new_image_box += '<div class="card-poi-Eng-'+imageform+'-box" id="poi_Eng_'+imageform+'_'+idImage+'">';
    new_image_box += '<div class="col-sm-12">';
    new_image_box += '<div class="owl-carousel owl-theme image_box_Eng_image_'+imageform+'_'+idImage+'" id="image_box_Eng_image_'+imageform+'_'+idImage+'">';
    new_image_box += '</div>';
    new_image_box += '</div>';
    new_image_box += '<div class="ibox-content">';
    new_image_box += '<div class="form-group  row">';
    new_image_box += '<div class="col-sm-4">';
    new_image_box += '<label class="col-form-label">ชื่อ'+nameProduc+'</label>';
    new_image_box += '</div>';
    new_image_box += '<div class="col-sm-8">';
    new_image_box += '<input type="" placeholder="ชื่อ'+nameProduc+'" class="form-control name-Eng-'+imageform+'" id="name_Eng_'+imageform+'_'+idImage+'">';
    new_image_box += '</div>';
    new_image_box += '</div>';
    new_image_box += '<div class="form-group  row">';
    new_image_box += '<div class="col-sm-4">';
    new_image_box += '<label class="col-form-label">ราคา</label>';
    new_image_box += '</div>';
    new_image_box += '<div class="col-sm-8">';
    new_image_box += '<label class="col-form-label price-Eng-'+imageform+'" id="price_Eng_'+imageform+'_'+idImage+'">ราคา</label>';
    new_image_box += '</div>';
    new_image_box += '</div>';
    new_image_box += '<div class="form-group  row">';
    new_image_box += '<div class="col-sm-4">';
    new_image_box += '<label class="col-form-label">รายละเอียด'+nameProduc+'</label>';
    new_image_box += '</div>';
    new_image_box += '<div class="col-sm-8">';
    new_image_box += '<textarea class="form-control details-Eng-'+imageform+'" placeholder="รายละเอียด'+nameProduc+'" rows="5" id="details_Eng_'+imageform+'_'+idImage+'"></textarea>';
    new_image_box += '</div>';
    new_image_box += '</div>';
    new_image_box += '</div>';
    new_image_box += '</div>';
    new_image_box += '</div>';
    return new_image_box;
    
}
function NewBoxProducLAOS(imageform,idImage,nameProduc)
{
    var new_image_box = "";
    new_image_box += '<div class="gallery-product container-image-field form-group-Laos-'+imageform+'-image" id="form_group_Laos_'+imageform+'_image_'+idImage+'">';
    new_image_box += '<div class="card-poi-Laos-'+imageform+'-box" id="poi_Laos_'+imageform+'_'+idImage+'">';
    new_image_box += '<div class="col-sm-12">';
    new_image_box += '<div class="owl-carousel owl-theme image_box_Laos_image_'+imageform+'_'+idImage+'" id="image_box_Laos_image_'+imageform+'_'+idImage+'">';
    new_image_box += '</div>';
    new_image_box += '</div>';
    new_image_box += '<div class="ibox-content">';
    new_image_box += '<div class="form-group  row">';
    new_image_box += '<div class="col-sm-4">';
    new_image_box += '<label class="col-form-label">ชื่อ'+nameProduc+'</label>';
    new_image_box += '</div>';
    new_image_box += '<div class="col-sm-8">';
    new_image_box += '<input type="" placeholder="ชื่อ'+nameProduc+'" class="form-control name-Laos-'+imageform+'" id="name_Laos_'+imageform+'_'+idImage+'">';
    new_image_box += '</div>';
    new_image_box += '</div>';
    new_image_box += '<div class="form-group  row">';
    new_image_box += '<div class="col-sm-4">';
    new_image_box += '<label class="col-form-label">ราคา</label>';
    new_image_box += '</div>';
    new_image_box += '<div class="col-sm-8">';
    new_image_box += '<label class="col-form-label price-Laos-'+imageform+'" id="price_Laos_'+imageform+'_'+idImage+'">ราคา</label>';
    new_image_box += '</div>';
    new_image_box += '</div>';
    new_image_box += '<div class="form-group  row">';
    new_image_box += '<div class="col-sm-4">';
    new_image_box += '<label class="col-form-label">รายละเอียด'+nameProduc+'</label>';
    new_image_box += '</div>';
    new_image_box += '<div class="col-sm-8">';
    new_image_box += '<textarea class="form-control details-Laos-'+imageform+'" placeholder="รายละเอียด'+nameProduc+'" rows="5" id="details_Laos_'+imageform+'_'+idImage+'"></textarea>';
    new_image_box += '</div>';
    new_image_box += '</div>';
    new_image_box += '</div>';
    new_image_box += '</div>';
    new_image_box += '</div>';
    return new_image_box;
    
}
function NewBoxProducCHN(imageform,idImage,nameProduc)
{
    var new_image_box = "";
    new_image_box += '<div class="gallery-product container-image-field form-group-Chinese-'+imageform+'-image" id="form_group_Chinese_'+imageform+'_image_'+idImage+'">';
    new_image_box += '<div class="card-poi-Chinese-'+imageform+'-box" id="poi_Chinese_'+imageform+'_'+idImage+'">';
    new_image_box += '<div class="col-sm-12">';
    new_image_box += '<div class="owl-carousel owl-theme image_box_Chinese_image_'+imageform+'_'+idImage+'" id="image_box_Chinese_image_'+imageform+'_'+idImage+'">';
    new_image_box += '</div>';
    new_image_box += '</div>';
    new_image_box += '<div class="ibox-content">';
    new_image_box += '<div class="form-group  row">';
    new_image_box += '<div class="col-sm-4">';
    new_image_box += '<label class="col-form-label">ชื่อ'+nameProduc+'</label>';
    new_image_box += '</div>';
    new_image_box += '<div class="col-sm-8">';
    new_image_box += '<input type="" placeholder="ชื่อ'+nameProduc+'" class="form-control name-Chinese-'+imageform+'" id="name_Chinese_'+imageform+'_'+idImage+'">';
    new_image_box += '</div>';
    new_image_box += '</div>';
    new_image_box += '<div class="form-group  row">';
    new_image_box += '<div class="col-sm-4">';
    new_image_box += '<label class="col-form-label">ราคา</label>';
    new_image_box += '</div>';
    new_image_box += '<div class="col-sm-8">';
    new_image_box += '<label class="col-form-label price-Chinese-'+imageform+'" id="price_Chinese_'+imageform+'_'+idImage+'">ราคา</label>';
    new_image_box += '</div>';
    new_image_box += '</div>';
    new_image_box += '<div class="form-group  row">';
    new_image_box += '<div class="col-sm-4">';
    new_image_box += '<label class="col-form-label">รายละเอียด'+nameProduc+'</label>';
    new_image_box += '</div>';
    new_image_box += '<div class="col-sm-8">';
    new_image_box += '<textarea class="form-control details-Chinese-'+imageform+'" placeholder="รายละเอียด'+nameProduc+'" rows="5" id="details_Chinese_'+imageform+'_'+idImage+'"></textarea>';
    new_image_box += '</div>';
    new_image_box += '</div>';
    new_image_box += '</div>';
    new_image_box += '</div>';
    new_image_box += '</div>';
    return new_image_box;
    
}
function ApprovalBoxProducImage(imageform,idImage1,idImage2) {
    var new_image_box = "";
    new_image_box += '<div class="gallery-product-item product-item-img  container-image-field-food form-group-'+imageform+'-image item" id="form_group_'+imageform+'_image_'+idImage1+'_'+idImage2+'">';
    new_image_box += '<img src="/dasta_thailand/assets/img/image_placeholder_test.jpg" class="image-'+imageform+'-uploaded image-uploaded-product" id="output_image_'+imageform+'_'+idImage1+'_'+idImage2+'">';
    new_image_box += '</div>';
    return new_image_box;
}
function ApprovalBoxProduc(imageform,idImage,nameProduc) {
    var new_image_box = "";
    new_image_box += '<div class="gallery-product container-image-field form-group-'+imageform+'-image" id="form_group_'+imageform+'_image_'+idImage+'">';
    new_image_box += '<div class="card-poi-'+imageform+'-box" id="poi_'+imageform+'_'+idImage+'">';
    new_image_box += '<div class="col-sm-12">';
    new_image_box += '<div class="owl-carousel owl-theme image_box_image_'+imageform+'_'+idImage+' image_box_image" id="image_box_image_'+imageform+'_'+idImage+'">';
    new_image_box += '</div>';
    new_image_box += '</div>';
    new_image_box += '<div class="ibox-content">';
    new_image_box += '<div class="form-group  row">';
    new_image_box += '<div class="col-sm-4">';
    new_image_box += '<label class="col-form-label">ชื่อ </label>';
    new_image_box += '</div>';
    new_image_box += '<div class="col-sm-8">';
    new_image_box += '<label class="col-form-label" id="product_names'+imageform+'_'+idImage+'">test</label>';
    new_image_box += '</div>';
    new_image_box += '</div>';
    new_image_box += '<div class="form-group  row">';
    new_image_box += '<div class="col-sm-4">';
    new_image_box += '<label class="col-form-label">ราคา</label>';
    new_image_box += '</div>';
    new_image_box += '<div class="col-sm-8">';
    new_image_box += '<label class="col-form-label" id="product_price'+imageform+'_'+idImage+'"></label>';
    new_image_box += '</div>';
    new_image_box += '</div>';
    new_image_box += '<div class="form-group  row">';
    new_image_box += '<div class="col-sm-4">';
    new_image_box += '<label class="col-form-label">รายละเอียด</label>';
    new_image_box += '</div>';
    new_image_box += '<div class="col-sm-8">';
    new_image_box += '<label class="col-form-label" id="product_description'+imageform+'_'+idImage+'"></label>';
    new_image_box += '</div>';
    new_image_box += '</div>';
    if(nameProduc === "hotelroom")
    {
       new_image_box += '<div class="form-group  row">';
       new_image_box += '<div class="col-sm-4">';
       new_image_box += '<label class="col-form-label">อาหารเช้า</label>';
       new_image_box += '</div>';
       new_image_box += '<div class="col-sm-8 breakfast-'+imageform+'" >';
       new_image_box += '<div class="form-check-inline i-checks">';
       new_image_box += '<label class="form-check-label">';
       new_image_box += '<input type="radio" class="form-check-input radio-breakfast-'+imageform+'" value="1" name="breakfast_'+imageform+'_'+idImage+'" ><i></i> มี';
       new_image_box += '</label>';
       new_image_box += '</div>';
       new_image_box += '<div class="form-check-inline i-checks">';
       new_image_box += '<label class="form-check-label">';
       new_image_box += '<input type="radio" class="form-check-input radio-breakfast-'+imageform+'" value="0" name="breakfast_'+imageform+'_'+idImage+'" checked ><i></i> ไม่มี';
       new_image_box += '</label>';
       new_image_box += '</div>';
       new_image_box += '</div>';
       new_image_box += '</div>';
    }
    new_image_box += '</div>';
    new_image_box += '</div>';
    new_image_box += '</div>';
    new_image_box += '</div>';
    return new_image_box;
}