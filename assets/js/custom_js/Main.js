$(document).ready(function(){
  document.getElementById("sidebar_Index").classList.add("active");

  $(".chosen-select").chosen({
    disable_search_threshold: 10,
    search_contains: true,
    width: "100%"
  });
  $.ajax({
    type: 'post',
    url: host+'/Main/getProvincesData',
    success: function (response) {
      $("#provinces").empty();
      $("#provinces").append(response);
      $('#provinces').trigger("chosen:updated");
    }
  });
  new_BarChart()
  new_PieChart()

  $(document).on("change", "#provinces", function () {
    var select = $('option:selected', this);
    var select_str =""
    if(select.val())
      select_str = "p.provinces_id = "+select.val()

      myPieChart.destroy();
      myBarChart.destroy(); 

      chart_pie_name =[];
      chart_pie_value =[];
      chart_pie_color =[];
      chart_bar =[];
      name_chart_bar =[];

    $.ajax({
      type: 'post',
      url: host+'/Main/getCountAllData',
      data: {
        provinces_id:select_str
      },
      success: function (response) {

        document.getElementById("count_item_Attractions").textContent=(response[0].count_item_Attractions);
        document.getElementById("count_item_Restaurant").textContent=(response[0].count_item_Restaurant);
        document.getElementById("count_item_Shopping").textContent=(response[0].count_item_Shopping);
        document.getElementById("count_item_Hotel").textContent=(response[0].count_item_Hotel);
        document.getElementById("count_item_ImportantContactPlaces").textContent=(response[0].count_item_ImportantContactPlaces);
        
        
        for (i = 0; i < response[0].CounProvinces.length; i++) {
          chart_pie_name.push(response[0].CounProvinces[i].provinces_thai);
          chart_pie_value.push(response[0].CounProvinces[i].count_item);
        }

        for (i = 0; i < response[0].CountSubCategory.length; i++) {
          chart_bar.push(response[0].CountSubCategory[i].count_item);
          name_chart_bar.push(response[0].CountSubCategory[i].subcategory_thai);
        }
          
        chart_pie_name.forEach(function(item){
          chart_pie_color.push(getRandomColor());
        });   
        // console.log(chart_bar)
        new_PieChart()
        new_BarChart()
      }
    });

    //myBarChart.reset();

  });

});