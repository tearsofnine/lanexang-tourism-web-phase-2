function getItemsData(items_id)
{
    $.ajax({
        type: "POST",
        url: host+'/management/ItemsEvent/GetItemsData',
        data: {items_id: items_id},
    
        success: function (response) {
            var $owl_old = $(".owl-carousel");
            $owl_old.trigger('destroy.owl.carousel');
            $owl_old.html($owl_old.find('.owl-stage-outer').html()).removeClass('owl-loaded');

            $(".box_data").remove();
            $(".gallery-product-item").remove();
            $(".item").remove();
            $(".container-image-field").remove();
            $(".product-item-img").remove();
            $(".Facilities").remove();

            var header_view_modal = document.getElementById("header_view_modal")
            var box_btn_approve = document.getElementById("box_btn_approve");

            if(box_btn_approve !=null)
            {
                if(parseInt(response[0].items_isActive) == 1)
                    box_btn_approve.style.visibility = "hidden";
                else
                    box_btn_approve.style.visibility = "visible";
            }
               
            if(header_view_modal !=null)
                header_view_modal.textContent= response[0].itmes_topicThai

            document.getElementById("coverItem_paths_Thai").src = "../../assets/img/uploadfile/"+response[0].coverItem_paths;
            document.getElementById("coverItem_paths_English").src = "../../assets/img/uploadfile/"+response[0].coverItem_paths;
            document.getElementById("coverItem_paths_Chinese").src = "../../assets/img/uploadfile/"+response[0].coverItem_paths;
            document.getElementById("coverItem_paths_Laos").src = "../../assets/img/uploadfile/"+response[0].coverItem_paths;

            document.getElementById("itmes_topicThai").textContent= response[0].itmes_topicThai
            document.getElementById("itmes_topicEnglish").textContent= response[0].itmes_topicEnglish
            document.getElementById("itmes_topicChinese").textContent= response[0].itmes_topicChinese
            document.getElementById("itmes_topicLaos").textContent= response[0].itmes_topicLaos
            
            document.getElementById("category_thai").textContent= response[0].category_thai+" / "+ response[0].subcategory_thai
            document.getElementById("category_English").textContent= response[0].category_english+" / "+ response[0].subcategory_english
            document.getElementById("category_Chinese").textContent= response[0].category_chinese+" / "+ response[0].subcategory_chinese
            document.getElementById("category_Laos").textContent= response[0].category_laos+" / "+ response[0].subcategory_laos

            document.getElementById("items_contactThai").textContent= response[0].items_contactThai
            document.getElementById("items_contactEnglish").textContent= response[0].items_contactEnglish
            document.getElementById("items_contactChinese").textContent= response[0].items_contactChinese
            document.getElementById("items_contactLaos").textContent= response[0].items_contactLaos

            document.getElementById("items_phoneThai").textContent= response[0].items_phone
            document.getElementById("items_phoneEnglish").textContent= response[0].items_phone
            document.getElementById("items_phoneLaos").textContent= response[0].items_phone

            document.getElementById("items_emailThai").textContent= response[0].items_email
            document.getElementById("items_emailEnglish").textContent= response[0].items_email
            document.getElementById("items_emailChinese").textContent= response[0].items_email
            document.getElementById("items_emailLaos").textContent= response[0].items_email

            document.getElementById("items_lineThai").textContent= response[0].items_line
            document.getElementById("items_lineEnglish").textContent= response[0].items_line
            document.getElementById("items_lineChinese").textContent= response[0].items_line
            document.getElementById("items_lineLaos").textContent= response[0].items_line

            document.getElementById("items_facebookPageThai").textContent= response[0].items_facebookPage
            document.getElementById("items_facebookPageEnglish").textContent= response[0].items_facebookPage
            document.getElementById("items_facebookPageChinese").textContent= response[0].items_facebookPage
            document.getElementById("items_facebookPageLaos").textContent= response[0].items_facebookPage

            document.getElementById("detail_textThai0").textContent= response[0].Detail[0].detail_textThai
            document.getElementById("detail_textEnglish0").textContent= response[0].Detail[0].detail_textEnglish
            document.getElementById("detail_textChinese0").textContent= response[0].Detail[0].detail_textChinese
            document.getElementById("detail_textLaos0").textContent= response[0].Detail[0].detail_textLaos
            
            document.getElementById("detail_textThai1").textContent= response[0].Detail[1].detail_textThai
            document.getElementById("detail_textEnglish1").textContent= response[0].Detail[1].detail_textEnglish
            document.getElementById("detail_textChinese1").textContent= response[0].Detail[1].detail_textChinese
            document.getElementById("detail_textLaos1").textContent= response[0].Detail[1].detail_textLaos

            document.getElementById("detail_textThai2").textContent= response[0].Detail[2].detail_textThai
            document.getElementById("detail_textEnglish2").textContent= response[0].Detail[2].detail_textEnglish
            document.getElementById("detail_textChinese2").textContent= response[0].Detail[2].detail_textChinese
            document.getElementById("detail_textLaos2").textContent= response[0].Detail[2].detail_textLaos

            var time = response[0].items_timeOpen   +" - " + response[0].items_timeClose;
            var TimePeriodThai = response[0].item_dayOpen_data[0].dayofweek_thai;
            var TimePeriodEnglish = response[0].item_dayOpen_data[0].dayofweek_English;
            var TimePeriodChinese = response[0].item_dayOpen_data[0].dayofweek_Chinese;
            var TimePeriodLaos = response[0].item_dayOpen_data[0].dayofweek_Laos;

            if(response[0].item_dayOpen == response[0].item_dayClose)
            {
                document.getElementById("item_Open_Close_data_Thai").textContent= TimePeriodThai +" "+ time;
                document.getElementById("item_Open_Close_data_English").textContent= TimePeriodEnglish +" "+ time;
                document.getElementById("item_Open_Close_data_Chinese").textContent= TimePeriodChinese +" "+ time;
                document.getElementById("item_Open_Close_data_Laos").textContent= TimePeriodLaos +" "+ time;
            }
            else{
                document.getElementById("item_Open_Close_data_Thai").textContent= response[0].item_dayOpen_data[0].dayofweek_thai +" - "+ response[0].item_dayClose_data[0].dayofweek_thai +" "+ time
                document.getElementById("item_Open_Close_data_English").textContent= response[0].item_dayOpen_data[0].dayofweek_English +" - "+ response[0].item_dayClose_data[0].dayofweek_English +" "+ time
                document.getElementById("item_Open_Close_data_Chinese").textContent= response[0].item_dayOpen_data[0].dayofweek_Chinese +" - "+ response[0].item_dayClose_data[0].dayofweek_Chinese +" "+ time
                document.getElementById("item_Open_Close_data_Laos").textContent= response[0].item_dayOpen_data[0].dayofweek_Laos +" - "+ response[0].item_dayClose_data[0].dayofweek_Laos +" "+ time
            }

            addOnKeyUpMarker(response[0].items_latitude,response[0].items_longitude)

            new_image_box ="";
            new_image_box_English ="";
            new_image_box_Chinese ="";
            new_image_box_Laos ="";
            for(i=0;i<Object.keys(response[0].Detail[0].Photo).length;i++)
            {
                new_image_box += "<div class=\"item\">";
                new_image_box += "<img class=\"d-block w-100\" src=\"../../assets/img/uploadfile/"+response[0].Detail[0].Photo[i].photo_paths+"\" alt=\"\">";
                new_image_box += "</div>";

                new_image_box_English += "<div class=\"item\">";
                new_image_box_English += "<img class=\"d-block w-100\" src=\"../../assets/img/uploadfile/"+response[0].Detail[0].Photo[i].photo_paths+"\" alt=\"\">";
                new_image_box_English += "</div>";

                new_image_box_Chinese += "<div class=\"item\">";
                new_image_box_Chinese += "<img class=\"d-block w-100\" src=\"../../assets/img/uploadfile/"+response[0].Detail[0].Photo[i].photo_paths+"\" alt=\"\">";
                new_image_box_Chinese += "</div>";

                new_image_box_Laos += "<div class=\"item\">";
                new_image_box_Laos += "<img class=\"d-block w-100\" src=\"../../assets/img/uploadfile/"+response[0].Detail[0].Photo[i].photo_paths+"\" alt=\"\">";
                new_image_box_Laos += "</div>";
            }
            $("#photo_detail_textThai0").append(new_image_box);
            $("#photo_detail_textEnglish0").append(new_image_box_English);
            $("#photo_detail_textChinese0").append(new_image_box_Chinese);
            $("#photo_detail_textLaos0").append(new_image_box_Laos);

            new_image_box ="";
            new_image_box_English ="";
            new_image_box_Chinese ="";
            new_image_box_Laos ="";
            for(i=0;i<Object.keys(response[0].Detail[1].Photo).length;i++)
            {
                new_image_box += "<div class=\"box_data\">";
                new_image_box += "<img class=\"d-block w-100\" src=\"../../assets/img/uploadfile/"+response[0].Detail[1].Photo[i].photo_paths+"\" alt=\"\">  ";
                new_image_box += "<br> ";
                new_image_box += "<p><span id=\"\">"+response[0].Detail[1].Photo[i].photo_textThai+"</span></p> ";
                new_image_box += " </div> ";

                new_image_box_English += "<div class=\"box_data\">";
                new_image_box_English += "<img class=\"d-block w-100\" src=\"../../assets/img/uploadfile/"+response[0].Detail[1].Photo[i].photo_paths+"\" alt=\"\">  ";
                new_image_box_English += "<br> ";
                new_image_box_English += "<p><span id=\"\">"+response[0].Detail[1].Photo[i].photo_textEnglish+"</span></p> ";
                new_image_box_English += " </div> ";

                new_image_box_Chinese += "<div class=\"box_data\">";
                new_image_box_Chinese += "<img class=\"d-block w-100\" src=\"../../assets/img/uploadfile/"+response[0].Detail[1].Photo[i].photo_paths+"\" alt=\"\">  ";
                new_image_box_Chinese += "<br> ";
                new_image_box_Chinese += "<p><span id=\"\">"+response[0].Detail[1].Photo[i].photo_textChinese+"</span></p> ";
                new_image_box_Chinese += " </div> ";

                new_image_box_Laos += "<div class=\"box_data\">";
                new_image_box_Laos += "<img class=\"d-block w-100\" src=\"../../assets/img/uploadfile/"+response[0].Detail[1].Photo[i].photo_paths+"\" alt=\"\">  ";
                new_image_box_Laos += "<br> ";
                new_image_box_Laos += "<p><span id=\"\">"+response[0].Detail[1].Photo[i].photo_textLaos+"</span></p> ";
                new_image_box_Laos += " </div> ";

            }
            $("#photo_detail_textThai1").append(new_image_box);
            $("#photo_detail_textEnglish1").append(new_image_box_English);
            $("#photo_detail_textChinese1").append(new_image_box_Chinese);
            $("#photo_detail_textLaos1").append(new_image_box_Laos);

            

            new_image_box ="";
            new_image_box_English ="";
            new_image_box_Chinese ="";
            new_image_box_Laos ="";
            for(i=0;i<Object.keys(response[0].Detail[2].Photo).length;i++)
            {
                new_image_box += "<div class=\"box_data\">";
                new_image_box += "<img class=\"d-block w-100\" src=\"../../assets/img/uploadfile/"+response[0].Detail[2].Photo[i].photo_paths+"\" alt=\"\">  ";
                new_image_box += "<br> ";
                new_image_box += "<p><span id=\"\">"+response[0].Detail[2].Photo[i].photo_textThai+"</span></p> ";
                new_image_box += " </div> ";

                new_image_box_English += "<div class=\"box_data\">";
                new_image_box_English += "<img class=\"d-block w-100\" src=\"../../assets/img/uploadfile/"+response[0].Detail[2].Photo[i].photo_paths+"\" alt=\"\">  ";
                new_image_box_English += "<br> ";
                new_image_box_English += "<p><span id=\"\">"+response[0].Detail[2].Photo[i].photo_textEnglish+"</span></p> ";
                new_image_box_English += " </div> ";

                new_image_box_Chinese += "<div class=\"box_data\">";
                new_image_box_Chinese += "<img class=\"d-block w-100\" src=\"../../assets/img/uploadfile/"+response[0].Detail[2].Photo[i].photo_paths+"\" alt=\"\">  ";
                new_image_box_Chinese += "<br> ";
                new_image_box_Chinese += "<p><span id=\"\">"+response[0].Detail[2].Photo[i].photo_textChinese+"</span></p> ";
                new_image_box_Chinese += " </div> ";

                new_image_box_Laos += "<div class=\"box_data\">";
                new_image_box_Laos += "<img class=\"d-block w-100\" src=\"../../assets/img/uploadfile/"+response[0].Detail[2].Photo[i].photo_paths+"\" alt=\"\">  ";
                new_image_box_Laos += "<br> ";
                new_image_box_Laos += "<p><span id=\"\">"+response[0].Detail[2].Photo[i].photo_textLaos+"</span></p> ";
                new_image_box_Laos += " </div> ";
            }
            $("#photo_detail_textThai2").append(new_image_box);
            $("#photo_detail_textEnglish2").append(new_image_box_English);
            $("#photo_detail_textChinese2").append(new_image_box_Chinese);
            $("#photo_detail_textLaos2").append(new_image_box_Laos);
            
            var $owl = $(".photo_detail_text");

            $owl.owlCarousel({
                
                loop:false,
                margin:10,
                nav:false
            
            });
            
            

            switch (parseInt(response[0].MenuItem_menuItem_id)) {
                case 3:
                case 4:           
                    for(i=0;i<Object.keys(response[0].Product).length;i++)
                    {
                        new_image_box ="";
                        new_image_box_English ="";
                        new_image_box_Chinese ="";
                        new_image_box_Laos ="";

                        new_image_box = ApprovalBoxProduc("Thai",(i+1),"Product"); 
                        new_image_box_English = ApprovalBoxProduc("English",(i+1),"Product");
                        new_image_box_Chinese = ApprovalBoxProduc("Chinese",(i+1),"Product"); 
                        new_image_box_Laos = ApprovalBoxProduc("Laos",(i+1),"Product"); 

                        $("#ProductThai").append(new_image_box);
                        $("#ProductEnglish").append(new_image_box_English);
                        $("#ProductChinese").append(new_image_box_Chinese);
                        $("#ProductLaos").append(new_image_box_Laos);
                        

                        
                        for(j=0;j<Object.keys(response[0].Product[i].PhotoProduct).length;j++)
                        {
                            new_image_box ="";
                            new_image_box_English ="";
                            new_image_box_Chinese ="";
                            new_image_box_Laos ="";
                            
                            new_image_box = ApprovalBoxProducImage("Thai",(i+1),(j+1))
                            new_image_box_English = ApprovalBoxProducImage("English",(i+1),(j+1))
                            new_image_box_Chinese = ApprovalBoxProducImage("Chinese",(i+1),(j+1))
                            new_image_box_Laos = ApprovalBoxProducImage("Laos",(i+1),(j+1))

                            $("#image_box_image_Thai_"+(i+1)).append(new_image_box);
                            $("#image_box_image_English_"+(i+1)).append(new_image_box_English);
                            $("#image_box_image_Chinese_"+(i+1)).append(new_image_box_Chinese);
                            $("#image_box_image_Laos_"+(i+1)).append(new_image_box_Laos);

                            document.getElementById("output_image_Thai_"+(i+1)+"_"+(j+1)).src = "../../assets/img/uploadfile/"+response[0].Product[i].PhotoProduct[j].photoProduct_paths;
                            document.getElementById("output_image_English_"+(i+1)+"_"+(j+1)).src = "../../assets/img/uploadfile/"+response[0].Product[i].PhotoProduct[j].photoProduct_paths;
                            document.getElementById("output_image_Chinese_"+(i+1)+"_"+(j+1)).src = "../../assets/img/uploadfile/"+response[0].Product[i].PhotoProduct[j].photoProduct_paths;
                            document.getElementById("output_image_Laos_"+(i+1)+"_"+(j+1)).src = "../../assets/img/uploadfile/"+response[0].Product[i].PhotoProduct[j].photoProduct_paths;
                            
                        }
                        document.getElementById("product_namesThai_"+(i+1)).textContent= response[0].Product[i].product_namesThai
                        document.getElementById("product_namesEnglish_"+(i+1)).textContent= response[0].Product[i].product_namesEnglish
                        document.getElementById("product_namesChinese_"+(i+1)).textContent= response[0].Product[i].product_namesChinese
                        document.getElementById("product_namesLaos_"+(i+1)).textContent= response[0].Product[i].product_namesLaos

                        document.getElementById("product_priceThai_"+(i+1)).textContent= response[0].Product[i].product_price
                        document.getElementById("product_priceEnglish_"+(i+1)).textContent= response[0].Product[i].product_price
                        document.getElementById("product_priceChinese_"+(i+1)).textContent= response[0].Product[i].product_price
                        document.getElementById("product_priceLaos_"+(i+1)).textContent= response[0].Product[i].product_price

                        document.getElementById("product_descriptionThai_"+(i+1)).textContent= response[0].Product[i].product_descriptionThai
                        document.getElementById("product_descriptionEnglish_"+(i+1)).textContent= response[0].Product[i].product_descriptionEnglish
                        document.getElementById("product_descriptionChinese_"+(i+1)).textContent= response[0].Product[i].product_descriptionChinese
                        document.getElementById("product_descriptionLaos_"+(i+1)).textContent= response[0].Product[i].product_descriptionLaos
                       
                    }
                    var $owlimg = $(".image_box_image");
                    $owlimg.owlCarousel({
                        autoWidth:true,
                        loop:false,
                        margin:-10,
                        nav:false
                    });
                    if(parseInt(response[0].MenuItem_menuItem_id) == 3)
                    {
                        new_image_box ="";
                        new_image_box_English ="";
                        new_image_box_Chinese ="";
                        new_image_box_Laos ="";

                        for(i=0;i<Object.keys(response[0].Delicious).length;i++)
                        {
                            new_image_box += "<div class=\"item\">";
                            new_image_box += "<img class=\"gallery-product-item product-item-img  container-image-field-food\" src=\"../../assets/img/"+response[0].Delicious[i].deliciousGuarantee_paths+"\" alt=\"\">  ";
                            new_image_box += "<br> ";
                            new_image_box += "<p><span id=\"\">"+response[0].Delicious[i].deliciousGuarantee_textThai+"</span></p> ";
                            new_image_box += " </div> ";

                            new_image_box_English += "<div class=\"item\">";
                            new_image_box_English += "<img class=\"gallery-product-item product-item-img  container-image-field-food\" src=\"../../assets/img/"+response[0].Delicious[i].deliciousGuarantee_paths+"\" alt=\"\">  ";
                            new_image_box_English += "<br> ";
                            new_image_box_English += "<p><span id=\"\">"+response[0].Delicious[i].deliciousGuarantee_textEnglish+"</span></p> ";
                            new_image_box_English += " </div> ";

                            new_image_box_Chinese += "<div class=\"item\">";
                            new_image_box_Chinese += "<img class=\"gallery-product-item product-item-img  container-image-field-food\" src=\"../../assets/img/"+response[0].Delicious[i].deliciousGuarantee_paths+"\" alt=\"\">  ";
                            new_image_box_Chinese += "<br> ";
                            new_image_box_Chinese += "<p><span id=\"\">"+response[0].Delicious[i].deliciousGuarantee_textChinese+"</span></p> ";
                            new_image_box_Chinese += " </div> ";

                            new_image_box_Laos += "<div class=\"item\">";
                            new_image_box_Laos += "<img class=\"gallery-product-item product-item-img  container-image-field-food\" src=\"../../assets/img/"+response[0].Delicious[i].deliciousGuarantee_paths+"\" alt=\"\">  ";
                            new_image_box_Laos += "<br> ";
                            new_image_box_Laos += "<p><span id=\"\">"+response[0].Delicious[i].deliciousGuarantee_textLaos+"</span></p> ";
                            new_image_box_Laos += " </div> ";
                            
                        }
                        $("#DeliciousThai").append(new_image_box);
                        $("#DeliciousEnglish").append(new_image_box_English);
                        $("#DeliciousChinese").append(new_image_box_Chinese);
                        $("#DeliciousLaos").append(new_image_box_Laos);
                        var $owl = $(".image_box_Delicious");
            
                        $owl.owlCarousel({
                            
                            loop:false,
                            margin:10,
                            nav:false
                        
                        });
                    }
                    break;
                case 5:
                    document.getElementById("HotelAccommodationThai").textContent= response[0].HotelAccommodation + " ดาว";
                    document.getElementById("HotelAccommodationEnglish").textContent= response[0].HotelAccommodation + " ดาว";
                    document.getElementById("HotelAccommodationChinese").textContent= response[0].HotelAccommodation + " ดาว";
                    document.getElementById("HotelAccommodationLaos").textContent= response[0].HotelAccommodation + " ดาว";
                    
                    for(i=0;i<Object.keys(response[0].Facilities).length;i++)
                    {
                        new_image_box ="<p class=\"Facilities\">- <span id=\"\">"+response[0].Facilities[i].facilities_textThai+"</span></p> ";
                        new_image_box_English ="<p class=\"Facilities\">- <span id=\"\">"+response[0].Facilities[i].facilities_textEnglish+"</span></p> ";
                        new_image_box_Chinese ="<p class=\"Facilities\">- <span id=\"\">"+response[0].Facilities[i].facilities_textChinese+"</span></p> ";
                        new_image_box_Laos ="<p class=\"Facilities\">- <span id=\"\">"+response[0].Facilities[i].facilities_textLaos+"</span></p> ";
        
                        $("#FacilitiesThai").append(new_image_box);
                        $("#FacilitiesEnglish").append(new_image_box_English);
                        $("#FacilitiesChinese").append(new_image_box_Chinese);
                        $("#FacilitiesLaos").append(new_image_box_Laos);
                    }
                    for(i=0;i<Object.keys(response[0].Room).length;i++)
                    {
                        new_image_box ="";
                        new_image_box_English ="";
                        new_image_box_Chinese ="";
                        new_image_box_Laos ="";

                        new_image_box = ApprovalBoxProduc("Thai",(i+1),"hotelroom"); 
                        new_image_box_English = ApprovalBoxProduc("English",(i+1),"hotelroom"); 
                        new_image_box_Chinese = ApprovalBoxProduc("Chinese",(i+1),"hotelroom"); 
                        new_image_box_Laos = ApprovalBoxProduc("Laos",(i+1),"hotelroom"); 

                        $("#ProductThai").append(new_image_box);
                        $("#ProductEnglish").append(new_image_box_English);
                        $("#ProductChinese").append(new_image_box_Chinese);
                        $("#ProductLaos").append(new_image_box_Laos);

                        for(j=0;j<Object.keys(response[0].Room[i].PhotoRoom).length;j++)
                        {
                            new_image_box ="";
                            new_image_box_English ="";
                            new_image_box_Chinese ="";
                            new_image_box_Laos ="";
                            
                            new_image_box = ApprovalBoxProducImage("Thai",(i+1),(j+1))
                            new_image_box_English = ApprovalBoxProducImage("English",(i+1),(j+1))
                            new_image_box_Chinese = ApprovalBoxProducImage("Chinese",(i+1),(j+1))
                            new_image_box_Laos = ApprovalBoxProducImage("Laos",(i+1),(j+1))

                            $("#image_box_image_Thai_"+(i+1)).append(new_image_box);
                            $("#image_box_image_English_"+(i+1)).append(new_image_box_English);
                            $("#image_box_image_Chinese_"+(i+1)).append(new_image_box_Chinese);
                            $("#image_box_image_Laos_"+(i+1)).append(new_image_box_Laos);

                            document.getElementById("output_image_Thai_"+(i+1)+"_"+(j+1)).src = "../../assets/img/uploadfile/"+response[0].Room[i].PhotoRoom[j].pictureRoom_paths;
                            document.getElementById("output_image_English_"+(i+1)+"_"+(j+1)).src = "../../assets/img/uploadfile/"+response[0].Room[i].PhotoRoom[j].pictureRoom_paths;
                            document.getElementById("output_image_Chinese_"+(i+1)+"_"+(j+1)).src = "../../assets/img/uploadfile/"+response[0].Room[i].PhotoRoom[j].pictureRoom_paths;
                            document.getElementById("output_image_Laos_"+(i+1)+"_"+(j+1)).src = "../../assets/img/uploadfile/"+response[0].Room[i].PhotoRoom[j].pictureRoom_paths;
                            
                        }
                        
                        document.getElementById("product_namesThai_"+(i+1)).textContent= response[0].Room[i].room_topicThai
                        document.getElementById("product_namesEnglish_"+(i+1)).textContent= response[0].Room[i].room_topicEnglish
                        document.getElementById("product_namesChinese_"+(i+1)).textContent= response[0].Room[i].room_topicChinese
                        document.getElementById("product_namesLaos_"+(i+1)).textContent= response[0].Room[i].room_topicLaos

                        document.getElementById("product_priceThai_"+(i+1)).textContent= response[0].Room[i].room_price
                        document.getElementById("product_priceEnglish_"+(i+1)).textContent= response[0].Room[i].room_price
                        document.getElementById("product_priceChinese_"+(i+1)).textContent= response[0].Room[i].room_price
                        document.getElementById("product_priceLaos_"+(i+1)).textContent= response[0].Room[i].room_price

                        document.getElementById("product_descriptionThai_"+(i+1)).textContent= response[0].Room[i].room_descriptionThai
                        document.getElementById("product_descriptionEnglish_"+(i+1)).textContent= response[0].Room[i].room_descriptionEnglish
                        document.getElementById("product_descriptionChinese_"+(i+1)).textContent= response[0].Room[i].room_descriptionChinese
                        document.getElementById("product_descriptionLaos_"+(i+1)).textContent= response[0].Room[i].room_descriptionLaos

                        var radio_Thai = document.getElementsByName('breakfast_Thai_'+(i+1));
                        var radio_English = document.getElementsByName('breakfast_English_'+(i+1));
                        var radio_Chinese = document.getElementsByName('breakfast_Chinese_'+(i+1));
                        var radio_Laos = document.getElementsByName('breakfast_Laos_'+(i+1));
                        for(var k=0; k < radio_Thai.length;k++)
                        {
                            if(radio_Thai[k].value == response[0].Room[i].room_breakfast)
                            {
                                radio_Thai[k].checked = true;
                                radio_English[k].checked = true;
                                radio_Chinese[k].checked = true;
                                radio_Laos[k].checked = true;
                            }  
                            else
                            {
                                radio_Thai[k].disabled =  true;
                                radio_English[k].disabled =  true;
                                radio_Chinese[k].disabled =  true;
                                radio_Laos[k].disabled =  true;
                            }
                        }
                        
                    }
                    var $owlimg = $(".image_box_image");
                    $owlimg.owlCarousel({
                        autoWidth:true,
                        loop:false,
                        margin:-10,
                        nav:false
                    });

                    $('.i-checks').iCheck({
                        checkboxClass: 'icheckbox_square-green',
                        radioClass: 'iradio_square-green',
                    });
                    //$('.i-checks').iCheck('disable');
                    break;
                default:
                    break;
            }
            var btn_items_approve = document.getElementById("btn_items_approve")
            var btn_items_disapprove = document.getElementById("btn_items_disapprove")
           
            if(btn_items_approve != null)
                btn_items_approve.value= response[0].items_id
            if(btn_items_disapprove != null)
                btn_items_disapprove.value= response[0].items_id
        }
    });
    $('#ApprovalTable tbody').on( 'click', 'tr', function () {
        var rows = table.row( this ).data();
        getItemsData(rows[0])
    });
}