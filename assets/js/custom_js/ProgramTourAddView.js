
$(document).ready(function(){

    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });
    
    document.getElementById('image_crop_img').addEventListener('crop', (event) => {

        CropData = cropper.getData()
        CropBoxData = cropper.getCropBoxData()
        CanvasData = cropper.getCanvasData()
    });
    $.ajax({
        type: 'post',
        data: { getOption: 1},
        url: host+'/ProgramTour/getNameCategorySubCategory',
        success: function (response) {
          
            $("#subcategory").empty();
            $("#subcategory").append(response);
            $("#subcategory").trigger("chosen:updated");
        }
    }); 

    $(document).on("click", "#save", function () {
        $("#save").prop("disabled", true);
        resetNotification();
        cropper.getCroppedCanvas().toBlob((blob) =>{
            var formData = new FormData();

            formData.append('cropperImage', blob,'cropperImage.png');
            
            formData.append('originalImage', document.getElementById("inputImage").files[0]);

            formData.append('CropData', JSON.stringify(CropData));
            formData.append('CropBoxData', JSON.stringify(CropBoxData));
            formData.append('CanvasData', JSON.stringify(CanvasData));

            formData.append('programTour_nameThai',$("#programTour_nameThai").val());
            formData.append('programTour_nameEnglish',$("#programTour_nameEnglish").val());
            formData.append('programTour_nameLaos',$("#programTour_nameLaos").val());
            formData.append('programTour_nameChinese',$("#programTour_nameChinese").val());

            formData.append('programTour_time_period_start',$("#start").val());
            formData.append('programTour_time_period_end',$("#end").val());
            formData.append('subcategory',$("#subcategory").val());

            formData.append('programTour_interestingThingsThai',$("#programTour_interestingThingsThai").val());
            formData.append('programTour_interestingThingsEnglish',$("#programTour_interestingThingsEnglish").val());
            formData.append('programTour_interestingThingsLaos',$("#programTour_interestingThingsLaos").val());
            formData.append('programTour_interestingThingsChinese',$("#programTour_interestingThingsChinese").val());
            
            formData.append('ProgramTourCount',array_dates_ProgramTour.length);
            
            for (var i = 0; i < array_dates_ProgramTour.length; i++) {
                var datesTrip_topic_ProgramTour =[];
                datesTrip_topic_ProgramTour.push($("#datesTrip_topicThai_ProgramTour_"+(i+1)).val())
                datesTrip_topic_ProgramTour.push($("#datesTrip_topicEnglish_ProgramTour_"+(i+1)).val())
                datesTrip_topic_ProgramTour.push($("#datesTrip_topicChinese_ProgramTour_"+(i+1)).val())
                datesTrip_topic_ProgramTour.push($("#datesTrip_topicLaos_ProgramTour_"+(i+1)).val())

                formData.append('datesTrip_topic_ProgramTour_'+(i+1),JSON.stringify(datesTrip_topic_ProgramTour));
                var Itemid = [];
                for (var j = 0; j < array_dates_ProgramTour[i].Itemid.length; j++) {
                    var detail_item =[]
                    detail_item.push($("#detail_Thai_item_ProgramTour_"+(i+1)+"_"+(j+1)).val())
                    detail_item.push($("#detail_English_item_ProgramTour_"+(i+1)+"_"+(j+1)).val())
                    detail_item.push($("#detail_Chinese_item_ProgramTour_"+(i+1)+"_"+(j+1)).val())
                    detail_item.push($("#detail_Laos_item_ProgramTour_"+(i+1)+"_"+(j+1)).val())
                    
                    for (var k = 0; k < array_dates_ProgramTour[i].Itemid[j].numImgItem; k++) {
                        formData.append("img_detail_item_ProgramTour_"+(i+1)+"_"+(j+1)+"_"+(k+1),document.getElementById("img_detail_item_ProgramTour_"+(i+1)+"_"+(j+1)+"_fname_"+(k+1)).files[0]);

                        formData.append("photoTourist_topicThai_detail_item_ProgramTour_"+(i+1)+"_"+(j+1)+"_"+(k+1),$("#photoTourist_topicThai_detail_item_ProgramTour_"+(i+1)+"_"+(j+1)+"_"+(k+1)).val())
                        formData.append("photoTourist_topicEnglish_detail_item_ProgramTour_"+(i+1)+"_"+(j+1)+"_"+(k+1),$("#photoTourist_topicEnglish_detail_item_ProgramTour_"+(i+1)+"_"+(j+1)+"_"+(k+1)).val())
                        formData.append("photoTourist_topicChinese_detail_item_ProgramTour_"+(i+1)+"_"+(j+1)+"_"+(k+1),$("#photoTourist_topicChinese_detail_item_ProgramTour_"+(i+1)+"_"+(j+1)+"_"+(k+1)).val())
                        formData.append("photoTourist_topicLaos_detail_item_ProgramTour_"+(i+1)+"_"+(j+1)+"_"+(k+1),$("#photoTourist_topicLaos_detail_item_ProgramTour_"+(i+1)+"_"+(j+1)+"_"+(k+1)).val())
                        
                    } 
                    var data_item = {
                        ItemId :array_dates_ProgramTour[i].Itemid[j].place,
                        detail :detail_item,
                        numImgItem :array_dates_ProgramTour[i].Itemid[j].numImgItem
                    }
                    Itemid.push(data_item)
                }
                formData.append('tourist_attractions_data_'+(i+1),JSON.stringify(Itemid)); 
            }

            $.ajax({
                type: "POST",
                url: host+'/'+MenuName+'/saveNewProgramTour',
                data: formData,
                contentType: false,
                processData:false,
                success: function (response) {
                   
                    if(response.state)
                    {
                        var element = document.getElementById("notifi_error_save");
                        element.style.color = "green";
                        $("#notifi_error_save").css('display', 'inline', 'important');
                        element.innerHTML = response.msg;
                        window.location.href ='/dasta_thailand/html/'+MenuName;
                    }
                    else
                    {
                        $("#save").prop("disabled", false);
                        if(response.error == 'validation')
                        { 
                            msg = response.msg.replace(/<p>The /g, "");  
                            msg = msg.replace(/ field is required.<\/p>/g,"");
                            msg =msg.split('\n');
                            msg.forEach((element, index) => {
                                    var ids = element.split('|');
                                    if(ids[0] == "required")
                                    {
                                        SetNavLinkTabMainActive();
                                        var element = document.getElementById("notifi_"+ids[2]);
                                        element.style.color = "red";
                                        $("#notifi_"+ids[2]).css('display', 'inline', 'important');
                                        $('#notifi_'+ids[2]).text(ids[3]);
                                        document.getElementById(ids[1]).focus();
                                    }
                            });
                        }
                        else
                        {
                            var element = document.getElementById("notifi_error_save");
                            element.style.color = "red";
                            $("#notifi_error_save").css('display', 'inline', 'important');
                            element.innerHTML = response.msg;
                        }
                    }
                }
            });
            
        });
    });
   
});