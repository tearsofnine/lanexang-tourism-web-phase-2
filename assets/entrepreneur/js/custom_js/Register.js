$(document).ready(function(){
    const input = document.getElementById("pac-input");
    const searchBox = new google.maps.places.SearchBox(input);
    searchMapsBox(searchBox)

    $(document).on("click", "#get_current_location", function () {
        document.getElementById("get_current_location").disabled = true;
        document.getElementById("get_current_location").innerHTML = "Processing...";
        if ("geolocation" in navigator){
            navigator.geolocation.getCurrentPosition(function(position){ 
                var currentLatitude = position.coords.latitude;
                var currentLongitude = position.coords.longitude;
    
                document.getElementById("business_latitude").value = currentLatitude;
                document.getElementById("business_longitude").value = currentLongitude;
                
                removeMarkers()
                
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                    
                  };
      
                map = new google.maps.Map(document.getElementById('map'), {
                    center: pos,
                    zoom: 16,
                    fullscreenControl:false
                });  

                let marker = new google.maps.Marker({
                    map: map,
                    position: new google.maps.LatLng(currentLatitude, currentLongitude),
                    draggable: true,
                });
            
                gmarkers.push(marker);

                document.getElementById("get_current_location").disabled = false;
                document.getElementById("get_current_location").innerHTML = "ตำแหน่งปัจจุบัน";
            });
        }
    });

    $.ajax({
        type: 'post',
        url: host_entrepreneur+'/Register/getNameCategory',
        success: function (response) {
          $("#namecategory").empty();
          $("#namecategory").append(response);
          $('#namecategory').trigger("chosen:updated");
        }
    });

    $("#user_password_re").map(function() {return $(this).attr('id');}).get().forEach((element, index) => { 
        // setInputFilter(document.getElementById(element), function(value) {return /^[a-zA-Z0-9]*$/i.test(value); });
        ["input", "keydown", "keyup", "select"].forEach(function(event) {
            document.getElementById(element).addEventListener(event, function() {
                $("#notifi_user_password_re").css('display', 'none', 'important');
                if (this.value != document.getElementById("user_password").value  ) {
                    var element = document.getElementById("notifi_user_password_re");
                    element.style.color = "red";
                    $("#notifi_user_password_re").css('display', 'inline', 'important');
                    $("#notifi_user_password_re").text("Confirmation password do not match.");
                }
               
            });
        });
    });
    $("#user_account").map(function() {return $(this).attr('id');}).get().forEach((element, index) => { 
        setInputFilter(document.getElementById(element), function(value) {return /^[a-zA-Z0-9]*$/i.test(value); });
        ["input", "keydown", "keyup", "select"].forEach(function(event) {
            document.getElementById(element).addEventListener(event, function() {
                $("#notifi_user_account").css('display', 'none', 'important');
                //resetNotification();
                if (this.value.length < 5  ) {
                    var element = document.getElementById("notifi_user_account");
                    element.style.color = "red";
                    $("#notifi_user_account").css('display', 'inline', 'important');
                    $("#notifi_user_account").text("The username is the wrong length.");
                }
                if(this.value.length >= 5)//check username on server
                {

                }
            });
        });
        
    });
    $("#btn_success").click(function(){
        window.location.href ='/dasta_thailand/entrepreneur/';
    });
   

    $(document).on("click", "#save", function () {
        $("#save").prop("disabled", true);
        resetNotification();
        var formData = new FormData();

        formData.append('business_nameThai',$("#business_nameThai").val());
        formData.append('business_nameEnglish',$("#business_nameEnglish").val());
        formData.append('business_nameChinese',$("#business_nameChinese").val());
        formData.append('business_nameLaos',$("#business_nameLaos").val());

        formData.append('namecategory',$("#namecategory").val());

        formData.append('user_presentAddress',$("#user_presentAddress").val());

        formData.append('country',$("#country").val());
        formData.append('provinces',$("#provinces").val());
        formData.append('districts',$("#districts").val());
        formData.append('subdistricts',$("#subdistricts").val());

        formData.append('business_latitude',$("#business_latitude").val());
        formData.append('business_longitude',$("#business_longitude").val());

        formData.append('business_phone',$("#business_phone").val());
        formData.append('business_www',$("#business_www").val());
        formData.append('business_facebook',$("#business_facebook").val());
        formData.append('business_line',$("#business_line").val());
        formData.append('user_email',$("#user_email").val());
        formData.append('business_operator_number',$("#business_operator_number").val());
        
        formData.append('business_license_paths',document.getElementById("business_license_paths").files[0]);
        formData.append('user_profile_pic_url',document.getElementById("user_profile_pic_url").files[0]);

        formData.append('user_firstName',$("#user_firstName").val());
        formData.append('user_lastName',$("#user_lastName").val());
        formData.append('user_phone',$("#user_phone").val());
        formData.append('user_account',$("#user_account").val());
        formData.append('user_password',$("#user_password").val());
        formData.append('user_password_re',$("#user_password_re").val());

        // for (var pair of formData.entries()) {
        //     console.log(pair[0]+ ', ' + pair[1]); 
        // }
        if(checktypefile() == 0)
        {
            $.ajax({
                type: "POST",
                url: host_entrepreneur+'/Register/Registration',
                data: formData,
                contentType: false,
                processData:false,
                success: function (response) {
                   
                    console.log(response)
                    if(response.state)
                    {
                        var element = document.getElementById("notifi_error_save");
                        element.style.color = "green";
                        $("#notifi_error_save").css('display', 'inline', 'important');
                        element.innerHTML = response.msg;
                        $("#Modalregister").modal({
                            backdrop: 'static',
                            keyboard: false
                        });
                       
                    }
                    else
                    {
                        $("#save").prop("disabled", false);
                        if(response.error == 'validation')
                        { 
                            msg = response.msg.replace(/<p>The /g, "");  
                            msg = msg.replace(/ field is required.<\/p>/g,"");
                            msg = msg.replace(/. field does not match the required/g,"");
                            msg = msg.replace(/.<\/p>/g,"");
                            msg =msg.split('\n');
                            msg.forEach((element, index) => {
                                    var ids = element.split('|');
                                    if(ids[0] == "required")
                                    {
                                        var element = document.getElementById("notifi_"+ids[2]);
                                        element.style.color = "red";
                                        $("#notifi_"+ids[2]).css('display', 'inline', 'important');
                                        $('#notifi_'+ids[2]).text(ids[3]);
                                        document.getElementById(ids[1]).focus();
                                        
                                    }
                            });
                        }
                        else
                        {
                            var element = document.getElementById("notifi_error_save");
                            element.style.color = "red";
                            $("#notifi_error_save").css('display', 'inline', 'important');
                            element.innerHTML = response.msg;
                        }
                    }
                },
                // beforeSend:function(){
                //     document.getElementById("status").style.display = "";
                //     document.getElementById("preloader").style.display = "";
                // }
            });
        }
        else
        {
            var element = document.getElementById("notifi_error_save");
            element.style.color = "red";
            $("#notifi_error_save").css('display', 'inline', 'important');
            element.innerHTML = "Please select only jpg/png file.";
            $("#save").prop("disabled", false);
            $("#save").prop("disabled", true);
        }
        

    });

    $.ajax({
        type: 'post',
        url: host_entrepreneur+'/Register/getCountry',
        success: function (response) {
          $("#country").empty();
          $("#country").append(response);
          $('#country').trigger("chosen:updated");
        }
      });

    $(document).on('change', '#country', function (e) {
        var select = $('option:selected', this);
        ResetSelect("country")
        if(select.val() != ""){
            var data = {
                getOption: select.val()
              }
            $.ajax({
                type: 'post',
                data: data,
                url: host_entrepreneur+'/Register/getProvinces',
                success: function (response) {
                    $("#provinces").prop("disabled", false);
                    $("#provinces").empty();
                    $("#provinces").append(response);
                    $('#provinces').trigger("chosen:updated");
                }
            });
        }
    });

    $(document).on('change', '#provinces', function (e) {
        var select = $('option:selected', this);
        ResetSelect("provinces")
        if(select.val() != ""){
            var data = {
                getOption: select.val()
              }
            $.ajax({
                type: 'post',
                data: data,
                url: host_entrepreneur+'/Register/getDistricts',
                success: function (response) {
                    $("#districts").prop("disabled", false);
                    $("#districts").empty();
                    $("#districts").append(response);
                    $('#districts').trigger("chosen:updated");
                }
            });
        }
    });
    $(document).on('change', '#districts', function (e) {
        var select = $('option:selected', this);
        ResetSelect("districts")
        if(select.val() != ""){
            var data = {
                getOption: select.val()
              }
            $.ajax({
                type: 'post',
                data: data,
                url: host_entrepreneur+'/Register/getSubdistricts',
                success: function (response) {
                    $("#subdistricts").prop("disabled", false);
                    $("#subdistricts").empty();
                    $("#subdistricts").append(response);
                    $('#subdistricts').trigger("chosen:updated");
                }
            });
        }
    });
});

function ResetSelect(select){
    switch(select) {
        case "country":
            $("#provinces").empty();
            $("#districts").empty();
            $("#subdistricts").empty();

            $("#provinces").prop("disabled", true);
            $("#districts").prop("disabled", true);
            $("#subdistricts").prop("disabled", true);

            $('#provinces').trigger("chosen:updated");
            $('#districts').trigger("chosen:updated");
            $('#subdistricts').trigger("chosen:updated");
          break;
        case "provinces":
            $("#districts").empty();
            $("#subdistricts").empty();

            $("#districts").prop("disabled", true);
            $("#subdistricts").prop("disabled", true);

            
            $('#districts').trigger("chosen:updated");
            $('#subdistricts').trigger("chosen:updated");
          break;
        case "districts":
            $("#subdistricts").empty();
            
            $("#subdistricts").prop("disabled", true);
            
            $('#subdistricts').trigger("chosen:updated");
          break;
    }
}