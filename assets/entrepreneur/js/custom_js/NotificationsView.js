$(document).ready(function(){
    document.getElementById("sidebar_Notifications").classList.add("active");
    table = $('#ListChatTable').DataTable({
        processing: true,
        responsive: false, 
        columnDefs: [
            {
                "targets": [0],"visible": false,
                
            }
        ],
        "ordering": false,
        dom: '<"toolbar">frtip',
        fnInitComplete: function (oSettings, response) {
            // here you to do after load data
            //response.recordsTotal is used to get Total count data
            //document.getElementById("header_count_list").innerHTML =response.recordsTotal+" รายการ"
        },
        // "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            
        //       $('td', nRow).css('background-color', '#F7F7FA');
           
        //   },
        "ajax": {
            url :'/dasta_thailand/entrepreneur/Notifications/getDataForTable',
            type : 'POST',
            data :  function ( d ) {
                d.user_id = user_id;
            }, 
        },
    });
    $(".dataTables_filter").hide();


});
messaging.onMessage(function(payload) {
    
    updateNotifications()
    setTimeout( function () {
        table.ajax.reload();
    }, 200 );
});