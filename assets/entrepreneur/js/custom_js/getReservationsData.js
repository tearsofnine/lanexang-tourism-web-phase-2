function getReservationsData(items_id)
{
    $.ajax({
        type: "POST",
        url: host_entrepreneur+'/management/ItemsEvent/GetItemsData',
        data: {items_id: items_id},
    
        success: function (response) {
            
            $(".TravelDetails").remove();
            $(".BookingCondition").remove();

            new_data_box ="";
            new_data_box_English ="";
            new_data_box_Chinese ="";
            new_data_box_Laos ="";

            var header_view_modal = document.getElementById("header_view_modal")
            if(header_view_modal !=null)
                header_view_modal.textContent= response[0].itmes_topicThai
            
            document.getElementById("itmes_topicThai").textContent= response[0].itmes_topicThai
            document.getElementById("itmes_topicEnglish").textContent= response[0].itmes_topicEnglish
            document.getElementById("itmes_topicChinese").textContent= response[0].itmes_topicChinese
            document.getElementById("itmes_topicLaos").textContent= response[0].itmes_topicLaos
            
            if(Object.keys(response[0].Business).length != 0)
            {  
                document.getElementById("business_nameThai").textContent= response[0].Business[0].business_nameThai
                document.getElementById("business_nameEnglish").textContent= response[0].Business[0].business_nameEnglish
                document.getElementById("business_nameChinese").textContent= response[0].Business[0].business_nameChinese
                document.getElementById("business_nameLaos").textContent= response[0].Business[0].business_nameLaos
            }

            switch (parseInt(response[0].MenuItem_menuItem_id))
             {
                case 9:
                    $("#tbody_hotelThai").empty();
                    $("#tbody_hotelEnglish").empty();
                    $("#tbody_hotelChinese").empty();
                    $("#tbody_hotelLaos").empty();

                    $("#tbody_foodThai").empty();
                    $("#tbody_foodEnglish").empty();
                    $("#tbody_foodChinese").empty();
                    $("#tbody_foodLaos").empty();

                    $("#tbody_TravelPeriodThai").empty();
                    $("#tbody_TravelPeriodEnglish").empty();
                    $("#tbody_TravelPeriodChinese").empty();
                    $("#tbody_TravelPeriodLaos").empty();

                    var $owl_old = $(".owl-carousel");
                    $owl_old.trigger('destroy.owl.carousel');
                    $owl_old.html($owl_old.find('.owl-stage-outer').html()).removeClass('owl-loaded');
                    $(".item").remove();

                    var ScopeAreaThai =[];
                    var ScopeAreaEnglish =[];
                    var ScopeAreaChinese =[];
                    var ScopeAreaLaos =[];
                    for(i=0;i<Object.keys(response[0].ScopeArea).length;i++)
                    {
                        ScopeAreaThai.push(response[0].ScopeArea[i].provinces_thai)
                        
                        ScopeAreaEnglish.push(response[0].ScopeArea[i].provinces_english)
                        ScopeAreaChinese.push(response[0].ScopeArea[i].provinces_chinese)
                        ScopeAreaLaos.push(response[0].ScopeArea[i].provinces_laos)
                    }
                    document.getElementById("ScopeAreaThai").textContent= ScopeAreaThai.join('-')
                    document.getElementById("ScopeAreaEnglish").textContent= ScopeAreaEnglish.join('-')
                    document.getElementById("ScopeAreaChinese").textContent= ScopeAreaChinese.join('-')
                    document.getElementById("ScopeAreaLaos").textContent= ScopeAreaLaos.join('-')

                    new_image_box ="";
                    new_image_box_English ="";
                    new_image_box_Chinese ="";
                    new_image_box_Laos ="";

                    for(i=0;i<Object.keys(response[0].CoverItems).length;i++)
                    {
                        
                        new_image_box += "<div class=\"item\">";
                        new_image_box += "<img width=\"200\" height=\"200\" class=\"d-block w-100\" src=\"../../assets/img/uploadfile/"+response[0].CoverItems[i].coverItem_paths+"\" alt=\"\">";
                        new_image_box += "</div>";

                        new_image_box_English += "<div class=\"item\">";
                        new_image_box_English += "<img  width=\"200\" height=\"200\" class=\"d-block w-100\" src=\"../../assets/img/uploadfile/"+response[0].CoverItems[i].coverItem_paths+"\" alt=\"\">";
                        new_image_box_English += "</div>";

                        new_image_box_Chinese += "<div class=\"item\">";
                        new_image_box_Chinese += "<img  width=\"200\" height=\"200\"class=\"d-block w-100\" src=\"../../assets/img/uploadfile/"+response[0].CoverItems[i].coverItem_paths+"\" alt=\"\">";
                        new_image_box_Chinese += "</div>";

                        new_image_box_Laos += "<div class=\"item\">";
                        new_image_box_Laos += "<img width=\"200\" height=\"200\" class=\"d-block w-100\" src=\"../../assets/img/uploadfile/"+response[0].CoverItems[i].coverItem_paths+"\" alt=\"\">";
                        new_image_box_Laos += "</div>";
                    }
                    $("#owl_demoThai").append(new_image_box);
                    $("#owl_demoEnglish").append(new_image_box_English);
                    $("#owl_demoChinese").append(new_image_box_Chinese);
                    $("#owl_demoLaos").append(new_image_box_Laos);
                    var $owl = $(".owl-carousel");
                    $owl.owlCarousel({
                        utoWidth:true,
                        loop:false,
                        items : 1, 
                    });

                    document.getElementById("travelPeriod_adult_special_price_Thai").textContent= response[0].TravelPeriod[0].travelPeriod_adult_special_price;
                    document.getElementById("travelPeriod_adult_special_price_English").textContent= response[0].TravelPeriod[0].travelPeriod_adult_special_price;
                    document.getElementById("travelPeriod_adult_special_price_Chinese").textContent= response[0].TravelPeriod[0].travelPeriod_adult_special_price;
                    document.getElementById("travelPeriod_adult_special_price_Laos").textContent= response[0].TravelPeriod[0].travelPeriod_adult_special_price;

                    document.getElementById("travel_time_Thai").textContent= Object.keys(response[0].TravelDetails).length;
                    document.getElementById("travel_time_English").textContent= Object.keys(response[0].TravelDetails).length;
                    document.getElementById("travel_time_Chinese").textContent= Object.keys(response[0].TravelDetails).length;
                    document.getElementById("travel_time_Laos").textContent= Object.keys(response[0].TravelDetails).length;

                    document.getElementById("category_Thai").textContent =  response[0].category_thai + " : " +response[0].subcategory_thai;
                    document.getElementById("category_English").textContent =  response[0].category_english + " : " +response[0].subcategory_english;
                    document.getElementById("category_Chinese").textContent =  response[0].category_chinese + " : " +response[0].subcategory_chinese;
                    document.getElementById("category_Laos").textContent =  response[0].category_laos + " : " +response[0].subcategory_laos;

                    for(i=0;i<Object.keys(response[0].TravelDetails).length;i++)
                    {
                        new_data_box += "<div class=\"TravelDetails\">";
                        new_data_box += "<h4>"+response[0].TravelDetails[i].travelDetails_dateThai+"</h4>";
                        new_data_box += "<p>"+response[0].TravelDetails[i].travelDetails_textThai+"</p>";
                        new_data_box += "<hr class=\"hr-line-dashed\">";
                        new_data_box += "</div>";

                        new_data_box_English += "<div class=\"TravelDetails\">";
                        new_data_box_English += "<h4>"+response[0].TravelDetails[i].travelDetails_dateEnglish+"</h4>";
                        new_data_box_English += "<p>"+response[0].TravelDetails[i].travelDetails_textEnglish+"</p>";
                        new_data_box_English += "<hr class=\"hr-line-dashed\">";
                        new_data_box_English += "</div>";                      

                        new_data_box_Chinese += "<div class=\"TravelDetails\">";
                        new_data_box_Chinese += "<h4>"+response[0].TravelDetails[i].travelDetails_dateChinese+"</h4>";
                        new_data_box_Chinese += "<p>"+response[0].TravelDetails[i].travelDetails_textChinese+"</p>";
                        new_data_box_Chinese += "<hr class=\"hr-line-dashed\">";
                        new_data_box_Chinese += "</div>";                     

                        new_data_box_Laos += "<div class=\"TravelDetails\">";
                        new_data_box_Laos += "<h4>"+response[0].TravelDetails[i].travelDetails_dateLaos+"</h4>";
                        new_data_box_Laos += "<p>"+response[0].TravelDetails[i].travelDetails_textLaos+"</p>";
                        new_data_box_Laos += "<hr class=\"hr-line-dashed\">";
                        new_data_box_Laos += "</div>";
                        
                    }
                    $("#TravelDetailsThai").append(new_data_box);
                    $("#TravelDetailsEnglish").append(new_data_box_English);
                    $("#TravelDetailsChinese").append(new_data_box_Chinese);
                    $("#TravelDetailsLaos").append(new_data_box_Laos);
                    
                    var tableHotelRef_Thai = document.getElementById('hotelThai').getElementsByTagName('tbody')[0];
                    var tableHotelRef_English = document.getElementById('hotelEnglish').getElementsByTagName('tbody')[0];
                    var tableHotelRef_Chinese = document.getElementById('hotelChinese').getElementsByTagName('tbody')[0];
                    var tableHotelRef_Laos = document.getElementById('hotelLaos').getElementsByTagName('tbody')[0];

                    var tablefoodRef_Thai = document.getElementById('foodThai').getElementsByTagName('tbody')[0];
                    var tablefoodRef_English = document.getElementById('foodEnglish').getElementsByTagName('tbody')[0];
                    var tablefoodRef_Chinese = document.getElementById('foodChinese').getElementsByTagName('tbody')[0];
                    var tablefoodRef_Laos = document.getElementById('foodLaos').getElementsByTagName('tbody')[0];

                    var tableTravelPeriodRef_Thai = document.getElementById('TravelPeriodThai').getElementsByTagName('tbody')[0];
                    var tableTravelPeriodRef_English = document.getElementById('TravelPeriodEnglish').getElementsByTagName('tbody')[0];
                    var tableTravelPeriodRef_Chinese = document.getElementById('TravelPeriodChinese').getElementsByTagName('tbody')[0];
                    var tableTravelPeriodRef_Laos = document.getElementById('TravelPeriodLaos').getElementsByTagName('tbody')[0];

                    for(i=0;i<Object.keys(response[0].TravelPeriod).length;i++) 
                    {
                        var newRow_TravelPeriodThai   = tableTravelPeriodRef_Thai.insertRow();
                        var newRow_TravelPeriodEnglish   = tableTravelPeriodRef_English.insertRow();
                        var newRow_TravelPeriodChinese   = tableTravelPeriodRef_Chinese.insertRow();
                        var newRow_TravelPeriodLaos   = tableTravelPeriodRef_Laos.insertRow();

                        var newCell1_TravelPeriodThai = newRow_TravelPeriodThai.insertCell(0);
                        var newCell2_TravelPeriodThai = newRow_TravelPeriodThai.insertCell(1);
                        var newCell3_TravelPeriodThai = newRow_TravelPeriodThai.insertCell(2);
                        var newCell4_TravelPeriodThai = newRow_TravelPeriodThai.insertCell(3);
                        var newCell5_TravelPeriodThai = newRow_TravelPeriodThai.insertCell(4);
                        var newCell6_TravelPeriodThai = newRow_TravelPeriodThai.insertCell(5);
                        var newCell7_TravelPeriodThai = newRow_TravelPeriodThai.insertCell(6);

                        var newCell1_TravelPeriodEnglish = newRow_TravelPeriodEnglish.insertCell(0);
                        var newCell2_TravelPeriodEnglish = newRow_TravelPeriodEnglish.insertCell(1);
                        var newCell3_TravelPeriodEnglish = newRow_TravelPeriodEnglish.insertCell(2);
                        var newCell4_TravelPeriodEnglish = newRow_TravelPeriodEnglish.insertCell(3);
                        var newCell5_TravelPeriodEnglish = newRow_TravelPeriodEnglish.insertCell(4);
                        var newCell6_TravelPeriodEnglish = newRow_TravelPeriodEnglish.insertCell(5);
                        var newCell7_TravelPeriodEnglish = newRow_TravelPeriodEnglish.insertCell(6);

                        var newCell1_TravelPeriodChinese = newRow_TravelPeriodChinese.insertCell(0);
                        var newCell2_TravelPeriodChinese = newRow_TravelPeriodChinese.insertCell(1);
                        var newCell3_TravelPeriodChinese = newRow_TravelPeriodChinese.insertCell(2);
                        var newCell4_TravelPeriodChinese = newRow_TravelPeriodChinese.insertCell(3);
                        var newCell5_TravelPeriodChinese = newRow_TravelPeriodChinese.insertCell(4);
                        var newCell6_TravelPeriodChinese = newRow_TravelPeriodChinese.insertCell(5);
                        var newCell7_TravelPeriodChinese = newRow_TravelPeriodChinese.insertCell(6);

                        var newCell1_TravelPeriodLaos = newRow_TravelPeriodLaos.insertCell(0);
                        var newCell2_TravelPeriodLaos = newRow_TravelPeriodLaos.insertCell(1);
                        var newCell3_TravelPeriodLaos = newRow_TravelPeriodLaos.insertCell(2);
                        var newCell4_TravelPeriodLaos = newRow_TravelPeriodLaos.insertCell(3);
                        var newCell5_TravelPeriodLaos = newRow_TravelPeriodLaos.insertCell(4);
                        var newCell6_TravelPeriodLaos = newRow_TravelPeriodLaos.insertCell(5);
                        var newCell7_TravelPeriodLaos = newRow_TravelPeriodLaos.insertCell(6);

                        var newText1_TravelPeriod = (i+1);
                        var newText2_TravelPeriod = response[0].TravelPeriod[i].travelPeriod_amount;
                        var newText3_TravelPeriod = moment(response[0].TravelPeriod[i].travelPeriod_time_period_start).format('MM/DD/YYYY') +" - "+ moment(response[0].TravelPeriod[i].travelPeriod_time_period_end).format('MM/DD/YYYY');
                        var newText4_TravelPeriod = response[0].TravelPeriod[i].travelPeriod_adult_special_price;
                        var newText5_TravelPeriod = response[0].TravelPeriod[i].travelPeriod_adult_price;
                        var newText6_TravelPeriod = response[0].TravelPeriod[i].travelPeriod_child_price;
                        var newText7_TravelPeriod = response[0].TravelPeriod[i].travelPeriod_child_special_price;

                        newCell1_TravelPeriodThai.innerHTML = newText1_TravelPeriod;
                        newCell2_TravelPeriodThai.innerHTML = newText2_TravelPeriod;
                        newCell3_TravelPeriodThai.innerHTML = newText3_TravelPeriod;
                        newCell4_TravelPeriodThai.innerHTML = newText4_TravelPeriod;
                        newCell5_TravelPeriodThai.innerHTML = newText5_TravelPeriod;
                        newCell6_TravelPeriodThai.innerHTML = newText6_TravelPeriod;
                        newCell7_TravelPeriodThai.innerHTML = newText7_TravelPeriod;

                        newCell1_TravelPeriodEnglish.innerHTML = newText1_TravelPeriod;
                        newCell2_TravelPeriodEnglish.innerHTML = newText2_TravelPeriod;
                        newCell3_TravelPeriodEnglish.innerHTML = newText3_TravelPeriod;
                        newCell4_TravelPeriodEnglish.innerHTML = newText4_TravelPeriod;
                        newCell5_TravelPeriodEnglish.innerHTML = newText5_TravelPeriod;
                        newCell6_TravelPeriodEnglish.innerHTML = newText6_TravelPeriod;
                        newCell7_TravelPeriodEnglish.innerHTML = newText7_TravelPeriod;

                        newCell1_TravelPeriodChinese.innerHTML = newText1_TravelPeriod;
                        newCell2_TravelPeriodChinese.innerHTML = newText2_TravelPeriod;
                        newCell3_TravelPeriodChinese.innerHTML = newText3_TravelPeriod;
                        newCell4_TravelPeriodChinese.innerHTML = newText4_TravelPeriod;
                        newCell5_TravelPeriodChinese.innerHTML = newText5_TravelPeriod;
                        newCell6_TravelPeriodChinese.innerHTML = newText6_TravelPeriod;
                        newCell7_TravelPeriodChinese.innerHTML = newText7_TravelPeriod;

                        newCell1_TravelPeriodLaos.innerHTML = newText1_TravelPeriod;
                        newCell2_TravelPeriodLaos.innerHTML = newText2_TravelPeriod;
                        newCell3_TravelPeriodLaos.innerHTML = newText3_TravelPeriod;
                        newCell4_TravelPeriodLaos.innerHTML = newText4_TravelPeriod;
                        newCell5_TravelPeriodLaos.innerHTML = newText5_TravelPeriod;
                        newCell6_TravelPeriodLaos.innerHTML = newText6_TravelPeriod;
                        newCell7_TravelPeriodLaos.innerHTML = newText7_TravelPeriod;
                    }

                    setTimeout( function () {
                        $('#TravelPeriodThai').DataTable({
                            destroy: true,
                            responsive: true, 
                            searching: false,
                            paging: false,
                        }); 
                        $('#TravelPeriodEnglish').DataTable({
                            destroy: true,
                            responsive: true, 
                            searching: false,
                            paging: false,
                        }); 
                        $('#TravelPeriodChinese').DataTable({
                            destroy: true,
                            responsive: true, 
                            searching: false,
                            paging: false,
                        }); 
                        $('#TravelPeriodLaos').DataTable({
                            destroy: true,
                            responsive: true, 
                            searching: false,
                            paging: false,
                        }); 
                
                        
                     }, 500 );

                    for(i=0;i<Object.keys(response[0].TravelDetails).length;i++)
                    {
                        var newRow_hotelThai   = tableHotelRef_Thai.insertRow();

                        var newCell1_hotelThai  = newRow_hotelThai.insertCell(0);
                        var newCell2_hotelThai  = newRow_hotelThai.insertCell(1);

                        var newText1_hotelThai  = document.createTextNode((i+1));
                        var newText2_hotelThai  = document.createTextNode(response[0].TravelDetails[i].travelDetails_hotel_nameThai);

                        newCell1_hotelThai.appendChild(newText1_hotelThai);
                        newCell2_hotelThai.appendChild(newText2_hotelThai);


                        var newRow_hotelEnglish   = tableHotelRef_English.insertRow();
                        
                        var newCell1_hotelEnglish  = newRow_hotelEnglish.insertCell(0);
                        var newCell2_hotelEnglish  = newRow_hotelEnglish.insertCell(1);

                        var newText1_hotelEnglish  = document.createTextNode((i+1));
                        var newText2_hotelEnglish  = document.createTextNode(response[0].TravelDetails[i].travelDetails_hotel_nameEnglish);

                        newCell1_hotelEnglish.appendChild(newText1_hotelEnglish);
                        newCell2_hotelEnglish.appendChild(newText2_hotelEnglish);

                        var newRow_hotelChinese   = tableHotelRef_Chinese.insertRow();
                        
                        var newCell1_hotelChinese  = newRow_hotelChinese.insertCell(0);
                        var newCell2_hotelChinese  = newRow_hotelChinese.insertCell(1);

                        var newText1_hotelChinese  = document.createTextNode((i+1));
                        var newText2_hotelChinese  = document.createTextNode(response[0].TravelDetails[i].travelDetails_hotel_nameChinese);

                        newCell1_hotelChinese.appendChild(newText1_hotelChinese);
                        newCell2_hotelChinese.appendChild(newText2_hotelChinese);

                        var newRow_hotelLaos   = tableHotelRef_Laos.insertRow();
                        
                        var newCell1_hotelLaos  = newRow_hotelLaos.insertCell(0);
                        var newCell2_hotelLaos  = newRow_hotelLaos.insertCell(1);

                        var newText1_hotelLaos  = document.createTextNode((i+1));
                        var newText2_hotelLaos  = document.createTextNode(response[0].TravelDetails[i].travelDetails_hotel_nameLaos);

                        newCell1_hotelLaos.appendChild(newText1_hotelLaos);
                        newCell2_hotelLaos.appendChild(newText2_hotelLaos);

                        // Food //
                        var newRow_foodThai   = tablefoodRef_Thai.insertRow();
                        var newRow_foodEnglish   = tablefoodRef_English.insertRow();
                        var newRow_foodChinese   = tablefoodRef_Chinese.insertRow();
                        var newRow_foodLaos   = tablefoodRef_Laos.insertRow();

                        var newCell1_foodThai = newRow_foodThai.insertCell(0);
                        var newCell2_foodThai = newRow_foodThai.insertCell(1);
                        var newCell3_foodThai = newRow_foodThai.insertCell(2);
                        var newCell4_foodThai = newRow_foodThai.insertCell(3);

                        var newCell1_foodEnglish = newRow_foodEnglish.insertCell(0);
                        var newCell2_foodEnglish = newRow_foodEnglish.insertCell(1);
                        var newCell3_foodEnglish = newRow_foodEnglish.insertCell(2);
                        var newCell4_foodEnglish = newRow_foodEnglish.insertCell(3);

                        var newCell1_foodChinese = newRow_foodChinese.insertCell(0);
                        var newCell2_foodChinese = newRow_foodChinese.insertCell(1);
                        var newCell3_foodChinese = newRow_foodChinese.insertCell(2);
                        var newCell4_foodChinese = newRow_foodChinese.insertCell(3);

                        var newCell1_foodLaos = newRow_foodLaos.insertCell(0);
                        var newCell2_foodLaos = newRow_foodLaos.insertCell(1);
                        var newCell3_foodLaos = newRow_foodLaos.insertCell(2);
                        var newCell4_foodLaos = newRow_foodLaos.insertCell(3);

                        var newText1_food = (i+1);
                        var newText2_food = (parseInt(response[0].TravelDetails[i].travelDetails_food_breakfast) == 1?"<i class=\"fa fa-check-circle text-navy\" style=\"font-size: 20px;\"></i>":"-");
                        var newText3_food = (parseInt(response[0].TravelDetails[i].travelDetails_food_lunch) == 1?"<i class=\"fa fa-check-circle text-navy\" style=\"font-size: 20px;\"></i>":"-");
                        var newText4_food = (parseInt(response[0].TravelDetails[i].travelDetails_food_dinner) == 1?"<i class=\"fa fa-check-circle text-navy\" style=\"font-size: 20px;\"></i>":"-");

                        newCell1_foodThai.innerHTML = newText1_food;
                        newCell2_foodThai.innerHTML = newText2_food;
                        newCell3_foodThai.innerHTML = newText3_food;
                        newCell4_foodThai.innerHTML = newText4_food;

                        newCell1_foodEnglish.innerHTML = newText1_food;
                        newCell2_foodEnglish.innerHTML = newText2_food;
                        newCell3_foodEnglish.innerHTML = newText3_food;
                        newCell4_foodEnglish.innerHTML = newText4_food;

                        newCell1_foodChinese.innerHTML = newText1_food;
                        newCell2_foodChinese.innerHTML = newText2_food;
                        newCell3_foodChinese.innerHTML = newText3_food;
                        newCell4_foodChinese.innerHTML = newText4_food;

                        newCell1_foodLaos.innerHTML = newText1_food;
                        newCell2_foodLaos.innerHTML = newText2_food;
                        newCell3_foodLaos.innerHTML = newText3_food;
                        newCell4_foodLaos.innerHTML = newText4_food;
                    }
 
                    new_data_box ="";
                    new_data_box_English ="";
                    new_data_box_Chinese ="";
                    new_data_box_Laos ="";

                    for(i=0;i<Object.keys(response[0].BookingCondition).length;i++)
                    {
                        new_data_box += "<div class=\"BookingCondition\">";
                        new_data_box += "<h5>"+response[0].BookingCondition[i].bookingConditionCategory_topicThai+"</h5>";
                        new_data_box += "<p>"+response[0].BookingCondition[i].bookingConditioncol_detailThai+"</p>";
                        new_data_box += "<hr class=\"hr-line-dashed\">";
                        new_data_box += "</div>";
                       

                        new_data_box_English += "<div class=\"BookingCondition\">";
                        new_data_box_English += "<h5>"+response[0].BookingCondition[i].bookingConditionCategory_topicEnglish+"</h5>";
                        new_data_box_English += "<p>"+response[0].BookingCondition[i].bookingConditioncol_detailEnglish+"</p>";
                        new_data_box_English += "<hr class=\"hr-line-dashed\">";
                        new_data_box_English += "</div>";
                        

                        new_data_box_Chinese += "<div class=\"BookingCondition\">";
                        new_data_box_Chinese += "<h5>"+response[0].BookingCondition[i].bookingConditionCategory_topicChinese+"</h5>";
                        new_data_box_Chinese += "<p>"+response[0].BookingCondition[i].bookingConditioncol_detailChinese+"</p>";
                        new_data_box_Chinese += "<hr class=\"hr-line-dashed\">";
                        new_data_box_Chinese += "</div>";
                        

                        new_data_box_Laos += "<div class=\"BookingCondition\">";
                        new_data_box_Laos += "<h5>"+response[0].BookingCondition[i].bookingConditionCategory_topicLaos+"</h5>";
                        new_data_box_Laos += "<p>"+response[0].BookingCondition[i].bookingConditioncol_detailLaos+"</p>";
                        new_data_box_Laos += "<hr class=\"hr-line-dashed\">";
                        new_data_box_Laos += "</div>";
                    }
                    $("#BookingConditionThai").append(new_data_box);
                    $("#BookingConditionEnglish").append(new_data_box_English);
                    $("#BookingConditionChinese").append(new_data_box_Chinese);
                    $("#BookingConditionLaos").append(new_data_box_Laos);

                break;
                case 10:
                    $("#tbody_TimePeriodThai").empty();
                    $("#tbody_TimePeriodEnglish").empty();
                    $("#tbody_TimePeriodChinese").empty();
                    $("#tbody_TimePeriodLaos").empty();

                    var $owl_old = $(".owl-carousel");
                    $owl_old.trigger('destroy.owl.carousel');
                    $owl_old.html($owl_old.find('.owl-stage-outer').html()).removeClass('owl-loaded');
                    $(".item").remove();
                    $(".box_data").remove();

                    new_image_box ="";
                    new_image_box_English ="";
                    new_image_box_Chinese ="";
                    new_image_box_Laos ="";

                    for(i=0;i<Object.keys(response[0].CoverItems).length;i++)
                    {
                        
                        new_image_box += "<div class=\"item\">";
                        new_image_box += "<img width=\"200\" height=\"200\" class=\"d-block w-100\" src=\"../../assets/img/uploadfile/"+response[0].CoverItems[i].coverItem_paths+"\" alt=\"\">";
                        new_image_box += "</div>";

                        new_image_box_English += "<div class=\"item\">";
                        new_image_box_English += "<img  width=\"200\" height=\"200\" class=\"d-block w-100\" src=\"../../assets/img/uploadfile/"+response[0].CoverItems[i].coverItem_paths+"\" alt=\"\">";
                        new_image_box_English += "</div>";

                        new_image_box_Chinese += "<div class=\"item\">";
                        new_image_box_Chinese += "<img  width=\"200\" height=\"200\"class=\"d-block w-100\" src=\"../../assets/img/uploadfile/"+response[0].CoverItems[i].coverItem_paths+"\" alt=\"\">";
                        new_image_box_Chinese += "</div>";

                        new_image_box_Laos += "<div class=\"item\">";
                        new_image_box_Laos += "<img width=\"200\" height=\"200\" class=\"d-block w-100\" src=\"../../assets/img/uploadfile/"+response[0].CoverItems[i].coverItem_paths+"\" alt=\"\">";
                        new_image_box_Laos += "</div>";
                    }
                    $("#owl_demoThai").append(new_image_box);
                    $("#owl_demoEnglish").append(new_image_box_English);
                    $("#owl_demoChinese").append(new_image_box_Chinese);
                    $("#owl_demoLaos").append(new_image_box_Laos);
                    var $owl = $(".owl-carousel");
                    $owl.owlCarousel({
                        utoWidth:true,
                        loop:false,
                        items : 1, 
                    });
                    document.getElementById("category_Thai").textContent =  response[0].category_thai + " : " +response[0].subcategory_thai;
                    document.getElementById("category_English").textContent =  response[0].category_english + " : " +response[0].subcategory_english;
                    document.getElementById("category_Chinese").textContent =  response[0].category_chinese + " : " +response[0].subcategory_chinese;
                    document.getElementById("category_Laos").textContent =  response[0].category_laos + " : " +response[0].subcategory_laos;

                    document.getElementById("provinces_thai").textContent =  response[0].provinces_thai;
                    document.getElementById("provinces_English").textContent =  response[0].provinces_english;
                    document.getElementById("provinces_Chinese").textContent =  response[0].provinces_chinese;
                    document.getElementById("provinces_Laos").textContent =  response[0].provinces_laos;

                    document.getElementById("items_priceguestAdult_Thai").textContent= response[0].items_priceguestAdult
                    document.getElementById("items_priceguestChild_Thai").textContent= response[0].items_priceguestChild
                    document.getElementById("items_priceguestAdult_English").textContent= response[0].items_priceguestAdult
                    document.getElementById("items_priceguestChild_English").textContent= response[0].items_priceguestChild
                    document.getElementById("items_priceguestAdult_Chinese").textContent= response[0].items_priceguestAdult
                    document.getElementById("items_priceguestChild_Chinese").textContent= response[0].items_priceguestChild
                    document.getElementById("items_priceguestAdult_Laos").textContent= response[0].items_priceguestAdult
                    document.getElementById("items_priceguestChild_Laos").textContent= response[0].items_priceguestChild

                    document.getElementById("items_highlightsThai").textContent= response[0].items_highlightsThai
                    document.getElementById("items_highlightsEnglish").textContent= response[0].items_highlightsEnglish
                    document.getElementById("items_highlightsChinese").textContent= response[0].items_highlightsChinese
                    document.getElementById("items_highlightsLaos").textContent= response[0].items_highlightsLaos 

                    document.getElementById("items_contactThai").textContent= response[0].items_contactThai
                    document.getElementById("items_contactEnglish").textContent= response[0].items_contactEnglish
                    document.getElementById("items_contactChinese").textContent= response[0].items_contactChinese
                    document.getElementById("items_contactLaos").textContent= response[0].items_contactLaos
                    
                    addOnKeyUpMarker(response[0].items_latitude,response[0].items_longitude)

                    var tableTimePeriodRef_Thai = document.getElementById('TimePeriodThai').getElementsByTagName('tbody')[0];
                    var tableTimePeriodRef_English = document.getElementById('TimePeriodEnglish').getElementsByTagName('tbody')[0];
                    var tableTimePeriodRef_Chinese = document.getElementById('TimePeriodChinese').getElementsByTagName('tbody')[0];
                    var tableTimePeriodRef_Laos = document.getElementById('TimePeriodLaos').getElementsByTagName('tbody')[0];
                    
                    for(i=0;i<Object.keys(response[0].TimePeriod).length;i++)
                    {
                        if(i == 0)
                        {
                            // var time = response[0].TimePeriod[0].items_timeOpen + " - " + response[0].TimePeriod[0].items_timeClose;
                            var time = "";
                            var TimePeriodThai = "";
                            var TimePeriodEnglish = "";
                            var TimePeriodChinese = "";
                            var TimePeriodLaos = "";
                            if(response[0].TimePeriod[0].item_dayOpen == response[0].TimePeriod[0].item_dayClose)
                            {
                                TimePeriodThai = response[0].TimePeriod[0].item_dayOpen_data[0].dayofweek_thai +" "+ time;
                                TimePeriodEnglish = response[0].TimePeriod[0].item_dayOpen_data[0].dayofweek_English +" "+ time;
                                TimePeriodChinese = response[0].TimePeriod[0].item_dayOpen_data[0].dayofweek_Chinese +" "+ time;
                                TimePeriodLaos = response[0].TimePeriod[0].item_dayOpen_data[0].dayofweek_Laos +" "+ time;
                                
                                document.getElementById("item_Open_Close_data_Thai").textContent= TimePeriodThai;
                                document.getElementById("item_Open_Close_data_English").textContent= TimePeriodEnglish;
                                document.getElementById("item_Open_Close_data_Chinese").textContent= TimePeriodChinese;
                                document.getElementById("item_Open_Close_data_Laos").textContent= TimePeriodLaos;
                            }
                            else{

                                TimePeriodThai = response[0].TimePeriod[0].item_dayOpen_data[0].dayofweek_thai +" - "+ response[0].TimePeriod[0].item_dayClose_data[0].dayofweek_thai  +" "+ time;
                                TimePeriodEnglish = response[0].TimePeriod[0].item_dayOpen_data[0].dayofweek_English +" - "+ response[0].TimePeriod[0].item_dayClose_data[0].dayofweek_English  +" "+ time;
                                TimePeriodChinese = response[0].TimePeriod[0].item_dayOpen_data[0].dayofweek_Chinese +" - "+ response[0].TimePeriod[0].item_dayClose_data[0].dayofweek_Chinese  +" "+ time;
                                TimePeriodLaos = response[0].TimePeriod[0].item_dayOpen_data[0].dayofweek_Laos +" - "+ response[0].TimePeriod[0].item_dayClose_data[0].dayofweek_Laos  +" "+ time;
                                
                                document.getElementById("item_Open_Close_data_Thai").textContent= TimePeriodThai;
                                document.getElementById("item_Open_Close_data_English").textContent= TimePeriodEnglish;
                                document.getElementById("item_Open_Close_data_Chinese").textContent= TimePeriodChinese;
                                document.getElementById("item_Open_Close_data_Laos").textContent= TimePeriodLaos;
                            }
                        }

                        var newRow_TimePeriodThai   = tableTimePeriodRef_Thai.insertRow();
                        var newRow_TimePeriodEnglish   = tableTimePeriodRef_English.insertRow();
                        var newRow_TimePeriodChinese   = tableTimePeriodRef_Chinese.insertRow();
                        var newRow_TimePeriodLaos   = tableTimePeriodRef_Laos.insertRow();

                        var newCell1_TimePeriodThai = newRow_TimePeriodThai.insertCell(0);
                        var newCell2_TimePeriodThai = newRow_TimePeriodThai.insertCell(1);
                        var newCell3_TimePeriodThai = newRow_TimePeriodThai.insertCell(2);

                        var newCell1_TimePeriodEnglish = newRow_TimePeriodEnglish.insertCell(0);
                        var newCell2_TimePeriodEnglish = newRow_TimePeriodEnglish.insertCell(1);
                        var newCell3_TimePeriodEnglish = newRow_TimePeriodEnglish.insertCell(2);

                        var newCell1_TimePeriodChinese = newRow_TimePeriodChinese.insertCell(0);
                        var newCell2_TimePeriodChinese = newRow_TimePeriodChinese.insertCell(1);
                        var newCell3_TimePeriodChinese = newRow_TimePeriodChinese.insertCell(2);

                        var newCell1_TimePeriodLaos = newRow_TimePeriodLaos.insertCell(0);
                        var newCell2_TimePeriodLaos = newRow_TimePeriodLaos.insertCell(1);
                        var newCell3_TimePeriodLaos = newRow_TimePeriodLaos.insertCell(2);

                        var newText1_TimePeriod = (i+1);
                        var newText2_TimePeriod = response[0].TimePeriod[i].items_timeOpen;
                        var newText3_TimePeriod = response[0].TimePeriod[i].items_timeClose;

                        newCell1_TimePeriodThai.innerHTML = newText1_TimePeriod;
                        newCell2_TimePeriodThai.innerHTML = newText2_TimePeriod;
                        newCell3_TimePeriodThai.innerHTML = newText3_TimePeriod;

                        newCell1_TimePeriodEnglish.innerHTML = newText1_TimePeriod;
                        newCell2_TimePeriodEnglish.innerHTML = newText2_TimePeriod;
                        newCell3_TimePeriodEnglish.innerHTML = newText3_TimePeriod;

                        newCell1_TimePeriodChinese.innerHTML = newText1_TimePeriod;
                        newCell2_TimePeriodChinese.innerHTML = newText2_TimePeriod;
                        newCell3_TimePeriodChinese.innerHTML = newText3_TimePeriod;

                        newCell1_TimePeriodLaos.innerHTML = newText1_TimePeriod;
                        newCell2_TimePeriodLaos.innerHTML = newText2_TimePeriod;
                        newCell3_TimePeriodLaos.innerHTML = newText3_TimePeriod;
                        
                    }
                    
                    document.getElementById("detail_textThai0").textContent= response[0].Detail[0].detail_textThai;
                    document.getElementById("detail_textEnglish0").textContent= response[0].Detail[0].detail_textEnglish;
                    document.getElementById("detail_textChinese0").textContent= response[0].Detail[0].detail_textChinese;
                    document.getElementById("detail_textLaos0").textContent= response[0].Detail[0].detail_textLaos;

                    document.getElementById("topicdetail_Thai").textContent= response[0].Detail[0].topicdetail_Thai;
                    document.getElementById("topicdetail_English").textContent= response[0].Detail[0].topicdetail_English;
                    document.getElementById("topicdetail_Chinese").textContent= response[0].Detail[0].topicdetail_Chinese;
                    document.getElementById("topicdetail_Laos").textContent= response[0].Detail[0].topicdetail_Laos;
                    
                    new_data_box ="";
                    new_data_box_English ="";
                    new_data_box_Chinese ="";
                    new_data_box_Laos ="";
                    for(i=0;i<Object.keys(response[0].Detail[0].Photo).length;i++)
                    {
                        new_data_box += "<div class=\"box_data\">";
                        new_data_box += "<img class=\"d-block w-100\" src=\"../../assets/img/uploadfile/"+response[0].Detail[0].Photo[i].photo_paths+"\" alt=\"\">  ";
                        new_data_box += "<br> ";
                        new_data_box += "<p><span id=\"\">"+response[0].Detail[0].Photo[i].photo_textThai+"</span></p> ";
                        new_data_box += " </div> ";

                        new_data_box_English += "<div class=\"box_data\">";
                        new_data_box_English += "<img class=\"d-block w-100\" src=\"../../assets/img/uploadfile/"+response[0].Detail[0].Photo[i].photo_paths+"\" alt=\"\">  ";
                        new_data_box_English += "<br> ";
                        new_data_box_English += "<p><span id=\"\">"+response[0].Detail[0].Photo[i].photo_textEnglish+"</span></p> ";
                        new_data_box_English += " </div> ";

                        new_data_box_Chinese += "<div class=\"box_data\">";
                        new_data_box_Chinese += "<img class=\"d-block w-100\" src=\"../../assets/img/uploadfile/"+response[0].Detail[0].Photo[i].photo_paths+"\" alt=\"\">  ";
                        new_data_box_Chinese += "<br> ";
                        new_data_box_Chinese += "<p><span id=\"\">"+response[0].Detail[0].Photo[i].photo_textChinese+"</span></p> ";
                        new_data_box_Chinese += " </div> ";

                        new_data_box_Laos += "<div class=\"box_data\">";
                        new_data_box_Laos += "<img class=\"d-block w-100\" src=\"../../assets/img/uploadfile/"+response[0].Detail[0].Photo[i].photo_paths+"\" alt=\"\">  ";
                        new_data_box_Laos += "<br> ";
                        new_data_box_Laos += "<p><span id=\"\">"+response[0].Detail[0].Photo[i].photo_textLaos+"</span></p> ";
                        new_data_box_Laos += " </div> ";
                    }

                    $("#detail_photoThai0").append(new_data_box);
                    $("#detail_photoEnglish0").append(new_data_box_English);
                    $("#detail_photoChinese0").append(new_data_box_Chinese);
                    $("#detail_photoLaos0").append(new_data_box_Laos);

                    new_data_box ="";
                    new_data_box_English ="";
                    new_data_box_Chinese ="";
                    new_data_box_Laos ="";

                    for(i=0;i<Object.keys(response[0].BookingCondition).length;i++)
                    {
                        new_data_box += "<div class=\"BookingCondition\">";
                        new_data_box += "<h5>"+response[0].BookingCondition[i].bookingConditionCategory_topicThai+"</h5>";
                        new_data_box += "<p>"+response[0].BookingCondition[i].bookingConditioncol_detailThai+"</p>";
                        new_data_box += "<hr class=\"hr-line-dashed\">";
                        new_data_box += "</div>";
                       

                        new_data_box_English += "<div class=\"BookingCondition\">";
                        new_data_box_English += "<h5>"+response[0].BookingCondition[i].bookingConditionCategory_topicEnglish+"</h5>";
                        new_data_box_English += "<p>"+response[0].BookingCondition[i].bookingConditioncol_detailEnglish+"</p>";
                        new_data_box_English += "<hr class=\"hr-line-dashed\">";
                        new_data_box_English += "</div>";
                        

                        new_data_box_Chinese += "<div class=\"BookingCondition\">";
                        new_data_box_Chinese += "<h5>"+response[0].BookingCondition[i].bookingConditionCategory_topicChinese+"</h5>";
                        new_data_box_Chinese += "<p>"+response[0].BookingCondition[i].bookingConditioncol_detailChinese+"</p>";
                        new_data_box_Chinese += "<hr class=\"hr-line-dashed\">";
                        new_data_box_Chinese += "</div>";
                        

                        new_data_box_Laos += "<div class=\"BookingCondition\">";
                        new_data_box_Laos += "<h5>"+response[0].BookingCondition[i].bookingConditionCategory_topicLaos+"</h5>";
                        new_data_box_Laos += "<p>"+response[0].BookingCondition[i].bookingConditioncol_detailLaos+"</p>";
                        new_data_box_Laos += "<hr class=\"hr-line-dashed\">";
                        new_data_box_Laos += "</div>";
                        
                    }
                    $("#BookingConditionThai").append(new_data_box);
                    $("#BookingConditionEnglish").append(new_data_box_English);
                    $("#BookingConditionChinese").append(new_data_box_Chinese);
                    $("#BookingConditionLaos").append(new_data_box_Laos);

                    
                break;
                case 11:
                    document.getElementById("coverItem_paths_Thai").src = "../../assets/img/uploadfile/"+response[0].coverItem_paths;
                    document.getElementById("coverItem_paths_English").src = "../../assets/img/uploadfile/"+response[0].coverItem_paths;
                    document.getElementById("coverItem_paths_Chinese").src = "../../assets/img/uploadfile/"+response[0].coverItem_paths;
                    document.getElementById("coverItem_paths_Laos").src = "../../assets/img/uploadfile/"+response[0].coverItem_paths;

                    document.getElementById("items_carSeatsThai").textContent= response[0].items_carSeats;
                    document.getElementById("items_carSeatsEnglish").textContent= response[0].items_carSeats;
                    document.getElementById("items_carSeatsChinese").textContent= response[0].items_carSeats;
                    document.getElementById("items_carSeatsLaos").textContent= response[0].items_carSeats;
                    
                    document.getElementById("typesofCars_Thai").textContent= response[0].typesofCars_Thai;
                    document.getElementById("typesofCars_English").textContent= response[0].typesofCars_English;
                    document.getElementById("typesofCars_Chinese").textContent= response[0].typesofCars_Chinese;
                    document.getElementById("typesofCars_Laos").textContent= response[0].typesofCars_Laos; 
                    
                    document.getElementById("gearSystem_Thai").textContent= response[0].gearSystem_Thai;
                    document.getElementById("gearSystem_English").textContent= response[0].gearSystem_English;
                    document.getElementById("gearSystem_Chinese").textContent= response[0].gearSystem_Chinese;
                    document.getElementById("gearSystem_Laos").textContent= response[0].gearSystem_Laos;
                    
                    document.getElementById("items_PricePerDayThai").textContent= response[0].items_PricePerDay;
                    document.getElementById("items_PricePerDayEnglish").textContent= response[0].items_PricePerDay;
                    document.getElementById("items_PricePerDayChinese").textContent= response[0].items_PricePerDay;
                    document.getElementById("items_PricePerDayLaos").textContent= response[0].items_PricePerDay;
                    
                    document.getElementById("items_CarDeliveryPriceThai").textContent= response[0].items_CarDeliveryPrice;
                    document.getElementById("items_CarDeliveryPriceEnglish").textContent= response[0].items_CarDeliveryPrice;
                    document.getElementById("items_CarDeliveryPriceChinese").textContent= response[0].items_CarDeliveryPrice;
                    document.getElementById("items_CarDeliveryPriceLaos").textContent= response[0].items_CarDeliveryPrice;
                    
                    document.getElementById("items_PickUpPriceThai").textContent= response[0].items_PickUpPrice;
                    document.getElementById("items_PickUpPriceEnglish").textContent= response[0].items_PickUpPrice;
                    document.getElementById("items_PickUpPriceChinese").textContent= response[0].items_PickUpPrice;
                    document.getElementById("items_PickUpPriceLaos").textContent= response[0].items_PickUpPrice;

                    document.getElementById("items_DepositPriceThai").textContent= response[0].items_DepositPrice;
                    document.getElementById("items_DepositPriceEnglish").textContent= response[0].items_DepositPrice;
                    document.getElementById("items_DepositPriceChinese").textContent= response[0].items_DepositPrice;
                    document.getElementById("items_DepositPriceLaos").textContent= response[0].items_DepositPrice;
                    
                    var Thai = (parseInt(response[0].items_isBasicInsurance) == 1?"รวม":" ไม่รวม");
                    var English = (parseInt(response[0].items_isBasicInsurance) == 1?"Yes":" No");
                    var Chinese = (parseInt(response[0].items_isBasicInsurance) == 1?"是":" 沒有");
                    var Laos = (parseInt(response[0].items_isBasicInsurance) == 1?"ແມ່ນແລ້ວ":"ບໍ່");

                    document.getElementById("items_isBasicInsuranceThai").textContent= Thai;
                    document.getElementById("items_isBasicInsuranceEnglish").textContent= English;
                    document.getElementById("items_isBasicInsuranceChinese").textContent= Chinese;
                    document.getElementById("items_isBasicInsuranceLaos").textContent= Laos;
                    
                    document.getElementById("items_BissicInsurance_detailThai").textContent= response[0].items_BissicInsurance_detailThai;
                    document.getElementById("items_BissicInsurance_detailEnglish").textContent= response[0].items_BissicInsurance_detailEnglish;
                    document.getElementById("items_BissicInsurance_detailChinese").textContent= response[0].items_BissicInsurance_detailChinese;
                    document.getElementById("items_BissicInsurance_detailLaos").textContent= response[0].items_BissicInsurance_detailLaos;

                    new_data_box ="";
                    new_data_box_English ="";
                    new_data_box_Chinese ="";
                    new_data_box_Laos ="";

                    new_data_box += "<div class=\"BookingCondition\">";
                    new_data_box += "<h4>"+response[0].BookingCondition[0].bookingConditionCategory_topicThai+"</h4>";
                    new_data_box += "<p>"+response[0].BookingCondition[0].bookingConditioncol_detailThai+"</p>";
                    new_data_box += "</div>";
                   

                    new_data_box_English += "<div class=\"BookingCondition\">";
                    new_data_box_English += "<h4>"+response[0].BookingCondition[0].bookingConditionCategory_topicEnglish+"</h4>";
                    new_data_box_English += "<p>"+response[0].BookingCondition[0].bookingConditioncol_detailEnglish+"</p>";
                    new_data_box_English += "</div>";
                    

                    new_data_box_Chinese += "<div class=\"BookingCondition\">";
                    new_data_box_Chinese += "<h4>"+response[0].BookingCondition[0].bookingConditionCategory_topicChinese+"</h4>";
                    new_data_box_Chinese += "<p>"+response[0].BookingCondition[0].bookingConditioncol_detailChinese+"</p>";
                    new_data_box_Chinese += "</div>";
                    

                    new_data_box_Laos += "<div class=\"BookingCondition\">";
                    new_data_box_Laos += "<h4>"+response[0].BookingCondition[0].bookingConditionCategory_topicLaos+"</h4>";
                    new_data_box_Laos += "<p>"+response[0].BookingCondition[0].bookingConditioncol_detailLaos+"</p>";
                    new_data_box_Laos += "</div>";

                    $("#BookingConditionThai0").append(new_data_box);
                    $("#BookingConditionEnglish0").append(new_data_box_English);
                    $("#BookingConditionChinese0").append(new_data_box_Chinese);
                    $("#BookingConditionLaos0").append(new_data_box_Laos);

                    new_data_box ="";
                    new_data_box_English ="";
                    new_data_box_Chinese ="";
                    new_data_box_Laos ="";

                    for(i=1;i<Object.keys(response[0].BookingCondition).length;i++)
                    {
                        new_data_box += "<div class=\"BookingCondition\">";                     
                        new_data_box += "<p>"+response[0].BookingCondition[i].bookingConditioncol_detailThai+"</p>";
                        new_data_box += "<hr class=\"hr-line-dashed\">";
                        new_data_box += "</div>";
                       

                        new_data_box_English += "<div class=\"BookingCondition\">";
                        new_data_box_English += "<p>"+response[0].BookingCondition[i].bookingConditioncol_detailEnglish+"</p>";
                        new_data_box_English += "<hr class=\"hr-line-dashed\">";
                        new_data_box_English += "</div>";
                        
                        new_data_box_Chinese += "<div class=\"BookingCondition\">";
                        new_data_box_Chinese += "<p>"+response[0].BookingCondition[i].bookingConditioncol_detailChinese+"</p>";
                        new_data_box_Chinese += "<hr class=\"hr-line-dashed\">";
                        new_data_box_Chinese += "</div>";
                        

                        new_data_box_Laos += "<div class=\"BookingCondition\">";
                        new_data_box_Laos += "<p>"+response[0].BookingCondition[i].bookingConditioncol_detailLaos+"</p>";
                        new_data_box_Laos += "<hr class=\"hr-line-dashed\">";
                        new_data_box_Laos += "</div>";
                    }
                    $("#BookingConditionThai").append(new_data_box);
                    $("#BookingConditionEnglish").append(new_data_box_English);
                    $("#BookingConditionChinese").append(new_data_box_Chinese);
                    $("#BookingConditionLaos").append(new_data_box_Laos);


                break;
            }
            
            var btn_items_approve = document.getElementById("btn_items_approve")
            var btn_items_disapprove = document.getElementById("btn_items_disapprove")
           
            if(btn_items_approve != null)
                btn_items_approve.value= response[0].items_id
            if(btn_items_disapprove != null)
                btn_items_disapprove.value= response[0].items_id

        }
    });

    $('#ApprovalTable tbody').on( 'click', 'tr', function () {
        var rows = table.row( this ).data();
        getReservationsData(rows[0])
    });   
}