$(document).ready(function () {
          
    $("#sidebar_Main_a").click();
    document.getElementById("sidebar_BookingOrder").classList.add("active");

    document.getElementById("header_bar_1").innerText = "คำสั่งการรายการจอง";
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });

    table = $('#ApprovalTable').DataTable({
        processing: true,
        columnDefs: [
            {
                "targets": [ 0],"visible": false,
                
            }
        ],
        dom: '<"toolbar">frtip',
        fnInitComplete: function (oSettings, response) {
            // here you to do after load data
            //response.recordsTotal is used to get Total count data
            document.getElementById("header_count_list").innerHTML =response.recordsTotal
            
        },
        "ajax": {
            url : host_entrepreneur+'/BookingOrder/getDataForTable',
            type : 'POST',
            data :  function ( d ) {
                d.state_approv = state_approv;
                // d.country = $('#country').val();
                // d.provinces =$('#provinces').val();
                // d.districts =$('#districts').val();
            }, 
        },
    });
    $(".dataTables_filter").hide();
    $('#CustomSearchBox').keyup(function(){
        table.search($(this).val()).draw() ;
    });

    $(document).on("click", ".btn-booking-approve", function () {
        var confirms = confirm("Select Yes below if you are ready to Approve this data");
        if (confirms == true)
         {
            $.ajax({
                type: "POST",
                url: host_entrepreneur+'/BookingOrder/ApprovOrder',
                data: {paymentTransaction_id: this.value},
                success: function (response) {
                    table.ajax.reload(null, false);
                    setTimeout( function () {
                        document.getElementById("header_count_list").textContent= table.data().length
                    }, 500 );
                }
            });
        }
        
    }); 
    $(document).on("click", ".btn-booking-disapprove", function () {
        var confirms = confirm("Select Yes below if you are ready to Disapprove this data");
        if (confirms == true)
         {
            $.ajax({
                type: "POST",
                url: host_entrepreneur+'/BookingOrder/DisApprovOrder',
                data: {paymentTransaction_id: this.value},
                success: function (response) {
                    table.ajax.reload(null, false);
                    setTimeout( function () {
                        document.getElementById("header_count_list").textContent= table.data().length
                    }, 500 );
                }
            });
        }
        
    }); 

    $('.state_approv').on('ifClicked', function (event) {
        state_approv = this.value
        table.ajax.reload(null, false);
        setTimeout( function () {
            document.getElementById("header_count_list").textContent= table.data().length
        }, 500 );
    });
});