$(document).ready(function () {

    $("#sidebar_Main_a").click();
    document.getElementById("sidebar_BookingOrder").classList.add("active");
                
    document.getElementById("header_bar_1").innerText = "รายละเอียดเพิ่มเติม";

    $(document).on("click", "#chat_button", function () {
        $.ajax({
            type: "GET",
            url: '/dasta_thailand/api/chat/Chat/getChatRoom',
            data: {
                items_id: items_id,
                to_user_id: to_user_id,
                from_user_id: from_user_id
            },
            success: function (response) {
                window.location.href ='/dasta_thailand/entrepreneur/Chat?chatroom_id='+response.ChatRoomData[0].chatroom_id;
                console.log(response)
            }
        });
    })
    $(document).on("click", ".btn-booking-approve", function () {
        var confirms = confirm("Select Yes below if you are ready to Approve this data");
        if (confirms == true)
         {
            $.ajax({
                type: "POST",
                url: host_entrepreneur+'/BookingOrder/ApprovOrder',
                data: {paymentTransaction_id: this.value},
                success: function (response) {
                    window.history.back();
                }
            });
        }
        
    }); 
    $(document).on("click", ".btn-booking-disapprove", function () {
        var confirms = confirm("Select Yes below if you are ready to Disapprove this data");
        if (confirms == true)
         {
            $.ajax({
                type: "POST",
                url: host_entrepreneur+'/BookingOrder/DisApprovOrder',
                data: {paymentTransaction_id: this.value},
                success: function (response) {
                    window.history.back();
                }
            });
        }
        
    }); 
});