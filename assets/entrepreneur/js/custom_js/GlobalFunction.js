$(document).ready(function(){
    
    $(document).on("click", "#btn_booking_order", function () {
        window.location.href ='/dasta_thailand/entrepreneur/BookingOrder';
    }); 
    $(document).on("click", ".btn_information", function () {
        
        $.ajax({
            type: 'post',
            url: host_entrepreneur+'/Main/get_information_type_page',
                success: function (response) {
                    window.location.href ='/dasta_thailand/entrepreneur/'+response.typepage;
                }
            });
    });
    $(document).on("click", "#btn_report", function () {
        console.log("btn_report")
    });

    $(document).on("click", "#btn_save_ChangePassword_entrepreneur", function () {
        resetNotification();
        var oldpass = document.getElementById("oldpass").value;
        var newpass = document.getElementById("newpass").value;
        var conpass = document.getElementById("conpass").value;
        $.ajax({
            type: "POST",
            url: host_entrepreneur+'/management/UserEvent/ChangePassword',
            data: {oldpass: oldpass,newpass: newpass,conpass: conpass},
        
            success: function (response) {
                if(response.state)
                {
                    $('#ChangePasswordModal').modal('hide');
                }
                else
                {
                    if(response.error == 'validation')
                    { 
                        msg = response.msg.replace(/<p>The /g, "");  
                        msg = msg.replace(/ field is required.<\/p>/g,"");
                        msg =msg.split('\n');
                        msg.forEach((element, index) => {
                                var ids = element.split('|');
                                if(ids[0] == "required")
                                {
                                    // SetNavLinkTabMainActive();
                                    var element = document.getElementById("notifi_"+ids[2]);
                                    element.style.color = "red";
                                    $("#notifi_"+ids[2]).css('display', 'inline', 'important');
                                    $('#notifi_'+ids[2]).text(ids[3]);
                                    document.getElementById(ids[1]).focus();
                                }
                        });
                    }
                    else
                    {
                        var element = document.getElementById("notifi_error_save_ChangePassword");
                        element.style.color = "red";
                        $("#notifi_error_save_ChangePassword").css('display', 'inline', 'important');
                        element.innerHTML = response.msg;
                    }
                }
            } 
        });
    });
    
});

function resetNotification()
{
    $(".notifi").map(function() {return $(this).attr('id');}).get().forEach((element, index) => {
        $("#"+element).css('display', 'none', 'important');
    });     
}