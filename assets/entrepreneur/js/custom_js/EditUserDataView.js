$(document).ready(function(){
    const input = document.getElementById("pac-input");
    const searchBox = new google.maps.places.SearchBox(input);
    searchMapsBox(searchBox)

    $(document).on("click", "#get_current_location", function () {
        document.getElementById("get_current_location").disabled = true;
        document.getElementById("get_current_location").innerHTML = "Processing...";
        if ("geolocation" in navigator){
            navigator.geolocation.getCurrentPosition(function(position){ 
                var currentLatitude = position.coords.latitude;
                var currentLongitude = position.coords.longitude;
    
                document.getElementById("business_latitude").value = currentLatitude;
                document.getElementById("business_longitude").value = currentLongitude;
                
                removeMarkers()
                
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                    
                  };
      
                map = new google.maps.Map(document.getElementById('map'), {
                    center: pos,
                    zoom: 16,
                    fullscreenControl:false
                });  

                let marker = new google.maps.Marker({
                    map: map,
                    position: new google.maps.LatLng(currentLatitude, currentLongitude),
                    draggable: true,
                });
            
                gmarkers.push(marker);

                document.getElementById("get_current_location").disabled = false;
                document.getElementById("get_current_location").innerHTML = "ตำแหน่งปัจจุบัน";
            });
        }
    });
})