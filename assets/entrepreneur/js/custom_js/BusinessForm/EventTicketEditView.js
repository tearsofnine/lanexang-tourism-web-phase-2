$(document).ready(function(){
    $("#sidebar_Main_a").click();
    document.getElementById("sidebar_Information").classList.add("active");
    
    $.ajax({
        type: 'post',
        url:  host_entrepreneur+'/business/EventTicket/getNameCategorySubCategory',
        success: function (response) {
          $("#namesubcategory").empty();
          $("#namesubcategory").append(response);
          $("#namesubcategory").val(subcategory_id);
          $('#namesubcategory').trigger("chosen:updated");
        }
      });
      $('.timepicker').pickatime({
        format: 'HH:i',
        formatLabel: 'HH:i',
          formatSubmit: 'HH:i',
        interval: 30
      }).pickatime('picker');
  
      $(".chosen-select-dayofweek").chosen().change(function(e, params){
        values = $(".chosen-select-dayofweek").chosen().val();
        console.log(values)
        
        if(values[0] == "add")
        {
          //document.getElementById("dayofweek").value = 0;
          $('#dayofweek').trigger("chosen:updated");
        }
        else if(values.length > 1)
        {
          if(parseInt(values[1]) > 7)
          {
            //document.getElementById("dayofweek").value = 0;
            $('#dayofweek').trigger("chosen:updated");
          }
        }
      });

      $.ajax({
        type: 'post',
        url: host_entrepreneur+'/business/EventTicket/getDayOfWeek',
        success: function (response) {
          $("#dayofweek").empty();
          $("#dayofweek").append(response);
          if(str_array != null)
            $('#dayofweek').val(str_array);
          $('#dayofweek').trigger('chosen:updated');
        }
    });
    
    var $owl = $(".owl-carousel");
    $owl.owlCarousel({
        loop:false,
        margin:10,
        nav:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }   
    });
    setTimeout(function () {SetSelectLocation();}, 1000);
    
    SetEditBox();

    $(document).on("click", "#save", function () {
        $("#save").prop("disabled", true);
        resetNotification();
        var formData = new FormData();
        var BookingCondition = [];
        var PhotoText = [];

        formData.append('items_id',items_id);
        formData.append('box_cover_image_event_ticket',box_cover_image_event_ticket);
        for (var i = 0; i < box_cover_image_event_ticket; i++) { 
            if(document.getElementById("img_event_ticket_fname_"+(i+1)).files[0] != null)
            {   
                formData.append("cropperImage_"+(i+1),document.getElementById("img_event_ticket_fname_"+(i+1)).files[0]);
            }
            else
            {
                formData.append("cropperImage_"+(i+1),dataURLtoFile(document.getElementById("temp_cover_img_event_ticket_fname_"+(i+1)).value,'originalImage'))
            }
        }
        formData.append('coverItem_url',$("#coverItem_url").val());

        formData.append('items_priceguestAdult',$("#items_priceguestAdult").val());
        formData.append('items_priceguestChild',$("#items_priceguestChild").val());

        formData.append('itmes_topicThai',$("#itmes_topicThai").val());
        formData.append('itmes_topicEnglish',$("#itmes_topicEnglish").val());
        formData.append('itmes_topicLaos',$("#itmes_topicLaos").val());
        formData.append('itmes_topicChinese',$("#itmes_topicChinese").val());
        
        formData.append('namesubcategory',$("#namesubcategory").val());

        formData.append('items_highlightsThai',$("#items_highlightsThai").val());
        formData.append('items_highlightsEnglish',$("#items_highlightsEnglish").val());
        formData.append('items_highlightsLaos',$("#items_highlightsLaos").val());
        formData.append('items_highlightsChinese',$("#items_highlightsChinese").val());

        formData.append('country',$("#country").val());
        formData.append('provinces',$("#provinces").val());
        formData.append('districts',$("#districts").val());
        formData.append('subdistricts',$("#subdistricts").val());

        formData.append('items_contactThai',$("#items_contactThai").val());
        formData.append('items_contactEnglish',$("#items_contactEnglish").val());
        formData.append('items_contactLaos',$("#items_contactLaos").val());
        formData.append('items_contactChinese',$("#items_contactChinese").val());
        formData.append('items_latitude',$("#items_latitude").val());
        formData.append('items_longitude',$("#items_longitude").val());

        formData.append('dayofweek',$("#dayofweek").val());
        formData.append('box_field_timepicker',box_field_timepicker);
        for (var i = 0; i < box_field_timepicker; i++) { 
            formData.append('items_timeOpen_'+(i+1),$("#items_timeOpen_"+(i+1)).val());
            formData.append('items_timeClose_'+(i+1),$("#items_timeClose_"+(i+1)).val());
        }

        formData.append('box_cover_image_location_illustrations',box_cover_image_location_illustrations);

        formData.append('topicdetail_id_1_Thai',$("#topicdetail_id_1_Thai").val());
        formData.append('topicdetail_id_1_English',$("#topicdetail_id_1_English").val());
        formData.append('topicdetail_id_1_Laos',$("#topicdetail_id_1_Laos").val());
        formData.append('topicdetail_id_1_Chinese',$("#topicdetail_id_1_Chinese").val());
        
        for (var i = 0; i < box_cover_image_location_illustrations; i++) { 

            if(document.getElementById("img_location_illustrations_fname_"+(i+1)).files[0] != null)
            {   
                formData.append("img_location_illustrations_fname_"+(i+1),document.getElementById("img_location_illustrations_fname_"+(i+1)).files[0]);
            }
            else
            {
                formData.append("img_location_illustrations_fname_"+(i+1),dataURLtoFile(document.getElementById("temp_cover_img_location_illustrations_fname_"+(i+1)).value,'originalImage'))
            }
            PhotoText_detail={
                photo_textThai:$("#photo_text_location_illustrations_"+(i+1)).val(),
                photo_textEnglish:$("#photo_text_location_illustrations_"+(i+1)+"_English").val(),
                photo_textChinese:$("#photo_text_location_illustrations_"+(i+1)+"_Chinese").val(),
                photo_textLaos:$("#photo_text_location_illustrations_"+(i+1)+"_Laos").val()
            }
            PhotoText.push(PhotoText_detail)
        }
        formData.append('PhotoText',JSON.stringify(PhotoText));
  
        for(var i=8;i<13;i++)
        {
            var BookingCondition_detail= {
            bookingConditioncol_detailThai :$("#bookingConditioncol_detailThai_id_"+(i)).val(),
            bookingConditioncol_detailEnglish :$("#bookingConditioncol_detailEnglish_id_"+i).val(),
            bookingConditioncol_detailChinese :$("#bookingConditioncol_detailChinese_id_"+i).val(),
            bookingConditioncol_detailLaos :$("#bookingConditioncol_detailLaos_id_"+i).val(),
            }
            BookingCondition.push(BookingCondition_detail)
        }
        formData.append('bookingConditioncol',JSON.stringify(BookingCondition));

        // Display the values
        for (var pair of formData.entries()) {
            console.log(pair[0]+ ', ' + pair[1]); 
        }
        if(checktypefile() == 0)
        {
            ajaxSaveItemBusiness(formData,"saveEditItem","EventTicket")
        }
        else
        {
            var element = document.getElementById("notifi_error_save");
            element.style.color = "red";
            $("#notifi_error_save").css('display', 'inline', 'important');
            element.innerHTML = "Please select only jpg/png file.";
            $("#save").prop("disabled", false);
        }
    });

});
async function SetEditBox()
{
    for (i = 0; i < count_box_cover_image_event_ticket; i++) {
        addImageFieldReservations("EventTicket",false);
        document.getElementById('output_image_event_ticket_'+(i+1)).src = "/dasta_thailand/assets/img/uploadfile/"+CoverItems[i].coverItem_paths;

        typeImage = CoverItems[i].coverItem_paths.split('.')
        file = await srcToFile(document.getElementById('output_image_event_ticket_'+(i+1)).src,CoverItems[i].coverItem_paths,'image/'+typeImage[1])
        handleFileSelect(file,document.getElementById('temp_cover_img_event_ticket_fname_'+(i+1))) 
    }
    for (i = 0; i < count_box_field_timepicker; i++) {
        addBoxFieldTimepicker(false);
        document.getElementById('items_timeOpen_'+(i+1)).value = TimePeriod[i].items_timeOpen;
        document.getElementById('items_timeClose_'+(i+1)).value = TimePeriod[i].items_timeClose;
        
        
    }
    $('.timepicker').pickatime({
        format: 'HH:i',
        formatLabel: 'HH:i',
          formatSubmit: 'HH:i',
        interval: 30
      })

    document.getElementById('topicdetail_id_1_Thai').value = Detail.detail_textThai;
    document.getElementById('topicdetail_id_1_Thai').innerHTML = Detail.detail_textThai;

    document.getElementById('topicdetail_id_1_English').value = Detail.detail_textEnglish;
    document.getElementById('topicdetail_id_1_English').innerHTML = Detail.detail_textEnglish;

    document.getElementById('topicdetail_id_1_Chinese').value = Detail.detail_textChinese;
    document.getElementById('topicdetail_id_1_Chinese').innerHTML = Detail.detail_textChinese;

    document.getElementById('topicdetail_id_1_Laos').value = Detail.detail_textLaos;
    document.getElementById('topicdetail_id_1_Laos').innerHTML = Detail.detail_textLaos;

    for (i = 0; i < count_box_cover_image_location_illustrations; i++) {
        addImageFieldReservations("LocationIllustrations",false);
        document.getElementById('output_image_location_illustrations_'+(i+1)).src = "/dasta_thailand/assets/img/uploadfile/"+PhotoDetail[i].photo_paths;
        document.getElementById('output_image_location_illustrations_'+(i+1)+'_English').src = "/dasta_thailand/assets/img/uploadfile/"+PhotoDetail[i].photo_paths;
        document.getElementById('output_image_location_illustrations_'+(i+1)+'_Chinese').src = "/dasta_thailand/assets/img/uploadfile/"+PhotoDetail[i].photo_paths;
        document.getElementById('output_image_location_illustrations_'+(i+1)+'_Laos').src = "/dasta_thailand/assets/img/uploadfile/"+PhotoDetail[i].photo_paths;

        typeImage = PhotoDetail[i].photo_paths.split('.')
        file = await srcToFile(document.getElementById('output_image_location_illustrations_'+(i+1)).src,PhotoDetail[i].photo_paths,'image/'+typeImage[1])
        handleFileSelect(file,document.getElementById('temp_cover_img_location_illustrations_fname_'+(i+1))) 

        document.getElementById('photo_text_location_illustrations_'+(i+1)).value = PhotoDetail[i].photo_textThai;
        document.getElementById('photo_text_location_illustrations_'+(i+1)).innerHTML = PhotoDetail[i].photo_textThai;

        document.getElementById('photo_text_location_illustrations_'+(i+1)+'_English').value = PhotoDetail[i].photo_textEnglish;
        document.getElementById('photo_text_location_illustrations_'+(i+1)+'_English').innerHTML = PhotoDetail[i].photo_textEnglish;

        document.getElementById('photo_text_location_illustrations_'+(i+1)+'_Chinese').value = PhotoDetail[i].photo_textChinese;
        document.getElementById('photo_text_location_illustrations_'+(i+1)+'_Chinese').innerHTML = PhotoDetail[i].photo_textChinese;

        document.getElementById('photo_text_location_illustrations_'+(i+1)+'_Laos').value = PhotoDetail[i].photo_textLaos;
        document.getElementById('photo_text_location_illustrations_'+(i+1)+'_Laos').innerHTML = PhotoDetail[i].photo_textLaos;
    }
    
    


}