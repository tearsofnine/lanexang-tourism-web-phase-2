$(document).ready(function(){

  $("#sidebar_Main_a").click();
  document.getElementById("sidebar_Information").classList.add("active");

  $('.summernote').summernote({
      placeholder: 'Hello Bootstrap 4',
      tabsize: 2,
      height: 100
    });
    $('.i-checks').iCheck({
      checkboxClass: 'icheckbox_square-green',
      radioClass: 'iradio_square-green',
    });
    var $owl = $(".owl-carousel");
    $owl.owlCarousel({
        loop:false,
        margin:10,
        nav:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
            1000:{
                items:5
            }
        }   
    });
    $('#province_boundary').chosen();
    var MY_SELECT = $($('#province_boundary').get(0));

    

    $("#province_boundary").chosen().change(function(e, params){
      var selection = MY_SELECT.getSelectionOrder();
      province_boundary = []
      $(selection).each(function(i)
      {
        province_boundary.push(selection[i])
      });
      
    });

    $.ajax({
      type: 'post',
      data: {
        table:"Provinces",
        where:""
      },
      url: host+'/management/LocationManagement/getDataCountries',
      success: function (response) {
        $("#province_boundary").empty();
        $("#province_boundary").append(response);
        $('#province_boundary').trigger("chosen:updated");
        MY_SELECT.setSelectionOrder(province_boundary, true);
      }
    });

    
    
    $.ajax({
      type: 'post',
      url: host_entrepreneur+'/business/PackageTours/getNameCategorySubCategory',
      success: function (response) {
        $("#namesubcategory").empty();
        $("#namesubcategory").append(response);
        $("#namesubcategory").val(subcategory_id);
        $('#namesubcategory').trigger("chosen:updated");
      }
    });
    SetEditBox();
    $(document).on("click", "#save", function () {
      $("#save").prop("disabled", true);
      resetNotification();
      var formData = new FormData();
      var BookingCondition = [];
      var TravelDetails = [];
      var TravelPeriod = [];

      formData.append('items_id',items_id);
  
      formData.append('box_cover_image_package_tours',box_cover_image_package_tours);
      for (var i = 0; i < box_cover_image_package_tours; i++) { 
          if(document.getElementById("img_package_tours_fname_"+(i+1)).files[0] != null)
          {   
              formData.append("cropperImage_"+(i+1),document.getElementById("img_package_tours_fname_"+(i+1)).files[0]);
          }
          else
          {
              formData.append("cropperImage_"+(i+1),dataURLtoFile(document.getElementById("temp_cover_img_package_tours_fname_"+(i+1)).value,'originalImage'))
          }
      }

      formData.append('coverItem_url',$("#coverItem_url").val());
      
      formData.append('itmes_topicThai',$("#itmes_topicThai").val());
      formData.append('itmes_topicEnglish',$("#itmes_topicEnglish").val());
      formData.append('itmes_topicLaos',$("#itmes_topicLaos").val());
      formData.append('itmes_topicChinese',$("#itmes_topicChinese").val());
      
      formData.append('namesubcategory',$("#namesubcategory").val());

      formData.append('province_boundary',province_boundary.join());

      for(var i=0;i<box_travel_schedule;i++)
      {
        var TravelPeriod_detail= {
          travelPeriod_amount :($("#TravelSchedule_amount_"+(i+1)).val()==""?0:$("#TravelSchedule_amount_"+(i+1)).val()),

          travelPeriod_time_period_start :$("#TravelSchedule_start_"+(i+1)).val(),
          travelPeriod_time_period_end :$("#TravelSchedule_end_"+(i+1)).val(),

          travelPeriod_adult_price :($("#TravelSchedule_guestAdultSingle_"+(i+1)).val()==""?0:$("#TravelSchedule_guestAdultSingle_"+(i+1)).val()),
          travelPeriod_adult_special_price :($("#TravelSchedule_guestAdult_"+(i+1)).val()==""?0:$("#TravelSchedule_guestAdult_"+(i+1)).val()),
          travelPeriod_child_price :($("#TravelSchedule_guestChild_"+(i+1)).val()==""?0:$("#TravelSchedule_guestChild_"+(i+1)).val()),
          travelPeriod_child_special_price :($("#TravelSchedule_guestChildBed_"+(i+1)).val()==""?0:$("#TravelSchedule_guestChildBed_"+(i+1)).val()),
        }
        TravelPeriod.push(TravelPeriod_detail)
      }
      formData.append('TravelPeriod',JSON.stringify(TravelPeriod));

      for(var i=0;i<box_travel_details;i++)
      {
        var TravelDetails_detail= {
          travelDetails_dateThai :$("#TravelDetails_dateThai_"+(i+1)).val(),
          travelDetails_dateEnglish :$("#TravelDetails_dateEnglish_"+(i+1)).val(),
          travelDetails_dateLaos :$("#TravelDetails_dateLaos_"+(i+1)).val(),
          travelDetails_dateChinese :$("#TravelDetails_dateChinese_"+(i+1)).val(),

          travelDetails_textThai :$("#TravelDetails_textThai_"+(i+1)).val(),
          travelDetails_textEnglish :$("#TravelDetails_textEnglish_"+(i+1)).val(),
          travelDetails_textLaos :$("#TravelDetails_textLaos_"+(i+1)).val(),
          travelDetails_textChinese :$("#TravelDetails_textChinese_"+(i+1)).val(),

          travelDetails_hotel_nameThai :$("#TravelDetails_hotel_nameThai_"+(i+1)).val(),
          travelDetails_hotel_nameEnglish :$("#TravelDetails_hotel_nameEnglish_"+(i+1)).val(),
          travelDetails_hotel_nameLaos :$("#TravelDetails_hotel_nameLaos_"+(i+1)).val(),
          travelDetails_hotel_nameChinese :$("#TravelDetails_hotel_nameChinese_"+(i+1)).val(),

          travelDetails_food_breakfast : ($("#TravelDetails_food_breakfast_"+(i+1)).iCheck('update')[0].checked == true?1:0),
          travelDetails_food_lunch :($("#TravelDetails_food_lunch_"+(i+1)).iCheck('update')[0].checked == true?1:0),
          travelDetails_food_dinner :($("#TravelDetails_food_dinner_"+(i+1)).iCheck('update')[0].checked == true?1:0),
        }
        TravelDetails.push(TravelDetails_detail)
      }
      formData.append('TravelDetails',JSON.stringify(TravelDetails));

      for(var i =1;i<8;i++)
      {
        var BookingCondition_detail= {
          bookingConditioncol_detailThai :$("#bookingConditioncol_detailThai_id_"+i).val(),
          bookingConditioncol_detailEnglish :$("#bookingConditioncol_detailEnglish_id_"+i).val(),
          bookingConditioncol_detailChinese :$("#bookingConditioncol_detailChinese_id_"+i).val(),
          bookingConditioncol_detailLaos :$("#bookingConditioncol_detailLaos_id_"+i).val(),
        }
        BookingCondition.push(BookingCondition_detail)
      }
      formData.append('bookingConditioncol',JSON.stringify(BookingCondition));

      formData.append('items_phone',$("#items_phone").val());

      // for (var pair of formData.entries()) {
      //   console.log(pair[0]+ ', ' + pair[1]); 
      // }

      if(checktypefile() == 0)
        ajaxSaveItemBusiness(formData,"saveEditItem","PackageTours")
      else
      {
          var element = document.getElementById("notifi_error_save");
          element.style.color = "red";
          $("#notifi_error_save").css('display', 'inline', 'important');
          element.innerHTML = "Please select only jpg/png file.";
          $("#save").prop("disabled", false);
      }
  });
    
});
async function SetEditBox()
{
  
  for (i = 0; i < count_box_cover_image_package_tours; i++) {
      addImageFieldReservations("PackageTours",false);
      document.getElementById('output_image_package_tours_'+(i+1)).src = "/dasta_thailand/assets/img/uploadfile/"+CoverItems[i].coverItem_paths;

      typeImage = CoverItems[i].coverItem_paths.split('.')
      file = await srcToFile(document.getElementById('output_image_package_tours_'+(i+1)).src,CoverItems[i].coverItem_paths,'image/'+typeImage[1])
      handleFileSelect(file,document.getElementById('temp_cover_img_package_tours_fname_'+(i+1))) 
  }
  
  addPackageToursField("TravelSchedule",false);
  addPackageToursField("TravelDetails",false);

  for (i = 0; i < box_travel_schedule; i++) {
      document.getElementById('TravelSchedule_amount_'+(i+1)).value = TravelPeriod[i].travelPeriod_amount;
      document.getElementById('TravelSchedule_start_'+(i+1)).value = moment(TravelPeriod[i].travelPeriod_time_period_start).format('MM/DD/YYYY');
      document.getElementById('TravelSchedule_end_'+(i+1)).value = moment(TravelPeriod[i].travelPeriod_time_period_end).format('MM/DD/YYYY');
      document.getElementById('TravelSchedule_guestAdultSingle_'+(i+1)).value = TravelPeriod[i].travelPeriod_adult_price;
      document.getElementById('TravelSchedule_guestAdult_'+(i+1)).value = TravelPeriod[i].travelPeriod_adult_special_price;
      document.getElementById('TravelSchedule_guestChild_'+(i+1)).value = TravelPeriod[i].travelPeriod_child_price;
      document.getElementById('TravelSchedule_guestChildBed_'+(i+1)).value = TravelPeriod[i].travelPeriod_child_special_price;
  }

  for (i = 0; i < box_travel_details; i++) {

      document.getElementById('TravelDetails_dateThai_'+(i+1)).value = TravelDetails[i].travelDetails_dateThai;
      document.getElementById('TravelDetails_dateEnglish_'+(i+1)).value = TravelDetails[i].travelDetails_dateEnglish;
      document.getElementById('TravelDetails_dateLaos_'+(i+1)).value = TravelDetails[i].travelDetails_dateLaos;
      document.getElementById('TravelDetails_dateChinese_'+(i+1)).value = TravelDetails[i].travelDetails_dateChinese;

      document.getElementById('TravelDetails_textThai_'+(i+1)).value = TravelDetails[i].travelDetails_textThai;
      document.getElementById('TravelDetails_textThai_'+(i+1)).innerHTML = TravelDetails[i].travelDetails_textThai;
      document.getElementById('TravelDetails_textEnglish_'+(i+1)).value = TravelDetails[i].travelDetails_textEnglish;
      document.getElementById('TravelDetails_textEnglish_'+(i+1)).innerHTML = TravelDetails[i].travelDetails_textEnglish;
      document.getElementById('TravelDetails_textLaos_'+(i+1)).value = TravelDetails[i].travelDetails_textLaos;
      document.getElementById('TravelDetails_textLaos_'+(i+1)).innerHTML = TravelDetails[i].travelDetails_textLaos;
      document.getElementById('TravelDetails_textChinese_'+(i+1)).value = TravelDetails[i].travelDetails_textChinese;
      document.getElementById('TravelDetails_textChinese_'+(i+1)).innerHTML = TravelDetails[i].travelDetails_textChinese;

      document.getElementById('TravelDetails_hotel_nameThai_'+(i+1)).value = TravelDetails[i].travelDetails_hotel_nameThai;
      document.getElementById('TravelDetails_hotel_nameEnglish_'+(i+1)).value = TravelDetails[i].travelDetails_hotel_nameEnglish;
      document.getElementById('TravelDetails_hotel_nameLaos_'+(i+1)).value = TravelDetails[i].travelDetails_hotel_nameLaos;
      document.getElementById('TravelDetails_hotel_nameChinese_'+(i+1)).value = TravelDetails[i].travelDetails_hotel_nameChinese;

      if(TravelDetails[i].travelDetails_food_breakfast == "1")
          $('#TravelDetails_food_breakfast_'+(i+1)).iCheck('check'); 
      if(TravelDetails[i].travelDetails_food_lunch == "1")
          $('#TravelDetails_food_lunch_'+(i+1)).iCheck('check'); 
      if(TravelDetails[i].travelDetails_food_dinner == "1")
          $('#TravelDetails_food_dinner_'+(i+1)).iCheck('check'); 
  }
  
  $('#data_5 .input-daterange').datepicker({
      keyboardNavigation: false,
      forceParse: false,
      autoclose: true,                       
  });

}