
$(document).ready(function(){

    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });
    $.ajax({
        type: 'post',
        data: { getOption: 1},
        url:host_entrepreneur+'/business/ProgramTour/getNameCategorySubCategory',
        success: function (response) {
          
            $("#subcategory").empty();
            $("#subcategory").append(response);
            $("#subcategory").val(SubCategory_subcategory_id);
            $("#subcategory").trigger("chosen:updated");
        }
    }); 
    document.getElementById('image_crop_img').addEventListener('ready', (event) => {      
        if(!crop_img)
        {
            cropper.setData(CropData);
            cropper.setCropBoxData(CropBoxData);
            cropper.setCanvasData(CanvasData);
            crop_img = true;
        }   
    });
    
    document.getElementById('image_crop_img').addEventListener('crop', (event) => {
        if(crop_img)
        {
            CropData = cropper.getData()
            CropBoxData = cropper.getCropBoxData()
            CanvasData = cropper.getCanvasData()
        }
    });  
    SetProgramTourBox()

    $(document).on("click", "#save", function ()
    {
        $("#save").prop("disabled", true);
        resetNotification();
        cropper.getCroppedCanvas().toBlob((blob) =>
        {
            var formData = new FormData();

            formData.append('cropperImage', blob,'cropperImage.png');
            
            if(document.getElementById("inputImage").files[0] != null)
                formData.append('originalImage', document.getElementById("inputImage").files[0]);
            else
            {
                formData.append("originalImage",dataURLtoFile(document.getElementById("temp_inputImage").value,'originalImage'))
            }

            formData.append('CropData', JSON.stringify(CropData));
            formData.append('CropBoxData', JSON.stringify(CropBoxData));
            formData.append('CanvasData', JSON.stringify(CanvasData));

            formData.append('programTour_id',programTour_id);

            formData.append('programTour_nameThai',$("#programTour_nameThai").val());
            formData.append('programTour_nameEnglish',$("#programTour_nameEnglish").val());
            formData.append('programTour_nameLaos',$("#programTour_nameLaos").val());
            formData.append('programTour_nameChinese',$("#programTour_nameChinese").val());

            formData.append('programTour_time_period_start',$("#start").val());
            formData.append('programTour_time_period_end',$("#end").val());
            formData.append('subcategory',$("#subcategory").val());

            formData.append('programTour_interestingThingsThai',$("#programTour_interestingThingsThai").val());
            formData.append('programTour_interestingThingsEnglish',$("#programTour_interestingThingsEnglish").val());
            formData.append('programTour_interestingThingsLaos',$("#programTour_interestingThingsLaos").val());
            formData.append('programTour_interestingThingsChinese',$("#programTour_interestingThingsChinese").val());
            
            formData.append('ProgramTourCount',array_dates_ProgramTour.length);

            for (var i = 0; i < array_dates_ProgramTour.length; i++) 
            {
                var datesTrip_topic_ProgramTour =[];
                datesTrip_topic_ProgramTour.push($("#datesTrip_topicThai_ProgramTour_"+(i+1)).val())
                datesTrip_topic_ProgramTour.push($("#datesTrip_topicEnglish_ProgramTour_"+(i+1)).val())
                datesTrip_topic_ProgramTour.push($("#datesTrip_topicChinese_ProgramTour_"+(i+1)).val())
                datesTrip_topic_ProgramTour.push($("#datesTrip_topicLaos_ProgramTour_"+(i+1)).val())

                formData.append('datesTrip_topic_ProgramTour_'+(i+1),JSON.stringify(datesTrip_topic_ProgramTour));
                var Itemid = [];
                for (var j = 0; j < array_dates_ProgramTour[i].Itemid.length; j++) 
                {
                    var detail_item =[]
                    detail_item.push($("#detail_Thai_item_ProgramTour_"+(i+1)+"_"+(j+1)).val())
                    detail_item.push($("#detail_English_item_ProgramTour_"+(i+1)+"_"+(j+1)).val())
                    detail_item.push($("#detail_Chinese_item_ProgramTour_"+(i+1)+"_"+(j+1)).val())
                    detail_item.push($("#detail_Laos_item_ProgramTour_"+(i+1)+"_"+(j+1)).val())
                    
                    for (var k = 0; k < array_dates_ProgramTour[i].Itemid[j].numImgItem; k++) 
                    {
                        
                        if(document.getElementById("img_detail_item_ProgramTour_"+(i+1)+"_"+(j+1)+"_fname_"+(k+1)).files[0] != null)
                            formData.append("img_detail_item_ProgramTour_"+(i+1)+"_"+(j+1)+"_"+(k+1),document.getElementById("img_detail_item_ProgramTour_"+(i+1)+"_"+(j+1)+"_fname_"+(k+1)).files[0]);
                        else
                        {
                            formData.append("img_detail_item_ProgramTour_"+(i+1)+"_"+(j+1)+"_"+(k+1),dataURLtoFile(document.getElementById("temp_img_detail_item_ProgramTour_"+(i+1)+"_"+(j+1)+"_fname_"+(k+1)).value,"temp_img_navigate_fname_"+(i+1)+"_"+(j+1)+"_"+(k+1)));
                           
                        }
                        
                        formData.append("photoTourist_topicThai_detail_item_ProgramTour_"+(i+1)+"_"+(j+1)+"_"+(k+1),$("#photoTourist_topicThai_detail_item_ProgramTour_"+(i+1)+"_"+(j+1)+"_"+(k+1)).val())
                        formData.append("photoTourist_topicEnglish_detail_item_ProgramTour_"+(i+1)+"_"+(j+1)+"_"+(k+1),$("#photoTourist_topicEnglish_detail_item_ProgramTour_"+(i+1)+"_"+(j+1)+"_"+(k+1)).val())
                        formData.append("photoTourist_topicChinese_detail_item_ProgramTour_"+(i+1)+"_"+(j+1)+"_"+(k+1),$("#photoTourist_topicChinese_detail_item_ProgramTour_"+(i+1)+"_"+(j+1)+"_"+(k+1)).val())
                        formData.append("photoTourist_topicLaos_detail_item_ProgramTour_"+(i+1)+"_"+(j+1)+"_"+(k+1),$("#photoTourist_topicLaos_detail_item_ProgramTour_"+(i+1)+"_"+(j+1)+"_"+(k+1)).val())
                        
                    } 
                    var data_item = {
                        ItemId :array_dates_ProgramTour[i].Itemid[j].place,
                        detail :detail_item,
                        numImgItem :array_dates_ProgramTour[i].Itemid[j].numImgItem
                    }
                    Itemid.push(data_item)
                }
                formData.append('tourist_attractions_data_'+(i+1),JSON.stringify(Itemid)); 
            }
            // for (var pair of formData.entries()) {
            //     console.log(pair[0]+ ', ' + pair[1]); 
            // }
            for (var value of formData.values()) {
                console.log(value);
             }
             $.ajax({
                type: "POST",
                url:host_entrepreneur+'/business/'+MenuName+'/saveEditProgramTour',
                data: formData,
                contentType: false,
                processData:false,
                success: function (response) 
                {

                   console.log(response)

                    if(response.state)
                    {
                        var element = document.getElementById("notifi_error_save");
                        element.style.color = "green";
                        $("#notifi_error_save").css('display', 'inline', 'important');
                        element.innerHTML = response.msg;
                        window.location.href ='/dasta_thailand/entrepreneur/business/'+MenuName;
                    }
                    else
                    {
                        $("#save").prop("disabled", false);
                        if(response.error == 'validation')
                        { 
                            msg = response.msg.replace(/<p>The /g, "");  
                            msg = msg.replace(/ field is required.<\/p>/g,"");
                            msg =msg.split('\n');
                            msg.forEach((element, index) => {
                                    var ids = element.split('|');
                                    if(ids[0] == "required")
                                    {
                                        SetNavLinkTabMainActive();
                                        var element = document.getElementById("notifi_"+ids[2]);
                                        element.style.color = "red";
                                        $("#notifi_"+ids[2]).css('display', 'inline', 'important');
                                        $('#notifi_'+ids[2]).text(ids[3]);
                                        document.getElementById(ids[1]).focus();
                                    }
                            });
                        }
                        else
                        {
                            var element = document.getElementById("notifi_error_save");
                            element.style.color = "red";
                            $("#notifi_error_save").css('display', 'inline', 'important');
                            element.innerHTML = response.msg;
                        }
                    }
                }
            });

        });
    });  
   
});

async function SetProgramTourBox()
{
    var typeImage = document.getElementById('image_crop_img').src.split('.')
    var file = await srcToFile(document.getElementById('image_crop_img').src,"temp_image_crop_img",'image/'+typeImage[1])
    handleFileSelect(file,document.getElementById('temp_inputImage')) 
    $.ajax({
        type: 'post',
        data: { programTour_id: programTour_id},
        url:host_entrepreneur+'/business/ProgramTour/getProgramTourById',
        
        success: function (response) {
            (async () => { 
               
                var time_period =  document.getElementsByClassName("time_period")
                
                if((response[0].programTour_time_period_start != "1970-01-01") && 
                    (response[0].programTour_time_period_end != "1970-01-01")
                )
                {
                    $('#set_time_period').iCheck('check'); 
                    $(".time_period").prop("disabled", false);
                    time_period[0].value = moment(response[0].programTour_time_period_start).format('MM/DD/YYYY');
                    time_period[1].value = moment(response[0].programTour_time_period_end).format('MM/DD/YYYY');
                    $('#data_5 .input-daterange').datepicker({
                        keyboardNavigation: false,
                        forceParse: false,
                        autoclose: true,                       
                    });
                }
                for (i = 0; i < response[0].DatesTrip.length; i++) {
                    array_dates_ProgramTour.push(new ProgramTourModels());
                    NewImageBox((i+1),"ProgramTour");

                    document.getElementById('datesTrip_topicThai_ProgramTour_'+(i+1)).value =response[0].DatesTrip[i].datesTrip_topicThai;
                    document.getElementById('datesTrip_topicEnglish_ProgramTour_'+(i+1)).value =response[0].DatesTrip[i].datesTrip_topicEnglish;
                    document.getElementById('datesTrip_topicChinese_ProgramTour_'+(i+1)).value =response[0].DatesTrip[i].datesTrip_topicChinese;
                    document.getElementById('datesTrip_topicLaos_ProgramTour_'+(i+1)).value =response[0].DatesTrip[i].datesTrip_topicLaos;

                    for (j = 0; j < response[0].DatesTrip[i].TouristAttractions.length; j++) {

                        array_dates_ProgramTour[i].Itemid.push(new ItemArray());
                        NewImageBox( (j+1),"item_ProgramTour_"+(i+1));

                        array_dates_ProgramTour[i].Itemid[j].place =  parseInt(response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].items_id);
                        array_dates_ProgramTour[i].Itemid[j].country =  parseInt(response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].country_id);
                        array_dates_ProgramTour[i].Itemid[j].provinces =  parseInt(response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].provinces_id);
                        array_dates_ProgramTour[i].Itemid[j].districts =  parseInt(response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].districts_id);
                        array_dates_ProgramTour[i].Itemid[j].category =  parseInt(response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].category_id);
                        array_dates_ProgramTour[i].Itemid[j].type =  parseInt(response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].SubCategory_subcategory_id);

                        document.getElementById("item_ProgramTour_"+(i+1)+"_"+(j+1)+"_itmes_topicThai").innerText = response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].itmes_topicThai
                        document.getElementById('item_ProgramTour_'+(i+1)+"_"+(j+1)+'_items_contactThai').innerText =response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].items_contactThai
                        document.getElementById('item_ProgramTour_'+(i+1)+"_"+(j+1)+'_category_thai').innerText =response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].category_thai
                        document.getElementById('item_ProgramTour_'+(i+1)+"_"+(j+1)+'_subcategory_thai').innerText =response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].subcategory_thai
                        document.getElementById('item_ProgramTour_'+(i+1)+"_"+(j+1)+'_coverItem_paths').src = "/dasta_thailand/assets/img/uploadfile/"+response[0].DatesTrip[i].TouristAttractions[j].ItemDetail[0].coverItem_paths
                        RestProgramTourData((i+1),(j+1))

                        document.getElementById('detail_Thai_item_ProgramTour_'+(i+1)+"_"+(j+1)).value =response[0].DatesTrip[i].TouristAttractions[j].detail_Thai;
                        document.getElementById('detail_Thai_item_ProgramTour_'+(i+1)+"_"+(j+1)).innerHTML =response[0].DatesTrip[i].TouristAttractions[j].detail_Thai;
                        document.getElementById('detail_English_item_ProgramTour_'+(i+1)+"_"+(j+1)).value =response[0].DatesTrip[i].TouristAttractions[j].detail_English;
                        document.getElementById('detail_English_item_ProgramTour_'+(i+1)+"_"+(j+1)).innerHTML =response[0].DatesTrip[i].TouristAttractions[j].detail_English;
                        document.getElementById('detail_Chinese_item_ProgramTour_'+(i+1)+"_"+(j+1)).value =response[0].DatesTrip[i].TouristAttractions[j].detail_Chinese;
                        document.getElementById('detail_Chinese_item_ProgramTour_'+(i+1)+"_"+(j+1)).innerHTML =response[0].DatesTrip[i].TouristAttractions[j].detail_Chinese;
                        document.getElementById('detail_Laos_item_ProgramTour_'+(i+1)+"_"+(j+1)).value =response[0].DatesTrip[i].TouristAttractions[j].detail_Laos;
                        document.getElementById('detail_Laos_item_ProgramTour_'+(i+1)+"_"+(j+1)).innerHTML =response[0].DatesTrip[i].TouristAttractions[j].detail_Laos;

                        for (var k = 0; k < response[0].DatesTrip[i].TouristAttractions[j].PhotoTourist.length; k++) {

                            array_dates_ProgramTour[i].Itemid[j].numImgItem += 1;
                            NewImageBox( (k+1),"detail_item_ProgramTour_"+(i+1)+"_"+(j+1),false);

                            document.getElementById('photoTourist_topicThai_detail_item_ProgramTour_'+(i+1)+"_"+(j+1)+"_"+(k+1)).value = response[0].DatesTrip[i].TouristAttractions[j].PhotoTourist[k].photoTourist_topicThai;
                            document.getElementById('photoTourist_topicThai_detail_item_ProgramTour_'+(i+1)+"_"+(j+1)+"_"+(k+1)).innerHTML = response[0].DatesTrip[i].TouristAttractions[j].PhotoTourist[k].photoTourist_topicThai;
                            document.getElementById('photoTourist_topicEnglish_detail_item_ProgramTour_'+(i+1)+"_"+(j+1)+"_"+(k+1)).value = response[0].DatesTrip[i].TouristAttractions[j].PhotoTourist[k].photoTourist_topicEnglish;
                            document.getElementById('photoTourist_topicEnglish_detail_item_ProgramTour_'+(i+1)+"_"+(j+1)+"_"+(k+1)).innerHTML = response[0].DatesTrip[i].TouristAttractions[j].PhotoTourist[k].photoTourist_topicEnglish;
                            document.getElementById('photoTourist_topicChinese_detail_item_ProgramTour_'+(i+1)+"_"+(j+1)+"_"+(k+1)).value = response[0].DatesTrip[i].TouristAttractions[j].PhotoTourist[k].photoTourist_topicChinese;
                            document.getElementById('photoTourist_topicChinese_detail_item_ProgramTour_'+(i+1)+"_"+(j+1)+"_"+(k+1)).innerHTML = response[0].DatesTrip[i].TouristAttractions[j].PhotoTourist[k].photoTourist_topicChinese;
                            document.getElementById('photoTourist_topicLaos_detail_item_ProgramTour_'+(i+1)+"_"+(j+1)+"_"+(k+1)).value = response[0].DatesTrip[i].TouristAttractions[j].PhotoTourist[k].photoTourist_topicLaos;
                            document.getElementById('photoTourist_topicLaos_detail_item_ProgramTour_'+(i+1)+"_"+(j+1)+"_"+(k+1)).innerHTML = response[0].DatesTrip[i].TouristAttractions[j].PhotoTourist[k].photoTourist_topicLaos;

                            document.getElementById("output_image_detail_item_ProgramTour_"+(i+1)+"_"+(j+1)+"_"+(k+1)).src = "/dasta_thailand/assets/img/uploadfile/"+response[0].DatesTrip[i].TouristAttractions[j].PhotoTourist[k].photoTourist_paths;
                            document.getElementById("output_image_Eng_detail_item_ProgramTour_"+(i+1)+"_"+(j+1)+"_"+(k+1)).src = "/dasta_thailand/assets/img/uploadfile/"+response[0].DatesTrip[i].TouristAttractions[j].PhotoTourist[k].photoTourist_paths;
                            document.getElementById("output_image_Laos_detail_item_ProgramTour_"+(i+1)+"_"+(j+1)+"_"+(k+1)).src = "/dasta_thailand/assets/img/uploadfile/"+response[0].DatesTrip[i].TouristAttractions[j].PhotoTourist[k].photoTourist_paths;
                            document.getElementById("output_image_Chinese_detail_item_ProgramTour_"+(i+1)+"_"+(j+1)+"_"+(k+1)).src = "/dasta_thailand/assets/img/uploadfile/"+response[0].DatesTrip[i].TouristAttractions[j].PhotoTourist[k].photoTourist_paths;
                    
                            typeImage = response[0].DatesTrip[i].TouristAttractions[j].PhotoTourist[k].photoTourist_paths.split('.')
                            file = await srcToFile(document.getElementById("output_image_detail_item_ProgramTour_"+(i+1)+"_"+(j+1)+"_"+(k+1)).src,response[0].DatesTrip[i].TouristAttractions[j].PhotoTourist[k].photoTourist_paths,'image/'+typeImage[1])
                            handleFileSelect(file,document.getElementById("temp_img_detail_item_ProgramTour_"+(i+1)+"_"+(j+1)+"_fname_"+(k+1)))
                        }
                        
                        var div_content_info = document.getElementById("div_content_info_"+(i+1)+"_"+(j+1));
                        var div_content_show_result = document.getElementById("div_content_show_result_"+(i+1)+"_"+(j+1));
                        div_content_info.style.display = 'none';
                        div_content_show_result.style.display = '';
                    }   
                }
                
            })();
        }
    }); 
    
}