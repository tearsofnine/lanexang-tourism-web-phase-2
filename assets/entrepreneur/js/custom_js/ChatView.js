

$(document).ready(function(){
    document.getElementById("sidebar_Notifications").classList.add("active");

    document.getElementById("header_bar_1").innerText = name_user;
    addChat(chatroom_id,user_id)

    $("#message").on('keyup', function (event) {
        if (event.keyCode === 13) {
            sendChat(chatroom_id,user_id);
        }
    });
    $(document).on("click", "#send_message", function () {
        sendChat(chatroom_id,user_id)
    })
});

messaging.onMessage(function(payload) {
    setTimeout( function () {
        addChat(chatroom_id,user_id)
        updateNotifications()
    }, 200 );
});
function sendChat(chatroom_id,user_id)
{
    $.ajax({
        type: 'POST',
        data: {
            chatroom_id:chatroom_id,
            user_id:user_id
        },
        url: './Chat/getChatRoom',
        success: function (response) {
            var from_user_id =0;
            var to_user_id =0;
            if(response[0].UserSend_user_id == user_id)
            {
                from_user_id = user_id;
                to_user_id = response[0].UserReceive_user_id;
            }
            else
            {
                from_user_id = user_id;
                to_user_id = response[0].UserSend_user_id;
            }
            var message =  document.getElementById("message").value;
            if(message)
            {
                $.ajax({
                    type: 'POST',
                    data: {
                        from_user_id:from_user_id,
                        to_user_id:to_user_id,
                        message:message
                    },
                    url: '../api/chat/Chat/sendingMessage',
                    success: function (response) {

                        setTimeout( function () {
                            addChat(chatroom_id,user_id)

                            
                        }, 200 );
                    }
                });
                document.getElementById("message").value = "";
            }
                
        }
    });
}
function addChat(chatroom_id,user_id)
{
    var chat ="";
    $.ajax({
        type: 'POST',
        data: {
            chatroom_id:chatroom_id,
            user_id:user_id
        },
        url: './Chat/setChatDataForTable',
        success: function (response) {
            $("#chat_table").remove();
            $("#main_chat_table").append("<div class=\"chat-activity-list\" id=\"chat_table\"></div>");
            for (i = 0; i < response.length; i++) 
            {
                if(response[i].user_profile_pic_url)
                    image = "/dasta_thailand/assets/img/uploadfile/"+response[i].user_profile_pic_url;
                else
                    image = "/dasta_thailand/assets/img/account_avatar.jpg";

                chat ="";
                chat +="<div class=\"chat-element "+(response[i].User_user_id == user_id? 'right' :'')+"\">";
                chat +="<a href=\"#\" class=\"float-"+(response[i].User_user_id == user_id? 'right' :'left')+"\"><img alt=\"image\" class=\"rounded-circle\" src=\""+image+"\"></a>";
                chat +="<div class=\"media-body "+(response[i].User_user_id == user_id? 'text-right' :'')+"\" style=\"word-wrap: break-word;white-space: pre-wrap;white-space: -pre-wrap;white-space: -o-pre-wrap;word-wrap: break-word;\">";
                chat +="<strong>"+response[i].user_firstName+" "+response[i].user_lastName+"</strong>";
                chat +="<p class=\"m-b-xs \">"+response[i].message+"</p>";
                chat +="</div>";
                chat +="</div>";
                $("#chat_table").append(chat); 
            }

            var scrollTo_val = $('#messagelist').prop('scrollHeight') + 'px';
            $('#messagelist').slimScroll({ 
                scrollTo : scrollTo_val,
                height: '620px',
                start: 'bottom',
                alwaysVisible: true
            });
        }
    });
  
}