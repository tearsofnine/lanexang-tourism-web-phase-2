$(document).ready(function(){
    $('.login-form').submit(function(e) {
        e.preventDefault();        
        $.ajax({
            url : '/dasta_thailand/entrepreneur/Login/authenticate',
            type : 'post',
            data : $(this).serialize(),
            success : function(response) {
                if (response.state != 1) {
                    var element = document.getElementById("notification");
                    element.style.color = "red";
                    var msg = response.msg.replace(/(<([^>]+)>)/ig,"").replace(".",'<br/>');
                    $("#notification").css('display', 'inline', 'important');
                    element.innerHTML = msg;
                    
                } 
                else {
                    var user_id = response.user_id;
                    messaging.getToken().then((currentToken) => {
                        if (currentToken) 
                        {
                            var Token = currentToken
                            $.ajax({
                                type: "POST",
                                url: '/dasta_thailand/api/chat/Chat/updateCurrentToken',
                                data: {
                                    user_id: user_id,
                                    currentToken: Token
                                },
                                success : function(response) {
                                    if (response.state)
                                        window.location.href ='/dasta_thailand/entrepreneur/Main';
                                    else
                                        window.location.href ='/dasta_thailand/entrepreneur/Login/logout';  
                                }
                            })
                            console.log("Message received. ", currentToken);
                        } else 
                        {
                            // Show permission request.
                            console.log('No Instance ID token available. Request permission to generate one.');
                            window.location.href ='/dasta_thailand/entrepreneur/Login/logout'; 
                        }
                        }).catch((err) => {
                        console.log('An error occurred while retrieving token. ', err);
                        window.location.href ='/dasta_thailand/entrepreneur/Login/logout'; 
                    });
                    
                }
            }
        });
    });   
});