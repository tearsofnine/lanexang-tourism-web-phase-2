$(document).ready(function () {
          
    $("#sidebar_Main_a").click();
    document.getElementById("sidebar_ProgramTour").classList.add("active");
    
    tableContextMenu = new ContextMenu("context-menu-items", menuItemClickListener);

    document.getElementById("header_bar_1").innerText = "ข้อมูล";
    document.getElementById("header_bar_2").innerText = "โปรแกรมการท่องเที่ยว";

    $.ajax({
        type: 'post',
        data: { getOption: 1},
        url: host_entrepreneur+'/business/ProgramTour/getNameCategorySubCategory',
        success: function (response) {
          
            $("#subcategory").empty();
            $("#subcategory").append(response);
            $("#subcategory").trigger("chosen:updated");
        }
    }); 
    $.ajax({
        type: 'post',
        data: { getOption: 1},
        url: host_entrepreneur+'/business/ProgramTour/getNumDatesTrip',
        success: function (response) {
          
            $("#num_dates_trip").empty();
            $("#num_dates_trip").append(response);
            $("#num_dates_trip").trigger("chosen:updated");
        }
    }); 

    table = $('#'+MenuName+'Table').DataTable({
        processing: true,
        responsive: true,
        //stateSave: true,
        //serverSide: true,
        columnDefs: [
            { className: 'text-right', targets:7 }
          ],
        "ajax": {
            url : host_entrepreneur+'/business/ProgramTour/getDataProgramTourForTable',
            type : 'POST',
              data :  function ( d ) {
                  d.subcategory = $('#subcategory').val();
                  d.num_dates_trip = $('#num_dates_trip').val();
              },
    
        },
      
    });

    table.on( 'order.dt search.dt', function () {
        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();
    table.page.len( 10 ).draw();
    var html ='<div style="width: 385px;-moz-border-bottom-colors: none;-moz-border-left-colors: none;-moz-border-right-colors: none;-moz-border-top-colors: none;background-color: #ffffff;border-color: #e7eaec;border-image: none;border-style: solid solid none;border-width: 0px 0 0;color: inherit;margin-bottom: 0;padding: 10px 50px 8px 10px;">';
    html +='<select id="PagelengthMenu">';
    html +='<option value ="10">10</option>';
    html +='<option value ="25">25</option>';
    html +='<option value ="50">50</option>';
    html +='<option value ="100">100</option>';
    html +='</select>';
     $("div.toolbar").html(html);
    $(document).on('change', '#PagelengthMenu', function (e) {
        var select = $('option:selected', this);
        table.page.len( select.val() ).draw();
    });
    $(".dataTables_filter").hide();
    $('#CustomSearchBox').keyup(function(){
        table.search($(this).val()).draw() ;
    });

    $(document).on('change', '#subcategory', function (e) {
        table.ajax.reload();
        
    });
    $(document).on('change', '#num_dates_trip', function (e) {
        table.ajax.reload();   
    });

});