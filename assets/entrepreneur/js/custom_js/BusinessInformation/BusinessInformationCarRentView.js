$(document).ready(function () {
    
    $("#sidebar_Main_a").click();
    document.getElementById("sidebar_Information").classList.add("active");
    
    tableContextMenu = new ContextMenu("context-menu-items", menuItemClickListener);

    document.getElementById("header_bar_1").innerText = "ข้อมูล";
    document.getElementById("header_bar_2").innerText = "เกี่ยวกับกิจการของท่าน";

    document.getElementById("SearchBoxText").textContent="ค้นหารถเช่า";
    $("#SearchBoxText").append(' <span class="float-right"><i class="fa fa-chevron-down"></i></span>');

    $.ajax({
        type: 'post',
        url: host_entrepreneur+'/business/'+MenuName+'/getNameGearSystem',
        success: function (response) {
          $("#GearSystem_gearSystem_id").empty();
          $("#GearSystem_gearSystem_id").append(response);
          $('#GearSystem_gearSystem_id').trigger("chosen:updated");
        }
      });
    $.ajax({
        type: 'post',
        url: host_entrepreneur+'/business/'+MenuName+'/getNameTypesofCars',
        success: function (response) {
          $("#TypesofCars_typesofCars_id").empty();
          $("#TypesofCars_typesofCars_id").append(response);
          $('#TypesofCars_typesofCars_id').trigger("chosen:updated");
        }
      });

    table = $('#'+MenuName+'Table').DataTable({
        processing: true,
        //stateSave: true,
        //serverSide: true,
        columnDefs: [
            {
                "targets": [ 1,2,3,4],"visible": false,
            },
            { orderable: false, targets:[0] },
        ],
        dom: '<"toolbar">frtip',
        fnInitComplete: function (oSettings, response) {
            // here you to do after load data
            //response.recordsTotal is used to get Total count data
            document.getElementById("countData").textContent=(response.recordsTotal);
        },
        "ajax": {
            url : host_entrepreneur+'/business/'+MenuName+'/getData'+MenuName+'ForTable',
            type : 'POST',
            data :  function ( d ) {
                d.user_id = user_id;
                d.typesofCars_id = $('#TypesofCars_typesofCars_id').val();
                d.gearSystem_id = $('#GearSystem_gearSystem_id').val();
                
            },   
        },
        // "language": {
        //     "lengthMenu": "_MENU_",
        //     // "zeroRecords": "Nothing found - sorry",
        //     // "info": "Showing page _PAGE_ of _PAGES_",
        //     // "infoEmpty": "No records available",
        //     // "infoFiltered": "(filtered from _MAX_ total records)"
        // },
    });
    CustomsDataTable()
    $(document).on("click", "#btn_search", function () {
        table.ajax.reload();
        setTimeout( function () {
            document.getElementById("countData").textContent= table.data().length
        }, 500 );
        
    });

    $(document).on('click', '#btn_search_clear', function () {
        document.getElementById("TypesofCars_typesofCars_id").selectedIndex = 0;
        $('#TypesofCars_typesofCars_id').trigger("chosen:updated");
        document.getElementById("GearSystem_gearSystem_id").selectedIndex = 0;
        $('#GearSystem_gearSystem_id').trigger("chosen:updated");
        table.ajax.reload();
        setTimeout( function () {
            document.getElementById("countData").textContent= table.data().length
        }, 500 );
    });
});

function CustomsDataTable(){
    table.page.len( 10 ).draw();
    var html ='<div style="width: 641;-moz-border-bottom-colors: none;-moz-border-left-colors: none;-moz-border-right-colors: none;-moz-border-top-colors: none;background-color: #ffffff;border-color: #e7eaec;border-image: none;border-style: solid solid none;border-width: 1px 0 0;color: inherit;margin-bottom: 0;padding: 10px 50px 8px 10px;">';
    html +='<select id="PagelengthMenu">';
    html +='<option value ="10">10</option>';
    html +='<option value ="25">25</option>';
    html +='<option value ="50">50</option>';
    html +='<option value ="100">100</option>';
    html +='</select>';
    html +='<a class="text-muted customsorting" id="sort_name"> ชื่อ <span class="custom-sort-icon custom_sorting"/></a>';
    html +='<a class="text-muted customsorting" id="sort_type"> ประเภท <span class="custom-sort-icon custom_sorting"/></a>';
    html +='<a class="text-muted customsorting" id="sort_gear"> เกียร์ <span class="custom-sort-icon custom_sorting"/></a>';
    html +='<a class="text-muted customsorting" id="sort_date"> วันที่ <span class="custom-sort-icon custom_sorting"/></a>';
    html +='</div>';
    $("div.toolbar").html(html);
    $(document).on('change', '#PagelengthMenu', function (e) {
        var select = $('option:selected', this);
        table.page.len( select.val() ).draw();
    });
    $(".dataTables_filter").hide();
    $('#CustomSearchBox').keyup(function(){
        table.search($(this).val()).draw() ;
    })
    $('.customsorting').click(function(){
        
        var a = document.getElementById($(this).attr('id'));
        var spans = a.getElementsByTagName("span");
        
        if(spans[0].classList.contains('custom_sorting_asc'))
        {
            spans[0].classList.remove('custom_sorting_asc');
            spans[0].classList.toggle('custom_sorting_desc');
        }
        else
        {
            spans[0].classList.remove('custom_sorting_desc');
            spans[0].classList.toggle('custom_sorting_asc');
        }

        reset_customsorting($(this).attr('id'));

        switch($(this).attr('id')) {
            case "sort_name":
                if(spans[0].classList.contains('custom_sorting_asc'))
                    table.order( [ 1, 'asc' ] ).draw();
                else
                    table.order( [ 1, 'desc' ] ).draw();
                break;
            case "sort_type":
                if(spans[0].classList.contains('custom_sorting_asc'))
                    table.order( [ 2, 'asc' ] ).draw();
                else
                    table.order( [ 2, 'desc' ] ).draw();
                break;
            case "sort_gear":
                if(spans[0].classList.contains('custom_sorting_asc'))
                    table.order( [ 3, 'asc' ] ).draw();
                else
                    table.order( [ 3, 'desc' ] ).draw();
                break;
            case "sort_date":
                if(spans[0].classList.contains('custom_sorting_asc'))
                    table.order( [ 4, 'asc' ] ).draw();
                else
                    table.order( [ 4, 'desc' ] ).draw();
                break;
          }
    }); 
}
function reset_customsorting(idTagA){
    $(".customsorting").map(function() {return $(this).attr('id');}).get().forEach((element, index) => {
        if(idTagA!=element)
        {
            var a = document.getElementById(element);
            var spans = a.getElementsByTagName("span");
            spans[0].classList.remove('custom_sorting_asc');
            spans[0].classList.remove('custom_sorting_desc');
        }
    });
}