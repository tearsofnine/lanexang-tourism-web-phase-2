$(document).ready(function () {
    
    $("#sidebar_Main_a").click();
    document.getElementById("sidebar_Information").classList.add("active");
    
    document.getElementById("LiViewItemsModal").setAttribute("data-toggle", "modal");
    document.getElementById("LiViewItemsModal").setAttribute("data-target", "#ViewItemsModal");
    document.getElementById("LiViewItemsModal").value = items_id;
    document.getElementById("LiEdit").value = items_id;
    
    tableContextMenu = new ContextMenu("context-menu-items", menuItemClickListener);

    document.getElementById("header_bar_1").innerText = "ข้อมูล";
    document.getElementById("header_bar_2").innerText = "เกี่ยวกับกิจการของท่าน";
    
    table = $('#BusinessTable').DataTable({
        processing: true,
        responsive: true, 
        columnDefs: [
            {
                "targets": [ 1 ],"visible": false,
            },
            { orderable: false, targets:[1] },
        ],
        "order": [[ 1, "desc" ]],
        dom: '<"toolbar">frtip',
        fnInitComplete: function (oSettings, response) {
            // here you to do after load data
            //response.recordsTotal is used to get Total count data
            document.getElementById("countData").textContent=(response.recordsTotal);
            var $owl_old = $(".owl-carousel");
            $owl_old.trigger('destroy.owl.carousel');
            $owl_old.html($owl_old.find('.owl-stage-outer').html()).removeClass('owl-loaded');

            var $owl = $(".owl-carousel");
            $owl.owlCarousel({
                items : 1, 
                loop:false,
                margin:5,
                nav:false
            });
        },
        "ajax": {
            url : host_entrepreneur+'/business/'+MenuName+'/getData'+MenuName+'ForTable',
            type : 'POST',
            data :  function ( d ) {
                d.items_id = items_id;
                // d.country = $('#country').val();
                // d.provinces =$('#provinces').val();
                // d.districts =$('#districts').val();
            }, 
        },

    }); 
    $(".dataTables_filter").hide();
    $('#CustomSearchBox').keyup(function(){
        table.search($(this).val()).draw() ;
    });

    $('#ViewItemsModal,#ViewAddProductModal').on('hidden.bs.modal', function (e) { 
       
        table.ajax.reload(null, false);
        setTimeout( function () {
             var $owl_old = $(".owl-carousel");
            $owl_old.trigger('destroy.owl.carousel');
            $owl_old.html($owl_old.find('.owl-stage-outer').html()).removeClass('owl-loaded');
            var $owl = $(".owl-carousel");
            $owl.owlCarousel({
                items : 1, 
                loop:false,
                margin:5,
                nav:false
            });
        }, 500 );

        $(".gallery-product").remove();
        image_hotelroom = 0;
        array_image_hotelroom = [];

     })
     $(document).on("click", "#btn_save_product", function () {
        var formData = new FormData();

        for (var i = 0; i < image_hotelroom; i++) {
            formData.append("product_namesThai_"+(i+1),$("#name_hotelroom_"+(i+1)).val());
            formData.append("product_namesEnglish_"+(i+1),$("#name_Eng_hotelroom_"+(i+1)).val());
            formData.append("product_namesLaos_"+(i+1),$("#name_Laos_hotelroom_"+(i+1)).val());
            formData.append("product_namesChinese_"+(i+1),$("#name_Chinese_hotelroom_"+(i+1)).val());

            formData.append("product_price_"+(i+1),($("#price_hotelroom_"+(i+1)).val() == ""?"0":$("#price_hotelroom_"+(i+1)).val()));

            formData.append("product_descriptionThai_"+(i+1),$("#details_hotelroom_"+(i+1)).val());
            formData.append("product_descriptionEnglish_"+(i+1),$("#details_Eng_hotelroom_"+(i+1)).val());
            formData.append("product_descriptionLaos_"+(i+1),$("#details_Laos_hotelroom_"+(i+1)).val());
            formData.append("product_descriptionChinese_"+(i+1),$("#details_Chinese_hotelroom_"+(i+1)).val());
            formData.append("product_breakfast_"+(i+1),document.querySelector('input[name="breakfast_hotelroom_'+(i+1)+'"]:checked').value);

            for (var j = 0; j < array_image_hotelroom[i]; j++) {
                formData.append("img_image_hotelroom_"+(i+1)+"_fname_"+(j+1),document.getElementById("img_image_hotelroom_"+(i+1)+"_fname_"+(j+1)).files[0]);
            }
        }

        formData.append('image_hotelroom',image_hotelroom);
        formData.append('array_image_hotelroom',array_image_hotelroom);
        formData.append('items_id',items_id);

        for (var pair of formData.entries()) {
            console.log(pair[0]+ ', ' + pair[1]); 
          }
          $.ajax({
            type: "POST",
            url: host_entrepreneur+'/business/'+MenuName+'/saveNewProduct',
            data: formData,
            contentType: false,
            processData:false,
            success: function (response) {
                document.getElementById("status").style.display = "none";
                document.getElementById("preloader").style.display = "none";
                $('#ViewAddProductModal').modal('hide');
            },
            beforeSend:function(){
                document.getElementById("status").style.display = "";
                document.getElementById("preloader").style.display = "";
            }
        })
     })
});