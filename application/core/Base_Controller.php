<?php

class Base_Admin_Controller extends CI_Controller {
	public function __construct(){
		parent::__construct();

		$this->load->helper('url');
		$this->load->library('session');

		if ( ($this->session->userdata('logged_in') == false))
        {
            redirect("http://".$_SERVER['HTTP_HOST']."/dasta_thailand/html");
		}
		// else
		// {
		// 	redirect("http://".$_SERVER['HTTP_HOST']."/dasta_thailand/html/Main", 'refresh');
		// }
	}
}

class Base_Entrepreneur_Controller extends CI_Controller {
	public function __construct(){
		parent::__construct();

		$this->load->helper('url');
		$this->load->library('session');

		if ( ($this->session->userdata('entrepreneur_logged_in') == false))
        {
            redirect("http://".$_SERVER['HTTP_HOST']."/dasta_thailand/entrepreneur");
		}
		// else
		// {
		// 	redirect("http://".$_SERVER['HTTP_HOST']."/dasta_thailand/html/Main", 'refresh');
		// }
	}
}

?>