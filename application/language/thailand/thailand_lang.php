<?php
    //sidebar_menu
    $lang['text_edit_infor'] = 'แก้ไขข้อมูล';
    $lang['changePassword'] = 'แก้ไขรหัสผ่าน';
    $lang['Logout'] = 'ออกจากระบบ';

//////////////////////////////////////////////////////////////////////////////////
    $lang['sidebar_menu_main'] = 'หน้าแรก';
    $lang['sidebar_menu_ProgramTour'] = 'โปรแกรมการท่องเที่ยว';
    $lang['sidebar_menu_Attractions'] = 'สถานที่ท่องเที่ยว';
    $lang['sidebar_menu_Restaurant'] = 'ร้านอาหารแนะนำ';
    $lang['sidebar_menu_Shopping'] = 'ช้อปปิ้งที่ระลึก';
    $lang['sidebar_menu_Hotel'] = 'ที่พักโรงแรม';
    $lang['sidebar_menu_ImportantContactPlaces'] = 'สถานที่ติดต่อสำคัญ';

//////////////////////////////////////////////////////////////////////////////////
    $lang['sidebar_menu_MediaAndAdvertising'] = 'สื่อและโฆษณา';
    $lang['sidebar_menu_VideoContent'] = 'วิดีโอคอนเทนต์ที่น่าสนใจ';
    $lang['sidebar_menu_Sticker'] = 'สติ๊กเกอร์';
    $lang['sidebar_menu_Coupon'] = 'คูปอง/โค๊ดส่วนลด';
    $lang['sidebar_menu_CoverProgramTourOfficial'] = 'หน้าปกโปรแกรมท่องเที่ยว Official';
    $lang['sidebar_menu_News'] = 'ข่าวสารประชาสัมพันธ์';
    $lang['sidebar_menu_CarRentAds'] = 'โฆษณารถเช่า';
    $lang['sidebar_menu_PackageToursAds'] = 'โฆษณาแพคเกจทัวร์';
    $lang['sidebar_menu_EventTicketAds'] = 'โฆษณาตั๋วกิจกรรม';
    $lang['sidebar_menu_HotelAds'] = 'โฆษณาที่พัก/โรงแรม';

//////////////////////////////////////////////////////////////////////////////////
    $lang['sidebar_menu_Reservations'] = 'การจองบริการต่าง ๆ';
    $lang['sidebar_menu_PackageTours'] = 'แพคเกจทัวร์';
    $lang['sidebar_menu_EventTicket'] = 'ตั๋วกิจกรรม';
    $lang['sidebar_menu_CarRent'] = 'บริการรถเช่า';

//////////////////////////////////////////////////////////////////////////////////
    $lang['sidebar_menu_Booking'] = 'รายการคำสั่งจอง';
    $lang['sidebar_menu_BookingPackageTours'] = 'คำสั่งจองแพคเกจทัวร์';
    $lang['sidebar_menu_BookingEventTicket'] = 'คำสั่งจองตั๋วกิจกรรม';
    $lang['sidebar_menu_BookingCarRent'] = 'คำสั่งจองบริการรถเช่า'; 

//////////////////////////////////////////////////////////////////////////////////
    $lang['sidebar_menu_Approval'] = 'การอนุมัติ';
    $lang['sidebar_menu_ApprovalProgramTour'] = 'โปรแกรมการท่องเที่ยว';
    $lang['sidebar_menu_ApprovalAttractions'] = 'สถานที่ท่องเที่ยว';
    $lang['sidebar_menu_ApprovalRestaurant'] = 'ร้านอาหาร';
    $lang['sidebar_menu_ApprovalShopping'] = 'ช้อปปิ้งของที่ระลึก';
    $lang['sidebar_menu_ApprovalHotel'] = 'ที่พักโรงแรม';
    $lang['sidebar_menu_ApprovalPackageTours'] = 'แพคเกจทัวร์';
    $lang['sidebar_menu_ApprovalEventTicket'] = 'ตั๋วกิจกรรม';
    $lang['sidebar_menu_ApprovalCarRent'] = 'บริการรถเช่า';
    
//////////////////////////////////////////////////////////////////////////////////
    $lang['ApprovalStateBar_All'] = "ทั้งหมดในระบบ";
    $lang['ApprovalStateBar_Approved'] = "ที่อนุมัติในระบบ";
    $lang['ApprovalStateBar_Pending'] = "ที่รอการอนุมัติ";
    $lang['ApprovalStateBar_Not'] = "ที่ไม่ผ่านการอนุมัติ";
//////////////////////////////////////////////////////////////////////////////////
    $lang['sidebar_menu_Notifications'] = 'แชท';
    $lang['notifications'] = 'การแจ้งเตือน';
//////////////////////////////////////////////////////////////////////////////////
// Country
    $lang['country_th'] = 'ประเทศไทย';
    $lang['country_lao'] = 'ประเทศลาว';
    $lang['unit'] = 'แห่ง';
    $lang['head_main_pic_chart'] = 'จังหวัด/เมือง';

//////////////////////////////////////////////////////////////////////////////////
// Tab Language
    $lang['lang_th'] = 'ข้อมูลภาษาไทย';
    $lang['lang_en'] = 'ข้อมูลภาษาอังกฤษ';
    $lang['lang_lao'] = 'ข้อมูลภาษาลาว';
    $lang['lang_chn'] = 'ข้อมูลภาษาจีน';

//////////////////////////////////////////////////////////////////////////////////
    // button
    $lang['save'] = 'บันทึก';
    $lang['close'] = 'ยกเลิก';
    $lang['edit'] = 'แก้ไข';
    $lang['delete'] = 'ลบ'; 
    $lang['add'] = 'เพิ่ม'; 
    $lang['view'] = 'ดู'; 
    $lang['stop_publishing'] = 'หยุดเผยแพร่'; 
    $lang['publish'] = 'เผยแพร่'; 
    $lang['list'] = 'รายการ'; 
    
   

//////////////////////////////////////////////////////////////////////////////////
// add item
    $lang['name_place'] = 'ชื่อสถานที่';
    $lang['name_location_type'] = 'ประเภทสถานที่';
    $lang['address'] = 'ที่อยู่';
    $lang['name_date'] = 'วันที่';
    $lang['status'] = 'สถานะ';
//////////////////////////////////////////////////////////////////////////////////
// Other
    $lang['inSystem'] = 'ในระบบ';
    $lang['search'] = 'ค้นหา';
    $lang['category'] = 'ประเภท';
    $lang['country'] = 'ประเทศ';
    $lang['province'] = 'จังหวัด';
    $lang['district'] = 'อำเภอ';
    $lang['clear_data'] = 'ล้างข้อมูล';
    $lang['preview_pictures'] = 'ดูตัวอย่างรูปภาพ';
    $lang['cover_photo'] = 'ภาพหน้าปก';
    $lang['adjustments_on_pictures'] = 'สามารถทำการปรับแต่งรูปภาพได้';
//////////////////////////////////////////////////////////////////////////////////
    $lang['place_category'] = 'หมวดหมู่สถานที่';
    $lang['duration_of_the_excursion'] = 'ระยะวันของโปรแกรมการท่องเที่ยว';
    $lang['travel_program_type'] = 'ประเภทโปรแกรมท่องเที่ยว';
    $lang['add_tourism_program'] = 'เพิ่มโปรแกรมการท่องเที่ยว';
    $lang['day_period'] = 'ระยะวัน';
    $lang['travel_program_name'] = 'ชื่อโปรแกรมการท่องเที่ยว';
    $lang['scope'] = 'ขอบเขต';
    $lang['update_date'] = 'วันที่อัปเดต';
    $lang['tools'] = 'เครื่องมือ';
    $lang['added_cover'] = 'เพิ่มภาพหน้าปกโปรแกรมท่องเที่ยว';
//////////////////////////////////////////////////////////////////////////////////
?>