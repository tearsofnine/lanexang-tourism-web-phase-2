<?php
//sidebar_menu
    $lang['text_edit_infor'] = 'Edit Information';
    $lang['changePassword'] = 'Change password';
    $lang['Logout'] = 'Logout';

//////////////////////////////////////////////////////////////////////////////////
    $lang['sidebar_menu_main'] = 'Index';
    $lang['sidebar_menu_ProgramTour'] = 'Tourism Program';
    $lang['sidebar_menu_Attractions'] = 'Attractions';
    $lang['sidebar_menu_Restaurant'] = 'Restaurant';
    $lang['sidebar_menu_Shopping'] = 'Shopping';
    $lang['sidebar_menu_Hotel'] = 'Hotel';
    $lang['sidebar_menu_ImportantContactPlaces'] = 'Important Contact Places';

//////////////////////////////////////////////////////////////////////////////////
    $lang['sidebar_menu_MediaAndAdvertising'] = 'Media And Advertising';
    $lang['sidebar_menu_VideoContent'] = 'Video Content';
    $lang['sidebar_menu_Sticker'] = 'Sticker';
    $lang['sidebar_menu_Coupon'] = 'Coupon';
    $lang['sidebar_menu_CoverProgramTourOfficial'] = 'Cover Tour Program Official';
    $lang['sidebar_menu_News'] = 'News';
    $lang['sidebar_menu_CarRentAds'] = 'Car Rent Ads';
    $lang['sidebar_menu_PackageToursAds'] = 'Tours Package Ads';
    $lang['sidebar_menu_EventTicketAds'] = 'Event Ticket Ads';
    $lang['sidebar_menu_HotelAds'] = 'Hotel Ads';

//////////////////////////////////////////////////////////////////////////////////
    $lang['sidebar_menu_Reservations'] = 'Reservations';
    $lang['sidebar_menu_PackageTours'] = 'Tours Package';
    $lang['sidebar_menu_EventTicket'] = 'Event Ticket';
    $lang['sidebar_menu_CarRent'] = 'Car Rent';

//////////////////////////////////////////////////////////////////////////////////
    $lang['sidebar_menu_Booking'] = 'Booking';
    $lang['sidebar_menu_BookingPackageTours'] = 'Booking Package Tours';
    $lang['sidebar_menu_BookingEventTicket'] = 'Booking Event Ticket';
    $lang['sidebar_menu_BookingCarRent'] = 'Booking Car Rental'; 

//////////////////////////////////////////////////////////////////////////////////
    $lang['sidebar_menu_Approval'] = 'Approval';
    $lang['sidebar_menu_ApprovalProgramTour'] = 'Tour Program ';
    $lang['sidebar_menu_ApprovalAttractions'] = 'Attractions';
    $lang['sidebar_menu_ApprovalRestaurant'] = 'Restaurant';
    $lang['sidebar_menu_ApprovalShopping'] = 'Shopping';
    $lang['sidebar_menu_ApprovalHotel'] = 'Hotel';
    $lang['sidebar_menu_ApprovalPackageTours'] = 'Package Tours';
    $lang['sidebar_menu_ApprovalEventTicket'] = 'EventTicket';
    $lang['sidebar_menu_ApprovalCarRent'] = 'Car Rent';
    
//////////////////////////////////////////////////////////////////////////////////
    $lang['ApprovalStateBar_All'] = "All in the system";
    $lang['ApprovalStateBar_Approved'] = "Approved in the system";
    $lang['ApprovalStateBar_Pending'] = "Pending approval";
    $lang['ApprovalStateBar_Not'] = "Not approved";
//////////////////////////////////////////////////////////////////////////////////
    $lang['sidebar_menu_Notifications'] = 'Notifications';
    $lang['notifications'] = 'Notifications';
//////////////////////////////////////////////////////////////////////////////////
// Country
    $lang['country_th'] = 'Thailand';
    $lang['country_lao'] = 'Laos';
    $lang['unit'] = 'place';
    $lang['head_main_pic_chart'] = 'Province / City';
//////////////////////////////////////////////////////////////////////////////////
// Tab Language
    $lang['lang_th'] = 'Thai information';
    $lang['lang_en'] = 'English information';
    $lang['lang_lao'] = 'Laos information';
    $lang['lang_chn'] = 'Chinese information';

//////////////////////////////////////////////////////////////////////////////////
// button
    $lang['save'] = 'Save';
    $lang['close'] = 'Close';
    $lang['edit'] = 'Edit';
    $lang['delete'] = 'Delete';
    $lang['add'] = 'Add'; 
    $lang['view'] = 'View'; 
    $lang['stop_publishing'] = 'Stop publishing'; 
    $lang['publish'] = 'Publish'; 
    $lang['list'] = 'List'; 
    $lang['status'] = 'Status'; 
   
//////////////////////////////////////////////////////////////////////////////////
// add item
    $lang['name_place'] = 'Name place';
    $lang['location_type'] = 'Location type';
    $lang['name_location_type'] = 'Category';
    $lang['address'] = 'Address';
    $lang['date'] = 'Date';

//////////////////////////////////////////////////////////////////////////////////
// Other
    $lang['inSystem'] = 'in system';
    $lang['search'] = 'Search';
    $lang['category'] = 'Category';
    $lang['country'] = 'Country';
    $lang['province'] = 'Province';
    $lang['district'] = 'District';
    $lang['clear_data'] = 'Clear data';
    $lang['preview_pictures'] = 'Preview pictures';
    $lang['cover_photo'] = 'Cover photo';
    $lang['adjustments_on_pictures'] = 'Able to make adjustments on pictures';
//////////////////////////////////////////////////////////////////////////////////
    $lang['place_category'] = 'Place category';
    $lang['duration_of_the_excursion'] = 'Duration of the excursion';
    $lang['travel_program_type'] = 'Travel program type';
    $lang['add_tourism_program'] = 'Add Tourism program';
    $lang['day_period'] = 'Day period';
    $lang['travel_program_name'] = 'Travel program name';
    $lang['scope'] = 'scope';
    $lang['update_date'] = 'Update date';
    $lang['tools'] = 'Tools';
    $lang['added_cover'] = 'Added a travel program cover image';
//////////////////////////////////////////////////////////////////////////////////
?>