<?php
date_default_timezone_set('Asia/Bangkok');
class ProgramTourModels extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->model("UserModel","user_model", true);
        $this->load->model("ReviewModels","review_models", true);
        $this->load->model("LikesModels","likes_models", true);
    }
    public function AddProgramTourData($data,$table)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }
    public function UpdateProgramTourData($data,$table,$where)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to edit ProgramTour",
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "ProgramTour edited successfully",
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "ProgramTour edited successfully, but it looks like you haven't updated anything!",
                    'state' => true
                );
            }
        }
        return $res;
    }
    public function DeleteProgramTourData($table,$where)
    {
        $this->db->delete($table, $where);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to Delete",
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "Delete successfully",
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "Delete successfully, but it looks like you haven't updated anything!",
                    'state' => true
                );
            }
        }
        return $res;
    }
    public function GetNumDatesTripEntrepreneur($user_id)
    {
        $str_select ='DISTINCT(COUNT(dt.ProgramTour_programTour_id)) as NumDatesTrip';
        $this->db->select($str_select);
        $this->db->from('DatesTrip dt');
        $this->db->join('ProgramTour pt','pt.programTour_id = dt.ProgramTour_programTour_id');
        $this->db->where("pt.programTour_isActive = 1 AND pt.User_user_id = ".$user_id);
        $this->db->group_by("dt.ProgramTour_programTour_id");
        $this->db->order_by('NumDatesTrip', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    public function GetNumDatesTrip()
    {
        $str_select ='DISTINCT(COUNT(dt.ProgramTour_programTour_id)) as NumDatesTrip';
        $this->db->select($str_select);
        $this->db->from('DatesTrip dt');
        $this->db->join('ProgramTour pt','pt.programTour_id = dt.ProgramTour_programTour_id');
        $this->db->where("pt.programTour_isActive = 1  and pt.programTour_isPublish = 1 and pt.PublishStatus_publishStatus_id = 1");
        $this->db->group_by("dt.ProgramTour_programTour_id");
        $this->db->order_by('NumDatesTrip', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    public function GetPhotoTouristSubData($table,$where)
    {
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->result();
    }
    public function GetTouristAttractionsData($where)
    {
        $query_data = $this->GetPhotoTouristSubData("TouristAttractions",$where);
        for($i = 0;$i < count($query_data);$i++ )
        {
            $query_data[$i]->ItemDetail = $this->items_models->GetItemData("i.items_id = ".$query_data[$i]->Items_items_id);
            $query_data[$i]->PhotoTourist = $this->GetPhotoTouristSubData("PhotoTourist","TouristAttractions_id = ". $query_data[$i]->touristAttractions_id);
        }
        return $query_data;
    }
    public function GetDatesTripData($where)
    {
        $query_data = $this->GetPhotoTouristSubData("DatesTrip",$where);
        for($i = 0;$i < count($query_data);$i++ )
        {
            $query_data[$i]->TouristAttractions = $this->GetTouristAttractionsData("DatesTrip_datesTrip_id = ". $query_data[$i]->datesTrip_id);
        }
        return $query_data;
    }
    public function GetProgramTourDataReview($where)
    {
        $dataJson ='[{
            "AverageReview":0,
            "CountReview":0,
            "OneStar":0,
            "TwoStar":0,
            "ThreeStar":0,
            "FourStar":0,
            "FiveStar":0,
            "Reviews":[]
        }]';
        $data = json_decode($dataJson);

        $ReviewRating = $this->review_models->GetReviewRatingData("review_isActive =1 AND ".$where);
        $Reviews = $this->review_models->GetReviewData("rv.review_isActive =1 AND ".$where,3);

        $data[0]->AverageReview = intval($ReviewRating[0]->AverageReview);
        $data[0]->CountReview = intval($ReviewRating[0]->CountReview);
        $data[0]->OneStar = intval($ReviewRating[0]->OneStar);
        $data[0]->TwoStar = intval($ReviewRating[0]->TwoStar);
        $data[0]->ThreeStar = intval($ReviewRating[0]->ThreeStar);
        $data[0]->FourStar = intval($ReviewRating[0]->FourStar);
        $data[0]->FiveStar = intval($ReviewRating[0]->FiveStar);

        $i =0;
        foreach($Reviews as $r) {

            $data[0]->Reviews[] = $r;
            $originalDateTime = $r->review_timestamp;
            $newDate = date("d M Y", strtotime($originalDateTime));
            $newTime = date("h:i A", strtotime($originalDateTime));
            $newDateTime = $newDate." at ".$newTime;

            $data[0]->Reviews[$i]->review_timestamp = $newDateTime;
            $i++;
        }
        
        return $data;
    }
    public function siteURL() {
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $domainName = $_SERVER['HTTP_HOST'] . '/';
        return $protocol . $domainName;
    }
    public function GetProgramTourDataUser($where)
    {
        $query_user =  $this->user_model->GetUserData($where);
        $dataJson ='[{
            "user_id":0,
            "user_firstName":"",
            "user_lastName":"",
            "user_profile_pic_url":"",
            "official":0
            
        }]';
        $data = json_decode($dataJson);
        $data[0]->user_id = $query_user[0]->user_id;
        
        if(intval($query_user[0]->UserRole_userRole_id) <=3)
        {
            $data[0]->user_firstName = "Administrator";
            $data[0]->user_lastName = "";
            $data[0]->user_profile_pic_url =  $this->siteURL()."dasta_thailand/assets/img/logo.png";
            // $data[0]->user_profile_pic_url = $query_user[0]->user_profile_pic_url;
            $data[0]->official = true;
        }
        else
        {
            $data[0]->user_firstName = $query_user[0]->user_firstName;
            $data[0]->user_lastName = $query_user[0]->user_lastName;
            $data[0]->user_profile_pic_url = $query_user[0]->user_profile_pic_url;
            $data[0]->official = false;
        }
        return $data;
    }
    
    public function GetProgramTourData($where,$user_id = -1)
    {
        $str_select ='';
        $str_select .='pt.*,mi.*,phsc.*,sc.*,cat.*,ct.*,tic.*';
        $this->db->select($str_select);
        $this->db->from('ProgramTour pt');
        $this->db->join('MenuItem mi','mi.menuItem_id = pt.MenuItem_menuItem_id');
        $this->db->join('ProgramTour_has_SubCategory phsc','phsc.ProgramTour_programTour_id = pt.programTour_id');
        $this->db->join('SubCategory sc','sc.subcategory_id = phsc.SubCategory_subcategory_id');
        $this->db->join('Category cat','cat.category_id = sc.Category_category_id');
        $this->db->join('CoverItems ct','ct.ProgramTour_programTour_id = pt.programTour_id');
        $this->db->join('TempImageCover tic','tic.CoverItems_coverItem_id = ct.coverItem_id');
        $this->db->order_by('pt.programTour_id', 'DESC');
        $this->db->where($where);
        $query = $this->db->get();
        $query_data = $query->result();
        for($i = 0;$i < count($query_data);$i++ )
        {
            $query_data[$i]->programTour_createdDTTM = date("d M Y", strtotime($query_data[$i]->programTour_createdDTTM))." at ". date("h:i A", strtotime($query_data[$i]->programTour_createdDTTM));
            $query_data[$i]->DatesTrip = $this->GetDatesTripData("ProgramTour_programTour_id = ".$query_data[$i]->programTour_id);
            $query_data[$i]->Review = $this->GetProgramTourDataReview("ProgramTour_programTour_id = ".$query_data[$i]->programTour_id);
            $query_data[$i]->User = $this->GetProgramTourDataUser("user_id = ".$query_data[$i]->User_user_id);
            
            if($user_id != -1)
            {
                $query = $this->likes_models->GetLikesData("User_user_id = ".$user_id." AND  ProgramTour_programTour_id = ".$query_data[$i]->programTour_id);
                $query_data[$i]->Like_state = ($query?true:false);
            }
            else
            {
                $query_data[$i]->Like_state = false;
            }
            $query = $this->likes_models->GetLikesData("ProgramTour_programTour_id = ".$query_data[$i]->programTour_id);
            $query_data[$i]->Like_count = count($query);
    
        }
        return $query_data;
    }
    
    public function setDatatable($where)
    {
        $item = $this->GetProgramTourData($where);
        $data = array();
        foreach($item as $key => $r) {
            $data[] = array(
               "",
                $r->subcategory_thai,
                count( $r->DatesTrip)." วัน",/*ระยะวัน*/
                $r->programTour_nameThai,/*ชื่อโปรแกรมการท่องเที่ยว*/
                $this->GetScope($r->DatesTrip),/*ขอบเขต*/
                $this->GetStatus($r->programTour_time_period_start,$r->programTour_time_period_end,$r->PublishStatus_publishStatus_id).'
                </br>
                '.$this->ShowData($r->programTour_time_period_start,$r->programTour_time_period_end,$r->PublishStatus_publishStatus_id),/*สถานะ*/
                $r->programTour_updatedDTTM,/*วันที่อัปเดต*/
                '
                
                <button type="button" class="btn btn-outline btn-default btn-sm ViewItemsModal" data-toggle="modal" data-target="#ViewItemsModal" value="'.$r->programTour_id.'"> 
                    <i class="fa fa-folder"></i> '.$this->lang->line("view").'
                </button>
                <a href="./ProgramTour/editProgramTour?programTour_id='.$r->programTour_id.'" class="btn btn-outline btn-warning btn-sm"><i class="fa fa-pencil"></i> '.$this->lang->line("edit").' </a>
                <button class="btn btn-outline btn-danger btn-sm btn-program-tour-del" value="'.$r->programTour_id.'"><i class="fa fa-trash-o"></i> ลบ </button>',/*เครื่องมือ*/
                $r->programTour_id
            );
        }
        return $data;
    }
    public function GetScope($DatesTrip)
    {
        $scope = [];
        foreach($DatesTrip as $key => $_DatesTrip) {
            foreach($_DatesTrip->TouristAttractions as $key => $_TouristAttractions) {
                if(!in_array( $_TouristAttractions->ItemDetail[0]->provinces_thai, $scope))
                    $scope[] = $_TouristAttractions->ItemDetail[0]->provinces_thai;
            }
        }
        return join("-",$scope);
    }
    private function ShowData($start,$end,$publishStatus_id)
    {
        if( $start != "1970-01-01" && $end != "1970-01-01")
        {
            if(intval($publishStatus_id) == 0)
                return '';
            else
                return '<small>'.date('d/m/Y',strtotime($start)).' ถึง '.date('d/m/Y',strtotime($end)).'</small>';
        }
        else
        {
            if(intval($publishStatus_id) == 0)
                return '';
            else
                return "ไม่กำหนดช่วงเวลา";
        }
            
    }
    private function GetStatus($start,$end,$publishStatus_id)
    {
        $status ='';
        if(intval($publishStatus_id) == 1)
        {
            if (date('Y-m-d') >= date('Y-m-d',strtotime($start)) && date('Y-m-d') <= date('Y-m-d',strtotime($end))) {
                $status ='<span class="label label-primary">แสดงแล้ว</span>';
            }
            elseif(date('Y-m-d') < date('Y-m-d',strtotime($start)) && $start != "1970-01-01" )
            {
                $status ='<span class="label label-warning">ไม่ถึงช่วงเวลา</span>';
            }
            elseif(date('Y-m-d') > date('Y-m-d',strtotime($end)) && $end != "1970-01-01" )
            {
                $status ='<span class="label label-danger">เกินช่วงเวลา</span>';
            }
            else
            {
                $status ='<span class="label label-primary">แสดงแล้ว</span>';
            }
        }
        elseif (intval($publishStatus_id) == 2) {
            $status ='<span class="label label-danger">ที่ไม่ผ่านการอนุมัติ</span>';
        }
        elseif (intval($publishStatus_id) == 0) {
            $status ='<span class="label label-warning">รอการอนุมัติ</span>';
        }
        return $status;
    }
    public function GetDefaultProgramTourData($where) 
    {
       
        $str_select ='pt.*,u.*';
        $this->db->select($str_select);
        $this->db->from('ProgramTour pt');
        $this->db->join('User u','u.user_id = pt.User_user_id');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
      
    }

    public function CountApprovalProgramTourState($where) 
    {
        $ProgramTourData = $this->GetDefaultProgramTourData($where);

        $ApprovalProgramTourState = array (
            "All"=>0,
            "Approved"=>0,
            "Refuse"=>0
        );
        if(!is_null($ProgramTourData))
        {
            foreach($ProgramTourData as $key => $subArray)
            {
                foreach($subArray as $subArraykey => $value)
                {
                    if(strcmp($subArraykey,"PublishStatus_publishStatus_id")==0)
                    {
                        switch (intval($value)) {
                            case 1:
                                $ApprovalProgramTourState["Approved"] += 1;
                                break;
                            case 2:
                                $ApprovalProgramTourState["Refuse"] += 1;
                                break;
                        }
                    }
                }
            }
            $ApprovalProgramTourState["All"] = count($ProgramTourData);
        }

        return  $ApprovalProgramTourState;
    }
    public function getTouristProgramTalksAboutThisPlace($items_id,$user_id = -1)
    {
        $where ="programTour_id in (";
        $where .="select ProgramTour_programTour_id ";
        $where .="from DatesTrip ";
        $where .="where datesTrip_id in ( ";
        $where .="select DatesTrip_datesTrip_id ";
        $where .="from TouristAttractions ";
        $where .="where Items_items_id = ".$items_id." ";
        $where .="group by DatesTrip_datesTrip_id ) ";
        $where .="group by ProgramTour_programTour_id )";
        
        $this->db->select("programTour_id");
        $this->db->from('ProgramTour');
        $this->db->where($where);
        $this->db->group_by("programTour_id");
        $query = $this->db->get();
        $programTour_data = $query->result();
        if($programTour_data != null)
        {
            foreach($programTour_data as $programTour)
            {
                $programTour_id [] = $programTour->programTour_id;
            }
            return $this->program_tour_models->GetProgramTourData("pt.programTour_isActive = 1 AND pt.programTour_isPublish = 1 AND pt.PublishStatus_publishStatus_id = 1 AND pt.programTour_isSaveDraft = 0
                AND (NOW() BETWEEN pt.programTour_time_period_start AND pt.programTour_time_period_end OR (pt.programTour_time_period_start = '1970-01-01' AND pt.programTour_time_period_end = '1970-01-01' ))
                AND programTour_id IN (".join(",",$programTour_id).")",($user_id != -1?$user_id:-1));
        }
        else
        {
            return array();
        }
        
    }
}
?>