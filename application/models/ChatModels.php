<?php
date_default_timezone_set('Asia/Bangkok');
class ChatModels extends CI_Model {
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model("UserModel","user_model", true);

    }

    public function create_chat_room($UserSend, $UserReceive,$items_id = -1) 
    {
        $data = array(
            "chatroom_name"=>$UserSend."->".$UserReceive,
            "UserSend_user_id"=>$UserSend,
            "UserReceive_user_id"=> $UserReceive,
            "Items_items_id"=>($items_id == -1?Null:$items_id),
        );
        $this->db->insert('ChatRooms',$data);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to Create ChatRooms",
                'chatroom_id' => -1,
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "Create ChatRooms successfully",
                    'chatroom_id' => $this->db->insert_id(),
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "Create ChatRooms successfully, but it looks like you haven't updated anything!",
                    'chatroom_id' => 0,
                    'state' => true
                );
            }
        }
        return $res;
    }
    
    public function get_chat_room($where,$UserSend, $UserReceive,$items_id = -1) 
    {
        $this->db->where($where);
        $query = $this->db->get('ChatRooms');
        $query_data = $query->result();
        if(count($query_data) == 0 && $UserSend != -1 && $UserReceive != -1)
        {
            $this->create_chat_room($UserSend, $UserReceive,$items_id );
            sleep(1);
            $this->db->where($where);
            $query = $this->db->get('ChatRooms');
            $query_data = $query->result();
        }
        return $query_data;
    }
    
    public function add_messages($data) 
    {
        $this->db->insert('Messages',$data);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to Add Messages",
                'messages_id' => -1,
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "Add Messages successfully",
                    'messages_id' => $this->db->insert_id(),
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "Add Messages successfully, but it looks like you haven't updated anything!",
                    'messages_id' => 0,
                    'state' => true
                );
            }
        }
        return $res;
    }

    public function update_messages($data,$where) 
    {
        $this->db->where($where);
        $this->db->update("Messages", $data);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to edit Messages",
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "Messages edited successfully",
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "Messages edited successfully, but it looks like you haven't updated anything!",
                    'state' => true
                );
            }
        }
        return $res;
    }

    public function get_messages($where,$last = false)
    {
        $this->db->select("msg.* ,u.*");
        $this->db->from('Messages msg');
        $this->db->join('User u','u.user_id = msg.User_user_id');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_chat_room_for_table($where,$user_id)
    {
        $this->db->select("cr.*,msg.*,msg_id.count_read");
        $this->db->from('(SELECT MAX(messages_id) as messages_id,sum(case when messages_read = 0 AND User_user_id != '.$user_id.' then 1 else 0 end) AS count_read FROM Messages GROUP BY ChatRooms_chatroom_id) msg_id'); 
        $this->db->join('Messages msg','msg.messages_id = msg_id.messages_id');
        $this->db->join('ChatRooms cr','cr.chatroom_id = msg.ChatRooms_chatroom_id');
        $this->db->where($where);
        $this->db->order_by('msg.message_created_at DESC');
        $query = $this->db->get();
        $query_data = $query->result();
        for($i = 0;$i < count($query_data);$i++ )
        {
            $query_data[$i]->UserSend =$this->user_model->GetUserData("user_id = ".$query_data[$i]->UserSend_user_id);
            $query_data[$i]->UserReceiv =$this->user_model->GetUserData("user_id = ".$query_data[$i]->UserReceive_user_id);
            if($query_data[$i]->Items_items_id != NULL)
                $query_data[$i]->Items = $this->items_models->GetMinimalItemsData("i.items_isActive = 1 AND i.items_isPublish = 1 AND i.items_id = " .$query_data[$i]->Items_items_id);
           //$query_data[$i]->Business = $this->user_model->GetUserJoinBusinessDataForChat("u.user_id = ".$user_id);
        }
        return $query_data;
    }

    public function getUnReadMessages($user_id) 
    {
        $this->db->select("sum(case when messages_read = 0 then 1 else 0 end) AS count_read");
        $this->db->from('Messages');
        $this->db->where("ChatRooms_chatroom_id in (SELECT chatroom_id from ChatRooms where UserSend_user_id = ".$user_id." OR UserReceive_user_id = ".$user_id.") AND User_user_id != ".$user_id);
        $query = $this->db->get();
        return $query->result();
    }
}