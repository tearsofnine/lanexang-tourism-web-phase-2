<?php
date_default_timezone_set('Asia/Bangkok');
class CountriesModels extends CI_Model {
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
    }
    public function getCountry($superAdmin = false,$where)
    {
        if(!$superAdmin)
        {
            $this->db->where_in('country_id',$where);    
            $query = $this->db->get("Country");
        }
        else
        {
            $query = $this->db->get("Country");
        }
        return $query->result();
    }
    public function getProvinces($superAdmin = false,$whereCountry,$whereProvinces)
    {
        if(!$superAdmin)
        {
            $this->db->where_in('provinces_id',$whereProvinces); 
            $this->db->where('Country_country_id',$whereCountry); 
            $query = $this->db->get("Provinces");
        }
        else
        {
            $this->db->where($whereCountry);
            $query = $this->db->get("Provinces");
        }
        return $query->result();
    }
    public function getDistricts($where){
        $this->db->where($where);
        $query = $this->db->get("Districts");
        return $query->result();
    }
    public function getSubdistricts($where){
        $this->db->where($where);
        $query = $this->db->get("Subdistricts");
        return $query->result();
    }
    public function getNameLocation($querys,$where)
    {
        $this->db->where($where);
        return $this->db->get($querys);
    }
    public function getDataCountries($table,$where="")
    {
        if($where != "")
        {
            $this->db->where($where);
        }
        $query = $this->db->get($table);
        return $query->result();
    }
}
?>