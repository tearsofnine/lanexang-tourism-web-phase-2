<?php
date_default_timezone_set('Asia/Bangkok');
class FollowersModels extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
    }
    public function Getfollowing($user_id)
    {
        $this->db->select('u.*,fwing.*');
        $this->db->from('Followers fwing'); 
        $this->db->join('User u','fwing.followers_user_id = u.user_id','');
        $this->db->where(array('fwing.User_user_id'=>$user_id));
        return $this->db->get();
    }
    public function Getfollowers($user_id)
    {
        $this->db->select('u.*,fwer.*');
        $this->db->from('Followers fwer'); 
        $this->db->join('User u','fwer.User_user_id = u.user_id','');
        $this->db->where(array('fwer.followers_user_id'=>$user_id));
        return $this->db->get();
    }
    public function GetFollowersData($where)
    {
        $this->db->where($where);
        $query = $this->db->get('Followers');
        return $query->result();
    }
    public function AddFollowersData($data)
    {
        $this->db->insert("Followers", $data);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to Followers",
                'insert_id' =>-1,
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "User Followers successfully",
                    'insert_id' => $this->db->insert_id(),
                    'state' => true,
					'follow_state' => true,
                );
            }
            else
            {
                $res = array(
                    'msg' => "User Followers successfully, but it looks like you haven't updated anything!",
                    'insert_id' => 0,
                    'state' => true
                );
            }
        }
        return  $res;
    }

    public function UpdateFollowersData($data,$where)
    {
        $this->db->where($where);
        $this->db->update("Followers", $data);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to edit Followers",
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "Followers edited successfully",
                    'state' => true,
                    
                );
            }
            else
            {
                $res = array(
                    'msg' => "Followers edited successfully, but it looks like you haven't updated anything!",
                    'state' => true
                );
            }
        }
        return $res;
    }

    public function DeleteFollowersData($where)
    {
        $this->db->delete("Followers", $where);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to UnFollowers",
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "UnFollowers successfully",
                    'state' => true,
					'follow_state' => false,
                );
            }
            else
            {
                $res = array(
                    'msg' => "UnFollowers successfully, but it looks like you haven't updated anything!",
                    'state' => true
                );
            }
        }
        return $res;
    }
}
?>