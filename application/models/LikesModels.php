<?php
date_default_timezone_set('Asia/Bangkok');
class LikesModels extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
        
    }
    public function GetLikesData($where)
    {
        $this->db->where($where);
        $query = $this->db->get('Likes');
        $query_data = $query->result();
        return $query_data;
    }
    public function AddLikesData($data)
    {
        $this->db->insert("Likes", $data);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to Likes",
                'insert_id' =>-1,
                'state' => false,
				'like_state' => null,
				'state_login' => null
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "User Likes successfully",
                    'insert_id' => $this->db->insert_id(),
                    'state' => true,
                    'like_state' => true,
					'state_login' => null
                );
            }
            else
            {
                $res = array(
                    'msg' => "User Likes successfully, but it looks like you haven't updated anything!",
                    'insert_id' => 0,
                    'state' => true,
					'state_login' => null
                );
            }
        }
        return  $res;
    }

    public function UpdateLikesData($data,$where)
    {
        $this->db->where($where);
        $this->db->update("Likes", $data);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to edit Likes",
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "Likes edited successfully",
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "Likes edited successfully, but it looks like you haven't updated anything!",
                    'state' => true
                );
            }
        }
        return $res;
    }

    public function DeleteLikesData($where)
    {
        $this->db->delete("Likes", $where);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to UnLikes",
                'state' => false,
				'state_login' => null
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "UnLikes successfully",
                    'state' => true,
					'like_state' => false,
					'state_login' => null
                );
            }
            else
            {
                $res = array(
                    'msg' => "UnLikes successfully, but it looks like you haven't updated anything!",
                    'state' => true,
					'state_login' => null
                );
            }
        }
        return $res;
    }
}
?>