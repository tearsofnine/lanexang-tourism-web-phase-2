<?php
date_default_timezone_set('Asia/Bangkok');
class ReviewModels extends CI_Model {
    public function __construct(){
        parent::__construct();
        $this->load->library('session');

        $this->load->model("CountriesModels","countries_models", true);
        $this->load->model("UserModel","user_model", true);
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("ProgramTourModels","program_tour_models", true);
    }
    public function AddReviewData($data)
    {
        $this->db->insert("Review", $data);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to Added Review",
                'state' => false,
				'state_login' => null
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "Review Added successfully",
                    'state' => true,
					'state_login' => null
                );
            }
            else
            {
                $res = array(
                    'msg' => "Review Added successfully, but it looks like you haven't updated anything!",
                    'state' => true,
					'state_login' => null
                );
            }
        }
        return $res;
        //return $this->db->insert_id();
    }
    public function UpdateReviewData($data,$where)
    {
        $this->db->where($where);
        $this->db->update("Review", $data);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to edit Review",
                'state' => false,
				'state_login' => null
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "Review edited successfully",
                    'state' => true,
					'state_login' => null
                );
            }
            else
            {
                $res = array(
                    'msg' => "Review edited successfully, but it looks like you haven't updated anything!",
                    'state' => true,
					'state_login' => null
                );
            }
        }
        return $res;
    }
    public function DeleteReviewData($where)
    {
        $this->db->delete("Review", $where);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to Delete",
                'state' => false,
				'state_login' => null
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "Delete successfully",
                    'state' => true,
					'state_login' => null
                );
            }
            else
            {
                $res = array(
                    'msg' => "Delete successfully, but it looks like you haven't updated anything!",
                    'state' => true,
					'state_login' => null
                );
            }
        }
        return $res;
    }
    public function GetReviewRatingData($where)
    {
       
        $str_select ='AVG(review_rating) AS AverageReview,';
        $str_select .='count(review_rating) AS CountReview,';
        $str_select .='sum(case when review_rating = 1 then 1 else 0 end) AS OneStar,';
        $str_select .='sum(case when review_rating = 2 then 1 else 0 end) AS TwoStar,';
        $str_select .='sum(case when review_rating = 3 then 1 else 0 end) AS ThreeStar,';
        $str_select .='sum(case when review_rating = 4 then 1 else 0 end) AS FourStar,';
        $str_select .='sum(case when review_rating = 5 then 1 else 0 end) AS FiveStar';
        $this->db->select($str_select);
        $this->db->from('Review');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function GetReviewData($where,$limit = 0)
    {
        $str_select ='rv.*,u.user_firstName,u.user_lastName,u.user_profile_pic_url,';
        $str_select .='i.itmes_topicThai,i.itmes_topicEnglish,i.itmes_topicChinese,i.itmes_topicLaos,';
        $str_select .='pt.programTour_nameThai,pt.programTour_nameEnglish,pt.programTour_nameChinese,pt.programTour_nameLaos';
        $this->db->select($str_select);
        $this->db->from('Review rv');
        $this->db->join('User u','u.user_id = rv.User_user_id');
        $this->db->join('Items i','i.items_id = rv.Items_items_id',"LEFT");
        $this->db->join('ProgramTour pt','pt.programTour_id = rv.ProgramTour_programTour_id',"LEFT");
		
		$this->db->order_by('rv.review_id', 'DESC');
        $this->db->where($where);
		
        if($limit > 0)
            $this->db->limit($limit);
        $query = $this->db->get();
        return $query->result();
    }
}
?>