<?php
date_default_timezone_set('Asia/Bangkok');
class StickerModels extends CI_Model {
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
    }
    
    public function getNameCategory()
    {
        $query = $this->db->get("StickerType");
        return $query->result();
    }
    public function GetinitialPreviewConfigSticker($where)
    {
        $this->db->where($where);
        $query = $this->db->get("PathsSticker");
        $query_data = $query->result();
        $array_data =array();
        for($i = 0;$i < count($query_data);$i++ )
        {
            $array_data []=array(
                "caption"=>$query_data[$i]->pathsSticker_paths,
                "url" => "/dasta_thailand/html/MediaAndAdvertising/Sticker/delete_sticker_based_on_key",
                "key"=>$query_data[$i]->pathsSticker_id,
            );
        }
        return $array_data;
    }
    public function GetinitialPreviewSticker($where)
    {
        $this->db->where($where);
        $query = $this->db->get("PathsSticker");
        $query_data = $query->result();
        $array_data =array();
        for($i = 0;$i < count($query_data);$i++ )
        {
            $array_data []='/dasta_thailand/assets/img/uploadfile_Sticker/'.$query_data[$i]->pathsSticker_paths;
        }
        return $array_data;
    }
    public function GetPathsStickerByStickerType($where)
    {
        $this->db->select("psk.*");
        $this->db->from('PathsSticker psk');
        $this->db->join('Sticker sk','sk.sticker_id = psk.Sticker_sticker_id');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function GetPathsSticker($where)
    {
        $this->db->where($where);
        $query = $this->db->get("PathsSticker");
        return $query->result();
    }
    public function GetStickerData($where = "")
    {
        $this->db->select("sk.*,skt.*");
        $this->db->from('Sticker sk');
        $this->db->join('StickerType skt','skt.stickerType_id = sk.StickerType_stickerType_id');
        if($where != "")
            $this->db->where($where);
        $query = $this->db->get();
        $query_data = $query->result();
        for($i = 0;$i < count($query_data);$i++ )
        {
            $query_data[$i]->PathsSticker = $this->GetPathsSticker("Sticker_sticker_id = ".$query_data[$i]->sticker_id);
            $query_data[$i]->initialPreview = $this->GetinitialPreviewSticker("Sticker_sticker_id = ".$query_data[$i]->sticker_id);
            $query_data[$i]->initialPreviewConfig = $this->GetinitialPreviewConfigSticker("Sticker_sticker_id = ".$query_data[$i]->sticker_id);
        }
        return $query_data;
    }
    public function AddStickerData($table,$data)
    {
        $this->db->insert($table, $data);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to ".$table,
                'insert_id' =>-1,
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "Add ".$table." successfully",
                    'insert_id' => $this->db->insert_id(),
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "Add ".$table." successfully, but it looks like you haven't updated anything!",
                    'insert_id' => 0,
                    'state' => true
                );
            }
        }
        return  $res;
    }
    public function UpdateStickerData($table,$data,$where)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to edit ".$table,
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => $table." edited successfully",
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => $table." edited successfully, but it looks like you haven't updated anything!",
                    'state' => true
                );
            }
        }
        return $res;
    }
    public function DeleteStickerData($table,$where)
    {
        $this->db->delete($table, $where);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to Delete ".$table,
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "Delete ".$table." successfully",
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "Delete ".$table." successfully, but it looks like you haven't updated anything!",
                    'state' => true
                );
            }
        }
        return $res;
    }

    public function setDatatableSticker($where,$user_id = -1)
    {
        $data_Sticker = $this->GetStickerData($where);
        $data = array();
        foreach($data_Sticker as $key => $r)
        {
            $data[] = array(
                '<div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table shoping-cart-table display nowrap" style="width:100%" id="DatasTables">
                            <tbody>
                                <tr>
                                    <td width="190">
                                        <img src="/dasta_thailand/assets/img/uploadfile_Sticker/'.$r->PathsSticker[0]->pathsSticker_paths.'" class="cart-product-imitation" >
                                    </td>
                                    <td class="desc">
                                        <h3><a href="#" class="text-navy">'.$r->sticker_Thai.'</a></h3>
                                        <p class="">ประเภททัวร์ : '.$r->stickerType_Thai.'</p>
                                       
                                        <div class="m-t-sm">
                                            <button type="button" class="text-muted btn-in-table ViewItemsModal" data-toggle="modal" data-target="#ViewItemsModal" value="'.$r->sticker_id.'"> 
                                                <i class="fa fa-folder"></i> '.$this->lang->line("view").'
                                            </button>
                                            |
                                            <a href="./Sticker/editSticker?sticker_id='.$r->sticker_id.'" class="text-muted">
                                                <i class="fa fa-pencil"></i> '.$this->lang->line("edit").'</a>
                                            |
                                            <button class="text-muted btn-in-table btn-item-del" value="'.$r->sticker_id.'">
                                                <i class="fa fa-trash-o"></i> ลบ</button>
                                            <p class="text-right">'.$r->sticker_createdDTTM.'</p>
                                        </div>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>',
                $r->sticker_Thai,
                $r->StickerType_stickerType_id,
                $r->sticker_createdDTTM,
            );
        }
        return $data;
    }
}
?>