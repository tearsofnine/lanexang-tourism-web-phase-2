<?php
date_default_timezone_set('Asia/Bangkok');
class GlobalFunctionModels extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
       
    }
    public function time_elapsed_string($datetime, $full = false) 
    {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);
    
        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;
    
        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }
    
        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
    public function substrwords($text, $maxchar, $end=' ...')
    {
        if (mb_strlen($text) > $maxchar || $text == '') {
            // $words = preg_split('/\s/', $text);      
            $words = preg_split('//', $text, -1, PREG_SPLIT_NO_EMPTY);    
            $output = '';
            $i      = 0;
            while (1) 
            {
                $length = mb_strlen($output)+mb_strlen($words[$i]);
                if ($length > $maxchar) 
                {
                    break;
                } 
                else 
                {
                    $output .= "" . $words[$i];
                    ++$i;
                }
            }
            $output .= $end;
        } 
        else 
        {
            $output = $text;
        }
        return $output;
    }
}
?>