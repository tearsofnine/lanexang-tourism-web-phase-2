<?php
date_default_timezone_set('Asia/Bangkok');
class PaymentTransactionModels extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("BookingModels","booking_models", true);
        $this->load->model("OrderDetailsModels","order_details_models", true);
    }
    public function GetPaymentTransactionDataOrderDetails($where)
    {
        $this->db->select("pt.*,ps.*");
        $this->db->from('PaymentTransaction pt'); 
        $this->db->join('PurchaseStatus ps','ps.purchaseStatus_id = pt.PurchaseStatus_purchaseStatus_id');
        $this->db->where($where);
        $query = $this->db->get();
        $query_data = $query->result();
        $PaymentTransaction = array();
        for($i = 0;$i < count($query_data);$i++ )
        {
            $OrderData = $this->order_details_models->GetOrderDetailsData("PaymentTransaction_paymentTransaction_id = ".$query_data[$i]->paymentTransaction_id);
            if(count($OrderData) != 0)
            {
                $query_data[$i]->OrderData = $OrderData;
                for($j = 0;$j < count($OrderData);$j++ )
                {
                    $OrderData[$j]->Product= $this->items_models->GetItemDataProduct("product_id = ".$OrderData[$j]->Product_product_id." AND Items_items_id = ".$OrderData[$j]->Items_items_id);
                }
                
                $ItemsData = $this->items_models->GetItemData("i.items_id = ".$query_data[$i]->OrderData[0]->Items_items_id);
                $query_data[$i]->ItemData = $ItemsData;
                $PaymentTransaction[]=$query_data[$i];
            }
        }

        return $PaymentTransaction;
    }
    public function GetPaymentTransactionDataBooking($where)
    {
        $this->db->select("pt.*,ps.*");
        $this->db->from('PaymentTransaction pt'); 
        $this->db->join('PurchaseStatus ps','ps.purchaseStatus_id = pt.PurchaseStatus_purchaseStatus_id');
        $this->db->where($where);
        $query = $this->db->get();
        $query_data = $query->result();
        $PaymentTransaction = array();
        for($i = 0;$i < count($query_data);$i++ )
        {
            $BookingData = $this->booking_models->GetBookingData("PaymentTransaction_paymentTransaction_id = ".$query_data[$i]->paymentTransaction_id);
            if(count($BookingData) != 0)
            {
                
                $query_data[$i]->BookingData = $BookingData;
                $DefaultItemsData = $this->items_models->GetDefaultItemsData("i.items_id = ".$query_data[$i]->BookingData[0]->Items_items_id." AND i.items_isActive = 1 AND i.items_isPublish = 1");
                $DefaultItemsData = $this->items_models->GetDefaultItemsData("i.items_id = ".$query_data[$i]->BookingData[0]->Items_items_id." AND i.items_isActive = 1 AND i.items_isPublish = 1");
      
                $ItemsData=[];
                switch (intval($DefaultItemsData[0]->MenuItem_menuItem_id)) {
                    case 9:
                        $ItemsData = $this->items_models->GetItemReservationsData("i.items_id = ".$query_data[$i]->BookingData[0]->Items_items_id,"PackageTours");
                    break;
                    case 10:
                        $ItemsData = $this->items_models->GetItemReservationsData("i.items_id = ".$query_data[$i]->BookingData[0]->Items_items_id,"EventTicket");
                    break;
                    case 11: 
                        $ItemsData = $this->items_models->GetItemReservationsData("i.items_id = ".$query_data[$i]->BookingData[0]->Items_items_id,"CarRent");
                    break;
                    case 5:
                        $RoomData = $this->items_models->GetItemDataRoom("Items_items_id = ".$query_data[$i]->BookingData[0]->Items_items_id." AND room_id = ".$query_data[$i]->BookingData[0]->Room_room_id);
                        $ItemsData = $this->items_models->GetItemData("i.items_id = ".$query_data[$i]->BookingData[0]->Items_items_id);
                        $query_data[$i]->RoomData = $RoomData;
                    break;
                }
                $query_data[$i]->ItemData = $ItemsData;
                $PaymentTransaction[]=$query_data[$i];
            }
        }

        return $PaymentTransaction;
    }
    public function AddPaymentTransactionData($data)
    {
        $this->db->insert("PaymentTransaction", $data);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to PaymentTransaction",
                'insert_id' => -1,
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "User PaymentTransaction successfully",
                    'insert_id' => $this->db->insert_id(),
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "User PaymentTransaction successfully, but it looks like you haven't updated anything!",
                    'insert_id' => 0,
                    'state' => true
                );
            }
        }
        return $res;
    }
    public function TakePaymentTransactionData($data)
    {

        $DefaultItemsData = $this->items_models->GetDefaultItemsData("i.items_id = ".$data["Items_items_id"]." AND i.items_isActive = 1 AND i.items_isPublish = 1");
        if(count($DefaultItemsData) != 0)
        {
            $totalPrice = 0;
            switch (intval($DefaultItemsData[0]->MenuItem_menuItem_id)) {
                case 9:
                    $booking_guestAdult = intval($data["booking_guestAdult"]);
                    $booking_guestAdultSingle = intval($data["booking_guestAdultSingle"]);
                    $booking_guestChild = intval($data["booking_guestChild"]);
                    $booking_guestChildBed = intval($data["booking_guestChildBed"]);
                    $booking_duration = intval($data["booking_duration"]);

                    // $totalguestAdult = $booking_guestAdultSingle + $booking_guestAdult;

                    $ItemsData = $this->items_models->GetItemReservationsData("i.items_isActive = 1 AND i.items_isPublish = 1 AND i.items_id = ".$data["Items_items_id"],"PackageTours");
                    
                    $travelPeriod_adult_special_price = intval($ItemsData[0]->TravelPeriod[$booking_duration]->travelPeriod_adult_special_price);
                    $travelPeriod_adult_price = intval($ItemsData[0]->TravelPeriod[$booking_duration]->travelPeriod_adult_price);
                    $travelPeriod_child_price = intval($ItemsData[0]->TravelPeriod[$booking_duration]->travelPeriod_child_price);
                    $travelPeriod_child_special_price = intval($ItemsData[0]->TravelPeriod[$booking_duration]->travelPeriod_child_special_price);

                    if($booking_guestAdult > 1)
                    {
                        $totalPrice += $travelPeriod_adult_special_price * $booking_guestAdult;
                    }
                    else
                    {
                        $totalPrice += $travelPeriod_adult_price * $booking_guestAdult;
                    }

                    $totalPrice += ($travelPeriod_adult_price * $booking_guestAdultSingle);
                    $totalPrice += ($travelPeriod_child_price * $booking_guestChild);
                    $totalPrice += ($travelPeriod_child_special_price * $booking_guestChildBed);

                    $BookingData = array(
                        "booking_checkIn" =>date("Y-m-d",strtotime($ItemsData[0]->TravelPeriod[$booking_duration]->travelPeriod_time_period_start)),
                        "booking_checkOut" =>date("Y-m-d",strtotime($ItemsData[0]->TravelPeriod[$booking_duration]->travelPeriod_time_period_end)),
                        "booking_guestAdult" =>$data["booking_guestAdult"],
                        "booking_guestAdultSingle" =>$data["booking_guestAdultSingle"],
                        "booking_guestChild" =>$data["booking_guestChild"],
                        "booking_guestChildBed" =>$data["booking_guestChildBed"],
                        "booking_guestAdultPrice" => $travelPeriod_adult_special_price,
                        "booking_guestAdultSinglePrice" => $travelPeriod_adult_price,
                        "booking_guestChildPrice" =>$travelPeriod_child_price,
                        "booking_guestChildBedPrice" =>$travelPeriod_child_special_price,
                        "booking_duration" =>$data["booking_duration"],
                        "Items_items_id" =>$data["Items_items_id"],
                        "booking_createdDTTM" => date("Y-m-d H:i:s"),
                        "PaymentTransaction_paymentTransaction_id" => -1,
                    );

                    break;
                case 10:
                    $date_checkIn=date("Y-m-d",strtotime($data["booking_checkIn"]));
                    $booking_guestAdult = intval($data["booking_guestAdult"]);
                    $booking_guestChild = intval($data["booking_guestChild"]);

                    $ItemsData = $this->items_models->GetItemReservationsData("i.items_isActive = 1 AND i.items_isPublish = 1 AND i.items_id = ".$data["Items_items_id"],"EventTicket");
                    $travelPeriod_adult_price = intval($ItemsData[0]->items_priceguestAdult);
                    $travelPeriod_child_price = intval($ItemsData[0]->items_priceguestChild);

                    $totalPrice += ($travelPeriod_adult_price * $booking_guestAdult);
                    $totalPrice += ($travelPeriod_child_price * $booking_guestChild);

                    $BookingData = array(
                        "booking_checkIn" => $date_checkIn,
                        "booking_checkOut" => $date_checkIn,
                        "booking_checkInTime" =>  date("H:i",strtotime($date_checkIn." ".$ItemsData[0]->TimePeriod[intval($data["booking_duration"])]->items_timeOpen)),
                        "booking_checkOutTime" => date("H:i",strtotime($date_checkIn." ".$ItemsData[0]->TimePeriod[intval($data["booking_duration"])]->items_timeClose)),
                        "booking_guestAdult" =>$data["booking_guestAdult"],
                        "booking_guestChild" =>$data["booking_guestChild"],
                        "booking_guestAdultPrice" =>$travelPeriod_adult_price,
                        "booking_guestChildPrice" =>$travelPeriod_child_price,
                        "booking_duration" =>$data["booking_duration"],
                        "Items_items_id" =>$data["Items_items_id"],
                        "booking_createdDTTM" => date("Y-m-d H:i:s"),
                        "PaymentTransaction_paymentTransaction_id" => -1,
                    );
                    break;
                case 11:
                    $time_stre_checkIn = $data["booking_checkIn"] ." ".$data["booking_checkInTime"];
                    $time_str_checkOut = $data["booking_checkOut"] ." ".$data["booking_checkOutTime"];

                    $date_checkIn=date("Y-m-d",strtotime($time_stre_checkIn));
                    $date_checkOut=date("Y-m-d",strtotime($time_str_checkOut));
                    $date_checkInTime=date("H:i",strtotime($time_stre_checkIn));
                    $date_checkOutTime=date("H:i",strtotime($time_str_checkOut));

                    $diff = abs(strtotime($date_checkIn) - strtotime($date_checkOut));
                    $days = floor(($diff)/ (60*60*24))+1;

                    $ItemsData = $this->items_models->GetItemReservationsData("i.items_isActive = 1 AND i.items_isPublish = 1 AND i.items_id = ".$data["Items_items_id"],"CarRent");
                
                    $totalPrice += intval($ItemsData[0]->items_PricePerDay) * $days;
                    $totalPrice += intval($ItemsData[0]->items_CarDeliveryPrice);
                    $totalPrice += intval($ItemsData[0]->items_PickUpPrice);
                    //$totalPrice += intval($ItemsData[0]->items_DepositPrice);

                    $BookingData = array(
                        "booking_checkIn" => $date_checkIn,
                        "booking_checkOut" => $date_checkOut,
                        "booking_checkInTime" => $date_checkInTime,
                        "booking_checkOutTime" => $date_checkOutTime,
                        "booking_duration" => $days,
                        "Items_items_id" =>$data["Items_items_id"],
                        "booking_createdDTTM" => date("Y-m-d H:i:s"),
                        "PaymentTransaction_paymentTransaction_id" => -1,
                    );
                    
                break;
                case 5:
                    $booking_guestAdult = intval($data["booking_guestAdult"]);
                    $booking_guestChild = intval($data["booking_guestChild"]);
                    $Room_room_id = intval($data["Room_room_id"]);
                    

                    $date_checkIn=date("Y-m-d",strtotime($data["booking_checkIn"]));
                    $date_checkOut=date("Y-m-d",strtotime($data["booking_checkOut"]));

                    $diff = abs(strtotime($date_checkIn) - strtotime($date_checkOut));
                    $days = floor(($diff)/ (60*60*24));

                    $RoomData = $this->items_models->GetItemDataRoom("Items_items_id = ".$data["Items_items_id"]." AND room_id = ".$data["Room_room_id"]);

                    $totalPrice += intval($RoomData[0]->room_price) * $days;

                    $BookingData = array(
                        "booking_guestAdult" =>$data["booking_guestAdult"],
                        "booking_guestChild" =>$data["booking_guestChild"],
                        "booking_checkIn" => $date_checkIn,
                        "booking_checkOut" => $date_checkOut,
                        
                        "booking_duration" => $days,
                        "Items_items_id" =>$data["Items_items_id"],
                        "Room_room_id" =>$data["Room_room_id"],
                        "booking_createdDTTM" => date("Y-m-d H:i:s"),
                        "PaymentTransaction_paymentTransaction_id" => -1,
                    );

                break;
                default:
                    # code...
                    break;
            }
           if(intval($DefaultItemsData[0]->MenuItem_menuItem_id) != 4)
           {
                $PaymentTransactionData = array(
                    "paymentTransaction_timestamp"=>date("Y-m-d H:i:s"),
                    "PaymentTransaction_totalPrice"=>$totalPrice,
                    "paymentTransaction_firstName"=>$data["paymentTransaction_firstName"],
                    "paymentTransaction_lastName"=>$data["paymentTransaction_lastName"],
                    "paymentTransaction_email"=>$data["paymentTransaction_email"],
                    "paymentTransaction_phone"=>$data["paymentTransaction_phone"],
                    "paymentTransaction_weChatId"=>$data["paymentTransaction_weChatId"],
                    "paymentTransaction_whatsAppId"=>$data["paymentTransaction_whatsAppId"],
                    "PurchaseStatus_purchaseStatus_id"=>2,
                    "User_user_id"=>$data["User_user_id"],
                );

                $PaymentData = $this->AddPaymentTransactionData($PaymentTransactionData);
                if(intval($PaymentData["insert_id"]) > 0)
                {
                    $BookingData["PaymentTransaction_paymentTransaction_id"] = $PaymentData["insert_id"];
                    $Booking = $this->booking_models->AddBookingData($BookingData);
                    $res = array(
                        'msg' => "Add PaymentTransaction successfully",
                        'PaymentTransaction_id' =>  $BookingData["PaymentTransaction_paymentTransaction_id"],
                        'Booking_id' => $Booking["insert_id"],
                        'state' => true
                    );
                }
                else
                {
                    $res = array(
                        'msg' => "Add PaymentTransaction successfully, but it looks like you haven't updated anything!",
                        'PaymentTransaction_id' => -1,
                        'Booking_id' => -1 ,
                        'state' => true
                    );
                }
            }
            else
            {
                $this->form_validation->set_rules('orderDetails_quantity', 'orderDetails_quantity', 'trim|required');
                $this->form_validation->set_rules('Product_product_id', 'Product_product_id', 'trim|required');
                if ($this->form_validation->run() == FALSE) 
                {
                    $res = array(
                        'state' => false,
                        'error'=>'validation',
                        'PaymentTransaction_id' =>-1,
                        'Booking_id' => -1 ,
                        'msg' => validation_errors()
                    );
                }
                else
                {
                    $PaymentTransactionData = array(
                        "paymentTransaction_timestamp"=>date("Y-m-d H:i:s"),
                        "PaymentTransaction_totalPrice"=>0,
                        "paymentTransaction_firstName"=>$data["paymentTransaction_firstName"],
                        "paymentTransaction_lastName"=>$data["paymentTransaction_lastName"],
                        "paymentTransaction_email"=>$data["paymentTransaction_email"],
                        "paymentTransaction_phone"=>$data["paymentTransaction_phone"],
                        "paymentTransaction_weChatId"=>$data["paymentTransaction_weChatId"],
                        "paymentTransaction_whatsAppId"=>$data["paymentTransaction_whatsAppId"],
                        "PurchaseStatus_purchaseStatus_id"=>2,
                        "User_user_id"=>$data["User_user_id"],
                    );
                    $PaymentData = $this->AddPaymentTransactionData($PaymentTransactionData);

                    if(intval($PaymentData["insert_id"]) > 0)
                    {
                        $OrderDetails["PaymentTransaction_paymentTransaction_id"] = $PaymentData["insert_id"];
                        
                        $orderDetails_quantity = explode(",",$data["orderDetails_quantity"]);
                        $Product_product_id = explode(",",$data["Product_product_id"]);

                        for($i = 0; $i<count($Product_product_id);$i++)
                        {
                            $DataProduct = $this->items_models->GetItemDataProduct("product_id = ".$Product_product_id[$i]." AND Items_items_id = ".$data["Items_items_id"]);
                            $totalPrice += (intval($DataProduct[0]->product_price) * intval($orderDetails_quantity[$i]));

                            $OrderDetailsData = array(
                                "orderDetails_timestamp"=>date("Y-m-d H:i:s"),
                                "orderDetails_quantity"=>$orderDetails_quantity[$i],
                                "orderDetails_Price"=>$DataProduct[0]->product_price,
                                "orderDetails_totalPrice"=>(intval($DataProduct[0]->product_price) * intval($orderDetails_quantity[$i])),
                                "Product_product_id"=>$Product_product_id[$i],
                                "Items_items_id" =>$data["Items_items_id"],
                                "PaymentTransaction_paymentTransaction_id"=>$PaymentData["insert_id"],
                                "orderDetails_createdDTTM" => date("Y-m-d H:i:s"),
                            );

                            $order_details = $this->order_details_models->AddOrderDetailsData($OrderDetailsData);
                        }
                        $PaymentTransactionData = array(
                            "PaymentTransaction_totalPrice" => $totalPrice
                        );
                        $this->UpdatePaymentTransactionData($PaymentTransactionData,"paymentTransaction_id = ".$PaymentData["insert_id"]);

                        $res = array(
                            'msg' => "Add PaymentTransaction successfully",
                            'PaymentTransaction_id' =>  $PaymentData["insert_id"],
                            'state' => true
                        );
                    }
                    else
                    {
                        $res = array(
                            'msg' => "Add PaymentTransaction successfully, but it looks like you haven't updated anything!",
                            'state' => true
                        );
                    }
                }
            }
            
        }
        else
        {
            $res = array(
                'msg' => "Unable to PaymentTransaction",
                'PaymentTransaction_id' =>-1,
                'Booking_id' => -1 ,
                'state' => false
            );
        }

        return  $res;
    }
    public function UpdatePaymentTransactionData($data,$where)
    {
        $this->db->where($where);
        $this->db->update("PaymentTransaction", $data);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to edit PaymentTransaction",
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "PaymentTransaction edited successfully",
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "PaymentTransaction edited successfully, but it looks like you haven't updated anything!",
                    'state' => true
                );
            }
        }
        return $res;
    }
    public function DeletePaymentTransactionData($where)
    {
        $this->db->delete("PaymentTransaction", $where);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to Delete PaymentTransaction",
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "Delete PaymentTransaction successfully",
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "Delete PaymentTransaction successfully, but it looks like you haven't updated anything!",
                    'state' => true
                );
            }
        }
        return $res;
    }
}
?>