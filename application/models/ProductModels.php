<?php
date_default_timezone_set('Asia/Bangkok');
class ProductModels extends CI_Model {
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model("UserModel","user_model", true);
        $this->load->model("ItemsModels","items_models", true);
    }
    public function GetProductData($where = "i.MenuItem_menuItem_id = 4 AND i.items_isActive = 1",$latitude="",$longitude="",$user_id = -1)
    {
        $str_select ='p.product_namesThai,MAX(p.product_id) as product_id ,MAX(i.items_id) as items_id';
        //$this->db->distinct();
        $this->db->select($str_select);
        $this->db->from('Product p');
        $this->db->join('Items i','i.items_id = p.Items_items_id');
        $this->db->where($where);
        $this->db->group_by("p.product_namesThai");
        $this->db->order_by('product_id', 'DESC');
        $query = $this->db->get();
        $query_data = $query->result();
        for($i = 0;$i < count($query_data);$i++ )
        {
            $query_data[$i]->ProductData = $this->items_models->GetItemDataProduct("product_id = ".$query_data[$i]->product_id);
            $query_data[$i]->ItemsData = $this->items_models->GetItemData("items_id = ".$query_data[$i]->items_id,$latitude,$longitude,$user_id,0);
            
        }
        return $query_data;
    }
    public function GetProductDataSearch($where = "i.MenuItem_menuItem_id = 4 ",$latitude="",$longitude="",$user_id = -1)
    {
        $str_select ='p.product_namesThai,p.product_id,i.items_id';
        //$this->db->distinct();
        $this->db->select($str_select);
        $this->db->from('Product p');
        $this->db->join('Items i','i.items_id = p.Items_items_id');
        $this->db->where($where);
        //$this->db->order_by('product_id', 'DESC');
        $query = $this->db->get();
        $query_data = $query->result();
        for($i = 0;$i < count($query_data);$i++ )
        {
            $query_data[$i]->ItemsData = $this->items_models->GetItemData("items_id = ".$query_data[$i]->items_id,$latitude,$longitude,$user_id,0);
        }
        return $query_data;
    }
}
?>