<?php
date_default_timezone_set('Asia/Bangkok');
class MainModels extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->model("ReviewModels","review_models", true);
        $this->load->model("ReviewModels","review_models", true);
        $this->load->model("BookMarksModels","bookmarks_models", true);
        $this->load->model("UserModel","user_model", true);
    }
    public function getCountProvincesData($where = "")
    {
        $str_select ='';
        $str_select .='p.provinces_id,p.provinces_thai,p.provinces_english,';
        $str_select .='sum(case when p.provinces_id = d.Provinces_provinces_id then 1 else 0 end) AS count_item';
        $this->db->select($str_select);
        $this->db->from('Items i');
        $this->db->join('Subdistricts sd','sd.subdistricts_id = i.Subdistricts_subdistricts_id');
        $this->db->join('Districts d','d.districts_id =  sd.Districts_districts_id');
        $this->db->join('Provinces p','p.provinces_id = d.Provinces_provinces_id');
        $this->db->join('Country cou','cou.country_id = p.Country_country_id');
        $whereIn = "i.MenuItem_menuItem_id in(2,3,4,5,6) AND i.items_isActive = 1";
        if(!empty($where))
        {
            $whereIn .= " AND ".$where; 
        }
        $this->db->where($whereIn);
        $this->db->group_by('p.provinces_id'); 
        $query = $this->db->get();
        $query_data = $query->result();
        return $query_data;
    }
    private function getCountSubcategoryData($where = "")
    {
        $whereIn = "i.MenuItem_menuItem_id in(2,3,4,5,6) AND i.items_isActive = 1";
        $whereIn2 = "ihsc.SubCategory_subcategory_id = sc.subcategory_id";
        if(!empty($where))
        {
            $whereIn .= " AND ".$where; 
            $whereIn2 .= " AND ".$where; 
        }
        $str_select ='';
        $str_select .='sc.subcategory_id,sc.subcategory_thai,sc.subcategory_english,';
        $str_select .='(SELECT COUNT(*) 
        FROM Items_has_SubCategory ihsc
        join Items i on i.items_id = ihsc.Items_items_id
        join Subdistricts sd on sd.subdistricts_id = i.Subdistricts_subdistricts_id
        join Districts d on d.districts_id =  sd.Districts_districts_id
        join Provinces p on p.provinces_id = d.Provinces_provinces_id
        join Country cou on cou.country_id = p.Country_country_id WHERE '.$whereIn2.' ) as count_item';
       
        $this->db->select($str_select);
        $this->db->from('Items i');
        $this->db->join('Items_has_SubCategory ihsc','ihsc.Items_items_id = i.items_id','left');

        $this->db->join('SubCategory sc','sc.subcategory_id = ihsc.SubCategory_subcategory_id');

        $this->db->join('Subdistricts sd','sd.subdistricts_id = i.Subdistricts_subdistricts_id');
        $this->db->join('Districts d','d.districts_id =  sd.Districts_districts_id');
        $this->db->join('Provinces p','p.provinces_id = d.Provinces_provinces_id');
        $this->db->join('Country cou','cou.country_id = p.Country_country_id');

        $this->db->where($whereIn);
        $this->db->group_by('sc.subcategory_id'); 
        $query = $this->db->get();

        $query_data = $query->result();
        return $query_data;
    }
    public function getCountAllData($where = "")
    {
        $str_select ='';
        $str_select .='sum(case when cou.country_id = 219 then 1 else 0 end) AS count_item_thai,';
        $str_select .='sum(case when cou.country_id = 125 then 1 else 0 end) AS count_item_laos,';
        $str_select .='sum(case when i.MenuItem_menuItem_id = 2 then 1 else 0 end) AS count_item_Attractions,';
        $str_select .='sum(case when i.MenuItem_menuItem_id = 3 then 1 else 0 end) AS count_item_Restaurant,';
        $str_select .='sum(case when i.MenuItem_menuItem_id = 4 then 1 else 0 end) AS count_item_Shopping,';
        $str_select .='sum(case when i.MenuItem_menuItem_id = 5 then 1 else 0 end) AS count_item_Hotel,';
        $str_select .='sum(case when i.MenuItem_menuItem_id = 6 then 1 else 0 end) AS count_item_ImportantContactPlaces';

        $this->db->select($str_select);
        $this->db->from('Items i');
        $this->db->join('Subdistricts sd','sd.subdistricts_id = i.Subdistricts_subdistricts_id');
        $this->db->join('Districts d','d.districts_id =  sd.Districts_districts_id');
        $this->db->join('Provinces p','p.provinces_id = d.Provinces_provinces_id');
        $this->db->join('Country cou','cou.country_id = p.Country_country_id');
        $whereIn = "i.MenuItem_menuItem_id in(2,3,4,5,6) AND i.items_isActive = 1";
        if(!empty($where))
        {
            $whereIn .= " AND ".$where; 
        }
        $this->db->where($whereIn);
        $query = $this->db->get();
        $query_data = $query->result();

        $query_data[0]->CountSubCategory = $this->getCountSubcategoryData($where);
        $query_data[0]->CounProvinces = $this->getCountProvincesData($where);
        
        
        return $query_data;
    }
}
?>