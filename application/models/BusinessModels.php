<?php
date_default_timezone_set('Asia/Bangkok');
class BusinessModels extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->model("ProgramTourModels","program_tour_models", true);
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("FollowersModels","followers_models", true);
        $this->load->model("UserModel","user_model", true);
    }

    public function GetBusinessData($where)
    {
        $this->db->select("b.*,u.*");
        $this->db->from('Business b');
        $this->db->join("User u",'u.user_id = b.User_user_id');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function AddBusinessData($data)
    {
        $this->db->insert("Business", $data);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to Business",
                'insert_id' =>-1,
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "Add Business successfully",
                    'insert_id' => $this->db->insert_id(),
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "Add Business successfully, but it looks like you haven't updated anything!",
                    'insert_id' => 0,
                    'state' => true
                );
            }
        }
        return  $res;
    }
    public function UpdateBusinessData($data,$where)
    {
        $this->db->where($where);
        $this->db->update("Business", $data);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to edit Business",
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "Business edited successfully",
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "Business edited successfully, but it looks like you haven't updated anything!",
                    'state' => true
                );
            }
        }
        return $res;
    }
    public function DeleteBusinessData($where)
    {
        $this->db->delete("Business", $where);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to Delete Business",
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "Delete Business successfully",
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "Delete Business successfully, but it looks like you haven't updated anything!",
                    'state' => true
                );
            }
        }
        return $res;
    }

}