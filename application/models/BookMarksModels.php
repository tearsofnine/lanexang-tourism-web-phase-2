<?php
date_default_timezone_set('Asia/Bangkok');
class BookMarksModels extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->model("ReviewModels","review_models", true);
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("FollowersModels","followers_models", true);
    }

    public function GetUserJoinBookMarksData($where)
    {
        $str_select ='u.*';
        $this->db->select($str_select);
        $this->db->from('BookMarks bm');
        $this->db->join("User u",'u.user_id = bm.User_user_id');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function GetBookMarksData($where,$latitude="",$longitude="")
    {
        $this->db->where($where);
        $query = $this->db->get('BookMarks');
        $query_data = $query->result();
        for($i = 0;$i < count($query_data);$i++ )
        {
            $User_BookMarks = $this->GetUserJoinBookMarksData("bm.Items_items_id = ".$query_data[$i]->Items_items_id." AND bm.User_user_id NOT IN ( ".$query_data[$i]->User_user_id." ) ");
            for($j = 0;$j < count($User_BookMarks);$j++ )
            {
                $follow_state = $this->followers_models->GetFollowersData("User_user_id = ".$query_data[$i]->User_user_id." AND followers_user_id = ".$User_BookMarks[$j]->user_id);
                $User_BookMarks[$j]->follow_state = (count($follow_state) > 0?true:false);
            }
            $query_data[$i]->CountUser_BookMarks = count($User_BookMarks);
            $query_data[$i]->User_BookMarks = $User_BookMarks;
            $query_data[$i]->ItemData = $this->items_models->GetItemData("i.items_isActive = 1 AND i.items_isPublish = 1 AND i.items_id = " .$query_data[$i]->Items_items_id."",$latitude,$longitude,$query_data[$i]->User_user_id);
            
        }
        return $query_data;
    }
    public function AddBookMarksData($data)
    {
        $this->db->insert("BookMarks", $data);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to BookMarks",
                'insert_id' =>-1,
                'state' => false,
				'bookmarks_state' => false,
				'state_login' => null
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "BookMarks",
                    'insert_id' => $this->db->insert_id(),
                    'state' => true,
					'bookmarks_state' => true,
					'state_login' => null
                );
            }
            else
            {
                $res = array(
                    'msg' => "User BookMarks successfully, but it looks like you haven't updated anything!",
                    'insert_id' => 0,
                    'state' => true,
					'bookmarks_state' => false,
					'state_login' => null
                );
            }
        }
        return  $res;
    }

    public function UpdateBookMarksData($data,$where)
    {
        $this->db->where($where);
        $this->db->update("BookMarks", $data);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to edit BookMarks",
                'state' => false,
				'state_login' => null
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "BookMarks edited successfully",
                    'state' => true,
					'state_login' => null
                );
            }
            else
            {
                $res = array(
                    'msg' => "BookMarks edited successfully, but it looks like you haven't updated anything!",
                    'state' => true,
					'state_login' => null
                );
            }
        }
        return $res;
    }

    public function DeleteBookMarksData($where)
    {
        $this->db->delete("BookMarks", $where);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to UnBookMarks",
                'state' => false,
				'bookmarks_state' => false,
				'state_login' => null
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "UnBookMarks",
                    'state' => true,
					'bookmarks_state' => false,
					'state_login' => null
                );
            }
            else
            {
                $res = array(
                    'msg' => "UnBookMarks successfully, but it looks like you haven't updated anything!",
                    'state' => true,
					'bookmarks_state' => false,
					'state_login' => null
                );
            }
        }
        return $res;
    }
}
?>