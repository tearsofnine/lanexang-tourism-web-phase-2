<?php
date_default_timezone_set('Asia/Bangkok');
class ItemsModels extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model("ReviewModels","review_models", true);
        $this->load->model("BookMarksModels","bookmarks_models", true);
        $this->load->model("UserModel","user_model", true);
        $this->load->model("BusinessModels","business_models", true);
        $this->load->model("ProgramTourModels","program_tour_models", true);
        $this->load->model("CountriesModels","countries_models", true);
        $this->load->model("ProductModels","product_models", true);
    }

    public function getCategory($where_in)
    {
        $this->db->where("category_id in (".$where_in.")");
        return $this->db->get("Category");
    }
    public function getNameCategory()
    {
        $this->db->where("category_id in (2,3,4,5,6,7)");
        return $this->db->get("Category");
    }
    public function get_number_of_seats()
    {
        $this->db->select("distinct(items_carSeats)");
        $this->db->from('Items'); 
        $this->db->where("MenuItem_menuItem_id = 11");
        $query = $this->db->get();
        return $query->result();
    }
    public function getNameCategorySubCategory($category_id)
    {
        $this->db->select("c.*,sc.*");
        $this->db->from('Category c'); 
        $this->db->join('SubCategory sc','sc.Category_category_id = c.category_id');
        $this->db->where("c.category_id = ".$category_id);
        return $this->db->get();
    }

    public function getDayOfWeek($where = "")
    {
        if($where != "")
            $this->db->where($where);
        $query = $this->db->get("DayOfWeek");
        return $query->result();
    }
    
    public function getNameHotelAccommodation($where = "")
    {
        if($where != "")
            $this->db->where($where);
        return $this->db->get("HotelAccommodation");
    }

    public function getNameFacilities($where = "")
    {
        if($where != "")
            $this->db->where($where);
        return $this->db->get("Facilities");
    }

    public function getNameDeliciousGuarantee($where = "")
    {
        if($where != "")
            $this->db->where($where);
        return $this->db->get("DeliciousGuarantee");
    }

    public function AddItemData($data,$table)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function UpdateItemData($data,$table,$where)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to edit Item",
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "Item edited successfully",
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "Item edited successfully, but it looks like you haven't updated anything!",
                    'state' => true
                );
            }
        }
        return $res;
    }

    public function DeleteItemData($table,$where)
    {
        $this->db->delete($table, $where);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to Delete",
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "Delete successfully",
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "Delete successfully, but it looks like you haven't updated anything!",
                    'state' => true
                );
            }
        }
        return $res;
    }

    public function GetItemSubData($table,$where)
    {
        if($where != "")
            $this->db->where($where);
        $query = $this->db->get($table);
        return $query->result();
    }

    public function GetItemDataReview($where)
    {
        $dataJson ='[{
            "AverageReview":0,
            "CountReview":0,
            "OneStar":0,
            "TwoStar":0,
            "ThreeStar":0,
            "FourStar":0,
            "FiveStar":0,
            "Reviews":[]
        }]';
        $data = json_decode($dataJson);

        $ReviewRating = $this->review_models->GetReviewRatingData("review_isActive =1 AND ".$where);
        $Reviews = $this->review_models->GetReviewData("rv.review_isActive =1 AND ".$where,3);

        $data[0]->AverageReview = intval($ReviewRating[0]->AverageReview);
        $data[0]->CountReview = intval($ReviewRating[0]->CountReview);
        $data[0]->OneStar = intval($ReviewRating[0]->OneStar);
        $data[0]->TwoStar = intval($ReviewRating[0]->TwoStar);
        $data[0]->ThreeStar = intval($ReviewRating[0]->ThreeStar);
        $data[0]->FourStar = intval($ReviewRating[0]->FourStar);
        $data[0]->FiveStar = intval($ReviewRating[0]->FiveStar);

        $i =0;
        foreach($Reviews as $r) {

            $data[0]->Reviews[] = $r;
            $originalDateTime = $r->review_timestamp;
            $newDate = date("d M Y", strtotime($originalDateTime));
            $newTime = date("h:i A", strtotime($originalDateTime));
            $newDateTime = $newDate." at ".$newTime;

            $data[0]->Reviews[$i]->review_timestamp = $newDateTime;
            $i++;
        }
        
        return $data;
    }

    public function GetItemDataHotelAccommodation($where)
    {
        $this->db->where($where);
        $query = $this->db->get("Items_has_HotelAccommodation");
        $HotelAccommodation_hotelAccommodation_id =0;
        foreach($query->result() as $r) {
            $HotelAccommodation_hotelAccommodation_id =  $r->HotelAccommodation_hotelAccommodation_id;
        }
        return intval($HotelAccommodation_hotelAccommodation_id);
    }

    public function GetItemDataRoom($where)
    {
        $this->db->where($where);
        $query = $this->db->get("Room");
        $query_data = $query->result();
        for($i = 0;$i < count($query_data);$i++ )
        {
            $query_data[$i]->PhotoRoom = $this->GetItemSubData("PictureRoom","Room_room_id = ".$query_data[$i]->room_id);
        }
        return $query_data;
    }

    public function GetItemDataProduct($where)
    {
        $this->db->where($where);
        $this->db->order_by('product_id', 'DESC');
        $query = $this->db->get("Product");
        $query_data = $query->result();
        for($i = 0;$i < count($query_data);$i++ )
        {
            $query_data[$i]->PhotoProduct = $this->GetItemSubData("PhotoProduct","Product_product_id = ".$query_data[$i]->product_id);
        }
        return $query_data;
    }
    public function GetItemDataPhoto($where)
    {
        $this->db->where($where);
        $query = $this->db->get("Photo");
        return $query->result();
    }

    public function GetItemDataDetail($where)
    {
        $str_select ='d.*,td.*';
        $this->db->select($str_select);
        $this->db->from('Detail d');
        $this->db->join('TopicDetail td','td.topicdetail_id = d.TopicDetail_topicdetail_id');
        $this->db->where($where);
        $query = $this->db->get();
        $query_data = $query->result();
        for($i = 0;$i < count($query_data);$i++ )
        {
            $query_data[$i]->Photo = $this->GetItemSubData("Photo","Detail_detail_id = ".$query_data[$i]->detail_id);
            
        }
        return $query_data;
    }

    public function GetItemsFacilities($where)
    {
        $str_select ='ihf.*,f.*';
        $this->db->select($str_select);
        $this->db->from('Items_has_Facilities ihf');
        $this->db->join('Facilities f','f.facilities_id =ihf.Facilities_facilities_id');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function GetItemsDelicious($where)
    {
        $str_select ='ihd.*,dg.*';
        $this->db->select($str_select);
        $this->db->from('Items_has_Delicious ihd');
        $this->db->join('DeliciousGuarantee dg','dg.deliciousGuarantee_id =ihd.DeliciousGuarantee_deliciousGuarantee_id');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

    public function GetItemDataBookMarks($where)
    {
        $query = $this->bookmarks_models->GetUserJoinBookMarksData($where);
        if($query)
            return true;
        else
            return false;
    }

    public function GetItemData($where,$latitude="",$longitude="",$user_id = -1,$isUseFullData = 1)
    {
        $str_select ='';
        $str_select .='i.*,mi.*,cat.*,sc.*,ihsc.*,ihdow.*,ct.*,tic.*,sd.*,d.*,p.*,cou.*';
        $this->db->select($str_select);
        $this->db->from('Items i');
        $this->db->join('MenuItem mi','mi.menuItem_id = i.MenuItem_menuItem_id');
        $this->db->join('Items_has_SubCategory ihsc','ihsc.Items_items_id = i.items_id');
        $this->db->join('SubCategory sc','sc.subcategory_id = ihsc.SubCategory_subcategory_id');
        $this->db->join('Category cat','cat.category_id = sc.Category_category_id');
        $this->db->join('Items_has_DayOfWeek ihdow','ihdow.Items_items_id = i.items_id');
        $this->db->join('CoverItems ct','ct.Items_items_id = i.items_id');
        $this->db->join('TempImageCover tic','tic.CoverItems_coverItem_id = ct.coverItem_id');
        $this->db->join('Subdistricts sd','sd.subdistricts_id = i.Subdistricts_subdistricts_id');
        $this->db->join('Districts d','d.districts_id =  sd.Districts_districts_id');
        $this->db->join('Provinces p','p.provinces_id = d.Provinces_provinces_id');
        $this->db->join('Country cou','cou.country_id = p.Country_country_id');
        
        $this->db->order_by('i.items_id', 'DESC');
        $this->db->where($where);
        
        $query = $this->db->get();
        $query_data = $query->result();
        for($i = 0;$i < count($query_data);$i++ )
        {
            $query_data[$i]->item_dayOpen_data = $this->getDayOfWeek("dayofweek_id = ".$query_data[$i]->item_dayOpen);
            $query_data[$i]->item_dayClose_data = $this->getDayOfWeek("dayofweek_id = ".$query_data[$i]->item_dayClose);
            if($isUseFullData)
            {
                $query_data[$i]->HotelAccommodation = $this->GetItemDataHotelAccommodation("Items_items_id = ".$query_data[$i]->items_id);
                $query_data[$i]->Detail = $this->GetItemDataDetail("d.Items_items_id = ".$query_data[$i]->items_id);
                $query_data[$i]->Delicious = $this->GetItemsDelicious("ihd.Items_items_id = ".$query_data[$i]->items_id); 
                $query_data[$i]->Facilities = $this->GetItemsFacilities("ihf.Items_items_id = ".$query_data[$i]->items_id); 
                $query_data[$i]->Product = $this->GetItemDataProduct("Items_items_id = ".$query_data[$i]->items_id);
                $query_data[$i]->Room = $this->GetItemDataRoom("Items_items_id = ".$query_data[$i]->items_id);
                
            
                if(count($query_data[$i]->Room) != 0)
                {
                    $query_data[$i]->items_price = $query_data[$i]->Room[0]->room_price;
                }
                else
                    $query_data[$i]->items_price = 0;
            }
            
            $query_data[$i]->Review = $this->GetItemDataReview("Items_items_id = ".$query_data[$i]->items_id);

            if($user_id != -1)
            {
                $query_data[$i]->BookMarks_state = $this->GetItemDataBookMarks("bm.User_user_id = ".$user_id." AND bm.Items_items_id = ".$query_data[$i]->items_id);                
            }
            else
            {
                $query_data[$i]->BookMarks_state = false;
            }

            $distance = 0;
            $distance_metr = 0;
            if($latitude != "" && $longitude != "")
            {
                $distance = round($this->GetPrivateDistance(floatval($latitude),floatval($longitude),floatval($query_data[$i]->items_latitude),floatval($query_data[0]->items_longitude),"K"));
                $distance_metr = round($this->GetPrivateDistance(floatval($latitude),floatval($longitude),floatval($query_data[$i]->items_latitude),floatval($query_data[0]->items_longitude),"K")* 1000);
            }
            $query_data[$i]->Distance = $distance;
            $query_data[$i]->Distance_metr = $distance_metr;

            $query_data[$i]->Business = $this->user_model->GetUserEntrepreneurData("user_id = ".$query_data[$i]->User_user_id." AND UserRole_userRole_id in (4,5)");

            
            
        }
        return $query_data;
    }

    public function GetEditItem($whereMain)
    {
        if(is_null($this->session->userdata('UserRole_userRole_id')))
        {
            $where = $whereMain;
        }
        else
        {
            
            switch ($this->session->userdata('UserRole_userRole_id')) {
                case 1:
                    $where = $whereMain;
                break;
                case 2:
                    $userinfor = $this->user_model->getinformationUserAdminManager($this->session->userdata('user_id'),"p.*","p.provinces_id");
                    foreach($userinfor->result() as $r) {
                        $provinces_id[] =  $r->provinces_id;
                    }

                    $where = $whereMain." AND 
                        i.Subdistricts_subdistricts_id IN(
                            SELECT subdistricts_id
                            FROM Subdistricts
                            where Districts_districts_id IN (
                                SELECT districts_id
                                FROM Districts
                                where Provinces_provinces_id IN (".join(",",$provinces_id).")
                            )
                        )";
                break;
                case 3:
                    $userinfor=$this->user_model->getinformationUserAdmin($this->session->userdata('user_id'));
                    $userinfor_data = $userinfor->row_array();

                    $where = $whereMain." AND 
                        i.Subdistricts_subdistricts_id IN(
                            SELECT subdistricts_id
                            FROM Subdistricts
                            where Districts_districts_id IN (
                                SELECT districts_id
                                FROM Districts
                                where Provinces_provinces_id IN (".$userinfor_data["provinces_id"].")
                            )
                        )";
                break;
            }
        }
        return $this->GetItemData($where);
    }

    public function GetItemDataCoverItems($where)
    {
        return $this->GetItemSubData("CoverItems",$where);
    }
    public function GetItemDataTravelDetails($where)
    {
        return  $this->GetItemSubData("TravelDetails",$where);
    }
    public function GetItemDataItemsHasScopeArea($where)
    {
        $this->db->select("DISTINCT(Items_items_id)");
        $this->db->from('Items_has_ScopeArea');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function GetItemDataScopeArea($where)
    {
        $this->db->select("ihsa.*,p.*");
        $this->db->from('Items_has_ScopeArea ihsa');
        $this->db->join('Provinces p','p.provinces_id = ihsa.Provinces_provinces_id');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function GetItemDataTravelPeriod($where)
    {
        return  $this->GetItemSubData("TravelPeriod",$where);
    }
    public function GetItemshasDayOfWeek($where)
    {
        $this->db->where($where);
        $query = $this->db->get("Items_has_DayOfWeek");
        $query_data = $query->result();
        for($i = 0;$i < count($query_data);$i++ )
        {
            $query_data[$i]->item_dayOpen_data = $this->getDayOfWeek("dayofweek_id = ".$query_data[$i]->item_dayOpen);
            $query_data[$i]->item_dayClose_data = $this->getDayOfWeek("dayofweek_id = ".$query_data[$i]->item_dayClose);
        }
        return $query_data;
 
    }
    public function GetItemDataBookingCondition($where)
    {
        $str_select ='';
        $str_select .='bc.*,bcc.*';
        $this->db->select($str_select);
        $this->db->from('BookingCondition bc');
        $this->db->join('BookingConditionCategory bcc','bcc.bookingConditionCategory_id = bc.BookingConditionCategory_bookingConditionCategory_id');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function GetItemsIdByDatesTrip($numDate)
    {
        $this->db->select("Items_items_id AS items_id");
        $this->db->from('TravelDetails');
        $this->db->group_by("Items_items_id");
        $this->db->having("count(Items_items_id) = ".$numDate);
        $query = $this->db->get();
        return $query->result();
    }
    public function GetNumDatesTrip()
    {
        $str_select ='DISTINCT(COUNT(td.Items_items_id)) as NumDatesTrip';
        $this->db->select($str_select);
        $this->db->from('TravelDetails td');
        $this->db->join('Items i','i.items_id = td.Items_items_id');
        $this->db->where("i.items_isActive = 1 ");
        $this->db->group_by("td.Items_items_id");
        $this->db->order_by('NumDatesTrip', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }
    public function GetItemReservationsData($where,$controllers,$user_id = -1)
    {
        $str_select ='';

        if(strpos($controllers, "EventTicket") !== false || strpos($controllers, "PackageTours") !== false)
            $str_select .='i.*,mi.*,cat.*,sc.*,ihsc.*';
        elseif(strpos($controllers, "CarRent") !== false)
            $str_select .='i.*,mi.*,ihtc.*,tc.*,gs.*,ct.*,tic.*';

        if(strpos($controllers, "EventTicket") !== false)
            $str_select .=',sd.*,d.*,p.*,cou.*';

        $this->db->order_by('i.items_id', 'DESC');    
        $this->db->select($str_select);
        $this->db->from('Items i');
        $this->db->join('MenuItem mi','mi.menuItem_id = i.MenuItem_menuItem_id');
       

        if(strpos($controllers, "EventTicket") !== false || strpos($controllers, "PackageTours") !== false)
        {
            $this->db->join('Items_has_SubCategory ihsc','ihsc.Items_items_id = i.items_id');
            $this->db->join('SubCategory sc','sc.subcategory_id = ihsc.SubCategory_subcategory_id');
            $this->db->join('Category cat','cat.category_id = sc.Category_category_id');
        }
        elseif(strpos($controllers, "CarRent") !== false)
        {
            $this->db->join('Items_has_TypesofCars ihtc','ihtc.Items_items_id = i.items_id');
            $this->db->join('TypesofCars tc','tc.typesofCars_id = ihtc.TypesofCars_typesofCars_id');
            $this->db->join('GearSystem gs','gs.gearSystem_id = ihtc.GearSystem_gearSystem_id');
            $this->db->join('CoverItems ct','ct.Items_items_id = i.items_id');
            $this->db->join('TempImageCover tic','tic.CoverItems_coverItem_id = ct.coverItem_id');
        }

        if(strpos($controllers, "EventTicket") !== false)
        {
            $this->db->join('Subdistricts sd','sd.subdistricts_id = i.Subdistricts_subdistricts_id');
            $this->db->join('Districts d','d.districts_id =  sd.Districts_districts_id');
            $this->db->join('Provinces p','p.provinces_id = d.Provinces_provinces_id');
            $this->db->join('Country cou','cou.country_id = p.Country_country_id');
        }
        
        $this->db->where($where);
        $query = $this->db->get();
        $query_data = $query->result();
        for($i = 0;$i < count($query_data);$i++ )
        {
            if(strpos($controllers, "PackageTours") !== false)
            {
                $query_data[$i]->CoverItems = $this->GetItemDataCoverItems("Items_items_id = ".$query_data[$i]->items_id);
                $query_data[$i]->TravelDetails = $this->GetItemDataTravelDetails("Items_items_id = ".$query_data[$i]->items_id);
                $query_data[$i]->TravelPeriod = $this->GetItemDataTravelPeriod("Items_items_id = ".$query_data[$i]->items_id);
                $query_data[$i]->ScopeArea = $this->GetItemDataScopeArea("Items_items_id = ".$query_data[$i]->items_id);
            }
            elseif(strpos($controllers, "EventTicket") !== false)
            {
                $query_data[$i]->CoverItems = $this->GetItemDataCoverItems("Items_items_id = ".$query_data[$i]->items_id);
            
                $query_data[$i]->Detail = $this->GetItemDataDetail("d.Items_items_id = ".$query_data[$i]->items_id);
                $query_data[$i]->TimePeriod = $this->GetItemshasDayOfWeek("Items_items_id = ".$query_data[$i]->items_id);
            }
            $query_data[$i]->BookingCondition = $this->GetItemDataBookingCondition("bc.Items_items_id = ".$query_data[$i]->items_id);
            
            $query_data[$i]->Business = $this->business_models->GetBusinessData("b.User_user_id = ".$query_data[$i]->User_user_id);
        }
        return $query_data;
    }
    public function setDatatableReservations($where,$controllers)
    {
        $item = $this->GetItemReservationsData($where,$controllers);
        $data = array();
        foreach($item as $key => $r) {

            $state ="";
            if (!is_null($this->session->userdata('entrepreneur_user_id'))) 
            {
                switch (intval($r->items_isActive)) 
                {
                    case 0:
                        $state ="<p><span class=\"badge badge-warning\" >รอการอนุมัติ</span><p>";
                    break;
                    case 1:
                        $state ="<p><span class=\"badge badge-primary\" >อนุมัติแล้ว</span><p>";   
                    break;
                    case 2;
                        $state ="<p><span class=\"badge badge-danger\" >ไม่ผ่านการอนุมัติ</span><p>";
                    break;
                }
            }
            else
            {
                if(count($r->Business) !=0)
                    $state ="<p><span style=\"color: #1AB394;\" id=\"business_name\">".$r->Business[0]->business_nameThai."</span><p>";
                else
                    $state ="<p><span style=\"color: #1AB394;\" id=\"business_name\"></span><p>";
            }
            if(strpos($controllers, "PackageTours") !== false)
            {
                $ScopeArea = array();
                foreach($r->ScopeArea as $keys => $rr) {
                    $ScopeArea[] = $rr->provinces_thai;
                }


                $data[] = array(
                    '<div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table shoping-cart-table display nowrap" style="width:100%" id="DatasTables">
                                <tbody>
                                    <tr>
                                        <td width="190">
                                            <img src="/dasta_thailand/assets/img/uploadfile/'.$r->CoverItems[0]->coverItem_paths.'" class="cart-product-imitation" >
                                        </td>
                                        <td class="desc">
                                            <h3><a href="#" class="text-navy">'.$r->itmes_topicThai.'</a></h3>
                                            <p class="">ประเภททัวร์ : '.($r->category_id == 7?$r->category_thai." : ".$r->subcategory_thai:$r->subcategory_thai).'</p>
                                            <p class="">ขอบเขตทัวร์ : '.join("-",$ScopeArea).'</p>
                                            <p class="">ระยะวัน : '.count($r->TravelDetails).' วัน</p>
                                            <p class="">'.$this->lang->line("status").' : '.($r->items_isPublish === "0"?$this->lang->line("stop_publishing"):$this->lang->line("publish")).'</p>
                                            '.$state.'
                                            <div class="m-t-sm">
                                                <button type="button" class="text-muted btn-in-table ViewItemsModal" data-toggle="modal" data-target="#ViewItemsModal" value="'.$r->items_id.'"> 
                                                    <i class="fa fa-folder"></i> '.$this->lang->line("view").'
                                                </button>
                                                |
                                                <a href="./'.$controllers.'/edit'.$controllers.'?item_id='.$r->items_id.'" class="text-muted">
                                                    <i class="fa fa-pencil"></i> '.$this->lang->line("edit").'</a>
                                                |
                                                <button class="text-muted btn-in-table btn-item-del" value="'.$r->items_id.'">
                                                    <i class="fa fa-trash-o"></i> ลบ</button>
                                                |
                                                <button class="text-muted btn-in-table btn-item-public" value="'.$r->items_id.'_'.($r->items_isPublish === "1"?0:1).'">
                                                    <i class="fa fa-globe"></i> '.($r->items_isPublish === "1"?$this->lang->line("stop_publishing"):$this->lang->line("publish")).'</button>
                                                <p class="text-right">'.$r->items_createdDTTM.'</p>
                                            </div>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    ',
                    $r->itmes_topicThai,
                    $r->subcategory_thai,
                    "ขอบเขตทัวร์",
                    count($r->TravelDetails),
                    $r->items_createdDTTM,
                    $r->items_id
                );
            }
            elseif(strpos($controllers, "EventTicket") !== false)
            {
                $data[] = array(
                    '<div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table shoping-cart-table display nowrap" style="width:100%" id="DatasTables">
                                <tbody>
                                    <tr>
                                        <td width="190">
                                            <img src="/dasta_thailand/assets/img/uploadfile/'.$r->CoverItems[0]->coverItem_paths.'" class="cart-product-imitation" >
                                        </td>
                                        <td class="desc">
                                            <h3><a href="#" class="text-navy">'.$r->itmes_topicThai.'</a></h3>
                                            <p class="">ประเภทตั๋วกิจกรรม : '.($r->category_id == 7?$r->category_thai." : ".$r->subcategory_thai:$r->subcategory_thai).'</p>
                                            <p class="">สถานที่เข้าชมกิจกรรม : '.$r->items_contactThai.'</p>
                                            <p class="">'.$this->lang->line("status").' : '.($r->items_isPublish === "0"?$this->lang->line("stop_publishing"):$this->lang->line("publish")).'</p>
                                            '.$state.'
                                            <div class="m-t-sm">
                                                <button type="button" class="text-muted btn-in-table ViewItemsModal" data-toggle="modal" data-target="#ViewItemsModal" value="'.$r->items_id.'"> 
                                                    <i class="fa fa-folder"></i> '.$this->lang->line("view").'
                                                </button>
                                                |
                                                <a href="./'.$controllers.'/edit'.$controllers.'?item_id='.$r->items_id.'" class="text-muted">
                                                    <i class="fa fa-pencil"></i> '.$this->lang->line("edit").'</a>
                                                |
                                                <button class="text-muted btn-in-table btn-item-del" value="'.$r->items_id.'">
                                                    <i class="fa fa-trash-o"></i> ลบ</button>
                                                |
                                                <button class="text-muted btn-in-table btn-item-public" value="'.$r->items_id.'_'.($r->items_isPublish === "1"?0:1).'">
                                                    <i class="fa fa-globe"></i> '.($r->items_isPublish === "1"?$this->lang->line("stop_publishing"):$this->lang->line("publish")).'</button>
                                                <p class="text-right">'.$r->items_createdDTTM.'</p>
                                            </div>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    ',
                    $r->itmes_topicThai,
                    $r->subcategory_thai,
                    $r->items_contactThai,
                    $r->items_createdDTTM,
                    $r->items_id
                );
            }
            elseif(strpos($controllers, "CarRent") !== false)
            {
                $data[] = array(
                    '<div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table shoping-cart-table display nowrap" style="width:100%" id="DatasTables">
                                <tbody>
                                    <tr>
                                        <td width="190">
                                            <img src="/dasta_thailand/assets/img/uploadfile/'.$r->coverItem_paths.'" class="cart-product-imitation" >
                                        </td>
                                        <td class="desc">
                                            <h3><a href="#" class="text-navy">'.$r->itmes_topicThai.'</a></h3>
                                            <p class="">ประเภท: '.$r->typesofCars_Thai .'</p>
                                            <p class="">เกียร์: '.$r->gearSystem_Thai.'</p>
                                            <p class="">โดยสาร: '.$r->items_carSeats.' คน</p>
                                            <p class="">ราคา: '.number_format($r->items_PricePerDay).'/วัน</p>
                                            <p class="">'.$this->lang->line("status").' : '.($r->items_isPublish === "0"?$this->lang->line("stop_publishing"):$this->lang->line("publish")).'</p>
                                            '.$state.'
                                            <div class="m-t-sm">
                                                <button type="button" class="text-muted btn-in-table ViewItemsModal" data-toggle="modal" data-target="#ViewItemsModal" value="'.$r->items_id.'"> 
                                                    <i class="fa fa-folder"></i> '.$this->lang->line("view").'
                                                </button>
                                                |
                                                <a href="./'.$controllers.'/edit'.$controllers.'?item_id='.$r->items_id.'" class="text-muted">
                                                    <i class="fa fa-pencil"></i> '.$this->lang->line("edit").'</a>
                                                |
                                                <button class="text-muted btn-in-table btn-item-del" value="'.$r->items_id.'">
                                                    <i class="fa fa-trash-o"></i> ลบ</button>
                                                |
                                                <button class="text-muted btn-in-table btn-item-public" value="'.$r->items_id.'_'.($r->items_isPublish === "1"?0:1).'">
                                                    <i class="fa fa-globe"></i> '.($r->items_isPublish === "1"?$this->lang->line("stop_publishing"):$this->lang->line("publish")).'</button>
                                                <p class="text-right">'.$r->items_createdDTTM.'</p>
                                            </div>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    ',
                    $r->itmes_topicThai,
                    $r->typesofCars_Thai,
                    $r->gearSystem_Thai,
                    $r->items_updatedDTTM,
                    $r->items_id
                );
            }
        }
        return $data;
    }
    public function setDataProducttable($items_id,$controllers)
    {
        switch ($controllers) {
            case 'Restaurant':
            case 'Shopping':
                $Product = $this->GetItemDataProduct("Items_items_id = ".$items_id);
            break;
            case 'Hotel':
                $Product = $this->GetItemDataRoom("Items_items_id = ".$items_id);
            break;
        }
          
        $data = array();
        foreach($Product as $key => $r) 
        {
            $PhotoProduct ="";
            if(strpos($controllers, "Hotel") === false)
            {
                foreach($r->PhotoProduct as $key2 => $rr) {
                    $PhotoProduct .='
                        <div class="item">
                            <div class="">
                                <img src="/dasta_thailand/assets/img/uploadfile/'.$rr->photoProduct_paths.'" class="d-block w-100 rounded" >
                            </div>
                        </div>
                    ';
                }
                $data[] = array('
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table shoping-cart-table display nowrap" style="width:100%" id="DatasTables">
                                <tbody>
                                    <tr>
                                        <td class="desc">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="owl-carousel owl-theme product-images DataProducttable" id="">
                                                        '.$PhotoProduct.'
                                                    </div>
                                                </div>
                                                <div class="col-md-7">
                                                    
                                                    <h2 class="font-bold text-navy m-b-xs">'.$r->product_namesThai.'</h2>
                                                    <div class="m-t-md">
                                                        <p>รายละเอียด : '.$r->product_descriptionThai.'</p>
                                                    </div>
                                                    <div class="m-t-md">
                                                        <h3 class="product-main-price text-muted">ราคา : '.$r->product_price.' บาท</h3>
                                                    </div>
                                                    <br>
                                                    <br>
                                                    <div>
                                                        <div class="m-t-sm">
                                                            <!--<a href="#" class="text-muted"><i class="fa fa-pencil"></i> '.$this->lang->line("edit").'</a>
                                                            |-->
                                                            <button class="text-muted btn-in-table btn-product-del" value="'.$r->product_id.'"><i class="fa fa-trash-o"></i> ลบ</button>
                                                           
                                                            <!--<p class="text-right">14 ม.ค, 2020</p>-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>',
                    $r->product_id
                );
            }
            else
            {
                foreach($r->PhotoRoom as $key2 => $rr)
                {
                    $PhotoProduct .='
                        <div class="item">
                            <div class="gallery-product-item">
                                <img src="/dasta_thailand/assets/img/uploadfile/'.$rr->pictureRoom_paths.'" class="d-block w-100 rounded" >
                            </div>
                        </div>
                    ';
                }
                
                $data[] = array('
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table shoping-cart-table display nowrap" style="width:100%" id="DatasTables">
                                <tbody>
                                    <tr>
                                        <td class="desc">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="owl-carousel owl-theme product-images DataProducttable" id="">
                                                        '.$PhotoProduct.'
                                                    </div>
                                                </div>
                                                <div class="col-md-7">
                                                    <p></p>
                                                    <h2 class="font-bold text-navy m-b-xs">'.$r->room_topicThai.'</h2>
                                                    <div class="m-t-md">
                                                        <h2 class="product-main-price">THB '.$r->room_price.' <small class="text-muted">ราคาห้องต่อคืน</small></h2>
                                                    </div>
                                                    <hr>
                                                    <dl class="small m-t-md">
                                                        <dt>รายละเอียดห้องพัก</dt>
                                                        <dd>'.$r->room_descriptionThai.'</dd>
                                                        <dd>'.($r->room_breakfast == 1?"รวมอาหารเช้า":"ไม่รวมอาหารเช้า").'</dd>
                                                    </dl>
                                                    <hr>

                                                    <div>
                                                        <div class="m-t-sm">
                                                            <!--<a href="#" class="text-muted"><i class="fa fa-folder"></i> '.$this->lang->line("view").'</a>
                                                            |
                                                            <a href="#" class="text-muted"><i class="fa fa-pencil"></i> '.$this->lang->line("edit").'</a>
                                                            |-->
                                                            <button class="text-muted btn-in-table btn-product-del" value="'.$r->room_id.'"><i class="fa fa-trash-o"></i> ลบ</button>
                                                            <!--<p class="text-right">14 ม.ค, 2020</p>-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>',
                    $r->room_id
                );
            }
           
           
        }
        return $data;
    }
    public function setDatatable($where,$controllers)
    {
        $item = $this->GetItemData($where);
        $data = array();
        foreach($item as $key => $r) {
            $data[] = array(
                '<div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table shoping-cart-table display nowrap" style="width:100%" id="DatasTables">
                            <tbody>
                                <tr>
                                    <td width="190">
                                        
                                        <img src="/dasta_thailand/assets/img/uploadfile/'.$r->coverItem_paths.'" class="cart-product-imitation" >
                                        
                                    </td>
                                    <td class="desc">
                                        <h3>
                                            <a href="#" class="text-navy">
                                                '.$r->itmes_topicThai.'
                                            </a>
                                        </h3>
                                        <p class="">
                                        '.$this->lang->line("name_location_type").' : '.($r->category_id == 7?$r->category_thai." : ".$r->subcategory_thai:$r->subcategory_thai).'
                                        </p>
                                        <p class="">
                                        '.$this->lang->line("address").' : '.$r->items_contactThai.'
                                        </p>
                                        <p class="">
                                        '.$this->lang->line("status").' : '.($r->items_isPublish === "0"?$this->lang->line("stop_publishing"):$this->lang->line("publish")).'
                                        </p>
                                        <p><span style="color: #1AB394;" id="business_name">'.(count($r->Business) == 1?$r->Business[0]->user_firstName." ".$r->Business[0]->user_lastName:"").'</span><p>
                                        <div class="m-t-sm">
                                            <button type="button" class="text-muted btn-in-table ViewItemsModal" data-toggle="modal" data-target="#ViewItemsModal" value="'.$r->items_id.'"> <i class="fa fa-folder"></i> '.$this->lang->line("view").'</button>
                                            |
                                            <a href="./'.$controllers.'/edit'.$controllers.'?item_id='.$r->items_id.'" class="text-muted">
                                                <i class="fa fa-pencil"></i> '.$this->lang->line("edit").'</a>
                                            |
                                            <button class="text-muted btn-in-table btn-item-del" value="'.$r->items_id.'">
                                                <i class="fa fa-trash-o"></i> ลบ</button>
                                            |
                                            <button class="text-muted btn-in-table btn-item-public" value="'.$r->items_id.'_'.($r->items_isPublish === "1"?0:1).'">
                                                <i class="fa fa-globe"></i> '.($r->items_isPublish === "1"?$this->lang->line("stop_publishing"):$this->lang->line("publish")).'
                                            </button>
                                            <p class="text-right">'.$r->items_createdDTTM.'</p>
                                        </div>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                ',
                $r->itmes_topicThai,
                $r->subcategory_thai,
                $r->items_contactThai,
                $r->items_createdDTTM,
                $r->items_id,
            );
        }
        return $data;
    }
    public function GetMinimalItemsData($where)
    {
        $this->db->select("i.itmes_topicThai,i.itmes_topicEnglish,i.itmes_topicChinese,i.itmes_topicLaos,MenuItem_menuItem_id,i.User_user_id");
        $this->db->from('Items i');
        // $this->db->join('CoverItems ct','ct.Items_items_id = i.items_id');
        // $this->db->join('TempImageCover tic','tic.CoverItems_coverItem_id = ct.coverItem_id');
        //$this->db->join('User u','u.user_id = i.User_user_id');
        $this->db->where($where);
        $query = $this->db->get();
        $query_data = $query->result();
        for($i = 0;$i < count($query_data);$i++ )
        {
            $CoverItems = $this->GetItemDataCoverItems("Items_items_id = ".$query_data[$i]->items_id);
            $query_data[$i]->CoverItems = $CoverItems[0]->coverItem_paths;
            $query_data[$i]->Business = $this->user_model->GetUserJoinBusinessData("u.user_id = ".$query_data[$i]->User_user_id." AND u.UserRole_userRole_id in (4,5)");
        }
        return $query_data;
    }
    public function GetDefaultItemsData($where)
    {
       
        $str_select ='i.*,u.*';
        $this->db->select($str_select);
        $this->db->from('Items i');
        $this->db->join('User u','u.user_id = i.User_user_id');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }

    public function CountApprovalItem() 
    {
        $ItemData = $this->GetDefaultItemsData("i.MenuItem_menuItem_id in (2,3,4,5,9,10,11) AND i.items_isActive = 0 ");
        $ProgramTourData  = $this->program_tour_models->GetDefaultProgramTourData("pt.programTour_isActive = 1 AND PublishStatus_publishStatus_id = 0 AND pt.programTour_isSaveDraft = 0 AND u.UserRole_userRole_id in (4,5)");
        $ApprovalItem = array (
            "All" => 0,
            "ProgramTour"=>0,
            "Attractions"=>0,
            "Restaurant"=>0,
            "Shopping"=>0,
            "Hotel"=>0,
            "PackageTours"=>0,
            "EventTicket"=>0,
            "CarRent"=>0
        );
        if(!is_null($ItemData))
        {
            foreach($ItemData as $key => $subArray)
            {
                foreach($subArray as $subArraykey => $value)
                {
                    if(strcmp($subArraykey,"MenuItem_menuItem_id")==0)
                    {
                        switch (intval($value)) {
                            case 1:
                                //$ApprovalItem["ProgramTour"] += 1;
                                break;
                            case 2:
                                $ApprovalItem["Attractions"] += 1;
                                break;
                            case 3:
                                $ApprovalItem["Restaurant"] += 1;
                                break;
                            case 4:
                                $ApprovalItem["Shopping"] += 1;
                                break;
                            case 5:
                                $ApprovalItem["Hotel"] += 1;
                                break;
                            case 9:
                                $ApprovalItem["PackageTours"] += 1;
                                break;
                            case 10:
                                $ApprovalItem["EventTicket"] += 1;
                                break;
                            case 11:
                                $ApprovalItem["CarRent"] += 1;
                                break;
                        }
                    }
                        
                }
            
            }
            $All = $this->user_model->CountApprovalUserState("u.user_isActive = 0 AND u.UserRole_userRole_id = 4");
            $ApprovalItem["All"] = count($ItemData) + intval($All["Wait"]);

            // $ProgramTour = $this->user_model->CountApprovalUserState("b.business_type_category_id = 1");
            // $ApprovalItem["ProgramTour"] += intval($ProgramTour["Wait"]);

            $Attractions = $this->user_model->CountApprovalUserState("b.business_type_category_id = 2");
            $ApprovalItem["Attractions"] += intval($Attractions["Wait"]);
            
            $Restaurant = $this->user_model->CountApprovalUserState("b.business_type_category_id = 3");
            $ApprovalItem["Restaurant"] += intval($Restaurant["Wait"]);

            $Shopping = $this->user_model->CountApprovalUserState("b.business_type_category_id = 4");
            $ApprovalItem["Shopping"] += intval($Shopping["Wait"]);

            $Hotels = $this->user_model->CountApprovalUserState("b.business_type_category_id = 5");
            $ApprovalItem["Hotel"] += intval($Hotels["Wait"]);

            $PackageTours = $this->user_model->CountApprovalUserState("b.business_type_category_id = 9");
            $ApprovalItem["PackageTours"] += intval($PackageTours["Wait"]);

            $EventTicket = $this->user_model->CountApprovalUserState("b.business_type_category_id = 10");
            $ApprovalItem["EventTicket"] += intval($EventTicket["Wait"]);

            $CarRent = $this->user_model->CountApprovalUserState("b.business_type_category_id = 11");
            $ApprovalItem["CarRent"] += intval($CarRent["Wait"]);
        }
        if(!is_null($ProgramTourData))
        {
            foreach($ProgramTourData as $key => $subArray)
            {
                $ApprovalItem["ProgramTour"] += 1;
            }
            $ApprovalItem["All"] += intval($ApprovalItem["ProgramTour"]);
        }

        return  $ApprovalItem;
    }
    public function CountApprovalItemState($where) 
    {
       $ItemData = $this->GetDefaultItemsData($where);
        
        $ApprovalItemState = array (
            "All"=>0,
            "Approved"=>0,
            "Refuse"=>0
        );
        if(!is_null($ItemData))
        {
            foreach($ItemData as $key => $subArray)
            {
                foreach($subArray as $subArraykey => $value)
                {
                    if(strcmp($subArraykey,"items_isActive")==0)
                    {
                        switch (intval($value)) {
                            case 1:
                                $ApprovalItemState["Approved"] += 1;
                                break;
                            case 2:
                                $ApprovalItemState["Refuse"] += 1;
                                break;
                            
                            
                        }
                    }
                }
            }
            $ApprovalItemState["All"] = count($ItemData);
        }

        return  $ApprovalItemState;
    }
    public function getSearchItemData($where,$latitude="",$longitude="",$user_id = -1) 
    {
        $str_select ='i.items_id';
       // $str_select ='i.items_id,i.itmes_topicThai,i.itmes_topicEnglish,i.itmes_topicChinese,i.itmes_topicLaos,mi.*,sc.*';
        $this->db->select($str_select);
        $this->db->from('Items i');
        //$this->db->join('MenuItem mi','mi.menuItem_id = i.MenuItem_menuItem_id');
        // $this->db->join('Items_has_SubCategory ihsc','ihsc.Items_items_id = i.items_id');
        // $this->db->join('SubCategory sc','sc.subcategory_id = ihsc.SubCategory_subcategory_id');
        $this->db->join('Subdistricts sdt','sdt.subdistricts_id = i.Subdistricts_subdistricts_id');
        $this->db->join('Districts dt','dt.districts_id = sdt.Districts_districts_id');
        $this->db->join('Provinces p','p.provinces_id = dt.Provinces_provinces_id');
        $this->db->join('Country c','c.country_id = p.Country_country_id');
        $this->db->where($where);
        $this->db->limit(4);
        $query = $this->db->get();
        $query_data = $query->result();
        $return_query_data = array();
        for($i = 0;$i < count($query_data);$i++ )
        {
            $itemData = $this->GetItemData("i.items_id = ".$query_data[$i]->items_id,$latitude,$longitude,$user_id );
            $return_query_data[$i] =  $itemData[0];
        }
        return $return_query_data;
    }
    public function getApprovalItemData($where,$user_id = -1) 
    {
        $itemData = $this->GetDefaultItemsData($where);
        $data_item = [];
        switch (intval($itemData[0]->MenuItem_menuItem_id))
        {
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                $data_item = $this->GetItemData($where);
                break;
            case 9:
                $data_item = $this->GetItemReservationsData($where,"PackageTours");
                break;
            case 10:
                $data_item = $this->GetItemReservationsData($where,"EventTicket");
                break;
            case 11:
                $data_item = $this->GetItemReservationsData($where,"CarRent");
                break;
        }
        return $data_item;         
    }

    public function getRoomPriceRange()
    {
        $this->db->select("min(CONVERT(room_price, UNSIGNED)) as MinimumValue ,MAX(CONVERT(room_price, UNSIGNED)) as MaximumValue ");
        $this->db->from('(select CONVERT(room_price, UNSIGNED) as room_price from Room group by CONVERT(room_price, UNSIGNED) order by CONVERT(room_price, UNSIGNED) ASC) tbl1');
        $query = $this->db->get();
        return $query->result();
    }

    
    public function getPackageToursPriceRange()
    {
        $this->db->select("min(travelPeriod_adult_special_price) as MinimumValue ,MAX(travelPeriod_adult_special_price) as MaximumValue ");
        $this->db->from('(select travelPeriod_adult_special_price from TravelPeriod group by travelPeriod_adult_special_price order by travelPeriod_adult_special_price ASC) tbl1');
        $query = $this->db->get();
        return $query->result();
    }
           
    public function getEventTicketPriceRange()
    {
        $this->db->select("min(items_priceguestAdult) as MinimumValue ,MAX(items_priceguestAdult) as MaximumValue ");
        $this->db->from('(select items_priceguestAdult from Items where MenuItem_menuItem_id = 10 group by items_priceguestAdult order by items_priceguestAdult ASC) tbl1');
        $query = $this->db->get();
        return $query->result();
    }

    
    public function getCarPriceRange()
    {
        $this->db->select("min(items_PricePerDay) as MinimumValue ,MAX(items_PricePerDay) as MaximumValue ");
        $this->db->from('(select items_PricePerDay from Items where MenuItem_menuItem_id = 11 group by items_PricePerDay order by items_PricePerDay ASC) tbl1');
        $query = $this->db->get();
        return $query->result();
    }
    
    public function getFillterForHotel($subcategory_id,$price_range,$facilities_id,$accommodation_level,$str_search = "",$latitude,$longitude,$user_id = -1)
    {
        $where = "i.MenuItem_menuItem_id = 5 AND i.items_isPublish = 1 AND i.items_isActive = 1";
        
        if($str_search != "")
        {
            $where .= " AND (i.itmes_topicThai like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            i.itmes_topicEnglish like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            i.itmes_topicChinese like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            i.itmes_topicLaos like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            c.country_Thai like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            c.country_english like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            c.country_chinese like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            c.country_laos like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            p.provinces_thai like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            p.provinces_english like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            p.provinces_chinese like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            p.provinces_laos like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            dt.districts_thai like CONCAT(\"%\", \"".$str_search."\", \"%\") OR
            dt.districts_english like CONCAT(\"%\", \"".$str_search."\", \"%\") OR
            dt.districts_chinese like CONCAT(\"%\", \"".$str_search."\", \"%\") OR
            dt.districts_laos like CONCAT(\"%\", \"".$str_search."\", \"%\") OR
            sdt.subdistricts_thai like CONCAT(\"%\", \"".$str_search."\", \"%\") OR
            sdt.subdistricts_english like CONCAT(\"%\", \"".$str_search."\", \"%\") OR
            sdt.subdistricts_chinese like CONCAT(\"%\", \"".$str_search."\", \"%\") OR
            sdt.subdistricts_laos like CONCAT(\"%\", \"".$str_search."\", \"%\")  )"; 
        } 
        if($subcategory_id != "")
        {
            $where .= " AND ihsc.SubCategory_subcategory_id in (".$subcategory_id.")"; 
        }
        if($accommodation_level != "")
        {
            $where .= " AND i.items_id IN (";
            $where .= "select Items_items_id ";
            $where .= "from Items_has_HotelAccommodation ";
            $where .= "where HotelAccommodation_hotelAccommodation_id in(".$accommodation_level.") ";
            $where .= "group by Items_items_id ";
            $where .= "order by Items_items_id )";
            // $where .= " AND ihha.HotelAccommodation_hotelAccommodation_id = ".$accommodation_level;
        }
        if($price_range != "")
        {
            $price = explode(",", $price_range);
            $where .= " AND i.items_id IN (";
            $where .= "select Items_items_id ";
            $where .= "from Room ";
            $where .= "where (room_price between ".(intval($price[0]) == 0?"1":$price[0])." AND ".$price[1].") ";
            $where .= "group by Items_items_id ";
            $where .= "order by Items_items_id )";
        }

        if($facilities_id != "")
        {
            $where .= " AND i.items_id IN (";
            $where .= "select Items_items_id ";
            $where .= "from Items_has_Facilities ";
            $where .= "where Facilities_facilities_id in(".$facilities_id.") ";
            $where .= "group by Items_items_id ";
            $where .= "order by Items_items_id )";
        }
        
        $this->db->select("i.items_id,i.items_latitude,i.items_longitude");
        $this->db->from("Items i");

        $this->db->join('Subdistricts sdt','sdt.subdistricts_id = i.Subdistricts_subdistricts_id');
        $this->db->join('Districts dt','dt.districts_id = sdt.Districts_districts_id');
        $this->db->join('Provinces p','p.provinces_id = dt.Provinces_provinces_id');
        $this->db->join('Country c','c.country_id = p.Country_country_id');

        $this->db->join('Items_has_SubCategory ihsc','ihsc.Items_items_id = i.items_id');
        //$this->db->join('Items_has_HotelAccommodation ihha','ihha.Items_items_id = i.items_id',"left");
        $this->db->where($where);
        $this->db->order_by('i.items_id ASC');
        $query = $this->db->get();
        $item_id = array();
        $items_data = $query->result();
        if($items_data != null)
        {
            foreach($items_data as $item)
            {
                if($latitude != "" && $longitude != "")
                {
                    if(round($this->GetPrivateDistance(floatval($latitude),floatval($longitude),floatval($item->items_latitude),floatval($item->items_longitude),"K")) <= 50)
                        $item_id [] = $item->items_id;
                }
                else
                    $item_id [] = $item->items_id;
               
            }
            return $this->GetItemData("items_id in (".join(",",$item_id).")",$user_id);
        }
        else
        {
            return array();
        }

    }

    public function getFillterForPackageTours($subcategory_id,$price_range,$travel_period,$tour_package_duration,$str_search = "",$user_id = -1)
    {
        /*if($str_search != "")
		{
			$where ="(i.itmes_topicThai like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
			i.itmes_topicEnglish like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
			i.itmes_topicChinese like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
			i.itmes_topicLaos like CONCAT(\"%\", \"".$str_search."\", \"%\") )  AND ";
			$where .= "i.MenuItem_menuItem_id = 9 AND i.items_isPublish = 1 AND i.items_isActive = 1";
		}
        else
			*/
			//$where = "i.MenuItem_menuItem_id = 9 AND i.items_isPublish = 1 AND i.items_isActive = 1";
        $ScopeArea = "";
        if($str_search != "")
        {
			$whereProvinces = "provinces_thai like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            provinces_english like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            provinces_chinese like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            provinces_laos like CONCAT(\"%\", \"".$str_search."\", \"%\") ";
            $data_provinces = $this->countries_models->getProvinces(true,$whereProvinces,"");
            $provinces_id = array();
            foreach($data_provinces as $item)
            {
                $provinces_id [] = $item->provinces_id;
            }
            $ScopeArea = join(",",$provinces_id);
            if($ScopeArea != "")
            {
				$where = "i.MenuItem_menuItem_id = 9 AND i.items_isPublish = 1 AND i.items_isActive = 1";
                $where .= " AND i.items_id IN (";
                $where .= "select Items_items_id ";
                $where .= "from Items_has_ScopeArea ";
                $where .= "where Provinces_provinces_id in (".$ScopeArea.") ";
                $where .= "group by Items_items_id )";
            }
			else
			{
				$where = "(i.itmes_topicThai like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
				i.itmes_topicEnglish like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
				i.itmes_topicChinese like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
				i.itmes_topicLaos like CONCAT(\"%\", \"".$str_search."\", \"%\") ) AND i.MenuItem_menuItem_id = 9 AND i.items_isPublish = 1 AND i.items_isActive = 1";
			}
        }
		else
		{
			$where = "i.MenuItem_menuItem_id = 9 AND i.items_isPublish = 1 AND i.items_isActive = 1";
		}
        if($subcategory_id != "")
        {
            $where .= " AND ihsc.SubCategory_subcategory_id in (".$subcategory_id.")"; 
        }
        if($price_range != "")
        {
            $price = explode(",", $price_range);
            $where .= " AND i.items_id IN (";
            $where .= "select Items_items_id ";
            $where .= "from TravelPeriod ";
            $where .= "where (travelPeriod_adult_special_price between ".(intval($price[0]) == 0?"1":$price[0])." AND ".$price[1].") ";
            $where .= "group by Items_items_id ";
            $where .= "order by Items_items_id )";
        }
        if($tour_package_duration != "")
        {
            $TourPackageDuration = explode(",", $tour_package_duration);

            $where .= " AND i.items_id IN (";
            $where .= "select Items_items_id ";
            $where .= "from TravelDetails ";
            $where .= "group by Items_items_id ";
            // $where .= "HAVING COUNT(Items_items_id)".(intval($tour_package_duration) <= 2?" = ".$tour_package_duration:" >= 3")." )";
            if(count($TourPackageDuration) != 3)
            {
                if(in_array("3", $TourPackageDuration))
                    $where .= " )";
                else
                    $where .= "HAVING COUNT(Items_items_id) in (".$tour_package_duration.") )";
            }   
            else
                $where .= " )";
        }
        
        if($travel_period != "")
        {
            $where .= " AND i.items_id IN (";
            $where .= "select Items_items_id ";
            $where .= "from TravelPeriod ";
            $where .= "where MONTH(travelPeriod_time_period_start) in (".$travel_period.") ";
            $where .= "group by Items_items_id )";
        }
        

        $this->db->select("i.*");
        $this->db->from("Items i");
        $this->db->join('Items_has_SubCategory ihsc','ihsc.Items_items_id = i.items_id');
        $this->db->where($where);
        $this->db->order_by('i.items_id ASC');
        $query = $this->db->get();
        $item_id = array();
        $items_data = $query->result();
        
        if($items_data != null )
        {
            foreach($items_data as $item)
            {
                $item_id [] = $item->items_id;
            }
            return $this->GetItemReservationsData("items_id in (".join(",",$item_id).")","PackageTours",$user_id);
            
        }
        else
        {
            return array();
        }
        

    }

    public function getFillterForEventTicket($subcategory_id,$price_range,$str_search = "",$user_id = -1)
    {
        $where = "i.MenuItem_menuItem_id = 10 AND i.items_isPublish = 1 AND i.items_isActive = 1";

        if($str_search != "")
        {
            $where .= " AND (i.itmes_topicThai like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            i.itmes_topicEnglish like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            i.itmes_topicChinese like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            i.itmes_topicLaos like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            c.country_Thai like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            c.country_english like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            c.country_chinese like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            c.country_laos like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            p.provinces_thai like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            p.provinces_english like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            p.provinces_chinese like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            p.provinces_laos like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            dt.districts_thai like CONCAT(\"%\", \"".$str_search."\", \"%\") OR
            dt.districts_english like CONCAT(\"%\", \"".$str_search."\", \"%\") OR
            dt.districts_chinese like CONCAT(\"%\", \"".$str_search."\", \"%\") OR
            dt.districts_laos like CONCAT(\"%\", \"".$str_search."\", \"%\") OR
            sdt.subdistricts_thai like CONCAT(\"%\", \"".$str_search."\", \"%\") OR
            sdt.subdistricts_english like CONCAT(\"%\", \"".$str_search."\", \"%\") OR
            sdt.subdistricts_chinese like CONCAT(\"%\", \"".$str_search."\", \"%\") OR
            sdt.subdistricts_laos like CONCAT(\"%\", \"".$str_search."\", \"%\")  )"; 
        } 

        if($subcategory_id != "")
        {
            $where .= " AND ihsc.SubCategory_subcategory_id in (".$subcategory_id.")"; 
        }
        if($price_range != "")
        {
            $price = explode(",", $price_range);
            $where .= " AND i.items_id IN (";
            $where .= "select items_id ";
            $where .= "from Items ";
            $where .= "where (items_priceguestAdult between ".(intval($price[0]) == 0?"1":$price[0])." AND ".$price[1].") ";
            $where .= "group by items_id ";
            $where .= "order by items_id )";
        }

        $this->db->select("i.items_id");
        $this->db->from("Items i");

        $this->db->join('Subdistricts sdt','sdt.subdistricts_id = i.Subdistricts_subdistricts_id');
        $this->db->join('Districts dt','dt.districts_id = sdt.Districts_districts_id');
        $this->db->join('Provinces p','p.provinces_id = dt.Provinces_provinces_id');
        $this->db->join('Country c','c.country_id = p.Country_country_id');

        $this->db->join('Items_has_SubCategory ihsc','ihsc.Items_items_id = i.items_id');
        $this->db->where($where);
        $this->db->order_by('i.items_id ASC');
        $query = $this->db->get();
        $item_id = array();
        $items_data = $query->result();
        if($items_data != null)
        {
            foreach($items_data as $item)
            {
                $item_id [] = $item->items_id;
            }
            return $this->GetItemReservationsData("items_id in (".join(",",$item_id).")","EventTicket",$user_id);
        }
        else
        {
            return array();
        }
    }

    public function getFillterForCarRent($subcategory_id,$price_range,$number_of_seats,$str_search = "",$user_id = -1)
    {
        $where = "i.MenuItem_menuItem_id = 11 AND i.items_isPublish = 1 AND i.items_isActive = 1";
        if($str_search != "")
        {
            $where .= " AND (i.itmes_topicThai like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            i.itmes_topicEnglish like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            i.itmes_topicChinese like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            i.itmes_topicLaos like CONCAT(\"%\", \"".$str_search."\", \"%\") )";
        }
        if($subcategory_id != "")
        {
            $where .= " AND ihtc.TypesofCars_typesofCars_id in (".$subcategory_id.")"; 
        }
        if($price_range != "")
        {
            $price = explode(",", $price_range);
            $where .= " AND i.items_id IN (";
            $where .= "select items_id ";
            $where .= "from Items ";
            $where .= "where (items_PricePerDay between ".(intval($price[0]) == 0?"1":$price[0])." AND ".$price[1].") ";
            $where .= "group by items_id ";
            $where .= "order by items_id )";
        }
        if($number_of_seats != "")
        {
            $where .= " AND i.items_id IN (";
            $where .= "select items_id ";
            $where .= "from Items ";
            $where .= "group by items_id ";
            $where .= "HAVING items_carSeats in (".$number_of_seats.") )"; 
        }

        $this->db->select("i.items_id");
        $this->db->from("Items i");
        $this->db->join('Items_has_TypesofCars ihtc','ihtc.Items_items_id = i.items_id');
        $this->db->where($where);
        $this->db->order_by('i.items_id ASC');
        $query = $this->db->get();
        $item_id = array();
        $items_data = $query->result();
        if($items_data != null)
        {
            foreach($items_data as $item)
            {
                $item_id [] = $item->items_id;
            }
            return $this->GetItemReservationsData("items_id in (".join(",",$item_id).")","CarRent",$user_id);
        }
        else
        {
            return array();
        }
        
    }
    public function getTouristProgram($items_id,$user_id = -1)
    {
        return $this->program_tour_models->getTouristProgramTalksAboutThisPlace($items_id,$user_id);
    }
    private function GetPrivateDistance($lat1, $lon1, $lat2, $lon2, $unit) 
	{
		if (($lat1 == $lat2) && ($lon1 == $lon2)) {
			return 0;
		}
		else {
			$theta = $lon1 - $lon2;
			$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
			$dist = acos($dist);
			$dist = rad2deg($dist);
			$miles = $dist * 60 * 1.1515;
			$unit = strtoupper($unit);

			if ($unit == "K") {
				return ($miles * 1.609344);
			} else if ($unit == "N") {
				return ($miles * 0.8684);
			} else {
				return $miles;
			}
	  }
    }
    
}
