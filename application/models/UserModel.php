<?php 

date_default_timezone_set('Asia/Bangkok');
class UserModel extends CI_Model {
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');

        $this->load->model("FollowersModels","followers_models", true);
        $this->load->model("ReviewModels","review_models", true);
        $this->load->model("BookMarksModels","bookmarks_models", true);
        $this->load->model("ProgramTourModels","program_tour_models", true);
        // if(strcmp($this->session->userdata('lang'),"EN") == 0)
        //     $this->lang->load("english","english");
        // else
        //     $this->lang->load("thailand","english");
    }

    public function login_entrepreneur($user_account, $password,$lang)
    {
        $this->db->select("u.*,b.*");
        $this->db->from('User u'); 
        $this->db->join('Business b','b.User_user_id = u.user_id');
        $this->db->where('u.user_account', $user_account);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
            $row = $query->row();
            $password = openssl_encrypt($password, "AES-128-ECB", SECRETKEY);
            $data_user = array (
                "status" =>0,
                "user_id" =>0
            );
            if($row->user_password == $password)
            {
                if($row->user_isDelete == 1){
                    $data_user["status"] = -3;
                    $data_user["user_id"] = -1;
                    return $data_user; //user isDelete
                }
                else if($row->user_isActive == 0)
                {
                    $data_user["status"] = -4;
                    $data_user["user_id"] = -1;
                    return $data_user; //user isActive
                }
                else if($row->user_isActive == 2)
                {
                    $data_user["status"] = -5;
                    $data_user["user_id"] = -1;
                    return $data_user; //user isActive
                }
                else
                {
                    if($row->UserRole_userRole_id == 4)
                    {
                        $data_user["status"] = 1;
                        $data_user["user_id"] = $row->user_id;
                       
                    }
                    else
                    {
                        $data_user["status"] = -99;
                        $data_user["user_id"] = -1;
                         //permission denied
                    }
                    
                    if($data_user["status"] == 1)
                    {
                        $this->session->set_userdata("entrepreneur_user_id", intval($row->user_id));
                        $this->session->set_userdata("entrepreneur_user_account", $row->user_account);
                        $this->session->set_userdata("entrepreneur_user_email", $row->user_email);
                        $this->session->set_userdata("entrepreneur_user_firstName", $row->user_firstName);
                        $this->session->set_userdata("entrepreneur_user_lastName", $row->user_lastName);
                        $this->session->set_userdata("entrepreneur_UserRole_userRole_id", intval($row->UserRole_userRole_id));
                        // $this->session->set_userdata("ProvinceGroup_pg_id", intval($row->ProvinceGroup_pg_id));
                        $this->session->set_userdata("entrepreneur_logged_in", true);
                        $this->session->set_userdata("lang", $lang);
                        $this->session->set_userdata("business_type_category_id", $row->business_type_category_id);
                        
                        $this->session->set_userdata("business_nameThai", $row->business_nameThai);
                        $this->session->set_userdata("business_nameEnglish", $row->business_nameEnglish);
                        $this->session->set_userdata("business_nameChinese", $row->business_nameChinese);
                        $this->session->set_userdata("business_nameLaos", $row->business_nameLaos);
                        
                        $this->session->set_userdata("business_presentAddress", $row->business_presentAddress);
                        
                        $this->session->set_userdata("business_latitude", $row->business_latitude);
                        $this->session->set_userdata("business_longitude", $row->business_longitude);
                        
                        $this->session->set_userdata("business_phone", $row->business_phone);
                        $this->session->set_userdata("business_www", $row->business_www);
                        $this->session->set_userdata("business_facebook", $row->business_facebook);
                        $this->session->set_userdata("business_line", $row->business_line);
                        
                        $this->session->set_userdata("business_license_paths", $row->business_license_paths);
                        $this->session->set_userdata("user_profile_pic_url", $row->user_profile_pic_url);
                        $this->session->set_userdata("Subdistricts_subdistricts_id", $row->Subdistricts_subdistricts_id);

                    }
                    return $data_user;
                }
            }
            else
            {
                $data_user["status"] = -1;
                $data_user["user_id"] = -1;
                return $data_user;//"password wrong";
            }
        }
        else
        {
            $data_user["status"] = -2;
            $data_user["user_id"] = -1;
            return $data_user; //"No user!!!";
        }
    }
    public function login($email, $password,$lang)
    {
        $this->db->where('user_email', $email);
        $query = $this->db->get('User');
        if($query->num_rows() > 0)
        {
            $row = $query->row();
            $password = openssl_encrypt($password, "AES-128-ECB", SECRETKEY);
            $data_user = array (
                "status" =>0,
                "user_id" =>0
            );
            if($row->user_password == $password)
            {
                if($row->user_isDelete == 1){
                    $data_user["status"] = -3;
                    $data_user["user_id"] = -1;
                    return $data_user; //user isDelete
                }
                else if($row->user_isActive == 0)
                {
                    $data_user["status"] = -4;
                    $data_user["user_id"] = -1;
                    return $data_user; //user isActive
                }
                else
                {
                    if($row->UserRole_userRole_id == 1)
                    {
                        $data_user["status"] = 1;
                        $data_user["user_id"] = $row->user_id;
                    }
                    else if($row->UserRole_userRole_id == 2 || $row->UserRole_userRole_id == 3)
                    {
                        $this->db->where(array("pg_id"=>$row->ProvinceGroup_pg_id,"pg_isActive"=>1,"pg_isDelete"=>0));
                        $queryProvinceGroup = $this->db->get('ProvinceGroup');
                        if($queryProvinceGroup->num_rows() != 0)
                        {
                            $data_user["status"] = 1;
                            $data_user["user_id"] = $row->user_id;
                        }
                        else
                        {
                            $data_user["status"] = -99;
                            $data_user["user_id"] = -1;
                        }
                    }
                    else
                    {
                        $data_user["status"] = -99;
                        $data_user["user_id"] = -1;
                         //permission denied
                    }
                    if($data_user["status"] == 1)
                    {
                        $this->session->set_userdata("user_id", intval($row->user_id));
                        $this->session->set_userdata("user_email", $row->user_email);
                        $this->session->set_userdata("user_firstName", $row->user_firstName);
                        $this->session->set_userdata("user_lastName", $row->user_lastName);
                        $this->session->set_userdata("user_age", $row->user_age);
                        $this->session->set_userdata("user_gender", $row->user_gender);
                        $this->session->set_userdata("user_phone", $row->user_phone);
                        $this->session->set_userdata("UserRole_userRole_id", intval($row->UserRole_userRole_id));
                        $this->session->set_userdata("ProvinceGroup_pg_id", intval($row->ProvinceGroup_pg_id));
                        $this->session->set_userdata("logged_in", true);
                        $this->session->set_userdata("lang", $lang);
                    }
                    return  $data_user;
                }
            }
            else
            {
                $data_user["status"] = -1;
                $data_user["user_id"] = -1;
                return $data_user;//"password wrong";
            }
        }
        else
        {
            $data_user["status"] = -2;
            $data_user["user_id"] = -1;
            return $data_user; //"No user!!!";
        }
    }
    public function getinformationUserAdminManager($user_id,$select,$order_by)
    {
        $this->db->select($select);
        $this->db->from('User u'); 
        $this->db->join('ProvinceGroup pg','pg.pg_id = u.ProvinceGroup_pg_id');
        $this->db->join('PermissionProvinceGroup ppg','ppg.ProvinceGroup_pg_id = pg.pg_id');
        $this->db->join('Provinces p','p.provinces_id = ppg.Provinces_provinces_id');
        $this->db->join('Country c','c.country_id = p.Country_country_id');
        $this->db->where("u.user_id = ".$user_id);
        $this->db->order_by($order_by, "asc");

        return $this->db->get();
    }
    public function getinformationUserAdmin($user_id)
    {
        $this->db->select("u.*,pg.*,pp.*,ppg.*,c.*,p.*");
        $this->db->from('User u'); 
        $this->db->join('ProvinceGroup pg','pg.pg_id = u.ProvinceGroup_pg_id');
        $this->db->join('ProvincePermission pp',' pp.User_user_id = user_id');
        $this->db->join('PermissionProvinceGroup ppg','ppg.ppg_id = pp.PermissionProvinceGroup_ppg_id');
        $this->db->join('Provinces p','p.provinces_id = ppg.Provinces_provinces_id');
        $this->db->join('Country c','c.country_id = p.Country_country_id');
        $this->db->where("u.user_id = ".$user_id);

        return $this->db->get();
    }
    public function GetCheckUserData($where)
    {
        $this->db->where($where);
        $query =  $this->db->get('User');
        return $query->result();
    }
    public function GetUserData($where)
    {
        $this->db->where($where);
        $query =  $this->db->get('User');
        $query_data = $query->result();
        for($i = 0;$i < count($query_data);$i++ )
        {
            $ReviewRating = $this->review_models->GetReviewRatingData("review_isActive =1 AND User_user_id = ".$query_data[$i]->user_id." AND ProgramTour_programTour_id IS NULL");
            $followers = $this->followers_models->Getfollowers($query_data[$i]->user_id);
            $following = $this->followers_models->Getfollowing($query_data[$i]->user_id);
            $bookmarks = $this->bookmarks_models->GetUserJoinBookMarksData("bm.User_user_id = ".$query_data[$i]->user_id);
            $programtour = $this->program_tour_models->GetDefaultProgramTourData("pt.programTour_isActive = 1 AND pt.User_user_id = ".$query_data[$i]->user_id);
          

            $query_data[$i]->count_followers = count($followers->result());
            $query_data[$i]->count_following = count($following->result());
            $query_data[$i]->count_review = intval($ReviewRating[0]->CountReview);
            $query_data[$i]->count_recordedList = count($bookmarks);
            $query_data[$i]->count_programTour = count($programtour);
            $query_data[$i]->state = true;
        }
        return $query_data;
    }
    public function AddUserBusinessData($table,$data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }
    public function AddUserData($data)
    {
        $this->db->insert('User',$data);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to Register user",
                'insert_id' => -1,
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "User Register successfully",
                    'insert_id' => $this->db->insert_id(),
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "User Register successfully, but it looks like you haven't updated anything!",
                    'insert_id' => 0,
                    'state' => true
                );
            }
        }
        return $res;
    }

    public function UpdateUserData($data,$where)
    {
        $this->db->where($where);
        $this->db->update("User", $data);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to edit User",
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "User edited successfully",
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "User edited successfully, but it looks like you haven't updated anything!",
                    'state' => true
                );
            }
        }
        return $res;
    }

    public function DeleteUserData($where)
    {
        $this->db->delete("User", $where);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to Delete",
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "Delete successfully",
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "Delete successfully, but it looks like you haven't updated anything!",
                    'state' => true
                );
            }
        }
        return $res;
    }

    public function GetUserEntrepreneurData($where)
    {
        $this->db->select("user_id,user_account,user_firstName,user_lastName");
        $this->db->from('User u'); 
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function GetUserJoinBusinessData($where)
    {
        $this->db->select("u.*,b.*,c.*");
        $this->db->from('User u'); 
        $this->db->join('Business b','b.User_user_id = u.user_id','left');
        $this->db->join('Category c','c.category_id = b.business_type_category_id','left');
        $this->db->where($where);
        $query = $this->db->get();
        $query_data = $query->result();
        return $query_data;
    }
	public function GetUserJoinBusinessDataForChat($where)
    {
        $this->db->select("u.user_account,u.user_firstName,u.user_lastName,b.business_id,b.business_nameThai,c.*");
        $this->db->from('User u'); 
        $this->db->join('Business b','b.User_user_id = u.user_id','left');
        $this->db->join('Category c','c.category_id = b.business_type_category_id','left');
        $this->db->where($where);
        $query = $this->db->get();
        $query_data = $query->result();
        return $query_data;
    }
    public function CountApprovalUserState($where) 
    {
        $UserData = $this->GetUserJoinBusinessData($where);
        
        $ApprovalUserState = array (
            "All"=>0,
            "Approved"=>0,
            "Wait"=>0,
            "Refuse"=>0
        );
        if(!is_null($UserData))
        {
            foreach ($UserData as $key => $subArray)
            {
                foreach ($subArray as $subArraykey => $value)
                {
                    if(strcmp($subArraykey,"user_isActive")==0)
                    {
                        switch (intval($value)) 
                        {
                            case 0:
                                $ApprovalUserState["Wait"] += 1;
                                break;
                            case 1:
                                $ApprovalUserState["Approved"] += 1;
                                break;
                            case 2:
                                $ApprovalUserState["Refuse"] += 1;
                                break;
                        }
                    }
                }
            }
            $ApprovalUserState["All"] = count($UserData);
        }
        return  $ApprovalUserState;
    }
	public function login_web($email, $password)
    {
        $this->db->where('user_email', $email);
        $query = $this->db->get('User');
        if($query->num_rows() > 0)
        {
            $row = $query->row();
            $password = openssl_encrypt($password, "AES-128-ECB", SECRETKEY);
            $data_user = array (
                "status" =>0,
                "user_id" =>0
            );
            if($row->user_password == $password)
            {
                if($row->user_isDelete == 1){
                    $data_user["status"] = -3;
                    $data_user["user_id"] = -1;
                    return $data_user; //user isDelete
                }
                else if($row->user_isActive == 0)
                {
                    $data_user["status"] = -4;
                    $data_user["user_id"] = -1;
                    return $data_user; //user isActive
                }
                else
                {
                    if($row->UserRole_userRole_id == 5)
                    {
                        $data_user["status"] = 1;
                        $data_user["user_id"] = $row->user_id;
                    }
                    else
                    {
                        $data_user["status"] = -2;
                        $data_user["user_id"] = -1;
                         //permission denied
                    }
                    
                    return  $data_user;
                }
            }
            else
            {
                $data_user["status"] = -1;
                $data_user["user_id"] = -1;
                return $data_user;//"password wrong";
            }
        }
        else
        {
            $data_user["status"] = -2;
            $data_user["user_id"] = -1;
            return $data_user; //"No user!!!";
        }
    }
	public function identification_check($identification)
	{
		$this->db->where('user_identification', $identification);
        $query = $this->db->get('User');
        if($query->num_rows() == 0)
        {
			return TRUE;
		}
		else
			return FALSE;
	}
	public function email_check($email)
    {
        $this->db->where('user_email', $email);
        $query = $this->db->get('User');
        if($query->num_rows() == 0)
        {
			return TRUE;
		}
		else
			return FALSE;
	}
}
?>