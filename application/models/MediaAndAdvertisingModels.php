<?php
date_default_timezone_set('Asia/Bangkok');
class MediaAndAdvertisingModels extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->model("BookMarksModels","bookmarks_models", true);
        $this->load->model("UserModel","user_model", true);
        $this->load->model("ProgramTourModels","program_tour_models", true);
        $this->load->model("CountriesModels","countries_models", true);
        $this->load->model("BusinessModels","business_models", true);
        
    }
    public function AddMediaAndAdvertisingData($data,$table)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function UpdateMediaAndAdvertisingData($data,$table,$where)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to edit MediaAndAdvertisingModels",
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "MediaAndAdvertisingModels edited successfully",
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "MediaAndAdvertisingModels edited successfully, but it looks like you haven't updated anything!",
                    'state' => true
                );
            }
        }
        return $res;
    }

    public function DeleteMediaAndAdvertisingData($table,$where)
    {
        $this->db->delete($table, $where);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to Delete",
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "Delete successfully",
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "Delete successfully, but it looks like you haven't updated anything!",
                    'state' => true
                );
            }
        }
        return $res;
    }

    public function GetMediaAndAdvertisingData($where,$user_id = -1)
    {
        $str_select ='';
        $str_select .='maa.*,maat.*,mi.*,sc.*,ct.coverItem_paths,tic.*';
        $this->db->select($str_select);
        $this->db->from('MediaAndAdvertising maa');
        $this->db->join('MenuItem mi','mi.menuItem_id = maa.MenuItem_menuItem_id');
        $this->db->join('MediaAndAdvertisingType maat','maat.mediaAndAdvertisingType_id = maa.MediaAndAdvertisingType_mediaAndAdvertisingType_id');
        $this->db->join('SubCategory sc','sc.subcategory_id = maa.SubCategory_subcategory_id','left');
        $this->db->join('CoverItems ct','ct.MediaAndAdvertising_mediaAndAdvertising_id = maa.mediaAndAdvertising_id');
        $this->db->join('TempImageCover tic','tic.CoverItems_coverItem_id = ct.coverItem_id');
        $this->db->join('ProgramTour pt','pt.programTour_id = maa.ProgramTour_programTour_id','left');
        $this->db->join('Items i','i.items_id = maa.Items_items_id','left');
        $this->db->where($where);
        $query = $this->db->get();
        $query_data = $query->result();
        for($i = 0;$i < count($query_data);$i++ )
        {
            if(intval($query_data[$i]->MediaAndAdvertisingType_mediaAndAdvertisingType_id) == 2)
            {
                $districts_data = $this->countries_models->getDistricts("districts_id = ".$query_data[$i]->Districts_districts_id);
                $provinces_data = $this->countries_models->getProvinces(true,"provinces_id = ".$districts_data[0]->Provinces_provinces_id,"");
                
                $query_data[$i]->districts_id = $query_data[$i]->Districts_districts_id;
                $query_data[$i]->provinces_id = $districts_data[0]->Provinces_provinces_id;
                $query_data[$i]->country_id = $provinces_data[0]->Country_country_id;
            }
            
            //$query_data[$i]->Photo = $this->GetItemSubData("Photo","Detail_detail_id = ".$query_data[$i]->detail_id);
            
        }
        return $query_data;
    }
    public function GetEditMediaAndAdvertising($whereMain)
    {
        return $this->GetMediaAndAdvertisingData($whereMain);
    }
    public function setDatatableMediaAndAdvertising($where,$controllers ="",$user_id = -1)
    {
        $data_media_and_advertising = $this->GetMediaAndAdvertisingData($where);
        $data = array();
        foreach($data_media_and_advertising as $key => $r)
        {
            $time_period="-";
            if(strpos($r->mediaAndAdvertising_time_period_start, "1970-01-01") === false)
                $time_period = date('d/m/Y',strtotime($r->mediaAndAdvertising_time_period_start))." ถึง ".date('d/m/Y',strtotime($r->mediaAndAdvertising_time_period_end));

            if(strpos($controllers, "CoverProgramTourOfficial") !== false)
            {
                $program_data = $this->program_tour_models->GetProgramTourData("pt.programTour_id = ".$r->ProgramTour_programTour_id." AND pt.programTour_isActive = 1 AND pt.programTour_isSaveDraft = 0");
                $ScopeArea = $this->program_tour_models->GetScope($program_data[0]->DatesTrip);
                if(count($program_data) !=0)
                {
                    $data[] = array(
                        '<div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table shoping-cart-table display nowrap" style="width:100%" id="DatasTables">
                                    <tbody>
                                        <tr>
                                            <td width="190">
                                                <img src="/dasta_thailand/assets/img/uploadfile/'.$r->coverItem_paths.'" class="cart-product-imitation" >
                                            </td>
                                            <td class="desc">
                                                <h3><a href="#" class="text-navy">'.$r->mediaAndAdvertising_nameThai.'</a></h3>
                                                <p class="">ประเภททัวร์ : '.$program_data[0]->subcategory_thai.'</p>
                                                <p class="">ขอบเขตทัวร์ : '.$ScopeArea.'</p>
                                                <p class="">ช่วงเวลาที่จะให้แสดง : '.$time_period.'</p>
                                               
                                                <div class="m-t-sm">
                                                    <button type="button" class="text-muted btn-in-table ViewItemsModal" data-toggle="modal" data-target="#ViewItemsModal" value="'.$r->mediaAndAdvertising_id.'"> 
                                                        <i class="fa fa-folder"></i> '.$this->lang->line("view").'
                                                    </button>
                                                    |
                                                    <a href="./'.$controllers.'/edit'.$controllers.'?mediaAndAdvertising_id='.$r->mediaAndAdvertising_id.'" class="text-muted">
                                                        <i class="fa fa-pencil"></i> '.$this->lang->line("edit").'</a>
                                                    |
                                                    <button class="text-muted btn-in-table btn-item-del" value="'.$r->mediaAndAdvertising_id.'">
                                                        <i class="fa fa-trash-o"></i> ลบ</button>
                                                    <p class="text-right">'.$r->mediaAndAdvertising_createdDTTM.'</p>
                                                </div>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>',
                        $r->mediaAndAdvertising_nameThai,
                        $program_data[0]->subcategory_thai,
                        date('Y-m-d',strtotime( $r->mediaAndAdvertising_time_period_start))." / ".date('Y-m-d',strtotime($r->mediaAndAdvertising_time_period_end)),
                        $r->mediaAndAdvertising_createdDTTM,
                    );
                }
                
            }
            else if(strpos($controllers, "News") !== false)
            {
                $data[] = array(
                        '<div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table shoping-cart-table display nowrap" style="width:100%" id="DatasTables">
                                    <tbody>
                                        <tr>
                                            <td width="190">
                                                <img src="/dasta_thailand/assets/img/uploadfile/'.$r->coverItem_paths.'" class="cart-product-imitation" >
                                            </td>
                                            <td class="desc">
                                                <h3><a href="#" class="text-navy">'.$r->mediaAndAdvertising_nameThai.'</a></h3>
                                                <p class="">หมวดหมู่สถานที่ : '.$r->menuItem_thai.'</p>
                                                <p class="">'.$this->lang->line("address").' : </p>
                                                <p class="">ช่วงเวลาที่จะให้แสดง : '.$time_period.'</p>
                                            
                                                <div class="m-t-sm">
                                                    <button type="button" class="text-muted btn-in-table ViewItemsModal" data-toggle="modal" data-target="#ViewItemsModal" value="'.$r->mediaAndAdvertising_id.'"> 
                                                        <i class="fa fa-folder"></i> '.$this->lang->line("view").'
                                                    </button>
                                                    |
                                                    <a href="./'.$controllers.'/edit'.$controllers.'?mediaAndAdvertising_id='.$r->mediaAndAdvertising_id.'" class="text-muted">
                                                        <i class="fa fa-pencil"></i> '.$this->lang->line("edit").'</a>
                                                    |
                                                    <button class="text-muted btn-in-table btn-item-del" value="'.$r->mediaAndAdvertising_id.'">
                                                        <i class="fa fa-trash-o"></i> ลบ</button>
                                                    <p class="text-right">'.$r->mediaAndAdvertising_createdDTTM.'</p>
                                                </div>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>',
                        $r->mediaAndAdvertising_nameThai,
                        $r->MenuItem_menuItem_id,
                        "",
                        date('Y-m-d',strtotime( $r->mediaAndAdvertising_time_period_start))." / ".date('Y-m-d',strtotime($r->mediaAndAdvertising_time_period_end)),
                        $r->mediaAndAdvertising_createdDTTM,
                );
            }
            else if(strpos($controllers, "HotelAds") !== false)
            {
                $Hotel = $this->items_models->GetItemData("i.items_isActive = 1 AND i.items_isPublish = 1 AND mi.menuItem_id = 5 AND cat.category_id = 5 AND i.items_id = ".$r->Items_items_id);
                if(count($Hotel) != 0)
                {
                    $data[] = array(
                        '<div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table shoping-cart-table display nowrap" style="width:100%" id="DatasTables">
                                    <tbody>
                                        <tr>
                                            <td width="190">
                                                <img src="/dasta_thailand/assets/img/uploadfile/'.$r->coverItem_paths.'" class="cart-product-imitation" >
                                            </td>
                                            <td class="desc">
                                                <h3><a href="#" class="text-navy">'.$r->mediaAndAdvertising_nameThai.'</a></h3>
                                                <p class="">ชื่อที่พัก/โรงแรม : '.$Hotel[0]->itmes_topicThai.'</p>
                                                <p class="">'.$this->lang->line("address").' : '.$Hotel[0]->items_contactThai.'</p>
                                                <p class="">ประเภทที่พัก : '.$r->subcategory_thai.'</p>
                                                <p class="">ช่วงเวลาที่จะให้แสดง : '.$time_period.'</p>
                                            
                                                <div class="m-t-sm">
                                                    <button type="button" class="text-muted btn-in-table ViewItemsModal" data-toggle="modal" data-target="#ViewItemsModal" value="'.$r->mediaAndAdvertising_id.'"> 
                                                        <i class="fa fa-folder"></i> '.$this->lang->line("view").'
                                                    </button>
                                                    |
                                                    <a href="./'.$controllers.'/edit'.$controllers.'?mediaAndAdvertising_id='.$r->mediaAndAdvertising_id.'" class="text-muted">
                                                        <i class="fa fa-pencil"></i> '.$this->lang->line("edit").'</a>
                                                    |
                                                    <button class="text-muted btn-in-table btn-item-del" value="'.$r->mediaAndAdvertising_id.'">
                                                        <i class="fa fa-trash-o"></i> ลบ</button>
                                                    <p class="text-right">'.$r->mediaAndAdvertising_createdDTTM.'</p>
                                                </div>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>',
                        $r->mediaAndAdvertising_nameThai,
                        $Hotel[0]->itmes_topicThai,
                        $Hotel[0]->items_contactThai,
                        $r->SubCategory_subcategory_id,
                        date('Y-m-d',strtotime( $r->mediaAndAdvertising_time_period_start))." / ".date('Y-m-d',strtotime($r->mediaAndAdvertising_time_period_end)),
                        $r->mediaAndAdvertising_createdDTTM,
                    );
                }
            }
            else if(strpos($controllers, "PackageToursAds") !== false)
            {
                $PackageTours = $this->items_models->GetItemReservationsData("i.items_isActive = 1 AND i.items_isPublish = 1 AND mi.menuItem_id = 9 AND i.items_id = ".$r->Items_items_id,"PackageTours");
                if(count($PackageTours) != 0)
                {
                    $data[] = array(
                        '<div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table shoping-cart-table display nowrap" style="width:100%" id="DatasTables">
                                    <tbody>
                                        <tr>
                                            <td width="190">
                                                <img src="/dasta_thailand/assets/img/uploadfile/'.$r->coverItem_paths.'" class="cart-product-imitation" >
                                            </td>
                                            <td class="desc">
                                                <h3><a href="#" class="text-navy">'.$r->mediaAndAdvertising_nameThai.'</a></h3>
                                                <p class="">แพคเกจทัวร์ที่เลือก : '.$PackageTours[0]->itmes_topicThai.'</p>
                                                <p class="">ขอบเขตทัวร์ : </p>
                                                <p class="">ประเภททัวร์ : '.$r->subcategory_thai.'</p>
                                                <p class="">ช่วงเวลาที่จะให้แสดง : '.$time_period.'</p>
                                            
                                                <div class="m-t-sm">
                                                    <button type="button" class="text-muted btn-in-table ViewItemsModal" data-toggle="modal" data-target="#ViewItemsModal" value="'.$r->mediaAndAdvertising_id.'"> 
                                                        <i class="fa fa-folder"></i> '.$this->lang->line("view").'
                                                    </button>
                                                    |
                                                    <a href="./'.$controllers.'/edit'.$controllers.'?mediaAndAdvertising_id='.$r->mediaAndAdvertising_id.'" class="text-muted">
                                                        <i class="fa fa-pencil"></i> '.$this->lang->line("edit").'</a>
                                                    |
                                                    <button class="text-muted btn-in-table btn-item-del" value="'.$r->mediaAndAdvertising_id.'">
                                                        <i class="fa fa-trash-o"></i> ลบ</button>
                                                    <p class="text-right">'.$r->mediaAndAdvertising_createdDTTM.'</p>
                                                </div>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>',
                        $r->mediaAndAdvertising_nameThai,
                        "",
                        $r->SubCategory_subcategory_id,
                        "",
                        "",
                        date('Y-m-d',strtotime( $r->mediaAndAdvertising_time_period_start))." / ".date('Y-m-d',strtotime($r->mediaAndAdvertising_time_period_end)),
                        $r->mediaAndAdvertising_createdDTTM,
                    );
                }
            }
            else if(strpos($controllers, "EventTicketAds") !== false)
            {
                $EventTicket = $this->items_models->GetItemReservationsData("i.items_isActive = 1 AND i.items_isPublish = 1 AND mi.menuItem_id = 10 AND i.items_id = ".$r->Items_items_id,"EventTicket");
                if(count($EventTicket) != 0)
                {
                    $data[] = array(
                        '<div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table shoping-cart-table display nowrap" style="width:100%" id="DatasTables">
                                    <tbody>
                                        <tr>
                                            <td width="190">
                                                <img src="/dasta_thailand/assets/img/uploadfile/'.$r->coverItem_paths.'" class="cart-product-imitation" >
                                            </td>
                                            <td class="desc">
                                                <h3><a href="#" class="text-navy">'.$r->mediaAndAdvertising_nameThai.'</a></h3>
                                                <p class="">ตั๋วกิจกรรมที่เลือก : '.$EventTicket[0]->itmes_topicThai.'</p>
                                                <p class="">สถานที่เข้าชมกิจกรรม : </p>
                                                <p class="">ประเภทตั๋วกิจกรรม : '.$r->subcategory_thai.'</p>
                                                <p class="">บริษัท : </p>
                                                <p class="">ช่วงเวลาที่จะให้แสดง : '.$time_period.'</p>
                                            
                                                <div class="m-t-sm">
                                                    <button type="button" class="text-muted btn-in-table ViewItemsModal" data-toggle="modal" data-target="#ViewItemsModal" value="'.$r->mediaAndAdvertising_id.'"> 
                                                        <i class="fa fa-folder"></i> '.$this->lang->line("view").'
                                                    </button>
                                                    |
                                                    <a href="./'.$controllers.'/edit'.$controllers.'?mediaAndAdvertising_id='.$r->mediaAndAdvertising_id.'" class="text-muted">
                                                        <i class="fa fa-pencil"></i> '.$this->lang->line("edit").'</a>
                                                    |
                                                    <button class="text-muted btn-in-table btn-item-del" value="'.$r->mediaAndAdvertising_id.'">
                                                        <i class="fa fa-trash-o"></i> ลบ</button>
                                                    <p class="text-right">'.$r->mediaAndAdvertising_createdDTTM.'</p>
                                                </div>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>',
                        $r->mediaAndAdvertising_nameThai,
                        "",
                        $r->SubCategory_subcategory_id,
                        "",
                        date('Y-m-d',strtotime( $r->mediaAndAdvertising_time_period_start))." / ".date('Y-m-d',strtotime($r->mediaAndAdvertising_time_period_end)),
                        $r->mediaAndAdvertising_createdDTTM,
                    );
                }
            }
            else if(strpos($controllers, "CarRentAds") !== false)
            {
                $CarRent = $this->business_models->GetBusinessData("b.business_type_category_id = 11 AND u.user_isActive = 1 AND u.user_isDelete = 0 AND b.business_id = ".$r->Business_business_id,"CarRent");
                if(count($CarRent) != 0)
                {
                    $data[] = array(
                        '<div class="ibox-content">
                            <div class="table-responsive">
                                <table class="table shoping-cart-table display nowrap" style="width:100%" id="DatasTables">
                                    <tbody>
                                        <tr>
                                            <td width="190">
                                                <img src="/dasta_thailand/assets/img/uploadfile/'.$r->coverItem_paths.'" class="cart-product-imitation" >
                                            </td>
                                            <td class="desc">
                                                <h3><a href="#" class="text-navy">'.$r->mediaAndAdvertising_nameThai.'</a></h3>
                                                <p class="">บริษัท : '.$CarRent[0]->business_nameThai.'</p>
                                                <p class="">ช่วงเวลาที่จะให้แสดง : '.$time_period.'</p>
                                            
                                                <div class="m-t-sm">
                                                    <button type="button" class="text-muted btn-in-table ViewItemsModal" data-toggle="modal" data-target="#ViewItemsModal" value="'.$r->mediaAndAdvertising_id.'"> 
                                                        <i class="fa fa-folder"></i> '.$this->lang->line("view").'
                                                    </button>
                                                    |
                                                    <a href="./'.$controllers.'/edit'.$controllers.'?mediaAndAdvertising_id='.$r->mediaAndAdvertising_id.'" class="text-muted">
                                                        <i class="fa fa-pencil"></i> '.$this->lang->line("edit").'</a>
                                                    |
                                                    <button class="text-muted btn-in-table btn-item-del" value="'.$r->mediaAndAdvertising_id.'">
                                                        <i class="fa fa-trash-o"></i> ลบ</button>
                                                    <p class="text-right">'.$r->mediaAndAdvertising_createdDTTM.'</p>
                                                </div>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>',
                        $r->mediaAndAdvertising_nameThai,
                        "",
                        date('Y-m-d',strtotime( $r->mediaAndAdvertising_time_period_start))." / ".date('Y-m-d',strtotime($r->mediaAndAdvertising_time_period_end)),
                        $r->mediaAndAdvertising_createdDTTM,
                    );
                }
            }
        }
        return $data;
    }
    
}
?>