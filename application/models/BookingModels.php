<?php
date_default_timezone_set('Asia/Bangkok');
class BookingModels extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->model("ItemsModels","items_models", true);
    }
    public function GetBookingDataOrderDetails($where)
    {
        $this->db->select("pt.*,od.Items_items_id,COUNT(od.orderDetails_id) as quality");
        $this->db->from('PaymentTransaction pt'); 
        $this->db->join('OrderDetails od','od.PaymentTransaction_paymentTransaction_id = pt.paymenttransaction_id');
        $this->db->group_by("od.PaymentTransaction_paymentTransaction_id");
        $this->db->where($where);
        $query = $this->db->get();

        return $query->result();
    }
    public function GetBookingDataPaymentTransaction($where)
    {
        $this->db->select("b.*,pt.*");
        $this->db->from('Booking b'); 
        $this->db->join('PaymentTransaction pt','pt.paymentTransaction_id = b.PaymentTransaction_paymentTransaction_id');
        $this->db->where($where);
        $query = $this->db->get();
       
        return $query->result();
    }
    public function GetBookingData($where)
    {
        $this->db->where($where);
        $query = $this->db->get("Booking");
        return $query->result();
    }
    public function AddBookingData($data)
    {
        $this->db->insert("Booking", $data);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to Booking",
                'insert_id' =>-1,
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "Add Booking successfully",
                    'insert_id' => $this->db->insert_id(),
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "Add Booking successfully, but it looks like you haven't updated anything!",
                    'insert_id' => 0,
                    'state' => false
                );
            }
        }
        return  $res;
    }
    public function UpdateBookingData($data,$where)
    {
        $this->db->where($where);
        $this->db->update("Booking", $data);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to edit Booking",
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "Booking edited successfully",
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "Booking edited successfully, but it looks like you haven't updated anything!",
                    'state' => true
                );
            }
        }
        return $res;
    }
    public function DeleteBookingData($where)
    {
        $this->db->delete("Booking", $where);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to Delete Booking",
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "Delete Booking successfully",
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "Delete Booking successfully, but it looks like you haven't updated anything!",
                    'state' => true
                );
            }
        }
        return $res;
    }
}
?>