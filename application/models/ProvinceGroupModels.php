<?php 
date_default_timezone_set('Asia/Bangkok');
// define ("SECRETKEY", "KanTasStudio");
class ProvinceGroupModels extends CI_Model {
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        // if(strcmp($this->session->userdata('lang'),"EN") == 0)
        //     $this->lang->load("english","english");
        // else
        //     $this->lang->load("thailand","english");
    }

    public function GetProvinceGroup()
    {
        $this->db->select("pg.*,
        (
            select GROUP_CONCAT(DISTINCT p.provinces_thai order by p.provinces_id asc SEPARATOR '-') 
            from PermissionProvinceGroup ppg 
            inner join Provinces p on p.provinces_id = ppg.Provinces_provinces_id
            where ppg.ProvinceGroup_pg_id = pg.pg_id 
        ) as PermissionsArea");
        $this->db->from('ProvinceGroup pg'); 
        $this->db->where('pg.pg_isDelete',0);
        return $this->db->get();
    }

    public function getPermissionProvinceGroupData($where)
    {
        $this->db->select("p.*");
        $this->db->from('PermissionProvinceGroup ppg');
        $this->db->join('Provinces p','p.provinces_id = ppg.Provinces_provinces_id');
        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    
    public function setDatatable()
    {
        $item = $this->GetProvinceGroup();
        $data = array();
        foreach($item->result() as $key => $r) {
            $data[] = array(
                "",
                $r->pg_thai,
                $r->PermissionsArea,
                $r->pg_isActive == 1?'<span class="label label-primary">เปิดใช้งาน</span>':'<span class="label label-danger">ปิดใช้งาน</span>',
                '<button type="button" class="btn btn-outline btn-warning btn-sm" value="'.$r->pg_id.'" id="btn_edit_'.$r->pg_id.'">
                    <i class="fa fa-pencil"></i> '.$this->lang->line("edit").' </button>
                 <button type="button" class="btn btn-outline btn-danger btn-sm" value="'.$r->pg_id.'" id="btn_del_'.$r->pg_id.'">
                    <i class="fa fa-trash-o"></i> ลบ 
                </button>'
            );
        }
        return $data;
    }
    public function AddProvinceGroupData($data,$table)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }
}