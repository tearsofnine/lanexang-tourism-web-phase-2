<?php
date_default_timezone_set('Asia/Bangkok');
class OrderDetailsModels extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
        $this->load->model("ItemsModels","items_models", true);
    }
    public function GetOrderDetailsData($where)
    {
        $this->db->where($where);
        $query = $this->db->get("OrderDetails");
        return $query->result();
    }
    public function AddOrderDetailsData($data)
    {
        $this->db->insert("OrderDetails", $data);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to OrderDetails",
                'insert_id' =>-1,
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "Add OrderDetails successfully",
                    'insert_id' => $this->db->insert_id(),
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "Add OrderDetails successfully, but it looks like you haven't updated anything!",
                    'insert_id' => 0,
                    'state' => true
                );
            }
        }
        return  $res;
    }
    public function UpdateOrderDetailsData($data,$where)
    {
        $this->db->where($where);
        $this->db->update("OrderDetails", $data);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to edit OrderDetails",
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "OrderDetails edited successfully",
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "OrderDetails edited successfully, but it looks like you haven't updated anything!",
                    'state' => true
                );
            }
        }
        return $res;
    }
    public function DeleteOrderDetailsData($where)
    {
        $this->db->delete("OrderDetails", $where);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to Delete OrderDetails",
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "Delete OrderDetails successfully",
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "Delete OrderDetails successfully, but it looks like you haven't updated anything!",
                    'state' => true
                );
            }
        }
        return $res;
    }
}
?>