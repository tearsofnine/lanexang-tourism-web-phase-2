<?php
define("GOOGLE_API_KEY", "AAAAOYbcpeo:APA91bF5-LSqe9yLgpSALTzXAFcDaeM0Y7_n7ZAhY0L8_wUwMv4cGZ8BUWOZw0Jp916UsvXuRSskQM64j4jA1CJaRFoOeAq3JrMM6I8qTeHJMUiepEa4rctN44uAT7-wJO97wWK9RYHE");
// push notification flags
define('PUSH_FLAG_CHATROOM', 1);
define('PUSH_FLAG_USER', 2);
date_default_timezone_set('Asia/Bangkok');
class GCMModels extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
       
    }
    // sending push message to single user by gcm registration id
    public function send($to, $message) 
    {
        $notification= array(
            "title"=> "Notifications Message",
            "body"=> $message,
            
            //"click_action" => "https://dummypage.com",
        );
        // $data = array(
        //     "data" =>$notification
        // );
        $fields = array(
            'notification' => $notification,
            'to' => $to,
            "priority"=> "high",
            //'data' => $message,
        );
        return $this->sendPushNotification($fields);
    }

    // Sending message to a topic by topic id
    public function sendToTopic($to, $message)
     {
        $fields = array(
            'to' => '/topics/' . $to,
            'notification' => $message,
        );
        return $this->sendPushNotification($fields);
    }

    // sending push message to multiple users by gcm registration ids
    public function sendMultiple($registration_ids, $message) 
    {
        $fields = array(
            'registration_ids' => $registration_ids,
            'notification' => $message,
        );

        return $this->sendPushNotification($fields);
    }

    // function makes curl request to gcm servers
    private function sendPushNotification($fields) 
    {
        // Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';

        $headers = array(
            'Authorization: key=' . GOOGLE_API_KEY,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }

        // Close connection
        curl_close($ch);

        return $result;
    }
}
?>