<?php
date_default_timezone_set('Asia/Bangkok');
class ChatDbHandler extends CI_Model {
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
       
    }
    // updating user GCM registration ID
    public function updateGcmID($user_id, $gcm_registration_id)
     {
        $res_message = new stdClass();
        $data_gcm_registration_id = array(
            "gcm_registration_id" =>$gcm_registration_id
        );

        $this->db->where("user_id = ".$user_id);
        $this->db->update("User", $data_gcm_registration_id);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res_message->error = true;
            $res_message->message = "Failed to update GCM registration ID";
            $res = $res_message;
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res_message->error = false;
                $res_message->message = "GCM registration ID updated successfully";
                $res = $res_message;
                
            }
            else
            {
                $res_message->error = true;
                $res_message->message = "GCM edited successfully, but it looks like you haven't updated anything!";
                $res = $res_message;
               
            }
        }
        return $res;
    }
    // fetching single user by id
    public function getUser($user_id) 
    {

    }
    // fetching multiple users by ids
    public function getUsers($user_ids) 
    {
    }

    public function getMessages($where) 
    {
        $this->db->where($where);
        $query = $this->db->get("Messages");
        return $query->result();
    }

    // messaging in a chat room / to persional message
    public function addMessages($user_id, $chat_room_id, $message) 
    {
        $res_tmp = new stdClass();
        $res_message = new stdClass();

        $messages = array(
            "ChatRooms_chatroom_id" => $chat_room_id,
            "User_user_id" => $user_id,
            "message" => $message,
            "message_created_at" => date("Y-m-d H:i:s"),
        );

        $this->db->insert('Messages',$messages);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res_message->message = "Failed send message";
            $res_message->error = true;
            $res = $res_message;
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $data_message = $this->getMessages("message_id = ".$this->db->insert_id());
                
                $res_tmp->message_id = $data_message[0]->message_id;
                $res_tmp->chat_room_id = $data_message[0]->ChatRooms_chatroom_id;
                $res_tmp->message = $message;
                $res_tmp->message_created_at = $data_message[0]->message_created_at;
                
                $res_message->error = false;
                $res_message->message = $res_tmp;
                $res =  $res_message;
            }
            else
            {
                $res_message->message = "Send message successfully, but it looks like you haven't updated anything!";
                $res_message->error = true;
                $res = $res_message;
            }
        }
        return $res;
    }
    // fetching all chat rooms
    public function getAllChatrooms() 
    {
    }
    // fetching single chat room by id
    function getChatRoom($chat_room_id) 
    {
    }
    /**
     * Checking for duplicate user by email address
     * @param String $email email to check in db
     * @return boolean
     */
    private function isUserExists($email) 
    {
    }
    /**
     * Fetching user by email
     * @param String $email User email id
     */
    public function getUserByEmail($email) 
    {
    }
}
?>