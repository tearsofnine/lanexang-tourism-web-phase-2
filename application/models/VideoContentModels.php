<?php
date_default_timezone_set('Asia/Bangkok');
class VideoContentModels extends CI_Model {
    
    public function __construct(){
        parent::__construct();
        $this->load->library('session');
    }
    public function GetVideoContentData($where)
    {
        $this->db->select("vc.*,cat.*,d.*,p.*,cou.*");
        $this->db->from('VideoContent vc');
        $this->db->join('Category cat','cat.category_id = vc.Category_category_id');
        $this->db->join('Districts d','d.districts_id =  vc.Districts_districts_id');
        $this->db->join('Provinces p','p.provinces_id = d.Provinces_provinces_id');
        $this->db->join('Country cou','cou.country_id = p.Country_country_id');

        $this->db->where($where);
        $query = $this->db->get();
        return $query->result();
    }
    public function AddVideoContentData($data)
    {
        $this->db->insert("VideoContent", $data);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to VideoContent",
                'insert_id' =>-1,
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "Insert VideoContent successfully",
                    'insert_id' => $this->db->insert_id(),
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "Insert VideoContent successfully, but it looks like you haven't updated anything!",
                    'insert_id' => 0,
                    'state' => true
                );
            }
        }
        return  $res;
    }

    public function UpdateVideoContentData($data,$where)
    {
        $this->db->where($where);
        $this->db->update("VideoContent", $data);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to edit VideoContent",
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "VideoContent edited successfully",
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "VideoContent edited successfully, but it looks like you haven't updated anything!",
                    'state' => true
                );
            }
        }
        return $res;
    }

    public function DeleteVideoContentData($where)
    {
        $this->db->delete("VideoContent", $where);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to Delete VideoContent",
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "Delete VideoContent successfully",
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "Delete VideoContent successfully, but it looks like you haven't updated anything!",
                    'state' => true
                );
            }
        }
        return $res;
    }
    public function setDatatableVideoContent($where)
    {
        $data_video_content = $this->GetVideoContentData($where);
        $data = array();
        foreach($data_video_content as $key => $r)
        {
            $matches = array();
            $regex = "/http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?‌​[\w\?‌​=]*)?/";
            preg_match($regex, $r->videoContent_url, $matches);
            $data[] = array(
                '<div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table shoping-cart-table display nowrap" style="width:100%" id="DatasTables">
                            <tbody>
                                <tr>
                                    <td width="190">
                                        <iframe width="270" height="240" id="player" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" 
                                            src="https://www.youtube-nocookie.com/embed/'.$matches[1].'?controls=0">
                                            [ video ]
                                        </iframe>
                                    </td>
                                    <td class="desc">
                                        <h3><a href="#" class="text-navy">'.$r->videoContent_textThai.'</a></h3>
                                        <p class="">ประเภท : '.$r->category_thai.'</p>
                                        <div class="m-t-sm">
                                            <button type="button" class="text-muted btn-in-table ViewItemsModal" data-toggle="modal" data-target="#ViewItemsModal" value="'.$r->videoContent_id.'"> 
                                                <i class="fa fa-folder"></i> '.$this->lang->line("view").'
                                            </button>
                                            |
                                            <a href="./VideoContent/editVideoContent?videoContent_id='.$r->videoContent_id.'" class="text-muted">
                                                <i class="fa fa-pencil"></i> '.$this->lang->line("edit").'</a>
                                            |
                                            <button class="text-muted btn-in-table btn-item-del" value="'.$r->videoContent_id.'">
                                                <i class="fa fa-trash-o"></i> ลบ</button>
                                            <p class="text-right">'.$r->videoContent_createdDTTM.'</p>
                                        </div>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>',
                $r->videoContent_textThai,
                $r->Category_category_id,
                $r->videoContent_createdDTTM,
            );
        }
        return $data;
    }
}
?>