<?php
date_default_timezone_set('Asia/Bangkok');
// define ("SECRETKEY", "KanTasStudio");
class UserManagementModels extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        // if(strcmp($this->session->userdata('lang'),"EN") == 0)
        //     $this->lang->load("english","english");
        // else
        //     $this->lang->load("thailand","english");
    }
    public function getUserManagement($where)
    {
        $str_select = "u.user_id,u.user_email,u.user_firstName,u.user_lastName,u.user_gender,u.user_age,u.user_phone,u.UserRole_userRole_id,u.ProvinceGroup_pg_id ,u.user_createdDTTM,u.user_updatedDTTM,u.user_isActive,u.user_isDelete,";
        $str_select .= 'ur.userRole_textThai,ppg.ppg_id,';
        $str_select .= 'p.provinces_thai,p.provinces_english,p.provinces_chinese,p.provinces_laos';
        $this->db->select($str_select);
        $this->db->from('User u');
        $this->db->join('UserRole ur', 'ur.userRole_id = u.UserRole_userRole_id');

        $this->db->join('ProvincePermission pp', 'pp.User_user_id = u.user_id', 'left');
        $this->db->join('PermissionProvinceGroup ppg', 'ppg.ppg_id = pp.PermissionProvinceGroup_ppg_id', 'left');
        $this->db->join('Provinces p', 'p.provinces_id = ppg.Provinces_provinces_id', 'left');

        $this->db->where($where." AND u.user_isDelete = 0 AND u.user_id != ".$this->session->userdata('user_id'));

        return $this->db->get();
    }
    public function setDatatable($where)
    {
        $item = $this->getUserManagement($where);
        $data = array();
        foreach ($item->result() as $key => $r) {
            $data[] = array(
                "",
                $r->user_email,
                $r->user_firstName . " " . $r->user_lastName,
                $r->user_gender,
                $r->user_age,
                $r->user_phone,
                $r->userRole_textThai,
                $r->provinces_thai,
                $r->user_isActive == 1?'<button type="button" class="btn label label-primary DisabledUser" value="' . $r->user_id . '_0">เปิดใช้งาน</button>':'<button type="button" class="btn label label-danger DisabledUser" value="' . $r->user_id . '_1">ปิดใช้งาน</button>',
                '<button type="button" class="btn btn-outline btn-warning btn-sm btn-edit-usermanagement" value="' . $r->user_id . '" id="btn_edit_' . $r->user_id . '">
                    <i class="fa fa-pencil"></i> ' . $this->lang->line("edit") . ' </button>
                <button type="button" class="btn btn-outline btn-danger btn-sm btn-dal-usermanagement" value="' . $r->user_id . '" id="btn_del_' . $r->user_id . '">
                    <i class="fa fa-trash-o"></i> ลบ 
                </button>'

            );
        }
        return $data;
    }
    public function getDataFromTableName($table,$where = "")
    {
        if($where != "")
            $this->db->where($where);
        $query = $this->db->get($table);
        return $query->result();
    }
    public function getProvinceFromProvinceGroup($where)
    {
        $str_select = "ppg.ppg_id,p.provinces_id,provinces_thai,p.provinces_english,p.provinces_chinese,p.provinces_laos,ppg.ProvinceGroup_pg_id";
        $this->db->select($str_select);
        $this->db->from('PermissionProvinceGroup ppg');
        $this->db->join('Provinces p', 'p.provinces_id = ppg.Provinces_provinces_id');

        $this->db->where($where);
        $query = $this->db->get();
        return  $query->result();
    }
    public function insertData($table,$data)
    {
        $this->db->insert($table,$data);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to Insert",
                'insert_id' => -1,
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "User Insert successfully",
                    'insert_id' => $this->db->insert_id(),
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "User Insert successfully, but it looks like you haven't updated anything!",
                    'insert_id' => 0,
                    'state' => true
                );
            }
        }
        return $res;
    }
    public function DeleteData($table,$where)
    {
        $this->db->delete($table, $where);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to Delete",
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "Delete successfully",
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "Delete successfully, but it looks like you haven't updated anything!",
                    'state' => true
                );
            }
        }
        return $res;
    }
    public function UpdatData($table,$data,$where)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE)
        {
            $res = array(
                'msg' => "Unable to edit User",
                'state' => false
            );
        }
        else
        {
            if($this->db->affected_rows() > 0)
            {
                $res = array(
                    'msg' => "User edited successfully",
                    'state' => true
                );
            }
            else
            {
                $res = array(
                    'msg' => "User edited successfully, but it looks like you haven't updated anything!",
                    'state' => true
                );
            }
        }
        return $res;
    }
}