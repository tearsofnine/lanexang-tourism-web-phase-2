<?php
date_default_timezone_set('Asia/Bangkok');
class SearchModels extends CI_Model {
    public function __construct(){
        parent::__construct();
        $this->load->library('session');

        $this->load->model("CountriesModels","countries_models", true);
        $this->load->model("UserModel","user_model", true);
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("ProgramTourModels","program_tour_models", true);
        $this->load->model("ProvinceGroupModels","province_group_models", true);
    }
    private function getDataSearch($controllers,$superAdmin,$namesubcategory,$country,$provinces,$districts,$provinces_id,$whereMain)
    {
        $whereIs = "";
        $whereIn = "";
        $whereSubcategory = "";
        if(empty($namesubcategory) && empty($country))
        {
            if($superAdmin)
                $whereInSearch = $whereMain;
            else
            {
                $whereIs = "Provinces_provinces_id";
                $whereIn = "IN (".join(",",$provinces_id).")";
                $whereSubcategory = "";
                $whereInSearch = $whereMain." %s AND i.Subdistricts_subdistricts_id IN(
                    SELECT subdistricts_id
                    FROM Subdistricts
                     where Districts_districts_id IN (
                        SELECT districts_id
                        FROM Districts
                        where %s %s ))";
            }
        }
        else
        {
            $whereInSearch = $whereMain;
            if(!empty($namesubcategory) && empty($country))
            {
                if($superAdmin)
                    $whereInSearch = $whereMain." %s";
                else
                {
                    $whereIs = "Provinces_provinces_id";
                    $whereIn = "IN (".join(",",$provinces_id).")";
                    $whereSubcategory = "";
                    $whereInSearch = $whereMain." %s AND i.Subdistricts_subdistricts_id IN(
                        SELECT subdistricts_id
                        FROM Subdistricts
                        where Districts_districts_id IN (
                            SELECT districts_id
                            FROM Districts
                            where %s %s ))";
                }
                $whereSubcategory = "AND sc.subcategory_id = ".$namesubcategory;
            }
            else if(empty($namesubcategory) && !empty($country))
            {
                if(empty($provinces) && empty($districts))
                {
                    $whereIs = "Provinces_provinces_id";
                    $whereIn = "IN (".(count($provinces_id) == 0?0:join(",",$provinces_id)).")";
                    $whereSubcategory = "";
                }
                else if(!empty($provinces) && empty($districts))
                {
                    $whereIs = "Provinces_provinces_id";
                    $whereIn = "IN (".$provinces.")";
                    $whereSubcategory = "";
                }
                else
                {
                    $whereIs = "districts_id";
                    $whereIn = " = ".$districts;
                    $whereSubcategory = "";
                }
                $whereInSearch = $whereMain." %s AND i.Subdistricts_subdistricts_id IN(
                    SELECT subdistricts_id
                    FROM Subdistricts
                    where Districts_districts_id IN (
                        SELECT districts_id
                        FROM Districts
                        where %s %s ))";
            }
            else
            {
                if(empty($provinces) && empty($districts))
                {
                    $whereIs = "Provinces_provinces_id";
                    $whereIn = "IN (".(count($provinces_id) == 0?0:join(",",$provinces_id)).")";
                    $whereSubcategory = "AND sc.subcategory_id = ".$namesubcategory;
                }
                else if(!empty($provinces) && empty($districts))
                {
                    $whereIs = "Provinces_provinces_id";
                    $whereIn = "IN (".$provinces.")";
                    $whereSubcategory = "AND sc.subcategory_id = ".$namesubcategory;
                }
                else
                {
                    $whereIs = "districts_id";
                    $whereIn = " = ".$districts;
                    $whereSubcategory = "AND sc.subcategory_id = ".$namesubcategory;
                }

                $whereInSearch = $whereMain." %s AND i.Subdistricts_subdistricts_id IN(
                    SELECT subdistricts_id
                    FROM Subdistricts
                    where Districts_districts_id IN (
                        SELECT districts_id
                        FROM Districts
                        where %s %s ))";
            }
        }
        $where = sprintf($whereInSearch,$whereSubcategory,$whereIs,$whereIn);
        switch ($controllers) {
            case 'CarRent':
            case 'EventTicket':
            case 'PackageTours':
                $return_data = $this->items_models->setDatatableReservations($where,$controllers);
            break;
            
            default:
                $return_data = $this->items_models->setDatatable($where,$controllers);
            break;
        }
        return $return_data;
    }
    
    private function getDataSearchCarRent($controllers,$TypesofCars_typesofCars_id,$GearSystem_gearSystem_id,$whereMain)
    {
        $whereIs = "";
        $whereIn = "";
        if(empty($TypesofCars_typesofCars_id) && empty($GearSystem_gearSystem_id))
        {
            $where = $whereMain;
        }
        else
        {
            if(!empty($TypesofCars_typesofCars_id) && empty($GearSystem_gearSystem_id))
            {
                $where = $whereMain." AND tc.typesofCars_id = ".$TypesofCars_typesofCars_id;
            }
            else if(empty($TypesofCars_typesofCars_id) && !empty($GearSystem_gearSystem_id))
            {
                $where = $whereMain." AND gs.gearSystem_id = ".$GearSystem_gearSystem_id;
            }
            else
            {
                $where = $whereMain." AND tc.typesofCars_id = ".$TypesofCars_typesofCars_id." AND gs.gearSystem_id = ".$GearSystem_gearSystem_id;
            }
        }

        $return_data = $this->items_models->setDatatableReservations($where,$controllers);

        return $return_data;
    }
    private function getDataSearchPackageTours($controllers,$namesubcategory,$province_boundary,$NumDatesTrip,$whereMain)
    {
        $whereIs = "";
        $whereIn = "";
        if(empty($namesubcategory) && empty($province_boundary)&& empty($NumDatesTrip))
        {
            $where = $whereMain;
        }
        else
        {
            if(!empty($namesubcategory) && empty($province_boundary) && empty($NumDatesTrip))
            {
                $where = $whereMain." AND sc.subcategory_id = ".$namesubcategory;
            }
            else if(empty($namesubcategory) && !empty($province_boundary) && empty($NumDatesTrip))
            {
                $var = $this->items_models->GetItemDataItemsHasScopeArea("Provinces_provinces_id in(".join(",",$province_boundary).")");
                $array =array();
                foreach ($var as $value) 
                    $array[] = $value->Items_items_id;

                if(count($array) !=0)
                {
                    $items_id =  join(",",$array);
                    $where = $whereMain." AND i.items_id in (". $items_id.")";
                }
                else
                {
                    $where = $whereMain." AND i.items_id in (0)";
                }
            }
            else if(empty($namesubcategory) && empty($province_boundary) && !empty($NumDatesTrip))
            {
                $var = $this->items_models->GetItemsIdByDatesTrip($NumDatesTrip);
                $array =array();
                foreach ($var as $value) 
                    $array[] = $value->items_id;
                
                if(count($array) !=0)
                {
                    $items_id =  join(",",$array);
                    $where = $whereMain." AND i.items_id in (". $items_id.")";
                }
                else
                    $where = $whereMain." AND i.items_id in (0)";
            }
            else
            {
                if(!empty($namesubcategory) && !empty($NumDatesTrip))
                {
                    $where = $whereMain." AND sc.subcategory_id = ".$namesubcategory;

                    $var = $this->items_models->GetItemsIdByDatesTrip($NumDatesTrip);
                    foreach ($var as $value) 
                        $array[] = $value->items_id;
                    $items_id =  join(",",$array);

                    $where .= " AND i.items_id in (". $items_id.")";
                }
                else if(!empty($namesubcategory) && !empty($province_boundary))
                {
                    $var = $this->items_models->GetItemDataItemsHasScopeArea("Provinces_provinces_id in(".join(",",$province_boundary).")");
                    $array =array();
                    foreach ($var as $value) 
                        $array[] = $value->Items_items_id;
                    if(count($array) !=0)
                    {
                        $items_id =  join(",",$array);
                        $where = $whereMain." AND sc.subcategory_id = ".$namesubcategory." AND i.items_id in (". $items_id.")";
                    }
                    else
                    {
                        $where = $whereMain." AND sc.subcategory_id = ".$namesubcategory." AND i.items_id in (0)";
                    }
                }
                elseif(!empty($NumDatesTrip) && !empty($province_boundary))
                {
                    $var = $this->items_models->GetItemsIdByDatesTrip($NumDatesTrip);
                    $array =array();

                    foreach ($var as $value) 
                        $array[] = $value->items_id;

                    $var = $this->items_models->GetItemDataItemsHasScopeArea("Provinces_provinces_id in(".join(",",$province_boundary).")");
                    foreach ($var as $value) 
                        $array[] = $value->Items_items_id;

                    $items_id =  join(",",$array);
                    $where = $whereMain." AND i.items_id in (". $items_id.")";
                }
                else
                {
                    $var = $this->items_models->GetItemsIdByDatesTrip($NumDatesTrip);
                    foreach ($var as $value) 
                        $array[] = $value->items_id;

                    $var = $this->items_models->GetItemDataItemsHasScopeArea("Provinces_provinces_id in(".join(",",$province_boundary).")");
                    foreach ($var as $value) 
                        $array[] = $value->Items_items_id;

                    $items_id =  join(",",$array);
                    $where = $whereMain." AND sc.subcategory_id = ".$namesubcategory." AND i.items_id in (". $items_id.")";
                }
            }
        }
        $return_data = $this->items_models->setDatatableReservations($where,$controllers);
        return $return_data;
    }
    public function getDataSearchForTable($controllers,$namesubcategory,$country,$provinces,$districts,$whereMain)
    {
        switch ($controllers) {
            case 'CarRent':
                $data = $this->getDataSearchCarRent(
                    $controllers,
                    $namesubcategory,
                    $country,
                    $whereMain
                );
            break;
            case 'PackageTours':
                $data = $this->getDataSearchPackageTours(
                    $controllers,
                    $namesubcategory,
                    $country,
                    $provinces,
                    $whereMain
                );
            break;
            default:
                if(is_null($this->session->userdata('UserRole_userRole_id')))
                {
                    $provinces_id = array();
                    $provinces_loop=$this->countries_models->getProvinces(true,array('Country_country_id' => $country),"");
                    if(count($provinces_loop) >0)
                    {
                        foreach($provinces_loop as $r) {
                            $provinces_id[] =  $r->provinces_id;
                        }
                    }
                    $data = $this->getDataSearch(
                        $controllers,
                        true,
                        $namesubcategory,
                        $country,
                        $provinces,
                        $districts,
                        $provinces_id,
                        $whereMain
                    );
                }
                else
                {
                    switch ($this->session->userdata('UserRole_userRole_id')) 
                    {
                        case 1:
                            $provinces_id = array();
                            $provinces_loop=$this->countries_models->getProvinces(true,array('Country_country_id' => $country),"");
                            if(count($provinces_loop) >0)
                            {
                                foreach($provinces_loop as $r) {
                                    $provinces_id[] =  $r->provinces_id;
                                }
                            }
                            $data = $this->getDataSearch(
                                $controllers,
                                true,
                                $namesubcategory,
                                $country,
                                $provinces,
                                $districts,
                                $provinces_id,
                                $whereMain
                            );
                        break;
                        case 2:
                            $userinfor = $this->user_model->getinformationUserAdminManager($this->session->userdata('user_id'),"p.*","p.provinces_id");
                            foreach($userinfor->result() as $r) {
                                if($country == $r->Country_country_id)
                                    $provinces_id[] =  $r->provinces_id;
                                else if(empty($country))
                                    $provinces_id[] =  $r->provinces_id;
                            }
                            $data = $this->getDataSearch(
                                $controllers,
                                false,
                                $namesubcategory,
                                $country,
                                $provinces,
                                $districts,
                                $provinces_id,
                                $whereMain
                            );
                        break;
                        case 3:
                            $userinfor=$this->user_model->getinformationUserAdmin($this->session->userdata('user_id'));
                            $userinfor_data = $userinfor->row_array();
                            $provinces_id[] = $userinfor_data["provinces_id"];
                            $data = $this->getDataSearch(
                                $controllers,
                                false,
                                $namesubcategory,
                                $country,
                                $provinces,
                                $districts,
                                $provinces_id,
                                $whereMain
                            );
                        break;
                        default:
                            
                        break;

                    }
                }
            break;
        }
        return $data;
    }
    public function getDataSearchForTableProgramTour($subcategory,$num_dates_trip,$whereMain)
    {
        $where = "";
        if(empty($subcategory) && empty($num_dates_trip))
        {
            $where = $whereMain;
        }
        else
        {
            if(!empty($subcategory) && empty($num_dates_trip))
            {
                $where = $whereMain." AND sc.subcategory_id = ".$subcategory;
            }
            else if(empty($subcategory) && !empty($num_dates_trip))
            {
                $where = $whereMain. " AND pt.programTour_id IN(
                    SELECT ProgramTour_programTour_id
                    FROM DatesTrip
                    GROUP BY ProgramTour_programTour_id 
                    HAVING COUNT(ProgramTour_programTour_id) = ".$num_dates_trip.")";
            }
            else{
                $where = $whereMain." AND sc.subcategory_id = ".$subcategory. " AND pt.programTour_id IN(
                    SELECT ProgramTour_programTour_id
                    FROM DatesTrip
                    GROUP BY ProgramTour_programTour_id 
                    HAVING COUNT(ProgramTour_programTour_id) = ".$num_dates_trip.")";
            }
        }
        return $this->program_tour_models->setDatatable($where);
    }
    public function SearchItemAPI($str_search = "",$latitude="",$longitude="",$user_id = -1,$category_id = -1)
    {
        if($str_search != "")
        {
            $where ="(i.itmes_topicThai like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            i.itmes_topicEnglish like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            i.itmes_topicChinese like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            i.itmes_topicLaos like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            c.country_Thai like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            c.country_english like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            c.country_chinese like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            c.country_laos like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            p.provinces_thai like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            p.provinces_english like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            p.provinces_chinese like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            p.provinces_laos like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            dt.districts_thai like CONCAT(\"%\", \"".$str_search."\", \"%\") OR
            dt.districts_english like CONCAT(\"%\", \"".$str_search."\", \"%\") OR
            dt.districts_chinese like CONCAT(\"%\", \"".$str_search."\", \"%\") OR
            dt.districts_laos like CONCAT(\"%\", \"".$str_search."\", \"%\") OR
            sdt.subdistricts_thai like CONCAT(\"%\", \"".$str_search."\", \"%\") OR
            sdt.subdistricts_english like CONCAT(\"%\", \"".$str_search."\", \"%\") OR
            sdt.subdistricts_chinese like CONCAT(\"%\", \"".$str_search."\", \"%\") OR
            sdt.subdistricts_laos like CONCAT(\"%\", \"".$str_search."\", \"%\") ) AND 
            (i.items_isActive = 1 AND i.items_isPublish = 1 ".(intval($category_id) == -1?"AND i.MenuItem_menuItem_id in (2,3,4,5,6)":"AND i.MenuItem_menuItem_id = ".$category_id).")";
        }
        else
        {
            $where  ="i.items_isActive = 1 AND i.items_isPublish = 1 AND i.MenuItem_menuItem_id = ".$category_id;
        }
       
        
        return $this->items_models->getSearchItemData($where,$latitude,$longitude,$user_id);
    }
    
    public function ProvinceGroupAPI()
    {
        return $this->province_group_models->getPermissionProvinceGroupData("ppg.ProvinceGroup_pg_id = 1");
    }
    public function SearchItemByProvinceAPI($provinces_id,$latitude="",$longitude="",$user_id = -1,$category_id = -1)
    {
        $where ="p.provinces_id = ".$provinces_id." ".(intval($category_id) == -1?"AND i.MenuItem_menuItem_id in (2,3,4,5,6)":"AND i.MenuItem_menuItem_id = ".$category_id);
        return $this->items_models->getSearchItemData($where,$latitude,$longitude,$user_id);
    }
    public function SearchItemByMenuIdAPI($menuItem_id,$latitude="",$longitude="",$user_id = -1,$category_id = -1)
    {
        $where ="i.MenuItem_menuItem_id = ".$menuItem_id." AND i.items_isActive = 1 AND i.items_isPublish = 1";
        return $this->items_models->getSearchItemData($where,$latitude,$longitude,$user_id);
    }
}
?>