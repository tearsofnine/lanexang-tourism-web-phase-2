<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
class UserManagement extends Base_Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');

        $this->load->model("CountriesModels", "countries_models", true);
        $this->load->model("UserModel", "user_model", true);
        $this->load->model("ItemsModels", "items_models", true);
        $this->load->model("ProvinceGroupModels", "models_ProvinceGroup", true);
        $this->load->model("ChatModels", "chat_models", true);
        $this->load->model("UserManagementModels", "models_UserManagement", true);

        if (strcmp($this->session->userdata('lang'), "EN") == 0)
            $this->lang->load("english", "english");
        else
            $this->lang->load("thailand", "thailand");
    }
    public function index()
    {
        $content['host'] = "/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $content['Approval_Item'] = $this->items_models->CountApprovalItem();
        $content['UnReadMessages'] = $this->chat_models->getUnReadMessages($this->session->userdata('user_id'));

        $this->load->view('Html/UserManagementView', $content);
    }
    public function getDataUserManagement()
    {
        // Datatables Variables
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $UserRole_userRole_id = $this->session->userdata('UserRole_userRole_id');
        switch ($UserRole_userRole_id) {
            case 1:
                $data = $this->models_UserManagement->setDatatable("u.UserRole_userRole_id IN (1,2,3)");
                break;
            case 2:
                $data = $this->models_UserManagement->setDatatable("u.UserRole_userRole_id IN (3)");
                break;

            default:
                $data = [];
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($data),
            "recordsFiltered" => count($data),
            "data" => $data
        );
        echo json_encode($output);
    }
    public function getUserRole()
    {
        $UserRole_userRole_id = $this->session->userdata('UserRole_userRole_id');
        switch ($UserRole_userRole_id) {
            case 1:
                $where = "userRole_id IN (1,2,3)";
                break;
            case 2:
                $where = "userRole_id = 3";
                break;

            default:
                $where = "userRole_id = 3";
        }

        $Role = $this->models_UserManagement->getDataFromTableName("UserRole", $where);
        echo "<option value=\"\">เลือกระดับ</option>";
        foreach ($Role as $item) {
            echo "<option value=\"" . $item->userRole_id . "\">" . $item->userRole_textThai . "</option>";
        }
    }
    public function getProvinceGroup()
    {
        $ProvinceGroup = $this->models_UserManagement->getDataFromTableName("ProvinceGroup", "");
        echo "<option value=\"\">เลือกกลุ่มจังหวัด</option>";
        foreach ($ProvinceGroup as $item) {
            echo "<option value=\"" . $item->pg_id . "\">" . $item->pg_thai . "</option>";
        }
    }
    public function getProvince()
    {
        $Province = $this->models_UserManagement->getProvinceFromProvinceGroup("ppg.ProvinceGroup_pg_id = " . $this->input->post("ProvinceGroup_pg_id"));
        echo "<option value=\"\">เลือกกลุ่มจังหวัด</option>";
        foreach ($Province as $item) {
            echo "<option value=\"" . $item->ppg_id . "\">" . $item->provinces_thai . "</option>";
        }
    }
    public function addNewUser()
    {
        $this->load->library('form_validation');
        if (intval($this->input->post("UserRole_userRole_id"))  == 2) {
            $this->form_validation->set_rules('ProvinceGroup_pg_id', 'required|ProvinceGroup_pg_id|ProvinceGroup_pg_id|กรุณาเลือกกลุ่มจังหวัด', 'trim|required');
        }
        if (intval($this->input->post("UserRole_userRole_id"))  == 3) {
            $this->form_validation->set_rules('ProvinceGroup_pg_id', 'required|ProvinceGroup_pg_id|ProvinceGroup_pg_id|กรุณาเลือกกลุ่มจังหวัด', 'trim|required');
            $this->form_validation->set_rules('province', 'required|province|province|กรุณาเลือกจังหวัด', 'trim|required');
        }
        $this->form_validation->set_rules('user_email', 'required|user_email|user_email|กรุณากรอก Email', 'trim|required|valid_email|callback_email_check');
        $this->form_validation->set_rules('user_password', 'required|user_password|user_password|กรุณากรอกรหัสผ่าน', 'trim|required');
        $this->form_validation->set_rules('user_passwordcon', 'required|user_passwordcon|user_passwordcon|กรุณากรอกรหัสผ่านอีกครั้ง', 'trim|required|matches[user_password]');
        $this->form_validation->set_rules('user_firstName', 'required|user_firstName|user_firstName|กรุณากรอกชื่อ', 'trim|required');
        $this->form_validation->set_rules('user_lastName', 'required|user_lastName|user_lastName|กรุณากรอกนามสกุล', 'trim|required');
        $this->form_validation->set_rules('UserRole_userRole_id', 'required|UserRole_userRole_id|UserRole_userRole_id|กรุณาเลือกระดับการจัดการ', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $res = array('state' => false, 'error' => 'validation', 'msg' => validation_errors());
        } else {
            $password = openssl_encrypt($this->input->post("user_password"), "AES-128-ECB", SECRETKEY);
            $userdate = array(
                "user_email" => $this->input->post("user_email"),
                "user_password" => $password,
                "user_firstName" => $this->input->post("user_firstName"),
                "user_lastName" => $this->input->post("user_lastName"),
                "user_age" => $this->input->post("user_age"),
                "user_gender" => $this->input->post("user_gender"),
                "user_phone" => $this->input->post("user_phone"),
                "UserRole_userRole_id" => $this->input->post("UserRole_userRole_id"),
                "user_isActive" => 1,
                "user_isDelete" => 0,
                "Country_country_id" => 219,
                "User_by_user_id" => $this->session->userdata('user_id'),
                "user_createdDTTM" => date("Y-m-d H:i:s"),
                "ProvinceGroup_pg_id" => NULL,
            );
            switch (intval($this->input->post("UserRole_userRole_id"))) {
                case 1:
                    $res = $this->user_model->AddUserData($userdate);
                    break;
                case 2:
                    $userdate['ProvinceGroup_pg_id'] = intval($this->input->post("ProvinceGroup_pg_id"));
                    $res = $this->user_model->AddUserData($userdate);
                    break;
                case 3:
                    $userdate['ProvinceGroup_pg_id'] = intval($this->input->post("ProvinceGroup_pg_id"));
                    $res = $this->user_model->AddUserData($userdate);
                    $PPDATA = array(
                        "User_user_id" => $res['insert_id'],
                        "PermissionProvinceGroup_ppg_id" => $this->input->post("province"),
                    );
                    $res = $this->models_UserManagement->insertData("ProvincePermission", $PPDATA);
                    break;
            }
            //$res = array('state' => true,'error'=>'', 'msg' => 'เพิ่มสำเร็จ');
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }
    public function editNewUser()
    {
        $this->load->library('form_validation');
        if (intval($this->input->post("UserRole_userRole_id"))  == 2) {
            $this->form_validation->set_rules('ProvinceGroup_pg_id', 'required|ProvinceGroup_pg_id|ProvinceGroup_pg_id|กรุณาเลือกกลุ่มจังหวัด', 'trim|required');
           
        }
        if (intval($this->input->post("UserRole_userRole_id"))  == 3) {
            $this->form_validation->set_rules('ProvinceGroup_pg_id', 'required|ProvinceGroup_pg_id|ProvinceGroup_pg_id|กรุณาเลือกกลุ่มจังหวัด', 'trim|required');
            $this->form_validation->set_rules('province', 'required|province|province|กรุณาเลือกจังหวัด', 'trim|required');
        }

        $this->form_validation->set_rules('user_firstName', 'required|user_firstName|user_firstName|กรุณากรอกชื่อ', 'trim|required');
        $this->form_validation->set_rules('user_lastName', 'required|user_lastName|user_lastName|กรุณากรอกนามสกุล', 'trim|required');
        $this->form_validation->set_rules('UserRole_userRole_id', 'required|UserRole_userRole_id|UserRole_userRole_id|กรุณาเลือกระดับการจัดการ', 'trim|required');
        $this->form_validation->set_rules('user_id', 'required|user_id|user_id|', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $res = array('state' => false, 'error' => 'validation', 'msg' => validation_errors());
        } else {
            $userinfor = $this->models_UserManagement->getUserManagement("u.user_id = " . $this->input->post("user_id"));
            $userinfordata = $userinfor->result();
            $userdate = array(
                "user_firstName" => $this->input->post("user_firstName"),
                "user_lastName" => $this->input->post("user_lastName"),
                "user_age" => $this->input->post("user_age"),
                "user_gender" => $this->input->post("user_gender"),
                "user_phone" => $this->input->post("user_phone"),
                "UserRole_userRole_id" => $this->input->post("UserRole_userRole_id"),
                "user_updatedDTTM" => date("Y-m-d H:i:s"),
                "ProvinceGroup_pg_id" => NULL,
            );

            switch (intval($this->input->post("UserRole_userRole_id"))) {
                case 1:
                    if (intval($userinfordata[0]->UserRole_userRole_id) > 2) {
                        $this->models_UserManagement->DeleteData("ProvincePermission", "User_user_id = " . $this->input->post("user_id"));
                    }
                    $res = $this->user_model->UpdateUserData($userdate, "user_id = " . $this->input->post("user_id"));
                    break;
                case 2:
                    if (intval($userinfordata[0]->UserRole_userRole_id) > 2) {
                        $this->models_UserManagement->DeleteData("ProvincePermission", "User_user_id = " . $this->input->post("user_id"));
                    }
                    $userdate['ProvinceGroup_pg_id'] = intval($this->input->post("ProvinceGroup_pg_id"));
                    $res = $this->user_model->UpdateUserData($userdate, "user_id = " . $this->input->post("user_id"));
                    break;
                case 3:
                    $userdate['ProvinceGroup_pg_id'] = intval($this->input->post("ProvinceGroup_pg_id"));
                    $res = $this->user_model->UpdateUserData($userdate, "user_id = " . $this->input->post("user_id"));
                    if (intval($userinfordata[0]->UserRole_userRole_id) < 3) {
                        $PPDATA = array(
                            "User_user_id" => $this->input->post("user_id"),
                            "PermissionProvinceGroup_ppg_id" => $this->input->post("province"),
                        );
                        $res = $this->models_UserManagement->insertData("ProvincePermission",$PPDATA);
                    }
                    else
                    {
                        $res = $this->models_UserManagement->UpdatData("ProvincePermission",array("PermissionProvinceGroup_ppg_id"=>$this->input->post("province")),"User_user_id = ".$this->input->post("user_id"));
                    }
                    break;
            }
            //$res = array('state' => true,'error'=>'', 'msg' => 'เพิ่มสำเร็จ');
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }
    public function getDataUser()
    {
        $this->load->library('form_validation');
        $userinfor = $this->models_UserManagement->getUserManagement("u.user_id = " . $this->input->post("user_id"));

        return $this->output->set_content_type('application/json')->set_output(json_encode($userinfor->result()[0]));
    }
    public function email_check($email)
    {
        if (!$this->user_model->email_check($email)) {
            $this->form_validation->set_message('email_check', 'This %s has already been used.');
            return FALSE;
        } else {
            return TRUE;
        }
    }
}
