<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
class CarRentAds extends Base_Admin_Controller {
    public function __construct()
    {    	
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');

        $this->load->library('form_validation');
        $config['upload_path'] = './assets/img/uploadfile/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = 2048; 
        $config['encrypt_name'] = true; 
        $this->load->library('upload', $config);

        $this->load->model("CountriesModels","countries_models", true);
        $this->load->model("UserModel","user_model", true);
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("SearchModels","search_models", true);
        $this->load->model("MediaAndAdvertisingModels","media_and_advertising_models", true);
        $this->load->model("ChatModels","chat_models", true);
        $this->load->model("BusinessModels","business_models", true);

        if(strcmp($this->session->userdata('lang'),"EN") == 0)
            $this->lang->load("english","english");
        else
            $this->lang->load("thailand","thailand");
            
    }
    public function index()
	{
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $content['Approval_Item'] = $this->items_models->CountApprovalItem();
        $content['UnReadMessages'] = $this->chat_models->getUnReadMessages($this->session->userdata('user_id'));
        
        $this->load->view('Html/MediaAndAdvertising/CarRentAdsView',$content);
    }
    public function addCarRentAds()
    {
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $content['Approval_Item'] = $this->items_models->CountApprovalItem();
        $content['UnReadMessages'] = $this->chat_models->getUnReadMessages($this->session->userdata('user_id'));
        
        $this->load->view('Html/MediaAndAdvertising/CarRentAdsAddView',$content);
    }
    public function editCarRentAds()
    {
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $content['Approval_Item'] = $this->items_models->CountApprovalItem();
        $content['UnReadMessages'] = $this->chat_models->getUnReadMessages($this->session->userdata('user_id'));

        $content['data_item'] = $this->media_and_advertising_models->GetEditMediaAndAdvertising("maa.mediaAndAdvertising_isActive = 1 AND maa.mediaAndAdvertising_id = ".$this->input->get("mediaAndAdvertising_id")." AND maa.MediaAndAdvertisingType_mediaAndAdvertisingType_id = 6");
        
        if(count($content['data_item']) != 0)
            $this->load->view('Html/MediaAndAdvertising/CarRentAdsEditView',$content);
        else
            redirect('../CarRentAds');
 
    }
    public function saveNewCarRentAds()
    {
        $this->load->library('form_validation');
        
        $config['upload_path'] = './assets/img/uploadfile/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        //$config['max_size'] = 2048; 
        $config['encrypt_name'] = true; 
        $this->load->library('upload', $config);

        $this->form_validation->set_rules('mediaAndAdvertising_nameThai', 'required|mediaAndAdvertising_nameThai|mediaAndAdvertising_nameThai|กรุณากรอกชื่อ', 'trim|required');

       
        $this->form_validation->set_rules('business_id', 'required|business_id|business_id|กรุณาเลือกบริษัท', 'trim|required');
        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {   
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MediaAndAdvertising
            $mediaAndAdvertising_time_period_start = date('Y-m-d',strtotime($this->input->post("mediaAndAdvertising_time_period_start")));
            $mediaAndAdvertising_time_period_end = date('Y-m-d',strtotime($this->input->post("mediaAndAdvertising_time_period_end")));

            $MediaAndAdvertising = array(
                "mediaAndAdvertising_nameThai" =>$this->input->post("mediaAndAdvertising_nameThai"),
                "mediaAndAdvertising_nameEnglish" =>$this->input->post("mediaAndAdvertising_nameEnglish"),
                "mediaAndAdvertising_nameChinese" =>$this->input->post("mediaAndAdvertising_nameChinese"),
                "mediaAndAdvertising_nameLaos" =>$this->input->post("mediaAndAdvertising_nameLaos"),

                "mediaAndAdvertising_time_period_start" =>$mediaAndAdvertising_time_period_start,
                "mediaAndAdvertising_time_period_end" =>$mediaAndAdvertising_time_period_end,

                "Business_business_id" =>$this->input->post("business_id"),

                "MenuItem_menuItem_id" =>11,
                "MediaAndAdvertisingType_mediaAndAdvertisingType_id" =>6,
                "mediaAndAdvertising_isActive" =>1,
                "mediaAndAdvertising_isPublish" =>1,
                "mediaAndAdvertising_createdDTTM" => date("Y-m-d H:i:s"),
                "User_user_id" => $this->session->userdata('user_id'),
            );
            $inserted_id_MediaAndAdvertising = $this->media_and_advertising_models->AddMediaAndAdvertisingData($MediaAndAdvertising,"MediaAndAdvertising");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//CoverItems
            $this->upload->do_upload('cropperImage');
            $upload_data = $this->upload->data();
            $CoverItems = array(
                "coverItem_paths" =>$upload_data["file_name"],
                "MediaAndAdvertising_mediaAndAdvertising_id" =>$inserted_id_MediaAndAdvertising, 
            );
            $inserted_cover_item_id_item = $this->media_and_advertising_models->AddMediaAndAdvertisingData($CoverItems,"CoverItems");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if(empty($_FILES['originalImage']['name'][0]))
            {
                $originalImage = "p3.jpg";
            }
            else
            {
                $this->upload->do_upload('originalImage');
                $upload_data = $this->upload->data();
                $originalImage = $upload_data["file_name"];
            }
            $TempImageCover = array(
                "tempImageCover_paths" =>$originalImage,
                "tempImageCover_CropData" =>$this->input->post("CropData"),
                "tempImageCover_CropBoxData" =>$this->input->post("CropBoxData"),
                "tempImageCover_CanvasData" =>$this->input->post("CanvasData"),
                
                "CoverItems_coverItem_id" =>$inserted_cover_item_id_item,
            );
            $this->media_and_advertising_models->AddMediaAndAdvertisingData($TempImageCover,"TempImageCover");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            $res = array('state' => true,'error'=>'', 'msg' => 'เพิ่มสำเร็จ');
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }
    public function saveEditCarRentAds()
    {
        $this->load->library('form_validation');
        
        $config['upload_path'] = './assets/img/uploadfile/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        //$config['max_size'] = 2048; 
        $config['encrypt_name'] = true; 
        $this->load->library('upload', $config);

        $this->form_validation->set_rules('mediaAndAdvertising_nameThai', 'required|mediaAndAdvertising_nameThai|mediaAndAdvertising_nameThai|กรุณากรอกชื่อ', 'trim|required');

        $this->form_validation->set_rules('business_id', 'required|business_id|business_id|กรุณาเลือกบริษัท', 'trim|required');
        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {   
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MediaAndAdvertising
            $mediaAndAdvertising_time_period_start = date('Y-m-d',strtotime($this->input->post("mediaAndAdvertising_time_period_start")));
            $mediaAndAdvertising_time_period_end = date('Y-m-d',strtotime($this->input->post("mediaAndAdvertising_time_period_end")));

            $mediaAndAdvertising_models_data =  $this->media_and_advertising_models->GetMediaAndAdvertisingData("maa.mediaAndAdvertising_id = ".$this->input->post("mediaAndAdvertising_id"));

            $MediaAndAdvertising = array(
                "mediaAndAdvertising_nameThai" =>$this->input->post("mediaAndAdvertising_nameThai"),
                "mediaAndAdvertising_nameEnglish" =>$this->input->post("mediaAndAdvertising_nameEnglish"),
                "mediaAndAdvertising_nameChinese" =>$this->input->post("mediaAndAdvertising_nameChinese"),
                "mediaAndAdvertising_nameLaos" =>$this->input->post("mediaAndAdvertising_nameLaos"),

                "mediaAndAdvertising_time_period_start" =>$mediaAndAdvertising_time_period_start,
                "mediaAndAdvertising_time_period_end" =>$mediaAndAdvertising_time_period_end,

                "Business_business_id" =>$this->input->post("business_id"),

                "mediaAndAdvertising_createdDTTM" => date("Y-m-d H:i:s"),
                "User_user_id" => $this->session->userdata('user_id'),
            );
            $this->media_and_advertising_models->UpdateMediaAndAdvertisingData($MediaAndAdvertising,"MediaAndAdvertising","mediaAndAdvertising_id = ".$this->input->post("mediaAndAdvertising_id"));

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//CoverItems
            $coverItem_paths = './assets/img/uploadfile/'. $mediaAndAdvertising_models_data[0]->coverItem_paths;
            $tempImageCover_paths = './assets/img/uploadfile/'. $mediaAndAdvertising_models_data[0]->tempImageCover_paths;

            is_readable($coverItem_paths);
            unlink($coverItem_paths);
            if($mediaAndAdvertising_models_data[0]->tempImageCover_paths != "p3.jpg")
            {
                is_readable($tempImageCover_paths);
                unlink($tempImageCover_paths);
            }

            $trans_status = $this->media_and_advertising_models->DeleteMediaAndAdvertisingData("CoverItems","MediaAndAdvertising_mediaAndAdvertising_id = ".$this->input->post("mediaAndAdvertising_id"));

            $this->upload->do_upload('cropperImage');
            $upload_data = $this->upload->data();
            $CoverItems = array(
                "coverItem_paths" =>$upload_data["file_name"],
                "MediaAndAdvertising_mediaAndAdvertising_id" =>$this->input->post("mediaAndAdvertising_id"), 
            );
            $inserted_cover_item_id_item = $this->media_and_advertising_models->AddMediaAndAdvertisingData($CoverItems,"CoverItems");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if(empty($_FILES['originalImage']['name'][0]))
            {
                $originalImage = "p3.jpg";
            }
            else
            {
                $this->upload->do_upload('originalImage');
                $upload_data = $this->upload->data();
                $originalImage = $upload_data["file_name"];
            }
            $TempImageCover = array(
                "tempImageCover_paths" =>$originalImage,
                "tempImageCover_CropData" =>$this->input->post("CropData"),
                "tempImageCover_CropBoxData" =>$this->input->post("CropBoxData"),
                "tempImageCover_CanvasData" =>$this->input->post("CanvasData"),
                
                "CoverItems_coverItem_id" =>$inserted_cover_item_id_item,
            );
            $this->media_and_advertising_models->AddMediaAndAdvertisingData($TempImageCover,"TempImageCover");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            $res = array('state' => true,'error'=>'', 'msg' => 'เพิ่มสำเร็จ');
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }
    public function getDataCarRentAdsForTable()
    {
        // Datatables Variables
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));
        
        $data = $this->media_and_advertising_models->setDatatableMediaAndAdvertising("maa.mediaAndAdvertising_isActive = 1 AND maa.MediaAndAdvertisingType_mediaAndAdvertisingType_id = 6","CarRentAds");
        
        // $data = $this->search_models->getDataSearchForTable(
        //     "PackageTours",
        //     $this->input->post("namesubcategory"),
        //     $this->input->post("country"),
        //     $this->input->post("provinces"),
        //     $this->input->post("districts"),
        //     "i.items_isActive = 1 AND mi.menuItem_id = 2 AND cat.category_id = 2"
        // );

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($data),
            "recordsFiltered" =>count($data),
            "data" => $data
        );
        echo json_encode($output);
    }
    public function getNameBusiness()
    {
        $Business = $this->business_models->GetBusinessData("b.business_type_category_id = 11 AND u.user_isActive = 1 AND u.user_isDelete = 0");
        echo "<option value=\"\">บริษัท</option>";
        
        foreach ($Business as $item)
        {
            echo "<option value=\"".$item->business_id."\">".$item->business_nameThai."</option>";
        }
    }
    
}