<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
class VideoContent extends Base_Admin_Controller {
    public function __construct()
    {    	
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');

        $this->load->model("CountriesModels","countries_models", true);
        $this->load->model("UserModel","user_model", true);
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("SearchModels","search_models", true);
        $this->load->model("VideoContentModels","video_content_models", true);
        $this->load->model("MediaAndAdvertisingModels","media_and_advertising_models", true);
        $this->load->model("ChatModels","chat_models", true);

        if(strcmp($this->session->userdata('lang'),"EN") == 0)
            $this->lang->load("english","english");
        else
            $this->lang->load("thailand","thailand");
    }

    public function index()
	{
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $content['Approval_Item'] = $this->items_models->CountApprovalItem();
        $content['UnReadMessages'] = $this->chat_models->getUnReadMessages($this->session->userdata('user_id'));
        
        $this->load->view('Html/MediaAndAdvertising/VideoContentView',$content);
    }

    public function addVideoContent()
    {
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $content['Approval_Item'] = $this->items_models->CountApprovalItem();
        $content['UnReadMessages'] = $this->chat_models->getUnReadMessages($this->session->userdata('user_id'));
        
        $this->load->view('Html/MediaAndAdvertising/VideoContentAddView',$content);
    }

    public function editVideoContent()
    {
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $content['Approval_Item'] = $this->items_models->CountApprovalItem();
        $content['UnReadMessages'] = $this->chat_models->getUnReadMessages($this->session->userdata('user_id'));

        $content['data_item'] = $this->video_content_models->GetVideoContentData("vc.videoContent_id = ".$this->input->get("videoContent_id")." AND vc.videoContent_isActive = 1");
        
        if(count($content['data_item']) != 0)
            $this->load->view('Html/MediaAndAdvertising/VideoContentEditView',$content);
        else
            redirect('../VideoContent');
    }

    public function getNameCategory()
    {
        $NameCategory = $this->items_models->getNameCategory(2);
        echo "<option value=\"\">หมวดหมู่สถานที่</option>";
        
        foreach ($NameCategory->result() as $item)
        {
            echo "<option value=\"".$item->category_id."\">".$item->category_thai."</option>";
        }
    }

    public function saveNewVideoContent()
    {
        $this->load->library('form_validation');
        $config['upload_path'] = './assets/img/uploadfile/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = 2048; 
        $config['encrypt_name'] = true; 
        $this->load->library('upload', $config);

        $this->form_validation->set_rules('videoContent_textThai', 'required|videoContent_textThai|videoContent_textThai|กรุณากรอกชื่อ', 'trim|required');
        $this->form_validation->set_rules('link_youtube', 'required|link_youtube|link_youtube|กรุณากรอกลิ้งค์ youtube', 'trim|required');

        $this->form_validation->set_rules('country', 'required|country|country|กรุณาเลือกที่ตั้งประเทศ', 'trim|required');
        $this->form_validation->set_rules('provinces', 'required|provinces|provinces|กรุณาเลือกที่ตั้งจังหวัด', 'trim|required');
        $this->form_validation->set_rules('districts', 'required|districts|districts|กรุณาเลือกที่ตั้งอำเภอ', 'trim|required');

        $this->form_validation->set_rules('namecategory', 'required|namecategory|namecategory|หมวดหมู่สถานที่', 'trim|required');

        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {   
            $VideoContentData = array(
                "videoContent_textThai" =>$this->input->post("videoContent_textThai"),
                "videoContent_textEnglish" =>$this->input->post("videoContent_textEnglish"),
                "videoContent_textLaos" =>$this->input->post("videoContent_textLaos"),
                "videoContent_textChinese" =>$this->input->post("videoContent_textChinese"),
                "Category_category_id" =>$this->input->post("namecategory"),
                "Districts_districts_id" =>$this->input->post("districts"),
                "videoContent_url" =>$this->input->post("link_youtube"),
                "videoContent_createdDTTM" =>date("Y-m-d H:i:s"),
                "User_user_id" => $this->session->userdata('user_id'),
                "videoContent_isActive" => 1,
                "videoContent_isPublish" => 1,
               
            );
            $res = $this->video_content_models->AddVideoContentData($VideoContentData);
            // $res = array('state' => true,'error'=>'', 'msg' => 'เพิ่มสำเร็จ');
        }

        return $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }

    public function saveEditVideoContent()
    {
        $this->load->library('form_validation');
        $config['upload_path'] = './assets/img/uploadfile/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = 2048; 
        $config['encrypt_name'] = true; 
        $this->load->library('upload', $config);

        $this->form_validation->set_rules('videoContent_textThai', 'required|videoContent_textThai|videoContent_textThai|กรุณากรอกชื่อ', 'trim|required');
        $this->form_validation->set_rules('link_youtube', 'required|link_youtube|link_youtube|กรุณากรอกลิ้งค์ youtube', 'trim|required');

        $this->form_validation->set_rules('country', 'required|country|country|กรุณาเลือกที่ตั้งประเทศ', 'trim|required');
        $this->form_validation->set_rules('provinces', 'required|provinces|provinces|กรุณาเลือกที่ตั้งจังหวัด', 'trim|required');
        $this->form_validation->set_rules('districts', 'required|districts|districts|กรุณาเลือกที่ตั้งอำเภอ', 'trim|required');

        $this->form_validation->set_rules('namecategory', 'required|namecategory|namecategory|หมวดหมู่สถานที่', 'trim|required');

        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {   
            $VideoContentData = array(
                "videoContent_textThai" =>$this->input->post("videoContent_textThai"),
                "videoContent_textEnglish" =>$this->input->post("videoContent_textEnglish"),
                "videoContent_textLaos" =>$this->input->post("videoContent_textLaos"),
                "videoContent_textChinese" =>$this->input->post("videoContent_textChinese"),
                "Category_category_id" =>$this->input->post("namecategory"),
                "Districts_districts_id" =>$this->input->post("districts"),
                "videoContent_url" =>$this->input->post("link_youtube"),
                "videoContent_updatedDTTM" =>date("Y-m-d H:i:s"),
                "User_user_id" => $this->session->userdata('user_id'),

            );
            $res = $this->video_content_models->UpdateVideoContentData($VideoContentData,"videoContent_id = ".$this->input->post("videoContent_id"));
            // $res = array('state' => true,'error'=>'', 'msg' => 'เพิ่มสำเร็จ');
        }

        return $this->output->set_content_type('application/json')->set_output(json_encode($res));

    }

   
    public function getDataVideoContentForTable()
    {
        // Datatables Variables
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));
        
        $data = $this->video_content_models->setDatatableVideoContent("vc.videoContent_isActive = 1 AND vc.videoContent_isPublish = 1");;

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($data),
            "recordsFiltered" =>count($data),
            "data" => $data
        );
        echo json_encode($output);
    }
    public function gettest()
    {
        return $this->output->set_content_type('application/json')->set_output(json_encode( $this->video_content_models->GetVideoContentData("vc.videoContent_isActive = 1 AND vc.videoContent_isPublish = 1")));
        
        
    }
}