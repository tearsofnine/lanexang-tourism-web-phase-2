<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
class Sticker extends Base_Admin_Controller {
    public function __construct()
    {    	
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');

        $this->load->model("CountriesModels","countries_models", true);
        $this->load->model("UserModel","user_model", true);
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("SearchModels","search_models", true);
        $this->load->model("ChatModels","chat_models", true);
        $this->load->model("StickerModels","sticker_models", true);

        if(strcmp($this->session->userdata('lang'),"EN") == 0)
            $this->lang->load("english","english");
        else
            $this->lang->load("thailand","thailand");
    }
    
    public function index()
	{
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $content['Approval_Item'] = $this->items_models->CountApprovalItem();
        $content['UnReadMessages'] = $this->chat_models->getUnReadMessages($this->session->userdata('user_id'));
        
        $this->load->view('Html/MediaAndAdvertising/StickerView',$content);
    }

    public function addSticker()
    {
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $content['Approval_Item'] = $this->items_models->CountApprovalItem();
        $content['UnReadMessages'] = $this->chat_models->getUnReadMessages($this->session->userdata('user_id'));

        $this->load->view('Html/MediaAndAdvertising/StickerAddView',$content);
    }
    
    public function editSticker()
    {
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $content['Approval_Item'] = $this->items_models->CountApprovalItem();
        $content['UnReadMessages'] = $this->chat_models->getUnReadMessages($this->session->userdata('user_id'));

        $content['data_item'] = $this->sticker_models->GetStickerData("sk.sticker_isActive = 1 AND sk.sticker_id = ".$this->input->get("sticker_id"));
        
        if(count($content['data_item']) != 0)
            $this->load->view('Html/MediaAndAdvertising/StickerEditView',$content);
        else
            redirect('../Sticker');
    }

    public function saveNewSticker()
    {
        
        $this->load->library('form_validation');

        $this->form_validation->set_rules('sticker_Thai', 'required|sticker_Thai|sticker_Thai|กรุณากรอกชื่อสติ๊กเกอร์', 'trim|required');
        $this->form_validation->set_rules('sticker_type', 'required|sticker_type|sticker_type|กรุณากรอกหมวดหมู่สติ๊กเกอร์', 'trim|required');

        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>"validation", 'msg' => validation_errors(),"sticker_id"=> -1);
        }
        else
        {
            $Sticker = array(
                "sticker_Thai"=>$this->input->post("sticker_Thai"),
                "sticker_English"=>$this->input->post("sticker_English"),
                "sticker_Chinese"=>$this->input->post("sticker_Chinese"),
                "sticker_Laos"=>$this->input->post("sticker_Laos"),
                "sticker_isActive"=>1,
                "sticker_isPublish"=>1,
                "StickerType_stickerType_id"=>$this->input->post("sticker_type"),
            );
            $inserted_id_sticker = $this->sticker_models->AddStickerData("Sticker",$Sticker);
            

            $res = array('state' => true,'error'=>"", 'msg' => "","sticker_id"=>$inserted_id_sticker);
            
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }
    
    public function saveEditSticker()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('sticker_Thai', 'required|sticker_Thai|sticker_Thai|กรุณากรอกชื่อสติ๊กเกอร์', 'trim|required');
        $this->form_validation->set_rules('sticker_type', 'required|sticker_type|sticker_type|กรุณากรอกหมวดหมู่สติ๊กเกอร์', 'trim|required');
        

        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>"validation", 'msg' => validation_errors(),"sticker_id"=> -1);
        }
        else
        {
            $Sticker = array(
                "sticker_Thai"=>$this->input->post("sticker_Thai"),
                "sticker_English"=>$this->input->post("sticker_English"),
                "sticker_Chinese"=>$this->input->post("sticker_Chinese"),
                "sticker_Laos"=>$this->input->post("sticker_Laos"),
                "StickerType_stickerType_id"=>$this->input->post("sticker_type"),
            );
            $this->sticker_models->UpdateStickerData("Sticker",$Sticker,"sticker_id = ".$this->input->post("sticker_id"));
            

            $res = array('state' => true,'error'=>"", 'msg' => "","sticker_id"=>$this->input->post("sticker_id"));
            
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }
    public function savePhotoSticker()
    {
        $config['upload_path'] = './assets/img/uploadfile_Sticker/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        // $config['max_size'] = '5000';
        $config['encrypt_name'] = true; 
        $this->load->library('upload',$config); 

        $preview = $config_img = $errors = array();
        $input = 'fileSticker';

        if (empty($_FILES[$input])) 
            $res = array();

        $total = count($_FILES[$input]['name']); // multiple files
        for($i=0; $i < $total; $i++)
        {
            $_FILES['file']['name'] = $_FILES[$input]['name'][$i];
            $_FILES['file']['type'] = $_FILES[$input]['type'][$i];
            $_FILES['file']['tmp_name'] = $_FILES[$input]['tmp_name'][$i];
            $_FILES['file']['error'] = $_FILES[$input]['error'][$i];
            $_FILES['file']['size'] = $_FILES[$input]['size'][$i];

            //$fileName = $_FILES[$input]['name'][$i]; // the file name
            $fileSize = $_FILES[$input]['size'][$i]; // the file size

            if($this->upload->do_upload('file'))
            {
                $uploadData = $this->upload->data();
                $fileName = $uploadData['file_name'];

                $newFileUrl =  str_replace("./","/dasta_thailand/",$config['upload_path']);
                $newFileUrl .= "".$fileName;;

                $preview[] = $newFileUrl;

                $PathsSticker = array(
                    "pathsSticker_paths" =>$fileName,
                    "Sticker_sticker_id" =>$this->input->post("sticker_id"),
                );

                $inserted_pathsSticker_id = $this->sticker_models->AddStickerData("PathsSticker",$PathsSticker);

                $config_img[] = array(
                    'key' => $inserted_pathsSticker_id,
                    'caption' => $fileName,
                    'size' => $fileSize,
                    'downloadUrl' => $newFileUrl, // the url to download the file
                    'url' => '/dasta_thailand/html/MediaAndAdvertising/Sticker/delete_sticker_based_on_key', // server api to delete the file based on key
                );
            }
            else 
            {
                $errors[] = $_FILES[$input]['name'][$i];;
            }
        }

        $res = array(
            'initialPreview' => $preview,
            'initialPreviewConfig' => $config_img,
            'initialPreviewAsData' => true
        );
        
        if (!empty($errors)) 
        {
            $img = count($errors) === 1 ? 'file "' . $error[0]  . '" ' : 'files: "' . implode('", "', $errors) . '" ';
            $res['error'] = 'Oh snap! We could not upload the ' . $img . 'now. Please try again later.';
        }

        return $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }
    public function delete_sticker_based_on_key()
    {

        $pathsSticker = $this->sticker_models->GetPathsSticker("pathsSticker_id = ". $this->input->post("key"));
        $sticker_paths = './assets/img/uploadfile_Sticker/'. $pathsSticker[0]->pathsSticker_paths;
        is_readable($sticker_paths);
        unlink($sticker_paths);

        $this->sticker_models->DeleteStickerData("PathsSticker","pathsSticker_id = ". $this->input->post("key"));
        $res = array(
            'initialPreviewAsData' => true
        );
        //$res['error'] = $this->input->post("key");
        return $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }

    public function getNameCategory()
    {
        $NameCategory = $this->sticker_models->getNameCategory();
        echo "<option value=\"\">หมวดหมู่สติ๊กเกอร์</option>";
       
        foreach ($NameCategory as $item)
        {
            echo "<option value=\"".$item->stickerType_id."\">".$item->stickerType_Thai."</option>";
        }
    }
    public function getDataStickerForTable()
    {
        // Datatables Variables
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));
        
        $data = $this->sticker_models->setDatatableSticker("sk.sticker_isActive = 1");

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($data),
            "recordsFiltered" =>count($data),
            "data" => $data
        );
        echo json_encode($output);
    }
    public function gettest()
    {
        return $this->output->set_content_type('application/json')->set_output(json_encode( $this->sticker_models->GetStickerData("sk.sticker_isActive = 1")));
    }
}