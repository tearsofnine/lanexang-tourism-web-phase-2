<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
class ProvinceGroup extends Base_Admin_Controller {
    public function __construct()
    {    	
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');

        $this->load->model("CountriesModels","countries_models", true);
        $this->load->model("UserModel","user_model", true);
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("ProvinceGroupModels","models_ProvinceGroup", true);
        $this->load->model("ChatModels","chat_models", true);

        if(strcmp($this->session->userdata('lang'),"EN") == 0)
            $this->lang->load("english","english");
        else
            $this->lang->load("thailand","thailand");
    }
    public function index()
	{
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $content['Approval_Item'] = $this->items_models->CountApprovalItem();
        $content['UnReadMessages'] = $this->chat_models->getUnReadMessages($this->session->userdata('user_id'));
        
        $this->load->view('Html/ProvinceGroupView',$content);
    }
    public function getDataProvinceGroup()
    {
         // Datatables Variables
         $draw = intval($this->input->get("draw"));
         $start = intval($this->input->get("start"));
         $length = intval($this->input->get("length"));
 
         $data = $this->models_ProvinceGroup->setDatatable();
        
         $output = array(
             "draw" => $draw,
             "recordsTotal" => count($data),
             "recordsFiltered" =>count($data),
             "data" => $data
         );
        echo json_encode($output);
    }
    public function addProvinceGroup()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('pg_thai', 'required|pg_thai|pg_thai|กรุณากรอกชื่อกลุ่มจังหวัด', 'trim|required');
        $this->form_validation->set_rules('ProvinceSelection', 'required|ProvinceSelection|ProvinceSelection|กรุณาเลือกกลุ่มจังหวัด', 'trim|required');
        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        { 
            $provincegroup = array(
                "pg_thai" =>$this->input->post("pg_thai"),
                "pg_english" =>$this->input->post("pg_english"),
                "pg_chinese" =>$this->input->post("pg_chinese"),
                "pg_laos" =>$this->input->post("pg_laos"),
                "pg_isActive" =>1,
                "pg_createdDTTM" =>date("Y-m-d H:i:s"),
            );
            $ProvinceGroup_ID = $this->models_ProvinceGroup->AddProvinceGroupData($provincegroup,"ProvinceGroup");
            $Province_ID = explode(",",$this->input->post("ProvinceSelection"));
            foreach ($Province_ID as $value) {
                $data = array(
                    'Provinces_provinces_id'=>$value,
                    'ProvinceGroup_pg_id '=>$ProvinceGroup_ID,
                );
                $this->models_ProvinceGroup->AddProvinceGroupData($data,"PermissionProvinceGroup");
              }
            $res = array('state' => true,'error'=>'', 'msg' => 'เพิ่มสำเร็จ');
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }
}