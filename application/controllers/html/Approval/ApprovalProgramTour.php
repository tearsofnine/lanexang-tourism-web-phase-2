<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
class ApprovalProgramTour extends Base_Admin_Controller {
    public function __construct()
    {    	
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->helper('file');

        $this->load->model("CountriesModels","countries_models", true);
        $this->load->model("UserModel","user_model", true);
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("SearchModels","search_models", true);
        $this->load->model("ProgramTourModels","program_tour_models", true);
        $this->load->model("ChatModels","chat_models", true);

        if(strcmp($this->session->userdata('lang'),"EN") == 0)
            $this->lang->load("english","english");
        else
            $this->lang->load("thailand","thailand");
    }
    
    public function index()
	{
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $content['Approval_Item'] = $this->items_models->CountApprovalItem();
        $content['Approval_ItemState'] = $this->program_tour_models->CountApprovalProgramTourState("pt.MenuItem_menuItem_id = 1 AND pt.programTour_isActive != 99 AND pt.programTour_isSaveDraft = 0 AND u.UserRole_userRole_id in (4,5)");
        $content['ApprovalUserState'] = $this->user_model->CountApprovalUserState("b.business_type_category_id = 1");
        $content['UnReadMessages'] = $this->chat_models->getUnReadMessages($this->session->userdata('user_id'));
        
        $this->load->view('Html/Approval/ApprovalProgramTourView',$content);
    }
    public function getDataForTable()
    {
        // Datatables Variables
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));
        
        $ProgramTourData = $this->program_tour_models->GetDefaultProgramTourData("pt.programTour_isActive = 1 AND pt.PublishStatus_publishStatus_id = ".$this->input->post("find_approval")." AND pt.programTour_isSaveDraft = 0 AND pt.MenuItem_menuItem_id = 1 AND u.UserRole_userRole_id in (4,5) ");
        $data =array();
        for($i=0;$i<count($ProgramTourData);$i++)
        {
            if($ProgramTourData[$i]->PublishStatus_publishStatus_id == 0)
            {
                $label[0] = "warning";
                $label[1] = "รอการอนุมัติ";
            }   
            else if($ProgramTourData[$i]->PublishStatus_publishStatus_id == 1)
            {
                $label[0] = "primary";
                $label[1] = "อนุมัติแล้ว";
            }
            else
            {
                $label[0] = "danger";
                $label[1] = "ไม่ผ่านการอนุมัติ";
            }
            $userData = $this->user_model->GetUserJoinBusinessData("u.user_id = ".$ProgramTourData[$i]->User_user_id );
            $ProgramTourAllData = $this->program_tour_models->GetProgramTourData("pt.programTour_id = ".$ProgramTourData[$i]->programTour_id);
            $data[] = array(
                $ProgramTourData[$i]->programTour_id,
                "<span class=\"label label-".$label[0]."\">".$label[1]."</span>",
                "<p class=\"\">".$ProgramTourData[$i]->programTour_nameThai."</p>",
                "<p class=\"\">".$this->program_tour_models->GetScope($ProgramTourAllData[0]->DatesTrip)."</p>",
                "<p style=\"color: #1AB394;\">".$userData[0]->user_firstName."</p>",
                "<p>".date("d/m/Y",strtotime($ProgramTourData[$i]->programTour_createdDTTM))."</p>",
                "<button class=\"btn btn-info btn-program-tour-view\" value=\"".$ProgramTourData[$i]->programTour_id."\">".$this->lang->line("view")."</button>
                <a href=\"../ProgramTour/editProgramTour?programTour_id=".$ProgramTourData[$i]->programTour_id."\" class=\"btn btn-warning\">".$this->lang->line("edit")."</a>
                <button class=\"btn btn-danger btn-program-tour-del\" value=\"".$ProgramTourData[$i]->programTour_id."\">ลบ</button>
                ",
            );
        }
       
        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($data),
            "recordsFiltered" =>count($data),
            "data" => $data
        );
        echo json_encode($output);
    }
}