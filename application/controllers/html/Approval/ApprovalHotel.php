<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
class ApprovalHotel extends Base_Admin_Controller {
    public function __construct()
    {    	
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->helper('file');

        $this->load->model("CountriesModels","countries_models", true);
        $this->load->model("UserModel","user_model", true);
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("SearchModels","search_models", true);
        $this->load->model("ChatModels","chat_models", true);

        if(strcmp($this->session->userdata('lang'),"EN") == 0)
            $this->lang->load("english","english");
        else
            $this->lang->load("thailand","thailand");
    }
    
    public function index()
	{
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $content['Approval_Item'] = $this->items_models->CountApprovalItem();
        $content['Approval_ItemState'] = $this->items_models->CountApprovalItemState("i.MenuItem_menuItem_id = 5 AND i.items_isActive != 99 AND u.UserRole_userRole_id in (4,5)");
        $content['ApprovalUserState'] = $this->user_model->CountApprovalUserState("b.business_type_category_id = 5");
        $content['UnReadMessages'] = $this->chat_models->getUnReadMessages($this->session->userdata('user_id'));
        
        $this->load->view('Html/Approval/ApprovalHotelView',$content);
    }
    public function getDataForTable()
    {
        // Datatables Variables
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));
        
        $itemData = $this->items_models->GetDefaultItemsData("i.items_isActive = ".$this->input->post("find_approval")." AND i.MenuItem_menuItem_id = 5 AND u.UserRole_userRole_id in (4,5)");
        $data =array();
        for($i=0;$i<count($itemData);$i++)
        {
            if($itemData[$i]->items_isActive == 0)
            {
                $label[0] = "warning";
                $label[1] = "รอการอนุมัติ";
            }   
            else if($itemData[$i]->items_isActive == 1)
            {
                $label[0] = "primary";
                $label[1] = "อนุมัติแล้ว";
            }
            else
            {
                $label[0] = "danger";
                $label[1] = "ไม่ผ่านการอนุมัติ";
            }
            $userData = $this->user_model->GetUserJoinBusinessData("u.user_id = ".$itemData[$i]->User_user_id );
            $data[] = array(
                $itemData[$i]->items_id,
                "<span class=\"label label-".$label[0]."\">".$label[1]."</span>",
                "<p class=\"\">".$itemData[$i]->itmes_topicThai."</p>",
                "<p style=\"color: #1AB394;\">".$userData[0]->business_nameThai."</p>",
                "<p>".$itemData[$i]->items_phone."</p>",
                "<p>".date("d/m/Y",strtotime($itemData[$i]->items_createdDTTM))."</p>",
                "<button class=\"btn btn-info btn-item-view\" value=\"".$itemData[$i]->items_id."\">".$this->lang->line("view")."</button>
                <a href=\"../Items/Hotel/editHotel?item_id=".$itemData[$i]->items_id."\" class=\"btn btn-warning\">".$this->lang->line("edit")."</a>
                <button class=\"btn btn-danger btn-item-del\" value=\"".$itemData[$i]->items_id."\">ลบ</button>
                ",
            );
        }
       
        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($data),
            "recordsFiltered" =>count($data),
            "data" => $data
        );
        echo json_encode($output);
    }
}