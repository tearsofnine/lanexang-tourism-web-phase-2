<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
class UserEvent extends Base_Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model("CountriesModels", "countries_models", true);
        $this->load->model("UserModel", "user_model", true);
        $this->load->model("ItemsModels", "items_models", true);
        $this->load->model("SearchModels", "search_models", true);
        $this->load->model("ProgramTourModels", "program_tour_models", true);
        $this->load->model("ChatModels", "chat_models", true);
        $this->load->model("GlobalFunctionModels", "global_function_models", true);
    }
    public function DisabledUser()
    {
        $User = array(
            "user_isActive" => $this->input->post("state"),
            "user_updatedDTTM" => date("Y-m-d H:i:s"),
        );
        $trans_status = $this->user_model->UpdateUserData($User, "user_id = " . $this->input->post("user_id"));
    }
    public function DeleteUser()
    {
        $User = array(
            "user_isDelete" => 1,
            "user_isActive" => 2,
            "user_updatedDTTM" => date("Y-m-d H:i:s"),
        );
        $trans_status = $this->user_model->UpdateUserData($User, "user_id = " . $this->input->post("user_id"));
    }
    public function DisapprovalUser()
    {
        $User = array(
            "user_isActive" => 2,
            "user_updatedDTTM" => date("Y-m-d H:i:s"),
        );
        $trans_status = $this->user_model->UpdateUserData($User, "user_id = " . $this->input->post("user_id"));
    }
    public function ApprovUser()
    {
        $User = array(
            "user_isActive" => 1,
            "user_updatedDTTM" => date("Y-m-d H:i:s"),
        );
        $trans_status = $this->user_model->UpdateUserData($User, "user_id = " . $this->input->post("user_id"));
    }
    public function GetUserData()
    {
        $res = $this->user_model->GetUserJoinBusinessData("user_id = " . $this->input->post("user_id"));
        return $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }
    public function updateUnReadMessages()
    {
        return $this->output->set_content_type('application/json')->set_output(json_encode($this->chat_models->getUnReadMessages($this->session->userdata('user_id'))));
    }
    public function updateNotificationsMessages()
    {
        $chatroom_data = $this->chat_models->get_chat_room_for_table("(UserSend_user_id = " . $this->session->userdata('user_id') . " OR UserReceive_user_id = " . $this->session->userdata('user_id') . ") AND messages_read = 0", $this->session->userdata('user_id'));

        $new_chatroom_data = array();
        $j = 0;
        for ($i = 0; $i < count($chatroom_data); $i++) {
            if (intval($chatroom_data[$i]->count_read) != 0) {
                $chatroom_data[$i]->message_created = $this->global_function_models->time_elapsed_string($chatroom_data[$i]->message_created_at);
                $chatroom_data[$i]->message_created_at = $chatroom_data[$i]->message_created . " at " . date("h:i A - d.m.Y", strtotime($chatroom_data[$i]->message_created_at));

                $arrParsedUrl = parse_url($chatroom_data[$i]->UserSend[0]->user_profile_pic_url);
                if (!empty($arrParsedUrl['scheme'])) {
                    $chatroom_data[$i]->user_profile_pic_url = $chatroom_data[$i]->UserSend[0]->user_profile_pic_url;
                } else {
                    if ($chatroom_data[$i]->UserSend[0]->user_profile_pic_url != "" || $chatroom_data[$i]->UserSend[0]->user_profile_pic_url != NULL)
                        $chatroom_data[$i]->user_profile_pic_url = "/dasta_thailand/assets/img/uploadfile/" . $chatroom_data[$i]->UserSend[0]->user_profile_pic_url;
                    else
                        $chatroom_data[$i]->user_profile_pic_url = "/dasta_thailand/assets/img/account_avatar.jpg";
                }
                $new_chatroom_data[$j] = $chatroom_data[$i];
                $j++;
            }
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($new_chatroom_data));
    }
    public function oldpass_check($str)
    {
        $this->load->library('form_validation');
        $this->db->where('user_id', $this->session->userdata('user_id'));
        $query = $this->db->get('User');
        $row = $query->row();

        $password = openssl_encrypt($str, "AES-128-ECB", SECRETKEY);
        if ($row->user_password != $password) {
            $this->form_validation->set_message('oldpass_check', 'The %s ให้ถูกต้อง|');
            return FALSE;
        } else {
            return TRUE;
        }
    }
    public function ChangePassword()
    {
        $this->load->library('form_validation');
        //$this->db->where('user_id', $this->session->userdata('entrepreneur_user_id'));
        //$query = $this->db->get('User');

        $this->form_validation->set_rules('oldpass', 'required|oldpass|oldpass|กรุณากรอก รหัสผ่านเก่า', 'trim|required|callback_oldpass_check');
        $this->form_validation->set_rules('newpass', 'required|newpass|newpass|กรุณากรอก รหัสใหม่', 'trim|required|matches[conpass]');
        $this->form_validation->set_rules('conpass', 'required|conpass|conpass|กรุณากรอก รหัสใหม่อีกครั้ง', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $res = array('state' => false, 'error' => 'validation', 'msg' => validation_errors());
        } else {
            $password = openssl_encrypt($this->input->post("newpass"), "AES-128-ECB", SECRETKEY);
            $user_data = array(
                "user_password" => $password,
                "user_updatedDTTM" => date("Y-m-d H:i:s"),
            );
            $res = $this->user_model->UpdateUserData($user_data, "user_id = " . $this->session->userdata("user_id"));
            $res = array('state' => true, 'error' => '', 'msg' => 'เพิ่มสำเร็จ');
        }

        return $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }
    public function updateUserInfo()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('info_user_firstName', 'required|info_user_firstName|info_user_firstName|กรุณากรอกชื่อ', 'trim|required');
        $this->form_validation->set_rules('info_user_lastName', 'required|info_user_lastName|info_user_lastName|กรุณากรอกนามสกุล', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $res = array('state' => false, 'error' => 'validation', 'msg' => validation_errors());
        } else {
            $userdate = array(
                "user_firstName" => $this->input->post("info_user_firstName"),
                "user_lastName" => $this->input->post("info_user_lastName"),
                "user_age" => $this->input->post("info_user_age"),
                "user_gender" => $this->input->post("info_user_gender"),
                "user_phone" => $this->input->post("info_user_phone"),
                "user_updatedDTTM" => date("Y-m-d H:i:s"),

            );
            $this->session->set_userdata("user_firstName",$this->input->post("info_user_firstName"));
            $this->session->set_userdata("user_lastName", $this->input->post("info_user_lastName"));
            $this->session->set_userdata("user_age", $this->input->post("info_user_age"));
            $this->session->set_userdata("user_gender",$this->input->post("info_user_gender"));
            $this->session->set_userdata("user_phone", $this->input->post("info_user_phone"));
            $res = $this->user_model->UpdateUserData($userdate, "user_id = " . $this->session->userdata("user_id"));
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }
}
