<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
class MediaAndAdvertisingEvent extends Base_Admin_Controller {
    public function __construct()
    {    	
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model("CountriesModels","countries_models", true);
        $this->load->model("UserModel","user_model", true);
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("SearchModels","search_models", true);
        $this->load->model("ProgramTourModels","program_tour_models", true);
        $this->load->model("ChatModels","chat_models", true);
        $this->load->model("MediaAndAdvertisingModels","media_and_advertising_models", true);
        $this->load->model("VideoContentModels","video_content_models", true);
    }
    
    public function DeleteMediaAndAdvertising()
    {
        $mediaAndAdvertising = array(
            "mediaAndAdvertising_isActive" =>99,
            "mediaAndAdvertising_isPublish" =>0,
            "mediaAndAdvertising_updatedDTTM" => date("Y-m-d H:i:s"),
        );
        $trans_status = $this->media_and_advertising_models->UpdateMediaAndAdvertisingData($mediaAndAdvertising,"MediaAndAdvertising","mediaAndAdvertising_id = ".$this->input->post("mediaAndAdvertising_id"));
    }
    public function DeleteVideoContent()
    {
        $VideoContent = array(
            "videoContent_isActive" =>99,
            "videoContent_isPublish" =>0,
            "videoContent_updatedDTTM" => date("Y-m-d H:i:s"),
        );
        $trans_status = $this->video_content_models->UpdateVideoContentData($VideoContent,"videoContent_id = ".$this->input->post("videoContent_id"));
    }
}