<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
class LocationManagement extends CI_Controller {
    public function __construct()
    {    	
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model("CountriesModels","countries_models", true);
        $this->load->model("UserModel","user_model", true);
        $this->load->model("ChatModels","chat_models", true);
    }
    public function getCountry()
    {
        $UserRole_userRole_id = 0;
        if (!is_null($this->session->userdata('UserRole_userRole_id')))
            $UserRole_userRole_id = $this->session->userdata('UserRole_userRole_id');
        switch ($UserRole_userRole_id) {
            case 1:
                $countries=$this->countries_models->getCountry(true,"");
                break;
            case 2:
                $userinfor=$this->user_model->getinformationUserAdminManager($this->session->userdata('user_id'),"distinct(c.country_id)","c.country_id");
                foreach($userinfor->result() as $r) {
                    $where_country[] =  $r->country_id;
                }
                $countries=$this->countries_models->getCountry(false,$where_country);
                break;
            case 3:
                $userinfor=$this->user_model->getinformationUserAdmin($this->session->userdata('user_id'));
                $userinfor_data = $userinfor->row_array();
                $countries=$this->countries_models->getCountry(false,$userinfor_data["country_id"]);
                break;
            default:
                $countries=$this->countries_models->getCountry(true,"");
        }
        echo "<option value=\"\">ประเทศ</option>";
        foreach ($countries as $item)
        {
            echo "<option value=\"".$item->country_id."\">".$item->country_thai."</option>";
        }
    }
    public function getProvinces()
    {
        $UserRole_userRole_id = 0;
        if (!is_null($this->session->userdata('UserRole_userRole_id')))
            $UserRole_userRole_id = $this->session->userdata('UserRole_userRole_id');
        switch ($UserRole_userRole_id) {
            case 1:
                $provinces=$this->countries_models->getProvinces(true,array('Country_country_id' => $this->input->post('getOption')),"");
                break;
            case 2:
                $userinfor=$this->user_model->getinformationUserAdminManager($this->session->userdata('user_id'),"p.*","p.provinces_id");
                foreach($userinfor->result() as $r) {
                    $where_provinces[] =  $r->provinces_id;
                }
                $provinces=$this->countries_models->getProvinces(false,$this->input->post('getOption'),$where_provinces);
                break;
            case 3:
                $userinfor=$this->user_model->getinformationUserAdmin($this->session->userdata('user_id'));
                $userinfor_data = $userinfor->row_array();
                $provinces=$this->countries_models->getProvinces(false,$userinfor_data["country_id"],$userinfor_data["provinces_id"]);
                break;
            default:
                $provinces=$this->countries_models->getProvinces(true,array('Country_country_id' => $this->input->post('getOption')),"");
        }
        echo "<option value=\"\">จังหวัด</option>";
        foreach ($provinces as $item)
        {
            echo "<option value=\"".$item->provinces_id."\">".$item->provinces_thai."</option>";
        }
    }
    public function getDistricts()
    {
        $districts=$this->countries_models->getDistricts(array('Provinces_provinces_id' => $this->input->post('getOption')),"");
        echo "<option value=\"\">อำเภอ</option>";
        foreach ($districts as $item)
        {
            echo "<option value=\"".$item->districts_id."\">".$item->districts_thai."</option>";
        }
    }
    public function getSubdistricts()
    {
        $districts=$this->countries_models->getSubdistricts(array('Districts_districts_id' => $this->input->post('getOption')),"");
        echo "<option value=\"\">ตำบล</option>";
        foreach ($districts as $item)
        {
            echo "<option value=\"".$item->subdistricts_id."\">".$item->subdistricts_thai."</option>";
        }
    }
    public function getNameLocation()
    {
        $where = $this->input->post('types')." = ".$this->input->post('id');
        $item = $this->countries_models->getNameLocation($this->input->post('querys'),$where);
        return $this->output->set_content_type('application/json')->set_output(json_encode($item->row_array()));
    }
    public function getDataCountries()
    {
        $DataCountries=$this->countries_models->getDataCountries($this->input->post('table'),$this->input->post('where'));
        foreach ($DataCountries as $item)
        {
            echo "<option value=\"".$item->{strtolower($this->input->post('table')).'_id'}."\">".$item->{strtolower($this->input->post('table')).'_thai'}."</option>";
        }
    }
}