<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
class ItemsEvent extends Base_Admin_Controller {
    public function __construct()
    {    	
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model("CountriesModels","countries_models", true);
        $this->load->model("UserModel","user_model", true);
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("SearchModels","search_models", true);
        $this->load->model("ProgramTourModels","program_tour_models", true);
        $this->load->model("ChatModels","chat_models", true);
    }
    public function DeleteItem()
    {
        $Items = array(
            "items_isActive" =>99,
            "items_isPublish" =>0,
            "items_updatedDTTM" => date("Y-m-d H:i:s"),
        );
        $trans_status = $this->items_models->UpdateItemData($Items,"Items","items_id = ".$this->input->post("items_id"));
    }
    public function PublicItem()
    {
        $Items = array(
            "items_isPublish" =>$this->input->post("public_status"),
            "items_updatedDTTM" => date("Y-m-d H:i:s"),
        );
        $trans_status = $this->items_models->UpdateItemData($Items,"Items","items_id = ".$this->input->post("items_id"));
    }
    public function ApproveProgramTour()
    {
        $ProgramTour = array(
            "PublishStatus_publishStatus_id" =>1,
            "programTour_isPublish" =>1,
            "programTour_updatedDTTM" => date("Y-m-d H:i:s"),
        );
        $trans_status = $this->items_models->UpdateItemData($ProgramTour,"ProgramTour","programTour_id = ".$this->input->post("programTour_id"));
    }
    public function DisapprovalProgramTour()
    {
        $ProgramTour = array(
            "PublishStatus_publishStatus_id" =>2,
            "programTour_updatedDTTM" => date("Y-m-d H:i:s"),
        );
        $trans_status = $this->items_models->UpdateItemData($ProgramTour,"ProgramTour","programTour_id = ".$this->input->post("programTour_id"));
    }
    public function DeleteProgramTour()
    {
        $ProgramTour = array(
            "programTour_isActive" =>99,
            "programTour_isPublish" =>0,
            "programTour_updatedDTTM" => date("Y-m-d H:i:s"),
        );
        $trans_status = $this->items_models->UpdateItemData($ProgramTour,"ProgramTour","programTour_id = ".$this->input->post("programTour_id"));
    }
    public function ApproveItem()
    {
        $Items = array(
            "items_isActive" =>1,
            "items_isPublish" =>1,
            "items_updatedDTTM" => date("Y-m-d H:i:s"),
        );
        $trans_status = $this->items_models->UpdateItemData($Items,"Items","items_id = ".$this->input->post("items_id"));
    }
    public function DisapprovalItem()
    {
        $Items = array(
            "items_isActive" =>2,
            "items_isPublish" =>0,
            "items_updatedDTTM" => date("Y-m-d H:i:s"),
        );
        $trans_status = $this->items_models->UpdateItemData($Items,"Items","items_id = ".$this->input->post("items_id"));
    }
    public function GetItemsData()
    {
        $res = $this->items_models->getApprovalItemData("i.items_id = ".$this->input->post("items_id"));
        return $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }
    public function GetApprovalData()
    {
        $data = new stdClass();
        $data->Items = $this->items_models->CountApprovalItem();

        $data->ItemState_ProgramTour = $this->program_tour_models->CountApprovalProgramTourState("pt.MenuItem_menuItem_id = 1 AND pt.programTour_isActive != 99 AND pt.programTour_isSaveDraft = 0 AND u.UserRole_userRole_id in (4,5)");
        // $data->UserState_Attractions = $this->user_model->CountApprovalUserState("b.business_type_category_id = 2");

        $data->ItemState_Attractions = $this->items_models->CountApprovalItemState("i.MenuItem_menuItem_id = 2 AND i.items_isActive != 99 AND u.UserRole_userRole_id in (4,5)");
        $data->UserState_Attractions = $this->user_model->CountApprovalUserState("b.business_type_category_id = 2");

        $data->ItemState_Restaurant = $this->items_models->CountApprovalItemState("i.MenuItem_menuItem_id = 3 AND i.items_isActive != 99 AND u.UserRole_userRole_id in (4,5)");
        $data->UserState_Restaurant = $this->user_model->CountApprovalUserState("b.business_type_category_id = 3");

        $data->ItemState_Shopping = $this->items_models->CountApprovalItemState("i.MenuItem_menuItem_id = 4 AND i.items_isActive != 99 AND u.UserRole_userRole_id in (4,5)");
        $data->UserState_Shopping = $this->user_model->CountApprovalUserState("b.business_type_category_id = 4");

        $data->ItemState_Hotel = $this->items_models->CountApprovalItemState("i.MenuItem_menuItem_id = 5 AND i.items_isActive != 99 AND u.UserRole_userRole_id in (4,5)");
        $data->UserState_Hotel = $this->user_model->CountApprovalUserState("b.business_type_category_id = 5");

        $data->ItemState_PackageTours = $this->items_models->CountApprovalItemState("i.MenuItem_menuItem_id = 9 AND i.items_isActive != 99 AND u.UserRole_userRole_id in (4,5)");
        $data->UserState_PackageTours = $this->user_model->CountApprovalUserState("b.business_type_category_id = 9");
        
        $data->ItemState_EventTicket = $this->items_models->CountApprovalItemState("i.MenuItem_menuItem_id = 10 AND i.items_isActive != 99 AND u.UserRole_userRole_id in (4,5)");
        $data->UserState_EventTicket = $this->user_model->CountApprovalUserState("b.business_type_category_id = 10");
        
        $data->ItemState_CarRent = $this->items_models->CountApprovalItemState("i.MenuItem_menuItem_id = 11 AND i.items_isActive != 99 AND u.UserRole_userRole_id in (4,5)");
        $data->UserState_CarRent = $this->user_model->CountApprovalUserState("b.business_type_category_id = 11");
        
        return $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

}