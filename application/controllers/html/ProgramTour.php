<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
class ProgramTour extends Base_Admin_Controller {
    public function __construct()
    {    	
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->helper('file');

        $this->load->model("CountriesModels","countries_models", true);
        $this->load->model("UserModel","user_model", true);
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("SearchModels","search_models", true);
        $this->load->model("ProgramTourModels","program_tour_models", true);
        $this->load->model("ChatModels","chat_models", true);

        if(strcmp($this->session->userdata('lang'),"EN") == 0)
            $this->lang->load("english","english");
        else
            $this->lang->load("thailand","thailand");
    }
    public function index()
	{
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $content['Approval_Item'] = $this->items_models->CountApprovalItem();
        $content['ModalView'] = "ProgramTour";
        $content['UnReadMessages'] = $this->chat_models->getUnReadMessages($this->session->userdata('user_id'));
        
        $this->load->view('Html/ProgramTourView',$content);
    }
    public function addProgramTour()
	{
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $content['Approval_Item'] = $this->items_models->CountApprovalItem();
        $content['UnReadMessages'] = $this->chat_models->getUnReadMessages($this->session->userdata('user_id'));
        
        $this->load->view('Html/ProgramTourAddView',$content);
    }
    public function editProgramTour()
	{
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $content['Approval_Item'] = $this->items_models->CountApprovalItem();
        $content['data_program_tour'] = $this->program_tour_models->GetProgramTourData("pt.programTour_id = ".$this->input->get("programTour_id")." AND pt.programTour_isActive = 1 AND pt.programTour_isPublish = 1 AND PublishStatus_publishStatus_id = 1 AND pt.programTour_isSaveDraft = 0");
        $content['UnReadMessages'] = $this->chat_models->getUnReadMessages($this->session->userdata('user_id'));
        
        if(count($content['data_program_tour']) != 0)
            $this->load->view('Html/ProgramTourEditView',$content);
        else
        {
            $content['data_program_tour'] = $this->program_tour_models->GetProgramTourData("pt.programTour_id = ".$this->input->get("programTour_id")." AND pt.programTour_isActive = 1 AND pt.programTour_isSaveDraft = 0");
            if(count($content['data_program_tour']) != 0)
            {
                $this->load->view('Html/ProgramTourEditView',$content);
            }
            else
                redirect('../ProgramTour');
        }

        // if(count($content['data_program_tour']) != 0)
        //     $this->load->view('Html/ProgramTourEditView',$content);
        // else
        //     redirect('../ProgramTour');
    }
    public function getNameCategory()
    {
        $NameCategory = $this->items_models->getNameCategory();
        echo "<option value=\"\">".$this->lang->line("place_category")."</option>";
       
        foreach ($NameCategory->result() as $item)
        {
            if(strcmp($this->session->userdata('lang'),"EN") == 0)
                echo "<option value=\"".$item->category_id."\">".$item->category_english."</option>";
            else
                echo "<option value=\"".$item->category_id."\">".$item->category_thai."</option>";
        }
    }
    public function getNumDatesTrip()
    {
        $NumDatesTrip = $this->program_tour_models->GetNumDatesTrip();
        echo "<option value=\"\">".$this->lang->line("duration_of_the_excursion")."</option>";
       
        foreach ($NumDatesTrip as $item)
        {
            if(strcmp($this->session->userdata('lang'),"EN") == 0)
                echo "<option value=\"".$item->NumDatesTrip."\">".$item->NumDatesTrip." Day</option>";
            else
                echo "<option value=\"".$item->NumDatesTrip."\">".$item->NumDatesTrip." วัน</option>";
           
            
        }
    }
    public function getNameCategorySubCategory()
    {
        $NameCategorySubCategory = $this->items_models->getNameCategorySubCategory($this->input->post("getOption"));
        if($this->input->post("getOption") == 1)
            echo "<option value=\"\">".$this->lang->line("travel_program_type")."</option>";
        else
            echo "<option value=\"\">".$this->lang->line("name_location_type")."</option>";
       
        foreach ($NameCategorySubCategory->result() as $item)
        {
            if(strcmp($this->session->userdata('lang'),"EN") == 0)
                echo "<option value=\"".$item->subcategory_id."\">".$item->subcategory_english."</option>";
            else
                echo "<option value=\"".$item->subcategory_id."\">".$item->subcategory_thai."</option>";
            
            
        }
    }
    public function getNamePlace()
    {
        $strWhere = "i.items_isActive = 1 AND sc.subcategory_id = %s AND i.Subdistricts_subdistricts_id IN(
            SELECT subdistricts_id
            FROM Subdistricts
            where Districts_districts_id IN (
                SELECT districts_id
                FROM Districts
                where districts_id = %s ))";
        $where = sprintf($strWhere,$this->input->post("subcategory"),$this->input->post("districts"));
        $NamePlace = $this->items_models->GetItemData($where);
        echo "<option value=\"\">สถานที่</option>";
        foreach ($NamePlace as $item)
        {
            echo "<option value=\"".$item->items_id."\">".$item->itmes_topicThai."</option>";
            
        }
    }
    public function getDataItem()
    {
        $dataItem = $this->items_models->GetItemData("i.items_isActive = 1 AND i.items_id = ".$this->input->post("item_id"));
        return $this->output->set_content_type('application/json')->set_output(json_encode($dataItem));
    }
    public function getProgramTourById()
    {
        $data_program_tour = $this->program_tour_models->GetProgramTourData("pt.programTour_id = ".$this->input->post("programTour_id")." AND pt.programTour_isActive = 1 AND pt.programTour_isSaveDraft = 0");
        return $this->output->set_content_type('application/json')->set_output(json_encode($data_program_tour));
    }
    public function getDataProgramTourForTable()
    {
        // Datatables Variables
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));
 
        $data = $this->search_models->getDataSearchForTableProgramTour(
            $this->input->post("subcategory"),
            $this->input->post("num_dates_trip"),
            "pt.programTour_isActive = 1 AND pt.programTour_isPublish = 1 AND PublishStatus_publishStatus_id = 1 AND pt.programTour_isSaveDraft = 0"
        );
        // $this->program_tour_models->setDatatable("pt.programTour_isActive = 1 AND pt.programTour_isPublish = 1 AND PublishStatus_publishStatus_id = 1 AND pt.programTour_isSaveDraft = 0");
        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($data),
            "recordsFiltered" =>count($data),
            "data" => $data
        );
        echo json_encode($output);
    }
    public function saveNewProgramTour()
    {
        $this->load->library('form_validation');
        $config['upload_path'] = './assets/img/uploadfile/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        //$config['max_size'] = 2048; 
        $config['encrypt_name'] = true; 
        $this->load->library('upload', $config);

        $this->form_validation->set_rules('programTour_nameThai', 'required|programTour_nameThai|programTour_nameThai|กรุณากรอกชื่อโปรแกรมการท่องเที่ยว', 'trim|required');
        $this->form_validation->set_rules('subcategory', 'required|subcategory|subcategory|กรุณาหมวดหมู่โปรแกรมการท่องเที่ยว', 'trim|required');
        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        { 
            $programTour_time_period_start = date('Y-m-d',strtotime($this->input->post("programTour_time_period_start")));
            $programTour_time_period_end = date('Y-m-d',strtotime($this->input->post("programTour_time_period_end")));

            $ProgramTour = array(
                "programTour_nameThai" =>$this->input->post("programTour_nameThai"),
                "programTour_nameEnglish" =>$this->input->post("programTour_nameEnglish"),
                "programTour_nameChinese" =>$this->input->post("programTour_nameChinese"),
                "programTour_nameLaos" =>$this->input->post("programTour_nameLaos"),

                "programTour_interestingThingsThai" =>nl2br(htmlentities($this->input->post("programTour_interestingThingsThai"), ENT_QUOTES, 'UTF-8')),
                "programTour_interestingThingsEnglish" =>nl2br(htmlentities($this->input->post("programTour_interestingThingsEnglish"), ENT_QUOTES, 'UTF-8')),
                "programTour_interestingThingsChinese" =>nl2br(htmlentities($this->input->post("programTour_interestingThingsChinese"), ENT_QUOTES, 'UTF-8')),
                "programTour_interestingThingsLaos" =>nl2br(htmlentities($this->input->post("programTour_interestingThingsLaos"), ENT_QUOTES, 'UTF-8')),
                "programTour_isActive" =>1,
                "MenuItem_menuItem_id" =>1,
                "PublishStatus_publishStatus_id" =>1,
                "programTour_isPublish" =>1,
                "programTour_isSaveDraft" =>0,

                "programTour_createdDTTM" =>date("Y-m-d H:i:s"),
                //"programTour_updatedDTTM" =>date("Y-m-d H:i:s"),

                "programTour_time_period_start" =>$programTour_time_period_start,
                "programTour_time_period_end" =>$programTour_time_period_end,

                "User_user_id" =>$this->session->userdata('user_id')
            );
            $inserted_id_program_tour = $this->program_tour_models->AddProgramTourData($ProgramTour,"ProgramTour");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->upload->do_upload('cropperImage');
            $upload_data = $this->upload->data();
            $CoverItems = array(
                "coverItem_paths" =>$upload_data["file_name"],
                "coverItem_url" =>$this->input->post("coverItem_url"),
                "ProgramTour_programTour_id" =>$inserted_id_program_tour
            );
            $inserted_cover_program_tour_item = $this->program_tour_models->AddProgramTourData($CoverItems,"CoverItems");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if(empty($_FILES['originalImage']['name'][0]))
            {
                $originalImage = "p3.jpg";
            }
            else
            {
                $this->upload->do_upload('originalImage');
                $upload_data = $this->upload->data();
                $originalImage = $upload_data["file_name"];
            }
            $TempImageCover = array(
                "tempImageCover_paths" =>$originalImage,
                "tempImageCover_CropData" =>$this->input->post("CropData"),
                "tempImageCover_CropBoxData" =>$this->input->post("CropBoxData"),
                "tempImageCover_CanvasData" =>$this->input->post("CanvasData"),
                "CoverItems_coverItem_id" =>$inserted_cover_program_tour_item
            );
            $this->program_tour_models->AddProgramTourData($TempImageCover,"TempImageCover");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $ProgramTour_has_SubCategory = array(
                "SubCategory_subcategory_id" =>$this->input->post("subcategory"),
                "ProgramTour_programTour_id" =>$inserted_id_program_tour
            );
            $this->program_tour_models->AddProgramTourData($ProgramTour_has_SubCategory,"ProgramTour_has_SubCategory");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            for($i = 0; $i < intval($this->input->post("ProgramTourCount")); $i++)
            {
                $datesTrip_topic_ProgramTour = json_decode($this->input->post("datesTrip_topic_ProgramTour_".($i+1)));
                $tourist_attractions_data = json_decode($this->input->post("tourist_attractions_data_".($i+1)));

                $DatesTrip = array(
                    "datesTrip_topicThai" => $datesTrip_topic_ProgramTour[0],
                    "datesTrip_topicEnglish" => $datesTrip_topic_ProgramTour[1],
                    "datesTrip_topicChinese" => $datesTrip_topic_ProgramTour[2],
                    "datesTrip_topicLaos" => $datesTrip_topic_ProgramTour[3],
                    "ProgramTour_programTour_id" => $inserted_id_program_tour
                );
                $inserted_datesTrip_id = $this->program_tour_models->AddProgramTourData($DatesTrip,"DatesTrip");

                for ($j = 0; $j < count($tourist_attractions_data); $j++) 
                {
                    $TouristAttractions = array(
                        "Items_items_id" => $tourist_attractions_data[$j]->ItemId,
                        "detail_Thai" => $tourist_attractions_data[$j]->detail[0],
                        "detail_English" => $tourist_attractions_data[$j]->detail[1],
                        "detail_Chinese" => $tourist_attractions_data[$j]->detail[2],
                        "detail_Laos" => $tourist_attractions_data[$j]->detail[3],
                        "DatesTrip_datesTrip_id" =>  $inserted_datesTrip_id
                    );
                    $inserted_touristAttractions_id = $this->program_tour_models->AddProgramTourData($TouristAttractions,"TouristAttractions");

                    for ($k = 0; $k < intval($tourist_attractions_data[$j]->numImgItem); $k++) 
                    {
                        $this->upload->do_upload('img_detail_item_ProgramTour_'.($i+1)."_".($j+1)."_".($k+1));
                        $upload_data = $this->upload->data();
                        $PhotoTourist = array(
                            "photoTourist_paths" =>$upload_data["file_name"],
                            "photoTourist_topicThai" =>$this->input->post("photoTourist_topicThai_detail_item_ProgramTour_".($i+1)."_".($j+1)."_".($k+1)),
                            "photoTourist_topicEnglish" =>$this->input->post("photoTourist_topicEnglish_detail_item_ProgramTour_".($i+1)."_".($j+1)."_".($k+1)), 
                            "photoTourist_topicChinese" =>$this->input->post("photoTourist_topicChinese_detail_item_ProgramTour_".($i+1)."_".($j+1)."_".($k+1)), 
                            "photoTourist_topicLaos" =>$this->input->post("photoTourist_topicLaos_detail_item_ProgramTour_".($i+1)."_".($j+1)."_".($k+1)), 
                            "TouristAttractions_id" =>$inserted_touristAttractions_id
                        );
                        $this->program_tour_models->AddProgramTourData($PhotoTourist,"PhotoTourist");
                    }
                }
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           
            $res = array('state' => true,'error'=>'', 'msg' => 'เพิ่มสำเร็จ');
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }
    public function saveEditProgramTour()
    {
        $this->load->library('form_validation');
        $config['upload_path'] = './assets/img/uploadfile/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        //$config['max_size'] = 2048; 
        $config['encrypt_name'] = true; 
        
        $this->load->library('upload', $config);
       

        $this->form_validation->set_rules('programTour_nameThai', 'required|programTour_nameThai|programTour_nameThai|กรุณากรอกชื่อโปรแกรมการท่องเที่ยว', 'trim|required');
        $this->form_validation->set_rules('subcategory', 'required|subcategory|subcategory|กรุณาหมวดหมู่โปรแกรมการท่องเที่ยว', 'trim|required');
        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        { 
            $programTour_time_period_start = date('Y-m-d',strtotime($this->input->post("programTour_time_period_start")));
            $programTour_time_period_end = date('Y-m-d',strtotime($this->input->post("programTour_time_period_end")));
            $programTour_models_data =  $this->program_tour_models->GetProgramTourData("pt.programTour_id = ".$this->input->post("programTour_id"));
           
            
            $ProgramTour = array(
                "programTour_nameThai" =>$this->input->post("programTour_nameThai"),
                "programTour_nameEnglish" =>$this->input->post("programTour_nameEnglish"),
                "programTour_nameChinese" =>$this->input->post("programTour_nameChinese"),
                "programTour_nameLaos" =>$this->input->post("programTour_nameLaos"),

                "programTour_interestingThingsThai" => nl2br(htmlentities($this->input->post("programTour_interestingThingsThai"), ENT_QUOTES, 'UTF-8')),
                "programTour_interestingThingsEnglish" =>nl2br(htmlentities($this->input->post("programTour_interestingThingsEnglish"), ENT_QUOTES, 'UTF-8')),
                "programTour_interestingThingsChinese" =>nl2br(htmlentities($this->input->post("programTour_interestingThingsChinese"), ENT_QUOTES, 'UTF-8')),
                "programTour_interestingThingsLaos" =>nl2br(htmlentities($this->input->post("programTour_interestingThingsLaos"), ENT_QUOTES, 'UTF-8')),

                "programTour_updatedDTTM" =>date("Y-m-d H:i:s"),

                "programTour_time_period_start" =>$programTour_time_period_start,
                "programTour_time_period_end" =>$programTour_time_period_end,
            );
            $this->program_tour_models->UpdateProgramTourData($ProgramTour,"ProgramTour","programTour_id = ".$this->input->post("programTour_id"));
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $coverItem_paths = './assets/img/uploadfile/'. $programTour_models_data[0]->coverItem_paths;
            $tempImageCover_paths = './assets/img/uploadfile/'. $programTour_models_data[0]->tempImageCover_paths;
            
            //is_readable($coverItem_paths);
            if(is_file($coverItem_paths))
                unlink($coverItem_paths);
            if($programTour_models_data[0]->tempImageCover_paths != "p3.jpg")
            {
                //is_readable($tempImageCover_paths);
                if(is_file($tempImageCover_paths))
                    unlink($tempImageCover_paths);
            }

            $trans_status = $this->program_tour_models->DeleteProgramTourData("CoverItems","ProgramTour_programTour_id = ".$this->input->post("programTour_id"));
           
            $this->upload->do_upload('cropperImage');
            $upload_data = $this->upload->data();
            $CoverItems = array(
                "coverItem_paths" =>$upload_data["file_name"],
                //"coverItem_url" =>$this->input->post("coverItem_url"),
                "ProgramTour_programTour_id" =>$this->input->post("programTour_id"), 
            );
            $inserted_cover_program_tour_item = $this->program_tour_models->AddProgramTourData($CoverItems,"CoverItems");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            $this->upload->do_upload('originalImage');
            $upload_data = $this->upload->data();
            $originalImage = $upload_data["file_name"];
            
            $TempImageCover = array(
                "tempImageCover_paths" =>$originalImage,
                "tempImageCover_CropData" =>$this->input->post("CropData"),
                "tempImageCover_CropBoxData" =>$this->input->post("CropBoxData"),
                "tempImageCover_CanvasData" =>$this->input->post("CanvasData"),
                "CoverItems_coverItem_id" =>$inserted_cover_program_tour_item,
            );
            $this->program_tour_models->AddProgramTourData($TempImageCover,"TempImageCover");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $ProgramTour_has_SubCategory = array(
                "SubCategory_subcategory_id" =>$this->input->post("subcategory")
            );
            $this->program_tour_models->UpdateProgramTourData($ProgramTour_has_SubCategory,"ProgramTour_has_SubCategory","ProgramTour_programTour_id = ".$this->input->post("programTour_id"));
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////// Delete DatesTrip //////////////////////////////////////////////////////////////////////////
            for($i = 0; $i < count($programTour_models_data[0]->DatesTrip); $i++)
            {
                for($j = 0; $j < count($programTour_models_data[0]->DatesTrip[$i]->TouristAttractions); $j++)
                {
                    for($k = 0; $k < count($programTour_models_data[0]->DatesTrip[$i]->TouristAttractions[$j]->PhotoTourist); $k++)
                    {
                        $photo_paths = './assets/img/uploadfile/'. $programTour_models_data[0]->DatesTrip[$i]->TouristAttractions[$j]->PhotoTourist[$k]->photoTourist_paths;
                        //delete_files($photo_paths);
                        // is_readable($photo_paths);
                        if(is_file($photo_paths))
                        {
                            if(is_readable($photo_paths))
                                unlink($photo_paths);
                            sleep(0.25);
                        }
                            
                    }
                }
            }
            $trans_status = $this->program_tour_models->DeleteProgramTourData("DatesTrip","ProgramTour_programTour_id = ".$this->input->post("programTour_id"));
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            for($i = 0; $i < intval($this->input->post("ProgramTourCount")); $i++)
            {
                $datesTrip_topic_ProgramTour = json_decode($this->input->post("datesTrip_topic_ProgramTour_".($i+1)));
                $tourist_attractions_data = json_decode($this->input->post("tourist_attractions_data_".($i+1)));

                $DatesTrip = array(
                    "datesTrip_topicThai" => $datesTrip_topic_ProgramTour[0],
                    "datesTrip_topicEnglish" => $datesTrip_topic_ProgramTour[1],
                    "datesTrip_topicChinese" => $datesTrip_topic_ProgramTour[2],
                    "datesTrip_topicLaos" => $datesTrip_topic_ProgramTour[3],
                    "ProgramTour_programTour_id" => $this->input->post("programTour_id")
                );
                $inserted_datesTrip_id = $this->program_tour_models->AddProgramTourData($DatesTrip,"DatesTrip");

                for ($j = 0; $j < count($tourist_attractions_data); $j++) 
                {
                    $TouristAttractions = array(
                        "Items_items_id" => $tourist_attractions_data[$j]->ItemId,
                        "detail_Thai" => $tourist_attractions_data[$j]->detail[0],
                        "detail_English" => $tourist_attractions_data[$j]->detail[1],
                        "detail_Chinese" => $tourist_attractions_data[$j]->detail[2],
                        "detail_Laos" => $tourist_attractions_data[$j]->detail[3],
                        "DatesTrip_datesTrip_id" =>  $inserted_datesTrip_id
                    );
                    $inserted_touristAttractions_id = $this->program_tour_models->AddProgramTourData($TouristAttractions,"TouristAttractions");

                    for ($k = 0; $k < intval($tourist_attractions_data[$j]->numImgItem); $k++) 
                    {
                        $this->upload->do_upload('img_detail_item_ProgramTour_'.($i+1).'_'.($j+1).'_'.($k+1));
                        $upload_data = $this->upload->data();
                        $PhotoTourist = array(
                            "photoTourist_paths" =>$upload_data["file_name"],
                            "photoTourist_topicThai" =>$this->input->post("photoTourist_topicThai_detail_item_ProgramTour_".($i+1)."_".($j+1)."_".($k+1)),
                            "photoTourist_topicEnglish" =>$this->input->post("photoTourist_topicEnglish_detail_item_ProgramTour_".($i+1)."_".($j+1)."_".($k+1)), 
                            "photoTourist_topicChinese" =>$this->input->post("photoTourist_topicChinese_detail_item_ProgramTour_".($i+1)."_".($j+1)."_".($k+1)), 
                            "photoTourist_topicLaos" =>$this->input->post("photoTourist_topicLaos_detail_item_ProgramTour_".($i+1)."_".($j+1)."_".($k+1)), 
                            "TouristAttractions_id" =>$inserted_touristAttractions_id
                        );
                        $this->program_tour_models->AddProgramTourData($PhotoTourist,"PhotoTourist");
                    }
                }
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            $res = array('state' => true,'error'=>'', 'msg' => 'เพิ่มสำเร็จ');
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }
    public function GetProgramTourData()
    {
        $programTour_models_data =  $this->program_tour_models->GetProgramTourData("pt.programTour_id = ".$this->input->post("programTour_id")." AND pt.programTour_isActive = 1");
        return $this->output->set_content_type('application/json')->set_output(json_encode($programTour_models_data));
    }
}