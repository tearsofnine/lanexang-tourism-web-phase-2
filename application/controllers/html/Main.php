<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
class Main extends Base_Admin_Controller {
    public function __construct()
    {    	
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        
        $this->load->model("CountriesModels","countries_models", true);
        $this->load->model("UserModel","user_model", true);
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("MainModels","main_models", true);
        $this->load->model("ChatModels","chat_models", true);

        if(strcmp($this->session->userdata('lang'),"EN") == 0)
            $this->lang->load("english","english");
        else
            $this->lang->load("thailand","thailand");

    }
    public function index()
	{
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $content['CountAllData'] = $this->main_models->getCountAllData();
        $content['Approval_Item'] = $this->items_models->CountApprovalItem();

        $content['UnReadMessages'] = $this->chat_models->getUnReadMessages($this->session->userdata('user_id'));
        
        

        $this->load->view('Html/MainView',$content);

    }
    public function getCountAllData()
    {
        return $this->output->set_content_type('application/json')->set_output(json_encode ($this->main_models->getCountAllData($this->input->post("provinces_id")) ));
    }
    public function getProvincesData()
    {
        $ProvincesData = $this->main_models->getCountProvincesData();
        echo "<option value=\"\">".$this->lang->line("province")."</option>";
       
        for($i = 0;$i < count($ProvincesData);$i++ )
        {
            if(strcmp($this->session->userdata('lang'),"EN") == 0)
                echo "<option value=\"".$ProvincesData[$i]->provinces_id."\">".$ProvincesData[$i]->provinces_english."</option>";
            else
                echo "<option value=\"".$ProvincesData[$i]->provinces_id."\">".$ProvincesData[$i]->provinces_thai."</option>";
           
        }
       
    }
}