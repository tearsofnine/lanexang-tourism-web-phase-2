<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
class Login extends CI_Controller {
	public function __construct()
    {
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('array');
		$this->load->library('form_validation');
		$this->load->model("UserModel","User", true);

		if(strcmp($this->session->userdata('lang'),"EN") == 0)
            $this->lang->load("english","english");
        else
			$this->lang->load("thailand","thailand");
			
    }
    public function index()
	{
		$content['host'] ="/dasta_thailand/";
		if ($this->session->userdata('logged_in') == true)
        {
			$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
			$domainName = $_SERVER['HTTP_HOST']."/";
			
			if(strcmp(substr($domainName, -1),"/") == 0 )
				$mainHost = $protocol . $domainName.'dasta_thailand/html/Main';
			else
				$mainHost = $protocol . $domainName.'/dasta_thailand/html/Main';
			//str_replace('&#47;&#47;','&#47;',$mainHost);
			echo $mainHost;
			redirect($mainHost);

			
		}
		else
			$this->load->view('Html/LoginView',$content);
	}
	public function authenticate()
	{	
		$email = $this->input->post('input_email');
		$password = $this->input->post('input_password');

		$this->form_validation->set_rules('input_email', 'Username', 'trim|required|valid_email');
		$this->form_validation->set_rules('input_password', 'Password', 'required');

		if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false, 'msg' => validation_errors());
		}
		else 
		{
			$data_user = $this->User->login($email, $password,$this->input->post('radio_lang'));
			switch ($data_user["status"]) {
				case 1:
					$res   = array('state' => true, 'type' => $data_user["status"],'user_id' => $data_user["user_id"], 'msg' => 'เข้าสู่ระบบสำเร็จ');
					break;
				case -1:
					$msg = "เกิดข้อผิดพลาด password wrong";
					$res = array('state' => false,'type' => $data_user["status"],'user_id' => $data_user["user_id"], 'msg' => $msg);
					break;
				case -2:
					$msg = "เกิดข้อผิดพลาด No user!!!";
					$res = array('state' => false,'type' => $data_user["status"],'user_id' => $data_user["user_id"], 'msg' => $msg);
					break;
				case -3:
					$msg = "เกิดข้อผิดพลาด user is Delete";
					$res = array('state' => false,'type' => $data_user["status"],'user_id' => $data_user["user_id"], 'msg' => $msg);
					break;
				case -4:
					$msg = "เกิดข้อผิดพลาด user is not Active";
					$res = array('state' => false,'type' => $data_user["status"],'user_id' => $data_user["user_id"], 'msg' => $msg);
				break;
				default:
					$msg = "เกิดข้อผิดพลาด";
					$res = array('state' => false, 'msg' => $msg);
			}
			
		}

		return $this->output->set_content_type('application/json')->set_output(json_encode($res));
	}
	public function logout()
	{
		$this->session->unset_userdata("user_id");
		$this->session->unset_userdata("user_email");
		$this->session->unset_userdata("user_firstName");
		$this->session->unset_userdata("user_lastName");
		$this->session->unset_userdata("UserRole_userRole_id");
		$this->session->unset_userdata("ProvinceGroup_pg_id");
		$this->session->unset_userdata("logged_in");
		$this->session->unset_userdata("lang");
		redirect();
	}
}