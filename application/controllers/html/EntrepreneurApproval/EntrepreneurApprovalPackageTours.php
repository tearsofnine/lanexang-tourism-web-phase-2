<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
class EntrepreneurApprovalPackageTours extends Base_Admin_Controller {
    public function __construct()
    {    	
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->helper('file');

        $this->load->model("CountriesModels","countries_models", true);
        $this->load->model("UserModel","user_model", true);
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("SearchModels","search_models", true);
        $this->load->model("ChatModels","chat_models", true);

        if(strcmp($this->session->userdata('lang'),"EN") == 0)
            $this->lang->load("english","english");
        else
            $this->lang->load("thailand","thailand");
    }
    
    public function index()
	{
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $content['Approval_Item'] = $this->items_models->CountApprovalItem();
        $content['Approval_ItemState'] = $this->items_models->CountApprovalItemState("i.MenuItem_menuItem_id = 9 AND i.items_isActive != 99");
        $content['ApprovalUserState'] = $this->user_model->CountApprovalUserState("b.business_type_category_id = 9");
        $content['UnReadMessages'] = $this->chat_models->getUnReadMessages($this->session->userdata('user_id'));
        
        $this->load->view('Html/EntrepreneurApproval/EntrepreneurApprovalPackageToursView',$content);
    }

    public function getDataForTable()
    {
        // Datatables Variables
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $data =array();
        $userData = $this->user_model->GetUserJoinBusinessData("u.user_isActive = ".$this->input->post("find_approval")." AND u.user_isDelete = 0 AND b.business_type_category_id = 9");
       
        for($i=0;$i<count($userData);$i++)
        {
            if($userData[$i]->user_isActive == 0)
            {
                $label[0] = "warning";
                $label[1] = "รอการอนุมัติ";
            }   
            else if($userData[$i]->user_isActive == 1)
            {
                $label[0] = "primary";
                $label[1] = "อนุมัติแล้ว";
            }
            else
            {
                $label[0] = "danger";
                $label[1] = "ไม่ผ่านการอนุมัติ";
            }

            $data[] = array(
                $userData[$i]->user_id,
                "<span class=\"label label-".$label[0]."\">".$label[1]."</span>",
                " <p style=\"color: #1AB394;\">".$userData[$i]->business_nameThai."</p>",
                "<p class=\"\">".$userData[$i]->user_firstName." ".$userData[$i]->user_lastName."</p>",
                "<p>".$userData[$i]->user_phone."</p>",
                "<p>".date("d/m/Y",strtotime($userData[$i]->user_createdDTTM))."</p>",
                "<button class=\"btn btn-info btn-item-view\" value=\"".$userData[$i]->user_id."\">".$this->lang->line("view")."</button>
                <!--<a href=\"../Items/Attractions/editAttractions?item_id=".$userData[$i]->user_id."\" class=\"btn btn-warning\">".$this->lang->line("edit")."</a>-->
                <button class=\"btn btn-danger btn-item-del\" value=\"".$userData[$i]->user_id."\">ลบ</button>
                ",
            );
        }
       
        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($data),
            "recordsFiltered" =>count($data),
            "data" => $data
        );
        echo json_encode($output);
    }
}