<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
class EntrepreneurApprovalProgramTour extends Base_Admin_Controller {
    public function __construct()
    {    	
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->helper('file');

        $this->load->model("CountriesModels","countries_models", true);
        $this->load->model("UserModel","user_model", true);
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("SearchModels","search_models", true);
        $this->load->model("ChatModels","chat_models", true);

        if(strcmp($this->session->userdata('lang'),"EN") == 0)
            $this->lang->load("english","english");
        else
            $this->lang->load("thailand","thailand");
    }
    
    public function index()
	{
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $content['Approval_Item'] = $this->items_models->CountApprovalItem();
        $content['Approval_ItemState'] = $this->items_models->CountApprovalItemState("i.MenuItem_menuItem_id = 1 AND i.items_isActive != 99");
        $content['ApprovalUserState'] = $this->user_model->CountApprovalUserState("b.business_type_category_id = 1");
        $content['UnReadMessages'] = $this->chat_models->getUnReadMessages($this->session->userdata('user_id'));
        
        $this->load->view('Html/EntrepreneurApproval/EntrepreneurApprovalProgramTourView',$content);
    }
}