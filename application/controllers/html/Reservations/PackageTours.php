<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
class PackageTours extends Base_Admin_Controller {
    public function __construct()
    {    	
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');

        $this->load->library('form_validation');
        $config['upload_path'] = './assets/img/uploadfile/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = 2048; 
        $config['encrypt_name'] = true; 
        $this->load->library('upload', $config);

        $this->load->model("CountriesModels","countries_models", true);
        $this->load->model("UserModel","user_model", true);
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("SearchModels","search_models", true);
        $this->load->model("ChatModels","chat_models", true);

        if(strcmp($this->session->userdata('lang'),"EN") == 0)
            $this->lang->load("english","english");
        else
            $this->lang->load("thailand","thailand");
            
    }
    public function index()
	{
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $content['Approval_Item'] = $this->items_models->CountApprovalItem();
        $content['ModalView'] = "PackageTours";
        $content['UnReadMessages'] = $this->chat_models->getUnReadMessages($this->session->userdata('user_id'));
        
        $this->load->view('Html/Reservations/PackageToursView',$content);
    }
    public function addPackageTours()
    {
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $content['Approval_Item'] = $this->items_models->CountApprovalItem();  
        $content['UnReadMessages'] = $this->chat_models->getUnReadMessages($this->session->userdata('user_id'));
        
        $this->load->view('Html/Reservations/PackageToursAddView',$content);
    }
    public function editPackageTours()
    {
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $content['Approval_Item'] = $this->items_models->CountApprovalItem();
        $content['data_item'] = $this->items_models->GetItemReservationsData("i.items_isActive = 1 AND i.items_id = ".$this->input->get("item_id")." AND mi.menuItem_id = 9 AND cat.category_id = 9","PackageTours");
        $content['UnReadMessages'] = $this->chat_models->getUnReadMessages($this->session->userdata('user_id'));
        
        if(count($content['data_item']) != 0)
            $this->load->view('Html/Reservations/PackageToursEditView',$content);
        else
        {
            $content['data_item'] = $this->items_models->GetItemReservationsData("i.items_id = ".$this->input->get("item_id")." AND mi.menuItem_id = 9 AND cat.category_id = 9","PackageTours");
            if(count($content['data_item']) != 0)
            {
                $this->load->view('Html/Reservations/PackageToursEditView',$content);
            }
            else
                redirect('../PackageTours');
        }
  
    }
    public function getNameCategorySubCategory()
    {
        $NameCategorySubCategory = $this->items_models->getNameCategorySubCategory(9);
        echo "<option value=\"\">ประเภทแพคเกจทัวร์</option>";
        
        foreach ($NameCategorySubCategory->result() as $item)
        {
            echo "<option value=\"".$item->subcategory_id."\">".$item->subcategory_thai."</option>";
        }
    }
    public function saveNewItem()
    {
        $this->load->library('form_validation');
        
        $config['upload_path'] = './assets/img/uploadfile/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        //$config['max_size'] = 2048; 
        $config['encrypt_name'] = true; 
        $this->load->library('upload', $config);

        $this->form_validation->set_rules('itmes_topicThai', 'required|itmes_topicThai|itmes_topicThai|กรุณากรอกชื่อแพคเกจทัวร์', 'trim|required');
        $this->form_validation->set_rules('namesubcategory', 'required|namesubcategory|namesubcategory|กรุณากรอกชื่อแพคเกจทัวร์', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else if(intval($this->input->post("box_cover_image_package_tours")) == 0)
        {
            $res = array('state' => false,'error'=>'Image', 'msg' => "Please select cover image file");
        }
        else
        {
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Items
            $Items = array(
                "itmes_topicThai" =>$this->input->post("itmes_topicThai"),
                "itmes_topicEnglish" =>$this->input->post("itmes_topicEnglish"),
                "itmes_topicLaos" =>$this->input->post("itmes_topicLaos"),
                "itmes_topicChinese" =>$this->input->post("itmes_topicChinese"),
                "items_phone" =>$this->input->post("items_phone"),
                "MenuItem_menuItem_id" =>9,
                "User_user_id" => $this->session->userdata('user_id'),
                "items_createdDTTM" => date("Y-m-d H:i:s"),
                "items_isActive" => 1,
                "items_isPublish" => 1,
            );
            $inserted_id_item = $this->items_models->AddItemData($Items,"Items");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $Items_has_SubCategory = array(
                "SubCategory_subcategory_id" =>$this->input->post("namesubcategory"),
                "Items_items_id" =>$inserted_id_item,
            );
            $this->items_models->AddItemData($Items_has_SubCategory,"Items_has_SubCategory");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if(!empty($this->input->post("province_boundary")))
            {
                $province_boundary = explode(',',$this->input->post("province_boundary"));
                
                for($i = 0; $i < count($province_boundary); $i++){
                    $Items_has_ScopeArea = array(
                        "Provinces_provinces_id" =>$province_boundary[$i],
                        "Items_items_id" =>$inserted_id_item,
                    );
                    $this->items_models->AddItemData($Items_has_ScopeArea,"Items_has_ScopeArea");
                }
                
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CoverItems
            for($i = 0;$i< intval($this->input->post("box_cover_image_package_tours"));$i++)
            {
                $this->upload->do_upload('cropperImage_'.($i+1));
                $upload_data = $this->upload->data();
                $CoverItems = array(
                    "coverItem_paths" =>$upload_data["file_name"],
                    "coverItem_url" =>$this->input->post("coverItem_url"),
                    "Items_items_id" =>$inserted_id_item, 
                );
                $this->items_models->AddItemData($CoverItems,"CoverItems");
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// bookingConditioncol
            $bookingConditioncol = json_decode($this->input->post("bookingConditioncol"));
            for($i = 0;$i< count($bookingConditioncol);$i++)
            {
                $bookingConditioncol_detail = array(
                    "bookingConditioncol_detailThai" => $bookingConditioncol[$i]->bookingConditioncol_detailThai,
                    "bookingConditioncol_detailEnglish" => $bookingConditioncol[$i]->bookingConditioncol_detailEnglish,
                    "bookingConditioncol_detailLaos" => $bookingConditioncol[$i]->bookingConditioncol_detailLaos,
                    "bookingConditioncol_detailChinese" => $bookingConditioncol[$i]->bookingConditioncol_detailChinese,
                    "Items_items_id" => $inserted_id_item,
                    "BookingConditionCategory_bookingConditionCategory_id" => ($i+1)
                );
                $this->items_models->AddItemData($bookingConditioncol_detail,"BookingCondition");
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TravelPeriod
            $TravelPeriod = json_decode($this->input->post("TravelPeriod"));
            for($i = 0;$i< count($TravelPeriod);$i++)
            {
                $travelPeriod_time_period_start = date('Y-m-d',strtotime($TravelPeriod[$i]->travelPeriod_time_period_start));
                $travelPeriod_time_period_end = date('Y-m-d',strtotime($TravelPeriod[$i]->travelPeriod_time_period_end));

                $travelPeriod_detail = array(
                    "travelPeriod_amount" => $TravelPeriod[$i]->travelPeriod_amount,

                    "travelPeriod_time_period_start" => $travelPeriod_time_period_start,
                    "travelPeriod_time_period_end" => $travelPeriod_time_period_end,
                    
                    "travelPeriod_adult_price" => $TravelPeriod[$i]->travelPeriod_adult_price,
                    "travelPeriod_adult_special_price" => $TravelPeriod[$i]->travelPeriod_adult_special_price,
                    "travelPeriod_child_price" => $TravelPeriod[$i]->travelPeriod_child_price,
                    "travelPeriod_child_special_price" => $TravelPeriod[$i]->travelPeriod_child_special_price,
                    "Items_items_id" => $inserted_id_item
                );
                $this->items_models->AddItemData($travelPeriod_detail,"TravelPeriod");
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TravelDetails
            $TravelDetails = json_decode($this->input->post("TravelDetails"));
            for($i = 0;$i< count($TravelDetails);$i++)
            {
                $travelDetails_detail = array(
                    "travelDetails_dateThai" => $TravelDetails[$i]->travelDetails_dateThai,
                    "travelDetails_dateEnglish" => $TravelDetails[$i]->travelDetails_dateEnglish,
                    "travelDetails_dateLaos" => $TravelDetails[$i]->travelDetails_dateLaos,
                    "travelDetails_dateChinese" => $TravelDetails[$i]->travelDetails_dateChinese,

                    "travelDetails_textThai" => $TravelDetails[$i]->travelDetails_textThai,
                    "travelDetails_textEnglish" => $TravelDetails[$i]->travelDetails_textEnglish,
                    "travelDetails_textLaos" => $TravelDetails[$i]->travelDetails_textLaos,
                    "travelDetails_textChinese" => $TravelDetails[$i]->travelDetails_textChinese,

                    "travelDetails_hotel_nameThai" => $TravelDetails[$i]->travelDetails_hotel_nameThai,
                    "travelDetails_hotel_nameEnglish" => $TravelDetails[$i]->travelDetails_hotel_nameEnglish,
                    "travelDetails_hotel_nameLaos" => $TravelDetails[$i]->travelDetails_hotel_nameLaos,
                    "travelDetails_hotel_nameChinese" => $TravelDetails[$i]->travelDetails_hotel_nameChinese,

                    "travelDetails_food_breakfast" => $TravelDetails[$i]->travelDetails_food_breakfast,
                    "travelDetails_food_lunch" => $TravelDetails[$i]->travelDetails_food_lunch,
                    "travelDetails_food_dinner" => $TravelDetails[$i]->travelDetails_food_dinner,

                    "Items_items_id" => $inserted_id_item
                );
                $this->items_models->AddItemData($travelDetails_detail,"TravelDetails");
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $res = array('state' => true,'error'=>'', 'msg' => 'เพิ่มสำเร็จ');
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }
    public function saveEditItem()
    {
        $this->load->library('form_validation');
        $config['upload_path'] = './assets/img/uploadfile/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        //$config['max_size'] = 2048; 
        $config['encrypt_name'] = true; 
        $this->load->library('upload', $config);
        $this->form_validation->set_rules('itmes_topicThai', 'required|itmes_topicThai|itmes_topicThai|กรุณากรอกชื่อแพคเกจทัวร์', 'trim|required');
        $this->form_validation->set_rules('namesubcategory', 'required|namesubcategory|namesubcategory|กรุณากรอกชื่อแพคเกจทัวร์', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else if(intval($this->input->post("box_cover_image_package_tours")) == 0)
        {
            $res = array('state' => false,'error'=>'Image', 'msg' => "Please select cover image file");
        }
        else
        {
            $items_models_data = $this->items_models->GetItemReservationsData("i.items_id = ".$this->input->post("items_id"),"PackageTours");
            
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Items
            $Items = array(
                "itmes_topicThai" =>$this->input->post("itmes_topicThai"),
                "itmes_topicEnglish" =>$this->input->post("itmes_topicEnglish"),
                "itmes_topicLaos" =>$this->input->post("itmes_topicLaos"),
                "itmes_topicChinese" =>$this->input->post("itmes_topicChinese"),
                "items_phone" =>$this->input->post("items_phone"),
                "items_updatedDTTM" => date("Y-m-d H:i:s"),
                
            );
            $trans_status = $this->items_models->UpdateItemData($Items,"Items","items_id = ".$this->input->post("items_id"));

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $Items_has_SubCategory = array(
                "SubCategory_subcategory_id" =>$this->input->post("namesubcategory"),  
            );
            $this->items_models->UpdateItemData($Items_has_SubCategory,"Items_has_SubCategory","Items_items_id = ".$this->input->post("items_id"));
            $trans_status = $this->items_models->DeleteItemData("Items_has_ScopeArea","Items_items_id = ".$this->input->post("items_id"));
            if(!empty($this->input->post("province_boundary")))
            {
                $province_boundary = explode(',',$this->input->post("province_boundary"));
                
                for($i = 0; $i < count($province_boundary); $i++){
                    $Items_has_ScopeArea = array(
                        "Provinces_provinces_id" =>$province_boundary[$i],
                        "Items_items_id" =>$this->input->post("items_id"),
                    );
                    $this->items_models->AddItemData($Items_has_ScopeArea,"Items_has_ScopeArea");
                }
                
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CoverItems
            for($i = 0;$i< count($items_models_data[0]->CoverItems);$i++)
            {
                $coverItem_paths = './assets/img/uploadfile/'. $items_models_data[0]->CoverItems[$i]->coverItem_paths;
                is_readable($coverItem_paths);
                unlink($coverItem_paths);
            }
            $trans_status = $this->items_models->DeleteItemData("CoverItems","Items_items_id = ".$this->input->post("items_id"));

            for($i = 0;$i< intval($this->input->post("box_cover_image_package_tours"));$i++)
            {
                $this->upload->do_upload('cropperImage_'.($i+1));
                $upload_data = $this->upload->data();
                $CoverItems = array(
                    "coverItem_paths" =>$upload_data["file_name"],
                    "coverItem_url" =>$this->input->post("coverItem_url"),
                    "Items_items_id" =>$this->input->post("items_id"),
                );
                $this->items_models->AddItemData($CoverItems,"CoverItems");
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
// bookingConditioncol
            $trans_status = $this->items_models->DeleteItemData("BookingCondition","Items_items_id = ".$this->input->post("items_id"));
            $bookingConditioncol = json_decode($this->input->post("bookingConditioncol"));
            for($i = 0;$i< count($bookingConditioncol);$i++)
            {
                $bookingConditioncol_detail = array(
                    "bookingConditioncol_detailThai" => $bookingConditioncol[$i]->bookingConditioncol_detailThai,
                    "bookingConditioncol_detailEnglish" => $bookingConditioncol[$i]->bookingConditioncol_detailEnglish,
                    "bookingConditioncol_detailLaos" => $bookingConditioncol[$i]->bookingConditioncol_detailLaos,
                    "bookingConditioncol_detailChinese" => $bookingConditioncol[$i]->bookingConditioncol_detailChinese,
                    "Items_items_id" => $this->input->post("items_id"),
                    "BookingConditionCategory_bookingConditionCategory_id" => ($i+1)
                );
                $this->items_models->AddItemData($bookingConditioncol_detail,"BookingCondition");
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TravelPeriod
            $trans_status = $this->items_models->DeleteItemData("TravelPeriod","Items_items_id = ".$this->input->post("items_id"));
            
            $TravelPeriod = json_decode($this->input->post("TravelPeriod"));
            for($i = 0;$i< count($TravelPeriod);$i++)
            {
                $travelPeriod_time_period_start = date('Y-m-d',strtotime($TravelPeriod[$i]->travelPeriod_time_period_start));
                $travelPeriod_time_period_end = date('Y-m-d',strtotime($TravelPeriod[$i]->travelPeriod_time_period_end));

                $travelPeriod_detail = array(
                    "travelPeriod_amount" => $TravelPeriod[$i]->travelPeriod_amount,

                    "travelPeriod_time_period_start" => $travelPeriod_time_period_start,
                    "travelPeriod_time_period_end" => $travelPeriod_time_period_end,
                    
                    "travelPeriod_adult_price" => $TravelPeriod[$i]->travelPeriod_adult_price,
                    "travelPeriod_adult_special_price" => $TravelPeriod[$i]->travelPeriod_adult_special_price,
                    "travelPeriod_child_price" => $TravelPeriod[$i]->travelPeriod_child_price,
                    "travelPeriod_child_special_price" => $TravelPeriod[$i]->travelPeriod_child_special_price,
                    "Items_items_id" => $this->input->post("items_id"),
                );
                $this->items_models->AddItemData($travelPeriod_detail,"TravelPeriod");
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TravelDetails
            $trans_status = $this->items_models->DeleteItemData("TravelDetails","Items_items_id = ".$this->input->post("items_id"));
            
            $TravelDetails = json_decode($this->input->post("TravelDetails"));
            for($i = 0;$i< count($TravelDetails);$i++)
            {
                $travelDetails_detail = array(
                    "travelDetails_dateThai" => $TravelDetails[$i]->travelDetails_dateThai,
                    "travelDetails_dateEnglish" => $TravelDetails[$i]->travelDetails_dateEnglish,
                    "travelDetails_dateLaos" => $TravelDetails[$i]->travelDetails_dateLaos,
                    "travelDetails_dateChinese" => $TravelDetails[$i]->travelDetails_dateChinese,

                    "travelDetails_textThai" => $TravelDetails[$i]->travelDetails_textThai,
                    "travelDetails_textEnglish" => $TravelDetails[$i]->travelDetails_textEnglish,
                    "travelDetails_textLaos" => $TravelDetails[$i]->travelDetails_textLaos,
                    "travelDetails_textChinese" => $TravelDetails[$i]->travelDetails_textChinese,

                    "travelDetails_hotel_nameThai" => $TravelDetails[$i]->travelDetails_hotel_nameThai,
                    "travelDetails_hotel_nameEnglish" => $TravelDetails[$i]->travelDetails_hotel_nameEnglish,
                    "travelDetails_hotel_nameLaos" => $TravelDetails[$i]->travelDetails_hotel_nameLaos,
                    "travelDetails_hotel_nameChinese" => $TravelDetails[$i]->travelDetails_hotel_nameChinese,

                    "travelDetails_food_breakfast" => $TravelDetails[$i]->travelDetails_food_breakfast,
                    "travelDetails_food_lunch" => $TravelDetails[$i]->travelDetails_food_lunch,
                    "travelDetails_food_dinner" => $TravelDetails[$i]->travelDetails_food_dinner,

                    "Items_items_id" => $this->input->post("items_id"),
                );
                $this->items_models->AddItemData($travelDetails_detail,"TravelDetails");
            }
            $res = array('state' => true,'error'=>'', 'msg' => 'เพิ่มสำเร็จ');
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }
    public function getDataPackageToursForTable()
    {
        // Datatables Variables
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));
        
        //$data = $this->items_models->setDatatableReservations("mi.menuItem_id = 9 AND cat.category_id = 9 AND i.items_isActive = 1","PackageTours");
        $data = $this->search_models->getDataSearchForTable(
            "PackageTours",
            $this->input->post("namesubcategory"),
            $this->input->post("province_boundary"),
            $this->input->post("NumDatesTrip"),
            "",
            "i.items_isActive = 1 AND mi.menuItem_id = 9 AND cat.category_id = 9"
        );
        

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($data),
            "recordsFiltered" =>count($data),
            "data" => $data
        );
        echo json_encode($output);
    }
    public function getNumDatesTrip()
    {
        $NumDatesTrip = $this->items_models->GetNumDatesTrip();
        echo "<option value=\"\">ระยะวันของแพคเกจทัวร์</option>";
        
        foreach ($NumDatesTrip as $item)
        {
            echo "<option value=\"".$item->NumDatesTrip."\">".$item->NumDatesTrip." วัน</option>";
            
        }
    }
}