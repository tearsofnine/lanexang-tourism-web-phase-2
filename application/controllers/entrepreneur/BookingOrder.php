<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
class BookingOrder extends Base_Entrepreneur_Controller {
    public function __construct()
    {    	
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model("CountriesModels","countries_models", true);
        $this->load->model("UserModel","user_model", true);
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("SearchModels","search_models", true);
        $this->load->model("BookingModels","booking_models", true);
        $this->load->model("PaymentTransactionModels","payment_transaction_models", true);
        $this->load->model("ChatModels","chat_models", true);

        if(strcmp($this->session->userdata('lang'),"EN") == 0)
            $this->lang->load("english","english");
        else
            $this->lang->load("thailand","thailand");
    }
    
    public function index()
	{
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $this->load->view('Entrepreneur/BookingOrderView',$content);
        
    }
    public function OrderDetails()
    {
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        if(intval($this->session->userdata('business_type_category_id')) != 1)
            $DefaultItemsData = $this->items_models->GetDefaultItemsData("i.items_isActive = 1 AND i.items_isPublish = 1 AND i.User_user_id = ".$this->session->userdata('entrepreneur_user_id'));
        else
            $DefaultItemsData = $this->program_tour_models->GetProgramTourData("pt.User_user_id = ".$this->session->userdata('entrepreneur_user_id'));

        if(intval($this->session->userdata('business_type_category_id')) == 4)
            $bookingData = $this->booking_models->GetBookingDataOrderDetails("pt.paymentTransaction_id = ".$this->input->get("booking_id"));
        else
            $bookingData = $this->booking_models->GetBookingDataPaymentTransaction("b.booking_id = ".$this->input->get("booking_id"));
        $state ="";
        switch (intval($bookingData[0]->PurchaseStatus_purchaseStatus_id)) 
        {
            case 2:
                $state ="<p><span class=\"badge badge-warning\" >รอการอนุมัติ</span><p>";
                $state .="
                    <div class=\"text-right\">
                        <button class=\"btn btn-info btn-rounded font-bold btn-booking-approve\" value=\"".$bookingData[0]->paymentTransaction_id."\">ยอมรับ</button>
                        <button class=\"btn btn-danger btn-rounded font-bold btn-booking-disapprove\" value=\"".$bookingData[0]->paymentTransaction_id."\">ปฏิเสธ</button>
                    </div>";
            break;
            case 1:
                $state ="<p><span class=\"badge badge-primary\" >อนุมัติแล้ว</span><p>";   
            break;
            case 3;
                $state ="<p><span class=\"badge badge-danger\" >ไม่ผ่านการอนุมัติ</span><p>";
            break;
        }
        switch (intval($this->session->userdata('business_type_category_id'))) 
        {
            case 4:
                $OrderDetailsData = $this->payment_transaction_models->GetPaymentTransactionDataOrderDetails("paymentTransaction_id = ".$this->input->get("booking_id"));

                $table = "";
                for($i=0;$i < count($OrderDetailsData[0]->OrderData); $i++)
                {
                    $table .= "<tr>
                                    <td>
                                        <h3 class=\"text-muted font-normal\">".$OrderDetailsData[0]->OrderData[$i]->Product[0]->product_namesThai." </h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h3 class=\"text-muted font-normal\"> ราคา </h2>
                                    </td>
                                    <td>
                                        <h3 class=\"text-muted font-normal\"> THB ".number_format($OrderDetailsData[0]->OrderData[$i]->orderDetails_Price).".- </h3>
                                    </td>
                                    <td>
                                        <h2 class=\"text-muted font-normal\"> ".number_format($OrderDetailsData[0]->OrderData[$i]->orderDetails_quantity)." ชุด </h2>
                                    </td>
                                    <td>
                                        <h2 class=\"text-muted font-normal float-right\">THB  ".number_format($OrderDetailsData[0]->OrderData[$i]->orderDetails_totalPrice)." .- </h2>
                                    </td>
                                </tr>
                                
                    ";
                }
                $content["items_id"] = $DefaultItemsData[0]->Items_items_id;
                $content["user_id"] = $OrderDetailsData[0]->User_user_id;
                
                $content["orderDetails"] ="
                <div class=\"ibox\">
                    <div class=\"ibox-content-head\">
                        <p class=\"\">ชื่อผู้จอง : ".$bookingData[0]->paymentTransaction_firstName." ".$bookingData[0]->paymentTransaction_lastName."</p>
                        <p class=\"\">อีเมล : ".$bookingData[0]->paymentTransaction_email."</p>
                        <p class=\"\">เบอร์โทร : ".$bookingData[0]->paymentTransaction_phone."</p>
                        <p class=\"\">คูปองส่วนลด : ไม่มี</p>
                        <hr>
                        <table class=\"table table-hover\">
                            
                            ".$table."

                            <td>
                                <h2 class=\"text-muted font-bold\">รวม</h2>
                            </td>
                            <td>
                                <h2 class=\"text-info font-normal float-right\">  THB ".number_format($bookingData[0]->PaymentTransaction_totalPrice)."</h2>
                            </td>
                        </table>
                        <hr>
                       ".$state."
                    </div>
                    <div class=\"col-lg-3\"></div>
					
                </div>
                ";
                break;
            case 5: 
                $RoomData = $this->items_models->GetItemDataRoom("room_id = ".$bookingData[0]->Room_room_id);
                $content["items_id"] = $bookingData[0]->Items_items_id;
                $content["user_id"] = $bookingData[0]->User_user_id;
                $content["orderDetails"] = "
                    <div class=\"ibox-content\">
                        <h2 class=\"text-muted font-bold\">".$DefaultItemsData[0]->itmes_topicThai."</h2>
                        <div class=\"small m-b-xs\">
                            <p style=\"color:#FF7745;\">โรงแรม</p>
                        </div>
                        <hr>
                        <h3 class=\"text-muted font-bold\">แบบห้องที่เลือก</h3>
                        <div>
                            <div class=\"ibox-content no-padding border-left-right\">
                                <img class=\"d-block w-100\" src=\"/dasta_thailand/assets/img/uploadfile/".$RoomData[0]->PhotoRoom[0]->pictureRoom_paths."\" alt=\"First slide\">
                            </div>
                            <div class=\"ibox-content-hotel\">
                                <div class=\"row vertical-align\">
                                    <div class=\"col-7\">
                                        <h1 class=\"font-bold no-margins text-white\">".$RoomData[0]->room_topicThai."</h1>
                                    </div>
                                    <div class=\"col-5 text-right\">
                                        <h1 class=\"font-bold no-margins text-white\">THB ".$RoomData[0]->room_price."</h1>
                                        <h6 class=\"font-normal no-margins text-white\">ราคา/ต่อคืน รวมภาษีแล้ว </h6>
                                    </div>
                                </div>
                                <hr class=\"hr-line-dashed\">
                                <span class=\"text-white\">".$RoomData[0]->room_descriptionThai."</span>
                                <hr class=\"hr-line-dashed\">
                                <p class=\"text-white\">".(intval($RoomData[0]->room_breakfast) == 1?"รวมอาหารเช้า":"ไม่รวมอาหารเช้า")."</p>
                            </div>
                        </div>
                        <hr>
                        <h3 class=\"text-muted font-bold\">ช่วงเวลาการจอง</h3>
                        <div class=\"alert alert-info\">
                            <i class=\"fa fa-calendar-o\"> ".date("d/m/Y",strtotime($bookingData[0]->booking_checkIn))." - ".date("d/m/Y",strtotime($bookingData[0]->booking_checkOut))."</i>
                            <i class=\"fa fa-check-circle text-info float-right\" style=\"font-size: 20px;\"></i>
                        </div>
                        <hr>
                        <h3 class=\"text-muted font-bold\">รายละเอียดผู้จอง</h3>
                        <p class=\"\">ชื่อผู้จอง : ".$bookingData[0]->paymentTransaction_firstName." ".$bookingData[0]->paymentTransaction_lastName."</p>
                        <p class=\"\">อีเมล : ".$bookingData[0]->paymentTransaction_email."</p>
                        <p class=\"\">เบอร์โทร : ".$bookingData[0]->paymentTransaction_phone."</p>
                        <p class=\"\">คูปองส่วนลด : ไม่มี</p>
                        <hr>
                        ".$state."
						 
                    </div>
                ";
                break;
            case 9:
                $ItemsData = $this->items_models->GetItemReservationsData("i.items_isActive = 1 AND i.items_isPublish = 1 AND i.items_id = ".$bookingData[0]->Items_items_id,"PackageTours");
                $ScopeArea = array();
                foreach($ItemsData[0]->ScopeArea as $keys => $rr) {
                    $ScopeArea[] = $rr->provinces_thai;
                }
                // $travelPeriod_adult_special_price = $ItemsData[0]->TravelPeriod[intval($bookingData[0]->booking_duration)]->travelPeriod_adult_special_price;
                // $travelPeriod_adult_price = $ItemsData[0]->TravelPeriod[intval($bookingData[0]->booking_duration)]->travelPeriod_adult_price;
                // $travelPeriod_child_price = $ItemsData[0]->TravelPeriod[intval($bookingData[0]->booking_duration)]->travelPeriod_child_price;
                // $travelPeriod_child_special_price = $ItemsData[0]->TravelPeriod[intval($bookingData[0]->booking_duration)]->travelPeriod_child_special_price;
                $content["items_id"] = $bookingData[0]->Items_items_id;
                $content["user_id"] = $bookingData[0]->User_user_id;
                $content["orderDetails"] = "
                <div class=\"ibox-content\">
                    <h2 class=\"text-muted font-bold\">".$ItemsData[0]->itmes_topicThai."</h2>
                    <div class=\"small m-b-xs\">
                        <span class=\"text-muted\"><i class=\"fa fa-clock-o\"></i>".join("-",$ScopeArea)."</span>
                        <span class=\"text-muted\"><i class=\"fa fa-clock-o\"></i> ".$ItemsData[0]->subcategory_thai."</span>
                        <span class=\"text-muted\"><i class=\"fa fa-clock-o\"></i> ".count($ItemsData[0]->TravelDetails)." วัน ".(count($ItemsData[0]->TravelDetails)-1)." คืน</span>
                    </div>
                    <p class=\"text-right text-navy\">".$this->session->userdata('business_nameThai')."</p>
                    <hr>
                    <h3 class=\"text-muted font-bold\">ช่วงเวลาเดินทาง</h3>
                    <p class=\"\">".date("d/m/Y",strtotime($ItemsData[0]->TravelPeriod[intval($bookingData[0]->booking_duration)]->travelPeriod_time_period_start))." - ".date("d/m/Y",strtotime($ItemsData[0]->TravelPeriod[intval($bookingData[0]->booking_duration)]->travelPeriod_time_period_end))."</p>
                    <hr>
                    <h3 class=\"text-muted font-bold\">รายละเอียดผู้จอง</h3>
                    <p class=\"\">ชื่อผู้จอง : ".$bookingData[0]->paymentTransaction_firstName." ".$bookingData[0]->paymentTransaction_lastName."</p>
                    <p class=\"\">อีเมล : ".$bookingData[0]->paymentTransaction_email."</p>
                    <p class=\"\">เบอร์โทร :".$bookingData[0]->paymentTransaction_phone."</p>
                    <p class=\"\">คูปองส่วนลด : ไม่มี</p>
                    <hr>
                    <h3 class=\"text-muted font-bold\">จำนวนผู้เดินทาง</h3>
                    <table class=\"table table-hover\">
                        <thead>
                            <tr>
                                <th>ผู้เดินทาง</th>
                                <th>ราคา/ท่าน</th>
                                <th>จำนวน</th>
                                <th>ราคา(รวม)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>ผู้ใหญ่พัก 2-3 คน</td>
                                <td>".number_format($bookingData[0]->booking_guestAdultPrice).".-</td>
                                <td>".$bookingData[0]->booking_guestAdult." ท่าน</td>
                                <td>".number_format(intval($bookingData[0]->booking_guestAdultPrice) * intval($bookingData[0]->booking_guestAdult)).".-</td>
                            </tr>
                            <tr>
                                <td>ผู้ใหญ่พักเดี่ยว</td>
                                <td>".number_format($bookingData[0]->booking_guestAdultSinglePrice).".-</td>
                                <td>".$bookingData[0]->booking_guestAdultSingle." ท่าน</td>
                                <td>".number_format(intval($bookingData[0]->booking_guestAdultSinglePrice) * intval($bookingData[0]->booking_guestAdultSingle)).".-</td>
                            </tr>
                            <tr>
                                <td>เด็กไม่เพิ่มเตียง</td>
                                <td>".number_format($bookingData[0]->booking_guestChildPrice).".-</td>
                                <td>".$bookingData[0]->booking_guestChild." ท่าน</td>
                                <td>".number_format(intval($bookingData[0]->booking_guestChildPrice) * intval($bookingData[0]->booking_guestChild)).".-</td>
                            </tr>
                            <tr>
                                <td>เด็กเพิ่มเตียง</td>
                                <td>".number_format($bookingData[0]->booking_guestChildBedPrice).".-</td>
                                <td>".$bookingData[0]->booking_guestChildBed." ท่าน</td>
                                <td>".number_format(intval($bookingData[0]->booking_guestChildBedPrice) * intval($bookingData[0]->booking_guestChildBed)).".-</td>
                            </tr>
                            <tr>
                                <td>รวมทั้งสิ้น</td>
                                <td></td>
                                <td></td>
                                <td>
                                    <h3 class=\"text-muted font-bold\">THB ".number_format($bookingData[0]->PaymentTransaction_totalPrice).".-<h3>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <hr>
                   ".$state."
                </div>
                ";
                break;
            case 10:
                $ItemsData = $this->items_models->GetItemReservationsData("i.items_isActive = 1 AND i.items_isPublish = 1 AND i.items_id = ".$bookingData[0]->Items_items_id,"EventTicket");
                $content["items_id"] = $bookingData[0]->Items_items_id;
                $content["user_id"] = $bookingData[0]->User_user_id;
                $content["orderDetails"] = "
                <div class=\"ibox-content\">
                    <h2 class=\"text-muted font-bold\">".$ItemsData[0]->itmes_topicThai."</h2>
                    <div class=\"small m-b-xs\">
                        <span class=\"text-muted\"><i class=\"fa fa-clock-o\">".$ItemsData[0]->provinces_thai."</i></span>
                        <span class=\"text-muted\"><i class=\"fa fa-clock-o\"></i> ".$ItemsData[0]->subcategory_thai."</span>
                    </div>
                    <p class=\"text-right text-navy\">".$this->session->userdata('business_nameThai')."</p>
                    <hr>
                    <h3 class=\"text-muted font-bold\">วันที่ใช้บริการ</h3>
                    <p class=\"\">".date("d/m/Y",strtotime($bookingData[0]->booking_checkIn))." เวลา ".date("H:i",strtotime($bookingData[0]->booking_checkIn." ".$bookingData[0]->booking_checkInTime))." - ".date("H:i",strtotime($bookingData[0]->booking_checkIn." ".$bookingData[0]->booking_checkOutTime))."</p>
                    <hr>
                    <h3 class=\"text-muted font-bold\">รายละเอียดผู้จอง</h3>
                    <p class=\"\">ชื่อผู้จอง : ".$bookingData[0]->paymentTransaction_firstName." ".$bookingData[0]->paymentTransaction_lastName."</p>
                    <p class=\"\">อีเมล : ".$bookingData[0]->paymentTransaction_email."</p>
                    <p class=\"\">เบอร์โทร :".$bookingData[0]->paymentTransaction_phone."</p>
                    <p class=\"\">คูปองส่วนลด : ไม่มี</p>
                    <hr>
                    <h3 class=\"text-muted font-bold\">จำนวนผู้เดินทาง</h3>
                    <table class=\"table table-hover\">
                        <thead>
                            <tr>
                                <th>ผู้เดินทาง</th>
                                <th>ราคา/ท่าน</th>
                                <th>จำนวน</th>
                                <th>ราคา(รวม)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>ผู้ใหญ่</td>
                                <td>".number_format($bookingData[0]->booking_guestAdultPrice).".-</td>
                                <td>".$bookingData[0]->booking_guestAdult." ท่าน</td>
                                <td>".number_format(intval($bookingData[0]->booking_guestAdultPrice) * intval($bookingData[0]->booking_guestAdult)).".-</td>
                            </tr>
                            <tr>
                                <td>เด็ก</td>
                                <td>".number_format($bookingData[0]->booking_guestChildPrice).".-</td>
                                <td>".$bookingData[0]->booking_guestChild." ท่าน</td>
                                <td>".number_format(intval($bookingData[0]->booking_guestChildPrice) * intval($bookingData[0]->booking_guestChild)).".-</td>
                            </tr>
                            <tr>
                                <td>รวมทั้งสิ้น</td>
                                <td></td>
                                <td></td>
                                <td>
                                    <h3 class=\"text-muted font-bold\">THB ".number_format($bookingData[0]->PaymentTransaction_totalPrice).".-<h3>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <hr>
                   ".$state."
                </div>
                ";
                break;
            case 11:
                $ItemsData = $this->items_models->GetItemReservationsData("i.items_isActive = 1 AND i.items_isPublish = 1 AND i.items_id = ".$bookingData[0]->Items_items_id,"CarRent");
                $BookingCondition ="";
                for($i=1;$i < count($ItemsData[0]->BookingCondition)-1; $i++)
                {
                    $BookingCondition .=" <p><i class=\"fa fa-check-circle text-info\"></i> ".$ItemsData[0]->BookingCondition[$i]->bookingConditioncol_detailThai."</p>";
                }
                $content["items_id"] = $bookingData[0]->Items_items_id;
                $content["user_id"] = $bookingData[0]->User_user_id;
                $content["orderDetails"] = "
                    <div class=\"ibox-content\">
                        <div class=\"chat-element\">
                            <div class=\"float-left\">
                                <img alt=\"image\" class=\"rounded img-lg\" src=\"/dasta_thailand/assets/img/uploadfile/".$ItemsData[0]->coverItem_paths."\">
                            </div>
                            <div class=\"media-body \">
                                <h3 class=\"text-muted\">".$ItemsData[0]->itmes_topicThai."</h3>
                                <p class=\"m-b-xs\">ชื่อลูกค้า : ".$bookingData[0]->paymentTransaction_firstName." ".$bookingData[0]->paymentTransaction_lastName."</p>
                                <p class=\"text-muted\">
                                    วันที่รับรถ : ".date("d/m/Y",strtotime($bookingData[0]->booking_checkIn))."
                                    <br> 
                                    วันที่คืนรถ : ".date("d/m/Y",strtotime($bookingData[0]->booking_checkOut))."
                                </p>
                                <span class=\"text-muted font-bold\"> ราคาต่อวัน : THB </span>
                                <span class=\"text-info\">".number_format($ItemsData[0]->items_PricePerDay)."</span> /วัน
                                <p class=\"text-navy\">".$this->session->userdata('business_nameThai')."</p>
                                <div class=\"ibox\">
                                    <a href=\"\" class=\"btn btn-primary btn-rounded float-right\">".$bookingData[0]->booking_duration." วัน</a>
                                    <h3 class=\"text-muted font-bold\">ราคารวม : THB ".number_format($bookingData[0]->PaymentTransaction_totalPrice)."</h3>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <h3 class=\"text-info font-bold\">รายละเอียดผู้จอง</h3>
                        <p class=\"\">ชื่อผู้จอง : ".$bookingData[0]->paymentTransaction_firstName." ".$bookingData[0]->paymentTransaction_lastName."</p>
                        <p class=\"\">อีเมล : ".$bookingData[0]->paymentTransaction_email."</p>
                        <p class=\"\">เบอร์โทร :".$bookingData[0]->paymentTransaction_phone."</p>
                        <p class=\"\">คูปองส่วนลด : ไม่มี</p>
                        <hr>
                        <h3 class=\"text-info font-bold\">ดูราคาแบบละเอียด</h3>
                        <table class=\"table table-hover\">
                            <tr>
                                <td>ค่าส่งรถ</td>
                                <td>THB ".number_format($ItemsData[0]->items_CarDeliveryPrice)."</td>
                            </tr>
                            <tr>
                                <td>ค่ารับรถ</td>
                                <td>THB ".number_format($ItemsData[0]->items_PickUpPrice)."</td>
                            </tr>
                            <tr>
                                <td>ค่าเช่ารถต่อวัน</td>
                                <td>THB ".number_format($ItemsData[0]->items_PricePerDay)."</td>
                            </tr>
                        </table>
                        <hr>
                        <h3 class=\"text-info font-bold\">ค่ามัดจำเพื่อประกันความเสียหาย</h3>
                        <p>".(intval($ItemsData[0]->items_isBasicInsurance) == 1?"<i class=\"fa fa-check-circle text-info\"></i> รวมประกันภัยพื้นฐาน":"<i class=\"fa fa-window-close text-danger\"></i> ไม่รวมประกันภัยพื้นฐาน")."</p>
                        <p class=\"\">".$ItemsData[0]->items_BissicInsurance_detailThai."</p>
                        <div class=\"ibox\" style=\"margin-bottom: 50px;\">
                            <h3 class=\"text-muted font-bold float-right\" >THB ".number_format($ItemsData[0]->items_DepositPrice)."</h3>
                        </div>
                        <hr>
                        <h3 class=\"text-info font-bold\">เงื่อนไขของน้ำมันรถ</h3>
                        <p class=\"\">".$ItemsData[0]->BookingCondition[0]->bookingConditioncol_detailThai."</p>
                        <hr>
                        <h3 class=\"text-info font-bold\">เอกสารสำหรับการเช่ารถ</h3>
                        ".$BookingCondition."
                        <hr>
                        ".$state."
                    </div>
                ";
                break;
        }
        $this->load->view('Entrepreneur/BookingOrderDetailsView',$content);
    }
    public function getDataForTable()
    {
        // Datatables Variables
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $data =array();
        $state_approv = $this->input->post("state_approv");

        if(intval($this->session->userdata('business_type_category_id')) != 1)
            $DefaultItemsData = $this->items_models->GetDefaultItemsData("i.items_isActive = 1 AND i.items_isPublish = 1 AND i.User_user_id = ".$this->session->userdata('entrepreneur_user_id'));
        else
            $DefaultItemsData = $this->program_tour_models->GetProgramTourData("pt.User_user_id = ".$this->session->userdata('entrepreneur_user_id'));

        switch (intval($this->session->userdata('business_type_category_id'))) {
            case 1:
                break;
            case 2:
                
                break;
            case 3:
                
                break;
            case 4:
                $bookingData = $this->booking_models->GetBookingDataOrderDetails("od.Items_items_id = ".$DefaultItemsData[0]->items_id." AND pt.PurchaseStatus_purchaseStatus_id = ".$state_approv);
                for($j=0;$j<count($bookingData);$j++)
                {
                    $state ="";
                    switch (intval($bookingData[$j]->PurchaseStatus_purchaseStatus_id)) 
                    {
                        case 2:
                            $state ="<p><span class=\"badge badge-warning\" >รอการอนุมัติ</span><p>";
                            $state .="
                                <div class=\"text-right\">
                                    <button class=\"btn btn-info btn-rounded font-bold btn-booking-approve\" value=\"".$bookingData[$j]->paymentTransaction_id."\">ยอมรับ</button>
                                    <button class=\"btn btn-danger btn-rounded font-bold btn-booking-disapprove\" value=\"".$bookingData[$j]->paymentTransaction_id."\">ปฏิเสธ</button>
                                </div>";
                        break;
                        case 1:
                            $state ="<p><span class=\"badge badge-primary\" >อนุมัติแล้ว</span><p>";   
                        break;
                        case 3;
                            $state ="<p><span class=\"badge badge-danger\" >ไม่ผ่านการอนุมัติ</span><p>";
                        break;
                    }
                    $data[] = array(
                        $bookingData[$j]->paymentTransaction_id,
                        "
                        
                            <div class=\"ibox\">
                                <div class=\"ibox-content\">
                                    <a href=\"./BookingOrder/OrderDetails?booking_id=".$bookingData[$j]->paymentTransaction_id."\">
                                        <div class=\"chat-element\">
                                            <div class=\"float-left\">
                                                <img alt=\"image\" class=\"rounded img-lg\" src=\"/dasta_thailand/assets/img/pin.png\">
                                            </div>
                                            <div class=\"media-body \">
                                                <h2 class=\"text-muted font-bold\">".$bookingData[$j]->quality." ".$this->lang->line("list")."</h2>
                                                <h2 class=\"m-b-xs \">
                                                <h3 class=\"m-b-xs text-muted font-normal\">
                                                    ชื่อลูกค้า : ".$bookingData[$j]->paymentTransaction_firstName." ".$bookingData[$j]->paymentTransaction_lastName."
                                                </h3>
                                                <h2 class=\"text-info font-normal\"><br>6,100.-</h2>
                                                </a>
                                                ".$state."
                                            </div>
                                        </div>
                                    
                                </div>
                            </div>
                        
                        "
                    );

                }
                break;
            case 5:
                $bookingData = $this->booking_models->GetBookingDataPaymentTransaction("b.Items_items_id = ".$DefaultItemsData[0]->items_id." AND pt.PurchaseStatus_purchaseStatus_id = ".$state_approv);
                for($j=0;$j<count($bookingData);$j++)
                {
                    $state ="";
                    switch (intval($bookingData[$j]->PurchaseStatus_purchaseStatus_id)) 
                    {
                        case 2:
                            $state ="<p><span class=\"badge badge-warning\" >รอการอนุมัติ</span><p>";
                            $state .="
                                <div class=\"text-right\">
                                    <button class=\"btn btn-info btn-rounded font-bold btn-booking-approve\" value=\"".$bookingData[$j]->paymentTransaction_id."\">ยอมรับ</button>
                                    <button class=\"btn btn-danger btn-rounded font-bold btn-booking-disapprove\" value=\"".$bookingData[$j]->paymentTransaction_id."\">ปฏิเสธ</button>
                                </div>";
                        break;
                        case 1:
                            $state ="<p><span class=\"badge badge-primary\" >อนุมัติแล้ว</span><p>";   
                        break;
                        case 3;
                            $state ="<p><span class=\"badge badge-danger\" >ไม่ผ่านการอนุมัติ</span><p>";
                        break;
                    }
                    $RoomData = $this->items_models->GetItemDataRoom("room_id = ".$bookingData[0]->Room_room_id);
                    $data[] = array(
                        $bookingData[$j]->booking_id,
                        "
                        <div class=\"ibox\">
                            <div>
                                <a href=\"./BookingOrder/OrderDetails?booking_id=".$bookingData[$j]->booking_id."\">
                                    <div class=\"ibox-content no-padding border-left-right\">
                                        <img class=\"d-block w-100\" src=\"/dasta_thailand/assets/img/uploadfile/".$RoomData[0]->PhotoRoom[0]->pictureRoom_paths."\" alt=\"First slide\">
                                    </div>
                               
                                    <div class=\"ibox-content-hotel\">
                                        <h4>
                                            <strong class=\"text-white\">".$DefaultItemsData[0]->itmes_topicThai."</strong>
                                        </h4>
                                        <p style=\"color:#FF7745;\">โรงแรม</p>
                                        <div class=\"row vertical-align\">
                                            <div class=\"col-7\">
                                                <h1 class=\"font-bold no-margins text-white\">".$RoomData[0]->room_topicThai."</h1>
                                            </div>
                                            <div class=\"col-5 text-right\">
                                                <h1 class=\"font-bold no-margins text-white\">THB ".$RoomData[0]->room_price."</h1>
                                                <h6 class=\"font-normal no-margins text-white\">ราคา/ต่อคืน รวมภาษีแล้ว </h6>
                                            </div>
                                        </div>
                                        <hr class=\"hr-line-dashed\">
                                        <p class=\"text-white\">ชื่อลูกค้า : ".$bookingData[$j]->paymentTransaction_firstName." ".$bookingData[$j]->paymentTransaction_lastName." </p>
                                        <p class=\"text-white\">ช่วงเวลาการจอง : ".date("d/m/Y",strtotime($bookingData[$j]->booking_checkIn))." - ".date("d/m/Y",strtotime($bookingData[$j]->booking_checkOut))."</p>
                                        <p class=\"text-white\">ราคารวม : THB ".number_format($bookingData[$j]->PaymentTransaction_totalPrice)."</p>
                                        </a>
										
                                        <hr class=\"hr-line-dashed\">
                                        ".$state."
										
                                        <br>
                                    </div>
									
                            </div>
                        </div>
                        ");
                }
                break;
            case 9:
                for($i=0;$i<count($DefaultItemsData);$i++)
                {
                    $bookingData = $this->booking_models->GetBookingDataPaymentTransaction("b.Items_items_id = ".$DefaultItemsData[$i]->items_id." AND pt.PurchaseStatus_purchaseStatus_id = ".$state_approv);
                   
                    for($j=0;$j<count($bookingData);$j++)
                    {
                        $state ="";
                        $state2 ="";
                        switch (intval($bookingData[$j]->PurchaseStatus_purchaseStatus_id)) 
                        {
                            case 2:
                                $state ="<p><span class=\"badge badge-warning\" >รอการอนุมัติ</span><p>";
                                $state2 ="
                                    <div class=\"col-9 text-right\">
                                        <button class=\"btn btn-info btn-rounded font-bold btn-booking-approve\" value=\"".$bookingData[$j]->paymentTransaction_id."\">ยอมรับ</button>
                                        <button class=\"btn btn-danger btn-rounded font-bold btn-booking-disapprove\" value=\"".$bookingData[$j]->paymentTransaction_id."\">ปฏิเสธ</button>
                                    </div>";
                            break;
                            case 1:
                                $state ="<p><span class=\"badge badge-primary\" >อนุมัติแล้ว</span><p>";   
                            break;
                            case 3;
                                $state ="<p><span class=\"badge badge-danger\" >ไม่ผ่านการอนุมัติ</span><p>";
                            break;
                        }
                        $ItemsData = $this->items_models->GetItemReservationsData("i.items_isActive = 1 AND i.items_isPublish = 1 AND i.items_id = ".$bookingData[$j]->Items_items_id,"PackageTours");
                        $data[] = array(
                            $bookingData[$j]->booking_id,
                            "
                            <div class=\"ibox \">
                                <div>
                                    <a href=\"./BookingOrder/OrderDetails?booking_id=".$bookingData[$j]->booking_id."\" style=\"color: #676a6c;\">
                                        <div class=\"ibox-content no-padding border-left-right\">
                                            <img class=\"d-block w-100\" src=\"/dasta_thailand/assets/img/uploadfile/".$ItemsData[0]->CoverItems[0]->coverItem_paths."\" alt=\"First slide\">
                                        </div>
                                    
                                        <div class=\"ibox-content profile-content \">
                                            <h4>
                                                <strong>".$ItemsData[0]->itmes_topicThai."</strong>
                                            </h4>
                                            <p>ชื่อลูกค้า : ".$bookingData[$j]->paymentTransaction_firstName." ".$bookingData[$j]->paymentTransaction_lastName." </p>
                                            <p>วันที่ทำการจองเข้ามา : ".date("d/m/Y",strtotime($bookingData[$j]->booking_createdDTTM))."</p>
                                            <p>ระยะวันแพคเกจทัวร์ :
                                                <span class=\"label label-primary\">".count($ItemsData[0]->TravelPeriod)." วัน</span>
                                            </p>
                                            </a>
                                            <div class=\"row vertical-align\">
                                                <div class=\"col-3\">
                                                    <h1 class=\"font-bold no-margins text-info\">".number_format($bookingData[$j]->PaymentTransaction_totalPrice).".-</h1>
                                                    ".$state."
                                                </div>
                                               ".$state2."
                                            </div>
                                        </div>
                                   
                                </div>
                            </div>
                        ");
                    }
                }
                break;
            case 10:
                for($i=0;$i<count($DefaultItemsData);$i++)
                {
                    $bookingData = $this->booking_models->GetBookingDataPaymentTransaction("b.Items_items_id = ".$DefaultItemsData[$i]->items_id." AND pt.PurchaseStatus_purchaseStatus_id = ".$state_approv);
                   
                    for($j=0;$j<count($bookingData);$j++)
                    {
                        $state ="";
                        $state2 ="";
                        switch (intval($bookingData[$j]->PurchaseStatus_purchaseStatus_id)) 
                        {
                            case 2:
                                $state ="<p><span class=\"badge badge-warning\" >รอการอนุมัติ</span><p>";
                                $state2 ="
                                    <div class=\"col-9 text-right\">
                                        <button class=\"btn btn-info btn-rounded font-bold btn-booking-approve\" value=\"".$bookingData[$j]->paymentTransaction_id."\">ยอมรับ</button>
                                        <button class=\"btn btn-danger btn-rounded font-bold btn-booking-disapprove\" value=\"".$bookingData[$j]->paymentTransaction_id."\">ปฏิเสธ</button>
                                    </div>";
                            break;
                            case 1:
                                $state ="<p><span class=\"badge badge-primary\" >อนุมัติแล้ว</span><p>";   
                            break;
                            case 3;
                                $state ="<p><span class=\"badge badge-danger\" >ไม่ผ่านการอนุมัติ</span><p>";
                            break;
                        }
                        $ItemsData = $this->items_models->GetItemReservationsData("i.items_isActive = 1 AND i.items_isPublish = 1 AND i.items_id = ".$bookingData[$j]->Items_items_id,"EventTicket");
                        $data[] = array(
                            $bookingData[$j]->booking_id,
                            "
                            <div class=\"ibox \">
                                <div>
                                    <a href=\"./BookingOrder/OrderDetails?booking_id=".$bookingData[$j]->booking_id."\" style=\"color: #676a6c;\">
                                        <div class=\"ibox-content no-padding border-left-right\">
                                            <img class=\"d-block w-100\" src=\"/dasta_thailand/assets/img/uploadfile/".$ItemsData[0]->CoverItems[0]->coverItem_paths."\" alt=\"First slide\">
                                        </div>
                                    
                                        <div class=\"ibox-content profile-content \">
                                            <h4>
                                                <strong>".$ItemsData[0]->itmes_topicThai."</strong>
                                            </h4>
                                            <p>ชื่อลูกค้า : ".$bookingData[$j]->paymentTransaction_firstName." ".$bookingData[$j]->paymentTransaction_lastName." </p>
                                            <p>ช่วงเวลาในการใช้งานตั๋ว : ".date("d/m/Y",strtotime($bookingData[$j]->booking_checkIn))." เวลา ".date("H:i",strtotime($bookingData[$j]->booking_checkIn." ".$bookingData[$j]->booking_checkInTime))." - ".date("H:i",strtotime($bookingData[$j]->booking_checkIn." ".$bookingData[$j]->booking_checkOutTime))." น.</p>
                                            </a>
                                            <div class=\"row vertical-align\">
                                                <div class=\"col-3\">
                                                    <h1 class=\"font-bold no-margins text-info\">".number_format($bookingData[$j]->PaymentTransaction_totalPrice).".-</h1>
                                                    ".$state."
                                                </div>
                                               ". $state2."
                                            </div>
                                        </div>
                                   
                                </div>
                            </div>
                        ");
                    }
                }
                break;
            case 11:
                for($i=0;$i<count($DefaultItemsData);$i++)
                {
                    $bookingData = $this->booking_models->GetBookingDataPaymentTransaction("b.Items_items_id = ".$DefaultItemsData[$i]->items_id." AND pt.PurchaseStatus_purchaseStatus_id = ".$state_approv);
                   
                    for($j=0;$j<count($bookingData);$j++)
                    {
                        $state ="";
                        switch (intval($bookingData[$j]->PurchaseStatus_purchaseStatus_id)) 
                        {
                            case 2:
                                $state ="<p><span class=\"badge badge-warning\" >รอการอนุมัติ</span><p>";
                                $state .="
                                    <div class=\"text-right\">
                                        <button class=\"btn btn-info btn-rounded font-bold btn-booking-approve\" value=\"".$bookingData[$j]->paymentTransaction_id."\">ยอมรับ</button>
                                        <button class=\"btn btn-danger btn-rounded font-bold btn-booking-disapprove\" value=\"".$bookingData[$j]->paymentTransaction_id."\">ปฏิเสธ</button>
                                    </div>";
                            break;
                            case 1:
                                $state ="<p><span class=\"badge badge-primary\" >อนุมัติแล้ว</span><p>";   
                            break;
                            case 3;
                                $state ="<p><span class=\"badge badge-danger\" >ไม่ผ่านการอนุมัติ</span><p>";
                            break;
                        }
                        $ItemsData = $this->items_models->GetItemReservationsData("i.items_isActive = 1 AND i.items_isPublish = 1 AND i.items_id = ".$bookingData[$j]->Items_items_id,"CarRent");
                        $data[] = array(
                            $bookingData[$j]->booking_id,
                            "
                            <div class=\"chat-element\">
                                <a href=\"./BookingOrder/OrderDetails?booking_id=".$bookingData[$j]->booking_id."\" style=\"color: #676a6c;\">
                                    <div class=\"float-left\" style=\"margin-right: 10px;\">
                                        <img alt=\"image\" class=\"rounded img-lg\" src=\"/dasta_thailand/assets/img/uploadfile/".$ItemsData[0]->coverItem_paths."\">
                                    </div>
                                    <div class=\"media-body \">
                                        <h3 class=\"text-muted\">".$ItemsData[$i]->itmes_topicThai."</h3>
                                        <p class=\"m-b-xs\">ชื่อลูกค้า : ".$bookingData[$j]->paymentTransaction_firstName." ".$bookingData[$j]->paymentTransaction_lastName."</p>
                                        <p class=\"text-muted\">วันที่รับรถ : ".date("d/m/Y",strtotime($bookingData[$j]->booking_checkIn))."<br>วันที่คืนรถ : ".date("d/m/Y",strtotime($bookingData[$j]->booking_checkOut))."</p>
                                        <span class=\"label label-primary\">".$bookingData[$j]->booking_duration." วัน</span>
                                        <span class=\"text-muted font-bold\"> ราคาต่อวัน : THB </span>
                                        <span class=\"text-info\">".number_format($ItemsData[$i]->items_PricePerDay)."</span> /วัน
                                        <h3 class=\"text-muted font-normal\"><br>ราคารวม : THB ".number_format($bookingData[$j]->PaymentTransaction_totalPrice)."</h3>
                                        </a>
                                        ".$state."
                                    </div>
                                
                            </div>     
                            ",
                        );
                    }
                }
                break;
            default:
           
                break;
        }   

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($data),
            "recordsFiltered" =>count($data),
            "data" => $data
        );
        echo json_encode($output);
    }
    public function ApprovOrder()
    {
        $booking = array(
            "booking_updatedDTTM" => date("Y-m-d H:i:s")
        );
        $payment_transaction = array(
            "PurchaseStatus_purchaseStatus_id" => 1
        );
        $trans_status = $this->payment_transaction_models->UpdatePaymentTransactionData($payment_transaction,"paymentTransaction_id = ".$this->input->post("paymentTransaction_id"));
        $trans_status =  $this->booking_models->UpdateBookingData($booking,"PaymentTransaction_paymentTransaction_id = ".$this->input->post("paymentTransaction_id"));
    } 
    public function DisApprovOrder()
    {
        $booking = array(
            "booking_updatedDTTM" => date("Y-m-d H:i:s"),
        );
        $payment_transaction = array(
            "PurchaseStatus_purchaseStatus_id" => 3,
        );
        $trans_status = $this->payment_transaction_models->UpdatePaymentTransactionData($payment_transaction,"paymentTransaction_id = ".$this->input->post("paymentTransaction_id"));
        $trans_status = $this->booking_models->UpdateBookingData($booking,"PaymentTransaction_paymentTransaction_id = ".$this->input->post("paymentTransaction_id"));
    }
}