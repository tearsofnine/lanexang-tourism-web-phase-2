<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
class CarRent extends Base_Entrepreneur_Controller {
    public function __construct()
    {    	
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');

        $this->load->library('form_validation');
        $config['upload_path'] = './assets/img/uploadfile/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = 2048; 
        $config['encrypt_name'] = true; 
        $this->load->library('upload', $config);

        $this->load->model("CountriesModels","countries_models", true);
        $this->load->model("UserModel","user_model", true);
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("SearchModels","search_models", true);
        $this->load->model("ChatModels","chat_models", true);

        if(strcmp($this->session->userdata('lang'),"EN") == 0)
            $this->lang->load("english","english");
        else
            $this->lang->load("thailand","thailand");
    }
    public function index()
	{
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $content['ModalView'] = "CarRent";
        $item = $this->items_models->GetDefaultItemsData("i.User_user_id = ".$this->session->userdata('entrepreneur_user_id'));

        if(count($item) != 0 && intval($item[0]->MenuItem_menuItem_id) == 11)
        {
            $content['data_item'] = $this->items_models->GetItemReservationsData("i.items_id = ".$item[0]->items_id." AND mi.menuItem_id = 11","CarRent");

            $this->load->view('Entrepreneur/Information/BusinessInformationCarRentView',$content);
        }
        else
        {
            redirect('../Main');
        }
        
    }
    public function addCarRent()
    {
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $this->load->view('Entrepreneur/BusinessForm/CarRentAddView',$content);
    }
    public function editCarRent()
    {
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;

        $content['data_item'] = $this->items_models->GetItemReservationsData("i.items_id = ".$this->input->get("item_id")." AND mi.menuItem_id = 11","CarRent");

        if(count($content['data_item']) != 0)
            $this->load->view('Entrepreneur/BusinessForm/CarRentEditView',$content);
        else
            redirect('../CarRent');
    }
    public function saveNewItem()
    {
        $this->load->library('form_validation');
        
        $config['upload_path'] = './assets/img/uploadfile/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        //$config['max_size'] = 2048; 
        $config['encrypt_name'] = true; 
        $this->load->library('upload', $config);

        $this->form_validation->set_rules('itmes_topicThai', 'required|itmes_topicThai|itmes_topicThai|กรุณากรอกชื่อรถ', 'trim|required');
        $this->form_validation->set_rules('TypesofCars_typesofCars_id', 'required|TypesofCars_typesofCars_id|TypesofCars_typesofCars_id|กรุณาเลือกประเภทรถ', 'trim|required');
        $this->form_validation->set_rules('GearSystem_gearSystem_id', 'required|GearSystem_gearSystem_id|GearSystem_gearSystem_id|กรุณาเลือกประเภทเกียร์', 'trim|required');
        $this->form_validation->set_rules('items_carSeats', 'required|items_carSeats|items_carSeats|กรุณาระบุจำนวนที่นั่ง', 'trim|required');
        $this->form_validation->set_rules('items_PricePerDay', 'required|items_PricePerDay|items_PricePerDay|กรุณาระบุราคาต่อวัน', 'trim|required');
        $this->form_validation->set_rules('items_CarDeliveryPrice', 'required|items_CarDeliveryPrice|items_CarDeliveryPrice|กรุณาระบุราคาค่าส่งรถ', 'trim|required');
        $this->form_validation->set_rules('items_PickUpPrice', 'required|items_PickUpPrice|items_PickUpPrice|กรุณาระบุราคาค่ารับรถ', 'trim|required');

        $BookingCondition = json_decode($this->input->post("BookingCondition_14"));
        for($i = 0;$i<count($BookingCondition);$i++)
        {
            if($BookingCondition[$i]->bookingConditioncol_14_detailThai == "")
                $this->form_validation->set_rules('bookingConditioncol_detailThai_id_14_'.($i+1), 'required|bookingConditioncol_detailThai_id_14_'.($i+1).'|bookingConditioncol_detailThai_id_14_'.($i+1).'|กรุณากรอกรายละเอียดเอกสารสำหรับการเช่ารถ', 'trim|required');
        }

        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {  
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Items
            $Items = array(
                "itmes_topicThai" =>$this->input->post("itmes_topicThai"),
                "itmes_topicEnglish" =>$this->input->post("itmes_topicEnglish"),
                "itmes_topicLaos" =>$this->input->post("itmes_topicLaos"),
                "itmes_topicChinese" =>$this->input->post("itmes_topicChinese"),

                "items_carSeats" =>$this->input->post("items_carSeats"),
                "items_PricePerDay" =>$this->input->post("items_PricePerDay"),
                "items_CarDeliveryPrice" =>$this->input->post("items_CarDeliveryPrice"),
                "items_PickUpPrice" =>$this->input->post("items_PickUpPrice"),
                "items_DepositPrice" =>$this->input->post("items_DepositPrice"),
                "items_isBasicInsurance" =>$this->input->post("items_isBasicInsurance"),

                "items_BissicInsurance_detailThai" =>$this->input->post("items_BissicInsurance_detailThai"),
                "items_BissicInsurance_detailEnglish" =>$this->input->post("items_BissicInsurance_detailEnglish"),
                "items_BissicInsurance_detailLaos" =>$this->input->post("items_BissicInsurance_detailLaos"),
                "items_BissicInsurance_detailChinese" =>$this->input->post("items_BissicInsurance_detailChinese"),

                "MenuItem_menuItem_id" =>11,
                "User_user_id" => $this->session->userdata('entrepreneur_user_id'),
                "items_createdDTTM" => date("Y-m-d H:i:s"),
                "items_isActive" => 0,
                "items_isPublish" => 0,
            );
            $inserted_id_item = $this->items_models->AddItemData($Items,"Items");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $Items_has_TypesofCars = array(
                "TypesofCars_typesofCars_id" =>$this->input->post("TypesofCars_typesofCars_id"),
                "GearSystem_gearSystem_id" =>$this->input->post("GearSystem_gearSystem_id"),
                "Items_items_id" =>$inserted_id_item,
            );
            $this->items_models->AddItemData($Items_has_TypesofCars,"Items_has_TypesofCars");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // CoverItems
            $this->upload->do_upload('cropperImage');
            $upload_data = $this->upload->data();
            $CoverItems = array(
                "coverItem_paths" =>$upload_data["file_name"],
                "coverItem_url" =>$this->input->post("coverItem_url"),
                "Items_items_id" =>$inserted_id_item, 
            );
            $inserted_cover_item_id_item = $this->items_models->AddItemData($CoverItems,"CoverItems");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if(empty($_FILES['originalImage']['name'][0]))
            {
                $originalImage = "p3.jpg";
            }
            else
            {
                $this->upload->do_upload('originalImage');
                $upload_data = $this->upload->data();
                $originalImage = $upload_data["file_name"];
            }
            $TempImageCover = array(
                "tempImageCover_paths" =>$originalImage,
                "tempImageCover_CropData" =>$this->input->post("CropData"),
                "tempImageCover_CropBoxData" =>$this->input->post("CropBoxData"),
                "tempImageCover_CanvasData" =>$this->input->post("CanvasData"),
                
                "CoverItems_coverItem_id" =>$inserted_cover_item_id_item,
            );
            $this->items_models->AddItemData($TempImageCover,"TempImageCover");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// bookingCondition13
            $bookingConditioncol_13 = array(
                "bookingConditioncol_detailThai" => $this->input->post("bookingConditioncol_detailThai_13"),
                "bookingConditioncol_detailEnglish" => $this->input->post("bookingConditioncol_detailEnglish_13"),
                "bookingConditioncol_detailLaos" => $this->input->post("bookingConditioncol_detailLaos_13"),
                "bookingConditioncol_detailChinese" => $this->input->post("bookingConditioncol_detailChinese_13"),
                "Items_items_id" => $inserted_id_item,
                "BookingConditionCategory_bookingConditionCategory_id" => 13
            );
            $this->items_models->AddItemData($bookingConditioncol_13,"BookingCondition");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// bookingCondition14
            $BookingCondition_14 = json_decode($this->input->post("BookingCondition_14"));
            for($i = 0;$i< count($BookingCondition_14);$i++)
            {
                $bookingConditioncol_detail = array(
                    "bookingConditioncol_detailThai" => $BookingCondition_14[$i]->bookingConditioncol_14_detailThai,
                    "bookingConditioncol_detailEnglish" => $BookingCondition_14[$i]->bookingConditioncol_14_detailEnglish,
                    "bookingConditioncol_detailLaos" => $BookingCondition_14[$i]->bookingConditioncol_14_detailLaos,
                    "bookingConditioncol_detailChinese" => $BookingCondition_14[$i]->bookingConditioncol_14_detailChinese,
                    "Items_items_id" => $inserted_id_item,
                    "BookingConditionCategory_bookingConditionCategory_id" => 14
                );
                $this->items_models->AddItemData($bookingConditioncol_detail,"BookingCondition");
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $res = array('state' => true,'error'=>'', 'msg' => 'เพิ่มสำเร็จ');
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }
    public function saveEditItem()
    {
        $this->load->library('form_validation');
        
        $config['upload_path'] = './assets/img/uploadfile/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        //$config['max_size'] = 2048; 
        $config['encrypt_name'] = true; 
        $this->load->library('upload', $config);

        $this->form_validation->set_rules('itmes_topicThai', 'required|itmes_topicThai|itmes_topicThai|กรุณากรอกชื่อรถ', 'trim|required');
        $this->form_validation->set_rules('TypesofCars_typesofCars_id', 'required|TypesofCars_typesofCars_id|TypesofCars_typesofCars_id|กรุณาเลือกประเภทรถ', 'trim|required');
        $this->form_validation->set_rules('GearSystem_gearSystem_id', 'required|GearSystem_gearSystem_id|GearSystem_gearSystem_id|กรุณาเลือกประเภทเกียร์', 'trim|required');
        $this->form_validation->set_rules('items_carSeats', 'required|items_carSeats|items_carSeats|กรุณาระบุจำนวนที่นั่ง', 'trim|required');
        $this->form_validation->set_rules('items_PricePerDay', 'required|items_PricePerDay|items_PricePerDay|กรุณาระบุราคาต่อวัน', 'trim|required');
        $this->form_validation->set_rules('items_CarDeliveryPrice', 'required|items_CarDeliveryPrice|items_CarDeliveryPrice|กรุณาระบุราคาค่าส่งรถ', 'trim|required');
        $this->form_validation->set_rules('items_PickUpPrice', 'required|items_PickUpPrice|items_PickUpPrice|กรุณาระบุราคาค่ารับรถ', 'trim|required');

        $BookingCondition = json_decode($this->input->post("BookingCondition_14"));
        for($i = 0;$i<count($BookingCondition);$i++)
        {
            if($BookingCondition[$i]->bookingConditioncol_14_detailThai == "")
                $this->form_validation->set_rules('bookingConditioncol_detailThai_id_14_'.($i+1), 'required|bookingConditioncol_detailThai_id_14_'.($i+1).'|bookingConditioncol_detailThai_id_14_'.($i+1).'|กรุณากรอกรายละเอียดเอกสารสำหรับการเช่ารถ', 'trim|required');
        }

        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {  
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Items
            $items_models_data = $this->items_models->GetItemReservationsData("i.items_id = ".$this->input->post("items_id"),"CarRent");
            $Items = array(
                "itmes_topicThai" =>$this->input->post("itmes_topicThai"),
                "itmes_topicEnglish" =>$this->input->post("itmes_topicEnglish"),
                "itmes_topicLaos" =>$this->input->post("itmes_topicLaos"),
                "itmes_topicChinese" =>$this->input->post("itmes_topicChinese"),

                "items_carSeats" =>$this->input->post("items_carSeats"),
                "items_PricePerDay" =>$this->input->post("items_PricePerDay"),
                "items_CarDeliveryPrice" =>$this->input->post("items_CarDeliveryPrice"),
                "items_PickUpPrice" =>$this->input->post("items_PickUpPrice"),
                "items_DepositPrice" =>$this->input->post("items_DepositPrice"),
                "items_isBasicInsurance" =>$this->input->post("items_isBasicInsurance"),

                "items_BissicInsurance_detailThai" =>$this->input->post("items_BissicInsurance_detailThai"),
                "items_BissicInsurance_detailEnglish" =>$this->input->post("items_BissicInsurance_detailEnglish"),
                "items_BissicInsurance_detailLaos" =>$this->input->post("items_BissicInsurance_detailLaos"),
                "items_BissicInsurance_detailChinese" =>$this->input->post("items_BissicInsurance_detailChinese"),

                "MenuItem_menuItem_id" =>11,
                "items_updatedDTTM" => date("Y-m-d H:i:s"),
                "items_isActive" => 0,
                "items_isPublish" => 0,
                
            );
            $trans_status = $this->items_models->UpdateItemData($Items,"Items","items_id = ".$this->input->post("items_id"));

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $Items_has_TypesofCars = array(
                "TypesofCars_typesofCars_id" =>$this->input->post("TypesofCars_typesofCars_id"),
                "GearSystem_gearSystem_id" =>$this->input->post("GearSystem_gearSystem_id"),
            );
            $trans_status = $this->items_models->UpdateItemData($Items_has_TypesofCars,"Items_has_TypesofCars","Items_items_id = ".$this->input->post("items_id"));
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CoverItems
            $coverItem_paths = './assets/img/uploadfile/'. $items_models_data[0]->coverItem_paths;
            $tempImageCover_paths = './assets/img/uploadfile/'. $items_models_data[0]->tempImageCover_paths;
            //if (is_readable($coverItem_paths) && unlink($coverItem_paths) && is_readable($tempImageCover_paths) && unlink($tempImageCover_paths)) 
            //{
                is_readable($coverItem_paths);
                unlink($coverItem_paths);
                if($items_models_data[0]->tempImageCover_paths != "p3.jpg")
                {
                    is_readable($tempImageCover_paths);
                    unlink($tempImageCover_paths);
                }
               
                $trans_status = $this->items_models->DeleteItemData("CoverItems","Items_items_id = ".$this->input->post("items_id"));
               
                $this->upload->do_upload('cropperImage');
                $upload_data = $this->upload->data();
                $CoverItems = array(
                    "coverItem_paths" =>$upload_data["file_name"],
                    "coverItem_url" =>$this->input->post("coverItem_url"),
                    "Items_items_id" =>$this->input->post("items_id"), 
                );
                $inserted_cover_item_id_item = $this->items_models->AddItemData($CoverItems,"CoverItems");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                $this->upload->do_upload('originalImage');
                $upload_data = $this->upload->data();
                $originalImage = $upload_data["file_name"];
                
                $TempImageCover = array(
                    "tempImageCover_paths" =>$originalImage,
                    "tempImageCover_CropData" =>$this->input->post("CropData"),
                    "tempImageCover_CropBoxData" =>$this->input->post("CropBoxData"),
                    "tempImageCover_CanvasData" =>$this->input->post("CanvasData"),
                    
                    "CoverItems_coverItem_id" =>$inserted_cover_item_id_item,
                );
                $this->items_models->AddItemData($TempImageCover,"TempImageCover");
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
            $trans_status = $this->items_models->DeleteItemData("BookingCondition","Items_items_id = ".$this->input->post("items_id"));
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// bookingCondition13   
            $bookingConditioncol_13 = array(
                "bookingConditioncol_detailThai" => $this->input->post("bookingConditioncol_detailThai_13"),
                "bookingConditioncol_detailEnglish" => $this->input->post("bookingConditioncol_detailEnglish_13"),
                "bookingConditioncol_detailLaos" => $this->input->post("bookingConditioncol_detailLaos_13"),
                "bookingConditioncol_detailChinese" => $this->input->post("bookingConditioncol_detailChinese_13"),
                "Items_items_id" => $this->input->post("items_id"),
                "BookingConditionCategory_bookingConditionCategory_id" => 13
            );
            $this->items_models->AddItemData($bookingConditioncol_13,"BookingCondition"); 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// bookingCondition14
            $BookingCondition_14 = json_decode($this->input->post("BookingCondition_14"));
            for($i = 0;$i< count($BookingCondition_14);$i++)
            {
                $bookingConditioncol_detail = array(
                    "bookingConditioncol_detailThai" => $BookingCondition_14[$i]->bookingConditioncol_14_detailThai,
                    "bookingConditioncol_detailEnglish" => $BookingCondition_14[$i]->bookingConditioncol_14_detailEnglish,
                    "bookingConditioncol_detailLaos" => $BookingCondition_14[$i]->bookingConditioncol_14_detailLaos,
                    "bookingConditioncol_detailChinese" => $BookingCondition_14[$i]->bookingConditioncol_14_detailChinese,
                    "Items_items_id" => $this->input->post("items_id"),
                    "BookingConditionCategory_bookingConditionCategory_id" => 14
                );
                $this->items_models->AddItemData($bookingConditioncol_detail,"BookingCondition");
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $res = array('state' => true,'error'=>'', 'msg' => 'เพิ่มสำเร็จ');
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }
    public function getNameTypesofCars()
    {
        $TypesofCars_typesofCars_id = $this->items_models->GetItemSubData("TypesofCars","");
        echo "<option value=\"\">ประเภทรถ</option>";
        
        foreach ($TypesofCars_typesofCars_id as $item)
        {
            echo "<option value=\"".$item->typesofCars_id."\">".$item->typesofCars_Thai."</option>";
        }
    } 
    public function getNameGearSystem()
    {
        $GearSystem_gearSystem_id = $this->items_models->GetItemSubData("GearSystem","");
        echo "<option value=\"\">ประเภทเกียร์</option>";
        
        foreach ($GearSystem_gearSystem_id as $item)
        {
            echo "<option value=\"".$item->gearSystem_id."\">".$item->gearSystem_Thai."</option>";
        }
    }
    public function getDataCarRentForTable()
    {
        // Datatables Variables
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));
        
        // $data = $this->items_models->setDatatableReservations("mi.menuItem_id = 11 AND i.User_user_id = ".$this->input->post("user_id"),"CarRent");
        $data = $this->search_models->getDataSearchForTable(
            "CarRent",
            $this->input->post("typesofCars_id"),
            $this->input->post("gearSystem_id"),
            "",
            "",
            "i.items_isActive != 99 and mi.menuItem_id = 11 AND i.User_user_id = ".$this->input->post("user_id")
        );

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($data),
            "recordsFiltered" =>count($data),
            "data" => $data
        );
        echo json_encode($output);
    }
    public function getCarRent()
    {
        return $this->output->set_content_type('application/json')->set_output(json_encode($this->items_models->GetItemReservationsData("mi.menuItem_id = 11","CarRent")));
        //print_r($this->items_models->GetItemData());
    }
}