<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
class Restaurant extends Base_Entrepreneur_Controller {
    public function __construct()
    {    	
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');

        $this->load->library('form_validation');
        $config['upload_path'] = './assets/img/uploadfile/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = 2048; 
        $config['encrypt_name'] = true; 
        $this->load->library('upload', $config);

        $this->load->model("CountriesModels","countries_models", true);
        $this->load->model("UserModel","user_model", true);
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("SearchModels","search_models", true);
        $this->load->model("ChatModels","chat_models", true);

        if(strcmp($this->session->userdata('lang'),"EN") == 0)
            $this->lang->load("english","english");
        else
            $this->lang->load("thailand","thailand");
    }
    public function index()
	{
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;

        $item = $this->items_models->GetItemData("i.User_user_id = ".$this->session->userdata('entrepreneur_user_id'));

        if(count($item) != 0 && intval($item[0]->MenuItem_menuItem_id) == 3)
        {
            $ItemDataProduct = $this->items_models->GetItemDataProduct("Items_items_id = ". $item[0]->items_id );
            $content['data_item'] = $item;
            $content['data_product'] = $ItemDataProduct;
            $content['ModalView'] = "Restaurant";
            $this->load->view('Entrepreneur/Information/BusinessInformationRestaurantView',$content);
        }
        else
        {
            redirect('../Main');
        }
        
    }
    public function addRestaurant()
    {
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $this->load->view('Entrepreneur/BusinessForm/RestaurantAddView',$content);
    }
    public function editRestaurant()
    {
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $content['data_item'] = $this->items_models->GetEditItem("i.items_isActive = 1 AND i.items_id = ".$this->input->get("item_id")." AND mi.menuItem_id = 3 AND cat.category_id = 3 AND i.User_user_id = ".$this->session->userdata('entrepreneur_user_id'));

        if(count($content['data_item']) != 0)
            $this->load->view('Entrepreneur/BusinessForm/RestaurantEditView',$content);
        else
            redirect('../Restaurant');
    }
    public function saveNewItem()
    {
        $this->load->library('form_validation');
        $config['upload_path'] = './assets/img/uploadfile/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        //$config['max_size'] = 2048; 
        $config['encrypt_name'] = true; 
        $this->load->library('upload', $config);
        
        $this->form_validation->set_rules('itmes_topicThai', 'required|itmes_topicThai|itmes_topicThai|กรุณากรอกชื่อสถานที่', 'trim|required');

        $this->form_validation->set_rules('namesubcategory', 'required|namesubcategory|namesubcategory|กรุณาหมวดหมู่สถานที่', 'trim|required');
        $this->form_validation->set_rules('items_latitude', 'required|items_latitude|items_latitude|กรุณากรอก Latitude', 'trim|required');
        $this->form_validation->set_rules('items_longitude', 'required|items_longitude|items_longitude|กรุณากรอก Longitude', 'trim|required');

        $this->form_validation->set_rules('dayofweek', 'required|dayofweek|dayofweek|กรุณาเลือกวัน เปิด-ปิด', 'trim|required');
        $this->form_validation->set_rules('items_timeOpen', 'required|items_timeOpen|items_timeOpen|กรุณาเลือกเวลา เปิด', 'trim|required');
        $this->form_validation->set_rules('items_timeClose', 'required|items_timeClose|items_timeClose|กรุณาเลือกเวลา ปิด', 'trim|required');
        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {   
            $Items = array(
                "itmes_topicThai" =>$this->input->post("itmes_topicThai"),
                "itmes_topicEnglish" =>$this->input->post("itmes_topicEnglish"),
                "itmes_topicLaos" =>$this->input->post("itmes_topicLaos"),
                "itmes_topicChinese" =>$this->input->post("itmes_topicChinese"),
                "items_contactThai" =>$this->input->post("items_contactThai"),
                "items_contactEnglish" =>$this->input->post("items_contactEnglish"),
                "items_contactLaos" =>$this->input->post("items_contactLaos"),
                "items_contactChinese" =>$this->input->post("items_contactChinese"),
                "items_latitude" =>$this->input->post("items_latitude"),
                "items_longitude" =>$this->input->post("items_longitude"),
                "items_phone" =>$this->input->post("items_phone"),
                "items_email" =>$this->input->post("items_email"),
                "items_line" =>$this->input->post("items_line"),
                "items_facebookPage" =>$this->input->post("items_facebookPage"),
               
                "Subdistricts_subdistricts_id" =>$this->input->post("subdistricts"),
                "MenuItem_menuItem_id" =>3,
                "User_user_id" => $this->session->userdata('entrepreneur_user_id'),
                "items_createdDTTM" => date("Y-m-d H:i:s"),
                "items_isActive" => 0,
                "items_isPublish" => 0,
            );
            $inserted_id_item = $this->items_models->AddItemData($Items,"Items");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $this->upload->do_upload('cropperImage');
            $upload_data = $this->upload->data();
            $CoverItems = array(
                "coverItem_paths" =>$upload_data["file_name"],
                "coverItem_url" =>$this->input->post("coverItem_url"),
                "Items_items_id" =>$inserted_id_item, 
            );
            $inserted_cover_item_id_item = $this->items_models->AddItemData($CoverItems,"CoverItems");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if(empty($_FILES['originalImage']['name'][0]))
            {
                $originalImage = "p3.jpg";
            }
            else
            {
                $this->upload->do_upload('originalImage');
                $upload_data = $this->upload->data();
                $originalImage = $upload_data["file_name"];
            }
            $TempImageCover = array(
                "tempImageCover_paths" =>$originalImage,
                "tempImageCover_CropData" =>$this->input->post("CropData"),
                "tempImageCover_CropBoxData" =>$this->input->post("CropBoxData"),
                "tempImageCover_CanvasData" =>$this->input->post("CanvasData"),
                
                "CoverItems_coverItem_id" =>$inserted_cover_item_id_item,
            );
            $this->items_models->AddItemData($TempImageCover,"TempImageCover");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $Items_has_SubCategory = array(
                "SubCategory_subcategory_id" =>$this->input->post("namesubcategory"),
                "Items_items_id" =>$inserted_id_item,
            );
            $this->items_models->AddItemData($Items_has_SubCategory,"Items_has_SubCategory");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $dayofweek = $this->input->post("dayofweek");
            $dayofweekOC = explode(",",$dayofweek);
            $Items_has_DayOfWeek = array(
                "item_dayOpen" =>$dayofweekOC[0],
                "item_dayClose" =>(count($dayofweekOC) == 1?$dayofweekOC[0]:$dayofweekOC[1]),
                "items_timeOpen" =>$this->input->post("items_timeOpen"),
                "items_timeClose" =>$this->input->post("items_timeClose"),
                "Items_items_id" =>$inserted_id_item,
            );
            $this->items_models->AddItemData($Items_has_DayOfWeek,"Items_has_DayOfWeek");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $Detail = array(
                "detail_textThai" =>$this->input->post("topicdetail_id_1_Thai"),
                "detail_textEnglish" =>$this->input->post("topicdetail_id_1_English"),
                "detail_textLaos" =>$this->input->post("topicdetail_id_1_Laos"),
                "detail_textChinese" =>$this->input->post("topicdetail_id_1_Chinese"),
                "Items_items_id" =>$inserted_id_item,
                "TopicDetail_topicdetail_id" =>1,
            );
            $inserted_detail_id_item = $this->items_models->AddItemData($Detail,"Detail");
            for($i = 1; $i <= intval($this->input->post("image_thingstomeet")); $i++){
                $this->upload->do_upload('img_thingstomeet_fname_'.$i);
                $upload_data = $this->upload->data();
                $Photo=array(
                    "photo_paths" =>$upload_data["file_name"],
                    "photo_textThai" =>$this->input->post("topicdetail_id_1_Thai"),
                    "photo_textEnglish" =>$this->input->post("topicdetail_id_1_English"),
                    "photo_textLaos" =>$this->input->post("topicdetail_id_1_Laos"),
                    "photo_textChinese" =>$this->input->post("topicdetail_id_1_Chinese"),
                    "Detail_detail_id" => $inserted_detail_id_item,
                );
                $this->items_models->AddItemData($Photo,"Photo");
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $Detail = array(
                "detail_textThai" =>$this->input->post("topicdetail_id_2_Thai"),
                "detail_textEnglish" =>$this->input->post("topicdetail_id_2_English"),
                "detail_textLaos" =>$this->input->post("topicdetail_id_2_Laos"),
                "detail_textChinese" =>$this->input->post("topicdetail_id_2_Chinese"),
                "Items_items_id" =>$inserted_id_item,
                "TopicDetail_topicdetail_id" =>2,
            );
            $inserted_detail_id_item = $this->items_models->AddItemData($Detail,"Detail");
            for($i = 1; $i <= intval($this->input->post("image_info")); $i++){
                $this->upload->do_upload('img_info_fname_'.$i);
                $upload_data = $this->upload->data();
                $Photo = array(
                    "photo_paths" =>$upload_data["file_name"],
                    "photo_textThai" =>$this->input->post("photo_textThai_info_".$i),
                    "photo_textEnglish" =>$this->input->post("photo_textEnglish_info_".$i),
                    "photo_textLaos" =>$this->input->post("photo_textLaos_info_".$i),
                    "photo_textChinese" =>$this->input->post("photo_textChinese_info_".$i),
                    "Detail_detail_id" => $inserted_detail_id_item,
                );
                $this->items_models->AddItemData($Photo,"Photo");
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $Detail = array(
                "detail_textThai" =>$this->input->post("topicdetail_id_3_Thai"),
                "detail_textEnglish" =>$this->input->post("topicdetail_id_3_English"),
                "detail_textLaos" =>$this->input->post("topicdetail_id_3_Laos"),
                "detail_textChinese" =>$this->input->post("topicdetail_id_3_Chinese"),
                "Items_items_id" =>$inserted_id_item,
                "TopicDetail_topicdetail_id" =>3,
            );
            $inserted_detail_id_item = $this->items_models->AddItemData($Detail,"Detail");
            for($i = 1; $i <= intval($this->input->post("image_navigate")); $i++){
                $this->upload->do_upload('img_navigate_fname_'.$i);
                $upload_data = $this->upload->data();
                $Photo = array(
                    "photo_paths" =>$upload_data["file_name"],
                    "photo_textThai" =>$this->input->post("photo_textThai_navigate_".$i),
                    "photo_textEnglish" =>$this->input->post("photo_textEnglish_navigate_".$i),
                    "photo_textLaos" =>$this->input->post("photo_textLaos_navigate_".$i),
                    "photo_textChinese" =>$this->input->post("photo_textChinese_navigate_".$i),
                    "Detail_detail_id" => $inserted_detail_id_item,
                );
                $this->items_models->AddItemData($Photo,"Photo");
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
           if(!empty($this->input->post("array_delicious")))
           {
                $array_delicious = explode(',',$this->input->post("array_delicious"));
                
                for($i = 0; $i < count($array_delicious); $i++){
                    $Items_has_Delicious = array(
                        "DeliciousGuarantee_deliciousGuarantee_id" =>$array_delicious[$i],
                        "Items_items_id" =>$inserted_id_item,
                    );
                    $this->items_models->AddItemData($Items_has_Delicious,"Items_has_Delicious");
                }
                
           }
            
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////       
            for($i = 0; $i < intval($this->input->post("image_foodmenu")); $i++){
                $Product= array(
                    "product_namesThai" =>$this->input->post("product_namesThai_".($i+1)),
                    "product_namesEnglish" =>$this->input->post("product_namesEnglish_".($i+1)),
                    "product_namesLaos" =>$this->input->post("product_namesLaos_".($i+1)),
                    "product_namesChinese" =>$this->input->post("product_namesChinese_".($i+1)),

                    "product_price" =>$this->input->post("product_price_".($i+1)),

                    "product_descriptionThai" =>$this->input->post("product_descriptionThai_".($i+1)),
                    "product_descriptionEnglish" =>$this->input->post("product_descriptionEnglish_".($i+1)),
                    "product_descriptionLaos" =>$this->input->post("product_descriptionLaos_".($i+1)),
                    "product_descriptionChinese" =>$this->input->post("product_descriptionChinese_".($i+1)),
                    "Items_items_id" =>$inserted_id_item,
                
                );
                $inserted_product_id_item = $this->items_models->AddItemData($Product,"Product");
                $array_image_foodmenu = explode(',',$this->input->post("array_image_foodmenu"));
                if(count($array_image_foodmenu) >= $i)
                {
                    for ($j = 0; $j < $array_image_foodmenu[$i]; $j++) {
                        $this->upload->do_upload('img_image_foodmenu_'.($i+1).'_fname_'.($j+1));
                        $upload_data = $this->upload->data();
                        $PhotoProduct = array(
                        "photoProduct_paths" =>$upload_data["file_name"],
                        "Product_product_id" => $inserted_product_id_item,
                       );
                       $this->items_models->AddItemData($PhotoProduct,"PhotoProduct");
                    }
                }
                
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $res = array('state' => true,'error'=>'', 'msg' => 'เพิ่มสำเร็จ');
        }

        return $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }
    public function saveNewProduct()
    {
        $this->load->library('form_validation');
        $config['upload_path'] = './assets/img/uploadfile/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        //$config['max_size'] = 2048; 
        $config['encrypt_name'] = true; 
        $this->load->library('upload', $config);
        
        for($i = 0; $i < intval($this->input->post("image_foodmenu")); $i++)
        {
            $Product= array(
                "product_namesThai" =>$this->input->post("product_namesThai_".($i+1)),
                "product_namesEnglish" =>$this->input->post("product_namesEnglish_".($i+1)),
                "product_namesLaos" =>$this->input->post("product_namesLaos_".($i+1)),
                "product_namesChinese" =>$this->input->post("product_namesChinese_".($i+1)),

                "product_price" =>$this->input->post("product_price_".($i+1)),

                "product_descriptionThai" =>$this->input->post("product_descriptionThai_".($i+1)),
                "product_descriptionEnglish" =>$this->input->post("product_descriptionEnglish_".($i+1)),
                "product_descriptionLaos" =>$this->input->post("product_descriptionLaos_".($i+1)),
                "product_descriptionChinese" =>$this->input->post("product_descriptionChinese_".($i+1)),
                "Items_items_id" =>$this->input->post("items_id"),
            
            );
            $inserted_product_id_item = $this->items_models->AddItemData($Product,"Product");
            $array_image_foodmenu = explode(',',$this->input->post("array_image_foodmenu"));
            if(count($array_image_foodmenu) >= $i)
            {
                for ($j = 0; $j < $array_image_foodmenu[$i]; $j++) 
                {
                    $this->upload->do_upload('img_image_foodmenu_'.($i+1).'_fname_'.($j+1));
                    $upload_data = $this->upload->data();
                    $PhotoProduct = array(
                    "photoProduct_paths" =>$upload_data["file_name"],
                    "Product_product_id" => $inserted_product_id_item,
                   );
                   $this->items_models->AddItemData($PhotoProduct,"PhotoProduct");
                }
            }
            
        }
    }
    public function saveEditItem()
    {
        $this->load->library('form_validation');
        $config['upload_path'] = './assets/img/uploadfile/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        //$config['max_size'] = 2048; 
        $config['encrypt_name'] = true; 
        $this->load->library('upload', $config);

        $this->form_validation->set_rules('itmes_topicThai', 'required|itmes_topicThai|itmes_topicThai|กรุณากรอกชื่อสถานที่', 'trim|required');
        $this->form_validation->set_rules('namesubcategory', 'required|namesubcategory|namesubcategory|กรุณาหมวดหมู่สถานที่', 'trim|required');
        $this->form_validation->set_rules('items_latitude', 'required|items_latitude|items_latitude|กรุณากรอก Latitude', 'trim|required');
        $this->form_validation->set_rules('items_longitude', 'required|items_longitude|items_longitude|กรุณากรอก Longitude', 'trim|required');

        $this->form_validation->set_rules('dayofweek', 'required|dayofweek|dayofweek|กรุณาเลือกวัน เปิด-ปิด', 'trim|required');
        $this->form_validation->set_rules('items_timeOpen', 'required|items_timeOpen|items_timeOpen|กรุณาเลือกเวลา เปิด', 'trim|required');
        $this->form_validation->set_rules('items_timeClose', 'required|items_timeClose|items_timeClose|กรุณาเลือกเวลา ปิด', 'trim|required');
        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {
            $items_models_data = $this->items_models->GetItemData("i.items_id = ".$this->input->post("items_id"));
            $Items = array(
                "itmes_topicThai" =>$this->input->post("itmes_topicThai"),
                "itmes_topicEnglish" =>$this->input->post("itmes_topicEnglish"),
                "itmes_topicLaos" =>$this->input->post("itmes_topicLaos"),
                "itmes_topicChinese" =>$this->input->post("itmes_topicChinese"),
                "items_contactThai" =>$this->input->post("items_contactThai"),
                "items_contactEnglish" =>$this->input->post("items_contactEnglish"),
                "items_contactLaos" =>$this->input->post("items_contactLaos"),
                "items_contactChinese" =>$this->input->post("items_contactChinese"),
                "items_latitude" =>$this->input->post("items_latitude"),
                "items_longitude" =>$this->input->post("items_longitude"),
                "items_phone" =>$this->input->post("items_phone"),
                "items_email" =>$this->input->post("items_email"),
                "items_line" =>$this->input->post("items_line"),
                "items_facebookPage" =>$this->input->post("items_facebookPage"),
               
                "Subdistricts_subdistricts_id" =>$this->input->post("subdistricts"),
                "MenuItem_menuItem_id" =>3,
                "items_updatedDTTM" => date("Y-m-d H:i:s"),
                "items_isActive" => 0,
                "items_isPublish" => 0,
            );
            $trans_status = $this->items_models->UpdateItemData($Items,"Items","items_id = ".$this->input->post("items_id"));
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $coverItem_paths = './assets/img/uploadfile/'. $items_models_data[0]->coverItem_paths;
            $tempImageCover_paths = './assets/img/uploadfile/'. $items_models_data[0]->tempImageCover_paths;
            //if (is_readable($coverItem_paths) && unlink($coverItem_paths) && is_readable($tempImageCover_paths) && unlink($tempImageCover_paths)) 
            //{
                is_readable($coverItem_paths);
                unlink($coverItem_paths);
                if($items_models_data[0]->tempImageCover_paths != "p3.jpg")
                {
                    is_readable($tempImageCover_paths);
                    unlink($tempImageCover_paths);
                }
                $trans_status = $this->items_models->DeleteItemData("CoverItems","Items_items_id = ".$this->input->post("items_id"));
               
                $this->upload->do_upload('cropperImage');
                $upload_data = $this->upload->data();
                $CoverItems = array(
                    "coverItem_paths" =>$upload_data["file_name"],
                    "coverItem_url" =>$this->input->post("coverItem_url"),
                    "Items_items_id" =>$this->input->post("items_id"), 
                );
                $inserted_cover_item_id_item = $this->items_models->AddItemData($CoverItems,"CoverItems");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

                $this->upload->do_upload('originalImage');
                $upload_data = $this->upload->data();
                $originalImage = $upload_data["file_name"];
                
                $TempImageCover = array(
                    "tempImageCover_paths" =>$originalImage,
                    "tempImageCover_CropData" =>$this->input->post("CropData"),
                    "tempImageCover_CropBoxData" =>$this->input->post("CropBoxData"),
                    "tempImageCover_CanvasData" =>$this->input->post("CanvasData"),
                    
                    "CoverItems_coverItem_id" =>$inserted_cover_item_id_item,
                );
                $this->items_models->AddItemData($TempImageCover,"TempImageCover");
            //}        
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $Items_has_SubCategory = array(
                "SubCategory_subcategory_id" =>$this->input->post("namesubcategory"),
            );
            $trans_status = $this->items_models->UpdateItemData($Items_has_SubCategory,"Items_has_SubCategory","Items_items_id = ".$this->input->post("items_id"));
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $dayofweek = $this->input->post("dayofweek");
            $dayofweekOC = explode(",",$dayofweek);
            $Items_has_DayOfWeek = array(
                "item_dayOpen" =>$dayofweekOC[0],
                "item_dayClose" =>(count($dayofweekOC) == 1?$dayofweekOC[0]:$dayofweekOC[1]),
                "items_timeOpen" =>$this->input->post("items_timeOpen"),
                "items_timeClose" =>$this->input->post("items_timeClose"),
            );
            $trans_status = $this->items_models->UpdateItemData($Items_has_DayOfWeek,"Items_has_DayOfWeek","Items_items_id = ".$this->input->post("items_id"));
////////////////////////////////////////////////// Delete Detail //////////////////////////////////////////////////////////////////////////

            for($i = 0; $i < count($items_models_data[0]->Detail); $i++){
                for($j = 0; $j < count($items_models_data[0]->Detail[$i]->Photo); $j++)
                {
                    $photo_paths = './assets/img/uploadfile/'. $items_models_data[0]->Detail[$i]->Photo[$j]->photo_paths;
                    is_readable($photo_paths);
                    unlink($photo_paths);
                    sleep(0.25);
                }
            }
            for($i = 0; $i < count($items_models_data[0]->Product); $i++){
                for($j = 0; $j < count($items_models_data[0]->Product[$i]->PhotoProduct); $j++)
                {
                    $photo_paths = './assets/img/uploadfile/'. $items_models_data[0]->Product[$i]->PhotoProduct[$j]->photoProduct_paths;
                    is_readable($photo_paths);
                    unlink($photo_paths);
                    sleep(0.25);
                }
            }
            $trans_status = $this->items_models->DeleteItemData("Detail","Items_items_id = ".$this->input->post("items_id"));
            //$trans_status = $this->items_models->DeleteItemData("Product","Items_items_id = ".$this->input->post("items_id"));
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $Detail = array(
                "detail_textThai" =>$this->input->post("topicdetail_id_1_Thai"),
                "detail_textEnglish" =>$this->input->post("topicdetail_id_1_English"),
                "detail_textLaos" =>$this->input->post("topicdetail_id_1_Laos"),
                "detail_textChinese" =>$this->input->post("topicdetail_id_1_Chinese"),
                "Items_items_id" =>$this->input->post("items_id"),
                "TopicDetail_topicdetail_id" =>1,
            );
            $inserted_detail_id_item = $this->items_models->AddItemData($Detail,"Detail");
            for($i = 1; $i <= intval($this->input->post("image_thingstomeet")); $i++){
                $this->upload->do_upload('img_thingstomeet_fname_'.$i);
                $upload_data = $this->upload->data();
                $Photo=array(
                    "photo_paths" =>$upload_data["file_name"],
                    "photo_textThai" =>$this->input->post("topicdetail_id_1_Thai"),
                    "photo_textEnglish" =>$this->input->post("topicdetail_id_1_English"),
                    "photo_textLaos" =>$this->input->post("topicdetail_id_1_Laos"),
                    "photo_textChinese" =>$this->input->post("topicdetail_id_1_Chinese"),
                    "Detail_detail_id" => $inserted_detail_id_item,
                );
                $this->items_models->AddItemData($Photo,"Photo");
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $Detail = array(
                "detail_textThai" =>$this->input->post("topicdetail_id_2_Thai"),
                "detail_textEnglish" =>$this->input->post("topicdetail_id_2_English"),
                "detail_textLaos" =>$this->input->post("topicdetail_id_2_Laos"),
                "detail_textChinese" =>$this->input->post("topicdetail_id_2_Chinese"),
                "Items_items_id" =>$this->input->post("items_id"),
                "TopicDetail_topicdetail_id" =>2,
            );
            $inserted_detail_id_item = $this->items_models->AddItemData($Detail,"Detail");
            for($i = 1; $i <= intval($this->input->post("image_info")); $i++){
                $this->upload->do_upload('img_info_fname_'.$i);
                $upload_data = $this->upload->data();
                $Photo = array(
                    "photo_paths" =>$upload_data["file_name"],
                    "photo_textThai" =>$this->input->post("photo_textThai_info_".$i),
                    "photo_textEnglish" =>$this->input->post("photo_textEnglish_info_".$i),
                    "photo_textLaos" =>$this->input->post("photo_textLaos_info_".$i),
                    "photo_textChinese" =>$this->input->post("photo_textChinese_info_".$i),
                    "Detail_detail_id" => $inserted_detail_id_item,
                );
                $this->items_models->AddItemData($Photo,"Photo");
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $Detail = array(
                "detail_textThai" =>$this->input->post("topicdetail_id_3_Thai"),
                "detail_textEnglish" =>$this->input->post("topicdetail_id_3_English"),
                "detail_textLaos" =>$this->input->post("topicdetail_id_3_Laos"),
                "detail_textChinese" =>$this->input->post("topicdetail_id_3_Chinese"),
                "Items_items_id" =>$this->input->post("items_id"),
                "TopicDetail_topicdetail_id" =>3,
            );
            $inserted_detail_id_item = $this->items_models->AddItemData($Detail,"Detail");
            for($i = 1; $i <= intval($this->input->post("image_navigate")); $i++){
                $this->upload->do_upload('img_navigate_fname_'.$i);
                $upload_data = $this->upload->data();
                $Photo = array(
                    "photo_paths" =>$upload_data["file_name"],
                    "photo_textThai" =>$this->input->post("photo_textThai_navigate_".$i),
                    "photo_textEnglish" =>$this->input->post("photo_textEnglish_navigate_".$i),
                    "photo_textLaos" =>$this->input->post("photo_textLaos_navigate_".$i),
                    "photo_textChinese" =>$this->input->post("photo_textChinese_navigate_".$i),
                    "Detail_detail_id" => $inserted_detail_id_item,
                );
                $this->items_models->AddItemData($Photo,"Photo");
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $trans_status = $this->items_models->DeleteItemData("Items_has_Delicious","Items_items_id = ".$this->input->post("items_id"));
            if(!empty($this->input->post("array_delicious")))
            {
                $array_delicious = explode(',',$this->input->post("array_delicious"));   
                for($i = 0; $i < count($array_delicious); $i++){
                    $Items_has_Delicious = array(
                        "DeliciousGuarantee_deliciousGuarantee_id" =>$array_delicious[$i],
                        "Items_items_id" =>$this->input->post("items_id"),
                    );
                    $this->items_models->AddItemData($Items_has_Delicious,"Items_has_Delicious");
                }
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////       
            $temp_id_foodmenu = array();
            for($i = 0; $i < intval($this->input->post("image_foodmenu")); $i++)
            {
                $Product= array(
                    "product_namesThai" =>$this->input->post("product_namesThai_".($i+1)),
                    "product_namesEnglish" =>$this->input->post("product_namesEnglish_".($i+1)),
                    "product_namesLaos" =>$this->input->post("product_namesLaos_".($i+1)),
                    "product_namesChinese" =>$this->input->post("product_namesChinese_".($i+1)),

                    "product_price" =>$this->input->post("product_price_".($i+1)),

                    "product_descriptionThai" =>$this->input->post("product_descriptionThai_".($i+1)),
                    "product_descriptionEnglish" =>$this->input->post("product_descriptionEnglish_".($i+1)),
                    "product_descriptionLaos" =>$this->input->post("product_descriptionLaos_".($i+1)),
                    "product_descriptionChinese" =>$this->input->post("product_descriptionChinese_".($i+1)),
                    "Items_items_id" =>$this->input->post("items_id"),
                
                );

                if(!is_null( $this->input->post('temp_id_foodmenu_'.($i+1)) ) && $this->input->post('temp_id_foodmenu_'.($i+1)) != "")
                {
                    $inserted_product_id_item = $this->input->post('temp_id_foodmenu_'.($i+1));
                    $this->items_models->UpdateItemData($Product,"Product","product_id = ".$this->input->post('temp_id_foodmenu_'.($i+1)));
                    $trans_status = $this->items_models->DeleteItemData("PhotoProduct","Product_product_id = ".$this->input->post('temp_id_foodmenu_'.($i+1)));
                    $temp_id_foodmenu[] = $this->input->post('temp_id_foodmenu_'.($i+1));
                }
                else
                {
                    $inserted_product_id_item = $this->items_models->AddItemData($Product,"Product");
                    $temp_id_foodmenu[] = $inserted_product_id_item;
                }
                    

                $array_image_foodmenu = explode(',',$this->input->post("array_image_foodmenu"));
                if(count($array_image_foodmenu) >= $i)
                {
                    for ($j = 0; $j < $array_image_foodmenu[$i]; $j++) {
                        $this->upload->do_upload('img_image_foodmenu_'.($i+1).'_fname_'.($j+1));
                        $upload_data = $this->upload->data();
                        $PhotoProduct = array(
                        "photoProduct_paths" =>$upload_data["file_name"],
                        "Product_product_id" => $inserted_product_id_item,
                    );
                    $this->items_models->AddItemData($PhotoProduct,"PhotoProduct");
                    }
                }
            }
            $trans_status = $this->items_models->DeleteItemData("Product","product_id not in(".join(",",$temp_id_foodmenu).") and Items_items_id = ".$this->input->post("items_id"));   
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $res = array('state' => true,'error'=>'', 'msg' => 'เพิ่มสำเร็จ');
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }
    public function getDataRestaurantForTable()
    {
        // Datatables Variables
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $data = $this->items_models->setDataProducttable($this->input->post("items_id"),"Restaurant");

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($data),
            "recordsFiltered" =>count($data),
            "data" => $data
        );
        echo json_encode($output);
    }
    public function getDeliciousGuarantee()
    {
        $DeliciousGuarantee = $this->items_models->getNameDeliciousGuarantee(array("deliciousGuarantee_id" => $this->input->post("deliciousGuarantee_id")));
        $data = $DeliciousGuarantee->result();
        return $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }
    public function getNameDeliciousGuarantee()
    {
        $DeliciousGuarantee = $this->items_models->getNameDeliciousGuarantee();
       
        foreach ($DeliciousGuarantee->result() as $item)
        {
            echo "<option value=\"".$item->deliciousGuarantee_id."\">".$item->deliciousGuarantee_textThai."</option>";
        }
    }
    public function getNameCategorySubCategory()
    {
        $NameCategorySubCategory = $this->items_models->getNameCategorySubCategory(3);
        echo "<option value=\"\">".$this->lang->line("name_location_type")."</option>";
        
        foreach ($NameCategorySubCategory->result() as $item)
        {
            echo "<option value=\"".$item->subcategory_id."\">".$item->subcategory_thai."</option>";
        }
    }
    public function getDayOfWeek()
    {
        $dayOfWeek = $this->items_models->getDayOfWeek();
        
        foreach ($dayOfWeek as $item)
        {
            echo "<option value=\"".$item->dayofweek_id."\">".$item->dayofweek_thai."</option>";
        }
    }
}