<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
class EventTicket extends Base_Entrepreneur_Controller {
    public function __construct()
    {    	
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');

        $this->load->library('form_validation');
        $config['upload_path'] = './assets/img/uploadfile/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = 2048; 
        $config['encrypt_name'] = true; 
        $this->load->library('upload', $config);

        $this->load->model("CountriesModels","countries_models", true);
        $this->load->model("UserModel","user_model", true);
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("SearchModels","search_models", true);
        $this->load->model("ChatModels","chat_models", true);

        if(strcmp($this->session->userdata('lang'),"EN") == 0)
            $this->lang->load("english","english");
        else
            $this->lang->load("thailand","thailand");
    }
    public function index()
	{
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $content['ModalView'] = "EventTicket";
            
        $item = $this->items_models->GetDefaultItemsData("i.User_user_id = ".$this->session->userdata('entrepreneur_user_id'));

        if(count($item) != 0 && intval($item[0]->MenuItem_menuItem_id) == 10)
        {
            $content['data_item'] = $this->items_models->GetItemReservationsData("i.items_id = ".$item[0]->items_id." AND mi.menuItem_id = 10 AND cat.category_id = 10","EventTicket");
            $this->load->view('Entrepreneur/Information/BusinessInformationEventTicketView',$content);
        }
        else
        {
            redirect('../Main');
        }
        
    }
    public function addEventTicket()
    {
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $this->load->view('Entrepreneur/BusinessForm/EventTicketAddView',$content);
    }
    public function editEventTicket()
    {
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $content['data_item'] = $this->items_models->GetItemReservationsData("i.items_id = ".$this->input->get("item_id")." AND mi.menuItem_id = 10 AND cat.category_id = 10","EventTicket");

        if(count($content['data_item']) != 0)
            $this->load->view('Entrepreneur/BusinessForm/EventTicketEditView',$content);
        else
            redirect('../EventTicket');
    }
    public function getNameCategorySubCategory()
    {
        $NameCategorySubCategory = $this->items_models->getNameCategorySubCategory(10);
        echo "<option value=\"\">ประเภทแพคเกจทัวร์</option>";
       
        foreach ($NameCategorySubCategory->result() as $item)
        {
            echo "<option value=\"".$item->subcategory_id."\">".$item->subcategory_thai."</option>";
            
        }
    }
    public function saveNewItem()
    {
        $this->load->library('form_validation');
        
        $config['upload_path'] = './assets/img/uploadfile/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        //$config['max_size'] = 2048; 
        $config['encrypt_name'] = true; 
        $this->load->library('upload', $config);

        $this->form_validation->set_rules('itmes_topicThai', 'required|itmes_topicThai|itmes_topicThai|กรุณากรอกชื่อตั๋วกิจกรรม', 'trim|required');
        $this->form_validation->set_rules('namesubcategory', 'required|namesubcategory|namesubcategory|กรุณาเลือกประเภทตั๋วกิจกรรม', 'trim|required');
        $this->form_validation->set_rules('items_priceguestAdult', 'required|items_priceguestAdult|items_priceguestAdult|กรุณากรอกราคาผู้ใหญ่', 'trim|required');
        $this->form_validation->set_rules('items_priceguestChild', 'required|items_priceguestChild|items_priceguestChild|กรุณากรอกราคาเด็ก', 'trim|required');

        $this->form_validation->set_rules('country', 'required|country|country|กรุณาเลือกที่ตั้งประเทศ', 'trim|required');
        $this->form_validation->set_rules('provinces', 'required|provinces|provinces|กรุณาเลือกที่ตั้งจังหวัด', 'trim|required');
        $this->form_validation->set_rules('districts', 'required|districts|districts|กรุณาเลือกที่ตั้งอำเภอ', 'trim|required');
        $this->form_validation->set_rules('subdistricts', 'required|subdistricts|subdistricts|กรุณาเลือกที่ตั้งตำบล', 'trim|required');

        $this->form_validation->set_rules('items_latitude', 'required|items_latitude|items_latitude|กรุณากรอก Latitude', 'trim|required');
        $this->form_validation->set_rules('items_longitude', 'required|items_longitude|items_longitude|กรุณากรอก Longitude', 'trim|required');

        $this->form_validation->set_rules('dayofweek', 'required|dayofweek|dayofweek|กรุณาเลือกวัน เปิด-ปิด', 'trim|required');

        for($i = 0;$i<intval($this->input->post("box_field_timepicker"));$i++)
        {
            $this->form_validation->set_rules('items_timeOpen_'.($i+1), 'required|items_timeOpen_'.($i+1).'|items_timeOpen_'.($i+1).'|กรุณาเลือกเวลา เปิด', 'trim|required');
            $this->form_validation->set_rules('items_timeClose_'.($i+1), 'required|items_timeClose_'.($i+1).'|items_timeClose_'.($i+1).'|กรุณาเลือกเวลา ปิด', 'trim|required');
        }

        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else if(intval($this->input->post("box_cover_image_event_ticket")) == 0)
        {
            $res = array('state' => false,'error'=>'Image', 'msg' => "Please select cover image file");
        }
        else
        {
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Items
            $Items = array(
                "itmes_topicThai" =>$this->input->post("itmes_topicThai"),
                "itmes_topicEnglish" =>$this->input->post("itmes_topicEnglish"),
                "itmes_topicLaos" =>$this->input->post("itmes_topicLaos"),
                "itmes_topicChinese" =>$this->input->post("itmes_topicChinese"),
                "items_contactThai" =>$this->input->post("items_contactThai"),
                "items_contactEnglish" =>$this->input->post("items_contactEnglish"),
                "items_contactLaos" =>$this->input->post("items_contactLaos"),
                "items_contactChinese" =>$this->input->post("items_contactChinese"),
                "items_latitude" =>$this->input->post("items_latitude"),
                "items_longitude" =>$this->input->post("items_longitude"),

                "items_priceguestAdult" =>$this->input->post("items_priceguestAdult"),
                "items_priceguestChild" =>$this->input->post("items_priceguestChild"),

                "items_highlightsThai" =>$this->input->post("items_highlightsThai"),
                "items_highlightsEnglish" =>$this->input->post("items_highlightsEnglish"),
                "items_highlightsChinese" =>$this->input->post("items_highlightsChinese"),
                "items_highlightsLaos" =>$this->input->post("items_highlightsLaos"),
                
                "items_phone" =>$this->input->post("items_phone"),

                "Subdistricts_subdistricts_id" =>$this->input->post("subdistricts"),
                "MenuItem_menuItem_id" =>10,
                "User_user_id" => $this->session->userdata('entrepreneur_user_id'),
                "items_createdDTTM" => date("Y-m-d H:i:s"),
                "items_isActive" => 0,
                "items_isPublish" => 0,
            );
            $inserted_id_item = $this->items_models->AddItemData($Items,"Items");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $Items_has_SubCategory = array(
                "SubCategory_subcategory_id" =>$this->input->post("namesubcategory"),
                "Items_items_id" =>$inserted_id_item,
            );
            $this->items_models->AddItemData($Items_has_SubCategory,"Items_has_SubCategory");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Items_has_DayOfWeek
            $dayofweek = $this->input->post("dayofweek");
            $dayofweekOC = explode(",",$dayofweek);
            for($i = 0;$i< intval($this->input->post("box_field_timepicker"));$i++)
            {
                $Items_has_DayOfWeek = array(
                    "item_dayOpen" =>$dayofweekOC[0],
                    "item_dayClose" =>(count($dayofweekOC) == 1?$dayofweekOC[0]:$dayofweekOC[1]),
                    "items_timeOpen" =>$this->input->post("items_timeOpen_".($i+1)),
                    "items_timeClose" =>$this->input->post("items_timeClose_".($i+1)),
                    "Items_items_id" =>$inserted_id_item,
                );
                $this->items_models->AddItemData($Items_has_DayOfWeek,"Items_has_DayOfWeek");
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CoverItems
            for($i = 0;$i< intval($this->input->post("box_cover_image_event_ticket"));$i++)
            {
                $this->upload->do_upload('cropperImage_'.($i+1));
                $upload_data = $this->upload->data();
                $CoverItems = array(
                    "coverItem_paths" =>$upload_data["file_name"],
                    "coverItem_url" =>$this->input->post("coverItem_url"),
                    "Items_items_id" =>$inserted_id_item, 
                );
                $this->items_models->AddItemData($CoverItems,"CoverItems");
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// bookingConditioncol
            $bookingConditioncol = json_decode($this->input->post("bookingConditioncol"));
            for($i = 0;$i< count($bookingConditioncol);$i++)
            {
                $bookingConditioncol_detail = array(
                    "bookingConditioncol_detailThai" => $bookingConditioncol[$i]->bookingConditioncol_detailThai,
                    "bookingConditioncol_detailEnglish" => $bookingConditioncol[$i]->bookingConditioncol_detailEnglish,
                    "bookingConditioncol_detailLaos" => $bookingConditioncol[$i]->bookingConditioncol_detailLaos,
                    "bookingConditioncol_detailChinese" => $bookingConditioncol[$i]->bookingConditioncol_detailChinese,
                    "Items_items_id" => $inserted_id_item,
                    "BookingConditionCategory_bookingConditionCategory_id" => ($i+8)
                );
                $this->items_models->AddItemData($bookingConditioncol_detail,"BookingCondition");
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $Detail = array(
                "detail_textThai" =>$this->input->post("topicdetail_id_1_Thai"),
                "detail_textEnglish" =>$this->input->post("topicdetail_id_1_English"),
                "detail_textLaos" =>$this->input->post("topicdetail_id_1_Laos"),
                "detail_textChinese" =>$this->input->post("topicdetail_id_1_Chinese"),
                "Items_items_id" =>$inserted_id_item,
                "TopicDetail_topicdetail_id" =>1,
            );
            $inserted_detail_id_item = $this->items_models->AddItemData($Detail,"Detail");
// PhotoText
            $PhotoText = json_decode($this->input->post("PhotoText"));
            for($i = 0; $i < intval($this->input->post("box_cover_image_location_illustrations")); $i++){
                $this->upload->do_upload('img_location_illustrations_fname_'.($i+1));
                $upload_data = $this->upload->data();
                $Photo=array(
                    "photo_paths" =>$upload_data["file_name"],
                    "photo_textThai" =>$PhotoText[$i]->photo_textThai,
                    "photo_textEnglish" =>$PhotoText[$i]->photo_textEnglish,
                    "photo_textLaos" =>$PhotoText[$i]->photo_textLaos,
                    "photo_textChinese" =>$PhotoText[$i]->photo_textChinese,
                    "Detail_detail_id" => $inserted_detail_id_item,
                );
                $this->items_models->AddItemData($Photo,"Photo");
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $res = array('state' => true,'error'=>'', 'msg' => 'เพิ่มสำเร็จ');
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }
    public function saveEditItem()
    {
        $this->load->library('form_validation');
        
        $config['upload_path'] = './assets/img/uploadfile/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        //$config['max_size'] = 2048; 
        $config['encrypt_name'] = true; 
        $this->load->library('upload', $config);

        $this->form_validation->set_rules('itmes_topicThai', 'required|itmes_topicThai|itmes_topicThai|กรุณากรอกชื่อตั๋วกิจกรรม', 'trim|required');
        $this->form_validation->set_rules('namesubcategory', 'required|namesubcategory|namesubcategory|กรุณาเลือกประเภทตั๋วกิจกรรม', 'trim|required');
        $this->form_validation->set_rules('items_priceguestAdult', 'required|items_priceguestAdult|items_priceguestAdult|กรุณากรอกราคาผู้ใหญ่', 'trim|required');
        $this->form_validation->set_rules('items_priceguestChild', 'required|items_priceguestChild|items_priceguestChild|กรุณากรอกราคาเด็ก', 'trim|required');
        $this->form_validation->set_rules('country', 'required|country|country|กรุณาเลือกที่ตั้งประเทศ', 'trim|required');
        $this->form_validation->set_rules('provinces', 'required|provinces|provinces|กรุณาเลือกที่ตั้งจังหวัด', 'trim|required');
        $this->form_validation->set_rules('districts', 'required|districts|districts|กรุณาเลือกที่ตั้งอำเภอ', 'trim|required');
        $this->form_validation->set_rules('subdistricts', 'required|subdistricts|subdistricts|กรุณาเลือกที่ตั้งตำบล', 'trim|required');
        $this->form_validation->set_rules('items_latitude', 'required|items_latitude|items_latitude|กรุณากรอก Latitude', 'trim|required');
        $this->form_validation->set_rules('items_longitude', 'required|items_longitude|items_longitude|กรุณากรอก Longitude', 'trim|required');

        $this->form_validation->set_rules('dayofweek', 'required|dayofweek|dayofweek|กรุณาเลือกวัน เปิด-ปิด', 'trim|required');

        for($i = 0;$i<intval($this->input->post("box_field_timepicker"));$i++)
        {
            $this->form_validation->set_rules('items_timeOpen_'.($i+1), 'required|items_timeOpen_'.($i+1).'|items_timeOpen_'.($i+1).'|กรุณาเลือกเวลา เปิด', 'trim|required');
            $this->form_validation->set_rules('items_timeClose_'.($i+1), 'required|items_timeClose_'.($i+1).'|items_timeClose_'.($i+1).'|กรุณาเลือกเวลา ปิด', 'trim|required');
        }
        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else if(intval($this->input->post("box_cover_image_event_ticket")) == 0)
        {
            $res = array('state' => false,'error'=>'Image', 'msg' => "Please select cover image file");
        }
        else
        {
            $items_models_data = $this->items_models->GetItemReservationsData("i.items_id = ".$this->input->post("items_id"),"EventTicket");
            
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Items
            $Items = array(
                "itmes_topicThai" =>$this->input->post("itmes_topicThai"),
                "itmes_topicEnglish" =>$this->input->post("itmes_topicEnglish"),
                "itmes_topicLaos" =>$this->input->post("itmes_topicLaos"),
                "itmes_topicChinese" =>$this->input->post("itmes_topicChinese"),

                "items_contactThai" =>$this->input->post("items_contactThai"),
                "items_contactEnglish" =>$this->input->post("items_contactEnglish"),
                "items_contactLaos" =>$this->input->post("items_contactLaos"),
                "items_contactChinese" =>$this->input->post("items_contactChinese"),

                "items_latitude" =>$this->input->post("items_latitude"),
                "items_longitude" =>$this->input->post("items_longitude"),

                "items_priceguestAdult" =>$this->input->post("items_priceguestAdult"),
                "items_priceguestChild" =>$this->input->post("items_priceguestChild"),

                "items_highlightsThai" =>$this->input->post("items_highlightsThai"),
                "items_highlightsEnglish" =>$this->input->post("items_highlightsEnglish"),
                "items_highlightsChinese" =>$this->input->post("items_highlightsChinese"),
                "items_highlightsLaos" =>$this->input->post("items_highlightsLaos"),

                "Subdistricts_subdistricts_id" =>$this->input->post("subdistricts"),
                "MenuItem_menuItem_id" =>10,
                "items_updatedDTTM" => date("Y-m-d H:i:s"),
                "items_isActive" => 0,
                "items_isPublish" => 0,
            );
            $trans_status = $this->items_models->UpdateItemData($Items,"Items","items_id = ".$this->input->post("items_id"));

            $Items_has_SubCategory = array(
                "SubCategory_subcategory_id" =>$this->input->post("namesubcategory"),
            );
            $this->items_models->UpdateItemData($Items_has_SubCategory,"Items_has_SubCategory","Items_items_id = ".$this->input->post("items_id"));
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// CoverItems
            for($i = 0;$i< count($items_models_data[0]->CoverItems);$i++)
            {
                $coverItem_paths = './assets/img/uploadfile/'. $items_models_data[0]->CoverItems[$i]->coverItem_paths;
                is_readable($coverItem_paths);
                unlink($coverItem_paths);
            }
            $trans_status = $this->items_models->DeleteItemData("CoverItems","Items_items_id = ".$this->input->post("items_id"));

            for($i = 0;$i< intval($this->input->post("box_cover_image_event_ticket"));$i++)
            {
                $this->upload->do_upload('cropperImage_'.($i+1));
                $upload_data = $this->upload->data();
                $CoverItems = array(
                    "coverItem_paths" =>$upload_data["file_name"],
                    "coverItem_url" =>$this->input->post("coverItem_url"),
                    "Items_items_id" =>$this->input->post("items_id"),
                );
                $this->items_models->AddItemData($CoverItems,"CoverItems");
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
// Items_has_DayOfWeek
            $trans_status = $this->items_models->DeleteItemData("Items_has_DayOfWeek","Items_items_id = ".$this->input->post("items_id"));
            $dayofweek = $this->input->post("dayofweek");
            $dayofweekOC = explode(",",$dayofweek);
            for($i = 0;$i< intval($this->input->post("box_field_timepicker"));$i++)
            {
                $Items_has_DayOfWeek = array(
                    "item_dayOpen" =>$dayofweekOC[0],
                    "item_dayClose" =>(count($dayofweekOC) == 1?$dayofweekOC[0]:$dayofweekOC[1]),
                    "items_timeOpen" =>$this->input->post("items_timeOpen_".($i+1)),
                    "items_timeClose" =>$this->input->post("items_timeClose_".($i+1)),
                    "Items_items_id" =>$this->input->post("items_id"),
                );
                $this->items_models->AddItemData($Items_has_DayOfWeek,"Items_has_DayOfWeek");
            }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
// bookingConditioncol
            $trans_status = $this->items_models->DeleteItemData("BookingCondition","Items_items_id = ".$this->input->post("items_id"));
            $bookingConditioncol = json_decode($this->input->post("bookingConditioncol"));
            for($i = 0;$i< count($bookingConditioncol);$i++)
            {
                $bookingConditioncol_detail = array(
                    "bookingConditioncol_detailThai" => $bookingConditioncol[$i]->bookingConditioncol_detailThai,
                    "bookingConditioncol_detailEnglish" => $bookingConditioncol[$i]->bookingConditioncol_detailEnglish,
                    "bookingConditioncol_detailLaos" => $bookingConditioncol[$i]->bookingConditioncol_detailLaos,
                    "bookingConditioncol_detailChinese" => $bookingConditioncol[$i]->bookingConditioncol_detailChinese,
                    "Items_items_id" => $this->input->post("items_id"),
                    "BookingConditionCategory_bookingConditionCategory_id" => ($i+8)
                );
                $this->items_models->AddItemData($bookingConditioncol_detail,"BookingCondition");
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Detail
            for($j = 0; $j < count($items_models_data[0]->Detail[0]->Photo); $j++)
            {
                $photo_paths = './assets/img/uploadfile/'. $items_models_data[0]->Detail[0]->Photo[$j]->photo_paths;
                is_readable($photo_paths);
                unlink($photo_paths);
                sleep(0.25);
            }
            
            $trans_status = $this->items_models->DeleteItemData("Detail","Items_items_id = ".$this->input->post("items_id"));
            $Detail = array(
                "detail_textThai" =>$this->input->post("topicdetail_id_1_Thai"),
                "detail_textEnglish" =>$this->input->post("topicdetail_id_1_English"),
                "detail_textLaos" =>$this->input->post("topicdetail_id_1_Laos"),
                "detail_textChinese" =>$this->input->post("topicdetail_id_1_Chinese"),
                "Items_items_id" =>$this->input->post("items_id"),
                "TopicDetail_topicdetail_id" =>1,
            );
            $inserted_detail_id_item = $this->items_models->AddItemData($Detail,"Detail");
            // PhotoText
            $PhotoText = json_decode($this->input->post("PhotoText"));
            for($i = 0; $i < intval($this->input->post("box_cover_image_location_illustrations")); $i++){
                $this->upload->do_upload('img_location_illustrations_fname_'.($i+1));
                $upload_data = $this->upload->data();
                $Photo=array(
                    "photo_paths" =>$upload_data["file_name"],
                    "photo_textThai" =>$PhotoText[$i]->photo_textThai,
                    "photo_textEnglish" =>$PhotoText[$i]->photo_textEnglish,
                    "photo_textLaos" =>$PhotoText[$i]->photo_textLaos,
                    "photo_textChinese" =>$PhotoText[$i]->photo_textChinese,
                    "Detail_detail_id" => $inserted_detail_id_item,
                );
                $this->items_models->AddItemData($Photo,"Photo");
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $res = array('state' => true,'error'=>'', 'msg' => 'เพิ่มสำเร็จ');
        }
        return $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }
    public function getDayOfWeek()
    {
        $dayOfWeek = $this->items_models->getDayOfWeek();
        
        foreach ($dayOfWeek as $item)
        {
            echo "<option value=\"".$item->dayofweek_id."\">".$item->dayofweek_thai."</option>";
        }
    }
    public function getDataEventTicketForTable()
    {
        // Datatables Variables
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));
        
        //$data = $this->items_models->setDatatableReservations("mi.menuItem_id = 10 AND cat.category_id = 10 AND i.User_user_id = ".$this->input->post("user_id"),"EventTicket");
        $data = $this->search_models->getDataSearchForTable(
            "EventTicket",
            $this->input->post("namesubcategory"),
            $this->input->post("country"),
            $this->input->post("provinces"),
            $this->input->post("districts"),
            "i.items_isActive != 99 and mi.menuItem_id = 10 AND cat.category_id = 10 AND i.User_user_id = ".$this->input->post("user_id")
        );

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($data),
            "recordsFiltered" =>count($data),
            "data" => $data
        );
        echo json_encode($output);
    }
}