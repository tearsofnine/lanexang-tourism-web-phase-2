<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
class Login extends CI_Controller {
	public function __construct()
    {
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('array');
		$this->load->library('form_validation');
		$this->load->model("UserModel","User", true);

		if(strcmp($this->session->userdata('lang'),"EN") == 0)
            $this->lang->load("english","english");
        else
            $this->lang->load("thailand","thailand");
    }
    public function index()
	{
		$content['host'] ="/dasta_thailand/";
		if ($this->session->userdata('entrepreneur_logged_in') == true)
        {
			$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
			$domainName = $_SERVER['HTTP_HOST']."/";
			
			if(strcmp(substr($domainName, -1),"/") == 0 )
				$mainHost = $protocol . $domainName.'dasta_thailand/entrepreneur/Main';
			else
				$mainHost = $protocol . $domainName.'/dasta_thailand/entrepreneur/Main';
			//str_replace('&#47;&#47;','&#47;',$mainHost);
			echo $mainHost;
			redirect($mainHost);
			//redirect(base_url()+'/entrepreneur/Main');
		}
		else
			$this->load->view('Entrepreneur/LoginView',$content);
	}

    public function authenticate()
	{
		$user_account = $this->input->post('input_account');
		$user_password = $this->input->post('input_password');

		$this->form_validation->set_rules('input_account', 'User Account', 'trim|required');
		$this->form_validation->set_rules('input_password', 'Password', 'required');

		if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false, 'msg' => validation_errors());
		}
		else 
		{
			$data_user = $this->User->login_entrepreneur($user_account, $user_password,"test");
			switch ($data_user["status"]) {
				case 1:
					$res   = array('state' => true, 'type' => $data_user["status"],'user_id' => $data_user["user_id"], 'msg' => 'เข้าสู่ระบบสำเร็จ');
					break;
				case -1:
					$msg = "เกิดข้อผิดพลาด password wrong";
					$res = array('state' => false,'type' => $data_user["status"],'user_id' => $data_user["user_id"], 'msg' => $msg);
					break;
				case -2:
					$msg = "เกิดข้อผิดพลาด No user account!!!";
					$res = array('state' => false,'type' => $data_user["status"],'user_id' => $data_user["user_id"], 'msg' => $msg);
					break;
				case -3:
					$msg = "เกิดข้อผิดพลาด user account is Delete";
					$res = array('state' => false,'type' => $data_user["status"],'user_id' => $data_user["user_id"], 'msg' => $msg);
					break;
				case -4:
					$msg = "เกิดข้อผิดพลาด user account is not Active";
					$res = array('state' => false,'type' => $data_user["status"],'user_id' => $data_user["user_id"], 'msg' => $msg);
				break;
				case -5:
					$msg = "เกิดข้อผิดพลาด The user account was denied.";
					$res = array('state' => false,'type' => $data_user["status"],'user_id' => $data_user["user_id"], 'msg' => $msg);
				break;
				default:
					$msg = "เกิดข้อผิดพลาด";
					$res = array('state' => false, 'msg' => $msg);
			}
		}

		return $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }
    public function logout()
	{
		$this->session->unset_userdata("entrepreneur_user_id");
		$this->session->unset_userdata("entrepreneur_user_account");
		$this->session->unset_userdata("entrepreneur_user_email");
		$this->session->unset_userdata("entrepreneur_user_firstName");
		$this->session->unset_userdata("entrepreneur_user_lastName");
		$this->session->unset_userdata("entrepreneur_UserRole_userRole_id");
		$this->session->unset_userdata("entrepreneur_logged_in");
		$this->session->unset_userdata("entrepreneur_lang");
		$this->session->unset_userdata("business_type_category_id");

		$this->session->unset_userdata("business_nameThai");
		$this->session->unset_userdata("business_nameEnglish");
		$this->session->unset_userdata("business_nameChinese");
		$this->session->unset_userdata("business_nameLaos");

		$this->session->unset_userdata("business_presentAddress");

		$this->session->unset_userdata("business_latitude");
		$this->session->unset_userdata("business_longitude");

		$this->session->unset_userdata("business_phone");
		$this->session->unset_userdata("business_www");
		$this->session->unset_userdata("business_facebook");
		$this->session->unset_userdata("business_line");

		$this->session->unset_userdata("business_license_paths");

		$this->session->unset_userdata("user_profile_pic_url");
		$this->session->unset_userdata("Subdistricts_subdistricts_id");
		redirect();
	}
}