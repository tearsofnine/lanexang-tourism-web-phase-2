<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
class Notifications extends Base_Entrepreneur_Controller {
    public function __construct()
    {    	
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        
        // $this->load->model("CountriesModels","countries_models", true);
        $this->load->model("UserModel","user_model", true);
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("ProgramTourModels","program_tour_models", true);
        $this->load->model("ChatModels","chat_models", true);
        $this->load->model("GlobalFunctionModels","global_function_models", true);

        if(strcmp($this->session->userdata('lang'),"EN") == 0)
            $this->lang->load("english","english");
        else
            $this->lang->load("thailand","thailand");

    }
    public function index()
	{
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $this->load->view('Entrepreneur/NotificationsView',$content);
    }
    public function getDataForTable()
    {
        // Datatables Variables
        $draw = intval($this->input->get("draw"));
        $start = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $data =array();
        $chatroom_data = $this->chat_models->get_chat_room_for_table("UserSend_user_id = ".$this->input->post("user_id")." OR UserReceive_user_id = ".$this->input->post("user_id"),$this->session->userdata('entrepreneur_user_id'));
        for($i=0;$i<count($chatroom_data);$i++)
        {
            $name ="";
            if(intval($chatroom_data[$i]->UserSend_user_id) == $this->input->post("user_id"))
            {
                $name = $chatroom_data[$i]->UserReceiv[0]->user_firstName." ".$chatroom_data[$i]->UserReceiv[0]->user_lastName;
                
                $arrParsedUrl = parse_url($chatroom_data[$i]->UserReceiv[0]->user_profile_pic_url);
                if (!empty($arrParsedUrl['scheme']))
                {
                    $image = $chatroom_data[$i]->UserReceiv[0]->user_profile_pic_url;
                }
                else
                {
                    if($chatroom_data[$i]->UserReceiv[0]->user_profile_pic_url != "" || $chatroom_data[$i]->UserReceiv[0]->user_profile_pic_url != NULL)
                        $image = "/dasta_thailand/assets/img/uploadfile/".$chatroom_data[$i]->UserReceiv[0]->user_profile_pic_url;
                    else
                        $image = "/dasta_thailand/assets/img/account_avatar.jpg";
                }
               
            }
            else
            {
                $name = $chatroom_data[$i]->UserSend[0]->user_firstName." ".$chatroom_data[$i]->UserSend[0]->user_lastName;
                $arrParsedUrl = parse_url($chatroom_data[$i]->UserSend[0]->user_profile_pic_url);
                if (!empty($arrParsedUrl['scheme']))
                {
                    $image = $chatroom_data[$i]->UserSend[0]->user_profile_pic_url;
                }
                else
                {
                    if($chatroom_data[$i]->UserSend[0]->user_profile_pic_url != "" || $chatroom_data[$i]->UserSend[0]->user_profile_pic_url != NULL)
                        $image = "/dasta_thailand/assets/img/uploadfile/".$chatroom_data[$i]->UserSend[0]->user_profile_pic_url;
                    else
                        $image = "/dasta_thailand/assets/img/account_avatar.jpg";
                }

               
            }
            $count_read ="";
            if(intval($chatroom_data[$i]->count_read) !=0 )
            {
                $count_read = "<span class=\"float-right btn btn-primary btn-circle\">".$chatroom_data[$i]->count_read."</i></span>";
            }

            $data[] =  array(
                $chatroom_data[$i]->chatroom_id,
                "
                <a  href=\"./Chat?chatroom_id=".$chatroom_data[$i]->chatroom_id."\">
                    <div class=\"row \">
                        <div class=\"col-lg-12 \">
                            <div class=\"ibox\">
                                <div class=\"chat-element\">
                                    <div class=\"float-left\">
                                        <img alt=\"image\" class=\"rounded-circle\" src=\"".$image."\">
                                    </div>
                                    <div class=\"media-body \">
                                        <small class=\"float-right text-navy\">".$this->global_function_models->time_elapsed_string($chatroom_data[$i]->message_created_at)."</small>
                                        <h3 class=\"text-muted font-bold\">".$name."</h3>
                                        ".$count_read."
                                        <!--<p class=\"m-b-xs text-navy\">ทัวร์บึงกาฬ ภูทอก วัดผาตากเสื้อ วัดป่าภูก้อน...</p>-->
                                        <h3 class=\"text-muted font-bold\">".$this->global_function_models->substrwords($chatroom_data[$i]->message,40)."</h3>
                                    </div>
                                
                                </div>
                            </div>
                        </div>
                    </div>
                <a>
                "
            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($data),
            "recordsFiltered" =>count($data),
            "data" => $data
        );
        echo json_encode($output);
    }
}