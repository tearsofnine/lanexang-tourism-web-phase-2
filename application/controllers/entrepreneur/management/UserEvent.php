<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
//define ("SECRETKEY", "KanTasStudio");
class UserEvent extends Base_Entrepreneur_Controller {
    public function __construct()
    {    	
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model("CountriesModels","countries_models", true);
        $this->load->model("UserModel","user_model", true);
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("SearchModels","search_models", true);
        $this->load->model("ProgramTourModels","program_tour_models", true);
        $this->load->model("ChatModels","chat_models", true);
        $this->load->model("GlobalFunctionModels","global_function_models", true);

        
    }

    public function oldpass_check($str)
    {
        $this->load->library('form_validation');
        $this->db->where('user_id', $this->session->userdata('entrepreneur_user_id') );
        $query = $this->db->get('User');
        $row = $query->row();

        $password = openssl_encrypt($str, "AES-128-ECB", SECRETKEY);
        if($row->user_password != $password)
        {
            $this->form_validation->set_message('oldpass_check', 'The %s ให้ถูกต้อง|');
			return FALSE;
        }
        else
		{
			return TRUE;
		}
    }
    public function ChangePassword()
    {
        $this->load->library('form_validation');
        //$this->db->where('user_id', $this->session->userdata('entrepreneur_user_id'));
        //$query = $this->db->get('User');

        $this->form_validation->set_rules('oldpass', 'required|oldpass|oldpass|กรุณากรอก รหัสผ่านเก่า', 'trim|required|callback_oldpass_check');
        $this->form_validation->set_rules('newpass', 'required|newpass|newpass|กรุณากรอก รหัสใหม่', 'trim|required|matches[conpass]');
        $this->form_validation->set_rules('conpass', 'required|conpass|conpass|กรุณากรอก รหัสใหม่อีกครั้ง', 'trim|required');
       
        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {   
            $password = openssl_encrypt($this->input->post("newpass"), "AES-128-ECB", SECRETKEY);
            $user_data = array(
                "user_password" =>$password,
                "user_updatedDTTM" =>date("Y-m-d H:i:s"),
            );
            $res = $this->user_model->UpdateUserData($user_data,"user_id = ".$this->session->userdata("entrepreneur_user_id") );
            $res = array('state' => true,'error'=>'', 'msg' => 'เพิ่มสำเร็จ');
             
        }

        return $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }
}
