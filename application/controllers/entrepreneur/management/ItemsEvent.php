<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
class ItemsEvent extends Base_Entrepreneur_Controller {
    public function __construct()
    {    	
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model("CountriesModels","countries_models", true);
        $this->load->model("UserModel","user_model", true);
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("SearchModels","search_models", true);
        $this->load->model("ProgramTourModels","program_tour_models", true);

        if(strcmp($this->session->userdata('lang'),"EN") == 0)
            $this->lang->load("english","english");
        else
            $this->lang->load("thailand","thailand");
    }
    public function DeleteRoom()
    {
        $trans_status = $this->items_models->DeleteItemData("Room","room_id = ".$this->input->post("room_id"));
    }
    public function DeleteProduct()
    {
        $trans_status = $this->items_models->DeleteItemData("Product","product_id = ".$this->input->post("product_id"));
    }
    public function GetItemsData()
    {
        $res = $this->items_models->getApprovalItemData("i.items_id = ".$this->input->post("items_id"));
        return $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }
    
}