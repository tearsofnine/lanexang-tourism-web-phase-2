<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
class Chat extends Base_Entrepreneur_Controller {
    public function __construct()
    {    	
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        
        // $this->load->model("CountriesModels","countries_models", true);
        $this->load->model("UserModel","user_model", true);
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("ProgramTourModels","program_tour_models", true);
        $this->load->model("ChatModels","chat_models", true);
        $this->load->model("GlobalFunctionModels","global_function_models", true);

        if(strcmp($this->session->userdata('lang'),"EN") == 0)
            $this->lang->load("english","english");
        else
            $this->lang->load("thailand","thailand");

    }
    public function index()
	{
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;
        $chatroom_data = $this->chat_models->get_chat_room_for_table("(UserSend_user_id = ".$this->session->userdata('entrepreneur_user_id')." OR UserReceive_user_id = ".$this->session->userdata('entrepreneur_user_id').") AND chatroom_id = ".$this->input->get("chatroom_id"),$this->session->userdata('entrepreneur_user_id'));
        
        if(count($chatroom_data) != 0)
        {
            $content['chatroom_id'] = $this->input->get("chatroom_id");
            $content['name_user'] = $chatroom_data[0]->UserReceiv[0]->user_firstName." ".$chatroom_data[0]->UserReceiv[0]->user_lastName;
            $this->load->view('Entrepreneur/ChatView',$content);
        }  
        else
        {
            $chatroom_data_null_messages = $this->chat_models->get_chat_room("(UserSend_user_id = ".$this->session->userdata('entrepreneur_user_id')." OR UserReceive_user_id = ".$this->session->userdata('entrepreneur_user_id').") AND chatroom_id = ".$this->input->get("chatroom_id"),-1,-1);
            if(count($chatroom_data_null_messages) != 0)
            {
                $content['chatroom_id'] = $this->input->get("chatroom_id");
                $userDate =  $this->user_model->GetCheckUserData("user_id = ".($chatroom_data_null_messages[0]->UserSend_user_id == intval($this->session->userdata('entrepreneur_user_id')?$chatroom_data_null_messages[0]->UserSend_user_id:$chatroom_data_null_messages[0]->UserReceive_user_id )));
                $content['name_user'] = $userDate[0]->user_firstName." ".$userDate[0]->user_lastName;
                $this->load->view('Entrepreneur/ChatView',$content);
            }
            else
                redirect('./Notifications');
        } 
    }
    public function setChatDataForTable()
    {
        $this->chat_models->update_messages(array("messages_read"=>1),"ChatRooms_chatroom_id = ".$this->input->post("chatroom_id")." AND User_user_id != ".$this->session->userdata('entrepreneur_user_id'));
        return $this->output->set_content_type('application/json')->set_output(json_encode($this->chat_models->get_messages("msg.ChatRooms_chatroom_id = ".$this->input->post("chatroom_id"))));
    }
    public function getChatRoom()
    {
        return $this->output->set_content_type('application/json')->set_output( json_encode( $this->chat_models->get_chat_room("chatroom_id = ".$this->input->post("chatroom_id"),-1,-1)));
    }
}