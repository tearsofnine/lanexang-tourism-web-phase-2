<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
class Main extends Base_Entrepreneur_Controller {
    public function __construct()
    {    	
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        
        // $this->load->model("CountriesModels","countries_models", true);
        $this->load->model("UserModel","user_model", true);
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("ProgramTourModels","program_tour_models", true);
        $this->load->model("BookingModels","booking_models", true);
        $this->load->model("PaymentTransactionModels","payment_transaction_models", true);
        $this->load->model("ChatModels","chat_models", true);

        if(strcmp($this->session->userdata('lang'),"EN") == 0)
            $this->lang->load("english","english");
        else
            $this->lang->load("thailand","thailand");
    }
    public function index()
	{
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;

        $content['countBookingData'] = 0;
        if(intval($this->session->userdata('business_type_category_id')) != 1)
        {
            $item = $this->items_models->GetDefaultItemsData("i.User_user_id = ".$this->session->userdata('entrepreneur_user_id'));
            if($item)
            {
                switch (intval($this->session->userdata('business_type_category_id'))) 
                {
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        $bookingData = $this->booking_models->GetBookingDataOrderDetails("od.Items_items_id = ".$item[0]->items_id." AND pt.PurchaseStatus_purchaseStatus_id = 2");
                        $content['countBookingData'] += count($bookingData);
                        break;
                    case 5:
                        $bookingData = $this->booking_models->GetBookingDataPaymentTransaction("b.Items_items_id = ".$item[0]->items_id." AND pt.PurchaseStatus_purchaseStatus_id = 2");
                        $content['countBookingData'] += count($bookingData);
                        break;
                    case 9:
                    case 10:
                    case 11:
                        for($i=0;$i<count($item);$i++)
                        {
                            $bookingData = $this->booking_models->GetBookingDataPaymentTransaction("b.Items_items_id = ".$item[$i]->items_id." AND pt.PurchaseStatus_purchaseStatus_id = 2");
                            $content['countBookingData'] += count($bookingData);
                        }
                     break;
                }
            }
            // else
            // {
            //     $content['countBookingData'] = 0;
            // }
            
        }    
        else
        {
            $item = $this->program_tour_models->GetProgramTourData("pt.User_user_id = ".$this->session->userdata('entrepreneur_user_id'));
        }
            

        if(count($item) != 0)
        {
            if(intval($this->session->userdata('business_type_category_id')) != 1)
            {
                if(intval($item[0]->items_isActive) == 1)
                {
                    $this->load->view('Entrepreneur/MainView',$content);
                }
                else
                {
                    $content['isActive'] = 0;
                    switch (intval($this->session->userdata('business_type_category_id'))) 
                    {
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                            $this->load->view('Entrepreneur/GettingStartedView',$content);
                            break;
                        case 9:
                        case 10:
                        case 11:
                            $this->load->view('Entrepreneur/MainView',$content);
                         break;
                    }
                }
            }
            else
            {
                if(intval($item[0]->programTour_isActive) == 1)
                {
                    $this->load->view('Entrepreneur/MainView',$content);
                }
                else
                {
                    $content['isActive'] = 0;
                    $this->load->view('Entrepreneur/GettingStartedView',$content);
                }
            }
        }
        else
        {
            $content['isActive'] = -1;
            $this->load->view('Entrepreneur/GettingStartedView',$content);
        }
    }
    
    public function get_add_type_page()
    {
        switch (intval($this->session->userdata('business_type_category_id'))) {
            case 1:
                $res = array('state' => true,'error'=>'', 'typepage' => 'business/ProgramTour/addProgramTour');
                break;
            case 2:
                $res = array('state' => true,'error'=>'', 'typepage' => 'business/Attractions/addAttractions');
                break;
            case 3:
                $res = array('state' => true,'error'=>'', 'typepage' => 'business/Restaurant/addRestaurant');
                break;
            case 4:
                $res = array('state' => true,'error'=>'', 'typepage' => 'business/Shopping/addShopping');
                break;
            case 5:
                $res = array('state' => true,'error'=>'', 'typepage' => 'business/Hotel/addHotel');
                break;
            case 9:
                $res = array('state' => true,'error'=>'', 'typepage' => 'business/PackageTours/addPackageTours');
                break;
            case 10:
                $res = array('state' => true,'error'=>'', 'typepage' => 'business/EventTicket/addEventTicket');
                break;
            case 11:
                $res = array('state' => true,'error'=>'', 'typepage' => 'business/CarRent/addCarRent');
                break;
            default:
            $res = array('state' => true,'error'=>'', 'typepage' => 'Main');
                break;
        }
        
        return $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }

    public function get_information_type_page()
    {
        switch (intval($this->session->userdata('business_type_category_id'))) {
            case 1:
                $res = array('state' => true,'error'=>'', 'typepage' => 'business/ProgramTour');
                break;
            case 2:
                $res = array('state' => true,'error'=>'', 'typepage' => 'business/Attractions');
                break;
            case 3:
                $res = array('state' => true,'error'=>'', 'typepage' => 'business/Restaurant');
                break;
            case 4:
                $res = array('state' => true,'error'=>'', 'typepage' => 'business/Shopping');
                break;
            case 5:
                $res = array('state' => true,'error'=>'', 'typepage' => 'business/Hotel');
                break;
            case 9:
                $res = array('state' => true,'error'=>'', 'typepage' => 'business/PackageTours');
                break;
            case 10:
                $res = array('state' => true,'error'=>'', 'typepage' => 'business/EventTicket');
                break;
            case 11:
                $res = array('state' => true,'error'=>'', 'typepage' => 'business/CarRent');
                break;
            default:
            $res = array('state' => true,'error'=>'', 'typepage' => 'Main');
                break;
        }
        
        return $this->output->set_content_type('application/json')->set_output(json_encode($res));
    }

}