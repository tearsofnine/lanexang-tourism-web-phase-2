<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Bangkok');
class Register extends CI_Controller {
    public function __construct()
    {    	
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        
        
        $this->load->model("UserModel","user_model", true);
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("CountriesModels","countries_models", true);

        if(strcmp($this->session->userdata('lang'),"EN") == 0)
            $this->lang->load("english","english");
        else
            $this->lang->load("thailand","thailand");

    }
    public function index()
	{
        $content['host'] ="/dasta_thailand/";
        $content['ENVIRONMENT'] = ENVIRONMENT;

        $this->load->view('Entrepreneur/RegisterView',$content);
    }

    public function Registration()
    {
        $this->load->library('form_validation');
        
        $config['upload_path'] = './assets/img/uploadfile/';
        $config['allowed_types'] = 'jpg|png|jpeg';
        //$config['max_size'] = 2048; 
        $config['encrypt_name'] = true; 
        $this->load->library('upload', $config);

        $this->form_validation->set_rules('business_nameThai', 'required|business_nameThai|business_nameThai|กรุณากรอกชื่อสถานที่ประกอบการ', 'trim|required');
        $this->form_validation->set_rules('namecategory', 'required|namecategory|namecategory|กรุณาเลือกประเภทกลุ่มผู้ประกอบการ', 'trim|required');
        $this->form_validation->set_rules('user_presentAddress', 'required|user_presentAddress|user_presentAddress|กรุณากรอกที่อยู่', 'trim|required');
        
        $this->form_validation->set_rules('country', 'required|country|country|กรุณาเลือกที่ตั้งประเทศ', 'trim|required');
        $this->form_validation->set_rules('provinces', 'required|provinces|provinces|กรุณาเลือกที่ตั้งจังหวัด', 'trim|required');
        $this->form_validation->set_rules('districts', 'required|districts|districts|กรุณาเลือกที่ตั้งอำเภอ', 'trim|required');
        $this->form_validation->set_rules('subdistricts', 'required|subdistricts|subdistricts|กรุณาเลือกที่ตั้งตำบล', 'trim|required');
        
        $this->form_validation->set_rules('business_latitude', 'required|business_latitude|business_latitude|กรุณากรอก Latitude', 'trim|required');
        $this->form_validation->set_rules('business_longitude', 'required|business_longitude|business_longitude|กรุณากรอก Longitude', 'trim|required');
        $this->form_validation->set_rules('business_phone', 'required|business_phone|business_phone|กรุณากรอกหมายเลขโทรศัพท์ (ของสถานประกอบการ)', 'trim|required');
        $this->form_validation->set_rules('user_firstName', 'required|user_firstName|user_firstName|กรุณากรอก ชื่อ', 'trim|required');
        $this->form_validation->set_rules('user_lastName', 'required|user_lastName|user_lastName|กรุณากรอก นามสกุล ', 'trim|required');
        $this->form_validation->set_rules('user_phone', 'required|user_phone|user_phone|กรุณากรอก หมายเลขโทรศัพท์ของท่าน', 'trim|required');
        $this->form_validation->set_rules('user_account', 'required|user_account|user_account|กรุณากรอกชื่อผู้ใช้', 'trim|required');
        $this->form_validation->set_rules('user_account', 'required|user_account|user_account|', 'trim|min_length[5]');
        $this->form_validation->set_rules('user_password', 'required|user_password|user_password|กรุณากรอกรหัสผ่านผู้ใช้', 'trim|required');
        $this->form_validation->set_rules('user_password_re', 'required|user_password_re|user_password_re|confirm password must match password.', 'required|matches[user_password]');
        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {   
            $check_user_account = $this->user_model->GetCheckUserData("user_account = '".$this->input->post("user_account")."'");
            if(count($check_user_account) != 0)
            {
                $res = array('state' => false,'error'=>'validation', 'msg' => "<p>The required|user_account|user_account|user account already exists.</p>\n");
            }
            else
            {
                $business_license_paths ="";
                $user_profile_pic_url ="";

                $password = openssl_encrypt($this->input->post("user_password"), "AES-128-ECB", SECRETKEY);

                if(!empty($_FILES['business_license_paths']['name'][0]))
                {
                    $this->upload->do_upload('business_license_paths');
                    $upload_data = $this->upload->data();
                    $business_license_paths = $upload_data["file_name"];
                }
                if(!empty($_FILES['user_profile_pic_url']['name'][0]))
                {
                    $this->upload->do_upload('user_profile_pic_url');
                    $upload_data = $this->upload->data();
                    $user_profile_pic_url = $upload_data["file_name"];
                }
                
                $User = array(
                    "user_account" =>$this->input->post("user_account"),
                    "user_password" =>$password,
                    "user_firstName" =>$this->input->post("user_firstName"),
                    "user_lastName" =>$this->input->post("user_lastName"),
                    "user_presentAddress" =>$this->input->post("user_presentAddress"),
                    "user_phone" =>$this->input->post("user_phone"),
                    "user_email" =>$this->input->post("user_email"),
                    "user_profile_pic_url" =>$user_profile_pic_url,
                    "UserRole_userRole_id" =>4,
                    "Country_country_id" =>$this->input->post("country"),
                    "user_isActive" =>0,
                    "user_createdDTTM" =>date("Y-m-d H:i:s"),
                );
                $inserted_id_user = $this->user_model->AddUserBusinessData('User',$User);

                $Business = array(
                    "business_nameThai" =>$this->input->post("business_nameThai"),
                    "business_nameEnglish" =>$this->input->post("business_nameEnglish"),
                    "business_nameChinese" =>$this->input->post("business_nameChinese"),
                    "business_nameLaos" =>$this->input->post("business_nameLaos"),
                    "business_latitude" =>$this->input->post("business_latitude"),
                    "business_longitude" =>$this->input->post("business_longitude"),
                    "business_presentAddress" =>$this->input->post("user_presentAddress"),
                    "business_operator_number" =>$this->input->post("business_operator_number"),
                    "business_phone" =>$this->input->post("business_phone"),
                    "business_www" =>$this->input->post("business_www"),
                    "business_facebook" =>$this->input->post("business_facebook"),
                    "business_line" =>$this->input->post("business_line"),
                    "business_license_paths" =>$business_license_paths,
                    "User_user_id" =>$inserted_id_user,
                    "Subdistricts_subdistricts_id" =>$this->input->post("subdistricts"),
                    "business_type_category_id" =>$this->input->post("namecategory"),
                );

                $inserted_id_business = $this->user_model->AddUserBusinessData('Business',$Business);
                $res = array('state' => true,'error'=>'', 'msg' => 'เพิ่มสำเร็จ');
            }
        }

        return $this->output->set_content_type('application/json')->set_output(json_encode($res));

    }

    public function getNameCategory()
    {
        $NameCategory = $this->items_models->getCategory("2,3,4,5,9,10,11");
        echo "<option value=\"\">ประเภทกลุ่มผู้ประกอบการ</option>";
        foreach ($NameCategory->result() as $item)
        {
            echo "<option value=\"".$item->category_id."\">".$item->category_thai."</option>";
        }
    }

    public function getCountry()
    {
        $countries=$this->countries_models->getCountry(true,"");
        echo "<option value=\"\">ประเทศ</option>";
        foreach ($countries as $item)
        {
            echo "<option value=\"".$item->country_id."\">".$item->country_thai."</option>";
        }
    }
    public function getProvinces()
    {
        $provinces=$this->countries_models->getProvinces(true,array('Country_country_id' => $this->input->post('getOption')),"");
        echo "<option value=\"\">จังหวัด</option>";
        foreach ($provinces as $item)
        {
            echo "<option value=\"".$item->provinces_id."\">".$item->provinces_thai."</option>";
        }
    }
    public function getDistricts()
    {
        $districts=$this->countries_models->getDistricts(array('Provinces_provinces_id' => $this->input->post('getOption')),"");
        echo "<option value=\"\">อำเภอ</option>";
        foreach ($districts as $item)
        {
            echo "<option value=\"".$item->districts_id."\">".$item->districts_thai."</option>";
        }
    }
    public function getSubdistricts()
    {
        $districts=$this->countries_models->getSubdistricts(array('Districts_districts_id' => $this->input->post('getOption')),"");
        echo "<option value=\"\">ตำบล</option>";
        foreach ($districts as $item)
        {
            echo "<option value=\"".$item->subdistricts_id."\">".$item->subdistricts_thai."</option>";
        }
    }
    
}