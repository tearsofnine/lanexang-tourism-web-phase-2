<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
date_default_timezone_set('Asia/Bangkok');
class ProgramTour extends REST_Controller
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->helper('file');
        $this->load->library('form_validation');
        $this->db->db_debug = false;
        $this->load->model("UserModel","user_model", true);
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("SearchModels","search_models", true);
        $this->load->model("ProgramTourModels","program_tour_models", true);
    }

    public function index_get()
    {
        $segment_category_id = 5;
        $data = new stdClass();
        if($this->uri->segment($segment_category_id))
        {
            if($this->get('user_id') && $this->get('user_id') != "")
            {
                $data->ProgramTour = $this->program_tour_models->GetProgramTourData("
                pt.programTour_isActive = 1 AND pt.programTour_isPublish = 1 AND pt.PublishStatus_publishStatus_id = 1 AND pt.programTour_isSaveDraft = 0
                AND sc.subcategory_id = ".$this->uri->segment($segment_category_id)." AND (
                NOW() BETWEEN pt.programTour_time_period_start AND pt.programTour_time_period_end OR (pt.programTour_time_period_start = '1970-01-01' AND pt.programTour_time_period_end = '1970-01-01' )
                )",$this->get('user_id'));
            }
            else
            {
                $data->ProgramTour = $this->program_tour_models->GetProgramTourData("
                pt.programTour_isActive = 1 AND pt.programTour_isPublish = 1 AND pt.PublishStatus_publishStatus_id = 1 AND pt.programTour_isSaveDraft = 0
                AND sc.subcategory_id = ".$this->uri->segment($segment_category_id)." AND (
                NOW() BETWEEN pt.programTour_time_period_start AND pt.programTour_time_period_end OR (pt.programTour_time_period_start = '1970-01-01' AND pt.programTour_time_period_end = '1970-01-01' )
                )");
            }
            
        }
        else if($this->get('programTour_id') && $this->get('programTour_id') != "")
        {
            if($this->get('user_id') && $this->get('user_id') != "")
            {
                $data->ProgramTour = $this->program_tour_models->GetProgramTourData("pt.programTour_id = ".$this->get('programTour_id')." AND 
                pt.programTour_isActive = 1 AND pt.programTour_isPublish = 1 AND pt.PublishStatus_publishStatus_id = 1 AND pt.programTour_isSaveDraft = 0 AND (
                NOW() BETWEEN pt.programTour_time_period_start AND pt.programTour_time_period_end OR (pt.programTour_time_period_start = '1970-01-01' AND pt.programTour_time_period_end = '1970-01-01' )
                )",$this->get('user_id'));
            }
            else
            {
                $data->ProgramTour = $this->program_tour_models->GetProgramTourData("pt.programTour_id = ".$this->get('programTour_id')." AND 
                pt.programTour_isActive = 1 AND pt.programTour_isPublish = 1 AND pt.PublishStatus_publishStatus_id = 1 AND pt.programTour_isSaveDraft = 0 AND (
                NOW() BETWEEN pt.programTour_time_period_start AND pt.programTour_time_period_end OR (pt.programTour_time_period_start = '1970-01-01' AND pt.programTour_time_period_end = '1970-01-01' )
                )");
            }
        }
        elseif($this->get('user_id') && $this->get('user_id') != "")
        {
            $data->ProgramTour = $this->program_tour_models->GetProgramTourData("
            pt.programTour_isActive = 1 AND pt.programTour_isPublish = 1 AND pt.PublishStatus_publishStatus_id = 1 AND pt.programTour_isSaveDraft = 0 AND (
            NOW() BETWEEN pt.programTour_time_period_start AND pt.programTour_time_period_end OR (pt.programTour_time_period_start = '1970-01-01' AND pt.programTour_time_period_end = '1970-01-01' )
            )",$this->get('user_id'));
        }
        else
        {
            $data->ProgramTour = $this->program_tour_models->GetProgramTourData("
            pt.programTour_isActive = 1 AND pt.programTour_isPublish = 1 AND pt.PublishStatus_publishStatus_id = 1 AND pt.programTour_isSaveDraft = 0 AND (
            NOW() BETWEEN pt.programTour_time_period_start AND pt.programTour_time_period_end OR (pt.programTour_time_period_start = '1970-01-01' AND pt.programTour_time_period_end = '1970-01-01' )
            )");
        }
        $this->response($data, REST_Controller::HTTP_OK);
    }
    
    public function getEditProgramTour_get()
    {
        $data = new stdClass();
        $user_id = $this->get('user_id');
        $programTour_id = $this->get('programTour_id');
        $data_for_validation = array(
            'user_id' => $this->get('user_id'),
            'programTour_id' => $this->get('programTour_id') ,
            
        );
        $this->form_validation->set_data($data_for_validation);

        $this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
        //$this->form_validation->set_rules('programTour_id', 'programTour_id', 'trim|required');

        if ($this->form_validation->run() == FALSE) 
		{
			$data->ProgramTour = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {
            $user_data = $this->user_model->GetUserEntrepreneurData("user_id = ".$user_id." AND UserRole_userRole_id = 5");
            if(count($user_data))
            {
                if($this->get('programTour_id') && $this->get('programTour_id') != "")
                {
                    $data->ProgramTour = $this->program_tour_models->GetProgramTourData("pt.programTour_isActive = 1 AND pt.programTour_id = ".$programTour_id." AND pt.User_user_id = ".$user_id);
                }
                else
                {
                    $data->ProgramTour = $this->program_tour_models->GetProgramTourData("pt.programTour_isActive = 1 AND pt.User_user_id = ".$user_id);
                }
            }
            else
            {
                $data->ProgramTour = array('state' => false,'error'=>'get data', 'msg' => "user not data");
            }
            
        }

        $this->response($data, REST_Controller::HTTP_OK);
    }
    public function deleteProgramTour_get()
    {
        if($this->get('programTour_id') && $this->get('programTour_id') != "")
        {
            $ProgramTour = array(
                "programTour_isActive" =>99,
                "programTour_isPublish" =>0,
                "programTour_updatedDTTM" => date("Y-m-d H:i:s"),
            );
            $trans_status = $this->items_models->UpdateItemData($ProgramTour,"ProgramTour","programTour_id = ".$this->input->get("programTour_id"));
        }
        else
        {
            $trans_status = [];
        }
        $this->response($trans_status, REST_Controller::HTTP_OK);
    }
    public function getNameItemCategory_get()
    {
        $data = new stdClass();
        $NameCategory= $this->items_models->getCategory("2,3,4,5");
        $data->Category =$NameCategory->result();
        $this->response($data, REST_Controller::HTTP_OK);
    }
    public function getNameSubCategoryById_get()
    {
        $data = new stdClass();
        if($this->get('category_id') && $this->get('category_id') != "")
        {
            $NameCategorySubCategory = $this->items_models->getNameCategorySubCategory($this->get('category_id') );
            $data->SubCategory =$NameCategorySubCategory->result();
        }
        else
        {
            $data->SubCategory =[];
        }
        $this->response($data, REST_Controller::HTTP_OK);
    }
    
   
    public function saveNewProgramTour_post()
    {

        // $input = $this->input->post();
        $data = file_get_contents('php://input');
        $datajson_encode = json_decode(urldecode($data));

        $data_for_validation = array(
            'programTour_nameThai' => $datajson_encode->ProgramTour->programTour_nameThai,
            'subcategory_id' => $datajson_encode->ProgramTour->subcategory_id,
            
        );
        $this->form_validation->set_data($data_for_validation);

        $this->form_validation->set_rules('programTour_nameThai', 'programTour_nameThai', 'trim|required');
        $this->form_validation->set_rules('subcategory_id', 'subcategory_id', 'trim|required');

        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {
            $programTour_time_period_start = date('Y-m-d',strtotime("1970-01-01"));
            $programTour_time_period_end = date('Y-m-d',strtotime("1970-01-01"));

            $ProgramTour = array(
                "programTour_nameThai" =>$datajson_encode->ProgramTour->programTour_nameThai,
                "programTour_nameEnglish" =>$datajson_encode->ProgramTour->programTour_nameEnglish,
                "programTour_nameChinese" =>$datajson_encode->ProgramTour->programTour_nameChinese,
                "programTour_nameLaos" =>$datajson_encode->ProgramTour->programTour_nameLaos,

                "programTour_interestingThingsThai" =>$datajson_encode->ProgramTour->programTour_interestingThingsThai,
                "programTour_interestingThingsEnglish" =>$datajson_encode->ProgramTour->programTour_interestingThingsEnglish,
                "programTour_interestingThingsChinese" =>$datajson_encode->ProgramTour->programTour_interestingThingsChinese,
                "programTour_interestingThingsLaos" =>$datajson_encode->ProgramTour->programTour_interestingThingsLaos,

                "programTour_isActive" =>1,
                "MenuItem_menuItem_id" =>1,
                "PublishStatus_publishStatus_id" =>0,
                "programTour_isPublish" =>0,
                "programTour_isSaveDraft" =>$datajson_encode->ProgramTour->programTour_isSaveDraft,
                "programTour_createdDTTM" =>date("Y-m-d H:i:s"),

                "programTour_time_period_start" =>$programTour_time_period_start,
                "programTour_time_period_end" =>$programTour_time_period_end,

                "User_user_id" =>$datajson_encode->ProgramTour->user_id
            );

            $inserted_id_program_tour = $this->program_tour_models->AddProgramTourData($ProgramTour,"ProgramTour");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if($datajson_encode->ProgramTour->coverItem_paths == "")
            {
                $originalImage = "p3.jpg";
            }
            else
            {
                // image name generating with random number with 32 characters
                $image_name = md5(uniqid(rand(), true));
                $filename = $image_name.'.'. 'jpg';
                $originalImage = $filename;
                $data_img = $datajson_encode->ProgramTour->coverItem_paths;
                $data_img = base64_decode($data_img);
                file_put_contents("./assets/img/uploadfile/".$filename, $data_img);
            }
            $CoverItems = array(
                "coverItem_paths" =>$originalImage,
                "coverItem_url" =>"",
                "ProgramTour_programTour_id" =>$inserted_id_program_tour
            );
            $inserted_cover_program_tour_item = $this->program_tour_models->AddProgramTourData($CoverItems,"CoverItems");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if($datajson_encode->ProgramTour->coverItem_paths == "")
            {
                $originalImage = "p3.jpg";
            }
            else
            {
                // image name generating with random number with 32 characters
                $image_name = md5(uniqid(rand(), true));
                $filename = $image_name .'.'. 'jpg';
                $originalImage = $filename;
                $data_img = $datajson_encode->ProgramTour->coverItem_paths;
                $data_img = base64_decode($data_img);
                file_put_contents("./assets/img/uploadfile/".$filename, $data_img);
            }

            $TempImageCover = array(
                "tempImageCover_paths" =>$originalImage,
                "tempImageCover_CropData" =>'{"x":0,"y":0,"width":750,"height":421.875,"rotate":0,"scaleX":1,"scaleY":1}',
                "tempImageCover_CropBoxData" =>'{"left":0,"top":15.77066666666667,"width":502,"height":282.375,"naturalWidth":750,"naturalHeight":421.875}',
                "tempImageCover_CanvasData" =>'{"left":0,"top":15.77066666666667,"width":502,"height":282.45866666666666,"naturalWidth":750,"naturalHeight":422}',
                "CoverItems_coverItem_id" =>$inserted_cover_program_tour_item
            );
            $this->program_tour_models->AddProgramTourData($TempImageCover,"TempImageCover");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $ProgramTour_has_SubCategory = array(
                "SubCategory_subcategory_id" =>$datajson_encode->ProgramTour->subcategory_id,
                "ProgramTour_programTour_id" =>$inserted_id_program_tour
            );
            $this->program_tour_models->AddProgramTourData($ProgramTour_has_SubCategory,"ProgramTour_has_SubCategory");
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            for($i = 0; $i < count($datajson_encode->ProgramTour->DatesTrip); $i++)
            {
                $DatesTrip = array(
                    "datesTrip_topicThai" => $datajson_encode->ProgramTour->DatesTrip[$i]->datesTrip_topicThai,
                    "datesTrip_topicEnglish" => $datajson_encode->ProgramTour->DatesTrip[$i]->datesTrip_topicEnglish,
                    "datesTrip_topicChinese" => $datajson_encode->ProgramTour->DatesTrip[$i]->datesTrip_topicChinese,
                    "datesTrip_topicLaos" => $datajson_encode->ProgramTour->DatesTrip[$i]->datesTrip_topicLaos,
                    "ProgramTour_programTour_id" => $inserted_id_program_tour
                );
                $inserted_datesTrip_id = $this->program_tour_models->AddProgramTourData($DatesTrip,"DatesTrip");
                
                for ($j = 0; $j < count($datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions); $j++) 
                {
                    $TouristAttractions = array(
                        "Items_items_id" => $datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->items_id,
                        "detail_Thai" => $datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->detail_Thai,
                        "detail_English" => $datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->detail_English,
                        "detail_Chinese" => $datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->detail_Chinese,
                        "detail_Laos" => $datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->detail_Laos,
                        "DatesTrip_datesTrip_id" =>  $inserted_datesTrip_id
                    );
                    $inserted_touristAttractions_id = $this->program_tour_models->AddProgramTourData($TouristAttractions,"TouristAttractions");
                    for ($k = 0; $k < count($datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->Photo); $k++) 
                    {
                        // image name generating with random number with 32 characters
                        $image_name = md5(uniqid(rand(), true));
                        $filename = $image_name .'.'. 'jpg';
                        $originalImage = $filename;
                        $data_img = $datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->Photo[$k]->photo_paths;
                        $data_img = base64_decode($data_img);
                        file_put_contents("./assets/img/uploadfile/".$filename, $data_img);

                        $PhotoTourist = array(
                            "photoTourist_paths" =>$filename,
                            "photoTourist_topicThai" =>$datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->Photo[$k]->photo_textThai,
                            "photoTourist_topicEnglish" =>$datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->Photo[$k]->photo_textEnglish, 
                            "photoTourist_topicChinese" =>$datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->Photo[$k]->photo_textChinese, 
                            "photoTourist_topicLaos" =>$datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->Photo[$k]->photo_textLaos, 
                            "TouristAttractions_id" =>$inserted_touristAttractions_id
                        );
                        $this->program_tour_models->AddProgramTourData($PhotoTourist,"PhotoTourist");
                    }
                } 
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $res = array(
                'msg' => "Add ProgramTour successfully",
                'state' => true,
            );
        }
        $this->response($res, REST_Controller::HTTP_OK);
    }
    public function saveEditProgramTour_post()
    {
        // $input = $this->input->post();
        $data = file_get_contents('php://input');
        $datajson_encode = json_decode(urldecode($data));

        $data_for_validation = array(
            'programTour_nameThai' => $datajson_encode->ProgramTour->programTour_nameThai,
            'subcategory_id' => $datajson_encode->ProgramTour->subcategory_id,
            'programTour_id' => $datajson_encode->ProgramTour->programTour_id,
            
        );
        $this->form_validation->set_data($data_for_validation);

        $this->form_validation->set_rules('programTour_nameThai', 'programTour_nameThai', 'trim|required');
        $this->form_validation->set_rules('subcategory_id', 'subcategory_id', 'trim|required'); 
        $this->form_validation->set_rules('programTour_id', 'programTour_id', 'trim|required');
        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        { 
            $programTour_time_period_start = date('Y-m-d',strtotime("1970-01-01"));
            $programTour_time_period_end = date('Y-m-d',strtotime("1970-01-01"));

            $programTour_id = $datajson_encode->ProgramTour->programTour_id;
            $programTour_models_data =  $this->program_tour_models->GetProgramTourData("pt.programTour_id = ".$$programTour_id);
           
            $ProgramTour = array(
                "programTour_nameThai" =>$datajson_encode->ProgramTour->programTour_nameThai,
                "programTour_nameEnglish" =>$datajson_encode->ProgramTour->programTour_nameEnglish,
                "programTour_nameChinese" =>$datajson_encode->ProgramTour->programTour_nameChinese,
                "programTour_nameLaos" =>$datajson_encode->ProgramTour->programTour_nameLaos,

                "programTour_interestingThingsThai" =>$datajson_encode->ProgramTour->programTour_interestingThingsThai,
                "programTour_interestingThingsEnglish" =>$datajson_encode->ProgramTour->programTour_interestingThingsEnglish,
                "programTour_interestingThingsChinese" =>$datajson_encode->ProgramTour->programTour_interestingThingsChinese,
                "programTour_interestingThingsLaos" =>$datajson_encode->ProgramTour->programTour_interestingThingsLaos,

                "programTour_isActive" =>1,
                "MenuItem_menuItem_id" =>1,
                "PublishStatus_publishStatus_id" =>0,
                "programTour_isPublish" =>0,
                "programTour_isSaveDraft" =>$datajson_encode->ProgramTour->programTour_isSaveDraft,
                "programTour_updatedDTTM" =>date("Y-m-d H:i:s"),

                "programTour_time_period_start" =>$programTour_time_period_start,
                "programTour_time_period_end" =>$programTour_time_period_end,

                // "User_user_id" =>$datajson_encode->ProgramTour->user_id
            );
            $this->program_tour_models->UpdateProgramTourData($ProgramTour,"ProgramTour","programTour_id = ".$programTour_id);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if(intval($datajson_encode->ProgramTour->coverItem_state) == 1)
            {
                $coverItem_paths = './assets/img/uploadfile/'. $programTour_models_data[0]->coverItem_paths;
                $tempImageCover_paths = './assets/img/uploadfile/'. $programTour_models_data[0]->tempImageCover_paths;
                is_readable($coverItem_paths);
                unlink($coverItem_paths);
                if($programTour_models_data[0]->tempImageCover_paths != "p3.jpg")
                {
                    is_readable($tempImageCover_paths);
                    unlink($tempImageCover_paths);
                }
                $trans_status = $this->program_tour_models->DeleteProgramTourData("CoverItems","ProgramTour_programTour_id = ".$programTour_id);
            
                if($datajson_encode->ProgramTour->coverItem_paths == "")
                {
                    $originalImage = "p3.jpg";
                }
                else
                {
                    // image name generating with random number with 32 characters
                    $image_name = md5(uniqid(rand(), true));
                    $filename = $image_name.'.'. 'jpg';
                    $originalImage = $filename;
                    $data_img = $datajson_encode->ProgramTour->coverItem_paths;
                    $data_img = base64_decode($data_img);
                    file_put_contents("./assets/img/uploadfile/".$filename, $data_img);
                }
                $CoverItems = array(
                    "coverItem_paths" =>$originalImage,
                    "coverItem_url" =>"",
                    "ProgramTour_programTour_id" =>$programTour_id
                );
                $inserted_cover_program_tour_item = $this->program_tour_models->AddProgramTourData($CoverItems,"CoverItems");
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                if($datajson_encode->ProgramTour->coverItem_paths == "")
                {
                    $originalImage = "p3.jpg";
                }
                else
                {
                    // image name generating with random number with 32 characters
                    $image_name = md5(uniqid(rand(), true));
                    $filename = $image_name .'.'. 'jpg';
                    $originalImage = $filename;
                    $data_img = $datajson_encode->ProgramTour->coverItem_paths;
                    $data_img = base64_decode($data_img);
                    file_put_contents("./assets/img/uploadfile/".$filename, $data_img);
                }

                $TempImageCover = array(
                    "tempImageCover_paths" =>$originalImage,
                    "tempImageCover_CropData" =>'{"x":0,"y":0,"width":750,"height":421.875,"rotate":0,"scaleX":1,"scaleY":1}',
                    "tempImageCover_CropBoxData" =>'{"left":0,"top":15.77066666666667,"width":502,"height":282.375,"naturalWidth":750,"naturalHeight":421.875}',
                    "tempImageCover_CanvasData" =>'{"left":0,"top":15.77066666666667,"width":502,"height":282.45866666666666,"naturalWidth":750,"naturalHeight":422}',
                    "CoverItems_coverItem_id" =>$inserted_cover_program_tour_item
                );
                $this->program_tour_models->AddProgramTourData($TempImageCover,"TempImageCover");
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $ProgramTour_has_SubCategory = array(
                "SubCategory_subcategory_id" =>$datajson_encode->ProgramTour->subcategory_id
            );
            $this->program_tour_models->UpdateProgramTourData($ProgramTour_has_SubCategory,"ProgramTour_has_SubCategory","ProgramTour_programTour_id = ".$programTour_id);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $temp_id_datesTrip = array();            
            $temp_id_touristAttractions = array();            
            $temp_id_photoTourist = array();

            for($i = 0; $i < count($datajson_encode->ProgramTour->DatesTrip); $i++)
            {
                if(intval($datajson_encode->ProgramTour->DatesTrip[$i]->datesTrip_state) == 0)
                {
                    $DatesTrip = array(
                        "datesTrip_topicThai" => $datajson_encode->ProgramTour->DatesTrip[$i]->datesTrip_topicThai,
                        "datesTrip_topicEnglish" => $datajson_encode->ProgramTour->DatesTrip[$i]->datesTrip_topicEnglish,
                        "datesTrip_topicChinese" => $datajson_encode->ProgramTour->DatesTrip[$i]->datesTrip_topicChinese,
                        "datesTrip_topicLaos" => $datajson_encode->ProgramTour->DatesTrip[$i]->datesTrip_topicLaos,
                    );
                    $this->program_tour_models->UpdateProgramTourData($DatesTrip,"DatesTrip","datesTrip_id = ".$datajson_encode->ProgramTour->DatesTrip[$i]->datesTrip_id);
                    $temp_id_datesTrip[] = $datajson_encode->ProgramTour->DatesTrip[$i]->datesTrip_id;
                    for ($j = 0; $j < count($datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions); $j++) 
                    {
                        if(intval($datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->touristAttractions_state) == 0)
                        {
                            $TouristAttractions = array(
                                "Items_items_id" => $datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->items_id,
                                "detail_Thai" => $datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->detail_Thai,
                                "detail_English" => $datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->detail_English,
                                "detail_Chinese" => $datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->detail_Chinese,
                                "detail_Laos" => $datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->detail_Laos,
                            );
                            $this->program_tour_models->UpdateProgramTourData($TouristAttractions,"TouristAttractions","touristAttractions_id = ".$datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->touristAttractions_id);
                            $temp_id_touristAttractions[] = $datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->touristAttractions_id;

                            for ($k = 0; $k < count($datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->Photo); $k++) 
                            {
                                if(intval($datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->Photo[$k]->photo_state) == 0)
                                {
                                    $PhotoTourist = array(
                                        "photoTourist_topicThai" =>$datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->Photo[$k]->photo_textThai,
                                        "photoTourist_topicEnglish" =>$datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->Photo[$k]->photo_textEnglish, 
                                        "photoTourist_topicChinese" =>$datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->Photo[$k]->photo_textChinese, 
                                        "photoTourist_topicLaos" =>$datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->Photo[$k]->photo_textLaos, 
                                    );
                                    $this->program_tour_models->UpdateProgramTourData($PhotoTourist,"PhotoTourist","photoTourist_id = ".$datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->Photo[$k]->photoTourist_id);
                                    $temp_id_photoTourist[]=$datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->Photo[$k]->photoTourist_id;
                                }
                                else
                                {
                                    // image name generating with random number with 32 characters
                                    $image_name = md5(uniqid(rand(), true));
                                    $filename = $image_name .'.'. 'jpg';
                                    $originalImage = $filename;
                                    $data_img = $datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->Photo[$k]->photo_paths;
                                    $data_img = base64_decode($data_img);
                                    file_put_contents("./assets/img/uploadfile/".$filename, $data_img);
            
                                    $PhotoTourist = array(
                                        "photoTourist_paths" =>$filename,
                                        "photoTourist_topicThai" =>$datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->Photo[$k]->photo_textThai,
                                        "photoTourist_topicEnglish" =>$datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->Photo[$k]->photo_textEnglish, 
                                        "photoTourist_topicChinese" =>$datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->Photo[$k]->photo_textChinese, 
                                        "photoTourist_topicLaos" =>$datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->Photo[$k]->photo_textLaos, 
                                        "TouristAttractions_id" =>$datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->touristAttractions_id
                                    );
                                    $inserted_PhotoTourist_id = $this->program_tour_models->AddProgramTourData($PhotoTourist,"PhotoTourist");
                                    $temp_id_photoTourist[]=$inserted_PhotoTourist_id;
                                }
                            }
                        }
                        else
                        {
                            $TouristAttractions = array(
                                "Items_items_id" => $datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->items_id,
                                "detail_Thai" => $datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->detail_Thai,
                                "detail_English" => $datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->detail_English,
                                "detail_Chinese" => $datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->detail_Chinese,
                                "detail_Laos" => $datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->detail_Laos,
                                "DatesTrip_datesTrip_id" =>  $datajson_encode->ProgramTour->DatesTrip[$i]->datesTrip_id
                            );
                            $inserted_touristAttractions_id = $this->program_tour_models->AddProgramTourData($TouristAttractions,"TouristAttractions");
                            $temp_id_touristAttractions[] = $inserted_touristAttractions_id;

                            for ($k = 0; $k < count($datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->Photo); $k++) 
                            {
                                // image name generating with random number with 32 characters
                                $image_name = md5(uniqid(rand(), true));
                                $filename = $image_name .'.'. 'jpg';
                                $originalImage = $filename;
                                $data_img = $datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->Photo[$k]->photo_paths;
                                $data_img = base64_decode($data_img);
                                file_put_contents("./assets/img/uploadfile/".$filename, $data_img);
        
                                $PhotoTourist = array(
                                    "photoTourist_paths" =>$filename,
                                    "photoTourist_topicThai" =>$datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->Photo[$k]->photo_textThai,
                                    "photoTourist_topicEnglish" =>$datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->Photo[$k]->photo_textEnglish, 
                                    "photoTourist_topicChinese" =>$datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->Photo[$k]->photo_textChinese, 
                                    "photoTourist_topicLaos" =>$datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->Photo[$k]->photo_textLaos, 
                                    "TouristAttractions_id" =>$inserted_touristAttractions_id
                                );
                                $this->program_tour_models->AddProgramTourData($PhotoTourist,"PhotoTourist");
                            }
                        }
                       
                    }
                }
                else
                {
                    $DatesTrip = array(
                        "datesTrip_topicThai" => $datajson_encode->ProgramTour->DatesTrip[$i]->datesTrip_topicThai,
                        "datesTrip_topicEnglish" => $datajson_encode->ProgramTour->DatesTrip[$i]->datesTrip_topicEnglish,
                        "datesTrip_topicChinese" => $datajson_encode->ProgramTour->DatesTrip[$i]->datesTrip_topicChinese,
                        "datesTrip_topicLaos" => $datajson_encode->ProgramTour->DatesTrip[$i]->datesTrip_topicLaos,
                        "ProgramTour_programTour_id" => $programTour_id
                    );
                    $inserted_datesTrip_id = $this->program_tour_models->AddProgramTourData($DatesTrip,"DatesTrip");
                    $temp_id_datesTrip[] = $inserted_datesTrip_id;

                    for ($j = 0; $j < count($datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions); $j++) 
                    {
                        $TouristAttractions = array(
                            "Items_items_id" => $datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->items_id,
                            "detail_Thai" => $datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->detail_Thai,
                            "detail_English" => $datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->detail_English,
                            "detail_Chinese" => $datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->detail_Chinese,
                            "detail_Laos" => $datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->detail_Laos,
                            "DatesTrip_datesTrip_id" =>  $inserted_datesTrip_id
                        );
                        $inserted_touristAttractions_id = $this->program_tour_models->AddProgramTourData($TouristAttractions,"TouristAttractions");
                        for ($k = 0; $k < count($datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->Photo); $k++) 
                        {
                            // image name generating with random number with 32 characters
                            $image_name = md5(uniqid(rand(), true));
                            $filename = $image_name .'.'. 'jpg';
                            $originalImage = $filename;
                            $data_img = $datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->Photo[$k]->photo_paths;
                            $data_img = base64_decode($data_img);
                            file_put_contents("./assets/img/uploadfile/".$filename, $data_img);
    
                            $PhotoTourist = array(
                                "photoTourist_paths" =>$filename,
                                "photoTourist_topicThai" =>$datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->Photo[$k]->photo_textThai,
                                "photoTourist_topicEnglish" =>$datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->Photo[$k]->photo_textEnglish, 
                                "photoTourist_topicChinese" =>$datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->Photo[$k]->photo_textChinese, 
                                "photoTourist_topicLaos" =>$datajson_encode->ProgramTour->DatesTrip[$i]->TouristAttractions[$j]->Photo[$k]->photo_textLaos, 
                                "TouristAttractions_id" =>$inserted_touristAttractions_id
                            );
                            $this->program_tour_models->AddProgramTourData($PhotoTourist,"PhotoTourist");
                        }
                    }
                }
            }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            $trans_status = $this->program_tour_models->DeleteProgramTourData("DatesTrip","datesTrip_id not in(".join(",",$temp_id_datesTrip).") AND ProgramTour_programTour_id = ".$programTour_id);
            $res = array('state' => true,'error'=>'', 'msg' => 'เพิ่มสำเร็จ');
        }
        $this->response($res, REST_Controller::HTTP_OK);
    }
    public function updateProgramTourReadCount_post()
    {
        $input = $this->input->post();
        $this->form_validation->set_rules('programTour_id', 'programTour_id', 'trim|required');
        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {
            $this->db->set('programTour_readcount', 'programTour_readcount + 1',FALSE);
            $this->db->where("programTour_id",$input["programTour_id"]);
            $this->db->update("ProgramTour");
            $this->db->trans_complete();
            if($this->db->trans_status() === FALSE)
            {
                $res = array(
                    'msg' => "Unable to edit ProgramTour",
                    'state' => false
                );
            }
            else
            {
                if($this->db->affected_rows() > 0)
                {
                    $res = array(
                        'msg' => "ProgramTour edited successfully",
                        'state' => true
                    );
                }
                else
                {
                    $res = array(
                        'msg' => "ProgramTour edited successfully, but it looks like you haven't updated anything!",
                        'state' => true
                    );
                }
            }
        }
        $this->response($res, REST_Controller::HTTP_OK);
    }
    public function SearchProgramTour_post()
    {
        $input = $this->input->post();
        $data = new stdClass();
        $this->form_validation->set_rules('str_search', 'str_search', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) 
		{
			$data->ProgramTour = array();
        }
        else
        {
            $str_search = $input["str_search"];
            $where = "(pt.programTour_nameThai like CONCAT(\"%\", \"".$str_search."\", \"%\") OR 
            pt.programTour_nameEnglish like CONCAT(\"%\", \"".$str_search."\", \"%\") OR
            pt.programTour_nameChinese like CONCAT(\"%\", \"".$str_search."\", \"%\") OR
            pt.programTour_nameLaos like CONCAT(\"%\", \"".$str_search."\", \"%\") 
            ) AND pt.programTour_isActive = 1 AND pt.programTour_isPublish = 1 AND 
            (NOW() BETWEEN pt.programTour_time_period_start AND pt.programTour_time_period_end OR (pt.programTour_time_period_start = '1970-01-01' AND pt.programTour_time_period_end = '1970-01-01' ))";
            if( $this->input->post("user_id") && $this->input->post("user_id") != "")
            {
                $data->ProgramTour = $this->program_tour_models->GetProgramTourData($where,$input["user_id"]);
            }
            else
            {
                $data->ProgramTour = $this->program_tour_models->GetProgramTourData($where,-1);
            }
        }
        $this->response($data, 200);
    }
}