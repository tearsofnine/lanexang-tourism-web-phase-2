<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
date_default_timezone_set('Asia/Bangkok');
class Adsense extends REST_Controller
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->db->db_debug = false;
        $this->load->model("MediaAndAdvertisingModels","media_and_advertising_models", true);
        $this->load->model("BusinessModels","business_models", true);
        $this->load->model("VideoContentModels","video_content_models", true);
    }
    
    public function index_get()
    {
        $segment_mediaAndAdvertisingType_id = 5;
        $data = new stdClass();

        if($this->uri->segment($segment_mediaAndAdvertisingType_id))
        {
            $data->Adsense = $this->media_and_advertising_models->GetMediaAndAdvertisingData("maa.mediaAndAdvertising_isActive = 1 AND maa.mediaAndAdvertising_isPublish = 1 AND maa.MediaAndAdvertisingType_mediaAndAdvertisingType_id = ".$this->uri->segment($segment_mediaAndAdvertisingType_id)." AND (NOW() BETWEEN maa.mediaAndAdvertising_time_period_start AND maa.mediaAndAdvertising_time_period_end OR (maa.mediaAndAdvertising_time_period_start = '1970-01-01' AND maa.mediaAndAdvertising_time_period_end = '1970-01-01' ))");
        }
        else
        {
            $data->Adsense = [];
        }
        
        $this->response($data, 200);
    }
    
    public function getReservationsAds_get()
    {
        $data = new stdClass();
        $data->Adsense = $this->media_and_advertising_models->GetMediaAndAdvertisingData("maa.mediaAndAdvertising_isActive = 1 AND maa.mediaAndAdvertising_isPublish = 1 AND maa.MediaAndAdvertisingType_mediaAndAdvertisingType_id IN (4,5,6) AND (NOW() BETWEEN maa.mediaAndAdvertising_time_period_start AND maa.mediaAndAdvertising_time_period_end OR (maa.mediaAndAdvertising_time_period_start = '1970-01-01' AND maa.mediaAndAdvertising_time_period_end = '1970-01-01' ))");
        $this->response($data, 200);
    }
    public function getVideoContent_get()
    {
        $data = new stdClass();
        $data_video_content = $this->video_content_models->GetVideoContentData("vc.videoContent_isPublish = 1 AND vc.videoContent_isActive = 1");
        foreach($data_video_content as $key => $r)
        {
            $matches = array();
            $regex = "/http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?‌​[\w\?‌​=]*)?/";
            preg_match($regex, $r->videoContent_url, $matches);
            $r->videoContent_url =  $matches[1];
            $data->VideoContent[] = $r;
        }

        $this->response($data, 200);

    }
}