<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
date_default_timezone_set('Asia/Bangkok');
class Reservations extends REST_Controller
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->db->db_debug = false;
        $this->load->model("ItemsModels","items_models", true);
        
    }
    public function index_get()
    {
        $segment_controllers = 5;
        $data = new stdClass();
        if($this->uri->segment($segment_controllers))
        {
            switch ($this->uri->segment($segment_controllers)) {
                case 'PackageTours':
                    $menuItem_id = 9;
                    break;
                case 'EventTicket':
                    $menuItem_id = 10;
                    break;
                case 'CarRent':
                    $menuItem_id = 11;
                    break;
                default:
                    $menuItem_id = 9;
                    break;
            }
            if($this->get('items_id') && $this->get('items_id') != "")
            {
                if($this->get('user_id') && $this->get('user_id') != "")
                    $data->Items = $this->items_models->GetItemReservationsData("i.items_isActive = 1 AND i.items_isPublish = 1 AND mi.menuItem_id = ".$menuItem_id." AND i.items_id = ".$this->get('items_id'),$this->uri->segment($segment_controllers),$this->get('user_id'));
                else
                    $data->Items = $this->items_models->GetItemReservationsData("i.items_isActive = 1 AND i.items_isPublish = 1 AND mi.menuItem_id = ".$menuItem_id." AND i.items_id = ".$this->get('items_id'),$this->uri->segment($segment_controllers));
            }
            else
            {
                if($this->get('user_id') && $this->get('user_id') != "")
                    $data->Items = $this->items_models->GetItemReservationsData("i.items_isActive = 1 AND i.items_isPublish = 1 AND mi.menuItem_id = ".$menuItem_id,$this->uri->segment($segment_controllers));
                else
                    $data->Items = $this->items_models->GetItemReservationsData("i.items_isActive = 1 AND i.items_isPublish = 1 AND mi.menuItem_id = ".$menuItem_id,$this->uri->segment($segment_controllers),$this->get('user_id'));
            }
            // if($this->get('user_id') && $this->get('user_id') != "")
            // {
            //     $data->Items = $this->items_models->GetItemReservationsData("i.items_isActive = 1 AND i.items_isPublish = 1 AND mi.menuItem_id = ".$menuItem_id,$this->uri->segment($segment_controllers),$this->get('user_id'));
            // }
           
        }
        // else if($this->get('items_id') && $this->get('items_id') != "")
        // {
        //     if($this->get('user_id') && $this->get('user_id') != "")
        //         $data->Items = $this->items_models->GetItemReservationsData("i.items_isActive = 1 AND i.items_isPublish = 1 AND i.items_id = ".$this->get('items_id'),$this->get('user_id'));
        //     else
        //         $data->Items = $this->items_models->GetItemReservationsData("i.items_isActive = 1 AND i.items_isPublish = 1 AND i.items_id = ".$this->get('items_id'));
        // }
        // else
        // {
        //     if($this->get('user_id') && $this->get('user_id') != "")
        //         $data->Items = $this->items_models->GetItemReservationsData("i.items_isActive = 1 AND i.items_isPublish = 1 AND mi.menuItem_id IN (9,10,11)",$this->uri->segment($segment_controllers),$this->get('user_id'));
        //     else
        //         $data->Items = $this->items_models->GetItemReservationsData("i.items_isActive = 1 AND i.items_isPublish = 1 AND mi.menuItem_id IN (9,10,11)",$this->uri->segment($segment_controllers));
        // }

        $this->response($data, 200);
    }
    public function index_post()
    {
    }
}