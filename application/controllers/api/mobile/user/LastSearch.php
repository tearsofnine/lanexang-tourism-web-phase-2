<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
date_default_timezone_set('Asia/Bangkok');
class LastSearch extends REST_Controller
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->library('form_validation');

        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("CountriesModels","countries_models", true);
        $this->load->model("UserModel","user_model", true);
        $this->load->model("SearchModels","search_models", true);
        $this->load->model("ProgramTourModels","program_tour_models", true);

        $this->db->db_debug = false;
        
    }

    public function index_get()
    {

    }
    
    public function IndexLastSearch_post()
    {
        $input = $this->input->post();
        $data = new stdClass();
        
        $this->form_validation->set_rules('last_id', 'last_id', 'trim|required');
        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {
            if (!empty($input["latitude"]) && !empty($input["longitude"]) ) 
            {
                $latitude = $input["latitude"];
                $longitude = $input["longitude"];
            }
            else
            {
                $latitude = "";
                $longitude = "";
                
            }
            $data->Items = $this->items_models->GetItemData("i.items_isActive = 1 AND i.items_isPublish = 1 AND mi.menuItem_id IN (2,3,4,5,6) AND i.items_id IN (".$input["last_id"].")",$latitude,$longitude);
            $res = $data;
            $this->response($res, REST_Controller::HTTP_OK);
        }
    }
    public function index_post()
    {
        $input = $this->input->post();
        $data = new stdClass();

        $this->form_validation->set_rules('category_id', 'category_id', 'trim|required');
        $this->form_validation->set_rules('last_id', 'last_id', 'trim|required');
        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {
            if (!empty($input["latitude"]) && !empty($input["longitude"]) ) 
            {
                $latitude = $input["latitude"];
                $longitude = $input["longitude"];
            }
            else
            {
                $latitude = "";
                $longitude = "";
            }

            switch (intval($input["category_id"])) {
                case 1:
                    $data->ProgramTour = $this->program_tour_models->GetProgramTourData("pt.programTour_isActive = 1 AND pt.programTour_isPublish = 1 AND pt.PublishStatus_publishStatus_id = 1 AND pt.programTour_isSaveDraft = 0 AND pt.programTour_id IN (".$input["last_id"].") AND (NOW() BETWEEN pt.programTour_time_period_start AND pt.programTour_time_period_end OR (pt.programTour_time_period_start = '1970-01-01' AND pt.programTour_time_period_end = '1970-01-01' ))");
                break;
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    $data->Items = $this->items_models->GetItemData("i.items_isActive = 1 AND i.items_isPublish = 1 AND mi.menuItem_id = " .$input["category_id"]." AND i.items_id IN (".$input["last_id"].")",$latitude,$longitude);
                break;
                case 9:
                    $data->Items = $this->items_models->GetItemReservationsData("i.items_isActive = 1 AND i.items_isPublish = 1 AND mi.menuItem_id = 9 AND i.items_id IN (".$input["last_id"].")","PackageTours");
                break;
                case 10:
                    $data->Items = $this->items_models->GetItemReservationsData("i.items_isActive = 1 AND i.items_isPublish = 1 AND mi.menuItem_id = 10 AND i.items_id IN (".$input["last_id"].")","EventTicket");
                break;
                case 11:
                    $data->Items = $this->items_models->GetItemReservationsData("i.items_isActive = 1 AND i.items_isPublish = 1 AND mi.menuItem_id = 11 AND i.items_id IN (".$input["last_id"].")","CarRent");
                break;
           }

           $res = $data;
        }
        $this->response($res, REST_Controller::HTTP_OK);
    }
    
}