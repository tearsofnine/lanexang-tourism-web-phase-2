<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
date_default_timezone_set('Asia/Bangkok');
class User extends REST_Controller
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->helper('file');
        $this->load->library('form_validation');

        $this->load->model("ReviewModels","review_models", true);
        $this->load->model("FollowersModels","followers_models", true);
        $this->load->model("UserModel","user_model", true);

        $this->db->db_debug = false;
        
    }
    
    public function index_get()
    {
        $segment = 5;
        if($this->uri->segment($segment) && $this->get('follow') == "")
        {
            $where = "UserRole_userRole_id = 5 AND (user_id = '".$this->uri->segment($segment)."' OR user_identification = '" .$this->uri->segment($segment)."' OR user_token = '".$this->uri->segment($segment)."' )";
            $query =  $this->user_model->GetUserData($where);
        }
        else if($this->uri->segment($segment) && $this->get('follow') && $this->get('follow') != "" && $this->get('follow') != $this->uri->segment($segment))
        {
            $where = "UserRole_userRole_id = 5 AND user_id = ".$this->get('follow');
            $query =  $this->user_model->GetUserData($where);
            for($i = 0;$i < count($query);$i++ )
            {
                $follow_state = $this->followers_models->GetFollowersData("User_user_id = ".$this->uri->segment($segment)." AND followers_user_id = ".$this->get('follow'));
                $query[$i]->follow_state = (count($follow_state) > 0?true:false);
            }
        }
        else
        {
            $query =[];
        }

        if(count($query) > 0)
        {
            $this->response($query, REST_Controller::HTTP_OK);                
        }
        else
        {
            $this->response(array('state'=>false), REST_Controller::HTTP_OK);
        }                 
    }
  
    public function index_post()
    {
        $input = $this->input->post();
        $this->form_validation->set_rules('user_identification', 'Identification', 'trim|required');
        $this->form_validation->set_rules('user_token', 'Token', 'trim|required');
        $this->form_validation->set_rules('user_email', 'Email', 'trim|required');
        $this->form_validation->set_rules('user_firstName', 'FirstName', 'trim|required');
        $this->form_validation->set_rules('user_lastName', 'LastName', 'trim|required');
        $this->form_validation->set_rules('user_age', 'age', 'trim|required');
        $this->form_validation->set_rules('user_gender', 'Gender', 'trim|required');
        $this->form_validation->set_rules('user_loginWith', 'LoginWith', 'trim|required');
        $this->form_validation->set_rules('Country_country_id', 'Country', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {
            $input["UserRole_userRole_id"] = 5;
            $input["user_isActive"] = 1;
            $input["user_createdDTTM"] = date("Y-m-d H:i:s");

            $query = $this->user_model->GetUserData(array('user_identification'=>$input["user_identification"]));

            if(count($query) == 0)
            {
                $res = $this->user_model->AddUserData($input);
            }
            else
            {
                $res = array(
                    'msg' => "Unable to Register user, identification already exists",
                    'insert_id' => -1,
                    'state' => false
                );
            }
        }
        $this->response($res, REST_Controller::HTTP_OK);
    }
   
    public function updateUserData_post()
    {
        $input = $this->input->post();
       
       
        $this->form_validation->set_rules('user_id', 'UserID', 'trim|required');
        $this->form_validation->set_rules('user_email', 'Email', 'trim|required');
        $this->form_validation->set_rules('user_firstName', 'FirstName', 'trim|required');
        $this->form_validation->set_rules('user_lastName', 'LastName', 'trim|required');
        $this->form_validation->set_rules('user_age', 'age', 'trim|required');
        $this->form_validation->set_rules('user_gender', 'Gender', 'trim|required');
        $this->form_validation->set_rules('Country_country_id', 'Country', 'trim|required');
        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {
            if(intval($input["profile_pic_state"]) == 1)
            {
                $image_name = md5(uniqid(rand(), true));// image name generating with random number with 32 characters
                $filename = $image_name.'.'. 'jpg';
                $originalImage = $filename;
                $data_img = $input["user_profile_pic_url"];
                $data_img = base64_decode($data_img);
                file_put_contents("./assets/img/uploadfile/".$filename, $data_img);

                $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
                $domainName = $_SERVER['HTTP_HOST'] . '/';

                $user_data = array(
                    "user_email" =>$input["user_email"],
                    "user_firstName" =>$input["user_firstName"],
                    "user_lastName" =>$input["user_lastName"],
                    "user_age" =>$input["user_age"],
                    "user_gender" =>$input["user_gender"],
                    "user_profile_pic_url" =>$protocol . $domainName."dasta_thailand/assets/img/uploadfile/".$filename,
                    "Country_country_id" =>$input["Country_country_id"],
                    "user_updatedDTTM" =>date("Y-m-d H:i:s"),
                );
                $res = $this->user_model->UpdateUserData($user_data,"user_id = ".$input["user_id"]);
            }
            else
            {
                $user_data = array(
                    "user_email" =>$input["user_email"],
                    "user_firstName" =>$input["user_firstName"],
                    "user_lastName" =>$input["user_lastName"],
                    "user_age" =>$input["user_age"],
                    "user_gender" =>$input["user_gender"],
                    "Country_country_id" =>$input["Country_country_id"],
                    "user_updatedDTTM" =>date("Y-m-d H:i:s"),
                );
                $res = $this->user_model->UpdateUserData($user_data,"user_id = ".$input["user_id"]);
            }
           
        }

       
        $this->response($res, REST_Controller::HTTP_OK);
    }
    public function updateUserLocation_post()
    {
        $input = $this->input->post();
        $this->form_validation->set_rules('user_id', 'UserID', 'trim|required');
        $this->form_validation->set_rules('user_latitude', 'Latitude', 'trim|required');
        $this->form_validation->set_rules('user_longitude', 'Longitude', 'trim|required');
        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {
            $input["user_updatedDTTM"] = date("Y-m-d H:i:s");
            $res = $this->user_model->UpdateUserData($input,"user_id = ".$input["user_id"]);
        }

        $this->response($res, REST_Controller::HTTP_OK);
    }
    public function update_post()
    {
        $input = $this->input->post();
        $this->form_validation->set_rules('user_identification', 'Identification', 'trim|required');
        $this->form_validation->set_rules('user_firstName', 'FirstName', 'trim|required');
        $this->form_validation->set_rules('user_lastName', 'LastName', 'trim|required');
        $this->form_validation->set_rules('user_age', 'age', 'trim|required');
        $this->form_validation->set_rules('user_gender', 'Gender', 'trim|required');
        $this->form_validation->set_rules('Country_country_id', 'Country', 'trim|required');
        $this->form_validation->set_rules('user_id', 'UserID', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {
            $input["user_updatedDTTM"] = date("Y-m-d H:i:s");
            $res = $this->user_model->UpdateUserData($input,"user_id = ".$input["user_id"]);
        }

        $this->response($res, REST_Controller::HTTP_OK);
    }
    public function index_delete()
    {     
    }
    
}