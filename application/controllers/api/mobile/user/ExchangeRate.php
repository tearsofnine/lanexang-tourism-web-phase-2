<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
date_default_timezone_set('Asia/Bangkok');
class ExchangeRate extends REST_Controller
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->helper('file');
        $this->load->library('form_validation');
        $this->db->db_debug = false;
       
    }
    public function index_get()
    {
        // Get cURL resource
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'https://api.exchangeratesapi.io/latest?base=THB',
            //CURLOPT_USERAGENT => 'Codular Sample cURL Request'
        ));
        // Send the request & save response to $resp
        $resp = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);

        $ExchangeRate_ALL =json_decode($resp);

        $data = new stdClass();
        $data_main = new stdClass();

        $data->THB = $ExchangeRate_ALL->rates->THB;
        $data->USD = $ExchangeRate_ALL->rates->USD;
        $data->CNY = $ExchangeRate_ALL->rates->CNY;
        $data->DATE = $ExchangeRate_ALL->date;
        
        $data_main->ExchangeRate = $data;
        $this->response($data_main, REST_Controller::HTTP_OK);
    }
    public function index_post()
    {
    }
}