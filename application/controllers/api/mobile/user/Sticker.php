<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
date_default_timezone_set('Asia/Bangkok');
class Sticker extends REST_Controller
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->helper('file');
        $this->load->library('form_validation');

        $this->load->model("StickerModels","sticker_models", true);
        $this->load->model("UserModel","user_model", true);

        $this->db->db_debug = false;
        
    }
    public function index_get()
    {
        $segment_stickerType_id = 5;
        $data = new stdClass();
        if($this->uri->segment($segment_stickerType_id))
        {
            $data->PathsSticker =  $this->sticker_models->GetPathsStickerByStickerType("sk.sticker_isActive = 1 AND sk.sticker_isPublish = 1 AND sk.StickerType_stickerType_id = ".$this->uri->segment($segment_stickerType_id));
        }
        else
        {
            $data->PathsSticker = [];
        }
        
        $this->response($data, 200);
    }
    public function getStickerType_get()
    {
        $data = new stdClass();
        $data->StickerType = $this->sticker_models->getNameCategory();
        $this->response($data, 200);

    }
    public function index_post()
    {

    }
}
?>