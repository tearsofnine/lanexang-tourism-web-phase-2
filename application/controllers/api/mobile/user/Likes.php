<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
date_default_timezone_set('Asia/Bangkok');
class Likes extends REST_Controller
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->model("LikesModels","likes_models", true);
        $this->db->db_debug = false;
    }
    public function index_get()
    {
        $segment = 5;
        $data = new stdClass();
        if($this->uri->segment($segment))
        {
            
            $query = $this->likes_models->GetLikesData("User_user_id = ".$this->uri->segment($segment));
        }
        else
        {
            $query =[];
        }
        $data->Likes = $query;
        $this->response($data, 200);
    }
    public function index_post()
    {
        $input = $this->input->post();
        $this->form_validation->set_rules('User_user_id', 'UserID', 'trim|required');
        $this->form_validation->set_rules('ProgramTour_programTour_id', 'Likes ProgramTourId', 'trim|required');
        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {
            $where = array(
                'User_user_id'=> $input["User_user_id"],
                'ProgramTour_programTour_id'=>$input["ProgramTour_programTour_id"]
            );
            $query = $this->likes_models->GetLikesData($where);

            if(count($query) == 0)
            {
                $res = $this->likes_models->AddLikesData($input);
            }
            else
            {
                $res = $this->likes_models->DeleteLikesData($input);
            }
        }
        $this->response($res, REST_Controller::HTTP_OK);
    }
}
?>