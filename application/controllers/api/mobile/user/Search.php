<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
date_default_timezone_set('Asia/Bangkok');
class Search extends REST_Controller
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->library('form_validation');

        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("CountriesModels","countries_models", true);
        $this->load->model("UserModel","user_model", true);
        $this->load->model("SearchModels","search_models", true);

        $this->db->db_debug = false;
        
    }

    public function index_get()
    {

    }

    public function SearchItemByMenuItemId_get()
    {
        if (($this->get('latitude') && $this->get('latitude') != "") && $this->get('longitude') && $this->get('longitude') != "" ) 
        {
            $latitude = $this->get('latitude');
            $longitude = $this->get('longitude');
        }
        else
        {
            $latitude = "";
            $longitude = "";
        }

        $segment_menuItem_id  = 6;
        $data = new stdClass();
        if($this->uri->segment($segment_menuItem_id ))
        {
            if($this->get('user_id') && $this->get('user_id') != "")
                $data->Items = $this->search_models->SearchItemByMenuIdAPI($this->uri->segment($segment_menuItem_id),$latitude,$longitude,$this->get('user_id'));
            else
                $data->Items = $this->search_models->SearchItemByMenuIdAPI($this->uri->segment($segment_menuItem_id),$latitude,$longitude);
        }
        else
        {
            $data->Items = array();
        }
        $this->response($data, 200);
    }

    public function SearchItemByProvince_get()
    {
        $segment_province_id = 6;
        $data = new stdClass();

        if (($this->get('latitude') && $this->get('latitude') != "") && $this->get('longitude') && $this->get('longitude') != "" ) 
        {
            $latitude = $this->get('latitude');
            $longitude = $this->get('longitude');
        }
        else
        {
            $latitude = "";
            $longitude = "";
        }

        if($this->uri->segment($segment_province_id))
        {
            if($this->get('user_id') && $this->get('user_id') != "")
            {
                if($this->input->get("category_id") && $this->input->get("category_id") != "")
                {
                    $data->Items = $this->search_models->SearchItemByProvinceAPI($this->uri->segment($segment_province_id),$latitude,$longitude,$this->get('user_id'),$this->get('category_id'));
                }
                else
                    $data->Items = $this->search_models->SearchItemByProvinceAPI($this->uri->segment($segment_province_id),$latitude,$longitude,$this->get('user_id'));
            }
            else
            {
                if($this->input->get("category_id") && $this->input->get("category_id") != "")
                {
                    $data->Items = $this->search_models->SearchItemByProvinceAPI($this->uri->segment($segment_province_id),$latitude,$longitude,-1,$this->get('category_id'));
                }
                else
                    $data->Items = $this->search_models->SearchItemByProvinceAPI($this->uri->segment($segment_province_id),$latitude,$longitude);
            }
               
        }
        else
        {
            $data->Items = array();
        }
        $this->response($data, 200);
    }

    public function DeliciousGuarantee_get()
    {
        $data = new stdClass();
        $retrun_data = $this->items_models->getNameDeliciousGuarantee();

        $data->Facilities = $retrun_data->result();

        $this->response($data, 200);
        
    }

    public function Facilities_get()
    {
        $data = new stdClass();
        $retrun_data = $this->items_models->getNameFacilities();

        $data->Facilities = $retrun_data->result();

        $this->response($data, 200);
    }

    public function SubCategory_get()
    {
        $segment_category_id = 6;
        $data = new stdClass();
        if($this->uri->segment($segment_category_id))
        {
            if(intval($this->uri->segment($segment_category_id)) != 11)
            {
                $retrun_data = $this->items_models->getNameCategorySubCategory($this->uri->segment($segment_category_id));
                $data->SubCategory = $retrun_data->result();
            }
           else
           {
                $TypesofCars_typesofCars_id = $this->items_models->GetItemSubData("TypesofCars","");
                $getCategory = $this->items_models->getCategory("11");
                $NameCategory = $getCategory->result();
                foreach ($TypesofCars_typesofCars_id as $item)
                {
                    //echo "<option value=\"".$item->typesofCars_id."\">".$item->typesofCars_Thai."</option>";
                    $data->SubCategory [] =array(
                        "category_id"=> $NameCategory[0]->category_id,
                        "category_thai"=> $NameCategory[0]->category_thai,
                        "category_english"=> $NameCategory[0]->category_english,
                        "category_chinese"=> $NameCategory[0]->category_chinese,
                        "category_laos"=> $NameCategory[0]->category_laos,
                        "MenuItem_menuItem_id"=> $NameCategory[0]->MenuItem_menuItem_id,
                        "subcategory_id"=> $item->typesofCars_id,
                        "subcategory_thai"=> $item->typesofCars_Thai,
                        "subcategory_english"=> $item->typesofCars_English,
                        "subcategory_chinese"=> $item->typesofCars_Chinese,
                        "subcategory_laos"=> $item->typesofCars_Laos,
                        "Category_category_id"=>  $NameCategory[0]->category_id,
                    );
                }
           }
        }
        else
        {
            $data->SubCategory = array();
        }
        $this->response($data, 200);
    }

    public function ProvinceGroup_get()
    {
        $input = $this->input->post();
        $data = new stdClass();
        $data->ProvinceGroup = $this->search_models->ProvinceGroupAPI();
        $this->response($data, 200);
    }

    public function index_post()
    {
    }
    
    public function SearchItem_post()
    {
        $input = $this->input->post();
        $data = new stdClass();

        if (!empty($input["latitude"]) && !empty($input["longitude"]) ) 
        {
            $latitude = $input["latitude"];
            $longitude = $input["longitude"];
        }
        else
        {
            $latitude = "";
            $longitude = "";
        }

        $this->form_validation->set_rules('str_search', 'str_search', 'trim|required');
        if ($this->form_validation->run() == FALSE) 
		{
			// $data->Items = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
			$data->Items = array();
        }
        else
        {
            if( $this->input->post("user_id") && $this->input->post("user_id") != "")
            {
                if($this->input->post("category_id") && $this->input->post("category_id") != "")
                    $data->Items = $this->search_models->SearchItemAPI($input["str_search"],$latitude,$longitude,$input["user_id"],$input["category_id"]);
                else
                    $data->Items = $this->search_models->SearchItemAPI($input["str_search"],$latitude,$longitude,$input["user_id"]);
            }  
            else
            {
                if($this->input->post("category_id") && $this->input->post("category_id") != "")
                    $data->Items = $this->search_models->SearchItemAPI($input["str_search"],$latitude,$longitude,-1,$input["category_id"]);
                else
                    $data->Items = $this->search_models->SearchItemAPI($input["str_search"],$latitude,$longitude);
            }
                
        }
        $this->response($data, 200);
    }
    public function SearchItemNearMe_post()
    {
        $input = $this->input->post();
        $data = new stdClass();
        $this->form_validation->set_rules('category_id', 'category_id', 'trim|required');
        $this->form_validation->set_rules('latitude', 'latitude', 'trim|required');
        $this->form_validation->set_rules('longitude', 'longitude', 'trim|required');
        if ($this->form_validation->run() == FALSE) 
		{
			$data->Items = array();
        }
        else
        {
            if( $this->input->post("user_id") && $this->input->post("user_id") != "")
            {
                $Items = $this->search_models->SearchItemAPI("",$input["latitude"],$input["longitude"],$input["user_id"],$input["category_id"]);
            }
            else
            {
                $Items = $this->search_models->SearchItemAPI("",$input["latitude"],$input["longitude"],-1,$input["category_id"]);
            }

            foreach($Items as $item)
            {
                if(round($this->distance(floatval($input["latitude"]),floatval($input["longitude"]),floatval($item->items_latitude),floatval($item->items_longitude),"K")) <= 50)
                    $data->Items [] = $item;
            }

            if(count($data->Items) == 0)
                $data->Items = array();

        }
        $this->response($data, 200);
    }
    public function distance($lat1, $lon1, $lat2, $lon2, $unit) 
	{
		if (($lat1 == $lat2) && ($lon1 == $lon2)) {
			return 0;
		}
        else 
        {
			$theta = $lon1 - $lon2;
			$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
			$dist = acos($dist);
			$dist = rad2deg($dist);
			$miles = $dist * 60 * 1.1515;
			$unit = strtoupper($unit);

            if ($unit == "K") 
            {
				return ($miles * 1.609344);
            } 
            else if ($unit == "N") 
            {
				return ($miles * 0.8684);
            } 
            else 
            {
				return $miles;
			}
	  }
	}
}