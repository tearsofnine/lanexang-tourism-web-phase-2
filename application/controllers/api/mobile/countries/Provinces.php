<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
date_default_timezone_set('Asia/Bangkok');
class Provinces extends REST_Controller
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
    }
    
    public function index_get()
    {
        if($this->get('fields') && $this->get('fields')!="")
        {
            $this->db->select($this->get('fields'));
        }
        if($this->uri->segment(5)){
            $query =  $this->db->get_where("Provinces",array('provinces_id'=>$this->uri->segment(5)));
        }
        else
        {
            if($this->get('country_id') && $this->get('country_id')!="")
            {
                $query =  $this->db->get_where("Provinces",array('Country_country_id'=>$this->get('country_id')));
            }
            else
                $query =  $this->db->get('Provinces');
        }
        if($query && $query->num_rows() > 0)
        {
            foreach($query->result() as $key => $r)
            {
                $r->state = true;
                $data[] = $r;
            }
            $this->response($data);                
        }
        else
        {
            $this->response(array('state'=>false), 200);
        }
       
    }
  
    public function index_post()
    {
        
    }
  
    public function index_put()
    {
        
    }
  
    public function index_delete()
    {
        
    }
}