<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
date_default_timezone_set('Asia/Bangkok');
class Country extends REST_Controller
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
    }
    
    public function index_get()
    {
        $this->db->db_debug = false;
        if($this->get('fields') && $this->get('fields')!="")
        {
            $this->db->select($this->get('fields'));
        }

        if($this->uri->segment(5))
        {
            $query =  $this->db->get_where("Country",array('country_id'=>$this->uri->segment(5)));
        }else
        {
            $query =  $this->db->get('Country');
        }

        if($query && $query->num_rows() > 0)
        {
            foreach($query->result() as $key => $r)
            {
                $r->state = true;
                $country_data[] = $r;
            }
            $this->response($country_data);                
        }
        else
        {
            $this->response(array('state'=>false), 200);
        }                 
        // ดึงรายการทั้งหมด
    }
  
    public function index_post()
    {
        // สร้างรายการใหม่
    }
  
    public function index_put()
    {
        // แก้ไขรายการ
    }
  
    public function index_delete()
    {
        // ลบรายการ
    }
}