<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
date_default_timezone_set('Asia/Bangkok');
class BookMarks extends REST_Controller
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->model("BookMarksModels","bookmarks_models", true);
        $this->db->db_debug = false;
    }
    public function index_get()
    {
        $segment = 5;
        $data = new stdClass();
        if (($this->get('latitude') && $this->get('latitude') != "") && $this->get('longitude') && $this->get('longitude') != "" ) 
        {
            $latitude = $this->get('latitude');
            $longitude = $this->get('longitude');
        }
        else
        {
            $latitude = "";
            $longitude = "";
        }
        if($this->uri->segment($segment))
        {
            $query = $this->bookmarks_models->GetBookMarksData("User_user_id = ".$this->uri->segment($segment),$latitude,$longitude);
        }
        else
        {
            $query =[];
        }
        $data->BookMarks = $query;
        $this->response($data, 200);
    }
    public function index_post()
    {
        $input = $this->input->post();

        if (!empty($input["latitude"]) && !empty($input["longitude"]) ) 
            {
                $latitude = $input["latitude"];
                $longitude = $input["longitude"];
            }
            else
            {
                $latitude = "";
                $longitude = "";
            }


        $this->form_validation->set_rules('User_user_id', 'UserID', 'trim|required');
        $this->form_validation->set_rules('Items_items_id', 'BookMarks ItemId', 'trim|required');
        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {
            $where = array(
                'User_user_id'=> $input["User_user_id"],
                'Items_items_id'=>$input["Items_items_id"]
            );
            $query = $this->bookmarks_models->GetBookMarksData($where,$latitude,$longitude);

            if(count($query) == 0)
            {
                $res = $this->bookmarks_models->AddBookMarksData($input);
            }
            else
            {
                $res = $this->bookmarks_models->DeleteBookMarksData($input);
            }
        }
        $this->response($res, REST_Controller::HTTP_OK);
    }
}
?>