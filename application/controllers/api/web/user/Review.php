<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
date_default_timezone_set('Asia/Bangkok');
class Review extends REST_Controller
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->db->db_debug = false;
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("SearchModels","search_models", true);
        $this->load->model("ProgramTourModels","program_tour_models", true);
        $this->load->model("ReviewModels","review_models", true);
    }
    public function index_get()
    {
        $items_id = $this->get('items_id');
        $programTour_id = $this->get('programTour_id');
        $user_id = $this->get('user_id');

        $data = new stdClass();
        $dataJson ='{
            "AverageReview":0,
            "CountReview":0,
            "OneStar":0,
            "TwoStar":0,
            "ThreeStar":0,
            "FourStar":0,
            "FiveStar":0,
            "Reviews":[]
        }';
        $datas = json_decode($dataJson);
        if ($items_id !== NULL)
        {
            $ReviewRating = $this->review_models->GetReviewRatingData("review_isActive =1 AND Items_items_id = ".$items_id." AND ProgramTour_programTour_id IS NULL");
            $Reviews = $this->review_models->GetReviewData("rv.review_isActive =1 AND rv.Items_items_id = ".$items_id." AND rv.ProgramTour_programTour_id IS NULL");

            $datas->AverageReview = intval($ReviewRating[0]->AverageReview);
            $datas->CountReview = intval($ReviewRating[0]->CountReview);
            $datas->OneStar = intval($ReviewRating[0]->OneStar);
            $datas->TwoStar = intval($ReviewRating[0]->TwoStar);
            $datas->ThreeStar = intval($ReviewRating[0]->ThreeStar);
            $datas->FourStar = intval($ReviewRating[0]->FourStar);
            $datas->FiveStar = intval($ReviewRating[0]->FiveStar);

            $i =0;
            foreach($Reviews as $r) {

                $datas->Reviews[] = $r;
                $originalDateTime = $r->review_timestamp;
                $newDate = date("d M Y", strtotime($originalDateTime));
                $newTime = date("h:i A", strtotime($originalDateTime));
                $newDateTime = $newDate." at ".$newTime;

                $datas->Reviews[$i]->review_timestamp = $newDateTime;
                $i++;
            }
            $data->Review[] = $datas;
        }
        else if($programTour_id !== NULL)
        {
            $ReviewRating = $this->review_models->GetReviewRatingData("review_isActive =1 AND ProgramTour_programTour_id = ".$programTour_id);
            $Reviews = $this->review_models->GetReviewData("rv.review_isActive =1 AND rv.ProgramTour_programTour_id = ".$programTour_id);

            $datas->AverageReview = intval($ReviewRating[0]->AverageReview);
            $datas->CountReview = intval($ReviewRating[0]->CountReview);
            $datas->OneStar = intval($ReviewRating[0]->OneStar);
            $datas->TwoStar = intval($ReviewRating[0]->TwoStar);
            $datas->ThreeStar = intval($ReviewRating[0]->ThreeStar);
            $datas->FourStar = intval($ReviewRating[0]->FourStar);
            $datas->FiveStar = intval($ReviewRating[0]->FiveStar);

            $i =0;
            foreach($Reviews as $r) {

                $datas->Reviews[] = $r;
                $originalDateTime = $r->review_timestamp;
                $newDate = date("d M Y", strtotime($originalDateTime));
                $newTime = date("h:i A", strtotime($originalDateTime));
                $newDateTime = $newDate." at ".$newTime;
                $datas->Reviews[$i]->review_timestamp = $newDateTime;
                $i++;
            }
            $data->Review[] = $datas;
        }
        elseif($user_id !== NULL)
        {
            $Reviews = $this->review_models->GetReviewData("rv.review_isActive =1 AND rv.User_user_id = ".$user_id." AND rv.ProgramTour_programTour_id IS NULL");
            $ReviewRating = $this->review_models->GetReviewRatingData("review_isActive =1 AND User_user_id = ".$user_id." AND ProgramTour_programTour_id IS NULL");

            $datas->AverageReview = intval($ReviewRating[0]->AverageReview);
            $datas->CountReview = intval($ReviewRating[0]->CountReview);
            $datas->OneStar = intval($ReviewRating[0]->OneStar);
            $datas->TwoStar = intval($ReviewRating[0]->TwoStar);
            $datas->ThreeStar = intval($ReviewRating[0]->ThreeStar);
            $datas->FourStar = intval($ReviewRating[0]->FourStar);
            $datas->FiveStar = intval($ReviewRating[0]->FiveStar);

            $i =0;
            foreach($Reviews as $r) {

                $datas->Reviews[] = $r;
                $originalDateTime = $r->review_timestamp;
                $newDate = date("d M Y", strtotime($originalDateTime));
                $newTime = date("h:i A", strtotime($originalDateTime));
                $newDateTime = $newDate." at ".$newTime;

                $datas->Reviews[$i]->review_timestamp = $newDateTime;
                $i++;
            }
            $data->Review[] = $datas;
        }
        else
        {
            $data->Review = [];
        }
        $this->response($data, 200);
    }
    public function index_post()
    {
        $input = $this->input->post();
        //$this->form_validation->set_rules('review_text', 'review_text', 'trim|required');
        $this->form_validation->set_rules('review_rating', 'review_rating', 'trim|required');
        $this->form_validation->set_rules('User_user_id', 'User_user_id', 'trim|required');
     
        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {
            $input["review_timestamp"] =  date("Y-m-d H:i:s");
            $input["review_timestamp_edit"] =  date("Y-m-d H:i:s");
            $input["review_isActive"] =  1;

            if($this->input->post("ProgramTour_programTour_id") !== NULL && $this->input->post("Items_items_id") === NULL)
            {
                $res = $this->review_models->AddReviewData($input);
            }
            else if($this->input->post("ProgramTour_programTour_id") === NULL && $this->input->post("Items_items_id") !== NULL)
            {
                $chkReview = $this->review_models->GetReviewData("rv.review_isActive = 1 AND rv.User_user_id = ".$input["User_user_id"]." AND Items_items_id = ".$this->input->post("Items_items_id"));
                if(count($chkReview) == 0)
                    $res = $this->review_models->AddReviewData($input);
                else
                    $res = array(
                        'msg' => "You have already written a review.",
                        'state' => false,
						'state_login' => null
                    );
            }
            else if($this->input->post("ProgramTour_programTour_id") !== NULL && $this->input->post("Items_items_id") === NULL)
            {
                $res = $this->review_models->AddReviewData($input);
            }
           
        }
        $this->response($res, REST_Controller::HTTP_OK);
    } 
    public function update_post()
    {
        $input = $this->input->post();
        $this->form_validation->set_rules('review_rating', 'review_rating', 'trim|required');
        $this->form_validation->set_rules('User_user_id', 'User_user_id', 'trim|required');
        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {
            $data = array(
                "review_isEdit" => 1,
                "review_timestamp_edit" => date("Y-m-d H:i:s"),
                "review_rating" => $input["review_rating"],
                "review_text" => $input["review_text"],
            );
            $res = $this->review_models->UpdateReviewData($data,"review_id = ".$input["review_id"]." AND User_user_id = ".$input["User_user_id"] );
        }
        $this->response($res, REST_Controller::HTTP_OK);
    }
    public function delete_post()
    {
        $input = $this->input->post();
        $res = $this->review_models->UpdateReviewData(array("review_isActive"=>0),"review_id = ".$input["review_id"]." AND User_user_id = ".$input["User_user_id"] );
        $this->response($res, REST_Controller::HTTP_OK);
    }
}