<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
date_default_timezone_set('Asia/Bangkok');
class Product extends REST_Controller
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->db->db_debug = false;
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("ProductModels","product_models", true);
    }
    public function index_post()
    {
        $input = $this->input->post();
        $data = new stdClass();
        $data->Items = $this->product_models->GetProductDataSearch("i.MenuItem_menuItem_id = 4 and p.product_namesThai like '".$this->input->post("str_search")."'"." AND i.items_isActive = 1","","",$this->input->post("user_id"));
        $this->response($data, REST_Controller::HTTP_OK);
    }
    public function index_get()
    {
        $data = new stdClass();
        $data->Product = $this->product_models->GetProductData();
        $this->response($data, REST_Controller::HTTP_OK);
    }
}
