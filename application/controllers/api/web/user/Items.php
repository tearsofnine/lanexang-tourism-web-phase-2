<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
date_default_timezone_set('Asia/Bangkok');
class Items extends REST_Controller
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->db->db_debug = false;
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("SearchModels","search_models", true);
        $this->load->model("ProgramTourModels","program_tour_models", true);
        $this->load->model("BookMarksModels","bookmarks_models", true);
    }
   
    public function index_get()
    {
        $segment_category_id = 5;
        $data = new stdClass();
        
        if (($this->get('latitude') && $this->get('latitude') != "") && $this->get('longitude') && $this->get('longitude') != "" ) 
        {
            $latitude = $this->get('latitude');
            $longitude = $this->get('longitude');
        }
        else
        {
            $latitude = "";
            $longitude = "";
        }

        if($this->uri->segment($segment_category_id))
        {
            if($this->uri->segment($segment_category_id) > 1 || $this->uri->segment($segment_category_id) < 8 )
            {
                if($this->get('user_id') && $this->get('user_id') != "")
                    $data->Items = $this->items_models->GetItemData("i.items_isActive = 1 AND i.items_isPublish = 1 AND mi.menuItem_id = " .$this->uri->segment($segment_category_id)."",$latitude,$longitude,$this->get('user_id'));
                else
                    $data->Items = $this->items_models->GetItemData("i.items_isActive = 1 AND i.items_isPublish = 1 AND mi.menuItem_id = " .$this->uri->segment($segment_category_id)."",$latitude,$longitude);
            }
            else
            {
                $data->Items = null;
            }
                
        }
        else if($this->get('items_id') && $this->get('items_id') != "")
        {
            if($this->get('user_id') && $this->get('user_id') != "")
                $data->Items = $this->items_models->GetItemData("i.items_isActive = 1 AND i.items_isPublish = 1 AND i.items_id = ".$this->get('items_id'),$latitude,$longitude,$this->get('user_id'));
            else
                $data->Items = $this->items_models->GetItemData("i.items_isActive = 1 AND i.items_isPublish = 1 AND i.items_id = ".$this->get('items_id'),$latitude,$longitude);
        }
        else
        {
            if($this->get('user_id') && $this->get('user_id') != "")
                $data->Items = $this->items_models->GetItemData("i.items_isActive = 1 AND i.items_isPublish = 1 AND cat.category_id IN (2,3,4,5,6,7)",$latitude,$longitude,$this->get('user_id'));
            else
                $data->Items = $this->items_models->GetItemData("i.items_isActive = 1 AND i.items_isPublish = 1 AND cat.category_id IN (2,3,4,5,6,7)",$latitude,$longitude);
        }
        $this->response($data, 200);
    }
    public function tourist_program_talks_about_this_place_get()
    {
        $segment_items_id = 5;
        $data = new stdClass();
        if($this->get('items_id') && $this->get('items_id') != "")
        {
            if($this->get('user_id') && $this->get('user_id') != "")
                $data->ProgramTour = $this->items_models->getTouristProgram($this->get('items_id'),$this->get('user_id'));
            else
                $data->ProgramTour = $this->items_models->getTouristProgram($this->get('items_id'),-1);
        }
        else
        {
            $data->ProgramTour = null;
        }
        $this->response($data, 200);
    }
	public function namesubcategory_get()
	{
		$segment_sid = 6;
		$data = new stdClass();
		$data = $this->items_models->getNameCategorySubCategory($this->uri->segment($segment_sid));
		$this->response($data->result(), 200);
	}
}