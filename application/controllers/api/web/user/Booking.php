<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
date_default_timezone_set('Asia/Bangkok');
header("Access-Control-Allow-Origin: *");
//header("Access-Control-Allow-Methods: GET,HEAD,OPTIONS,POST,PUT");
//header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization");
class Booking extends REST_Controller
{
    public function __construct() 
    {
		
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->db->db_debug = false;
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("BookingModels","booking_models", true);
        $this->load->model("PaymentTransactionModels","payment_transaction_models", true);
        
    }
    
    public function index_get()
    {
        // $segment = 5;
        $data = new stdClass();
        // if($this->uri->segment($segment))
        // {
        //     $query = $this->payment_transaction_models->GetPaymentTransactionDataBooking("User_user_id = ".$this->uri->segment($segment));
        // }
        // else
        // {
        //     $query =[];
        // }
        // $data->Booking = $query;
        if($this->get('user_id') && $this->get('user_id') != "")
        {
            $query = $this->payment_transaction_models->GetPaymentTransactionDataBooking("User_user_id = ".$this->get('user_id')." ".($this->get('payment_transaction_id') != ""?" AND paymentTransaction_id = ".$this->get('payment_transaction_id'):""));
        }
        else
        {
            $query =[];
        }
        $data->Booking = $query;
        $this->response($data, 200);
    }
    
    public function index_post()
    {
        $input = $this->input->post();

        $this->form_validation->set_rules('User_user_id', 'User_user_id', 'trim|required');
        $this->form_validation->set_rules('Items_items_id', 'Items_items_id', 'trim|required');
        
        $this->form_validation->set_rules('paymentTransaction_firstName', 'paymentTransaction_firstName', 'trim|required');
        $this->form_validation->set_rules('paymentTransaction_lastName', 'paymentTransaction_lastName', 'trim|required');
        $this->form_validation->set_rules('paymentTransaction_phone', 'paymentTransaction_phone', 'trim|required');
        $this->form_validation->set_rules('paymentTransaction_email', 'paymentTransaction_email', 'trim|required');

        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {
            $res = $this->payment_transaction_models->TakePaymentTransactionData($input);
        }
        $this->response($res, REST_Controller::HTTP_OK);
    }
}