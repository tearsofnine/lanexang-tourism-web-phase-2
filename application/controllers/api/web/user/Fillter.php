<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
date_default_timezone_set('Asia/Bangkok');
class Fillter extends REST_Controller
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->library('form_validation');

        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("CountriesModels","countries_models", true);
        $this->load->model("UserModel","user_model", true);
        $this->load->model("SearchModels","search_models", true);
        $this->load->model("ProgramTourModels","program_tour_models", true);

        $this->db->db_debug = false;
        
    }
    public function index_get()
    {

    }
    public function number_of_seats_get()
    {
        $data = new stdClass();
        
        // $data->NumberOfSeats = array();
        // $retrun_data = $this->items_models->get_number_of_seats();
        // foreach ($retrun_data as $item)
        // {
        //     $data->NumberOfSeats[] = array(
        //         "items_carSeats" => $item->items_carSeats
        //     );
        // }

        $data->NumberOfSeats = $this->items_models->get_number_of_seats();
        $this->response($data, 200);
    }
    
    public function tour_package_duration_get()
    {
        $data = new stdClass();
        $data->NumDatesTrip [] = array(
            "textThai"=>"ทัวร์วันเดียว",
            "textnglish"=>"One Day Tour",
            "textLaos"=>"ການທ່ອງທ່ຽວມື້ ໜຶ່ງ",
            "textChinese"=>"一日遊",
            "textValue"=>"1",
        );
        $data->NumDatesTrip [] = array(
            "textThai"=>"ทัวร์สองวัน",
            "textnglish"=>"Two Day Tour",
            "textLaos"=>"ທ່ຽວສອງມື້",
            "textChinese"=>"兩日遊",
            "textValue"=>"2",
        ); 
        $data->NumDatesTrip [] = array(
            "textThai"=>"โปรแกรมทัวร์ระยะยาว",
            "textnglish"=>"Long-term tour program",
            "textLaos"=>"ໂຄງການທ່ອງທ່ຽວໄລຍະຍາວ",
            "textChinese"=>"長期旅行計劃",
            "textValue"=>"3",
        );
        $this->response($data, 200);
    }
    public function price_range_get()
    {
        $segment_category_id = 6;
        $data = new stdClass();
        if($this->uri->segment($segment_category_id))
        {
            switch (intval($this->uri->segment($segment_category_id)))
            {
                case 5:
                    $data->PriceRange = $this->items_models->getRoomPriceRange();
                break;
                case 9:
                    $data->PriceRange = $this->items_models->getPackageToursPriceRange();
                break;
                case 10:
                    $data->PriceRange = $this->items_models->getEventTicketPriceRange();
                break;
                    case 11:$data->PriceRange = $this->items_models->getCarPriceRange();
                break;
                default :$data->PriceRange = [];break;
            }
            
        }
        else
            $data->PriceRange = [];

        $this->response($data, 200);
    }
    public function index_post()
    {
        $input = $this->input->post();
        $data = new stdClass();
        $this->form_validation->set_rules('category_id', 'category_id', 'trim|required');
        if ($this->form_validation->run() == FALSE) 
		{
			$data->Items = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
			//$data->Items = array();
        }
        else
        {
            if (!empty($input["user_id"])) 
            {
                $user_id = $input["user_id"];
            }
            else
            {
                $user_id =-1;
            }
            if (!empty($input["latitude"]) && !empty($input["longitude"]) ) 
            {
                $latitude = $input["latitude"];
                $longitude = $input["longitude"];
            }
            else
            {
                $latitude = "";
                $longitude = "";
            }

            if (!empty($input["price_range"])) 
            {
                $price_range = $input["price_range"];
            }
            else
            {
                $price_range ="";
            }
            if (!empty($input["subcategory_id"])) 
            {
                $subcategory_id = $input["subcategory_id"];
            }
            else
            {
                $subcategory_id ="";
            }
            if (!empty($input["str_search"])) 
            {
                $str_search = $input["str_search"];
            }
            else
            {
                $str_search ="";
            }

            switch (intval($input["category_id"])) {
                case 5:
                        $data->Items = $this->items_models->getFillterForHotel($subcategory_id,$price_range,$input["facilities_id"],$input["accommodation_level"],$str_search,$latitude,$longitude,$user_id);
                    break;
                case 9:
                        $data->Items = $this->items_models->getFillterForPackageTours($subcategory_id,$price_range,$input["travel_period"],$input["tour_package_duration"],$str_search,$user_id);
                    break;
                case 10:
                    $data->Items = $this->items_models->getFillterForEventTicket($subcategory_id,$price_range,$str_search,$user_id);
                    break;
                case 11:
                    $data->Items = $this->items_models->getFillterForCarRent($subcategory_id,$price_range,$input["number_of_seats"],$str_search,$user_id);
                    break;
                
                default:
                    $data->Items = array();
                    break;
            }
        }
        $this->response($data, 200);
    }
}