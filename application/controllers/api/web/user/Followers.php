<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
date_default_timezone_set('Asia/Bangkok');
class Followers extends REST_Controller
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->model("FollowersModels","followers_models", true);
        $this->db->db_debug = false;
    }
    public function index_get()
    {
        $segment = 5;
        if($this->uri->segment($segment))
        {
            if($this->get('type') && $this->get('type') == "following")
            {
                $data =  $this->followers_models->Getfollowing($this->uri->segment($segment));
                $query = $data->result();
                
            }
            else if($this->get('type') && $this->get('type') == "follower")
            {
                $data =  $this->followers_models->Getfollowers($this->uri->segment($segment));
                $query = $data->result();
            }
            else
                $query =[];
        }
        else
        {
            if(ENVIRONMENT == 'development')
                $query =  $this->db->get('Followers');
            else if(ENVIRONMENT == 'production')
                $query =[];
           
        }
        for($i = 0;$i < count($query);$i++ )
        {
            $follow_state = $this->followers_models->GetFollowersData("User_user_id = ".$this->uri->segment($segment)." AND followers_user_id = ".$query[$i]->user_id);
            $query[$i]->follow_state = (count($follow_state) > 0?true:false);
        }
        $this->response($query, 200);
    }
    public function index_post()
    {
        $input = $this->input->post();
        $this->form_validation->set_rules('User_user_id', 'UserID', 'trim|required');
        $this->form_validation->set_rules('followers_user_id', 'Followers UserID', 'trim|required');
        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {
            $where = array(
                'User_user_id'=> $input["User_user_id"],
                'followers_user_id'=>$input["followers_user_id"]
            );
            $query = $this->followers_models->GetFollowersData($where);

            if(count($query) == 0)
            {
                $res = $this->followers_models->AddFollowersData($input);
            }
            else
            {
                $res = $this->followers_models->DeleteFollowersData($input);
            }
        }
        $this->response($res, REST_Controller::HTTP_OK);
    }
    
}