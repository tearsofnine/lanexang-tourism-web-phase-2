<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
date_default_timezone_set('Asia/Bangkok');
class User extends REST_Controller
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->helper('file');
        $this->load->library('form_validation');

        $this->load->model("ReviewModels","review_models", true);
        $this->load->model("FollowersModels","followers_models", true);
        $this->load->model("UserModel","user_model", true);

        $this->db->db_debug = false;
        
    }
    
    public function index_get()
    {
        $segment = 5;
        if($this->uri->segment($segment) && $this->get('follow') == "" && $this->get('forchat') == "")
        {
            $where = "UserRole_userRole_id = 5 AND (user_id = '".$this->uri->segment($segment)."' OR user_identification = '" .$this->uri->segment($segment)."' OR user_token = '".$this->uri->segment($segment)."' )";
            $query =  $this->user_model->GetUserData($where);
        }
        else if($this->uri->segment($segment) && $this->get('follow') != "" && $this->get('follow') != $this->uri->segment($segment) && $this->get('forchat') == "")
        {
            $where = "UserRole_userRole_id = 5 AND user_id = ".$this->get('follow');
            $query =  $this->user_model->GetUserData($where);
            for($i = 0;$i < count($query);$i++ )
            {
                $follow_state = $this->followers_models->GetFollowersData("User_user_id = ".$this->uri->segment($segment)." AND followers_user_id = ".$this->get('follow'));
                $query[$i]->follow_state = (count($follow_state) > 0?true:false);
            }
        }
        elseif($this->uri->segment($segment) && $this->get('forchat') == "1")
        {
            //$query =[];
            $where = "user_id = ".$this->uri->segment($segment);
            $query =  $this->user_model->GetUserData($where);
			$query[0]->Business = $this->user_model->GetUserJoinBusinessDataForChat("u.user_id = ".$this->uri->segment($segment));
        }
		else
        {
            $query =[];
            
        }

        if(count($query) > 0)
        {
            $this->response($query, REST_Controller::HTTP_OK);                
        }
        else
        {
            $this->response([array('state'=>0)], REST_Controller::HTTP_OK);
        }                 
    }
  
    public function index_post()
    {
        $input = $this->input->post();
        $this->form_validation->set_rules('user_identification', 'Identification', 'trim|required');
        $this->form_validation->set_rules('user_token', 'Token', 'trim|required');
        $this->form_validation->set_rules('user_email', 'Email', 'trim|required');
        $this->form_validation->set_rules('user_firstName', 'FirstName', 'trim|required');
        $this->form_validation->set_rules('user_lastName', 'LastName', 'trim|required');
        $this->form_validation->set_rules('user_age', 'age', 'trim|required');
        $this->form_validation->set_rules('user_gender', 'Gender', 'trim|required');
        $this->form_validation->set_rules('user_loginWith', 'LoginWith', 'trim|required');
        $this->form_validation->set_rules('Country_country_id', 'Country', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {
            $input["UserRole_userRole_id"] = 5;
            $input["user_isActive"] = 1;
            $input["user_createdDTTM"] = date("Y-m-d H:i:s");

            $query = $this->user_model->GetUserData(array('user_identification'=>$input["user_identification"]));

            if(count($query) == 0)
            {
                $res = $this->user_model->AddUserData($input);
            }
            else
            {
                $res = array(
                    'msg' => "Unable to Register user, identification already exists",
                    'insert_id' => -1,
                    'state' => false
                );
            }
        }
        $this->response($res, REST_Controller::HTTP_OK);
    }
	
	public function oldpass_check()
    {
		$oldpass = $this->input->post('oldpass');
		$user_id = $this->input->post('user_id');
		
        $this->load->library('form_validation');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get('User');
        $row = $query->row();

        $password = openssl_encrypt($oldpass, "AES-128-ECB", SECRETKEY);
        if ($row->user_password != $password) {
            $this->form_validation->set_message('oldpass_check', 'The %s ให้ถูกต้อง|');
            return FALSE;
        } else {
            return TRUE;
        }
    }
    public function updateUserPassword_post()
    {
        $input = $this->input->post();
		$user_id = $input["user_id"];
        $this->form_validation->set_rules('oldpass', 'required|oldpass|oldpass|กรุณากรอก รหัสผ่านเก่า', 'trim|required|callback_oldpass_check');
        $this->form_validation->set_rules('newpass', 'required|newpass|newpass|กรุณากรอก รหัสใหม่', 'trim|required|matches[conpass]');
        $this->form_validation->set_rules('conpass', 'required|conpass|conpass|กรุณากรอก รหัสใหม่อีกครั้ง', 'trim|required');
        $this->form_validation->set_rules('user_id', 'UserID', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $res = array('state' => false, 'error' => 'validation', 'msg' => validation_errors());
        } else {
            $password = openssl_encrypt($this->input->post("newpass"), "AES-128-ECB", SECRETKEY);
            $user_data = array(
                "user_password" => $password,
                "user_updatedDTTM" => date("Y-m-d H:i:s"),
            );
            $res = $this->user_model->UpdateUserData($user_data, "user_id = " .  $input["user_id"]);
            $res = array('state' => true, 'error' => '', 'msg' => 'เพิ่มสำเร็จ');
        }

        return $this->response($res, REST_Controller::HTTP_OK);
    }
    public function updateUserData_post()
    {
        $input = $this->input->post();
       
       
        $this->form_validation->set_rules('user_id', 'UserID', 'trim|required');
        $this->form_validation->set_rules('user_email', 'Email', 'trim|required');
        $this->form_validation->set_rules('user_firstName', 'FirstName', 'trim|required');
        $this->form_validation->set_rules('user_lastName', 'LastName', 'trim|required');
        $this->form_validation->set_rules('user_age', 'age', 'trim|required');
        $this->form_validation->set_rules('user_gender', 'Gender', 'trim|required');
        $this->form_validation->set_rules('Country_country_id', 'Country', 'trim|required');
        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {
            if(intval($input["profile_pic_state"]) == 1)
            {
                $image_name = md5(uniqid(rand(), true));// image name generating with random number with 32 characters
                $filename = $image_name.'.'. 'jpg';
                $originalImage = $filename;
                $data_img = $input["user_profile_pic_url"];
                $data_img = base64_decode($data_img);
                file_put_contents("./assets/img/uploadfile/".$filename, $data_img);

                $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
                $domainName = $_SERVER['HTTP_HOST'] . '/';

                $user_data = array(
                    "user_email" =>$input["user_email"],
                    "user_firstName" =>$input["user_firstName"],
                    "user_lastName" =>$input["user_lastName"],
                    "user_age" =>$input["user_age"],
                    "user_gender" =>$input["user_gender"],
                    "user_profile_pic_url" =>$protocol . $domainName."dasta_thailand/assets/img/uploadfile/".$filename,
                    "Country_country_id" =>$input["Country_country_id"],
                    "user_updatedDTTM" =>date("Y-m-d H:i:s"),
                );
                $res = $this->user_model->UpdateUserData($user_data,"user_id = ".$input["user_id"]);
            }
            else
            {
                $user_data = array(
                    "user_email" =>$input["user_email"],
                    "user_firstName" =>$input["user_firstName"],
                    "user_lastName" =>$input["user_lastName"],
                    "user_age" =>$input["user_age"],
                    "user_gender" =>$input["user_gender"],
                    "Country_country_id" =>$input["Country_country_id"],
                    "user_updatedDTTM" =>date("Y-m-d H:i:s"),
                );
                $res = $this->user_model->UpdateUserData($user_data,"user_id = ".$input["user_id"]);
            }
           
        }

       
        $this->response($res, REST_Controller::HTTP_OK);
    }
    public function updateUserLocation_post()
    {
        $input = $this->input->post();
        $this->form_validation->set_rules('user_id', 'UserID', 'trim|required');
        $this->form_validation->set_rules('user_latitude', 'Latitude', 'trim|required');
        $this->form_validation->set_rules('user_longitude', 'Longitude', 'trim|required');
        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {
            $input["user_updatedDTTM"] = date("Y-m-d H:i:s");
            $res = $this->user_model->UpdateUserData($input,"user_id = ".$input["user_id"]);
        }

        $this->response($res, REST_Controller::HTTP_OK);
    }
    public function update_post()
    {
        $input = $this->input->post();
        $this->form_validation->set_rules('user_identification', 'Identification', 'trim|required');
        $this->form_validation->set_rules('user_firstName', 'FirstName', 'trim|required');
        $this->form_validation->set_rules('user_lastName', 'LastName', 'trim|required');
        $this->form_validation->set_rules('user_age', 'age', 'trim|required');
        $this->form_validation->set_rules('user_gender', 'Gender', 'trim|required');
        $this->form_validation->set_rules('Country_country_id', 'Country', 'trim|required');
        $this->form_validation->set_rules('user_id', 'UserID', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {
            $input["user_updatedDTTM"] = date("Y-m-d H:i:s");
            $res = $this->user_model->UpdateUserData($input,"user_id = ".$input["user_id"]);
        }

        $this->response($res, REST_Controller::HTTP_OK);
    }
    public function index_delete()
    {     
    }
	public function takeloginweb_post()
	{
		$input = $this->input->post();
		$this->form_validation->set_rules('email', 'input_email', 'trim|required');
        $this->form_validation->set_rules('password', 'input_password', 'trim|required');
		if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
		else
		{
			$res = $this->user_model->login_web($input["email"],$input["password"]);
		}
		$this->response($res, REST_Controller::HTTP_OK);
	}
	
	public function takeregisterweb_post()
	{
		$input = $this->input->post();
        //$this->form_validation->set_rules('user_identification', 'user_identification', 'trim|required|callback_identification_check');
        $this->form_validation->set_rules('user_firstName', 'user_firstName', 'trim|required');
        $this->form_validation->set_rules('user_lastName', 'user_lastName', 'trim|required');
        $this->form_validation->set_rules('user_age', 'user_age', 'trim|required');
        $this->form_validation->set_rules('user_gender', 'user_gender', 'trim|required');
        $this->form_validation->set_rules('Country_country_id', 'Country_country_id', 'trim|required');
        $this->form_validation->set_rules('user_email', 'user_email', 'trim|required|valid_email|callback_email_check');
        $this->form_validation->set_rules('user_password', 'user_password', 'trim|required');
        $this->form_validation->set_rules('user_password_re', 'user_password_re', 'trim|required|matches[user_password]');
        
		if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {
			$password = openssl_encrypt($input["user_password"], "AES-128-ECB", SECRETKEY);
			$User = array(
                    "user_password" =>$password,
                    "user_identification" =>$input["user_identification"],
                    "user_firstName" =>$input["user_firstName"],
                    "user_lastName" =>$input["user_lastName"],
                    "user_age" =>$input["user_age"],
                    "user_email" =>$input["user_email"],
                    "Country_country_id" =>$input["Country_country_id"],
                    "user_gender" =>$input["user_gender"],
                    "user_loginWith" =>"Email",
                    "user_isActive" =>1,
					"UserRole_userRole_id" =>5,
                    "user_createdDTTM" =>date("Y-m-d H:i:s"),
			);
			$res = $this->user_model->AddUserData($User);
        }

        $this->response($res, REST_Controller::HTTP_OK);
        
	}
	public function takeregisterwebsocialmedia_post()
	{
		$input = $this->input->post();
		$this->form_validation->set_rules('user_firstName', 'user_firstName', 'trim|required');
        //$this->form_validation->set_rules('user_lastName', 'user_lastName', 'trim|required');
        $this->form_validation->set_rules('user_token', 'user_token', 'trim|required');
        $this->form_validation->set_rules('user_email', 'user_email', 'trim|required|valid_email');
		if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => FALSE,'error'=>'validation', 'msg' => validation_errors(),'login'=>FALSE);
        }
        else
        {
			$query = $this->user_model->GetUserData(array('user_token'=>$input["user_token"]));
			$User = array(
				"user_firstName" =>$input["user_firstName"],
				"user_lastName" =>$input["user_lastName"],
				"user_profile_pic_url" =>$input["user_profile_pic_url"],
				"user_email" =>$input["user_email"],
				"user_token" =>$input["user_token"],
				"user_loginWith" =>$input["user_loginWith"],
				"Country_country_id" =>219,
				"user_isActive" =>1,
				"UserRole_userRole_id" =>5,
				"user_createdDTTM" =>date("Y-m-d H:i:s"),
			);
			if(count($query) == 0)
            {
                $res_add = $this->user_model->AddUserData($User);
				$query_afer_add = $this->user_model->GetUserData(array('user_token'=>$input["user_token"]));
				if($res_add['state'])
				{
					$user_data = array(
						"user_id" =>$query_afer_add[0]->user_id,
						"user_firstName" =>$query_afer_add[0]->user_firstName,
						"user_lastName" =>$query_afer_add[0]->user_lastName,
						"user_email" =>$query_afer_add[0]->user_email,
						"user_loginWith" =>$query_afer_add[0]->user_loginWith,
						"user_updatedDTTM" =>$query_afer_add[0]->user_updatedDTTM,
						"user_profile_pic_url" =>$query_afer_add[0]->user_profile_pic_url,
					);
					$res = array('state' => TRUE,'error'=>'', 'msg' => 'successfully','login'=>TRUE,'user_data'=>$user_data);
				}
				else
				{
					$res = array('state' => FALSE,'error'=>'Register', 'msg' => $res_add['msg'],'login'=>FALSE,'user_data'=>[]);
				}
				
            }
            else
            {
				$user_data_retrun = array(
					"user_id" =>$query[0]->user_id,
					"user_firstName" =>$query[0]->user_firstName,
					"user_lastName" =>$query[0]->user_lastName,
					"user_email" =>$query[0]->user_email,
					"user_loginWith" =>$query[0]->user_loginWith,
					"user_profile_pic_url" =>$query[0]->user_profile_pic_url,
					"user_updatedDTTM" =>$query[0]->user_updatedDTTM,
				);
				$user_data_update = array(
                    
					"user_firstName" =>$input["user_firstName"],
					"user_lastName" =>$input["user_lastName"],
					"user_profile_pic_url" =>$input["user_profile_pic_url"],
					"user_email" =>$input["user_email"],
					"user_token" =>$input["user_token"],
                    "user_updatedDTTM" =>date("Y-m-d H:i:s"),
                );
                $res = $this->user_model->UpdateUserData($user_data_update,"user_id = ".$query[0]->user_id);
				
				$res = array('state' => TRUE,'error'=>'', 'msg' => 'successfully','login'=>TRUE,'user_data'=>$user_data_retrun);
			}
			
		}
		$this->response($res, REST_Controller::HTTP_OK);
	}
    public function email_check($email)
	{
		if (!$this->user_model->email_check($email))
		{
			$this->form_validation->set_message('email_check', 'This %s has already been used.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	public function identification_check($identification)
	{
		if (!$this->user_model->identification_check($identification))
		{
			$this->form_validation->set_message('identification_check', 'This %s has already been used.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
}