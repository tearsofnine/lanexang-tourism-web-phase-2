<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
date_default_timezone_set('Asia/Bangkok');
class OrderDetails extends REST_Controller
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->db->db_debug = false;
        $this->load->model("ItemsModels","items_models", true);
        $this->load->model("BookingModels","booking_models", true);
        $this->load->model("PaymentTransactionModels","payment_transaction_models", true);
        $this->load->model("OrderDetailsModels","order_details_models", true);
        
    }
    public function index_get()
    {
        $data = new stdClass();
        if($this->get('user_id') && $this->get('user_id') != "")
        {
            $query = $this->payment_transaction_models->GetPaymentTransactionDataOrderDetails("User_user_id = ".$this->get('user_id')." ".($this->get('payment_transaction_id') != ""?" AND paymentTransaction_id = ".$this->get('payment_transaction_id'):""));
        }
        else
        {
            $query =[];
        }
        $data->OrderDetails = $query;
        $this->response($data, 200);
    }
}