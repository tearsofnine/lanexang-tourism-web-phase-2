<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'libraries/REST_Controller.php');
date_default_timezone_set('Asia/Bangkok');
class Chat extends REST_Controller
{
    public function __construct() 
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->model("ChatModels","chat_models", true);
        $this->load->model("GCMModels","gcm_models", true);
        $this->load->model("PushModels","push_models", true);
        $this->load->model("UserModel","user_model", true);
    }
    
    public function index_get()
    {

    }

    public function index_post()
    {

    }

    public function updateCurrentToken_post()
    {
        $input = $this->input->post();
        $this->form_validation->set_rules('currentToken', 'currentToken', 'trim|required');
        $this->form_validation->set_rules('user_id', 'user_id', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {
            $data_update = array(
                "gcm_registration_id"=>$input["currentToken"]
            );
            $res = $this->user_model->UpdateUserData($data_update,"user_id = ".$input["user_id"]);
        }
        $this->response($res, REST_Controller::HTTP_OK);
    }

    public function sendingMessage_post()
    {
        $input = $this->input->post();
        $data = new stdClass();

        $this->form_validation->set_rules('to_user_id', 'to_user_id', 'trim|required');
        $this->form_validation->set_rules('from_user_id', 'from_user_id', 'trim|required');
        $this->form_validation->set_rules('message', 'message', 'trim|required');
        if ($this->form_validation->run() == FALSE) 
		{
            $data->ChatMessages = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {
            $from_user_id = $input["from_user_id"];
            $to_user_id = $input["to_user_id"];
            $message = $input["message"];
            $items_id = (intval($this->input->post("items_id"))?intval($this->input->post("items_id")):-1);
           
            $where ="(UserSend_user_id = ".$from_user_id." OR UserReceive_user_id = ".$from_user_id.") AND (UserSend_user_id = ".$to_user_id." OR UserReceive_user_id = ".$to_user_id.")";
            $chatroom_data = $this->chat_models->get_chat_room($where,$from_user_id, $to_user_id,$items_id);
            
            $user_data_receive = $this->user_model->GetCheckUserData("user_id = ".$to_user_id);

            $message_data = array(
                "ChatRooms_chatroom_id"=> $chatroom_data[0]->chatroom_id,
                "message"=> $message,
                "User_user_id"=> $from_user_id,
                // "messages_read"=> 1,
            );
            $res_add_messages = $this->chat_models->add_messages($message_data);

            $res_send = $this->gcm_models->send($user_data_receive[0]->gcm_registration_id,$message);
            // $res_send_json = json_decode($res_send);
            // $res_send_json->results[0]->message_id = $res_add_messages["messages_id"];

            // $res = array('state' => true,'error'=>'', 'msg' => $message,"gcm_state"=>$res_send_json);  


            $data->ChatMessages = $this->chat_models->get_messages("msg.ChatRooms_chatroom_id = ".$chatroom_data[0]->chatroom_id." AND msg.messages_id = ".$res_add_messages["messages_id"]);
            //$res = $data->ChatMessages;
        }

        $this->response($data, REST_Controller::HTTP_OK);
    }

    public function getChatMessages_get()
    {
        $segment_chatroom_id = 5;
        $data = new stdClass();
        if($this->uri->segment($segment_chatroom_id) )
        {
            $data->ChatMessages = $this->chat_models->get_messages("msg.ChatRooms_chatroom_id = ".$this->uri->segment($segment_chatroom_id));
        }
        else
        {
            $data->ChatMessages = null;
        }
        $this->response($data, REST_Controller::HTTP_OK);
       
    }
    
    public function getListMessage_get()
    {
        $segment_user_id = 5;
        $data = new stdClass();

        if($this->uri->segment($segment_user_id) && ($this->get('type') && $this->get('type') != ""))
        {
            if(intval($this->get('type')) == 1)
                $data->ChatRoomData = $this->chat_models->get_chat_room_for_table("cr.Items_items_id is Null AND (UserSend_user_id = ".$this->uri->segment($segment_user_id)." OR UserReceive_user_id = ".$this->uri->segment($segment_user_id).")",$this->uri->segment($segment_user_id));
            else if(intval($this->get('type')) == 2)
                $data->ChatRoomData = $this->chat_models->get_chat_room_for_table("cr.Items_items_id is not Null AND (UserSend_user_id = ".$this->uri->segment($segment_user_id)." OR UserReceive_user_id = ".$this->uri->segment($segment_user_id).")",$this->uri->segment($segment_user_id));
            else if(intval($this->get('type')) == 3)
				$data->ChatRoomData = $this->chat_models->get_chat_room_for_table("(UserSend_user_id = ".$this->uri->segment($segment_user_id)." OR UserReceive_user_id = ".$this->uri->segment($segment_user_id).")",$this->uri->segment($segment_user_id));
			else
                $data->ChatRoomData = [];
        }
        else
        {
            $data->ChatRoomData = null;
        }
        $this->response($data, REST_Controller::HTTP_OK);
    }

    
    public function getUnReadMessages_get()
    {
        $segment_user_id = 5;
        $data = new stdClass();

        if($this->uri->segment($segment_user_id))
        {
            
                $data->UnReadMessages = $this->chat_models->getUnReadMessages($this->uri->segment($segment_user_id));
                if($data->UnReadMessages[0]->count_read[0] == null )
                {
                    $data->UnReadMessages[0]->count_read = "0";
                }
        }
        else
        {
            $data->UnReadMessages[] = array(
                "count_read" => "0"
            );
        }
        $this->response($data, REST_Controller::HTTP_OK);
    }
    
    public function updateReadMessages_post()
    {
        $input = $this->input->post();
        $this->form_validation->set_rules('chatroom_id', 'chatroom_id', 'trim|required');
        $this->form_validation->set_rules('user_id', 'user_id', 'trim|required');

        if ($this->form_validation->run() == FALSE) 
		{
			$res = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        else
        {
            $res = $this->chat_models->update_messages(array("messages_read"=>1),"ChatRooms_chatroom_id = ".$input["chatroom_id"]." AND User_user_id != ".$input["user_id"]);
        }
        $this->response($res, REST_Controller::HTTP_OK);
    }
    public function getChatRoom_get()
    {
        $data = new stdClass();

        if( ($this->get('from_user_id') && $this->get('from_user_id') != "") && ($this->get('to_user_id') && $this->get('to_user_id') != "")) 
		{
            $from_user_id = $this->get('from_user_id');
            $to_user_id = $this->get('to_user_id');
            $items_id = -1;

            if($this->get('items_id') && $this->get('items_id') != "")
                $items_id = $this->get('items_id');

            $where ="(UserSend_user_id = ".$from_user_id." OR UserReceive_user_id = ".$from_user_id.") AND (UserSend_user_id = ".$to_user_id." OR UserReceive_user_id = ".$to_user_id.")";
            $data->ChatRoomData = $this->chat_models->get_chat_room($where,$from_user_id, $to_user_id,$items_id);
			
        }
        else
        {
            $data->ChatRoomData = array('state' => false,'error'=>'validation', 'msg' => validation_errors());
        }
        $this->response($data, REST_Controller::HTTP_OK);
    }
}