<!-- Mainly scripts -->
<script src="<?php echo $host;?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $host;?>assets/js/popper.min.js"></script>
<script src="<?php echo $host;?>assets/js/bootstrap.js"></script>

<script src="<?php echo $host;?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo $host;?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo $host;?>assets/js/inspinia.js"></script>
<script src="<?php echo $host;?>assets/js/plugins/pace/pace.min.js"></script>
<!-- slick carousel-->
<script src="<?php echo $host;?>assets/js/plugins/slick/slick.min.js"></script>
<script src="<?php echo $host;?>assets/js/plugins/owlcarousel/owl.carousel.min.js"></script>

<!-- Chosen -->
<script src="<?php echo $host;?>assets/js/plugins/chosen/chosen.jquery.js"></script>
<!-- iCheck -->
<script src="<?php echo $host;?>assets/js/plugins/iCheck/icheck.min.js"></script>

<!-- Date range use moment.js same as full calendar plugin -->
<script src="<?php echo $host;?>assets/js/plugins/fullcalendar/moment.min.js"></script>

<script src="<?php echo $host;?>assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDrdQG5bfvPftYnMMdd8dOdgcl1GvODN6U&callback=initMap"type="text/javascript"></script>

<script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/NotificationsEvent.js"></script>

<!-- Insert these scripts at the bottom of the HTML, but before you use any Firebase services -->

<!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-app.js"></script>

<!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
<script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-analytics.js"></script>

<!-- Add Firebase products that you want to use -->
<script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-firestore.js"></script>

<script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-messaging.js"></script>

<script src="<?php echo $host;?>assets/js/custom_js/firebase-init.js"></script>
