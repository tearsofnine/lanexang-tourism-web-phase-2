<link href="<?php echo $host;?>assets/entrepreneur/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $host;?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo $host;?>assets/entrepreneur/css/animate.css" rel="stylesheet">
    <link href="<?php echo $host;?>assets/entrepreneur/css/style.css" rel="stylesheet">

    <link href="<?php echo $host;?>assets/css/plugins/slick/slick.css" rel="stylesheet">
    <link href="<?php echo $host;?>assets/css/plugins/slick/slick-theme.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo $host;?>assets/css/plugins/owlcarousel/owl.carousel.min.css" >
    <link rel="stylesheet" href="<?php echo $host;?>assets/css/plugins/owlcarousel/owl.theme.default.min.css" >

    <link href="<?php echo $host;?>assets/css/plugins/iCheck/custom.css" rel="stylesheet">
    
    <link href="<?php echo $host;?>assets/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">

    <!-- dataTables css -->
    <link href="<?php echo $host;?>assets/css/jquery.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">