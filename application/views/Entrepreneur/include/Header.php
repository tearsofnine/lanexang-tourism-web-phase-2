<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-outline btn-default " href="#"><i class="fa fa-bars"></i></a>
            <?php 
            if(!strpos(current_url(),"/entrepreneur/Main"))
                echo "
                <a class=\"minimalize-styl-2 btn btn-outline btn-default \" href=\"javascript:history.go(-1)\" style=\"margin: 14px 5px 5px 5px;\">
                    <i class=\"fa fa-angle-left\"></i>
                </a>";
            ?>
            
        </div>
        <ul class="nav navbar-top-links navbar-center">
            <li class="text-center">
                <h3 class="m-r-sm text-muted" id="header_bar_1">จัดการข้อมูลเบื่องต้น</h3>
                <span class="m-r-sm text-muted " id="header_bar_2"></span>
            </li>
        </ul>
        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                    <i class="fa fa-envelope"></i>
                    <!-- <span class="label label-warning">16</span> -->
                </a>

                <!-- <ul class="dropdown-menu dropdown-messages dropdown-menu-right">
                    <li>
                        <div class="dropdown-messages-box">
                            <a class="dropdown-item float-left" href="">
                                <img alt="image" class="rounded-circle" src="<?php echo $host;?>assets/img/a7.jpg">
                            </a>
                            <div class="media-body">
                                <small class="float-right">46h ago</small>
                                <strong>Mike Loreipsum</strong> started following
                                <strong>Monica Smith</strong>.
                                <br>
                                <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                            </div>
                        </div>
                    </li>
                    <li class="dropdown-divider"></li>
                    <li>
                        <div class="dropdown-messages-box">
                            <a class="dropdown-item float-left" href="">
                                <img alt="image" class="rounded-circle" src="<?php echo $host;?>assets/img/a4.jpg">
                            </a>
                            <div class="media-body ">
                                <small class="float-right text-navy">5h ago</small>
                                <strong>Chris Johnatan Overtunk</strong> started following
                                <strong>Monica Smith</strong>.
                                <br>
                                <small class="text-muted">Yesterday 1:21 pm - 11.06.2014</small>
                            </div>
                        </div>
                    </li>
                </ul> -->
                
            </li>
        </ul>
    </nav>
</div>