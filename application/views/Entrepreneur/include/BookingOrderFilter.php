<div class="ibox-content-head">
    <h3 class="text-muted font-bold">จำนวนทั้งหมด <span id="header_count_list"></span> การจอง</h3>
</div>
<div class="input-group">
    <input type="text" class="form-control" id="CustomSearchBox">
    <div class="input-group-append">
        <button data-toggle="dropdown" class="btn btn-muted dropdown-toggle" type="button" aria-expanded="false">เรียงตาม</button>
        <ul class="dropdown-menu float-right" x-placement="bottom-start" style="position: absolute; top: 35px; left: 245px; will-change: top, left;">
            <li style="margin: 5px;"> 
                <div class="form-check-inline i-checks" >
                    <label class="form-check-label" >
                        <input type="checkbox" class="form-check-input " name="last" checked><i></i> วันที่ทำการจองเข้ามาล่าสุด
                    </label>
                </div>
                <!-- <input type="checkbox" class ="form-control form-check-label">วันที่ทำการจองเข้ามาล่าสุด</input> -->
            </li>
            <li class="dropdown-divider"></li>
            <li style="margin: 5px;">
                <div class="form-check-inline i-checks">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input state_approv" value="2" name="state_approv" checked><i></i> การจองที่รอการยอมรับ
                    </label>
                </div>
               
            </li>
            <li style="margin: 5px;">
                <div class="form-check-inline i-checks" >
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input state_approv" value="1" name="state_approv"><i></i> การจองที่ยอมรับแล้ว
                    </label>
                </div>
               
            </li>
            <li style="margin: 5px;">
                <div class="form-check-inline i-checks" >
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input state_approv" value="3" name="state_approv"><i></i> การจองที่ปฏิเสธ
                    </label>
                </div>
               
            </li> 
            <li style="margin: 5px;">
                <div class="form-check-inline i-checks">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input state_approv" value="0" name="state_approv"><i></i> การจองที่หมดอายุแล้ว
                    </label>
                </div>
               
            </li>
            <li class="dropdown-divider"></li>
        </ul>
    </div>
</div>