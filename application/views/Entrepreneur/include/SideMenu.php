<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="block m-t-xs font-bold"><?php echo $this->session->userdata("entrepreneur_user_firstName")." ".$this->session->userdata("entrepreneur_user_lastName");?></span>
                        <span class="text-muted text-xs block">menu
                            <b class="caret"></b>
                        </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li>
                            <a class="dropdown-item" href="/dasta_thailand/entrepreneur/EditUserData">แก้ไขข้อมูล</a>
                        </li>
                        <li>
                            <a class="dropdown-item" data-toggle="modal" data-target="#ChangePasswordModal" href="#" id="ChangePassword">แก้ไขรหัสผ่าน</a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="/dasta_thailand/entrepreneur/Login/logout">ออกจากระบบ</a>
                        </li>
                    </ul>
                </div>
                <div class="logo-element">

                </div>
            </li>
            <li id="sidebar_Main">
                <a href="" id="sidebar_Main_a">
                   
                    <span class="nav-label">หน้าหลัก</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse" id="sidebar_Main_ul">
                    <li id="sidebar_Main_li">
                        <a href="<?php echo $host;?>entrepreneur/Main"><span class="nav-label">หน้าหลัก</span></a>
                    </li>
                    <?php
                        switch (intval($this->session->userdata('business_type_category_id'))) 
                        {
                            case 4:
                            case 5:
                            case 9:
                            case 10:
                            case 11:
                                echo "
                                <li id=\"sidebar_BookingOrder\">
                                    <a href=\"".$host."entrepreneur/BookingOrder\"><span class=\"nav-label\">เช็คยอดการจองที่เข้ามา</span></a>
                                </li> 
                                <li id=\"sidebar_Report\">
                                    <a href=\"".$host."entrepreneur/DataShowReport\"><span class=\"nav-label\">รายงานแสดงข้อมูล</span></a>
                                </li> 
                                ";
                            break;
                        }
                    ?>
                    
                    <li id="sidebar_Information" class="btn_information">
                        <a href="" class="btn_information"><span class="nav-label ">ข้อมูลเกี่ยวกับกิจการของท่าน</span></a>
                    </li>
                    <?php
                        if(intval($this->session->userdata('business_type_category_id')) == 9)
                        {
                            echo "
                                <li id=\"sidebar_ProgramTour\">
                                    <a href=\"".$host."entrepreneur/business/ProgramTour\"><span class=\"nav-label\">ข้อมูลเกี่ยวกับโปรแกรมท่องเที่ยวของท่าน</span></a>
                                </li> ";
                        }
                    ?>
               
                </ul>
            </li>
            <!-- <li>
                <a href=""><span class="nav-label">คูปอง/โค๊ดส่วนลด</span></span></a>
            </li> -->
            <li id="sidebar_Notifications">
                <a href="<?php echo $host;?>entrepreneur/Notifications"><span class="nav-label"><?=$this->lang->line("notifications");?></span></a>
            </li>
        </ul>
    </div>
</nav>
<?php $this->load->view('Entrepreneur/include/ChangePasswordModal.php');?>
