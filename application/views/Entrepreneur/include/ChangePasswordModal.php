<div class="modal fade" id="ChangePasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Change Password</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <label for="thTravelPlaceDescription" class="col-sm-4 col-xs-4 col-form-label">Old Password</label>
                    <div class="col-sm-6 col-xs-6">
                        <input type="test" style="-webkit-text-security: disc;text-security: disc;" placeholder="Old Password" class="form-control form-control-input flat-input ChangePasswordModals" id="oldpass"  required >
                        <span class="note notifi" id="notifi_oldpass"><span>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="thTravelPlaceDescription" class="col-sm-4 col-xs-4 col-form-label">New Password</label>
                    <div class="col-sm-6 col-xs-6">
                        <input type="text" style="-webkit-text-security: disc;text-security: disc;" placeholder="New Password" class="form-control form-control-input flat-input ChangePasswordModals" id="newpass" required>
                        <span class="note notifi" id="notifi_newpass"><span>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="thTravelPlaceDescription" class="col-sm-4 col-xs-4 col-form-label">Confirm Password</label>
                    <div class="col-sm-6 col-xs-6">
                    <input type="text" style="-webkit-text-security: disc;text-security: disc;" placeholder="Confirm Password"  class="form-control form-control-input flat-input ChangePasswordModals" id="conpass" required>
                        <span class="note notifi" id="notifi_conpass"><span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col">
                        <span class="note notifi" id="notifi_error_save_ChangePassword"><span>
                    </div>
                    <div class="row">
                        <div class="col-md-6" >
                            <button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-success" type="button" id="btn_save_ChangePassword_entrepreneur">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>