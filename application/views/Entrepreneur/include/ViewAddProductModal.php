<div class="modal fade bd-example-modal-xl" id="ViewAddProductModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <h5 class="modal-title" style="color: #1AB394;" id="header_view_modal">ViewAddProductModal</h5> -->
                <h5 class="modal-title" id="header_view_modal_product">ViewAddProductModal</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            
            <div class="col-sm-12 col-xs-12">
                <div class="ibox selected">
                    <div class="ibox-content">
                        <div class="tab-content">
                            <div id="" class="">
                                <div class="client-detail">
                                    <div class="full-height-scroll" id="items_content">

                                        <?php
                                            switch ($ModalView) {
                                                case 'Hotel':
                                                    $this->load->view('Entrepreneur/ModalView/ModalViewHotel.php');
                                                break;
                                                case 'Restaurant':
                                                    $this->load->view('Entrepreneur/ModalView/ModalViewRestaurant.php');
                                                break;
                                                case 'Shopping':
                                                    $this->load->view('Entrepreneur/ModalView/ModalViewShopping.php');
                                                break;
                                            }
                                        ?>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <div class="row">
                    <div class="col">
                        <span class="note notifi" id="notifi_error_save_product"><span>
                    </div>
                    <div class="row">
                        <div class="col-md-6" >
                            <button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-success" type="button" id="btn_save_product">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>