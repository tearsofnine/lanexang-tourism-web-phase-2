<!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>จัดการระบบ สำหรับผู้ประกอบการ เส้นทางท่องเที่ยว สี่เหลี่ยมวัฒนธรรมล้านช้าง</title>

        <link href="<?php echo $host;?>assets/entrepreneur/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $host;?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo $host;?>assets/entrepreneur/css/animate.css" rel="stylesheet">
        <link href="<?php echo $host;?>assets/entrepreneur/css/style.css" rel="stylesheet">

    </head>

    <body>
        <div id="wrapper">
            <!-- SideMenu -->
            <?php $this->load->view('Entrepreneur/include/SideMenu.php');?>
            <!-- End of SideMenu -->
            <div id="page-wrapper" class="gray-bg">
                <!-- Header -->
                <?php $this->load->view('Entrepreneur/include/Header.php');?>
                <!-- End of Header -->

                <div class="wrapper wrapper-content animated fadeInRight">
                <?php
                    switch (intval($this->session->userdata('business_type_category_id'))) 
                    {
                        case 4:
                        case 5:
                        case 9:
                        case 10:
                        case 11:
                            echo "
                            <div class=\"row\">
                                <div class=\"col-lg-3\"></div>
                                <div class=\"col-lg-6\">
                                    <a id=\"btn_booking_order\">
                                        <div class=\"ibox \">
                                            <div>
                                                <div class=\"ibox-content no-padding border-left-right\">
                                                    <img alt=\"image\" class=\"img-fluid\" src=\"".$host."assets/img/10219848.png\">
                                                </div>
                                                <div class=\"ibox-content profile-content lazur-bg\">
                                                    <h4>
                                                        <strong>เช็คยอดการจองที่เข้ามา".(intval($countBookingData) != 0?"<span class=\"label label-danger\">".$countBookingData."</span>":"")."</strong>
                                                    </h4>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <div class=\"col-lg-3\"></div>
                                </div>
                            </div>
                            <div class=\"row\">
                                <div class=\"col-lg-3\"></div>
                                <div class=\"col-lg-6\">
                                    <a id=\"btn_report\">
                                        <div class=\"ibox \">
                                            <div>
                                                <div class=\"ibox-content no-padding border-left-right\">
                                                    <img alt=\"image\" class=\"img-fluid\" src=\"".$host."assets/img/23343621.png\">
                                                </div>
                                                <div class=\"ibox-content profile-content lazur-bg\">
                                                    <h4>
                                                        <strong>รายงานแสดงข้อมูล</strong>
                                                    </h4>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <div class=\"col-lg-3\"></div>
                                </div>
                            </div>
                            ";
                            break;
                        
                        default:
                            # code...
                            break;
                    }
                ?>

                    <div class="row">
                        <div class="col-lg-3"></div>
                        <div class="col-lg-6">
                            <a id="btn_information" class="btn_information">
                                <div class="ibox ">
                                    <div>
                                        <div class="ibox-content no-padding border-left-right">
                                            <img alt="image" class="img-fluid" src="<?php echo $host;?>assets/img/Group 2047.png">
                                        </div>
                                        <div class="ibox-content profile-content lazur-bg">
                                            <h4>
                                                <strong>ข้อมูลเกี่ยวกับกิจการของท่าน</strong>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <div class="col-lg-3">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

    <!-- Mainly scripts -->
    <script src="<?php echo $host;?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo $host;?>assets/js/popper.min.js"></script>
    <script src="<?php echo $host;?>assets/js/bootstrap.js"></script>

    <script src="<?php echo $host;?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo $host;?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo $host;?>assets/js/inspinia.js"></script>
    <script src="<?php echo $host;?>assets/js/plugins/pace/pace.min.js"></script>

    <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/NotificationsEvent.js"></script>
    <!-- Insert these scripts at the bottom of the HTML, but before you use any Firebase services -->

    <!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-app.js"></script>

    <!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
    <script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-analytics.js"></script>

    <!-- Add Firebase products that you want to use -->
    <script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-firestore.js"></script>

    <script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-messaging.js"></script>

    <script src="<?php echo $host;?>assets/js/custom_js/firebase-init.js"></script>
   
    <script>
        var host = "<?php echo $host;?>html";
        var host_entrepreneur = "<?php echo $host;?>entrepreneur";
       
        var MenuName = "Main";
        var UseGoogleMap = 'production';
        var gmarkers = [];
        
        messaging.onMessage(function(payload) {
            console.log("Message received. ", payload);
        });

    </script>
    <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/GlobalFunction.js"></script>
    <!-- <script src="<?php echo $host;?>assets/js/custom_js/GlobalFunction.js"></script> -->
    <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/Main.js"></script>
</html>