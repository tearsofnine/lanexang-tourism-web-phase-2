<!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>จัดการระบบ สำหรับผู้ประกอบการ เส้นทางท่องเที่ยว สี่เหลี่ยมวัฒนธรรมล้านช้าง</title>

        <link href="<?php echo $host;?>assets/entrepreneur/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $host;?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo $host;?>assets/entrepreneur/css/animate.css" rel="stylesheet">
        <link href="<?php echo $host;?>assets/entrepreneur/css/style.css" rel="stylesheet">

    </head>

    <body>
        <div id="wrapper">

            <!-- SideMenu -->
            <?php $this->load->view('Entrepreneur/include/SideMenu.php');?>
            <!-- End of SideMenu -->
            <div id="page-wrapper" class="gray-bg">
                <!-- Header -->
                <?php $this->load->view('Entrepreneur/include/Header.php');?>
                <!-- End of Header -->
               
                <div class="wrapper wrapper-content animated fadeInRight">

                    <div class="row">
                        <div class="col-lg-3"></div>
                        <div class="col-lg-6">
                            <div class="ibox">

                                <div class="slimScroll" id="messagelist">
                                    <div class="ibox-content-head">
                                        <div id="main_chat_table">
                                            <div class="chat-activity-list" id="chat_table">

                                                <!-- <div class="chat-element ">
                                                    <a href="#" class="float-left"><img alt="image" class="rounded-circle" src="img/a6.jpg"></a>
                                                    <div class="media-body ">
                                                        <strong>Maria Snow</strong>
                                                        <p class="m-b-xs">2 วันค่ะ</p>
                                                    </div>
                                                </div>
                                                <div class="chat-element right">
                                                    <a href="#" class="float-right"><img alt="image" class="rounded-circle" src="img/111.png"></a>
                                                    <div class="media-body text-right ">
                                                        <strong>ทัวร์บึงกาฬ ภูทอก วัดผาตากเสื้อ วัดป่าภูก้อน...</strong>
                                                        <p class="m-b-xs">สวัสดี</p>
                                                    </div>
                                                </div>
                                                <div class="chat-element right">
                                                    <a href="#" class="float-right"><img alt="image" class="rounded-circle" src="img/111.png"></a>
                                                    <div class="media-body text-right "><strong>ทัวร์บึงกาฬ ภูทอก วัดผาตากเสื้อ วัดป่าภูก้อน...</strong>
                                                        <p class="m-b-xs">สนใจเป็นกี่วันดีครับ</p>
                                                    </div>
                                                </div> -->

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-3"></div>
                    </div>

                </div>
                <div class="footer gray-bg">
                    <div class="row">
                        <div class="col-lg-3"></div>
                        <div class="col-lg-6 ">
                            <div class="chat-form">
                                <div class="input-group">
                                   <input type="text" class="form-control " placeholder="Write your message…" id="message"> &nbsp; &nbsp;
                                    <button class="btn btn-info btn-circle" type="button"><i class="fa fa-paper-plane" id="send_message"></i></button>
                                </div>
                            </div>
                            <div class="col-lg-3"></div>
                        </div>
                    </div>
                </div>
                <br>

            </div>
        </div>

        <!-- Mainly scripts -->
        <script src="<?php echo $host;?>assets/js/jquery-3.1.1.min.js"></script>
        <script src="<?php echo $host;?>assets/js/popper.min.js"></script>
        <script src="<?php echo $host;?>assets/js/bootstrap.js"></script>

        <script src="<?php echo $host;?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script src="<?php echo $host;?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

        <!-- Custom and plugin javascript -->
        <script src="<?php echo $host;?>assets/js/inspinia.js"></script>
        <script src="<?php echo $host;?>assets/js/plugins/pace/pace.min.js"></script>
        <script src="<?php echo $host;?>assets/js/plugins/pace/pace.min.js"></script>

        <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/NotificationsEvent.js"></script>

        <!-- Insert these scripts at the bottom of the HTML, but before you use any Firebase services -->

        <!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
        <script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-app.js"></script>

        <!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
        <script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-analytics.js"></script>

        <!-- Add Firebase products that you want to use -->
        <script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-auth.js"></script>
        <script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-firestore.js"></script>

        <script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-messaging.js"></script>

        <script src="<?php echo $host;?>assets/js/custom_js/firebase-init.js"></script>
        
        <script>
            var host = "<?php echo $host;?>html";
            var host_entrepreneur = "<?php echo $host;?>entrepreneur";
        
            var MenuName = "Chat";
            var UseGoogleMap = 'Chat';
            var gmarkers = [];

            var user_id ="<?php echo $this->session->userdata("entrepreneur_user_id");?>"
            var chatroom_id ="<?php echo $chatroom_id;?>"
            var name_user ="<?php echo $name_user;?>"

        </script>
        <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/GlobalFunction.js"></script>
        <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/ChatView.js"></script>
    </body>
</html>