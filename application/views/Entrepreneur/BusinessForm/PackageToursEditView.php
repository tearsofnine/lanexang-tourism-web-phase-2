<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Content | Management System</title>
    <?php $this->load->view('Html/include/Header_css.php');?> 
    
</head>
<body>
    <!-- Preloader -->
    <div class="preloader" id="preloader">
        <div class="status" id="status"></div>
    </div>
    <div id="wrapper">
       <!-- SideMenu -->
       <?php $this->load->view('Entrepreneur/include/SideMenu.php');?>
            <!-- End of SideMenu -->

        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header">
                        
                    </div>
                    <ul class="nav navbar-top-links navbar-center">
                        <li>
                            <h3 class="m-r-sm text-muted">แก้ข้อมูลแพคเกจทัวร์</h3>
                        </li>
                    </ul>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <h3 class="m-r-sm text-muted"></h3>
                        </li>
                    </ul>
                </nav>
            </div>
            
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="tabs-container">
                            <ul class="nav nav-tabs" role="tablist">
                                <li>
                                    <a class="nav-link active" data-toggle="tab" href="#tab-1" id="tabTH"><?=$this->lang->line("lang_th");?></a>
                                </li>
                                <li>
                                    <a class="nav-link" data-toggle="tab" href="#tab-2" id="tabENG"><?=$this->lang->line("lang_en");?></a>
                                </li>
                                <li>
                                    <a class="nav-link" data-toggle="tab" href="#tab-3" id="tabLAOS"><?=$this->lang->line("lang_lao");?></a>
                                </li>
                                <li>
                                    <a class="nav-link" data-toggle="tab" href="#tab-4" id="tabCHN"><?=$this->lang->line("lang_chn");?></a>
                                </li>
                            </ul>
                            <div id="summernote"></div>
                            <div class="tab-content">
                                <!--tab-1 Thai -->
                                <div role="tabpanel" id="tab-1" class="tab-pane active">
                                    <div class="">
                                        <div class="row">
                                            <div class="col-lg-12"><!-- รูปภาพหรือวีดีโอหน้าปก -->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รูปภาพหรือวีดีโอหน้าปก</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <h2>
                                                            <?=$this->lang->line("cover_photo");?>
                                                        </h2>
                                                        <p>สามารถเพิ่มกี่รูปก็ได้</p>
                                                        <div class="form-group  row">
                                                            <div class="col-sm-12">
                                                                <div class="owl-carousel owl-theme image_box_package_tours" id="image_box_package_tours">
                                                                 
                                                                </div>
                                                                <div class="text-left" style="margin-top: 10px;">
                                                                    <button class="btn btn-primary btn-xs add_image_field-Reservations" name="image_PackageTours">เพิ่มรูปภาพ</button>
                                                                <!-- <img class="" src="../../../assets/img/image_placeholder.jpg" alt=""> -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12"> <!-- ลิ้งวีดีโอ youtube -->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>วีดีโอ (ถ้ามี)</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ลิ้งวีดีโอ youtube</label>
                                                            <div class="col-sm-10">
                                                                <input type="" placeholder="www.youtube" class="form-control" id="coverItem_url" value ="<?php echo $data_item[0]->CoverItems[0]->coverItem_url;?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12"> <!-- รายละเอียดแพคเกจทัวร์ Thai-->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดแพคเกจทัวร์</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ชื่อแพคเกจทัวร์</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" placeholder="ชื่อแพคเกจทัวร์" class="form-control" id="itmes_topicThai" value ="<?php echo $data_item[0]->itmes_topicThai;?>">
                                                                <span class="note notifi" id="notifi_itmes_topicThai"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ประเภทแพคเกจทัวร์</label>
                                                            <div class="col-sm-10">
                                                                <select data-placeholder="Choose a Country..." class="chosen-select namesubcategory" tabindex="1" id="namesubcategory">
                                                                </select>
                                                                <span class="note notifi" id="notifi_namesubcategory"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ขอบเขตจังหวัด</label>
                                                            <div class="col-sm-10">
                                                                <!-- <select data-placeholder="เลือกขอบเขตจังหวัด" class="chosen-select-province-boundary" tabindex="1" multiple id="province_boundary"> -->
                                                                <select data-placeholder="เลือกขอบเขตจังหวัด" class="form-control chosen" tabindex="1" multiple id="province_boundary">
                                                                </select>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12"><!-- รายละเอียดช่วงเวลาตารางการเดินทาง Thai-->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดช่วงเวลาตารางการเดินทาง</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ช่วงเวลาที่จะเพิ่มข้อมูล</label>
                                                            <div class="col-sm-10">
                                                                <div class="input-group">
                                                                    <input type="number" placeholder="เพิ่มช่วงเวลาในการเดินทางในแต่ล่ะรอบของแพคเกจทัวร์" class="form-control-sm form-control num-box-TravelSchedule typenumber" min="0" max="10" id="num_box_travel_schedule" value="<?php echo count($data_item[0]->TravelPeriod);?>">
                                                                    <span class="input-group-btn">
                                                                        <button type="button" class="btn btn-sm btn-primary add_PackageTours_field" name="TravelSchedule"> <?=$this->lang->line("add");?></button>
                                                                    </span>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                          
                                            <div class="col-lg-12" id="create_field_TravelSchedule"><!-- TravelSchedule Thai-->
                                                <div class="" id="box_TravelSchedule">
                                                    <!-- javascript  -->
                                                    
                                                    <!-- end javascript  -->
                                                </div>
                                            </div>
                                            <div class="col-lg-12"><!-- รายละเอียดการเดินทาง Thai-->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดการเดินทาง</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ระยะเวลา</label>
                                                            <div class="col-sm-10">
                                                                <div class="input-group">
                                                                    <input type="number" placeholder="ระยะวัน ของแพคเกจทัวร์" class="form-control-sm form-control typenumber" min="0" max="10" id="num_box_travel_details" value="<?php echo count($data_item[0]->TravelDetails);?>">
                                                                    <span class="input-group-btn">
                                                                        <button type="button" class="btn btn-sm btn-primary add_PackageTours_field" name="TravelDetails"> <?=$this->lang->line("add");?></button>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12" id="create_field_TravelDetails"> <!-- TravelDetails Thai-->
                                                <div class="" id="box_TravelDetails">
                                                    <!-- javascript  -->
                                                    
                                                    <!-- end javascript  -->
                                                </div>
                                            </div>
                                            
                                            <div class="col-lg-12"><!-- รายละเอียดเงื่อนไขในการจอง Thai-->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดเงื่อนไขในการจอง</h5>
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">อัตราค่าบริการนี้รวม
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด อัตราค่าบริการ" rows="4" id="bookingConditioncol_detailThai_id_1"><?php echo $data_item[0]->BookingCondition[0]->bookingConditioncol_detailThai;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">อัตราค่าบริการนี้ไม่รวม
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด อัตราค่าบริการ" rows="4" id="bookingConditioncol_detailThai_id_2"><?php echo $data_item[0]->BookingCondition[1]->bookingConditioncol_detailThai;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">การยกเลิกการเดินทาง
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด การยกเลิกการเดินทาง" rows="4" id="bookingConditioncol_detailThai_id_3"><?php echo $data_item[0]->BookingCondition[2]->bookingConditioncol_detailThai;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">การเลื่อนการเดินทาง
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด การเลื่อนการเดินทาง" rows="4" id="bookingConditioncol_detailThai_id_4"><?php echo $data_item[0]->BookingCondition[3]->bookingConditioncol_detailThai;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">สิ่งที่ควรนำไปด้วย
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด สิ่งที่ควรนำไปด้วย" rows="4" id="bookingConditioncol_detailThai_id_5"><?php echo $data_item[0]->BookingCondition[4]->bookingConditioncol_detailThai;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">เงื่อนไขการเดินทาง
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด เงื่อนไขการเดินทาง" rows="4" id="bookingConditioncol_detailThai_id_6"><?php echo $data_item[0]->BookingCondition[5]->bookingConditioncol_detailThai;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">การจองและสำรองที่นั่ง
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด การจองและสำรองที่นั่ง" rows="4" id="bookingConditioncol_detailThai_id_7"><?php echo $data_item[0]->BookingCondition[6]->bookingConditioncol_detailThai;?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12"><!-- เบอร์โทรที่สามารถติดต่อกลับ -->
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>เบอร์โทรที่สามารถติดต่อกลับ</h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">เบอร์</label>
                                                                <div class="col-sm-10">
                                                                    <div class="input-group">
                                                                            <input type="text" class="form-control telephone" placeholder="" id="items_phone" value ="<?php echo $data_item[0]->items_phone;?>">
                                                                    </div>
    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- tab-2 English-->
                                <div role="tabpanel" id="tab-2" class="tab-pane">
                                    <div class="">
                                        <div class="row">

                                            <div class="col-lg-12"> <!-- รายละเอียดแพคเกจทัวร์ English-->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดแพคเกจทัวร์</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ชื่อแพคเกจทัวร์</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" placeholder="ชื่อแพคเกจทัวร์" class="form-control" id="itmes_topicEnglish" value ="<?php echo $data_item[0]->itmes_topicEnglish;?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- TravelDetails English-->
                                            <div class="col-lg-12"><!-- รายละเอียดการเดินทาง English-->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดการเดินทาง</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="col-lg-12" id="create_field_English_TravelDetails"><!-- TravelDetails English-->
                                                            <div class="" id="box_English_TravelDetails">
                                                                <!-- javascript  -->
                                                                
                                                                <!-- end javascript  -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>   
                                            <div class="col-lg-12"><!-- รายละเอียดเงื่อนไขในการจอง English-->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดเงื่อนไขในการจอง</h5>
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">อัตราค่าบริการนี้รวม
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด อัตราค่าบริการ" rows="4" id="bookingConditioncol_detailEnglish_id_1"><?php echo $data_item[0]->BookingCondition[0]->bookingConditioncol_detailEnglish;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">อัตราค่าบริการนี้ไม่รวม
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด อัตราค่าบริการ" rows="4" id="bookingConditioncol_detailEnglish_id_2"><?php echo $data_item[0]->BookingCondition[1]->bookingConditioncol_detailEnglish;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">การยกเลิกการเดินทาง
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด การยกเลิกการเดินทาง" rows="4" id="bookingConditioncol_detailEnglish_id_3"><?php echo $data_item[0]->BookingCondition[2]->bookingConditioncol_detailEnglish;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">การเลื่อนการเดินทาง
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด การเลื่อนการเดินทาง" rows="4" id="bookingConditioncol_detailEnglish_id_4"><?php echo $data_item[0]->BookingCondition[3]->bookingConditioncol_detailEnglish;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">สิ่งที่ควรนำไปด้วย
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด สิ่งที่ควรนำไปด้วย" rows="4" id="bookingConditioncol_detailEnglish_id_5"><?php echo $data_item[0]->BookingCondition[4]->bookingConditioncol_detailEnglish;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">เงื่อนไขการเดินทาง
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด เงื่อนไขการเดินทาง" rows="4" id="bookingConditioncol_detailEnglish_id_6"><?php echo $data_item[0]->BookingCondition[5]->bookingConditioncol_detailEnglish;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">การจองและสำรองที่นั่ง
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด การจองและสำรองที่นั่ง" rows="4" id="bookingConditioncol_detailEnglish_id_7"><?php echo $data_item[0]->BookingCondition[6]->bookingConditioncol_detailEnglish;?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- tab-3 Laos-->
                                <div role="tabpanel" id="tab-3" class="tab-pane">
                                    <div class="">
                                        <div class="row">
                                            <div class="col-lg-12"> <!-- รายละเอียดแพคเกจทัวร์ Laos-->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดแพคเกจทัวร์</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ชื่อแพคเกจทัวร์</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" placeholder="ชื่อแพคเกจทัวร์" class="form-control" id="itmes_topicLaos" value ="<?php echo $data_item[0]->itmes_topicLaos;?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- TravelDetails Laos-->
                                            <div class="col-lg-12"><!-- รายละเอียดการเดินทาง Laos-->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดการเดินทาง</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="col-lg-12" id="create_field_Laos_TravelDetails"><!-- TravelDetails Laos-->
                                                            <div class="" id="box_Laos_TravelDetails">
                                                                <!-- javascript  -->
                                                                
                                                                <!-- end javascript  -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="col-lg-12"><!-- รายละเอียดเงื่อนไขในการจอง Laos-->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดเงื่อนไขในการจอง</h5>
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">อัตราค่าบริการนี้รวม
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด อัตราค่าบริการ" rows="4" id="bookingConditioncol_detailLaos_id_1"><?php echo $data_item[0]->BookingCondition[0]->bookingConditioncol_detailLaos;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">อัตราค่าบริการนี้ไม่รวม
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด อัตราค่าบริการ" rows="4" id="bookingConditioncol_detailLaos_id_2"><?php echo $data_item[0]->BookingCondition[1]->bookingConditioncol_detailLaos;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">การยกเลิกการเดินทาง
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด การยกเลิกการเดินทาง" rows="4" id="bookingConditioncol_detailLaos_id_3"><?php echo $data_item[0]->BookingCondition[2]->bookingConditioncol_detailLaos;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">การเลื่อนการเดินทาง
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด การเลื่อนการเดินทาง" rows="4" id="bookingConditioncol_detailLaos_id_4"><?php echo $data_item[0]->BookingCondition[3]->bookingConditioncol_detailLaos;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">สิ่งที่ควรนำไปด้วย
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด สิ่งที่ควรนำไปด้วย" rows="4" id="bookingConditioncol_detailLaos_id_5"><?php echo $data_item[0]->BookingCondition[4]->bookingConditioncol_detailLaos;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">เงื่อนไขการเดินทาง
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด เงื่อนไขการเดินทาง" rows="4" id="bookingConditioncol_detailLaos_id_6"><?php echo $data_item[0]->BookingCondition[5]->bookingConditioncol_detailLaos;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">การจองและสำรองที่นั่ง
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด การจองและสำรองที่นั่ง" rows="4" id="bookingConditioncol_detailLaos_id_7"><?php echo $data_item[0]->BookingCondition[6]->bookingConditioncol_detailLaos;?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <!-- tab-4 Chinese-->
                                <div role="tabpanel" id="tab-4" class="tab-pane">
                                    <div class="">
                                        <div class="row">
                                            <div class="col-lg-12"> <!-- รายละเอียดแพคเกจทัวร์ Chinese-->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดแพคเกจทัวร์</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ชื่อแพคเกจทัวร์</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" placeholder="ชื่อแพคเกจทัวร์" class="form-control" id="itmes_topicChinese" value ="<?php echo $data_item[0]->itmes_topicChinese;?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- TravelDetails Chinese-->
                                            <div class="col-lg-12"><!-- รายละเอียดการเดินทาง Chinese-->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดการเดินทาง</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="col-lg-12" id="create_field_Chinese_TravelDetails"><!-- TravelDetails Chinese-->
                                                            <div class="" id="box_Chinese_TravelDetails">
                                                                <!-- javascript  -->
                                                                
                                                                <!-- end javascript  -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="col-lg-12"><!-- รายละเอียดเงื่อนไขในการจอง Chinese-->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดเงื่อนไขในการจอง</h5>
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">อัตราค่าบริการนี้รวม
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด อัตราค่าบริการ" rows="4" id="bookingConditioncol_detailChinese_id_1"><?php echo $data_item[0]->BookingCondition[0]->bookingConditioncol_detailChinese;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">อัตราค่าบริการนี้ไม่รวม
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด อัตราค่าบริการ" rows="4" id="bookingConditioncol_detailChinese_id_2"><?php echo $data_item[0]->BookingCondition[1]->bookingConditioncol_detailChinese;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">การยกเลิกการเดินทาง
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด การยกเลิกการเดินทาง" rows="4" id="bookingConditioncol_detailChinese_id_3"><?php echo $data_item[0]->BookingCondition[2]->bookingConditioncol_detailChinese;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">การเลื่อนการเดินทาง
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด การเลื่อนการเดินทาง" rows="4" id="bookingConditioncol_detailChinese_id_4"><?php echo $data_item[0]->BookingCondition[3]->bookingConditioncol_detailChinese;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">สิ่งที่ควรนำไปด้วย
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด สิ่งที่ควรนำไปด้วย" rows="4" id="bookingConditioncol_detailChinese_id_5"><?php echo $data_item[0]->BookingCondition[4]->bookingConditioncol_detailChinese;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">เงื่อนไขการเดินทาง
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด เงื่อนไขการเดินทาง" rows="4" id="bookingConditioncol_detailChinese_id_6"><?php echo $data_item[0]->BookingCondition[5]->bookingConditioncol_detailChinese;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">การจองและสำรองที่นั่ง
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด การจองและสำรองที่นั่ง" rows="4" id="bookingConditioncol_detailChinese_id_7"><?php echo $data_item[0]->BookingCondition[6]->bookingConditioncol_detailChinese;?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <div class="col">
                                        <span class="note notifi" id="notifi_error_save"></span>
                                    </div>
                                    <button class="btn btn-primary btn-xl" id="save">บันทึก</button>
                                    
                                </div>
                                <br>
                            </div>
                        </div> 
                    </div> 
                </div> 
            </div> 

        </div>  
    </div>
    
    <?php $this->load->view('Html/include/Footer_js.php');?>
    <script>
        var host = "<?php echo $host;?>html";
        var host_entrepreneur = "<?php echo $host;?>entrepreneur";
        var items_id = <?php echo $data_item[0]->items_id;?>;
        var table;
        var map;
        var cropper;
        var gmarkers = [];
        var CropData,CropBoxData,CanvasData;
        var MenuName = "PackageTours";
        var TypeMenu = "Reservations";
        var UseGoogleMap = '<?php echo $ENVIRONMENT?>';

        var box_travel_schedule = 0;
        var box_travel_details = 0;
        var box_cover_image_package_tours = 0;
        var count_box_cover_image_package_tours = <?php echo count($data_item[0]->CoverItems);?>;

        var subcategory_id = <?php echo $data_item[0]->subcategory_id;?>;

        var TravelDetails = JSON.parse("<?php echo escapeJsonString(json_encode($data_item[0]->TravelDetails)) ?>");
        var TravelPeriod = JSON.parse("<?php echo escapeJsonString(json_encode($data_item[0]->TravelPeriod)) ?>");
        var CoverItems = JSON.parse("<?php echo escapeJsonString(json_encode($data_item[0]->CoverItems)) ?>");

        var string_province_boundary ='<?php 
            $arr = array();
            for($i=0;$i<count($data_item[0]->ScopeArea);$i++)
                $arr[] = $data_item[0]->ScopeArea[$i]->Provinces_provinces_id;
            
            if(count($arr)>0)
                echo join(",",$arr);
            else
                echo '0'
        ?>'
        var province_boundary =[];
        if(string_province_boundary != '0')       
        {
            province_boundary = string_province_boundary.split(',');
            province_boundary.map(v => parseInt(v, 10))
        }
    </script>
    
    <script src="<?php echo $host;?>assets/js/custom_js/GoogleMaps.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/GlobalFunction.js"></script>
    <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/GlobalFunction.js"></script>
    <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/BusinessForm/PackageToursEditView.js"></script>
    

</body>

</html>
<?php
function escapeJsonString($value) { # list from www.json.org: (\b backspace, \f formfeed)
    $escapers = array("\\", "/", "\"", "\n", "\r", "\t", "\x08", "\x0c");
    $replacements = array("\\\\", "\\/", "\\\"", "\\n", "\\r", "\\t", "\\f", "\\b");
    $result = str_replace($escapers, $replacements, $value);
    return $result;
}
?>