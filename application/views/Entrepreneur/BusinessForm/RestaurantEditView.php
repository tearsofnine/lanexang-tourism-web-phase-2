<!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>จัดการระบบ สำหรับผู้ประกอบการ เส้นทางท่องเที่ยว สี่เหลี่ยมวัฒนธรรมล้านช้าง</title>

        <?php $this->load->view('Html/include/Header_css.php');?>

    </head>

    <body>
       <!-- Preloader -->
       <div class="preloader" id="preloader">
            <div class="status" id="status"></div>
        </div>
        <div id="wrapper">
            <!-- SideMenu -->
            <?php $this->load->view('Entrepreneur/include/SideMenu.php');?>
            <!-- End of SideMenu -->

            <div id="page-wrapper" class="gray-bg">
                <div class="row border-bottom">
                    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                        <div class="navbar-header">
                           
                        </div>
                        <ul class="nav navbar-top-links navbar-center">
                            <li>
                                <h3 class="m-r-sm text-muted">แก้ข้อมูลกิจการร้านอาหาร</h3>
                            </li>
                        </ul>
                        <ul class="nav navbar-top-links navbar-right">
                            <li>
                                <h3 class="m-r-sm text-muted"></h3>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="wrapper wrapper-content animated fadeInRight">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="tabs-container">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li>
                                        <a class="nav-link active" data-toggle="tab" href="#tab-1" id="tabTH"> <?=$this->lang->line("lang_th");?></a>
                                    </li>
                                    <li>
                                        <a class="nav-link" data-toggle="tab" href="#tab-2" id="tabENG"><?=$this->lang->line("lang_en");?></a>
                                    </li>
                                    <li>
                                        <a class="nav-link" data-toggle="tab" href="#tab-3" id="tabLAOS"><?=$this->lang->line("lang_lao");?></a>
                                    </li>
                                    <li>
                                        <a class="nav-link" data-toggle="tab" href="#tab-4" id="tabCHN"><?=$this->lang->line("lang_chn");?></a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div role="tabpanel" id="tab-1" class="tab-pane active">
                                        <div class="">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5><?=$this->lang->line("cover_photo");?></h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <p>
                                                                เพิ่มภาพหน้าปกร้านอาหารแนะนำ
                                                            </p>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="image-crop" >
                                                                        <img class="image-crop-img" src="<?php echo $host;?>assets/img/p3.jpg" id="image_crop_img" style="width: 590px; height: 314px;">
                                                                        
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <h4><?=$this->lang->line("preview_pictures");?></h4>
                                                                    <div class="form-group  row">
                                                                        <div class="img-preview img-preview-sm"></div>
                                                                        <div class="" style="margin-left: 5px;">
                                                                            <button type="button" class="btn btn-primary btn-reset-crop" id="btn_reset_crop" data-method="reset" title="Reset">
                                                                                <span class="fa fa-refresh"></span>
                                                                                </span>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                    <h4><?=$this->lang->line("cover_photo");?></h4>
                                                                    <p>
                                                                        <?=$this->lang->line("adjustments_on_pictures");?>
                                                                    </p>
                                                                    <div class="btn-group">
                                                                        <label title="Upload image file" for="inputImage" class="btn btn-primary">
                                                                            <input type="file" accept="image/jpg,image/png,image/jpeg" name="file" id="inputImage" class="hide">
                                                                            <input type="hidden" name="temp_inputImage" id="temp_inputImage"> 
                                                                        </label>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>วีดีโอ (ถ้ามี)</h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">ลิ้งวีดีโอ youtube</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" placeholder="www.youtube" class="form-control" id="coverItem_url" value ="<?php echo $data_item[0]->coverItem_url;?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>ข้อมูลสถานที่</h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">ชื่อสถานที่</label>
                                                                <div class="col-sm-10">
                                                                    <input type="" placeholder="ชื่อสถานที่" class="form-control" id="itmes_topicThai" value="<?php echo $this->session->userdata("business_nameThai");?>" disabled>
                                                                    <span class="note notifi" id="notifi_itmes_topicThai"></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label"><?=$this->lang->line("name_location_type");?></label>
                                                                <div class="col-sm-10">
                                                                    <select data-placeholder="Choose a Country..." class="chosen-select namesubcategory" tabindex="1" id="namesubcategory">
                                                                    </select>
                                                                    <span class="note notifi" id="notifi_namesubcategory"></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label"></label>
                                                                <div class="col-sm-10">
                                                                    <textarea rows="4" class="form-control form-control-input flat-input contact" placeholder="รายละเอียดสถานที่ติดต่อ เช่น บ้านเลขที่" id="items_contactThai" value="<?php echo $this->session->userdata("business_presentAddress");?>"><?php echo $this->session->userdata("business_presentAddress");?></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12"><!-- ข้อมูลการติดต่อ -->
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>ข้อมูลการติดต่อ</h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <h5>
                                                                รายละเอียดการเปิด-ปิด
                                                            </h5>
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">เลือกวันเปิด-ปิด</label>
                                                                <div class="col-sm-10">
                                                                    <select data-placeholder="เลือกวันเปิด-ปิด" class="chosen-select-dayofweek" tabindex="1" multiple="multiple" id="dayofweek">
                                                                    </select>
                                                                    <span class="note notifi" id="notifi_dayofweek"></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">เวลาเปิด</label>
                                                            <div class="col-sm-10">
                                                                <input class="form-control timepicker" type="text" id="items_timeOpen" value ="<?php echo $data_item[0]->items_timeOpen;?>">
                                                                <span class="note notifi" id="notifi_items_timeOpen"></span>
                                                            </div>
                                                            </div>
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">เวลาปิด</label>
                                                                <div class="col-sm-10">
                                                                    <input class="form-control timepicker" type="text" id="items_timeClose" value ="<?php echo $data_item[0]->items_timeClose;?>">
                                                                    <span class="note notifi" id="notifi_items_timeClose"></span>
                                                                </div>
                                                            </div>
                                                            <!-- <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">เบอร์โทรติดต่อ</label>
                                                                <div class="col-sm-10">
                                                                        <input type="text" class="form-control telephone" placeholder="" id="items_phone">
                                                                </div>
                                                            </div> -->
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                
                                                <div class="col-lg-12">
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>รายละเอียดสถานที่</h5>
                                                            <div class="ibox-tools">
                                                                <a class="collapse-link">
                                                                    <i class="fa fa-chevron-up"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">สิ่งที่จะได้พบ</label>
                                                                <div class="col-sm-10">
                                                                    <textarea class="form-control" placeholder="รายละเอียด คำบรรยายสรุปสถานที่" rows="5" id="topicdetail_id_1_Thai"><?php echo $data_item[0]->Detail[0]->detail_textThai;?></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="form-group  row">
                                                                <div class="ibox-content ">
                                                                    <label class=" col-form-label">เลือกรูปภาพประกอบสถานที่ สามารถเพิ่มได้หลายรูป</label>
                                                                    <div class="row">
                                                                        <div class="col-lg-12" id="create_image_field_thingstomeet">
                                                                            <div class="" id="image_box_thingstomeet">
                                                                            <!-- javascript  -->
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="">
                                                                        <button class="btn btn-outline btn-primary add_image_field" name="thingstomeet">เลือกรูปภาพ</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>เพิ่มเนื้อหาและรูปภาพ</h5>
                                                            <div class="ibox-tools">
                                                                <a class="collapse-link">
                                                                    <i class="fa fa-chevron-up"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">เนื้อหา</label>
                                                                <div class="col-sm-10">
                                                                    <textarea class="form-control" placeholder="รายละเอียด คำบรรยายสรุปเนื้อหา" rows="5" id="topicdetail_id_2_Thai"><?php echo $data_item[0]->Detail[1]->detail_textThai;?></textarea>
                                                                </div>
                                                            </div>
                                                            <h5>สร้างเรื่องราวให้กับสถานที่นี้</h5>

                                                            <div class="" id="create_image_field_info">
                                                                <div class="" id="image_box_info">
                                                                    <!-- javascript  -->
                                                                </div>
                                                            </div>
                                                            
                                                            <div class="">
                                                                <button class="btn btn-outline btn-primary add_image_field" name="info">เพิ่มรูปภาพพร้อมคำบรรยาย</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>รายละเอียดการเดินทาง</h5>
                                                            <div class="ibox-tools">
                                                                <a class="collapse-link">
                                                                    <i class="fa fa-chevron-up"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">รายละเอียดการเดินทาง</label>
                                                                <div class="col-sm-10">
                                                                    <textarea class="form-control" placeholder="รายละเอียดการเดินทาง" rows="5" id="topicdetail_id_3_Thai"><?php echo $data_item[0]->Detail[2]->detail_textThai;?></textarea>
                                                                </div>
                                                            </div>
                                                            <h5>สร้างเรื่องราวในการเดินทางมาสถานที่นี้</h5>
                                                                
                                                            <div class="" id="create_image_field_navigate">
                                                                <div class="" id="image_box_navigate">
                                                                    <!-- javascript  -->
                                                                </div>
                                                            </div>

                                                            <div class="">
                                                                <button  class="btn btn-outline btn-primary add_image_field"  name="navigate">เพิ่มรูปภาพพร้อมคำบรรยาย</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>รายละเอียดเมนูเด็ดของทางร้าน</h5>
                                                            <div class="ibox-tools">
                                                                <a class="collapse-link">
                                                                    <i class="fa fa-chevron-up"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <div class="ibox-content ">
                                                                    <label class=" col-form-label">เพิ่มเนื้อหาและรูปภาพ สามารถเพิ่มกี่เมนูก็ได้</label>
                                                                    <div class="row">
                                                                        <div class="col-lg-12" id="create_image_field_foodmenu">
                                                                            <div class="" id="image_box_foodmenu">
                                                                            <!-- javascript  -->

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="">
                                                                        <button class="btn btn-outline btn-primary add_image_field" name="foodmenu">เพิ่มเมนูอาหาร</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="ibox">
                                                        <div class="ibox-title  back-change">
                                                            <h5>รับประกันความอร่อยโดย (ถ้ามี)</h5>
                                                            <div class="ibox-tools">
                                                                <a class="collapse-link">
                                                                    <i class="fa fa-chevron-up"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <h5>เพิ่มโลโก้ที่ได้รับประกันความอร่อยของร้านอาหารแห่งนี้</h5>                          
                                                            <div class="form-group  row">
                                                                <div class="col-sm-10">
                                                                    <select data-placeholder="เลือกโลโก้ที่ได้รับประกันความอร่อย" class="chosen-select-delicious" tabindex="1" multiple id="delicious">
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-12" id="create_image_field_delicious">
                                                                    <div class="" id="image_box_delicious">
                                                                        <!-- javascript  -->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        
                                                            
                                                        </div>
                                                    </div>
                                                </div>

                                               
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div role="tabpanel" id="tab-2" class="tab-pane">
                                        <div class="">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>ข้อมูลสถานที่</h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">ชื่อสถานที่</label>
                                                                <div class="col-sm-10">
                                                                    <input type="" placeholder="ชื่อสถานที่" class="form-control" id="itmes_topicEnglish" value="<?php echo $this->session->userdata("business_nameEnglish");?>" disabled>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label"></label>
                                                                <div class="col-sm-10">
                                                                    <textarea rows="4" class="form-control form-control-input flat-input contact" placeholder="รายละเอียดสถานที่ติดต่อ เช่น บ้านเลขที่" id="items_contactEnglish"><?php echo $data_item[0]->items_contactEnglish;?></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>รายละเอียดสถานที่</h5>
                                                            <div class="ibox-tools">
                                                                <a class="collapse-link">
                                                                    <i class="fa fa-chevron-up"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">สิ่งที่จะได้พบ</label>
                                                                <div class="col-sm-10">
                                                                    <textarea class="form-control" placeholder="รายละเอียด คำบรรยายสรุปสถานที่" rows="5" id="topicdetail_id_1_English"><?php echo $data_item[0]->Detail[0]->detail_textEnglish;?></textarea>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>เพิ่มเนื้อหาและรูปภาพ</h5>
                                                        
                                                            <div class="ibox-tools">
                                                                <a class="collapse-link">
                                                                    <i class="fa fa-chevron-up"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">เนื้อหา</label>
                                                                <div class="col-sm-10">
                                                                    <textarea class="form-control" placeholder="รายละเอียด คำบรรยายสรุปเนื้อหา" rows="5" id="topicdetail_id_2_English"><?php echo $data_item[0]->Detail[1]->detail_textEnglish;?></textarea>
                                                                </div>
                                                            </div>
                                                            <h5>สร้างเรื่องราวให้กับสถานที่นี้</h5>

                                                            <div class="" id="create_image_Eng_field_info">
                                                                <div class="" id="image_box_Eng_info">
                                                                    <!-- javascript  -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>รายละเอียดการเดินทาง</h5>
                                                            <div class="ibox-tools">
                                                                <a class="collapse-link">
                                                                    <i class="fa fa-chevron-up"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">รายละเอียดการเดินทาง</label>
                                                                <div class="col-sm-10">
                                                                    <textarea class="form-control" placeholder="รายละเอียดการเดินทาง" rows="5" id="topicdetail_id_3_English"><?php echo $data_item[0]->Detail[2]->detail_textEnglish;?></textarea>
                                                                </div>
                                                            </div>
                                                            <h5>สร้างเรื่องราวในการเดินทางมาสถานที่นี้</h5>
                                                            
                                                            <div class="" id="create_image_Eng_field_navigate">
                                                                <div class="" id="image_box_Eng_navigate">
                                                                    <!-- javascript  -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>รายละเอียดเมนูเด็ดของทางร้าน</h5>
                                                            <div class="ibox-tools">
                                                                <a class="collapse-link">
                                                                    <i class="fa fa-chevron-up"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <div class="ibox-content ">
                                                                    <label class=" col-form-label">เพิ่มเนื้อหาและรูปภาพ สามารถเพิ่มกี่เมนูก็ได้</label>
                                                                    <div class="row">
                                                                        <div class="col-lg-12" id="create_image_Eng_field_foodmenu">
                                                                            <div class="" id="image_box_Eng_foodmenu">
                                                                            <!-- javascript  -->
                                                                            
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" id="tab-3" class="tab-pane">
                                        <div class="">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>ข้อมูลสถานที่</h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">ชื่อสถานที่</label>
                                                                <div class="col-sm-10">
                                                                    <input type="" placeholder="ชื่อสถานที่" class="form-control" id="itmes_topicLaos" value="<?php echo $this->session->userdata("business_nameLaos");?>" disabled>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label"></label>
                                                                <div class="col-sm-10">
                                                                    <textarea rows="4" class="form-control form-control-input flat-input contact" placeholder="รายละเอียดสถานที่ติดต่อ เช่น บ้านเลขที่" id="items_contactLaos"><?php echo $data_item[0]->items_contactLaos;?></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>รายละเอียดสถานที่</h5>
                                                            <div class="ibox-tools">
                                                                <a class="collapse-link">
                                                                    <i class="fa fa-chevron-up"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">สิ่งที่จะได้พบ</label>
                                                                <div class="col-sm-10">
                                                                    <textarea class="form-control" placeholder="รายละเอียด คำบรรยายสรุปสถานที่" rows="5" id="topicdetail_id_1_Laos"><?php echo $data_item[0]->Detail[0]->detail_textLaos;?></textarea>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>เพิ่มเนื้อหาและรูปภาพ</h5>
                                                            
                                                            <div class="ibox-tools">
                                                                <a class="collapse-link">
                                                                    <i class="fa fa-chevron-up"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">เนื้อหา</label>
                                                                <div class="col-sm-10">
                                                                    <textarea class="form-control" placeholder="รายละเอียด คำบรรยายสรุปเนื้อหา" rows="5" id="topicdetail_id_2_Laos"><?php echo $data_item[0]->Detail[1]->detail_textLaos;?></textarea>
                                                                </div>
                                                            </div>
                                                            <h5>สร้างเรื่องราวให้กับสถานที่นี้</h5>

                                                            <div class="" id="create_image_Laos_field_info">
                                                                <div class="" id="image_box_Laos_info">
                                                                    <!-- javascript  -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>รายละเอียดการเดินทาง</h5>
                                                            <div class="ibox-tools">
                                                                <a class="collapse-link">
                                                                    <i class="fa fa-chevron-up"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">รายละเอียดการเดินทาง</label>
                                                                <div class="col-sm-10">
                                                                    <textarea class="form-control" placeholder="รายละเอียดการเดินทาง" rows="5" id="topicdetail_id_3_Laos"><?php echo $data_item[0]->Detail[2]->detail_textLaos;?></textarea>
                                                                </div>
                                                            </div>
                                                            <h5>สร้างเรื่องราวในการเดินทางมาสถานที่นี้</h5>
                                                            
                                                            <div class="" id="create_image_Laos_field_navigate">
                                                                <div class="" id="image_box_Laos_navigate">
                                                                    <!-- javascript  -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>รายละเอียดเมนูเด็ดของทางร้าน</h5>
                                                            <div class="ibox-tools">
                                                                <a class="collapse-link">
                                                                    <i class="fa fa-chevron-up"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <div class="ibox-content ">
                                                                    <label class=" col-form-label">เพิ่มเนื้อหาและรูปภาพ สามารถเพิ่มกี่เมนูก็ได้</label>
                                                                    <div class="row">
                                                                        <div class="col-lg-12" id="create_image_Laos_field_foodmenu">
                                                                            <div class="" id="image_box_Laos_foodmenu">
                                                                            <!-- javascript  -->
                                                                            
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" id="tab-4" class="tab-pane">
                                        <div class="">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>ข้อมูลสถานที่</h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">ชื่อสถานที่</label>
                                                                <div class="col-sm-10">
                                                                    <input type="" placeholder="ชื่อสถานที่" class="form-control" id="itmes_topicChinese" value="<?php echo $this->session->userdata("business_nameChinese");?>" disabled>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label"></label>
                                                                <div class="col-sm-10">
                                                                    <textarea rows="4" class="form-control form-control-input flat-input contact" placeholder="รายละเอียดสถานที่ติดต่อ เช่น บ้านเลขที่" id="items_contactChinese"><?php echo $data_item[0]->items_contactChinese;?></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>รายละเอียดสถานที่</h5>
                                                            <div class="ibox-tools">
                                                                <a class="collapse-link">
                                                                    <i class="fa fa-chevron-up"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">สิ่งที่จะได้พบ</label>
                                                                <div class="col-sm-10">
                                                                    <textarea class="form-control" placeholder="รายละเอียด คำบรรยายสรุปสถานที่" rows="5" id="topicdetail_id_1_Chinese"><?php echo $data_item[0]->Detail[0]->detail_textChinese;?></textarea>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>เพิ่มเนื้อหาและรูปภาพ</h5>
                                                            
                                                            <div class="ibox-tools">
                                                                <a class="collapse-link">
                                                                    <i class="fa fa-chevron-up"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">เนื้อหา</label>
                                                                <div class="col-sm-10">
                                                                    <textarea class="form-control" placeholder="รายละเอียด คำบรรยายสรุปเนื้อหา" rows="5" id="topicdetail_id_2_Chinese"><?php echo $data_item[0]->Detail[1]->detail_textChinese;?></textarea>
                                                                </div>
                                                            </div>
                                                            <h5>สร้างเรื่องราวให้กับสถานที่นี้</h5>

                                                            <div class="" id="create_image_Chinese_field_info">
                                                                <div class="" id="image_box_Chinese_info">
                                                                    <!-- javascript  -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>รายละเอียดการเดินทาง</h5>
                                                            <div class="ibox-tools">
                                                                <a class="collapse-link">
                                                                    <i class="fa fa-chevron-up"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">รายละเอียดการเดินทาง</label>
                                                                <div class="col-sm-10">
                                                                    <textarea class="form-control" placeholder="รายละเอียดการเดินทาง" rows="5" id="topicdetail_id_3_Chinese"><?php echo $data_item[0]->Detail[2]->detail_textChinese;?></textarea>
                                                                </div>
                                                            </div>
                                                            <h5>สร้างเรื่องราวในการเดินทางมาสถานที่นี้</h5>
                                                            
                                                            <div class="" id="create_image_Chinese_field_navigate">
                                                                <div class="" id="image_box_Chinese_navigate">
                                                                    <!-- javascript  -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>รายละเอียดเมนูเด็ดของทางร้าน</h5>
                                                            <div class="ibox-tools">
                                                                <a class="collapse-link">
                                                                    <i class="fa fa-chevron-up"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <div class="ibox-content ">
                                                                    <label class=" col-form-label">เพิ่มเนื้อหาและรูปภาพ สามารถเพิ่มกี่เมนูก็ได้</label>
                                                                    <div class="row">
                                                                        <div class="col-lg-12" id="create_image_Chinese_field_foodmenu">
                                                                            <div class="" id="image_box_Chinese_foodmenu">
                                                                            <!-- javascript  -->
                                                                            
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-right">
                                        <div class="col">
                                            <span class="note notifi" id="notifi_error_save"></span>
                                        </div>
                                        <button class="btn btn-primary btn-xl" id="save">บันทึก</button>
                                        
                                    </div>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    
    </body>
    <!-- Footer -->
    <?php $this->load->view('Html/include/Footer_js.php');?>
    <!-- End of Footer -->
    <script>
        var host = "<?php echo $host;?>html";
        var host_entrepreneur = "<?php echo $host;?>entrepreneur";

        var items_id = <?php echo $data_item[0]->items_id;?>;
        var table;
        var crop_img = false;
        var image_info = <?php echo count($data_item[0]->Detail[1]->Photo);?>;
        var image_navigate = <?php echo count($data_item[0]->Detail[2]->Photo);?>;
        var image_thingstomeet = <?php echo count($data_item[0]->Detail[0]->Photo);?>;
        var image_foodmenu = <?php echo count($data_item[0]->Product);?>;
        var data_image_foodmenu = JSON.parse( "<?php echo escapeJsonString(json_encode($data_item[0]->Product));?>");
        var string_array_image_foodmenu ='<?php
            $arr_image_foodmenu = array();
            for($i=0;$i<count($data_item[0]->Product);$i++)
            {
                $arr_image_foodmenu[] = count($data_item[0]->Product[$i]->PhotoProduct);
            }
            if(count($arr_image_foodmenu)>0)
                echo join(",",$arr_image_foodmenu);
            else
                echo '0'
            ?>'
        var array_image_foodmenu = [] ;
        if(string_array_image_foodmenu != '0')       
        {
            array_image_foodmenu = string_array_image_foodmenu.split(',');
            array_image_foodmenu = array_image_foodmenu.map(v => parseInt(v, 10));
        }
        
        var string_array_delicious ='<?php 
            $arr = array();
            for($i=0;$i<count($data_item[0]->Delicious);$i++)
                $arr[] = $data_item[0]->Delicious[$i]->DeliciousGuarantee_deliciousGuarantee_id;
            
            if(count($arr)>0)
                echo join(",",$arr);
            else
                echo '0'
        ?>'
        var array_delicious =[];
        if(string_array_delicious != '0')       
        {
            array_delicious = string_array_delicious.split(',');
            array_delicious.map(v => parseInt(v, 10))
        }

       
        var MenuName = "Restaurant";
        var TypeMenu = "Items";
        var UseGoogleMap = '<?php echo $ENVIRONMENT?>';

        var map;
        var cropper;
        var gmarkers = [];
        var CropData = JSON.parse( '<?php echo str_replace("\\t", "",$data_item[0]->tempImageCover_CropData);?>');
        var CropBoxData = JSON.parse('<?php echo str_replace("\\t", "",$data_item[0]->tempImageCover_CropBoxData);?>');
        var CanvasData = JSON.parse('<?php echo str_replace("\\t", "",$data_item[0]->tempImageCover_CanvasData);?>');
        var dayOCVal = '<?php echo $data_item[0]->item_dayOpen.($data_item[0]->item_dayOpen == $data_item[0]->item_dayClose?'':','.$data_item[0]->item_dayClose);?>';
        var str_array = dayOCVal.split(',');
        var subcategory_id = <?php echo $data_item[0]->subcategory_id;?>;


        var PhotoThingstomeet = JSON.parse("<?php echo escapeJsonString(json_encode($data_item[0]->Detail[0]->Photo))?>");
        var PhotoInfo = JSON.parse("<?php echo escapeJsonString(json_encode($data_item[0]->Detail[1]->Photo))?>");
        var PhotoNavigate = JSON.parse("<?php echo escapeJsonString(json_encode($data_item[0]->Detail[2]->Photo))?>");

        var itmes_topicThai ="<?php echo $this->session->userdata("business_nameThai");?>";
        var itmes_topicEnglish ="<?php echo $this->session->userdata("business_nameEnglish");?>";
        var itmes_topicChinese ="<?php echo $this->session->userdata("business_nameChinese");?>";
        var itmes_topicLaos ="<?php echo $this->session->userdata("business_nameLaos");?>";

        var items_latitude ="<?php echo $this->session->userdata("business_latitude");?>";
        var items_longitude ="<?php echo $this->session->userdata("business_longitude");?>";

        var items_phone ="<?php echo $this->session->userdata("business_phone");?>";
        var items_email ="<?php echo $this->session->userdata("entrepreneur_user_email");?>";
        var items_line ="<?php echo $this->session->userdata("business_line");?>";
        var items_facebookPage ="<?php echo $this->session->userdata("business_facebook");?>";
        var items_www ="<?php echo $this->session->userdata("business_www");?>";

        var Subdistricts_subdistricts_id ="<?php echo $this->session->userdata("Subdistricts_subdistricts_id");?>"

        
    </script>
    <script src="<?php echo $host;?>assets/js/custom_js/ProductFunction.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/GoogleMaps.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/GlobalFunction.js"></script>
    <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/GlobalFunction.js"></script>
    <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/BusinessForm/RestaurantEditView.js"></script>
</html>

<?php
function escapeJsonString($value) { # list from www.json.org: (\b backspace, \f formfeed)
    $escapers = array("\\", "/", "\"", "\n", "\r", "\t", "\x08", "\x0c");
    $replacements = array("\\\\", "\\/", "\\\"", "\\n", "\\r", "\\t", "\\f", "\\b");
    $result = str_replace($escapers, $replacements, $value);
    return $result;
}
?>