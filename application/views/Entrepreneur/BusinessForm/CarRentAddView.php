<!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>จัดการระบบ สำหรับผู้ประกอบการ เส้นทางท่องเที่ยว สี่เหลี่ยมวัฒนธรรมล้านช้าง</title>

        <?php $this->load->view('Html/include/Header_css.php');?>

    </head>

    <body>
        <!-- Preloader -->
        <div class="preloader" id="preloader">
            <div class="status" id="status"></div>
        </div>
        <div id="wrapper">
            <!-- SideMenu -->
            <?php $this->load->view('Entrepreneur/include/SideMenu.php');?>
            <!-- End of SideMenu -->

            <div id="page-wrapper" class="gray-bg">
                <div class="row border-bottom">
                    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                        <div class="navbar-header">
                            
                        </div>
                        <ul class="nav navbar-top-links navbar-center">
                            <li>
                                <h3 class="m-r-sm text-muted">เพิ่มข้อมูลกิจการรถเช่า</h3>
                            </li>
                        </ul>
                        <ul class="nav navbar-top-links navbar-right">
                            <li>
                                <h3 class="m-r-sm text-muted"></h3>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="wrapper wrapper-content animated fadeInRight">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="tabs-container">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li>
                                        <a class="nav-link active" data-toggle="tab" href="#tab-1" id="tabTH"> <?=$this->lang->line("lang_th");?></a>
                                    </li>
                                    <li>
                                        <a class="nav-link" data-toggle="tab" href="#tab-2" id="tabENG"><?=$this->lang->line("lang_en");?></a>
                                    </li>
                                    <li>
                                        <a class="nav-link" data-toggle="tab" href="#tab-3" id="tabLAOS"><?=$this->lang->line("lang_lao");?></a>
                                    </li>
                                    <li>
                                        <a class="nav-link" data-toggle="tab" href="#tab-4" id="tabCHN"><?=$this->lang->line("lang_chn");?></a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <!--tab-1 Thai -->
                                    <div role="tabpanel" id="tab-1" class="tab-pane active">
                                        <div class="">
                                            <div class="row">
                                                <div class="col-lg-12"> <!-- รูปภาพหรือวีดีโอหน้าปก -->
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5><?=$this->lang->line("cover_photo");?></h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <p>
                                                                เพิ่มภาพหน้าปกสถานที่ท่องเที่ยว
                                                            </p>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="image-crop" >
                                                                        <img class="image-crop-img" src="<?php echo $host;?>assets/img/p3.jpg" id="image_crop_img" style="width: 590px; height: 314px;">
                                                                        
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <h4><?=$this->lang->line("preview_pictures");?></h4>
                                                                    <div class="form-group  row">
                                                                        <div class="img-preview img-preview-sm"></div>
                                                                        <div class="" style="margin-left: 5px;">
                                                                            <button type="button" class="btn btn-primary btn-reset-crop" id="btn_reset_crop" data-method="reset" title="Reset">
                                                                                <span class="fa fa-refresh"></span>
                                                                                </span>
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                    <h4><?=$this->lang->line("cover_photo");?></h4>
                                                                    <p>
                                                                        <?=$this->lang->line("adjustments_on_pictures");?>
                                                                    </p>
                                                                    <div class="btn-group">
                                                                        <label title="Upload image file" for="inputImage" class="btn btn-primary">
                                                                            <input type="file" accept="image/jpg,image/png,image/jpeg" name="file" id="inputImage" class="hide"> Upload new image
                                                                        </label>
                                                                    </div>
                                                                    <label style="color: red;">* รูปภาพควรมีอัตราส่วน 16:9, กว้าง x ยาว ไม่เกิน 1920 X 1080  pixels และขนาดไม่เกิน 2 MB.</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12"> <!-- รายละเอียดรถ Thai-->
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>รายละเอียดตั๋วกิจกรรม</h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <!-- <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">หมวดหมู่รถ</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" placeholder="รถยนต์" class="form-control" id="itmes_topicThai">
                                                                </div>
                                                            </div> -->
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">ชื่อรถ</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" placeholder="โปรดระบุเป็น ยีห้อ ปี รุ่น" class="form-control" id="itmes_topicThai">
                                                                    <span class="note notifi" id="notifi_itmes_topicThai"></span>
                                                                </div>
                                                            </div>   
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">ประเภท</label>
                                                                <div class="col-sm-10">
                                                                    <select data-placeholder="Choose a TypesofCars_typesofCars_id..." class="chosen-select" tabindex="1" id="TypesofCars_typesofCars_id">
                                                                    </select>
                                                                    <span class="note notifi" id="notifi_TypesofCars_typesofCars_id"></span>
                                                                </div>
                                                            </div>  
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">เกียร์</label>
                                                                <div class="col-sm-10">
                                                                    <select data-placeholder="Choose a GearSystem_gearSystem_id..." class="chosen-select" tabindex="1" id="GearSystem_gearSystem_id">
                                                                    </select>
                                                                    <span class="note notifi" id="notifi_GearSystem_gearSystem_id"></span>
                                                                </div>
                                                            </div>  
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">จำนวนที่นั่ง</label>
                                                                <div class="col-sm-10">
                                                                    <input type="number" placeholder="จำนวนที่นั่ง" class="form-control-sm form-control typenumberEventTicketPrice" min="0" max="10" id="items_carSeats">
                                                                    <span class="note notifi" id="notifi_items_carSeats"></span>
                                                                </div>
                                                            </div>  
                                                            
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-lg-12"> <!-- รายละเอียดราคา Thai-->
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>รายละเอียดราคา</h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">ราคาต่อวัน</label>
                                                                <div class="col-sm-10">
                                                                    <input type="number" placeholder="ราคาต่อวัน" class="form-control-sm form-control typenumberEventTicketPrice" min="0" max="10" id="items_PricePerDay">
                                                                    <span class="note notifi" id="notifi_items_PricePerDay"></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">ราคาค่าส่งรถ</label>
                                                                <div class="col-sm-10">
                                                                    <input type="number" placeholder="ราคาค่าส่งรถ" class="form-control-sm form-control typenumberEventTicketPrice" min="0" max="10" id="items_CarDeliveryPrice">
                                                                    <span class="note notifi" id="notifi_items_CarDeliveryPrice"></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">ราคาค่ารับรถ</label>
                                                                <div class="col-sm-10">
                                                                    <input type="number" placeholder="ราคาค่ารับรถ" class="form-control-sm form-control typenumberEventTicketPrice" min="0" max="10" id="items_PickUpPrice">
                                                                    <span class="note notifi" id="notifi_items_PickUpPrice"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12"> <!-- รายละเอียดค่ามัดจำเพื่อประกันความเสียหาย Thai-->
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>รายละเอียดค่ามัดจำเพื่อประกันความเสียหาย</h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">ค่ามัดจำ</label>
                                                                <div class="col-sm-10">
                                                                    <input type="number" placeholder="ค่ามัดจำ" class="form-control-sm form-control typenumberEventTicketPrice" min="0" max="10" id="items_DepositPrice">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">รวมประกันภัยพื้นฐาน</label>
                                                                <div class="col-sm-10">
                                                                    <div class="form-check-inline i-checks">
                                                                        <label class="form-check-label">
                                                                            <input type="radio" class="form-check-input " value="1" name="items_isBasicInsurance"><i></i> รวม
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check-inline i-checks">
                                                                        <label class="form-check-label">
                                                                            <input type="radio" class="form-check-input " value="0" name="items_isBasicInsurance" checked><i></i> ไม่รวม
                                                                        </label>
                                                                    </div>
                                                                
                                                                </div>
                                                            </div>
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">รายละเอียดประกันภัยพื้นฐาน</label>
                                                                <div class="col-sm-10">
                                                                    <textarea class="form-control" placeholder="รายละเอียด ประกันภัยพื้นฐาน" rows="6" id="items_BissicInsurance_detailThai"></textarea>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12"> <!-- รายละเอียดเงื่อนไขของน้ำมันรถ Thai-->
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>รายละเอียดเงื่อนไขของน้ำมันรถ</h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">รายละเอียด
                                                                </label>
                                                                <div class="col-sm-10">
                                                                    <textarea class="form-control" placeholder="รายละเอียด เงื่อนไขของน้ำมันรถ" rows="6" id="bookingConditioncol_detailThai_id_13"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12"> <!-- รายละเอียดเอกสารสำหรับการเช่ารถ (เพิ่มเป็นข้อ) Thai-->
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>รายละเอียดเอกสารสำหรับการเช่ารถ (เพิ่มเป็นข้อ)</h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="col-lg-12" id="create_field_documents">
                                                                <div class="" id="box_documents">
                                                                </div>
                                                                <div class="text-left" style="margin-top: 10px;">
                                                                    <button class="btn btn-primary btn-xs add_box_field_documents" name="documents">รายละเอียดเอกสาร</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--tab-2 English -->
                                    <div role="tabpanel" id="tab-2" class="tab-pane">
                                        <div class="">
                                            <div class="row">
                                                <div class="col-lg-12"> <!-- รายละเอียดรถ English-->
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>รายละเอียดตั๋วกิจกรรม</h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">ชื่อรถ</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" placeholder="โปรดระบุเป็น ยีห้อ ปี รุ่น" class="form-control" id="itmes_topicEnglish">
                                                                </div>
                                                            </div>   
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12"> <!-- รายละเอียดค่ามัดจำเพื่อประกันความเสียหาย English-->
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>รายละเอียดค่ามัดจำเพื่อประกันความเสียหาย</h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">รายละเอียดประกันภัยพื้นฐาน</label>
                                                                <div class="col-sm-10">
                                                                    <textarea class="form-control" placeholder="รายละเอียด ประกันภัยพื้นฐาน" rows="6" id="items_BissicInsurance_detailEnglish"></textarea>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12"> <!-- รายละเอียดเงื่อนไขของน้ำมันรถ English-->
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>รายละเอียดเงื่อนไขของน้ำมันรถ</h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">รายละเอียด
                                                                </label>
                                                                <div class="col-sm-10">
                                                                    <textarea class="form-control" placeholder="รายละเอียด เงื่อนไขของน้ำมันรถ" rows="6" id="bookingConditioncol_detailEnglish_id_13"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12"> <!-- รายละเอียดเอกสารสำหรับการเช่ารถ (เพิ่มเป็นข้อ) English-->
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>รายละเอียดเอกสารสำหรับการเช่ารถ (เพิ่มเป็นข้อ)</h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="col-lg-12" id="create_field_documents_English">
                                                                <div class="" id="box_documents_English">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--tab-3 Laos -->
                                    <div role="tabpanel" id="tab-3" class="tab-pane">
                                        <div class="">
                                            <div class="row">
                                                <div class="col-lg-12"> <!-- รายละเอียดรถ Laos-->
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>รายละเอียดตั๋วกิจกรรม</h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">ชื่อรถ</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" placeholder="โปรดระบุเป็น ยีห้อ ปี รุ่น" class="form-control" id="itmes_topicLaos">
                                                                </div>
                                                            </div>   
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12"> <!-- รายละเอียดค่ามัดจำเพื่อประกันความเสียหาย Laos-->
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>รายละเอียดค่ามัดจำเพื่อประกันความเสียหาย</h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">รายละเอียดประกันภัยพื้นฐาน</label>
                                                                <div class="col-sm-10">
                                                                    <textarea class="form-control" placeholder="รายละเอียด ประกันภัยพื้นฐาน" rows="6" id="items_BissicInsurance_detailLaos"></textarea>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12"> <!-- รายละเอียดเงื่อนไขของน้ำมันรถ Laos-->
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>รายละเอียดเงื่อนไขของน้ำมันรถ</h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">รายละเอียด
                                                                </label>
                                                                <div class="col-sm-10">
                                                                    <textarea class="form-control" placeholder="รายละเอียด เงื่อนไขของน้ำมันรถ" rows="6" id="bookingConditioncol_detailLaos_id_13"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12"> <!-- รายละเอียดเอกสารสำหรับการเช่ารถ (เพิ่มเป็นข้อ) Laos-->
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>รายละเอียดเอกสารสำหรับการเช่ารถ (เพิ่มเป็นข้อ)</h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="col-lg-12" id="create_field_documents_Laos">
                                                                <div class="" id="box_documents_Laos">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!--tab-4 Chinese -->
                                    <div role="tabpanel" id="tab-4" class="tab-pane">
                                        <div class="">
                                            <div class="row">
                                                <div class="col-lg-12"> <!-- รายละเอียดรถ Chinese-->
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>รายละเอียดตั๋วกิจกรรม</h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">ชื่อรถ</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" placeholder="โปรดระบุเป็น ยีห้อ ปี รุ่น" class="form-control" id="itmes_topicChinese">
                                                                </div>
                                                            </div>   
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12"> <!-- รายละเอียดค่ามัดจำเพื่อประกันความเสียหาย Chinese-->
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>รายละเอียดค่ามัดจำเพื่อประกันความเสียหาย</h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">รายละเอียดประกันภัยพื้นฐาน</label>
                                                                <div class="col-sm-10">
                                                                    <textarea class="form-control" placeholder="รายละเอียด ประกันภัยพื้นฐาน" rows="6" id="items_BissicInsurance_detailChinese"></textarea>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12"> <!-- รายละเอียดเงื่อนไขของน้ำมันรถ Chinese-->
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>รายละเอียดเงื่อนไขของน้ำมันรถ</h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="form-group  row">
                                                                <label class="col-sm-2 col-form-label">รายละเอียด
                                                                </label>
                                                                <div class="col-sm-10">
                                                                    <textarea class="form-control" placeholder="รายละเอียด เงื่อนไขของน้ำมันรถ" rows="6" id="bookingConditioncol_detailChinese_id_13"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12"> <!-- รายละเอียดเอกสารสำหรับการเช่ารถ (เพิ่มเป็นข้อ) Chinese-->
                                                    <div class="ibox ">
                                                        <div class="ibox-title  back-change">
                                                            <h5>รายละเอียดเอกสารสำหรับการเช่ารถ (เพิ่มเป็นข้อ)</h5>
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div class="col-lg-12" id="create_field_documents_Chinese">
                                                                <div class="" id="box_documents_Chinese">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-right">
                                        <div class="col">
                                            <span class="note notifi" id="notifi_error_save"></span>
                                        </div>
                                        <button class="btn btn-primary btn-xl" id="save">บันทึก</button>
                                        
                                    </div>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    
    </body>
    <!-- Footer -->
    <?php $this->load->view('Html/include/Footer_js.php');?>
    <!-- End of Footer -->
    <script>
        var host = "<?php echo $host;?>html";
        var host_entrepreneur = "<?php echo $host;?>entrepreneur";

        var map;
        var cropper;
        var gmarkers = [];
        var CropData,CropBoxData,CanvasData;
        var MenuName = "CarRent";
      
        var str_array;
        var UseGoogleMap = '<?php echo $ENVIRONMENT?>';
        var box_field_documents = 0;

        var items_phone ="<?php echo $this->session->userdata("business_phone");?>";
        var items_email ="<?php echo $this->session->userdata("entrepreneur_user_email");?>";
        var items_line ="<?php echo $this->session->userdata("business_line");?>";
        var items_facebookPage ="<?php echo $this->session->userdata("business_facebook");?>";
        var items_www ="<?php echo $this->session->userdata("business_www");?>";

        var items_latitude ="<?php echo $this->session->userdata("business_latitude");?>";
        var items_longitude ="<?php echo $this->session->userdata("business_longitude");?>";

        var Subdistricts_subdistricts_id ="<?php echo $this->session->userdata("Subdistricts_subdistricts_id");?>"

    </script>
    <script src="<?php echo $host;?>assets/js/custom_js/GoogleMaps.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/GlobalFunction.js"></script>
    <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/GlobalFunction.js"></script>
    <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/BusinessForm/CarRentAddView.js"></script>

</html>