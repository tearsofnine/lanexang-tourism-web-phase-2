<!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>จัดการระบบ สำหรับผู้ประกอบการ เส้นทางท่องเที่ยว สี่เหลี่ยมวัฒนธรรมล้านช้าง</title>

        <?php $this->load->view('Html/include/Header_css.php');?>

    </head>

    <body>
        <!-- Preloader -->
        <div class="preloader" id="preloader">
            <div class="status" id="status"></div>
        </div>
        <div id="wrapper">
            <!-- SideMenu -->
            <?php $this->load->view('Entrepreneur/include/SideMenu.php');?>
            <!-- End of SideMenu -->

            <div id="page-wrapper" class="gray-bg">
                <div class="row border-bottom">
                    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                        <div class="navbar-header">
                            
                        </div>
                        <ul class="nav navbar-top-links navbar-center">
                            <li>
                                <h3 class="m-r-sm text-muted">แก้ข้อมูลโปรแกรมท่องเที่ยว</h3>
                            </li>
                        </ul>
                        <ul class="nav navbar-top-links navbar-right">
                            <li>
                                <h3 class="m-r-sm text-muted"></h3>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="wrapper wrapper-content animated fadeInRight">
                    <div class="row">
                    
                        <div class="col-lg-12">
                            <div class="ibox ">
                                <div class="ibox-title  back-change">
                                    <h5><?=$this->lang->line("cover_photo");?></h5>
                                </div>
                                <div class="ibox-content">
                                    <p>
                                        เพิ่มภาพหน้าปกโปรแกรมท่องเที่ยว
                                    </p>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="image-crop" >
                                                <img class="image-crop-img" src="<?php echo $host;?>assets/img/uploadfile/<?php echo $data_program_tour[0]->tempImageCover_paths;?>" id="image_crop_img" style="width: 590px; height: 314px;">
                                                
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <h4><?=$this->lang->line("preview_pictures");?></h4>
                                            <div class="form-group  row">
                                                <div class="img-preview img-preview-sm"></div>
                                                <div class="" style="margin-left: 5px;">
                                                    <button type="button" class="btn btn-primary btn-reset-crop" id="btn_reset_crop" data-method="reset" title="Reset">
                                                        <span class="fa fa-refresh"></span>
                                                        </span>
                                                    </button>
                                                </div>
                                            </div>
                                            <h4><?=$this->lang->line("cover_photo");?></h4>
                                            <p>
                                                <?=$this->lang->line("adjustments_on_pictures");?>
                                            </p>
                                            <div class="btn-group">
                                                <label title="Upload image file" for="inputImage" class="btn btn-primary">
                                                    <input type="file" accept="image/jpg,image/png,image/jpeg" name="file" id="inputImage" class="hide"> Upload new image
                                                    <input type="hidden" name="temp_inputImage" id="temp_inputImage"> 
                                                </label>
                                            </div>
                                            <label style="color: red;">* รูปภาพควรมีอัตราส่วน 16:9, กว้าง x ยาว ไม่เกิน 1920 X 1080  pixels และขนาดไม่เกิน 2 MB.</label>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ชื่อโปรแกรมการท่องเที่ยว -->
                        <div class="col-lg-12">
                            <div class="ibox ">
                                <div class="ibox-content">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h4>
                                                ชื่อโปรแกรมการท่องเที่ยว
                                            </h4>
                                            <p>
                                                * ใส่ชื่อสถานมี่สำคัญและระยะเวลาของตัวโปรแกรทเพื่อบ่งบอก ถึงตัวโปรแกรม
                                            </p>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <input type="" placeholder="ชื่อโปรแกรมการท่องเที่ยว ภาษาไทย" class="form-control" id="programTour_nameThai" value ="<?php echo $data_program_tour[0]->programTour_nameThai;?>">
                                                    <span class="note notifi" id="notifi_programTour_nameThai"></span>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <input type="" placeholder="ชื่อโปรแกรมการท่องเที่ยว ภาษาอังกฤษ" class="form-control" id="programTour_nameEnglish" value ="<?php echo $data_program_tour[0]->programTour_nameEnglish;?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <input type="" placeholder="ชื่อโปรแกรมการท่องเที่ยว ภาษาลาว" class="form-control" id="programTour_nameLaos" value ="<?php echo $data_program_tour[0]->programTour_nameLaos;?>">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-12">
                                                    <input type="" placeholder="ชื่อโปรแกรมการท่องเที่ยว ภาษาจีน" class="form-control" id="programTour_nameChinese" value ="<?php echo $data_program_tour[0]->programTour_nameChinese;?>">
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="col-md-6">
                                            <h4>
                                                ตั้งค่าสถานะ
                                            </h4>
                                            <p>
                                                * เลือกช่วงเวลาในการแสดงโปรแกรมท่องเที่ยวนี้
                                            </p>
                                            <div class="form-group row">
                                                <div class="col-sm-10">
                                                    <div class="form-check-inline i-checks">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input set_time_period" value="1" name="set_time_period" id="set_time_period"><i></i> กำหนดช่วงเวลา
                                                            </label>
                                                    </div>
                                                    <div class="form-check-inline i-checks">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input set_time_period" value="0" name="set_time_period" id="un_set_time_period" checked><i></i> ไม่กำหนดช่วงเวลา
                                                        </label>
                                                    </div>
                                                
                                                </div>
                                            </div>
                                            <div class="form-group" id="data_5">
                                                <div class="input-daterange input-group" id="datepicker">
                                                    <input type="text" class="form-control-sm form-control time_period" name="start" id="start" value="" disabled/>
                                                    <span class="input-group-addon">ถึง</span>
                                                    <input type="text" class="form-control-sm form-control time_period" name="end" id="end" value="" disabled/>
                                                </div>
                                                
                                            </div>
                                            <div class="form-group  row">
                                            <div class="col-md-12">
                                                <h4>ประเภทโปรแกรมท่องเที่ยว</h4>
                                                <select  data-placeholder="Choose a Category..." class="chosen-select programtour" tabindex="-1" name="subcategory" id="subcategory">
                                                </select>
                                                <span class="note notifi" id="notifi_subcategory"></span>
                                            </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- เริ่มต้นบอกสิ่งที่น่าสนใจ -->
                        <div class="col-lg-12">
                            <div class="ibox ">
                                <div class="ibox-title  back-change">
                                    <h5>เริ่มต้นบอกสิ่งที่น่าสนใจ</h5>
                                    <!-- <div class="ibox-tools">
                                        <p>0/300</p>
                                    </div> -->
                                </div>
                                <div class="ibox-content">
                                    <div class="tabs-container">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li>
                                                <a class="nav-link active" data-toggle="tab" href="#tabMain1" id="AtabMainTH"> ภาษาไทย</a>
                                            </li>
                                            <li>
                                                <a class="nav-link" data-toggle="tab" href="#tabMain2" id="AtabMainENG">ภาษาอังกฤษ</a>
                                            </li>
                                            <li>
                                                <a class="nav-link" data-toggle="tab" href="#tabMain3" id="AtabMainLAOS">ภาษาลาว</a>
                                            </li>
                                            <li>
                                                <a class="nav-link" data-toggle="tab" href="#tabMain4" id="AtabMainCHN">ภาษาจีน</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div role="tabpanel" id="tabMain1" class="tab-pane active">
                                                <div class="">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="ibox ">
                                                                <div class="">
                                                                    <div class="form-group  row">
                                                                        <div class="col-lg-12">
                                                                            <textarea class="form-control" placeholder="เริ่มต้นบอกสิ่งที่น่าสนใจ ภาษาไทย" rows="4" id="programTour_interestingThingsThai"><?php echo $data_program_tour[0]->programTour_interestingThingsThai;?></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" id="tabMain2" class="tab-pane">
                                                <div class="">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="ibox ">
                                                                <div class="">
                                                                    <div class="form-group  row">
                                                                        <div class="col-lg-12">
                                                                            <textarea class="form-control" placeholder="เริ่มต้นบอกสิ่งที่น่าสนใจ ภาษาอังกฤษ" rows="4" id="programTour_interestingThingsEnglish"><?php echo $data_program_tour[0]->programTour_interestingThingsEnglish;?></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" id="tabMain3" class="tab-pane">
                                                <div class="">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="ibox ">
                                                                <div class="">
                                                                    <div class="form-group  row">
                                                                        <div class="col-lg-12">
                                                                            <textarea class="form-control" placeholder="เริ่มต้นบอกสิ่งที่น่าสนใจ ภาษาลาว"  rows="4" id="programTour_interestingThingsLaos"><?php echo $data_program_tour[0]->programTour_interestingThingsLaos;?></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" id="tabMain4" class="tab-pane">
                                                <div class="">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="ibox ">
                                                                <div class="">
                                                                    <div class="form-group  row">
                                                                        <div class="col-lg-12">
                                                                            <textarea class="form-control" placeholder="เริ่มต้นบอกสิ่งที่น่าสนใจ ภาษาจีน" rows="4" id="programTour_interestingThingsChinese"><?php echo $data_program_tour[0]->programTour_interestingThingsChinese;?></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="ibox ">
                                <div class="ibox-title  back-change">
                                    <h5>เริ่มโปรแกรมท่องเที่ยว</h5>
                                    
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <div class="" id="create_image_field_ProgramTour">
                                        <div class="" id="image_box_ProgramTour">
                                            <!-- javascript  -->
                                            
                                            <!-- end javascript  -->
                                        </div>
                                    </div>
                                    
                                    <div class="ibox-title  back-change">
                                        <button type="button" class="btn btn-outline btn-primary add_image_field" name="ProgramTour">เพิ่มวัน เริ่มกันเลย</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="ibox ">
                                <div class="text-right">
                                    <div class="col"><span class="note notifi" id="notifi_error_save"></span></div>
                                    <button class="btn btn-primary btn-xl" id="save">บันทึก</button>
                                    <br>
                                </div>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>

    
    </body>
    <!-- Footer -->
    <?php $this->load->view('Html/include/Footer_js.php');?>
    <!-- End of Footer -->
    <script>
        var host = "<?php echo $host;?>html";
        var host_entrepreneur = "<?php echo $host;?>entrepreneur";
        var table;
        var MenuName = "ProgramTour";
        var type_page = "entrepreneur";
        var UseGoogleMap = '<?php echo $ENVIRONMENT?>';
        var array_dates_ProgramTour = [];
        var programTour_id = <?php echo $data_program_tour[0]->programTour_id;?>;
        var SubCategory_subcategory_id = <?php echo $data_program_tour[0]->SubCategory_subcategory_id;?>;
        var cropper;
        var CropData = JSON.parse( '<?php echo str_replace("\\t", "",$data_program_tour[0]->tempImageCover_CropData);?>');
        var CropBoxData = JSON.parse('<?php echo str_replace("\\t", "",$data_program_tour[0]->tempImageCover_CropBoxData);?>');
        var CanvasData = JSON.parse('<?php echo str_replace("\\t", "",$data_program_tour[0]->tempImageCover_CanvasData);?>');
        var crop_img = false;

    </script>
    <script src="<?php echo $host;?>assets/js/custom_js/GoogleMaps.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/ProgramTourFunction.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/GlobalFunction.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/LocationManagement.js"></script>
    <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/GlobalFunction.js"></script>
    <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/BusinessForm/ProgramTourEditView.js"></script>
</html>