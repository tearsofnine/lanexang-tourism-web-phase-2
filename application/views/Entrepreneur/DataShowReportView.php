<!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>จัดการระบบ สำหรับผู้ประกอบการ เส้นทางท่องเที่ยว สี่เหลี่ยมวัฒนธรรมล้านช้าง</title>

        <?php $this->load->view('Entrepreneur/include/Header_css.php');?>
        <link href="<?php echo $host;?>assets/css/plugins/3DotContextMenu/context-menu.css" rel="stylesheet">
        <link href="<?php echo $host;?>assets/css/plugins/chartist/chartist.min.css" rel="stylesheet">
    </head>

    <body>

        <div id="wrapper">

            <!-- SideMenu -->
            <?php $this->load->view('Entrepreneur/include/SideMenu.php');?>
            <!-- End of SideMenu -->

            <div id="page-wrapper" class="gray-bg">
                <!-- Header -->
                <?php $this->load->view('Entrepreneur/include/Header.php');?>
                <!-- End of Header -->

                <div class="wrapper wrapper-content animated fadeInRight">
                    
                    <div class="row">
                        <div class="col-lg-3"></div>
                        <div class="col-lg-6">
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group  row">
                                            <div class="col-sm-12">
                                                <label class="">เดือนที่เริ่มต้น</label>
                                                <button type="button" class="btn btn-block btn-rounded btn-outline btn-default">July/2019</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group  row">
                                            <div class="col-sm-12">
                                                <label class="">เดือนที่สิ้นสุด</label>
                                                <button type="button" class="btn btn-block btn-rounded btn-outline btn-default">November/2019</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group  row">
                                            <div class="col-sm-12">
                                                <button type="button" class="btn btn-block btn-rounded btn-info">ค้นหา</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ibox-content-head">
                                <h2 class="text-muted font-bold ">
                                    ผลการค้นหา
                                </h2>
                            </div>
                            <div class="ibox ">
                                <div class="ibox-title">
                                    <h5 class="text-muted">จำนวนผู้เข้าใช้งาน
                                    </h5>
                                </div>
                                <div class="ibox-content">
                                    <div id="ct-chart1" class="ct-perfect-fourth"></div>
                                </div>
                            </div>
                            <!-- <div class="ibox-content">
                                <h3 class="text-muted text-normal">ภาพรวมคะแนนความพึงพอใจ
                                    <span class="badge badge-success">4
                                        <i class="fa fa-star"></i>
                                    </span>
                                </h3>
                                <div class="ibox">
                                    <h4 class="text-Darkgreen">คะแนนจากผู้ใช้งาน</h4>
                                    <div>
                                        <h3 class="text-milk text-normal float-right">2</h3>
                                        <h2 class="text-Darkgreen">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </h2>
                                        <div class="progress progress-small">
                                            <div style="" class="progress-bar progress-bar-Darkgreen"></div>
                                        </div>
                                    </div>
                                    <div>
                                        <h3 class="text-milk text-normal float-right">2</h3>
                                        <h2 class="text-Darkgreen">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star text-milk"></i>
                                        </h2>
                                        <div class="progress progress-small">
                                            <div style="width: 20%;" class="progress-bar progress-bar-Darkgreen"></div>
                                        </div>
                                    </div>
                                    <div>
                                        <h3 class="text-milk text-normal float-right">2</h3>
                                        <h2 class="text-Darkgreen">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star text-milk"></i>
                                            <i class="fa fa-star text-milk"></i>
                                        </h2>
                                        <div class="progress progress-small">
                                            <div style="" class="progress-bar progress-bar-Darkgreen"></div>
                                        </div>
                                    </div>
                                    <div>
                                        <h3 class="text-milk text-normal float-right">2</h3>
                                        <h2 class="text-Darkgreen">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star text-milk"></i>
                                            <i class="fa fa-star text-milk"></i>
                                            <i class="fa fa-star text-milk"></i>
                                        </h2>
                                        <div class="progress progress-small">
                                            <div style="" class="progress-bar progress-bar-Darkgreen"></div>
                                        </div>
                                    </div>
                                    <div>
                                        <h3 class="text-milk text-normal float-right">2</h3>
                                        <h2 class="text-Darkgreen">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star text-milk"></i>
                                            <i class="fa fa-star text-milk"></i>
                                            <i class="fa fa-star text-milk"></i>
                                            <i class="fa fa-star text-milk"></i>
                                        </h2>
                                        <div class="progress progress-small">
                                            <div style="" class="progress-bar progress-bar-Darkgreen"></div>
                                        </div>
                                    </div>
                                    <div>
                                        <h3 class="text-milk text-normal float-right">2</h3>
                                        <h2 class="text-milk">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star "></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </h2>
                                        <div class="progress progress-small">
                                            <div style="" class="progress-bar progress-bar-Darkgreen"></div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="chat-element">
                                    <a href="Chat.html" class="float-left">
                                        <img alt="image" class="rounded-circle" src="img/a6.jpg">
                                    </a>
                                    <div class="media-body ">
                                        <a href="Chat.html">
                                            <h3 class="text-muted font-bold">Maria Snow</h3>
                                        </a>
                                        <h4 class="text-Darkgreen">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star text-milk"></i>
                                        </h4>
                                        <p class="m-b-xs text-milk">
                                            20 Apr 2019 Yesterday at 8:42 AM
                                        </p>
                                        <h3 class="text-muted font-normal">น่าไป</h3>
                                    </div>
                                    <hr>
                                </div>
                            </div> -->

                            <div class="col-lg-3"></div>
                        </div>
                    </div>




                </div>
            </div>
        </div>
       

        <?php $this->load->view('Entrepreneur/include/Footer_js.php');?>

        <!-- Chartist -->
        <script src="<?php echo $host;?>assets/js/plugins/chartist/chartist.min.js"></script>

        <script src="<?php echo $host;?>assets/js/plugins/3DotContextMenu/context-menu.js"></script>

        <script>
            var host = "<?php echo $host;?>html"
            var host_entrepreneur = "<?php echo $host;?>entrepreneur"
            var MenuName = "DataShowReport";
            var TypeMenu = "DataShowReport";

            $(document).ready(function () {

                // Simple line
                new Chartist.Line('#ct-chart1', {
                    labels: ['July', 'August', 'September', 'October', 'November'],
                    series: [
                        [12, 9, 7, 8, 5],

                    ]
                }, {
                        fullWidth: true,
                        chartPadding: {
                            right: 40
                        }
                    });
                });
        </script>
        <script src="<?php echo $host;?>assets/js/custom_js/GoogleMaps.js"></script>
        <script src="<?php echo $host;?>assets/js/custom_js/GlobalFunction.js"></script>
        <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/GlobalFunction.js"></script>
        <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/DataShowReportView.js"></script>
    </body>

</html>