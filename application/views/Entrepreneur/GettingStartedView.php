<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>จัดการระบบ สำหรับผู้ประกอบการ เส้นทางท่องเที่ยว สี่เหลี่ยมวัฒนธรรมล้านช้าง</title>

    <link href="<?php echo $host;?>assets/entrepreneur/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $host;?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo $host;?>assets/entrepreneur/css/animate.css" rel="stylesheet">
    <link href="<?php echo $host;?>assets/entrepreneur/css/style.css" rel="stylesheet">


</head>

<body class="gray-bg">
    <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="minimalize-styl-2 btn btn-outline btn-default " href="/dasta_thailand/entrepreneur/Login/logout">
                    <i class="fa fa-angle-left"></i>
                    ออกจากระบบ
                </a>
            </div>
            <ul class="nav navbar-top-links navbar-center">
                <li class="text-center">
                    <h3 class="m-r-sm text-muted"> เพิ่มข้อมูล
                    </h3>
                    <span class="m-r-sm text-muted"> เกี่ยวกับกิจการของท่าน</span>
                </li>
            </ul>
            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope"></i>
                        <!-- <span class="label label-warning">16</span> -->
                    </a>
                </li> 
            </ul>
        </nav>
    </div>
    <div class="middle-box text-center animated fadeInDown">
        <h2 class="text-muted">เริ่มการใช้งานของท่าน</h2>
        <h1>
            <div style="text-align: center;">
                <img src="<?php echo $host;?>assets/img/Group 1969.png"  class="img">
            </div>
        </h1>
        <?php
            if($isActive == 0)
            {
                echo "<h3 class=\"font-bold text-muted\">กรุณารอการตรวจสอบ ระบบกำส่งข้อมูลเบื่องต้นไปยังเจ้าหน้าที่เพื่อตรวจสอบข้อมูลการลงทะเบียนของท่าน</h3>";
            }
            else{
                echo "<h3 class=\"font-bold text-muted\">คุณยังไม่มีรายการ</h3>";
                echo "<div class=\"error-desc\">";
                echo "<p class=\"text-muted\"> เพิ่มข้อมูลของท่าน เกี่ยวกับกิจการของท่าน</p> <br/>";
                echo "<button class=\"btn btn-primary m-t\" id=\"add_data\">เพิ่มข้อมูล</button>";
                echo "</div>";
            }
        ?>
    </div>
    <!-- Mainly scripts -->
    <script src="<?php echo $host;?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo $host;?>assets/js/popper.min.js"></script>
    <script src="<?php echo $host;?>assets/js/bootstrap.js"></script>
    <script>
        var host = "<?php echo $host;?>entrepreneur";
        $(document).ready(function(){
            $(document).on('click', '#add_data', function (e) {
                $.ajax({
                type: 'post',
                url: host+'/Main/get_add_type_page',
                    success: function (response) {
                        window.location.href ='/dasta_thailand/entrepreneur/'+response.typepage;
                    }
                });
            
            });
        });

    </script>

</body>

</html>