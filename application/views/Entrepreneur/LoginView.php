<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Content | Management System</title>

    <link href="<?php echo $host;?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $host;?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo $host;?>assets/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo $host;?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo $host;?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo $host;?>assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <div style="text-align: center;">
                    <img src="<?php echo $host;?>assets/img/Group 1916.png" style="width: 100%; " class="img">
                </div>
            </div>
            <div>
                <div style="text-align: center;">
                    <h2 class="text-muted font-bold">สำหรับผู้ประกอบการ</h2>
                </div>
            </div>
            <h4 class="text-muted">เส้นทางท่องเที่ยว สี่เหลี่ยมวัฒนธรรมล้านช้าง</h4>
            <br>
            <form class="m-t login-form" role="form" method="post">
                <div>
                    <div style="text-align: center;">
                        <h4 class="text-muted font-bold">ลงทะเบียน</h4>
                    </div>
                </div>
                <h5 class="text-muted">เพื่อการใช้งานที่ง่ายขึ้น</h5>
                <br>
                <div class="form-group">
                    <p class="text-center"><span class="note " id="notification"></span></p>
                </div>
                <div class="form-group">
                    <input type="Username" class="form-control" placeholder="ชื่อผู้ใช้" required="" name="input_account" >
                </div>
                <div class="form-group">
                    <input type="Password" class="form-control" placeholder="รหัสผ่าน" required="" name="input_password">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">เข้าสู่ระบบ</button>

                <a href="#" data-toggle="modal" data-target="#ModalForgot">
                    <small>ลืมรหัสผ่าน ?</small>
                </a>
                <p class="text-muted text-center"><small>Do not have an account?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="/dasta_thailand/entrepreneur/Register">Create an account</a>
            </form>
            <div>
                <div style="text-align: center;">
                    <img src="<?php echo $host;?>assets/img/APT.png" style="width: 100%; " class="img">
                </div>
            </div>
        </div>
    </div>
    <!--ModalForgotpassword-->
    <div class="modal inmodal" id="ModalForgot" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title">ลืมรหัสผ่าน</h4>
                </div>
                <div class="modal-body">
                    <div class="modal-body">ระบบจะทำการส่งรหัสผ่านใหม่ ไปยัง email ที่คุณลงทะเบียนไว้
                    </div>
                    <div class="form-group inner-addon left-addon">
                        <input class="form-control flat-input-underline" placeholder="กรอกชื่อผู้ใช้งาน" name="user_forget_password" type="text"
                            autofocus="">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">ปิด</button>
                    <button type="button" class="btn btn-primary">บันทึก</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Mainly scripts -->
    <script src="<?php echo $host;?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo $host;?>assets/js/popper.min.js"></script>
    <script src="<?php echo $host;?>assets/js/bootstrap.js"></script>

    <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/NotificationsEvent.js"></script>
    
    <!-- Insert these scripts at the bottom of the HTML, but before you use any Firebase services -->

    <!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-app.js"></script>

    <!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
    <script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-analytics.js"></script>

    <!-- Add Firebase products that you want to use -->
    <script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-firestore.js"></script>

    <script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-messaging.js"></script>

    <script src="<?php echo $host;?>assets/js/custom_js/firebase-init.js"></script>
    
    
    
    <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/Login.js"></script>

    

</body>

</html>