<!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>จัดการระบบ สำหรับผู้ประกอบการ เส้นทางท่องเที่ยว สี่เหลี่ยมวัฒนธรรมล้านช้าง</title>

        <?php $this->load->view('Entrepreneur/include/Header_css.php');?>

    </head>

    <body>

        <div id="wrapper">

            <!-- SideMenu -->
            <?php $this->load->view('Entrepreneur/include/SideMenu.php');?>
            <!-- End of SideMenu -->

            <div id="page-wrapper" class="gray-bg">
                 <!-- Header -->
                <?php $this->load->view('Entrepreneur/include/Header.php');?>
                <!-- End of Header -->

                <div class="wrapper wrapper-content animated fadeInRight">
                    <div class="row">
                        <div class="col-lg-3"></div>
                        <div class="col-lg-6 ">
                            <div class="ibox">
                                <!-- BookingOrderFilter -->
                                <?php $this->load->view('Entrepreneur/include/BookingOrderFilter.php');?>
                                <!-- End of BookingOrderFilter -->
                            </div>
                            <div class="col-lg-3"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-3"></div>
                            <div class="col-lg-6">
                                <div class="ibox">
                                    <!-- BookingOrder -->
                                    <div class="project-list">
                                        <table class="table table-hover" data-page-size="8" data-filter=#filter style="width:100%" id="ApprovalTable">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                        </table>
                                    <!-- BookingOrder -->
                                    </div>
                                </div>

                            <div class="col-lg-3"></div>
                            </div>
                        </div>
                    </div>

                    
                </div>
            </div>
        </div>
        <?php $this->load->view('Entrepreneur/include/Footer_js.php');?>

        <script>
            var host = "<?php echo $host;?>html";
            var host_entrepreneur = "<?php echo $host;?>entrepreneur";

            var MenuName = "BookingOrder";
            var TypeMenu = "Booking";
            var table;
            var state_approv = 2;
            var map;
            var gmarkers = [];
            var UseGoogleMap = '<?php echo $ENVIRONMENT?>';
            
        </script>
        <script src="<?php echo $host;?>assets/js/custom_js/GoogleMaps.js"></script>
        <script src="<?php echo $host;?>assets/js/custom_js/GlobalFunction.js"></script>
        <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/GlobalFunction.js"></script>
        <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/BookingOrderView.js"></script>
        
        
    </body>

</html>