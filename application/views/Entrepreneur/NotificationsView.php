<!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>จัดการระบบ สำหรับผู้ประกอบการ เส้นทางท่องเที่ยว สี่เหลี่ยมวัฒนธรรมล้านช้าง</title>

        <link href="<?php echo $host;?>assets/entrepreneur/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $host;?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo $host;?>assets/entrepreneur/css/animate.css" rel="stylesheet">
        <link href="<?php echo $host;?>assets/entrepreneur/css/style.css" rel="stylesheet">

        <link href="<?php echo $host;?>assets/css/jquery.dataTables.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">

    </head>

    <body>

        <div id="wrapper">

            <!-- SideMenu -->
            <?php $this->load->view('Entrepreneur/include/SideMenu.php');?>
            <!-- End of SideMenu -->

            <div id="page-wrapper" class="gray-bg">
                <div class="row border-bottom">
                    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                        <div class="navbar-header">
                            <!-- <a class="minimalize-styl-2 btn btn-outline btn-default " href="Booking-order-Details.html"><i class="fa fa-angle-left"></i></a> -->
                        </div>
                        <ul class="nav navbar-top-links navbar-center">
                            <li>
                                <h3 class="m-r-sm text-muted"><?=$this->lang->line("notifications");?></h3>
                            </li>
                        </ul>
                        <ul class="nav navbar-top-links navbar-right">
                            <li>
                                <h3 class="m-r-sm text-muted"></h3>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="wrapper wrapper-content animated fadeInRight">
                    <div class="row">
                        <div class="col-lg-3"></div>
                            <div class="col-lg-6 ">
                                <div class="ibox ">

                                    <table class="footable table table-hover nowrap" data-page-size="8" data-filter=#filter style="width:100%" id="ListChatTable">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        </tbody>
                                    </table>
                                
                                </div>
                            </div>
                            <div class="col-lg-3"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    <!-- Mainly scripts -->
    <script src="<?php echo $host;?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo $host;?>assets/js/popper.min.js"></script>
    <script src="<?php echo $host;?>assets/js/bootstrap.js"></script>

    <script src="<?php echo $host;?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo $host;?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo $host;?>assets/js/inspinia.js"></script>
    <script src="<?php echo $host;?>assets/js/plugins/pace/pace.min.js"></script>
    <script src="<?php echo $host;?>assets/js/plugins/pace/pace.min.js"></script>

    <script src="<?php echo $host;?>assets/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

    <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/NotificationsEvent.js"></script>

    <!-- Insert these scripts at the bottom of the HTML, but before you use any Firebase services -->

    <!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-app.js"></script>

    <!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
    <script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-analytics.js"></script>

    <!-- Add Firebase products that you want to use -->
    <script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-firestore.js"></script>

    <script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-messaging.js"></script>

    <script src="<?php echo $host;?>assets/js/custom_js/firebase-init.js"></script>

    <script>
        var host = "<?php echo $host;?>html";
        var host_entrepreneur = "<?php echo $host;?>entrepreneur";
       
        var MenuName = "Notifications";
        var UseGoogleMap = 'Notifications';
        var gmarkers = [];

        var user_id ="<?php echo $this->session->userdata("entrepreneur_user_id");?>"

    </script>
    <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/GlobalFunction.js"></script>
    <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/NotificationsView.js"></script>
    

    </body>

</html>