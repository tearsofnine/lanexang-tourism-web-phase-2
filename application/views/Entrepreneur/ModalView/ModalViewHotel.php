<div class="tabs-container">
    <ul class="nav nav-tabs" role="tablist">
        <li>
            <a class="nav-link active" data-toggle="tab" href="#tab-1-Product" id="tabTH-Product"> <?=$this->lang->line("lang_th");?></a>
        </li>
        <li>
            <a class="nav-link" data-toggle="tab" href="#tab-2-Product" id="tabENG-Product"><?=$this->lang->line("lang_en");?></a>
        </li>
        <li>
            <a class="nav-link" data-toggle="tab" href="#tab-3-Product" id="tabLAOS-Product"><?=$this->lang->line("lang_lao");?></a>
        </li>
        <li>
            <a class="nav-link" data-toggle="tab" href="#tab-4-Product" id="tabCHN-Product"><?=$this->lang->line("lang_chn");?></a>
        </li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" id="tab-1-Product" class="tab-pane active">
            <div class="">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox ">
                            <div class="ibox-content">
                                <div class="col-lg-12">
                                    <div class="ibox ">
                                        <div class="ibox-title  back-change">
                                            <h5>รายละเอียดตัวอย่างประเภทห้องพัก</h5>
                                            <div class="ibox-tools">
                                                <a class="collapse-link">
                                                    <i class="fa fa-chevron-up"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="form-group  row">
                                                <div class="ibox-content ">
                                                    <label class=" col-form-label">เพิ่มตัวอย่างประเภทห้องพัก</label>
                                                    <div class="row">
                                                        <div class="col-lg-12" id="create_image_field_hotelroom">
                                                            <div class="" id="image_box_hotelroom">
                                                            <!-- javascript  -->

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="">
                                                        <button class="btn btn-outline btn-primary add_image_field" name="hotelroom">เพิ่มห้องพัก</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" id="tab-2-Product" class="tab-pane">
            <div class="">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox ">
                            <div class="ibox-content">
                                <div class="col-lg-12">
                                    <div class="ibox ">
                                        <div class="ibox-title  back-change">
                                            <h5>รายละเอียดตัวอย่างประเภทห้องพัก</h5>
                                            <div class="ibox-tools">
                                                <a class="collapse-link">
                                                    <i class="fa fa-chevron-up"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="form-group  row">
                                                <div class="ibox-content ">
                                                    <label class=" col-form-label">เพิ่มตัวอย่างประเภทห้องพัก</label>
                                                    <div class="row">
                                                        <div class="col-lg-12" id="create_image_Eng_field_hotelroom">
                                                            <div class="" id="image_box_Eng_hotelroom">
                                                                <!-- javascript  -->
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" id="tab-3-Product" class="tab-pane">
            <div class="">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox ">
                            <div class="ibox-content">
                                <div class="col-lg-12">
                                    <div class="ibox ">
                                        <div class="ibox-title  back-change">
                                            <h5>รายละเอียดตัวอย่างประเภทห้องพัก</h5>
                                            <div class="ibox-tools">
                                                <a class="collapse-link">
                                                    <i class="fa fa-chevron-up"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="form-group  row">
                                                <div class="ibox-content ">
                                                    <label class=" col-form-label">เพิ่มตัวอย่างประเภทห้องพัก</label>
                                                    <div class="row">
                                                        <div class="col-lg-12" id="create_image_Laos_field_hotelroom">
                                                            <div class="" id="image_box_Laos_hotelroom">
                                                                <!-- javascript  -->
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" id="tab-4-Product" class="tab-pane">
            <div class="">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox ">
                            <div class="ibox-content">
                                <div class="col-lg-12">
                                    <div class="ibox ">
                                        <div class="ibox-title  back-change">
                                            <h5>รายละเอียดตัวอย่างประเภทห้องพัก</h5>
                                            <div class="ibox-tools">
                                                <a class="collapse-link">
                                                    <i class="fa fa-chevron-up"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="form-group  row">
                                                <div class="ibox-content ">
                                                    <label class=" col-form-label">เพิ่มตัวอย่างประเภทห้องพัก</label>
                                                    <div class="row">
                                                        <div class="col-lg-12" id="create_image_Chinese_field_hotelroom">
                                                            <div class="" id="image_box_Chinese_hotelroom">
                                                                <!-- javascript  -->
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>