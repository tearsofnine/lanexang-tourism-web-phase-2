
<!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>จัดการระบบ สำหรับผู้ประกอบการ เส้นทางท่องเที่ยว สี่เหลี่ยมวัฒนธรรมล้านช้าง</title>

        <link href="<?php echo $host;?>assets/entrepreneur/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $host;?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo $host;?>assets/entrepreneur/css/animate.css" rel="stylesheet">
        <link href="<?php echo $host;?>assets/entrepreneur/css/style.css" rel="stylesheet">

        <link href="<?php echo $host;?>assets/css/plugins/slick/slick.css" rel="stylesheet">
        <link href="<?php echo $host;?>assets/css/plugins/slick/slick-theme.css" rel="stylesheet">

    </head>

    <body>

        <div id="wrapper">
            <!-- SideMenu -->
            <?php $this->load->view('Entrepreneur/include/SideMenu.php');?>
            <!-- End of SideMenu -->

            <div id="page-wrapper" class="gray-bg">
                <!-- Header -->
                <?php $this->load->view('Entrepreneur/include/Header.php');?>
                <!-- End of Header -->
                <div class="wrapper wrapper-content animated fadeInRight">
                    <div class="row">
                        <div class="col-lg-3"></div>
                        <div class="col-lg-6">
                            <div class="ibox">
                                <div>
                                    <a href="Booking-order-Details.html">
                                        <div class="ibox-content no-padding border-left-right">
                                            <img class="d-block w-100 rounded" src="<?php echo $host;?>assets/img/uploadfile/<?php echo $data_item[0]->tempImageCover_paths;?>" alt="First slide">
                                        </div>
                                    </a>
                                    <div class="ibox-content-head text-center">
                                        <h2 class="text-Darkgreen font-bold ">
                                            <?php echo $data_item[0]->itmes_topicThai;?>
                                        </h2>
                                        <h3 class="text-muted font-normal">ที่อยู่สถานประกอบการ : <?php echo $data_item[0]->items_contactThai;?></h3>
                                        <h3 class="text-muted font-normal">ประเภทกลุ่มผู้ประกอบการ : <?php echo $data_item[0]->menuItem_thai;?></h3>
                                        <!-- <h3 class="text-muted font-normal"><?php echo $data_item[0]->category_thai;?></h3> -->
                                        <hr>
                                        <h3 class="text-Darkgreen font-normal float-right"> อัปเดตล่าสุด 14 ม.ค, 2020</h3>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Mainly scripts -->
        <script src="<?php echo $host;?>assets/js/jquery-3.1.1.min.js"></script>
        <script src="<?php echo $host;?>assets/js/popper.min.js"></script>
        <script src="<?php echo $host;?>assets/js/bootstrap.js"></script>

        <script src="<?php echo $host;?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script src="<?php echo $host;?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

        <!-- Custom and plugin javascript -->
        <script src="<?php echo $host;?>assets/js/inspinia.js"></script>
        <script src="<?php echo $host;?>assets/js/plugins/pace/pace.min.js"></script>


        <!-- slick carousel-->
        <script src="<?php echo $host;?>assets/js/plugins/slick/slick.min.js"></script>

        <script>
            $(document).ready(function () {


                $('.product-images').slick({
                    dots: true
                });

            });

        </script>

    </body>

</html>