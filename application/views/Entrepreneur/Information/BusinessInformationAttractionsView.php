
<!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>จัดการระบบ สำหรับผู้ประกอบการ เส้นทางท่องเที่ยว สี่เหลี่ยมวัฒนธรรมล้านช้าง</title>

        <?php $this->load->view('Entrepreneur/include/Header_css.php');?>
        <link href="<?php echo $host;?>assets/css/plugins/3DotContextMenu/context-menu.css" rel="stylesheet">
    </head>

    <body>
        
        <div id="wrapper">
            <!-- SideMenu -->
            <?php $this->load->view('Entrepreneur/include/SideMenu.php');?>
            <!-- End of SideMenu -->

            <div id="page-wrapper" class="gray-bg">
                <!-- Header -->
                <?php $this->load->view('Entrepreneur/include/Header.php');?>
                <!-- End of Header -->
                <div class="wrapper wrapper-content animated fadeInRight">
                    <div class="row">
                        <div class="col-lg-3">
                        </div>
                        <div class="col-lg-6">
                            <div class="ibox">
                                <div>
                                    
                                    <div class="ibox-content no-padding border-left-right">
                                        
                                        <img class="w-100 rounded" src="<?php echo $host;?>assets/img/uploadfile/<?php echo $data_item[0]->tempImageCover_paths;?>" alt="First slide">
                                        <span class="context-menu" data-container-id="context-menu-items" style="position: absolute;margin-left: -20px;color: white;"> </span>
                                    </div>
                                    <div class="ibox-content-head text-center">
                                        <h2 class="text-Darkgreen font-bold ">
                                            <?php echo $data_item[0]->itmes_topicThai;?>
                                        </h2>
                                        <h3 class="text-muted font-normal">ที่อยู่สถานประกอบการ : <?php echo $data_item[0]->items_contactThai;?></h3>
                                        <h3 class="text-muted font-normal">ประเภทกลุ่มผู้ประกอบการ : <?php echo $data_item[0]->menuItem_thai;?></h3>
                                        <h3 class="text-muted font-normal"><?php echo $data_item[0]->subcategory_thai;?></h3>
                                        <hr>
                                        <h3 class="text-Darkgreen font-normal float-right"> อัปเดตล่าสุด <?php echo date('d M , Y',strtotime($data_item[0]->items_updatedDTTM));?></h3>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-3"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- 3Dot -->
        <?php $this->load->view('Entrepreneur/include/3Dot.php');?>
        <!-- End of 3Dot -->

        <!-- ViewItemsModal -->
        <?php $this->load->view('Html/include/ViewItemsModal.php');?>
        <!-- End of ViewItemsModal -->

        <?php $this->load->view('Entrepreneur/include/Footer_js.php');?>
        <script src="<?php echo $host;?>assets/js/plugins/3DotContextMenu/context-menu.js"></script>

        <script>
            var host = "<?php echo $host;?>html"
            var host_entrepreneur = "<?php echo $host;?>entrepreneur"
            var map;
            var gmarkers = [];
            var UseGoogleMap = '<?php echo $ENVIRONMENT?>';
            var MenuName = "Attractions";
            var TypeMenu = "Items";
            var tableContextMenu = null;

            var items_id = "<?php echo $data_item[0]->items_id;?>";
            var user_id = "<?php echo $this->session->userdata('entrepreneur_user_id');?>";
            
        </script>
        <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/getItemsData.js"></script>
        <script src="<?php echo $host;?>assets/js/custom_js/GoogleMaps.js"></script>
        <script src="<?php echo $host;?>assets/js/custom_js/GlobalFunction.js"></script>
        <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/GlobalFunction.js"></script>
        <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/BusinessInformation/BusinessInformationAttractionsView.js"></script>

    </body>

</html>