<!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>จัดการระบบ สำหรับผู้ประกอบการ เส้นทางท่องเที่ยว สี่เหลี่ยมวัฒนธรรมล้านช้าง</title>

        <?php $this->load->view('Entrepreneur/include/Header_css.php');?>
        <link href="<?php echo $host;?>assets/css/plugins/3DotContextMenu/context-menu.css" rel="stylesheet">

    </head>

    <body>

        <div id="wrapper">

            <!-- SideMenu -->
            <?php $this->load->view('Entrepreneur/include/SideMenu.php');?>
            <!-- End of SideMenu -->

            <div id="page-wrapper" class="gray-bg">
                <!-- Header -->
                <?php $this->load->view('Entrepreneur/include/Header.php');?>
                <!-- End of Header -->

                <div class="wrapper wrapper-content animated fadeInRight">
                    <div class="row">
                        <div class="col-lg-3">
                        </div>
                        <div class="col-lg-6">
                            <div class="ibox">
                                <div>
                                    <div class="ibox-content no-padding border-left-right">
                                        <?php
                                            if(!is_null($this->session->userdata('user_profile_pic_url')) && $this->session->userdata('user_profile_pic_url') != "")
                                            {
                                                $src = $host."assets/img/uploadfile/".$this->session->userdata('user_profile_pic_url');
                                            }
                                            else
                                                $src = $host."assets/img/image_placeholder_test.jpg";
                                            
                                        ?>
                                        
                                        <img class="w-100 rounded" src="<?php echo $src;?>" alt="First slide">
                                        <span class="context-menu" data-container-id="context-menu-items" style="position: absolute;margin-left: -20px;color: white;"> </span>
                                    
                                    </div>
                                    <div class="ibox-content-head text-center">
                                        <h2 class="text-Darkgreen font-bold ">บริษัท <?php echo $this->session->userdata('business_nameThai');?></h2>
                                        <h3 class="text-muted font-normal">ที่อยู่สถานประกอบการ : <?php echo $this->session->userdata('business_presentAddress');?></h3>
                                        <h3 class="text-muted font-normal"> ประเภทกลุ่มผู้ประกอบการ : บริการแพคเกจทัวร์</h3>
                                        <hr>
                                        <h3 class="text-Darkgreen font-normal float-right"> อัปเดตล่าสุด <?php //echo date('d M , Y',strtotime($data_item[0]->items_updatedDTTM));?></h3>
                                    </div>
                                </div>
                            </div>
                            
                            
                            <br>
                            <div class="row">
                                <div class="">
                                    <div class="ibox ">
                                        <div class="ibox-title">

                                            <div class="ibox-tools">
                                                <a href="./ProgramTour/addProgramTour" class="btn btn-primary btn-xs">เพิ่มโปรแกรมการท่องเที่ยว</a>
                                                <!-- <a class="collapse-link">
                                                    <i class="fa fa-chevron-up"></i> -->
                                                </a>
                                            </div>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row m-b-sm m-t-sm">
                                                <div class="col-md-3">
                                                    <select data-placeholder="Choose a Category..."class="chosen-select programtour" tabindex="1" name="subcategory" id="subcategory">

                                                    </select>
                                                </div>
                                                <div class="col-md-3">
                                                    <select data-placeholder="Choose a Num Dates Trip..." class="chosen-select" tabindex="2" name="num_dates_trip" id="num_dates_trip">
                                                    
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="CustomSearchBox" placeholder="Search" class="form-control-sm form-control" >
                                                        <span class="input-group-btn">
                                                            <button type="button" class="btn btn-sm btn-primary"> Go!</button>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <table class="footable table table-hover" data-page-size="8" data-filter=#filter style="width:100%" id="ProgramTourTable">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>ประเภท</th>
                                                        <th>ระยะวัน</th>
                                                        <th>ชื่อโปรแกรมการท่องเที่ยว</th>
                                                        <th>ขอบเขต</th>
                                                        <th>สถานะ</th>
                                                        <th>วันที่อัปเดต</th>
                                                        <th class="text-right">เครื่องมือ</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ViewItemsModal -->
        <?php $this->load->view('Html/include/ViewItemsModal.php');?>
        <!-- End of ViewItemsModal -->
        <!-- 3Dot -->
        <?php $this->load->view('Entrepreneur/include/3Dot.php');?>
        <!-- End of 3Dot -->

        <?php $this->load->view('Entrepreneur/include/Footer_js.php');?>
        <script src="<?php echo $host;?>assets/js/plugins/3DotContextMenu/context-menu.js"></script>

        <script>
            var host = "<?php echo $host;?>html"
            var host_entrepreneur = "<?php echo $host;?>entrepreneur"
            var table;
            var MenuName = "ProgramTour";
            var TypeMenu = "ProgramTour";
            var type_page = "entrepreneur";
            
            var UseGoogleMap = '<?php echo $ENVIRONMENT?>';
        </script>
        <script src="<?php echo $host;?>assets/js/custom_js/getProgramTourData.js"></script>
        <script src="<?php echo $host;?>assets/js/custom_js/GoogleMaps.js"></script>
        <script src="<?php echo $host;?>assets/js/custom_js/GlobalFunction.js"></script>
        <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/GlobalFunction.js"></script>
        <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/BusinessInformation/BusinessInformationProgramTourView.js"></script>
    </body>

</html>