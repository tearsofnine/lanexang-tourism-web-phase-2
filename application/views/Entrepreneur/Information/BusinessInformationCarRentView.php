<!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>จัดการระบบ สำหรับผู้ประกอบการ เส้นทางท่องเที่ยว สี่เหลี่ยมวัฒนธรรมล้านช้าง</title>

        <?php $this->load->view('Entrepreneur/include/Header_css.php');?>
        <link href="<?php echo $host;?>assets/css/plugins/3DotContextMenu/context-menu.css" rel="stylesheet">
        
    </head>

    <body>

        <div id="wrapper">

            <!-- SideMenu -->
            <?php $this->load->view('Entrepreneur/include/SideMenu.php');?>
            <!-- End of SideMenu -->

            <div id="page-wrapper" class="gray-bg">
                <!-- Header -->
                <?php $this->load->view('Entrepreneur/include/Header.php');?>
                <!-- End of Header -->

                <div class="wrapper wrapper-content animated fadeInRight">
                    <div class="row">
                        <div class="col-lg-3">
                        </div>
                        <div class="col-lg-6">
                            <div class="ibox">
                            <div>
                                <a href="#">
                                    <div class="ibox-content no-padding border-left-right">
                                        <?php
                                            if(!is_null($this->session->userdata('user_profile_pic_url')) && $this->session->userdata('user_profile_pic_url') != "")
                                            {
                                                $src = $host."assets/img/uploadfile/".$this->session->userdata('user_profile_pic_url');
                                            }
                                            else
                                                $src = $host."assets/img/image_placeholder_test.jpg";
                                            
                                        ?>
                                        
                                        <img class="w-100 rounded" src="<?php echo $src;?>" alt="First slide">
                                        <span class="context-menu" data-container-id="context-menu-items" style="position: absolute;margin-left: -20px;color: white;"> </span>
                                    
                                    </div>
                                </a>
                                <div class="ibox-content-head text-center">
                                    <h2 class="text-Darkgreen font-bold ">
                                        บริษัท <?php echo $this->session->userdata('business_nameThai');?>
                                    </h2>
                                    <h3 class="text-muted font-normal">ที่อยู่สถานประกอบการ : <?php echo $this->session->userdata('business_presentAddress');?></h3>
                                    <h3 class="text-muted font-normal"> ประเภทกลุ่มผู้ประกอบการ : บริการรถเช่า</h3>
                                    <hr>
                                    <h3 class="text-Darkgreen font-normal float-right"> อัปเดตล่าสุด <?php echo date('d M , Y',strtotime($data_item[0]->items_updatedDTTM));?></h3>
                                </div>
                            </div>
                        </div>
                        <div class="ibox">
                            <hr>
                            <!-- SearchBox -->
                            <?php $this->load->view('Html/include/SearchBoxCarRent.php');?>
                            <!-- End of SearchBox -->
                        </div>
                        <div class="text-right">
                            <a href="./CarRent/addCarRent" class="btn btn-primary btn-xs">เพิ่มรถเช่า</a>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="ibox">
                                    <table class="footable table table-hover" data-page-size="8" data-filter=#filter style="width:100%" id="CarRentTable">
                                        <thead>
                                            <tr>
                                                <th class="th-item-table">
                                                    <span class="float-right">
                                                        <strong><span id="countData"></span></strong> <?=$this->lang->line("list");?>
                                                        </span>
                                                    <h5 style="font-size: 14px;">บริการรถเช่า</h5>                             
                                                </th>
                                               
                                                <th>ชื่อ</th>
                                                <th>ประเภท</th>
                                                <th>เกียร์</th>
                                                <th>วันที่</th>
                                            </tr>
                                        </thead>
                                        <tbody>                                                                         
                                                                                       
                                        </tbody>
                                    </table>
                                   
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ViewItemsModal -->
        <?php $this->load->view('Html/include/ViewItemsModal.php');?>
        <!-- End of ViewItemsModal -->
        <!-- 3Dot -->
        <?php $this->load->view('Entrepreneur/include/3Dot.php');?>
        <!-- End of 3Dot -->
        <?php $this->load->view('Entrepreneur/include/Footer_js.php');?>
        <script src="<?php echo $host;?>assets/js/plugins/3DotContextMenu/context-menu.js"></script>

        <script>
            var host = "<?php echo $host;?>html"
            var host_entrepreneur = "<?php echo $host;?>entrepreneur"
            var table;
            var MenuName = "CarRent";
            var TypeMenu = "Reservations";
            var items_id = "<?php echo $data_item[0]->items_id;?>";
            var user_id = "<?php echo $this->session->userdata('entrepreneur_user_id');?>";
            var map;
            var gmarkers = [];
            var UseGoogleMap = '<?php echo $ENVIRONMENT?>';
            var tableContextMenu = null;

        </script>
        <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/getReservationsData.js"></script>
        <script src="<?php echo $host;?>assets/js/custom_js/GoogleMaps.js"></script>
        <script src="<?php echo $host;?>assets/js/custom_js/GlobalFunction.js"></script>
        <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/GlobalFunction.js"></script>
        <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/BusinessInformation/BusinessInformationCarRentView.js"></script>
    </body>

</html>