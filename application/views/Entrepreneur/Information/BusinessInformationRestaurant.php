<!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>จัดการระบบ สำหรับผู้ประกอบการ เส้นทางท่องเที่ยว สี่เหลี่ยมวัฒนธรรมล้านช้าง</title>


        <link href="<?php echo $host;?>assets/entrepreneur/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo $host;?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo $host;?>assets/entrepreneur/css/animate.css" rel="stylesheet">
        <link href="<?php echo $host;?>assets/entrepreneur/css/style.css" rel="stylesheet">

        <link href="<?php echo $host;?>assets/css/plugins/slick/slick.css" rel="stylesheet">
        <link href="<?php echo $host;?>assets/css/plugins/slick/slick-theme.css" rel="stylesheet">

    </head>

    <body>
        <div id="wrapper">
            <!-- SideMenu -->
            <?php $this->load->view('Entrepreneur/include/SideMenu.php');?>
            <!-- End of SideMenu -->

            <div id="page-wrapper" class="gray-bg">
                <!-- Header -->
                <?php $this->load->view('Entrepreneur/include/Header.php');?>
                <!-- End of Header -->
                <div class="wrapper wrapper-content animated fadeInRight">
                    <div class="row">
                        <div class="col-lg-3">
                        </div>
                        <div class="col-lg-6">
                            <div class="ibox">
                                <div>
                                    <a href="#">
                                        <div class="ibox-content no-padding border-left-right">
                                            <img class="d-block w-100 rounded" src="<?php echo $host;?>assets/img/uploadfile/<?php echo $data_item[0]->tempImageCover_paths;?>" alt="First slide">
                                        </div>
                                    </a>
                                    <div class="ibox-content-head text-center">
                                        <h2 class="text-Darkgreen font-bold "><?php echo $data_item[0]->itmes_topicThai?></h2>
                                        <h3 class="text-muted font-normal">ที่อยู่สถานประกอบการ : <?php echo $data_item[0]->items_contactThai?></h3>
                                        <h3 class="text-muted font-normal">ประเภทกลุ่มผู้ประกอบการ : ร้านอาหาร</h3>
                                        <h3 class="text-muted font-normal"><?php echo $data_item[0]->subcategory_thai?></h3>
                                        <hr>
                                        <h3 class="text-Darkgreen font-normal float-right"> อัปเดตล่าสุด 14 ม.ค, 2020</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="ibox">
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group">
                                            <input type="text" placeholder="ค้นหา...." class="form-control-sm form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="text-right">
                                <a href="" class="btn btn-primary btn-xs">เพิ่มเมนู</a>
                            </div>
                            <br>

                            <div class="row">
                                <div class="col-md-12">

                                    <div class="ibox">
                                        <div class="ibox-titleTB">
                                            <span class="float-right">
                                                <strong><?php echo count($data_product);?></strong> รายการ</span>
                                            <h5>เมนูเด็ดต้องลอง</h5>
                                        </div>
                                        <?php 
                                            for($i = 0; $i<count($data_product);$i++ )
                                            {
                                                $PhotoProduct ="";
                                                for($j = 0; $j<count($data_product[$i]->PhotoProduct);$j++)
                                                {
                                                    $PhotoProduct .= "
                                                    <div>
                                                        <div class=\"image-imitation\">
                                                            <img class=\"w-100 rounded\" src=\"".$host."assets/img/uploadfile/".$data_product[$i]->PhotoProduct[$j]->photoProduct_paths."\" alt=\"First slide\">
                                                        </div>
                                                    </div>
                                                    ";
                                                }
                                                echo "
                                                    <div class=\"ibox-content-head2\">
                                                        <div class=\"row\">
                                                            <div class=\"col-md-5\">
                                                                <div class=\"product-images\">".$PhotoProduct."
                                                                </div>
                                                            </div>
                                                            <div class=\"col-md-7\">
                                                                <p></p>
                                                                <h2 class=\"font-bold text-navy m-b-xs\">".$data_product[$i]->product_namesThai."</h2>
                                                                <div class=\"m-t-md\">
                                                                    <h2 class=\"product-main-price text-muted\">ราคาขาย : ".$data_product[$i]->product_price." บาท</h2>
                                                                </div>
                                                                <br>
                                                                <br>
                                                                <br>
                                                                <div>
                                                                    <div class=\"m-t-sm\">
                                                                        <a href=\"#\" class=\"text-muted\"><i class=\"fa fa-folder\"></i> ดู</a>
                                                                        |
                                                                        <a href=\"#\" class=\"text-muted\"><i class=\"fa fa-pencil\"></i> แก้ไข</a>
                                                                        |
                                                                        <a href=\"#\" class=\"text-muted\"><i class=\"fa fa-trash-o\"></i> ลบ</a>
                                                                        <!--<p class=\"text-right\">14 ม.ค, 2020</p>-->
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                ";
                                            }
                                        ?>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-lg-3"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Mainly scripts -->
        <script src="<?php echo $host;?>assets/js/jquery-3.1.1.min.js"></script>
        <script src="<?php echo $host;?>assets/js/popper.min.js"></script>
        <script src="<?php echo $host;?>assets/js/bootstrap.js"></script>

        <script src="<?php echo $host;?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <script src="<?php echo $host;?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

        <!-- Custom and plugin javascript -->
        <script src="<?php echo $host;?>assets/js/inspinia.js"></script>
        <script src="<?php echo $host;?>assets/js/plugins/pace/pace.min.js"></script>


        <!-- slick carousel-->
        <script src="<?php echo $host;?>assets/js/plugins/slick/slick.min.js"></script>

        <script>
            $(document).ready(function () {


                $('.product-images').slick({
                    dots: true
                });

            });

        </script>

    </body>

</html>