<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>จัดการระบบ สำหรับผู้ประกอบการ เส้นทางท่องเที่ยว สี่เหลี่ยมวัฒนธรรมล้านช้าง</title>

    <link href="<?php echo $host;?>assets/entrepreneur/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $host;?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo $host;?>assets/entrepreneur/css/animate.css" rel="stylesheet">
    <link href="<?php echo $host;?>assets/entrepreneur/css/style.css" rel="stylesheet">

    <!-- <link href="<?php echo $host;?>assets/css/plugins/chosen/chosen.css" rel="stylesheet"> -->
    <link href="<?php echo $host;?>assets/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">
</head>

<body class="gray-bg">
<div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <div style="text-align: center;">
                    <h2 class="text-muted font-bold">แก้ไขข้อมูลผู้ประกอบการ </h2>
                </div>
            </div>
            <h4 class="text-muted">เส้นทางท่องเที่ยว สี่เหลี่ยมวัฒนธรรมล้านช้าง </h4>
            <br>
            <form class="m-t" role="form">
                <div class="form-group text-muted">
                    <h3 class="float-left">ข้อมูลสถานประกอบกิจการ</h3>
                    <label class="float-left">ชื่อสถานที่ประกอบการ *</label>
                    <input type="text" class="form-control" placeholder="ชื่อสถานที่ประกอบการ ภาษาไทย" required="" id="business_nameThai" value="<?php echo $user_data->business_nameThai;?>" >
                    <span class="note notifi" id="notifi_business_nameThai"></span>
                    <br>
                    <input type="text" class="form-control" placeholder="ชื่อสถานที่ประกอบการ ภาษาอังกฤษ" required="" id="business_nameEnglish" value="<?php echo $user_data->business_nameEnglish;?>"><br>
                    <input type="text" class="form-control" placeholder="ชื่อสถานที่ประกอบการ ภาษาจีน" required="" id="business_nameChinese" value="<?php echo $user_data->business_nameChinese;?>"><br>
                    <input type="text" class="form-control" placeholder="ชื่อสถานที่ประกอบการ ภาษาลาว" required="" id="business_nameLaos" value="<?php echo $user_data->business_nameLaos;?>"><br>
                </div>
            </form>
            <form class="m-t" role="form">
                <div class="form-group text-muted">
                    <label class="float-left">ประเภทกลุ่มผู้ประกอบการ *</label>
                    <!-- <select data-placeholder="Choose a Country..." class="chosen-select namecategory" tabindex="1" id="namecategory">
                    </select> -->
                    <span class="note notifi" id="notifi_namecategory"></span>
                </div>
            </form>
            <form class="m-t" role="form">
                <div class="form-group text-muted">
                    <label class="float-left">ที่อยู่ *</label>
                    <textarea class="form-control" placeholder="เช่น บ้านเลขที่ ถนน ซอย " id="user_presentAddress"><?php echo $user_data->business_presentAddress;?></textarea>
                    <span class="note notifi" id="notifi_user_presentAddress"></span>
                </div>
            </form>
            <form class="m-t" role="form">
                <div class="form-group text-muted">
                    <label class="float-left">ที่ตั้งประเทศ *</label>
                    <select data-placeholder="Choose a Country..." class="chosen-select" tabindex="1" id="country">
                    </select>
                    <span class="note notifi" id="notifi_country"></span>
                </div>
            </form>
            <form class="m-t" role="form">
                <div class="form-group text-muted">
                    <label class="float-left">ที่ตั้งจังหวัด *</label>
                    <select data-placeholder="Choose a Provinces..." class="chosen-select" tabindex="1" id="provinces" disabled>
                    </select>
                    <span class="note notifi" id="notifi_provinces"></span>
                </div>
            </form>
            <form class="m-t" role="form">
                <div class="form-group text-muted">
                    <label class="float-left">ที่ตั้งอำเภอ *</label>
                    <select data-placeholder="Choose a Districts..." class="chosen-select" tabindex="1" id="districts" disabled>
                    </select>
                    <span class="note notifi" id="notifi_districts"></span>
                </div>
            </form>
            <form class="m-t" role="form">
                <div class="form-group text-muted">
                    <label class="float-left">ที่ตั้งตำบล *</label>
                    <select data-placeholder="Choose a Subdistricts..." class="chosen-select" tabindex="1" id="subdistricts" disabled>
                    </select>
                    <span class="note notifi" id="notifi_subdistricts"></span>
                </div>
            </form>
            <form class="m-t" role="form">
                <div class="form-group text-muted">
                    <label class="float-left">ตำแหน่ง GPS</label>
                    <input type="text" class="form-control" placeholder="ค้นหาชื่อสถานที่เพิ่อปักหมุด" required="" id="pac-input">
                </div>
            </form>
            <form class="m-t" role="form">
                <div class="form-group text-muted">
                    <label class="float-left">หรือระบุ ละติจูล ลองจิจูล *</label>
                    <input type="text" class="form-control latitude" placeholder="latitude e.g. 13.7244416 " required="" id="business_latitude" maxlength="18" value="<?php echo $user_data->business_latitude;?>">
                    <span class="note notifi" id="notifi_business_latitude"></span>
                    <br>
                    <input type="text" class="form-control longitude" placeholder="longitude e.g. 100.3529128" required="" id="business_longitude" maxlength="18" value="<?php echo $user_data->business_longitude;?>">
                    <span class="note notifi" id="notifi_business_longitude"></span>
                </div>
            </form>
            <button class="btn btn-primary block full-width m-b" id="get_current_location">ตำแหน่งปัจจุบัน</button>
            <form class="m-t" role="form">
                <div class="form-group text-muted">
                    <div class="ibox ">
                        <div class="ibox-content" style="padding: 5px 5px 5px 5px;">
                            <div class="google-map" id="map"></div>
                        </div>
                    </div>
                </div>
            </form>
            <form class="m-t" role="form">
                <div class="form-group text-muted">
                    <label class="float-left">หมายเลขโทรศัพท์ (ของสถานประกอบการ) *</label>
                    <input type="text" class="form-control telephone" data-mask="(999) 999-9999" placeholder="" id="business_phone" value="<?php echo $user_data->business_phone;?>">
                    <span class="note notifi" id="notifi_business_phone"></span>
                </div>
            </form>
            <form class="m-t" role="form">
                <div class="form-group text-muted">
                    <label class="float-left">เว็บไซต์</label>
                    <input type="text" placeholder="www" class="form-control" id="business_www" value="<?php echo $user_data->business_www;?>">
                </div>
            </form>
            <form class="m-t" role="form">
                <div class="form-group text-muted">
                    <label class="float-left">เฟซบุ๊ก</label>
                    <input type="text" placeholder="www.Facebook/" class="form-control" id="business_facebook" value="<?php echo $user_data->business_facebook;?>">
                </div>
            </form>
            <form class="m-t" role="form">
                <div class="form-group text-muted">
                    <label class="float-left">ไลน์</label>
                    <input type="text" placeholder="ID Line" class="form-control" id="business_line" value="<?php echo $user_data->business_line;?>">
                </div>
            </form>
            <form class="m-t" role="form">
                <div class="form-group text-muted">
                    <label class="float-left">อีเมล</label>
                    <input type="email" placeholder="@gmail.com" class="form-control" id="user_email" value="<?php echo $user_data->user_email;?>">
                </div>
            </form>
            <form class="m-t" role="form">
                <div class="form-group text-muted">
                    <label class="float-left">หมายเลขผู้ประกอบกิจการ (ถ้ามี)</label>
                    <input type="text" placeholder="" class="form-control" id="business_operator_number" value="<?php echo $user_data->business_operator_number;?>">
                </div>
            </form>
            <form class="m-t" role="form">
                <div class="form-group text-muted">
                    <h3 class="float-left">ใบอนุญาตประกอบธุรกิจ (ถ้ามี)</h3>
                    <label class="float-left">* รองรับไฟล์สกุล .pdf .png jpg</label>
                    <!-- <div class="custom-file">
                        
                        <label for="business_license_paths" class="custom-file-label">เลือกไฟล์</label>
                    </div> -->
                    <input accept="image/jpg,image/png,image/jpeg" id="business_license_paths" type="file" class="" >
                </div>
            </form>
            <form class="m-t" role="form">
                <div class="form-group text-muted">
                    <h2 class="float-left">ข้อมูลผู้ลงทะเบียน</h2>
                    <h3 class="float-left">รูปโปรไพล์กิจการท่าน</h3>
                    <label class="float-left">* รองรับไฟล์สกุล .png jpg</label>
                    <!-- <div class="custom-file">
                        
                        <label for="business_profile_picture_paths" class="custom-file-label">เลือกไฟล์</label>
                    </div> -->
                    <input accept="image/jpg,image/png,image/jpeg" id="user_profile_pic_url" type="file" class="">
                </div>
            </form>
            <form class="m-t" role="form">
                <div class="form-group text-muted">
                    <label class="float-left">ชื่อ-นามสกุล *</label>
                    <input type="text" class="form-control" placeholder="ชื่อ " required="" id="user_firstName" value="<?php echo $user_data->user_firstName;?>">
                    <span class="note notifi" id="notifi_user_firstName"></span>
                    <br>
                    <input type="text" class="form-control" placeholder="นามสกุล" required="" id="user_lastName" value="<?php echo $user_data->user_lastName;?>">
                    <span class="note notifi" id="notifi_user_lastName"></span>
                </div>
            </form>
            <form class="m-t" role="form">
                <div class="form-group text-muted">
                    <label class="float-left">หมายเลขโทรศัพท์ของท่าน *</label>
                    <input type="text" class="form-control telephone" data-mask="(999) 999-9999" placeholder="" id="user_phone" value="<?php echo $user_data->user_phone;?>">
                    <span class="note notifi" id="notifi_user_phone"></span>
                </div>
            </form>
            <!-- <form class="m-t" role="form">
                <div class="form-group text-muted">
                    <label class="float-left">ชื่อและรหัสผ่านผู้ใช้ *</label>
                    <input type="text" class="form-control" placeholder="ชื่อผู้ใช้" required="" id="user_account" maxlength="20">
                    <span class="note notifi" id="notifi_user_account"></span>
                    <br>
                    <input type="password" class="form-control password" placeholder="รหัสผ่าน" required="" id="user_password">
                    <span class="note notifi" id="notifi_user_password"></span>
                    <br>
                    <input type="password" class="form-control password" placeholder="รหัสผ่านอีกครั้ง" required="" id="user_password_re">
                    <span class="note notifi" id="notifi_user_password_re"></span>
                </div>
            </form> -->
            <div class="col">
                <span class="note notifi" id="notifi_error_save"></span>
            </div>
            <button class="btn btn-primary block full-width m-b" id="save">บันทึก</button>
            <!-- <button type="submit" class="btn btn-primary block full-width m-b" data-toggle="modal" data-target="#Modalregister" >ลงทะเบียน</button> -->
        </div>
        <div>
            <div style="text-align: center;">
                <img src="<?php echo $host;?>assets/img/APT.png" style="width: 100%; " class="img">
            </div>
        </div>
    </div>
    <!--ModalModalregister-->
    <div class="modal inmodal fade" id="Modalregister" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title">กำลังทำการส่งข้อมูล</h4>
                </div>
                <div class="modal-body">
                    <div class="modal-body"> กรุณารอสักครู่ระบบกำส่งข้อมูลเบื่องต้น
                            ไปยังเจ้าหน้าที่เพื่อตรวจสอบข้อมูลการลงทะเบียนของท่าน
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="btn_success">ตกลง</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="<?php echo $host;?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo $host;?>assets/js/popper.min.js"></script>
    <script src="<?php echo $host;?>assets/js/bootstrap.js"></script>

    <script src="<?php echo $host;?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?php echo $host;?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo $host;?>assets/js/inspinia.js"></script>
    <script src="<?php echo $host;?>assets/js/plugins/pace/pace.min.js"></script>
    <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDrdQG5bfvPftYnMMdd8dOdgcl1GvODN6U&callback=initAutocomplete&libraries=places&v=weekly"defer></script> -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDrdQG5bfvPftYnMMdd8dOdgcl1GvODN6U&callback=initMap&libraries=places" defer></script>
    <!-- Chosen -->
    <script src="<?php echo $host;?>assets/js/plugins/chosen/chosen.jquery.js"></script>

    <!-- Insert these scripts at the bottom of the HTML, but before you use any Firebase services -->

    <script>
        var host = "<?php echo $host;?>html";
        var host_entrepreneur = "<?php echo $host;?>entrepreneur";
       
        var MenuName = "EditUserData";
        var TypeMenu = "EditUserData";
        var UseGoogleMap = 'production';
        var gmarkers = [];
        

    </script>
    <script src="<?php echo $host;?>assets/js/custom_js/GoogleMaps.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/GlobalFunction.js"></script>
    <script src="<?php echo $host;?>assets/entrepreneur/js/custom_js/EditUserDataView.js"></script>
</body>
</html>