<!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Content | Management System</title>
        <?php $this->load->view('Html/include/Header_css.php');?>  
    </head>

    <body>
        <!-- Preloader -->
        <div class="preloader" id="preloader">
            <div class="status" id="status"></div>
        </div>

        <div id="wrapper">
            <!-- SideMenu -->
            <?php $this->load->view('Html/include/SideMenu.php');?>
            <!-- End of SideMenu -->

            <div id="page-wrapper" class="gray-bg">
                <!-- Header -->
                <?php $this->load->view('Html/include/Header.php');?>
                <!-- End of Header -->
                <div class="wrapper wrapper-content animated fadeInRight">
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="ibox-content m-b-sm border-bottom">
                                <div class="forum-item">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <h2><?=$this->lang->line("sidebar_menu_Approval");?></h2>
                                            <ol class="breadcrumb">
                                                <li class="breadcrumb-item">
                                                    <a href="/dasta_thailand/html/Main">Home</a>
                                                </li>
                                                <li class="breadcrumb-item">
                                                    <?=$this->lang->line("sidebar_menu_Approval");?>
                                                </li>
                                                <li class="breadcrumb-item active">
                                                    <strong>โปรแกรมทัวร์ที่เข้ามาในระบบ</strong>
                                                </li>
                                                <li class="breadcrumb-item">
                                                    <?php 
                                                        if((intval($Approval_Item["ProgramTour"])) == 0)
                                                            echo "<span class=\"\" ></span>";
                                                        else
                                                            echo "<span class=\"badge badge-warning\" id=\"ApprovalItem\">".(intval($Approval_Item["ProgramTour"]))." การรอ</span>";
                                                        
                                                    ?>
                                                </li>
                                            </ol>
                                        </div>

                                        <!-- ApprovalStateBar -->
                                        <?php $this->load->view('Html/include/ApprovalStateBar.php');?>
                                        <!-- End of ApprovalStateBar -->

                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm-7">
                                    <div class="ibox">
                                        <div class="ibox-title">
                                            <h5 id="header_table_text">การอนุมัติ/โค๊ดส่วนลด</h5>
                                            <div class="ibox-tools">
                                                <h5 id="header_count_list"></h5>
                                            </div>
                                        </div>
                                        <div class="ibox-content">
                                            <!-- ApprovalSearchBox -->
                                            <?php $this->load->view('Html/include/ApprovalSearchBox.php');?>
                                            <!-- End of ApprovalSearchBox -->

                                            <div class="project-list">
                                                <table class="footable table table-hover table-striped nowrap" data-page-size="8" data-filter=#filter style="width:100%" id="ApprovalTable">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- View -->
                                <div class="col-sm-5">
                                    <div class="ibox selected">
		                                <div class="ibox-content">
                                            <div class="tab-content">
				                                <div id="" class="">
                                                    <div class="client-detail">
                                                        <div class="full-height-scroll" id="items_content">
                                                            <!-- ModalView -->
                                                            <?php $this->load->view('Html/ModalView/ModalViewProgramTour.php');?>
                                                            <!-- End of ModalView -->
                                                            <br>
                                                            <div class="text-right"  id="box_btn_programTour_approve">
                                                                <button class="btn btn-primary btn-xl btn-item-approve" value="" id="btn_programTour_approve">อนุมัติ</button>
                                                                <button class="btn btn-danger btn-xl btn-item-disapproval" value="" id="btn_programTour_disapprove">ไม่อนุมัติ</button>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- View -->
                            </div>
                        </div>
                    </div>
                </div>


                <!-- Footer -->
                <?php $this->load->view('Html/include/Footer.php');?>
                <!-- End of Footer -->
            </div>
        </div>
        <?php $this->load->view('Html/include/Footer_js.php');?>
       
        <script>
            var host = "<?php echo $host;?>html";
            
            var table;
            var map,mapEnglish,mapChinese,mapLaos;
            var cropper;
            var gmarkers = [];

            var MenuName = "ApprovalProgramTour";
            var TypeMenu = "Approval";
            var type_page = "html";
            var UseGoogleMap = '<?php echo $ENVIRONMENT?>';

            document.getElementById("state_all").textContent= "<?php echo $Approval_ItemState["All"];?>"
            document.getElementById("state_approved").textContent= "<?php echo $Approval_ItemState["Approved"];?>"
            document.getElementById("state_wait").textContent= "<?php echo (intval($Approval_Item["ProgramTour"]));?>"
            document.getElementById("state_refuse").textContent= "<?php echo $Approval_ItemState["Refuse"];?>"
            var header_table_text= "<?=$this->lang->line("sidebar_menu_Approval");?> / <?=$this->lang->line("sidebar_menu_ApprovalProgramTour");?>";
        </script>
        <script src="<?php echo $host;?>assets/js/custom_js/getProgramTourData.js"></script>
        <script src="<?php echo $host;?>assets/js/custom_js/GoogleMaps.js"></script>
        <script src="<?php echo $host;?>assets/js/custom_js/GlobalFunction.js"></script>
        <script src="<?php echo $host;?>assets/js/custom_js/Approval/ApprovalProgramTourView.js"></script>
        
    </body>
</html>