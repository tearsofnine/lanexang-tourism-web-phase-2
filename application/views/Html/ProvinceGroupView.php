<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Content | Management System</title>
    <?php $this->load->view('Html/include/Header_css.php'); ?>
</head>

<body>
    <!-- Preloader -->
    <div class="preloader" id="preloader">
        <div class="status" id="status"></div>
    </div>
    <div id="wrapper">
        <!-- SideMenu -->
        <?php $this->load->view('Html/include/SideMenu.php'); ?>
        <!-- End of SideMenu -->
        <div id="page-wrapper" class="gray-bg">
            <!-- SideMenu -->
            <?php $this->load->view('Html/include/Header.php'); ?>
            <!-- End of SideMenu -->

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>จัดการกลุ่มจังหวัด</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="../Main">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            สิทธิ์การใช้งาน
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>จัดการกลุ่มจังหวัด</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox ">
                            <div class="ibox-title">

                                <div class="ibox-tools">
                                    <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#ProvinceGroupModal" id="ProvinceGroupAdd">เพิ่มกลุ่มจังหวัด</button>
                                    <!-- <a href="Tourprogram-fromadd.html" class="btn btn-primary btn-xs">เพิ่มกลุ่มจังหวัด</a> -->
                                    <!-- <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i> -->
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content table-responsive">
                                <table class="footable table table-hover table-striped table-bordered nowrap" data-page-size="8" data-filter=#filter style="width:100%" id="ProvinceGroupTable">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>ชื่อกลุ่มจังหวัด</th>
                                            <th>ขอบเขต</th>
                                            <th>สถานะ</th>
                                            <th>เครื่องมือ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php $this->load->view('Html/ProvinceGroupModalView.php'); ?>

            <!-- Footer -->
            <?php $this->load->view('Html/include/Footer.php'); ?>

            <!-- End of Footer -->

        </div>
    </div>
    <?php $this->load->view('Html/include/Footer_pg_js.php'); ?>
    <!-- Custom and plugin javascript -->
    
    <script>
        var host = "<?php echo $host; ?>html/"
       
        var MenuName = "PermissionGroup";
        var box_field_province = 0;
        $(window).on('load', function() {
            $(".status").fadeOut();
            $(".preloader").fadeOut("slow");
        });
    </script>
    <script src="<?php echo $host; ?>assets/js/custom_js/GlobalFunction.js"></script>
    <script src="<?php echo $host; ?>assets/js/custom_js/ProvinceGroup.js"></script>
</body>

</html>