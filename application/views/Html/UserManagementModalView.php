<div class="modal fade UserManagementModal" id="UserManagementModal" tabindex="-1" role="dialog" aria-labelledby="UserManagementModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="UserManagementModalLabel">เพิ่มผู้ดูแลระบบ</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <label for="thTravelPlaceDescription" class="col-sm-4 col-xs-4 col-form-label">Email</label>
                    <div class="col-sm-6 col-xs-6">
                        <input type="email" placeholder="Email เพื่อเข้าใช้งาน" class="form-control form-control-input flat-input " id="user_email">
                        <span class="note notifi" id="notifi_user_email"><span>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="thTravelPlaceDescription" class="col-sm-4 col-xs-4 col-form-label">Password</label>
                    <div class="col-sm-6 col-xs-6">
                        <input type="text" style="-webkit-text-security: disc;text-security: disc;" placeholder="Password" class="form-control form-control-input flat-input ChangePasswordModals" id="user_password" required>
                        <span class="note notifi" id="notifi_user_password"><span>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="thTravelPlaceDescription" class="col-sm-4 col-xs-4 col-form-label">Confirm Password</label>
                    <div class="col-sm-6 col-xs-6">
                        <input type="text" style="-webkit-text-security: disc;text-security: disc;" placeholder="Confirm Password" class="form-control form-control-input flat-input ChangePasswordModals" id="user_passwordcon" required>
                        <span class="note notifi" id="notifi_user_passwordcon"><span>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="thTravelPlaceDescription" class="col-sm-4 col-xs-4 col-form-label">ชื่อ</label>
                    <div class="col-sm-6 col-xs-6">
                        <input type="text" placeholder="ชื่อ" class="form-control form-control-input flat-input " id="user_firstName">
                        <span class="note notifi" id="notifi_user_firstName"><span>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="thTravelPlaceDescription" class="col-sm-4 col-xs-4 col-form-label">นามสกุล</label>
                    <div class="col-sm-6 col-xs-6">
                        <input type="text" placeholder="นามสกุล" class="form-control form-control-input flat-input " id="user_lastName">
                        <span class="note notifi" id="notifi_user_lastName"><span>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="thTravelPlaceDescription" class="col-sm-4 col-xs-4 col-form-label">เพศ</label>
                    <div class="col-sm-6 col-xs-6">
                        <select class="form-control chosen-select" tabindex="1" id="user_gender">
                            <option value="">เพศ</option>
                            <option value="Male">ชาย</option>
                            <option value="Female">หญิง</option>
                        </select>
                        <span class="note notifi" id="notifi_user_gender"><span>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="thTravelPlaceDescription" class="col-sm-4 col-xs-4 col-form-label">อายุ</label>
                    <div class="col-sm-6 col-xs-6">
                        <input type="number" placeholder="" class="form-control form-control-input flat-input " id="user_age" min="0">
                        <span class="note notifi" id="notifi_user_age"><span>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="thTravelPlaceDescription" class="col-sm-4 col-xs-4 col-form-label">เบอร์โทรศัพท์</label>
                    <div class="col-sm-6 col-xs-6">
                        <input type="text" placeholder="" class="form-control form-control-input flat-input telephone" id="user_phone" maxlength="10">
                        <span class="note notifi" id="notifi_user_phone"><span>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="thTravelPlaceDescription" class="col-sm-4 col-xs-4 col-form-label">ระดับ</label>
                    <div class="col-sm-6 col-xs-6">
                        <select data-placeholder="Choose a Level..." class="chosen-select " tabindex="1" id="UserRole_userRole_id"></select>
                        <span class="note notifi" id="notifi_UserRole_userRole_id"><span>
                    </div>
                </div>
                <div class="form-group row invisible"  id="box_ProvinceGroup">
                    <label for="thTravelPlaceDescription" class="col-sm-4 col-xs-4 col-form-label">กลุ่มจังหวัด</label>
                    <div class="col-sm-6 col-xs-6">
                        <select data-placeholder="Choose a Level..." class="chosen-select " tabindex="1" id="ProvinceGroup_pg_id"></select>
                        <span class="note notifi" id="notifi_ProvinceGroup_pg_id"><span>
                    </div>
                </div>
                <div class="form-group row invisible" id="box_province">
                    <label for="thTravelPlaceDescription" class="col-sm-4 col-xs-4 col-form-label">จังหวัด</label>
                    <div class="col-sm-6 col-xs-6">
                        <select data-placeholder="Choose a Level..." class="chosen-select " tabindex="1" id="province"></select>
                        <span class="note notifi" id="notifi_province"><span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col">
                        <span class="note notifi" id="notifi_error_save_UserManagement"><span>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-success" type="button" id="btn_save" value="">Save</button>
                            <input type="hidden" id="user_id">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>