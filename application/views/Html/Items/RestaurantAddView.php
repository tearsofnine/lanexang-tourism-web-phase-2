<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Content | Management System</title>

    <?php $this->load->view('Html/include/Header_css.php');?>  

</head>
<body>
    <!-- Preloader -->
    <div class="preloader" id="preloader">
        <div class="status" id="status"></div>
    </div>
    <div id="wrapper">
        <!-- SideMenu -->
        <?php $this->load->view('Html/include/SideMenu.php');?>
        <!-- End of SideMenu -->

        <div id="page-wrapper" class="gray-bg">
            <!-- Header -->
            <?php $this->load->view('Html/include/Header.php');?>
            <!-- End of Header -->

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2><?=$this->lang->line("sidebar_menu_Restaurant");?></h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="/dasta_thailand/html/Main">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="/dasta_thailand/html/Items/Restaurant"><?=$this->lang->line("sidebar_menu_Restaurant");?></a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong><?=$this->lang->line("add");?> <?=$this->lang->line("sidebar_menu_Restaurant");?></strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="tabs-container">
                            <ul class="nav nav-tabs" role="tablist">
                                <li>
                                    <a class="nav-link active" data-toggle="tab" href="#tab-1" id="tabTH"> <?=$this->lang->line("lang_th");?></a>
                                </li>
                                <li>
                                    <a class="nav-link" data-toggle="tab" href="#tab-2" id="tabENG"><?=$this->lang->line("lang_en");?></a>
                                </li>
                                <li>
                                    <a class="nav-link" data-toggle="tab" href="#tab-3" id="tabLAOS"><?=$this->lang->line("lang_lao");?></a>
                                </li>
                                <li>
                                    <a class="nav-link" data-toggle="tab" href="#tab-4" id="tabCHN"><?=$this->lang->line("lang_chn");?></a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div role="tabpanel" id="tab-1" class="tab-pane active">
                                    <div class="">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5><?=$this->lang->line("cover_photo");?></h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <p>
                                                            เพิ่มภาพหน้าปก<?=$this->lang->line("sidebar_menu_Restaurant");?>
                                                        </p>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="image-crop" >
                                                                    <img class="image-crop-img" src="<?php echo $host;?>assets/img/p3.jpg" id="image_crop_img" style="width: 590px; height: 314px;">
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <h4><?=$this->lang->line("preview_pictures");?></h4>
                                                                <div class="form-group  row">
                                                                    <div class="img-preview img-preview-sm"></div>
                                                                    <div class="" style="margin-left: 5px;">
                                                                        <button type="button" class="btn btn-primary btn-reset-crop" id="btn_reset_crop" data-method="reset" title="Reset">
                                                                            <span class="fa fa-refresh"></span>
                                                                            </span>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                                <h4><?=$this->lang->line("cover_photo");?></h4>
                                                                <p>
                                                                    <?=$this->lang->line("adjustments_on_pictures");?>
                                                                </p>
                                                                <div class="btn-group">
                                                                    <label title="Upload image file" for="inputImage" class="btn btn-primary">
                                                                        <input type="file" accept="image/jpg,image/png,image/jpeg" name="file" id="inputImage" class="hide"> Upload new image
                                                                    </label>
                                                                </div>
                                                                <label style="color: red;">* รูปภาพควรมีอัตราส่วน 16:9, กว้าง x ยาว ไม่เกิน 1920 X 1080  pixels และขนาดไม่เกิน 2 MB.</label>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>วีดีโอ (ถ้ามี)</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ลิ้งวีดีโอ youtube</label>
                                                            <div class="col-sm-10">
                                                                <input type="" placeholder="www.youtube" class="form-control" id="coverItem_url">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>ข้อมูลสถานที่</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ชื่อสถานที่</label>
                                                            <div class="col-sm-10">
                                                                <input type="" placeholder="ชื่อสถานที่" class="form-control" id="itmes_topicThai">
                                                                <span class="note notifi" id="notifi_itmes_topicThai"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label"><?=$this->lang->line("name_location_type");?></label>
                                                            <div class="col-sm-10">
                                                                <select data-placeholder="Choose a Country..." class="chosen-select namesubcategory" tabindex="1" id="namesubcategory">
                                                                </select>
                                                                <span class="note notifi" id="notifi_namesubcategory"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ที่ตั้งประเทศ</label>
                                                            <div class="col-sm-10">
                                                                <select data-placeholder="Choose a Country..." class="chosen-select country" tabindex="1" id="country">
                                                                </select>
                                                                <span class="note notifi" id="notifi_country"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ที่ตั้งจังหวัด</label>
                                                            <div class="col-sm-10">
                                                                <select data-placeholder="Choose a Provinces..." class="chosen-select provinces" tabindex="1" id="provinces" disabled>
                                                                </select>
                                                                <span class="note notifi" id="notifi_provinces"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ที่ตั้งอำเภอ</label>
                                                            <div class="col-sm-10">
                                                                <select data-placeholder="Choose a Districts..." class="chosen-select districts" tabindex="1" id="districts" disabled>
                                                                </select>
                                                                <span class="note notifi" id="notifi_districts"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ที่ตั้งตำบล</label>
                                                            <div class="col-sm-10">
                                                                <select data-placeholder="Choose a Subdistricts..." class="chosen-select subdistricts" tabindex="1" id="subdistricts" disabled>
                                                                </select>
                                                                <span class="note notifi" id="notifi_subdistricts"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 col-form-label"></label>
                                                            <div class="col-sm-10">
                                                                <textarea rows="4" class="form-control form-control-input flat-input contact" placeholder="รายละเอียดสถานที่ติดต่อ เช่น บ้านเลขที่" id="items_contactThai"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>ตำแหน่ง GPS</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <div id="map"></div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ละติจูล</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" placeholder="latitude e.g. 13.7244416" class="form-control latitude" id="items_latitude" maxlength="18">
                                                                <span class="note notifi" id="notifi_items_latitude"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ลองจิจูล</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" placeholder="longitude e.g. 100.3529128" class="form-control longitude" id="items_longitude" maxlength="18">
                                                                <span class="note notifi" id="notifi_items_longitude"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>ข้อมูลการติดต่อ</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <h5>
                                                            รายละเอียดการเปิด-ปิด
                                                        </h5>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">เลือกวันเปิด-ปิด</label>
                                                            <div class="col-sm-10">
                                                                <select data-placeholder="เลือกวันเปิด-ปิด" class="chosen-select-dayofweek" tabindex="1" multiple id="dayofweek">
                                                                </select>
                                                                <span class="note notifi" id="notifi_dayofweek"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">เวลาเปิด</label>
                                                            <div class="col-sm-10">
                                                                <input class="form-control timepicker" type="text" id="items_timeOpen">
                                                                <span class="note notifi" id="notifi_items_timeOpen"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">เวลาปิด</label>
                                                            <div class="col-sm-10">
                                                                <input class="form-control timepicker" type="text" id="items_timeClose">
                                                                <span class="note notifi" id="notifi_items_timeClose"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">เบอร์โทรติดต่อ</label>
                                                            <div class="col-sm-10">
                                                                    <input type="text" class="form-control telephone" placeholder="" id="items_phone">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>การติดต่อทาง สังคมออนไลน์ (ถ้ามี) </h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">e-mail</label>
                                                            <div class="col-sm-10">
                                                                <input type="" placeholder="@" class="form-control" id="items_email">
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">Line</label>
                                                            <div class="col-sm-10">
                                                                <input type="" placeholder="@" class="form-control" id="items_line">
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">Facebook Page</label>
                                                            <div class="col-sm-10">
                                                                <input type="" placeholder="www.Facebook/" class="form-control" id="items_facebookPage">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-lg-12">
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดสถานที่</h5>
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">สิ่งที่จะได้พบ</label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด คำบรรยายสรุปสถานที่" rows="5" id="topicdetail_id_1_Thai"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <div class="ibox-content ">
                                                                <label class=" col-form-label">เลือกรูปภาพประกอบสถานที่ สามารถเพิ่มได้หลายรูป</label>
                                                                <div class="row">
                                                                    <div class="col-lg-12" id="create_image_field_thingstomeet">
                                                                        <div class="" id="image_box_thingstomeet">
                                                                           <!-- javascript  -->
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="">
                                                                    <button class="btn btn-outline btn-primary add_image_field" name="thingstomeet">เลือกรูปภาพ</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>เพิ่มเนื้อหาและรูปภาพ</h5>
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">เนื้อหา</label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด คำบรรยายสรุปเนื้อหา" rows="5" id="topicdetail_id_2_Thai"></textarea>
                                                            </div>
                                                        </div>
                                                        <h5>สร้างเรื่องราวให้กับสถานที่นี้</h5>

                                                        <div class="" id="create_image_field_info">
                                                            <div class="" id="image_box_info">
                                                                <!-- javascript  -->
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="">
                                                            <button class="btn btn-outline btn-primary add_image_field" name="info">เพิ่มรูปภาพพร้อมคำบรรยาย</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดการเดินทาง</h5>
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">รายละเอียดการเดินทาง</label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียดการเดินทาง" rows="5" id="topicdetail_id_3_Thai"></textarea>
                                                            </div>
                                                        </div>
                                                        <h5>สร้างเรื่องราวในการเดินทางมาสถานที่นี้</h5>
                                                            
                                                        <div class="" id="create_image_field_navigate">
                                                            <div class="" id="image_box_navigate">
                                                                <!-- javascript  -->
                                                            </div>
                                                        </div>

                                                        <div class="">
                                                            <button  class="btn btn-outline btn-primary add_image_field"  name="navigate">เพิ่มรูปภาพพร้อมคำบรรยาย</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดเมนูเด็ดของทางร้าน</h5>
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <div class="ibox-content ">
                                                                <label class=" col-form-label">เพิ่มเนื้อหาและรูปภาพ สามารถเพิ่มกี่เมนูก็ได้</label>
                                                                <div class="row">
                                                                    <div class="col-lg-12" id="create_image_field_foodmenu">
                                                                        <div class="" id="image_box_foodmenu">
                                                                           <!-- javascript  -->

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="">
                                                                    <button class="btn btn-outline btn-primary add_image_field" name="foodmenu">เพิ่มเมนูอาหาร</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="ibox">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รับประกันความอร่อยโดย (ถ้ามี)</h5>
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <h5>เพิ่มโลโก้ที่ได้รับประกันความอร่อยของร้านอาหารแห่งนี้</h5>                          
                                                        <div class="form-group  row">
                                                            <div class="col-sm-10">
                                                                <select data-placeholder="เลือกโลโก้ที่ได้รับประกันความอร่อย" class="chosen-select-delicious" tabindex="1" multiple id="delicious">
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-12" id="create_image_field_delicious">
                                                                <div class="" id="image_box_delicious">
                                                                    <!-- javascript  -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                       
                                                        
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>ลิงค์สำหรับเชื่อมโยงไปยัง AR APPLICATION</h5>
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ลิงค์จากระบบ 3D ล้านช้าง</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" placeholder="www" class="form-control" id="item_ar_url">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>ลิงค์สำหรับเชื่อมโยงไปยัง VR APPLICATION</h5>
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ลิงค์จากระบบ 3D ล้านช้าง</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" placeholder="www" class="form-control" id="item_vr_url" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div role="tabpanel" id="tab-2" class="tab-pane">
                                    <div class="">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>ข้อมูลสถานที่</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ชื่อสถานที่</label>
                                                            <div class="col-sm-10">
                                                                <input type="" placeholder="ชื่อสถานที่" class="form-control" id="itmes_topicEnglish">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 col-form-label"></label>
                                                            <div class="col-sm-10">
                                                                <textarea rows="4" class="form-control form-control-input flat-input contact" placeholder="รายละเอียดสถานที่ติดต่อ เช่น บ้านเลขที่" id="items_contactEnglish"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดสถานที่</h5>
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">สิ่งที่จะได้พบ</label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด คำบรรยายสรุปสถานที่" rows="5" id="topicdetail_id_1_English"></textarea>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>เพิ่มเนื้อหาและรูปภาพ</h5>
                                                       
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">เนื้อหา</label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด คำบรรยายสรุปเนื้อหา" rows="5" id="topicdetail_id_2_English"></textarea>
                                                            </div>
                                                        </div>
                                                        <h5>สร้างเรื่องราวให้กับสถานที่นี้</h5>

                                                        <div class="" id="create_image_Eng_field_info">
                                                            <div class="" id="image_box_Eng_info">
                                                                <!-- javascript  -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดการเดินทาง</h5>
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">รายละเอียดการเดินทาง</label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียดการเดินทาง" rows="5" id="topicdetail_id_3_English"></textarea>
                                                            </div>
                                                        </div>
                                                        <h5>สร้างเรื่องราวในการเดินทางมาสถานที่นี้</h5>
                                                        
                                                        <div class="" id="create_image_Eng_field_navigate">
                                                            <div class="" id="image_box_Eng_navigate">
                                                                <!-- javascript  -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดเมนูเด็ดของทางร้าน</h5>
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <div class="ibox-content ">
                                                                <label class=" col-form-label">เพิ่มเนื้อหาและรูปภาพ สามารถเพิ่มกี่เมนูก็ได้</label>
                                                                <div class="row">
                                                                    <div class="col-lg-12" id="create_image_Eng_field_foodmenu">
                                                                        <div class="" id="image_box_Eng_foodmenu">
                                                                           <!-- javascript  -->
                                                                           
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" id="tab-3" class="tab-pane">
                                    <div class="">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>ข้อมูลสถานที่</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ชื่อสถานที่</label>
                                                            <div class="col-sm-10">
                                                                <input type="" placeholder="ชื่อสถานที่" class="form-control" id="itmes_topicLaos">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 col-form-label"></label>
                                                            <div class="col-sm-10">
                                                                <textarea rows="4" class="form-control form-control-input flat-input contact" placeholder="รายละเอียดสถานที่ติดต่อ เช่น บ้านเลขที่" id="items_contactLaos"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดสถานที่</h5>
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">สิ่งที่จะได้พบ</label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด คำบรรยายสรุปสถานที่" rows="5" id="topicdetail_id_1_Laos"></textarea>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>เพิ่มเนื้อหาและรูปภาพ</h5>
                                                        
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">เนื้อหา</label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด คำบรรยายสรุปเนื้อหา" rows="5" id="topicdetail_id_2_Laos"></textarea>
                                                            </div>
                                                        </div>
                                                        <h5>สร้างเรื่องราวให้กับสถานที่นี้</h5>

                                                        <div class="" id="create_image_Laos_field_info">
                                                            <div class="" id="image_box_Laos_info">
                                                                <!-- javascript  -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดการเดินทาง</h5>
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">รายละเอียดการเดินทาง</label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียดการเดินทาง" rows="5" id="topicdetail_id_3_Laos"></textarea>
                                                            </div>
                                                        </div>
                                                        <h5>สร้างเรื่องราวในการเดินทางมาสถานที่นี้</h5>
                                                        
                                                        <div class="" id="create_image_Laos_field_navigate">
                                                            <div class="" id="image_box_Laos_navigate">
                                                                <!-- javascript  -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดเมนูเด็ดของทางร้าน</h5>
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <div class="ibox-content ">
                                                                <label class=" col-form-label">เพิ่มเนื้อหาและรูปภาพ สามารถเพิ่มกี่เมนูก็ได้</label>
                                                                <div class="row">
                                                                    <div class="col-lg-12" id="create_image_Laos_field_foodmenu">
                                                                        <div class="" id="image_box_Laos_foodmenu">
                                                                           <!-- javascript  -->
                                                                           
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" id="tab-4" class="tab-pane">
                                    <div class="">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>ข้อมูลสถานที่</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ชื่อสถานที่</label>
                                                            <div class="col-sm-10">
                                                                <input type="" placeholder="ชื่อสถานที่" class="form-control" id="itmes_topicChinese">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 col-form-label"></label>
                                                            <div class="col-sm-10">
                                                                <textarea rows="4" class="form-control form-control-input flat-input contact" placeholder="รายละเอียดสถานที่ติดต่อ เช่น บ้านเลขที่" id="items_contactChinese"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดสถานที่</h5>
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">สิ่งที่จะได้พบ</label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด คำบรรยายสรุปสถานที่" rows="5" id="topicdetail_id_1_Chinese"></textarea>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>เพิ่มเนื้อหาและรูปภาพ</h5>
                                                        
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">เนื้อหา</label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด คำบรรยายสรุปเนื้อหา" rows="5" id="topicdetail_id_2_Chinese"></textarea>
                                                            </div>
                                                        </div>
                                                        <h5>สร้างเรื่องราวให้กับสถานที่นี้</h5>

                                                        <div class="" id="create_image_Chinese_field_info">
                                                            <div class="" id="image_box_Chinese_info">
                                                                <!-- javascript  -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดการเดินทาง</h5>
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">รายละเอียดการเดินทาง</label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียดการเดินทาง" rows="5" id="topicdetail_id_3_Chinese"></textarea>
                                                            </div>
                                                        </div>
                                                        <h5>สร้างเรื่องราวในการเดินทางมาสถานที่นี้</h5>
                                                        
                                                        <div class="" id="create_image_Chinese_field_navigate">
                                                            <div class="" id="image_box_Chinese_navigate">
                                                                <!-- javascript  -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดเมนูเด็ดของทางร้าน</h5>
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <div class="ibox-content ">
                                                                <label class=" col-form-label">เพิ่มเนื้อหาและรูปภาพ สามารถเพิ่มกี่เมนูก็ได้</label>
                                                                <div class="row">
                                                                    <div class="col-lg-12" id="create_image_Chinese_field_foodmenu">
                                                                        <div class="" id="image_box_Chinese_foodmenu">
                                                                           <!-- javascript  -->
                                                                           
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <div class="col">
                                        <span class="note notifi" id="notifi_error_save"></span>
                                    </div>
                                    <button class="btn btn-primary btn-xl" id="save">บันทึก</button>
                                    
                                </div>
                                <br>
                            </div>


                        </div>
                    </div>

                </div>
            </div>

            <!-- Footer -->
            <?php $this->load->view('Html/include/Footer.php');?>
            <!-- End of Footer -->
        </div>  
    </div>
        
    <?php $this->load->view('Html/include/Footer_js.php');?>
    <script>
        var host = "<?php echo $host;?>html";
        var table;
        var image_info = 0;
        var image_navigate = 0;
        var image_thingstomeet = 0;
        var image_foodmenu = 0;
        var array_image_foodmenu = [];
        var array_delicious =[];
        var map;
        var cropper;
        var gmarkers = [];
        var CropData,CropBoxData,CanvasData;
        var MenuName = "Restaurant";
        var TypeMenu = "Items";
        var str_array;
        var UseGoogleMap = '<?php echo $ENVIRONMENT?>';
		var type_page = "html";
    </script>
    <script src="<?php echo $host;?>assets/js/custom_js/ProductFunction.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/GoogleMaps.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/GlobalFunction.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/LocationManagement.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/Items/RestaurantAddView.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/DateManagement.js"></script>
    

</body>

</html>