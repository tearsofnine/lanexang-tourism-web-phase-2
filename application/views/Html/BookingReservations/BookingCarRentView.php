<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Content | Management System</title>
    <?php $this->load->view('Html/include/Header_css.php');?>  
</head>
<body>
    <!-- Preloader -->
    <div class="preloader" id="preloader">
        <div class="status" id="status"></div>
    </div>
    <div id="wrapper">
        <!-- SideMenu -->
        <?php $this->load->view('Html/include/SideMenu.php');?>
        <!-- End of SideMenu -->
        <div id="page-wrapper" class="gray-bg">
            <!-- Header -->
            <?php $this->load->view('Html/include/Header.php');?>
            <!-- End of Header -->

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2><?=$this->lang->line("sidebar_menu_BookingCarRent");?></h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="../Main">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <?=$this->lang->line("sidebar_menu_Booking");?>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong><?=$this->lang->line("sidebar_menu_BookingCarRent");?></strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-8 ">
                        <div class="ibox">
                            <!-- BookingOrderFilter -->
                            <?php $this->load->view('Entrepreneur/include/BookingOrderFilter.php');?>
                            <!-- End of BookingOrderFilter -->
                        </div>
                        <div class="col-lg-2"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-8">
                  
                        <div class="ibox">
                            <!-- BookingOrder -->
                            <div class="project-list">
                                <table class="table table-hover" data-page-size="8" data-filter=#filter style="width:100%" id="BookingCarRentTable">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                    </tbody>
                                </table>
                            <!-- BookingOrder -->
                            </div>
                        </div>


                    </div>
                    <div class="col-lg-2"></div>
                </div>

            </div>

            <!-- Footer -->
            <?php $this->load->view('Html/include/Footer.php');?>
            <!-- End of Footer -->
        </div>
    </div>
    <?php $this->load->view('Html/include/Footer_js.php');?>

    <script>
        var host = "<?php echo $host;?>html"
        var table;
        var MenuName = "BookingCarRent";
        var UseGoogleMap = '<?php echo $ENVIRONMENT?>';
    </script>
    <script src="<?php echo $host;?>assets/js/custom_js/GoogleMaps.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/GlobalFunction.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/LocationManagement.js"></script>
    <!-- <script src="<?php echo $host;?>assets/js/custom_js/CarRentAdsView.js"></script> -->
    <!-- <script src="<?php echo $host;?>assets/js/custom_js/CustomsDataTable.js"></script> -->
    <script src="<?php echo $host;?>assets/js/custom_js/BookingReservations/BookingCarRentView.js"></script>
        
</body>

</html>