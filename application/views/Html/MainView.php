<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Content | Management System</title>
    <link href="<?php echo $host; ?>assets/css/plugins/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="<?php echo $host; ?>assets/css/sb-admin-2.css" rel="stylesheet">
    <?php $this->load->view('Html/include/Header_css.php'); ?>

</head>

<body>
    <!-- Preloader -->
    <div class="preloader" id="preloader">
        <div class="status" id="status"></div>
    </div>
    <div id="wrapper">
        <!-- SideMenu -->
        <?php $this->load->view('Html/include/SideMenu.php'); ?>
        <!-- End of SideMenu -->
        <div id="page-wrapper" class="gray-bg">
            <!-- Header -->
            <?php $this->load->view('Html/include/Header.php'); ?>
            <!-- End of Header -->

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">

                    <div class="col-lg-8">
                        <div class="text-center">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm">
                                        <div class="card border-left-success shadow h-100 py-2">
                                            <div class="card-body">
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col mr-2">
                                                        <div class="h4 text font-weight-bold text-success text-uppercase mb-2"><?= $this->lang->line("country_th"); ?></div>
                                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $CountAllData[0]->count_item_thai; ?> <?= $this->lang->line("unit"); ?></div>
                                                    </div>
                                                    <div class="col-auto">
                                                        <i class="fas fa-landmark fa-2x text-gray-300"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm">
                                        <div class="card border-left-success shadow h-100 py-2">
                                            <div class="card-body">
                                                <div class="row no-gutters align-items-center">
                                                    <div class="col mr-2">
                                                        <div class="h4 text font-weight-bold text-success text-uppercase mb-2"><?= $this->lang->line("country_lao"); ?></div>
                                                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?php echo $CountAllData[0]->count_item_laos; ?> <?= $this->lang->line("unit"); ?></div>
                                                    </div>
                                                    <div class="col-auto">
                                                        <i class="fas fa-landmark fa-2x text-gray-300"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-xl-8 ">

                                <div class="">
                                    <!-- Card Body -->
                                    <div class="card-body">
                                        <div class="form-group  row">
                                            <div class="col-sm-12">
                                                <select data-placeholder="Choose a Provinces..." class="chosen-select provinces" tabindex="1" id="provinces">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="card card-box-item border-left-success">
                                                <div class="no-gutters align-items-center" style="padding: 15px;">
                                                    <div class="col mr-2">
                                                        <div class="h5 font-weight-bold text text-uppercase mb-2"><?= $this->lang->line("sidebar_menu_Attractions"); ?></div>
                                                    </div>
                                                    <div class="h6 mb-0 font-weight-bold text-gray-800 text-right">
                                                        <span id="count_item_Attractions">
                                                            <?php echo $CountAllData[0]->count_item_Attractions; ?>
                                                        </span>
                                                        <?= $this->lang->line("unit"); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card card-box-item border-left-success">
                                                <div class="no-gutters align-items-center" style="padding: 15px;">
                                                    <div class="col mr-2">
                                                        <div class="h5 font-weight-bold text text-uppercase mb-2"><?= $this->lang->line("sidebar_menu_Restaurant"); ?></div>
                                                    </div>
                                                    <div class="h6 mb-0 font-weight-bold text-gray-800 text-right">
                                                        <span id="count_item_Restaurant">
                                                            <?php echo $CountAllData[0]->count_item_Restaurant; ?>
                                                        </span>

                                                        <?= $this->lang->line("unit"); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card card-box-item border-left-success">
                                                <div class="no-gutters align-items-center" style="padding: 15px;">
                                                    <div class="col mr-2">
                                                        <div class="h5 font-weight-bold text text-uppercase mb-2"><?= $this->lang->line("sidebar_menu_Shopping"); ?></div>
                                                    </div>
                                                    <div class="h6 mb-0 font-weight-bold text-gray-800 text-right">
                                                        <span id="count_item_Shopping">
                                                            <?php echo $CountAllData[0]->count_item_Shopping; ?>
                                                        </span>
                                                        <?= $this->lang->line("unit"); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card card-box-item border-left-success">
                                                <div class="no-gutters align-items-center" style="padding: 15px;">
                                                    <div class="col mr-2">
                                                        <div class="h5 font-weight-bold text text-uppercase mb-2"><?= $this->lang->line("sidebar_menu_Hotel"); ?></div>
                                                    </div>
                                                    <div class="h6 mb-0 font-weight-bold text-gray-800 text-right">
                                                        <span id="count_item_Hotel">
                                                            <?php echo $CountAllData[0]->count_item_Hotel; ?>
                                                        </span>
                                                        <?= $this->lang->line("unit"); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card card-box-item border-left-success">
                                                <div class="no-gutters align-items-center" style="padding: 15px;">
                                                    <div class="col mr-2">
                                                        <div class="h5 font-weight-bold text text-uppercase mb-2"><?= $this->lang->line("sidebar_menu_ImportantContactPlaces"); ?></div>
                                                    </div>
                                                    <div class="h6 mb-0 font-weight-bold text-gray-800 text-right">
                                                        <span id="count_item_ImportantContactPlaces">
                                                            <?php echo $CountAllData[0]->count_item_ImportantContactPlaces; ?>
                                                        </span>
                                                        <?= $this->lang->line("unit"); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!-- Pie Chart -->
                            <div class="col-xl-4 col-lg-5">
                                <div class="card shadow mb-4">
                                    <!-- Card Header - Dropdown -->
                                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                        <h3 class="m-0 font-weight-bold text-success"><?= $this->lang->line("head_main_pic_chart"); ?></h6>
                                    </div>
                                    <!-- Card Body -->
                                    <div class="card-body">
                                        <div class="chart-pie pt-4 pb-2">
                                            <canvas id="myPieChart"></canvas>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12 col-lg-7">
                        <div class="card shadow mb-4">
                            <div class="chart-area">
                                <canvas id="myBarChart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php $this->load->view('Html/include/Footer.php'); ?>
            <!-- End of Footer -->
        </div>
    </div>
    <?php $this->load->view('Html/include/Footer_pg_js.php'); ?>
    <script>
        var host = "<?php echo $host; ?>html";
        var MenuName = "Home";
        var chart_bar_from_php, name_chart_bar_from_php, chart_pie_name_from_php, chart_pie_value_from_php;

        var chart_bar = [],
            name_chart_bar = [],
            chart_pie_name = [],
            chart_pie_value = []

        var chart_pie_color = [];

        chart_bar_from_php = "<?php
                                $chart_bar_value = [];
                                for ($i = 0; $i < count($CountAllData[0]->CountSubCategory); $i++) {
                                    $chart_bar_value[] = $CountAllData[0]->CountSubCategory[$i]->count_item;
                                }
                                echo join(",", $chart_bar_value);
                                ?>;";
        chart_bar = chart_bar_from_php.split(',');

        name_chart_bar_from_php = "<?php
                                    $chart_bar_name = [];
                                    for ($i = 0; $i < count($CountAllData[0]->CountSubCategory); $i++) {
                                        $chart_bar_name[] = (strcmp($this->session->userdata('lang'), "EN") == 0 ? $CountAllData[0]->CountSubCategory[$i]->subcategory_english : $CountAllData[0]->CountSubCategory[$i]->subcategory_thai);
                                    }
                                    echo join(",", $chart_bar_name);
                                    ?>";
        name_chart_bar = name_chart_bar_from_php.split(',');

        chart_pie_name_from_php = "<?php
                                    $chart_pie_name = [];
                                    for ($i = 0; $i < count($CountAllData[0]->CounProvinces); $i++) {
                                        $chart_pie_name[] = (strcmp($this->session->userdata('lang'), "EN") == 0 ? $CountAllData[0]->CounProvinces[$i]->provinces_english : $CountAllData[0]->CounProvinces[$i]->provinces_thai);
                                    }
                                    echo join(",", $chart_pie_name);
                                    ?>";
        chart_pie_name = chart_pie_name_from_php.split(',');

        chart_pie_value_from_php = "<?php
                                    $chart_pie_value = [];
                                    for ($i = 0; $i < count($CountAllData[0]->CounProvinces); $i++) {
                                        $chart_pie_value[] = $CountAllData[0]->CounProvinces[$i]->count_item;
                                    }
                                    echo join(",", $chart_pie_value);
                                    ?>";
        chart_pie_value = chart_pie_value_from_php.split(',');

        var unit = "จำนวน";

        chart_pie_name.forEach(function(item) {
            chart_pie_color.push(getRandomColor());
        });

        function getRandomColor() {
            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }
        $(window).on('load', function() {
            $(".status").fadeOut();
            $(".preloader").fadeOut("slow");
        });


        var myBarChart, myPieChart;
    </script>

    <script src="<?php echo $host; ?>assets/js/plugins/chart.js/Chart.min.js"></script>
    <script src="<?php echo $host; ?>assets/js/chart-bar.js"></script>
    <script src="<?php echo $host; ?>assets/js/chart-pie.js"></script>

    <script src="<?php echo $host; ?>assets/js/sb-admin-2.js"></script>
    <script src="<?php echo $host; ?>assets/js/custom_js/GlobalFunction.js"></script>
    <script src="<?php echo $host; ?>assets/js/custom_js/GoogleMaps.js"></script>
    <script src="<?php echo $host; ?>assets/js/custom_js/Main.js"></script>

</body>

</html>