<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Content | Management System</title>
    <?php $this->load->view('Html/include/Header_css.php'); ?>
</head>

<body>
    <!-- Preloader -->
    <div class="preloader" id="preloader">
        <div class="status" id="status"></div>
    </div>
    <div id="wrapper">
        <!-- SideMenu -->
        <?php $this->load->view('Html/include/SideMenu.php'); ?>
        <!-- End of SideMenu -->
        <div id="page-wrapper" class="gray-bg">
            <!-- Header -->
            <?php $this->load->view('Html/include/Header.php'); ?>
            <!-- End of Header -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2><?= $this->lang->line("sidebar_menu_ProgramTour"); ?></h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="../Main">Home</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong><?= $this->lang->line("sidebar_menu_ProgramTour"); ?></strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox ">
                            <div class="ibox-title">

                                <div class="ibox-tools">
                                    <a href="./ProgramTour/addProgramTour" class="btn btn-primary btn-xs"><?= $this->lang->line("add_tourism_program"); ?></a>
                                    <!-- <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i> -->
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="row m-b-sm m-t-sm">
                                    <div class="col-md-3">
                                        <select data-placeholder="Choose a Category..." class="chosen-select programtour" tabindex="1" name="subcategory" id="subcategory">

                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <select data-placeholder="Choose a Num Dates Trip..." class="chosen-select" tabindex="2" name="num_dates_trip" id="num_dates_trip">

                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <input type="text" id="CustomSearchBox" placeholder="Search" class="form-control-sm form-control">
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-sm btn-primary"> Go!</button>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <table class="footable table table-hover" data-page-size="8" data-filter=#filter style="width:100%" id="ProgramTourTable">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th><?= $this->lang->line("category"); ?></th>
                                            <th><?= $this->lang->line("day_period"); ?></th>
                                            <th><?= $this->lang->line("travel_program_name"); ?></th>
                                            <th><?= $this->lang->line("scope"); ?></th>
                                            <th><?= $this->lang->line("status"); ?></th>
                                            <th><?= $this->lang->line("update_date"); ?></th>
                                            <th class="text-right"><?= $this->lang->line("tools"); ?></th>
                                            <th>id</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Footer -->
            <?php $this->load->view('Html/include/Footer.php'); ?>
            <!-- End of Footer -->
            <!-- ViewItemsModal -->
            <?php $this->load->view('Html/include/ViewItemsModal.php'); ?>
            <!-- End of ViewItemsModal -->
        </div>
    </div>
    <?php $this->load->view('Html/include/Footer_js.php'); ?>
    <script>
        var host = "<?php echo $host; ?>html"
        var table;
        var MenuName = "ProgramTour";
        var TypeMenu = "ProgramTour";
        var type_page = "html";
        var UseGoogleMap = '<?php echo $ENVIRONMENT ?>';
    </script>

    <script src="<?php echo $host; ?>assets/js/custom_js/getProgramTourData.js"></script>
    <script src="<?php echo $host; ?>assets/js/custom_js/GoogleMaps.js"></script>
    <script src="<?php echo $host; ?>assets/js/custom_js/GlobalFunction.js"></script>
    <script src="<?php echo $host; ?>assets/js/custom_js/LocationManagement.js"></script>
    <script src="<?php echo $host; ?>assets/js/custom_js/ProgramTourView.js"></script>

</body>

</html>