<!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Content | Management System</title>
        <?php $this->load->view('Html/include/Header_css.php');?>  
    </head>

    <body>
        <!-- Preloader -->
        <div class="preloader" id="preloader">
            <div class="status" id="status"></div>
        </div>

        <div id="wrapper">
            <!-- SideMenu -->
            <?php $this->load->view('Html/include/SideMenu.php');?>
            <!-- End of SideMenu -->

            <div id="page-wrapper" class="gray-bg">
                <!-- Header -->
                <?php $this->load->view('Html/include/Header.php');?>
                <!-- End of Header -->
                <div class="row wrapper border-bottom white-bg page-heading">
                    <div class="col-lg-10">
                        <br>
                        <div class="forum-icon">
                            <i style="color: #1AB394;" class="fa fa-calendar"></i>
                        </div>
                        <a href="/dasta_thailand/html/EntrepreneurApproval/EntrepreneurApprovalShoppingView" class="forum-item-title" style="color: #1AB394;">สถานประกอบกิจการบริการแหล่งท่องเที่ยว</a>
                        <div class="forum-sub-title">ผู้ประกอบการบริการช้อปปิ้งของที่ระลึกที่ลงทะเบียนเข้ามาในระบบ </div>
                    </div>
                </div>
                <div class="wrapper wrapper-content animated fadeInRight">
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- CountApprovalItemState  -->
                            <div class="ibox-content m-b-sm border-bottom">
                                <div class="forum-item">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <h2><?=$this->lang->line("sidebar_menu_Approval");?></h2>
                                            <ol class="breadcrumb">
                                                <li class="breadcrumb-item">
                                                    <a href="/dasta_thailand/html/Main">Home</a>
                                                </li>
                                                <li class="breadcrumb-item">
                                                    <?=$this->lang->line("sidebar_menu_Approval");?>
                                                </li>
                                                <li class="breadcrumb-item active">
                                                    <strong>สถานประกอบกิจการช้อปปิ้งของที่ระลึกที่เข้ามาในระบบ</strong>
                                                </li>
                                                <li class="breadcrumb-item">
                                                    <?php 
                                                        if(intval($ApprovalUserState["Wait"]) == 0)
                                                            echo "<span class=\"\" id=\"ApprovalUserState\"></span>";
                                                        else
                                                            echo "<span class=\"badge badge-warning\" id=\"ApprovalUserState\">".$ApprovalUserState["Wait"]." การรอ</span>";
                                                        
                                                    ?>
                                                </li>
                                            </ol>
                                        </div>

                                        <!-- ApprovalStateBar -->
                                        <?php $this->load->view('Html/include/ApprovalStateBar.php');?>
                                        <!-- End of ApprovalStateBar -->

                                    </div>
                                </div>
                            </div>
                            <!-- CountApprovalItemState  -->
                            <div class="row">
                                <div class="col-sm-7">
                                    <div class="ibox">
                                        <div class="ibox-title">
                                            <h5 id="header_table_text">การอนุมัติ/โค๊ดส่วนลด</h5>
                                            <div class="ibox-tools">
                                                <h5 id="header_count_list"></h5>
                                            </div>
                                        </div>
                                        <div class="ibox-content">
                                            <!-- ApprovalSearchBox -->
                                            <?php $this->load->view('Html/include/ApprovalSearchBox.php');?>
                                            <!-- End of ApprovalSearchBox -->
                                
                                            <div class="project-list">
                                                <table class="footable table table-hover table-striped nowrap" data-page-size="8" data-filter=#filter style="width:100%" id="ApprovalTable">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!-- View -->
                                <div class="col-sm-5">
                                    <div class="ibox selected">
		                                <div class="ibox-content">
                                            <div class="tab-content">
				                                <div id="" class="">
                                                    <div class="client-detail">
                                                        <div class="full-height-scroll" id="user_content">
                                                            <div class="panel-body" >
                                                                <div class="ibox-content">
                                                                    <img class="d-block w-100" src="<?php echo $host;?>assets/img/p3.jpg" alt="" id="crop_img">  
                                                                </div>
                                                                <div class="ibox-content">
                                                                    <hr class="hr-line-solid">
                                                                    <h3><span style="color: #1AB394;" id="business_nameThai"></span></h3>
                                                                    <p >ที่อยู่สถานประกอบการ : <span id="business_presentAddress"></span></p>
                                                                    <p>ประเภทกลุ่มผู้ประกอบการ : <span id="business_type_category_id"></span></p>
                                                                    <p>หมายเลขโทรศัพท์ (ของสถานประกอบการ) : <span id="business_phone"></span></p>
                                                                    <p>เว็บไซต์ : <span id="business_www"></span></p>
                                                                    <p>เฟซบุ๊ก :  <span id="business_facebook"></span></p>
                                                                    <p>อีเมล : <span id="user_email"></span></p>
                                                                    <p>ใบอนุญาตประกอบธุรกิจ : </p>
                                                                    <hr class="hr-line-solid">
                                                                    <h3>ข้อมูลผู้ลงทะเบียน</h3>
                                                                    <p>ชื่อ : <span id="user_firstName"></span></p>
                                                                    <p>นามสกุล : <span id="user_lastName"></span></p>
                                                                    <p>หมายเลขโทรศัพท์ : <span id="user_phone"></span></p>
                                                                    <p>ชื่อผู้ใช้ : <span id="user_account"></span></p>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <div class="text-right" id="box_btn_approve">
                                                                <button class="btn btn-primary btn-xl btn-item-approve" value="" id="btn_approve">อนุมัติ</button>
                                                                <button class="btn btn-danger btn-xl btn-item-disapprove" value="" id="btn_disapprove">ไม่อนุมัติ</button>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- View -->
                                
                            </div>
                        </div>
                    </div>
                </div>



                <!-- Footer -->
                <?php $this->load->view('Html/include/Footer.php');?>
                <!-- End of Footer -->
            </div>
        </div>
        <?php $this->load->view('Html/include/Footer_js.php');?>
       
        <script>
            var host = "<?php echo $host;?>html";
            var table;
            var map;
            var cropper;
            var gmarkers = [];

            var MenuName = "EntrepreneurApprovalShopping";
            var TypeMenu = "EntrepreneurApproval";
           
            var UseGoogleMap = '<?php echo $ENVIRONMENT?>';

            var ApprovalItem ="<?php echo $Approval_Item["Attractions"]?>";

            document.getElementById("state_all").textContent= "<?php echo $ApprovalUserState["All"];?>"
            document.getElementById("state_approved").textContent= "<?php echo $ApprovalUserState["Approved"];?>"
            document.getElementById("state_wait").textContent= "<?php echo $ApprovalUserState["Wait"];?>"
            document.getElementById("state_refuse").textContent= "<?php echo $ApprovalUserState["Refuse"];?>"

        </script>
        <script src="<?php echo $host;?>assets/js/custom_js/getUserData.js"></script>
        <script src="<?php echo $host;?>assets/js/custom_js/GoogleMaps.js"></script>
        <script src="<?php echo $host;?>assets/js/custom_js/GlobalFunction.js"></script>
        <script src="<?php echo $host;?>assets/js/custom_js/EntrepreneurApproval/EntrepreneurApprovalShoppingView.js"></script>
        
    </body>
</html>