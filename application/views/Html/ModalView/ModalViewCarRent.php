<div class="tabs-container">
    <ul class="nav nav-tabs" role="tablist">
        <li>
            <a class="nav-link active" data-toggle="tab" href="#tab-1" id="tabTH"> <?=$this->lang->line("lang_th");?></a>
        </li>
        <li>
            <a class="nav-link" data-toggle="tab" href="#tab-2" id="tabENG"><?=$this->lang->line("lang_en");?></a>
        </li>
        <li>
            <a class="nav-link" data-toggle="tab" href="#tab-3" id="tabCHN"><?=$this->lang->line("lang_chn");?></a>
        </li>
        <li>
            <a class="nav-link" data-toggle="tab" href="#tab-4" id="tabLAOS"><?=$this->lang->line("lang_lao");?></a>
        </li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" id="tab-1" class="tab-pane active">
            <div class="panel-body">
                <div class="ibox-content">
                    <img class="d-block w-100" src="<?php echo $host;?>assets/img/p3.jpg" alt="" id="coverItem_paths_Thai">  
                </div>
                <div class="ibox-content">
                    <hr class="hr-line-solid">
                    <h4><span id="itmes_topicThai"></span></h4>
                    <p><span style="color: #1AB394;" id="business_nameThai"></span><p>
                    <p>ประเภท : <span id="typesofCars_Thai"> อีโคคาร์</span></p>
                    <p>เกียร์ : <span id="gearSystem_Thai"> อีโคคาร์</span></p>
                    <p>โดยสาร : <span id="items_carSeatsThai"> 5 </span> คน</p>
                    <hr class="hr-line-solid">
                    <h4>รายละเอียดราคา</h4>
                    <p>ราคาต่อวัน THB <span id="items_PricePerDayThai"></span>.-</p>
                    <p>ราคาค่าส่งรถ THB <span id="items_CarDeliveryPriceThai"></span>.-</p>
                    <p>ราคาค่ารับรถ THB <span id="items_PickUpPriceThai"></span>.-</p>
                    <hr class="hr-line-solid">
                    <h4>รายละเอียดค่ามัดจำเพื่อประกันความเสียหาย</h4>
                    <p>ค่ามัดจำ THB <span id="items_DepositPriceThai"> </span>.-</p>
                    <p>รวมประกันภัยพื้นฐาน : <span id="items_isBasicInsuranceThai"> </span></p>
                    <p>รายละเอียดประกันภัยพื้นฐาน </p>
                    <span id="items_BissicInsurance_detailThai"> </span>
                    <hr class="hr-line-solid">
                    <div id="BookingConditionThai0">
                        <!--DATA  -->
                    </div>
                    <hr class="hr-line-solid"> 
                    <h4>รายละเอียดเอกสารสำหรับการเช่ารถ (เพิ่มเป็นข้อ) </h4>
                    <div id="BookingConditionThai">
                        <!--DATA  -->
                    </div>
                    <hr class="hr-line-solid">
                </div>
            </div>
        </div>
        <div role="tabpanel" id="tab-2" class="tab-pane">
            <div class="panel-body">
                <div class="ibox-content">
                    <img class="d-block w-100" src="<?php echo $host;?>assets/img/p3.jpg" alt="" id="coverItem_paths_English">  
                </div>
                <div class="ibox-content">
                    <hr class="hr-line-solid">
                    <h4><span id="itmes_topicEnglish"></span></h4>
                    <p><span style="color: #1AB394;" id="business_nameEnglish"></span><p>
                    <p>ประเภท : <span id="typesofCars_English"> อีโคคาร์</span></p>
                    <p>เกียร์ : <span id="gearSystem_English"> อีโคคาร์</span></p>
                    <p>โดยสาร : <span id="items_carSeatsEnglish"> 5 </span> คน</p>
                    <hr class="hr-line-solid">
                    <h4>รายละเอียดราคา</h4>
                    <p>ราคาต่อวัน THB <span id="items_PricePerDayEnglish"></span>.-</p>
                    <p>ราคาค่าส่งรถ THB <span id="items_CarDeliveryPriceEnglish"></span>.-</p>
                    <p>ราคาค่ารับรถ THB <span id="items_PickUpPriceEnglish"></span>.-</p>
                    <hr class="hr-line-solid">
                    <h4>รายละเอียดค่ามัดจำเพื่อประกันความเสียหาย</h4>
                    <p>ค่ามัดจำ THB <span id="items_DepositPriceEnglish"> </span>.-</p>
                    <p>รวมประกันภัยพื้นฐาน : <span id="items_isBasicInsuranceEnglish"> </span></p>
                    <p>รายละเอียดประกันภัยพื้นฐาน </p>
                    <span id="items_BissicInsurance_detailEnglish"> </span>
                    <hr class="hr-line-solid">
                    <div id="BookingConditionEnglish0">
                        <!--DATA  -->
                    </div>
                    <hr class="hr-line-solid"> 
                    <h4>รายละเอียดเอกสารสำหรับการเช่ารถ (เพิ่มเป็นข้อ) </h4>
                    <div id="BookingConditionEnglish">
                        <!--DATA  -->
                    </div>
                    <hr class="hr-line-solid">
                </div>
            </div>
        </div>
        <div role="tabpanel" id="tab-3" class="tab-pane">
            <div class="panel-body">
                <div class="ibox-content">
                    <img class="d-block w-100" src="<?php echo $host;?>assets/img/p3.jpg" alt="" id="coverItem_paths_Chinese">  
                </div>
                <div class="ibox-content">
                    <hr class="hr-line-solid">
                    <h4><span id="itmes_topicChinese"></span></h4>
                    <p><span style="color: #1AB394;" id="business_nameChinese"></span><p>
                    <p>ประเภท : <span id="typesofCars_Chinese"> อีโคคาร์</span></p>
                    <p>เกียร์ : <span id="gearSystem_Chinese"> อีโคคาร์</span></p>
                    <p>โดยสาร : <span id="items_carSeatsChinese"> 5 </span> คน</p>
                    <hr class="hr-line-solid">
                    <h4>รายละเอียดราคา</h4>
                    <p>ราคาต่อวัน THB <span id="items_PricePerDayChinese"></span>.-</p>
                    <p>ราคาค่าส่งรถ THB <span id="items_CarDeliveryPriceChinese"></span>.-</p>
                    <p>ราคาค่ารับรถ THB <span id="items_PickUpPriceChinese"></span>.-</p>
                    <hr class="hr-line-solid">
                    <h4>รายละเอียดค่ามัดจำเพื่อประกันความเสียหาย</h4>
                    <p>ค่ามัดจำ THB <span id="items_DepositPriceChinese"> </span>.-</p>
                    <p>รวมประกันภัยพื้นฐาน : <span id="items_isBasicInsuranceChinese"> </span></p>
                    <p>รายละเอียดประกันภัยพื้นฐาน </p>
                    <span id="items_BissicInsurance_detailChinese"> </span>
                    <hr class="hr-line-solid">
                    <div id="BookingConditionChinese0">
                        <!--DATA  -->
                    </div>
                    <hr class="hr-line-solid"> 
                    <h4>รายละเอียดเอกสารสำหรับการเช่ารถ (เพิ่มเป็นข้อ) </h4>
                    <div id="BookingConditionChinese">
                        <!--DATA  -->
                    </div>
                    <hr class="hr-line-solid">
                </div>
            </div>
        </div> 
        <div role="tabpanel" id="tab-4" class="tab-pane">
            <div class="panel-body">
                <div class="ibox-content">
                    <img class="d-block w-100" src="<?php echo $host;?>assets/img/p3.jpg" alt="" id="coverItem_paths_Laos">  
                </div>
                <div class="ibox-content">
                    <hr class="hr-line-solid">
                    <h4><span id="itmes_topicLaos"></span></h4>
                    <p><span style="color: #1AB394;" id="business_nameLaos"></span><p>
                    <p>ประเภท : <span id="typesofCars_Laos"> อีโคคาร์</span></p>
                    <p>เกียร์ : <span id="gearSystem_Laos"> อีโคคาร์</span></p>
                    <p>โดยสาร : <span id="items_carSeatsLaos"> 5 </span> คน</p>
                    <hr class="hr-line-solid">
                    <h4>รายละเอียดราคา</h4>
                    <p>ราคาต่อวัน THB <span id="items_PricePerDayLaos"></span>.-</p>
                    <p>ราคาค่าส่งรถ THB <span id="items_CarDeliveryPriceLaos"></span>.-</p>
                    <p>ราคาค่ารับรถ THB <span id="items_PickUpPriceLaos"></span>.-</p>
                    <hr class="hr-line-solid">
                    <h4>รายละเอียดค่ามัดจำเพื่อประกันความเสียหาย</h4>
                    <p>ค่ามัดจำ THB <span id="items_DepositPriceLaos"> </span>.-</p>
                    <p>รวมประกันภัยพื้นฐาน : <span id="items_isBasicInsuranceLaos"> </span></p>
                    <p>รายละเอียดประกันภัยพื้นฐาน </p>
                    <span id="items_BissicInsurance_detailLaos"> </span>
                    <hr class="hr-line-solid">
                    <div id="BookingConditionLaos0">
                        <!--DATA  -->
                    </div>
                    <hr class="hr-line-solid"> 
                    <h4>รายละเอียดเอกสารสำหรับการเช่ารถ (เพิ่มเป็นข้อ) </h4>
                    <div id="BookingConditionLaos">
                        <!--DATA  -->
                    </div>
                    <hr class="hr-line-solid">
                </div>
            </div>
        </div>
    </div>
</div>