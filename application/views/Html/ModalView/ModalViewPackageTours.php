<div class="tabs-container">
    <ul class="nav nav-tabs" role="tablist">
        <li>
            <a class="nav-link active" data-toggle="tab" href="#tab-1" id="tabTH"> <?=$this->lang->line("lang_th");?></a>
        </li>
        <li>
            <a class="nav-link" data-toggle="tab" href="#tab-2" id="tabENG"><?=$this->lang->line("lang_en");?></a>
        </li>
        <li>
            <a class="nav-link" data-toggle="tab" href="#tab-3" id="tabCHN"><?=$this->lang->line("lang_chn");?></a>
        </li>
        <li>
            <a class="nav-link" data-toggle="tab" href="#tab-4" id="tabLAOS"><?=$this->lang->line("lang_lao");?></a>
        </li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" id="tab-1" class="tab-pane active">
            <div class="panel-body">
                <div class="ibox-content">
                    <div id="owl_demoThai" class="owl-carousel owl-theme">
                        <!--DATA  -->
                    </div>
                </div> 
                <div class="ibox-content">
                    <!-- <hr class="hr-line-solid"> -->
                    <h4><span id="itmes_topicThai"></span></h4>
                    <p><span style="color: #1AB394;" id="business_nameThai"></span><p>
                    <p>ขอบเขตจังหวัด : <span id="ScopeAreaThai"></span></p>
                    <p><span id="category_Thai"></span></p>
                    
                    <h4>THB <span id="travelPeriod_adult_special_price_Thai"> </span>.-</h4>
                    <hr class="hr-line-solid">
                    <h4>รายละเอียดการเดินทาง</h4>
                    <span>ระยะเวลา : <span id="travel_time_Thai"></span> วัน</span>
                    
                    <div id="TravelDetailsThai">
                        <!--DATA  -->
                    </div>
                    <hr class="hr-line-solid">
                    <h4>รายละเอียดช่วงเวลาตารางการเดินทาง</h4>
                    <!-- <table class="table table-bordered TravelPeriodThai" id="TravelPeriodThai"> -->
                    <table class="footable table table-hover table-striped nowrap TravelPeriod" id="TravelPeriodThai">
                        <thead>
                            <tr>
                                <th>-</th>
                                <th>จำนวนที่สามารถจองได้</th>
                                <th>วันเดินทางไป-กลับ</th>
                                <th>ผู้ใหญ่ พัก 2-3 คน</th>
                                <th>ผู้ใหญ่ พักเดี่ยว</th>
                                <th>เด็ก เพิ่มเตียง</th>
                                <th>เด็ก ไม่เพิ่มเตียง</th>
                                
                            </tr>
                        </thead>
                        <tbody id="tbody_TravelPeriodThai">
                            <!--DATA  -->
                        </tbody>
                    </table>
                    <hr class="hr-line-solid">
                    <h4>ที่พักโรงแรม</h4>
                    <table class="footable table table-hover table-striped nowrap hotel" id="hotelThai">
                        <thead>
                            <tr>
                                <th>วันที่</th>
                                <th>สถานที่พัก</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_hotelThai">
                            <!--DATA  -->
                        </tbody>
                    </table>
                    <hr class="hr-line-solid">
                    <h4>อาหาร</h4>
                    <table class="footable table table-hover table-striped nowrap food" id="foodThai">
                        <thead>
                            <tr>
                                <th>วันที่</th>
                                <th>เช้า</th>
                                <th>กลางวัน</th>
                                <th>เย็น</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_foodThai">
                            <!--DATA  -->
                        </tbody>
                    </table>
                    <hr class="hr-line-solid">
                    <h4>เงื่อนไขในการจอง</h4>
                    <div id="BookingConditionThai">
                        <!--DATA  -->
                    </div>
                    
                </div>
            </div>
        </div>
        <div role="tabpanel" id="tab-2" class="tab-pane">
            <div class="panel-body">
                <div class="ibox-content">
                    <div id="owl_demoEnglish" class="owl-carousel owl-theme">
                        <!--DATA  -->
                    </div>
                </div> 
                <div class="ibox-content">
                    <!-- <hr class="hr-line-solid"> -->
                    <h4><span id="itmes_topicEnglish"></span></h4>
                    <p><span style="color: #1AB394;" id="business_nameEnglish"></span><p>
                    <p>ขอบเขตจังหวัด : <span id="ScopeAreaEnglish"></span></p>
                    <p><span id="category_English"></span></p>
                    <h4>THB <span id="travelPeriod_adult_special_price_English"> </span>.-</h4>
                    <hr class="hr-line-solid">
                    <h4>รายละเอียดการเดินทาง</h4>
                    <span>ระยะเวลา : <span id="travel_time_English"></span> วัน</span>
                    
                    <div id="TravelDetailsEnglish">
                        <!--DATA  -->
                    </div>
                    <hr class="hr-line-solid">
                    <h4>รายละเอียดช่วงเวลาตารางการเดินทาง</h4>
                    <table class="footable table table-hover table-striped nowrap TravelPeriod" id="TravelPeriodEnglish">
                        <thead>
                            <tr>
                                <th>-</th>
                                <th>จำนวนที่สามารถจองได้</th>
                                <th>วันเดินทางไป-กลับ</th>
                                <th>ผู้ใหญ่ พัก 2-3 คน</th>
                                <th>ผู้ใหญ่ พักเดี่ยว</th>
                                <th>เด็ก เพิ่มเตียง</th>
                                <th>เด็ก ไม่เพิ่มเตียง</th>
                                
                            </tr>
                        </thead>
                        <tbody id="tbody_TravelPeriodEnglish">
                            <!--DATA  -->
                        </tbody>
                    </table>
                    <hr class="hr-line-solid">
                    <h4>ที่พักโรงแรม</h4>
                    <table class="footable table table-hover table-striped nowrap hotel" id="hotelEnglish">
                        <thead>
                            <tr>
                                <th>วันที่</th>
                                <th>สถานที่พัก</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_hotelEnglish">
                            <!--DATA  -->
                        </tbody>
                    </table>
                    <hr class="hr-line-solid">
                    <h4>อาหาร</h4>
                    <table class="footable table table-hover table-striped nowrap food" id="foodEnglish">
                        <thead>
                            <tr>
                                <th>วันที่</th>
                                <th>เช้า</th>
                                <th>กลางวัน</th>
                                <th>เย็น</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_foodEnglish">
                            <!--DATA  -->
                        </tbody>
                    </table>
                    <hr class="hr-line-solid">
                    <h4>เงื่อนไขในการจอง</h4>
                    <div id="BookingConditionEnglish">
                        <!--DATA  -->
                    </div>
                    
                </div>
            </div>
        </div>
        <div role="tabpanel" id="tab-3" class="tab-pane">
            <div class="panel-body">
                <div class="ibox-content">
                    <div id="owl_demoChinese" class="owl-carousel owl-theme">
                        <!--DATA  -->
                    </div>
                </div> 
                <div class="ibox-content">
                    <!-- <hr class="hr-line-solid"> -->
                    <h4><span id="itmes_topicChinese"></span></h4>
                    <p><span style="color: #1AB394;" id="business_nameChinese"></span><p>
                    <p>ขอบเขตจังหวัด : <span id="ScopeAreaChinese"></span></p>
                    <p><span id="category_Chinese"></span></p>
                    
                    <h4>THB <span id="travelPeriod_adult_special_price_Chinese"> </span>.-</h4>
                    <hr class="hr-line-solid">
                    <h4>รายละเอียดการเดินทาง</h4>
                    <span>ระยะเวลา : <span id="travel_time_Chinese"></span> วัน</span>
                    
                    <div id="TravelDetailsChinese">
                        <!--DATA  -->
                    </div>
                    <hr class="hr-line-solid">
                    <h4>รายละเอียดช่วงเวลาตารางการเดินทาง</h4>
                    <table class="footable table table-hover table-striped nowrap TravelPeriod" id="TravelPeriodChinese">
                        <thead>
                            <tr>
                                <th>-</th>
                                <th>จำนวนที่สามารถจองได้</th>
                                <th>วันเดินทางไป-กลับ</th>
                                <th>ผู้ใหญ่ พัก 2-3 คน</th>
                                <th>ผู้ใหญ่ พักเดี่ยว</th>
                                <th>เด็ก เพิ่มเตียง</th>
                                <th>เด็ก ไม่เพิ่มเตียง</th>
                                
                            </tr>
                        </thead>
                        <tbody id="tbody_TravelPeriodChinese">
                            <!--DATA  -->
                        </tbody>
                    </table>
                    <hr class="hr-line-solid">
                    <h4>ที่พักโรงแรม</h4>
                    <table class="footable table table-hover table-striped nowrap hotel" id="hotelChinese">
                        <thead>
                            <tr>
                                <th>วันที่</th>
                                <th>สถานที่พัก</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_hotelChinese">
                            <!--DATA  -->
                        </tbody>
                    </table>
                    <hr class="hr-line-solid">
                    <h4>อาหาร</h4>
                    <table class="footable table table-hover table-striped nowrap food" id="foodChinese">
                        <thead>
                            <tr>
                                <th>วันที่</th>
                                <th>เช้า</th>
                                <th>กลางวัน</th>
                                <th>เย็น</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_foodChinese">
                            <!--DATA  -->
                        </tbody>
                    </table>
                    <hr class="hr-line-solid">
                    <h4>เงื่อนไขในการจอง</h4>
                    <div id="BookingConditionChinese">
                        <!--DATA  -->
                    </div>
                    
                </div>
            </div>
        </div> 
        <div role="tabpanel" id="tab-4" class="tab-pane">
            <div class="panel-body">
                <div class="ibox-content">
                    <div id="owl_demoLaos" class="owl-carousel owl-theme">
                            <!--DATA  -->
                    </div>
                </div> 
                <div class="ibox-content">
                    <!-- <hr class="hr-line-solid"> -->
                    <h4><span id="itmes_topicLaos"></span></h4>
                    <p><span style="color: #1AB394;" id="business_nameLaos"></span><p>
                    <p>ขอบเขตจังหวัด : <span id="ScopeAreaLaos"></span></p>
                    <p><span id="category_Laos"></span></p>
                    <h4>THB <span id="travelPeriod_adult_special_price_Laos"> </span>.-</h4>
                    <hr class="hr-line-solid">
                    <h4>รายละเอียดการเดินทาง</h4>
                    <span>ระยะเวลา : <span id="travel_time_Laos"></span> วัน</span>
                    
                    <div id="TravelDetailsLaos">
                        <!--DATA  -->
                    </div>
                    <hr class="hr-line-solid">
                    <h4>รายละเอียดช่วงเวลาตารางการเดินทาง</h4>
                    <table class="footable table table-hover table-striped nowrap TravelPeriod" id="TravelPeriodLaos">
                        <thead>
                            <tr>
                                <th>-</th>
                                <th>จำนวนที่สามารถจองได้</th>
                                <th>วันเดินทางไป-กลับ</th>
                                <th>ผู้ใหญ่ พัก 2-3 คน</th>
                                <th>ผู้ใหญ่ พักเดี่ยว</th>
                                <th>เด็ก เพิ่มเตียง</th>
                                <th>เด็ก ไม่เพิ่มเตียง</th>
                                
                            </tr>
                        </thead>
                        <tbody id="tbody_TravelPeriodLaos">
                            <!--DATA  -->
                        </tbody>
                    </table>
                    <hr class="hr-line-solid">
                    <h4>ที่พักโรงแรม</h4>
                    <table class="footable table table-hover table-striped nowrap hotel" id="hotelLaos">
                        <thead>
                            <tr>
                                <th>วันที่</th>
                                <th>สถานที่พัก</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_hotelLaos">
                            <!--DATA  -->
                        </tbody>
                    </table>
                    <hr class="hr-line-solid">
                    <h4>อาหาร</h4>
                    <table class="footable table table-hover table-striped nowrap food" id="foodLaos">
                        <thead>
                            <tr>
                                <th>วันที่</th>
                                <th>เช้า</th>
                                <th>กลางวัน</th>
                                <th>เย็น</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_foodLaos">
                            <!--DATA  -->
                        </tbody>
                    </table>
                    <hr class="hr-line-solid">
                    <h4>เงื่อนไขในการจอง</h4>
                    <div id="BookingConditionLaos">
                        <!--DATA  -->
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>