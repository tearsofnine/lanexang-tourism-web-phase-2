<div class="tabs-container">
    <ul class="nav nav-tabs" role="tablist">
        <li>
            <a class="nav-link active" data-toggle="tab" href="#tab-1" id="tabTH"> <?=$this->lang->line("lang_th");?></a>
        </li>
        <li>
            <a class="nav-link" data-toggle="tab" href="#tab-2" id="tabENG"><?=$this->lang->line("lang_en");?></a>
        </li>
        <li>
            <a class="nav-link" data-toggle="tab" href="#tab-3" id="tabCHN"><?=$this->lang->line("lang_chn");?></a>
        </li>
        <li>
            <a class="nav-link" data-toggle="tab" href="#tab-4" id="tabLAOS"><?=$this->lang->line("lang_lao");?></a>
        </li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" id="tab-1" class="tab-pane active">
            <div class="panel-body">
                <div class="ibox-content">
                    <img class="d-block w-100" src="<?php echo $host;?>assets/img/p3.jpg" alt="" id="coverItem_paths_Thai">  
                </div>
                <div class="ibox-content">
                    <hr class="hr-line-solid">
                    <h2><span id="programTour_nameThai">เลย 3 วัน 2 คืน สบายสบาย</span></h2>
                    <p><span id="programTour_interestingThingsThai">ทริปนี้เป็นทริปสั้นๆ ราคาไม่แพงมาก แต่ก็ไม่ถูกเกินไป ยิ่งไปหลายคนยิ่งถูกเด้อ เราไปต้น เดือนตุลา ราคาที่พัก\nบนดอยอาจสูงกว่าปกตินิสนึง</span></p>
                    <h4>รายละเอียดโปรแกรมท่องเที่ยว</h4>
                    <p>ช่วงเวลา : <span id="programTour_time_periodThai" > </span></p>
                    <p><span id="category_Thai1" style="color: #133053;">ประเภทโปรแกรมท่องเที่ยว : </span><span id="category_Thai2" > แหล่งท่องเที่ยวตามธรรมชาติ</span></p>
                    <div id="DatesTripScopeThai">
                    <!-- DATA -->
                    </div>
                    <div id="DatesTripThai">
                    <!-- DATA -->
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" id="tab-2" class="tab-pane">
            <div class="panel-body">
                <div class="ibox-content">
                    <img class="d-block w-100" src="<?php echo $host;?>assets/img/p3.jpg" alt="" id="coverItem_paths_English">  
                </div>
                <div class="ibox-content">
                    <hr class="hr-line-solid">
                    <h2><span id="programTour_nameEnglish">เลย 3 วัน 2 คืน สบายสบาย</span></h2>
                    <p><span id="programTour_interestingThingsEnglish">ทริปนี้เป็นทริปสั้นๆ ราคาไม่แพงมาก แต่ก็ไม่ถูกเกินไป ยิ่งไปหลายคนยิ่งถูกเด้อ เราไปต้น เดือนตุลา ราคาที่พัก\nบนดอยอาจสูงกว่าปกตินิสนึง</span></p>
                    <h4>รายละเอียดโปรแกรมท่องเที่ยว</h4>
                    <p>ช่วงเวลา : <span id="programTour_time_periodEnglish" > </span></p>
                    <p><span id="category_English1" style="color: #133053;">ประเภทโปรแกรมท่องเที่ยว : </span><span id="category_English2" > แหล่งท่องเที่ยวตามธรรมชาติ</span></p>
                    <div id="DatesTripScopeEnglish">
                    <!-- DATA -->
                    </div>
                    <div id="DatesTripEnglish">
                    <!-- DATA -->
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" id="tab-3" class="tab-pane">
            <div class="panel-body">
                <div class="ibox-content">
                    <img class="d-block w-100" src="<?php echo $host;?>assets/img/p3.jpg" alt="" id="coverItem_paths_Chinese">  
                </div>
                <div class="ibox-content">
                    <hr class="hr-line-solid">
                    <h2><span id="programTour_nameChinese">เลย 3 วัน 2 คืน สบายสบาย</span></h2>
                    <p><span id="programTour_interestingThingsChinese">ทริปนี้เป็นทริปสั้นๆ ราคาไม่แพงมาก แต่ก็ไม่ถูกเกินไป ยิ่งไปหลายคนยิ่งถูกเด้อ เราไปต้น เดือนตุลา ราคาที่พัก\nบนดอยอาจสูงกว่าปกตินิสนึง</span></p>
                    <h4>รายละเอียดโปรแกรมท่องเที่ยว</h4>
                    <p>ช่วงเวลา : <span id="programTour_time_periodChinese" > </span></p>
                    <p><span id="category_Chinese1" style="color: #133053;">ประเภทโปรแกรมท่องเที่ยว : </span><span id="category_Chinese2" > แหล่งท่องเที่ยวตามธรรมชาติ</span></p>
                    <div id="DatesTripScopeChinese">
                    <!-- DATA -->
                    </div>
                    <div id="DatesTripChinese">
                    <!-- DATA -->
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" id="tab-4" class="tab-pane">
            <div class="panel-body">
                <div class="ibox-content">
                    <img class="d-block w-100" src="<?php echo $host;?>assets/img/p3.jpg" alt="" id="coverItem_paths_Laos">  
                </div>
                <div class="ibox-content">
                    <hr class="hr-line-solid">
                    <h2><span id="programTour_nameLaos">เลย 3 วัน 2 คืน สบายสบาย</span></h2>
                    <p><span id="programTour_interestingThingsLaos">ทริปนี้เป็นทริปสั้นๆ ราคาไม่แพงมาก แต่ก็ไม่ถูกเกินไป ยิ่งไปหลายคนยิ่งถูกเด้อ เราไปต้น เดือนตุลา ราคาที่พัก\nบนดอยอาจสูงกว่าปกตินิสนึง</span></p>
                    <h4>รายละเอียดโปรแกรมท่องเที่ยว</h4>
                    <p>ช่วงเวลา : <span id="programTour_time_periodLaos" > </span></p>
                    <p><span id="category_Laos1" style="color: #133053;">ประเภทโปรแกรมท่องเที่ยว : </span><span id="category_Laos2" > แหล่งท่องเที่ยวตามธรรมชาติ</span></p>
                    <div id="DatesTripScopeLaos">
                    <!-- DATA -->
                    </div>
                    <div id="DatesTripLaos">
                    <!-- DATA -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>