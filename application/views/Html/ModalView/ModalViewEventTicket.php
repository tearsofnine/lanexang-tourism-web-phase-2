<div class="tabs-container">
    <ul class="nav nav-tabs" role="tablist">
        <li>
            <a class="nav-link active" data-toggle="tab" href="#tab-1" id="tabTH"> <?=$this->lang->line("lang_th");?></a>
        </li>
        <li>
            <a class="nav-link" data-toggle="tab" href="#tab-2" id="tabENG"><?=$this->lang->line("lang_en");?></a>
        </li>
        <li>
            <a class="nav-link" data-toggle="tab" href="#tab-3" id="tabCHN"><?=$this->lang->line("lang_chn");?></a>
        </li>
        <li>
            <a class="nav-link" data-toggle="tab" href="#tab-4" id="tabLAOS"><?=$this->lang->line("lang_lao");?></a>
        </li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" id="tab-1" class="tab-pane active">
            <div class="panel-body">
                <div class="ibox-content">
                    <div id="owl_demoThai" class="owl-carousel owl-theme">
                        <!--DATA  -->
                    </div>
                </div> 
                <div class="ibox-content">
                    <h4><span id="itmes_topicThai"></span></h4>
                    <p><span style="color: #1AB394;" id="business_nameThai"></span><p>
                    <p>จังหวัด : <span id="provinces_thai">เชียงใหม่</span></p>
                    <p><span id="category_Thai"></span></p>
                    
                    <h4>ราคาผู้ใหญ่ THB <span id="items_priceguestAdult_Thai"> </span>.-</h4>
                    <h4>ราคาเด็ก THB <span id="items_priceguestChild_Thai"> </span>.-</h4>
                    <hr class="hr-line-solid">
                    <h4>ไฮไลต์</h4>
                    <span id="items_highlightsThai"></span>
                    <hr class="hr-line-solid">
                    <h4>ข้อมูลสถานที่</h4>
                    <span id="items_contactThai"></span>
                    <div>
                        <div id="map"></div>
                    </div>
                    <hr class="hr-line-solid">
                    <h4>รายละเอียดการเปิด-ปิด</h4>
                    <p><span id="item_Open_Close_data_Thai">เปิดให้บริการทุกวัน  17:00 - 03:00 น.</span></p>
                    <h4>รายละเอียดช่วงเวลาทำการ</h4>
                    <table class="table table-bordered" id="TimePeriodThai">
                        <thead>
                            <tr>
                                <th>ช่วงเวลา</th>
                                <th>เปิด </th>
                                <th>ปิด</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_TimePeriodThai">
                            <!--DATA  -->
                        </tbody>
                    </table>
                    <hr class="hr-line-solid">
                    <h4 id="topicdetail_Thai">สิ่งที่จะได้พบ</h4>
                    <p><span id="detail_textThai0"></span></p>
                    <div id="detail_photoThai0">
                    </div>
                    <hr class="hr-line-solid">
                    <h4>เงื่อนไขในการจอง</h4>
                    <div id="BookingConditionThai">
                        <!--DATA  -->
                    </div>
                    
                </div>
            </div>
        </div>
        <div role="tabpanel" id="tab-2" class="tab-pane">
            <div class="panel-body">
                <div class="ibox-content">
                    <div id="owl_demoEnglish" class="owl-carousel owl-theme">
                        <!--DATA  -->
                    </div>
                </div> 
                <div class="ibox-content">
                    <h4><span id="itmes_topicEnglish"></span></h4>
                    <p><span style="color: #1AB394;" id="business_nameEnglish"></span><p>
                    <p>จังหวัด : <span id="provinces_English">เชียงใหม่</span></p>
                    <p><span id="category_English"></span></p>
                    
                    <h4>ราคาผู้ใหญ่ THB <span id="items_priceguestAdult_English"> </span>.-</h4>
                    <h4>ราคาเด็ก THB <span id="items_priceguestChild_English"> </span>.-</h4>
                    <hr class="hr-line-solid">
                    <h4>ไฮไลต์</h4>
                    <span id="items_highlightsEnglish"></span>
                    <hr class="hr-line-solid">
                    <h4>ข้อมูลสถานที่</h4>
                    <span id="items_contactEnglish"></span>
                    <div>
                        <div id="map"></div>
                    </div>
                    <hr class="hr-line-solid">
                    <h4>รายละเอียดการเปิด-ปิด</h4>
                    <p><span id="item_Open_Close_data_English">เปิดให้บริการทุกวัน  17:00 - 03:00 น.</span></p>
                    <h4>รายละเอียดช่วงเวลาทำการ</h4>
                    <table class="table table-bordered" id="TimePeriodEnglish">
                        <thead>
                            <tr>
                                <th>ช่วงเวลา</th>
                                <th>เปิด </th>
                                <th>ปิด</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_TimePeriodEnglish">
                            <!--DATA  -->
                        </tbody>
                    </table>
                    <hr class="hr-line-solid">
                    <h4 id="topicdetail_English">สิ่งที่จะได้พบ</h4>
                    <p><span id="detail_textEnglish0"></span></p>
                    <div id="detail_photoEnglish0">
                    </div>
                    <hr class="hr-line-solid">
                    <h4>เงื่อนไขในการจอง</h4>
                    <div id="BookingConditionEnglish">
                        <!--DATA  -->
                    </div>
                    
                </div>
            </div>
        </div>
        <div role="tabpanel" id="tab-3" class="tab-pane">
            <div class="panel-body">
                <div class="ibox-content">
                    <div id="owl_demoChinese" class="owl-carousel owl-theme">
                        <!--DATA  -->
                    </div>
                </div> 
                <div class="ibox-content">
                    <h4><span id="itmes_topicChinese"></span></h4>
                    <p><span style="color: #1AB394;" id="business_nameChinese"></span><p>
                    <p>จังหวัด : <span id="provinces_Chinese">เชียงใหม่</span></p>
                    <p><span id="category_Chinese"></span></p>
                    
                    <h4>ราคาผู้ใหญ่ THB <span id="items_priceguestAdult_Chinese"> </span>.-</h4>
                    <h4>ราคาเด็ก THB <span id="items_priceguestChild_Chinese"> </span>.-</h4>
                    <hr class="hr-line-solid">
                    <h4>ไฮไลต์</h4>
                    <span id="items_highlightsChinese"></span>
                    <hr class="hr-line-solid">
                    <h4>ข้อมูลสถานที่</h4>
                    <span id="items_contactChinese"></span>
                    <div>
                        <div id="map"></div>
                    </div>
                    <hr class="hr-line-solid">
                    <h4>รายละเอียดการเปิด-ปิด</h4>
                    <p><span id="item_Open_Close_data_Chinese">เปิดให้บริการทุกวัน  17:00 - 03:00 น.</span></p>
                    <h4>รายละเอียดช่วงเวลาทำการ</h4>
                    <table class="table table-bordered" id="TimePeriodChinese">
                        <thead>
                            <tr>
                                <th>ช่วงเวลา</th>
                                <th>เปิด </th>
                                <th>ปิด</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_TimePeriodChinese">
                            <!--DATA  -->
                        </tbody>
                    </table>
                    <hr class="hr-line-solid">
                    <h4 id="topicdetail_Chinese">สิ่งที่จะได้พบ</h4>
                    <p><span id="detail_textChinese0"></span></p>
                    <div id="detail_photoChinese0">
                    </div>
                    <hr class="hr-line-solid">
                    <h4>เงื่อนไขในการจอง</h4>
                    <div id="BookingConditionChinese">
                        <!--DATA  -->
                    </div>
                    
                </div>
            </div>
        </div> 
        <div role="tabpanel" id="tab-4" class="tab-pane">
            <div class="panel-body">
                <div class="ibox-content">
                    <div id="owl_demoLaos" class="owl-carousel owl-theme">
                        <!--DATA  -->
                    </div>
                </div>  
                <div class="ibox-content">
                    <h4><span id="itmes_topicLaos"></span></h4>
                    <p><span style="color: #1AB394;" id="business_nameLaos"></span><p>
                    <p>จังหวัด : <span id="provinces_Laos">เชียงใหม่</span></p>
                    <p><span id="category_Laos"></span></p>
                    
                    <h4>ราคาผู้ใหญ่ THB <span id="items_priceguestAdult_Laos"> </span>.-</h4>
                    <h4>ราคาเด็ก THB <span id="items_priceguestChild_Laos"> </span>.-</h4>
                    <hr class="hr-line-solid">
                    <h4>ไฮไลต์</h4>
                    <span id="items_highlightsLaos"></span>
                    <hr class="hr-line-solid">
                    <h4>ข้อมูลสถานที่</h4>
                    <span id="items_contactLaos"></span>
                    <div>
                        <div id="map"></div>
                    </div>
                    <hr class="hr-line-solid">
                    <h4>รายละเอียดการเปิด-ปิด</h4>
                    <p><span id="item_Open_Close_data_Laos">เปิดให้บริการทุกวัน  17:00 - 03:00 น.</span></p>
                    <h4>รายละเอียดช่วงเวลาทำการ</h4>
                    <table class="table table-bordered" id="TimePeriodLaos">
                        <thead>
                            <tr>
                                <th>ช่วงเวลา</th>
                                <th>เปิด </th>
                                <th>ปิด</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_TimePeriodLaos">
                            <!--DATA  -->
                        </tbody>
                    </table>
                    <hr class="hr-line-solid">
                    <h4 id="topicdetail_Laos">สิ่งที่จะได้พบ</h4>
                    <p><span id="detail_textLaos0"></span></p>
                    <div id="detail_photoLaos0">
                    </div>
                    <hr class="hr-line-solid">
                    <h4>เงื่อนไขในการจอง</h4>
                    <div id="BookingConditionLaos">
                        <!--DATA  -->
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>