<div class="tabs-container">
    <ul class="nav nav-tabs" role="tablist">
        <li>
            <a class="nav-link active" data-toggle="tab" href="#tab-1" id="tabTH"> <?=$this->lang->line("lang_th");?></a>
        </li>
        <li>
            <a class="nav-link" data-toggle="tab" href="#tab-2" id="tabENG"><?=$this->lang->line("lang_en");?></a>
        </li>
        <li>
            <a class="nav-link" data-toggle="tab" href="#tab-3" id="tabCHN"><?=$this->lang->line("lang_chn");?></a>
            
        </li>
        <li>
        <a class="nav-link" data-toggle="tab" href="#tab-4" id="tabLAOS"><?=$this->lang->line("lang_lao");?></a>
        </li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" id="tab-1" class="tab-pane active">
            <div class="panel-body">
                <div class="ibox-content">
                    <img class="d-block w-100" src="<?php echo $host;?>assets/img/p3.jpg" alt="" id="coverItem_paths_Thai">  
                </div>
                <div class="ibox-content">
                    <hr class="hr-line-solid">
                    <h2><span id="itmes_topicThai"></span></h2>
                    <p><span id="category_thai"></span></p>
                    <p><h3><span id="">เวลาเปิด-ปิด</span></h3></p>
                    <p><span id="item_Open_Close_data_Thai">เปิดให้บริการทุกวัน  17:00 - 03:00 น.</span></p>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">สิ่งที่จะได้พบ</span></h3></p>
                    <p><span id="detail_textThai0"></span></p>
                    <div class="">
                        <div class="owl-carousel owl-theme photo_detail_textThai0 photo_detail_text" id="photo_detail_textThai0">
                            <!-- DATA -->
                        </div> 
                    </div>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">ตำแหน่งใกล้เคียง</span></h3></p>
                    <div>
                        <div id="map"></div>
                    </div>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">ติดต่อ</span></h3></p>
                    <p>ที่อยู่ : <span id="items_contactThai"></span></p>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">เบอร์โทร</span></h3></p>                                                                              
                    <p><span id="items_phoneThai"></span></p>   
                    <hr class="hr-line-solid">                                                                            
                    <p><h3><span id="">การติดต่อทาง สังคมออนไลน์</span></h3></p>
                    <p>E-mail : <span id="items_emailThai"></span></p>
                    <p>Line : <span id="items_lineThai"></span></p>
                    <p>Facebook page : <span id="items_facebookPageThai"></span></p>
                    <p>Website : <span id="items_wwwThai"></span></p>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">รายละเอียดที่น่าสนใจ</span></h3></p>
                    <p><span id="detail_textThai1"></span></p>
                    <div class ="row" id="photo_detail_textThai1">
                        <!-- DATA -->
                    </div>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">การเดินทาง</span></h3></p>
                    <p><span id="detail_textThai2"></span></p>
                    <div class ="row" id="photo_detail_textThai2">
                        <!-- DATA -->
                    </div>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">ระดับที่พัก</span></h3></p>
                    <p><span id="HotelAccommodationThai"></span></p>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">สิ่งอำนวยความสะดวก</span></h3></p>
                    <div id="FacilitiesThai">
                        <!-- DATA -->
                    </div>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">รายละเอียดตัวอย่างประเภทห้องพัก</span></h3></p>
                    <div class ="row" id="ProductThai">
                        <!-- DATA -->
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" id="tab-2" class="tab-pane">
            <div class="panel-body">
                <div class="ibox-content">
                    <img class="d-block w-100" src="<?php echo $host;?>assets/img/p3.jpg" alt="" id="coverItem_paths_English">  
                </div>
                <div class="ibox-content">
                    <hr class="hr-line-solid">
                    <h2><span id="itmes_topicEnglish"></span></h2>
                    <p><span id="category_English"></span></p>
                    <p><h3><span id="">เวลาเปิด-ปิด</span></h3></p>
                    <p><span id="item_Open_Close_data_English">เปิดให้บริการทุกวัน  17:00 - 03:00 น.</span></p>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">สิ่งที่จะได้พบ</span></h3></p>
                    <p><span id="detail_textEnglish0"></span></p>
                    <div class="">
                        <div class="owl-carousel owl-theme photo_detail_textEnglish0 photo_detail_text" id="photo_detail_textEnglish0">
                            <!-- DATA -->
                        </div> 
                    </div>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">ตำแหน่งใกล้เคียง</span></h3></p>
                    <div>
                        <div id="map"></div>
                    </div>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">ติดต่อ</span></h3></p>
                    <p>ที่อยู่ : <span id="items_contactEnglish"></span></p>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">เบอร์โทร</span></h3></p>                                                                              
                    <p><span id="items_phoneEnglish"></span></p>   
                    <hr class="hr-line-solid">                                                                            
                    <p><h3><span id="">การติดต่อทาง สังคมออนไลน์</span></h3></p>
                    <p>E-mail : <span id="items_emailEnglish"></span></p>
                    <p>Line : <span id="items_lineEnglish"></span></p>
                    <p>Facebook page : <span id="items_facebookPageEnglish"></span></p>
                    <p>Website : <span id="items_wwwEnglish"></span></p>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">รายละเอียดที่น่าสนใจ</span></h3></p>
                    <p><span id="detail_textEnglish1"></span></p>
                    <div class ="row" id="photo_detail_textEnglish1">
                        <!-- DATA -->
                    </div>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">การเดินทาง</span></h3></p>
                    <p><span id="detail_textEnglish2"></span></p>
                    <div class ="row" id="photo_detail_textEnglish2">
                        <!-- DATA -->
                    </div>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">ระดับที่พัก</span></h3></p>
                    <p><span id="HotelAccommodationEnglish"></span></p>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">สิ่งอำนวยความสะดวก</span></h3></p>
                    <div id="FacilitiesEnglish">
                        <!-- DATA -->
                    </div>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">รายละเอียดตัวอย่างประเภทห้องพัก</span></h3></p>
                    <div class ="row" id="ProductEnglish">
                        <!-- DATA -->
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" id="tab-3" class="tab-pane">
            <div class="panel-body">
                <div class="ibox-content">
                    <img class="d-block w-100" src="<?php echo $host;?>assets/img/p3.jpg" alt="" id="coverItem_paths_Chinese">  
                </div>
                <div class="ibox-content">
                    <hr class="hr-line-solid">
                    <h2><span id="itmes_topicChinese"></span></h2>
                    <p><span id="category_Chinese"></span></p>
                    <p><h3><span id="">เวลาเปิด-ปิด</span></h3></p>
                    <p><span id="item_Open_Close_data_Chinese">เปิดให้บริการทุกวัน  17:00 - 03:00 น.</span></p>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">สิ่งที่จะได้พบ</span></h3></p>
                    <p><span id="detail_textChinese0"></span></p>
                    <div class="">
                        <div class="owl-carousel owl-theme photo_detail_textChinese0 photo_detail_text" id="photo_detail_textChinese0">
                            <!-- DATA -->
                        </div> 
                    </div>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">ตำแหน่งใกล้เคียง</span></h3></p>
                    <div>
                        <div id="map"></div>
                    </div>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">ติดต่อ</span></h3></p>
                    <p>ที่อยู่ : <span id="items_contactChinese"></span></p>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">เบอร์โทร</span></h3></p>                                                                              
                    <p><span id="items_phoneChinese"></span></p>   
                    <hr class="hr-line-solid">                                                                            
                    <p><h3><span id="">การติดต่อทาง สังคมออนไลน์</span></h3></p>
                    <p>E-mail : <span id="items_emailChinese"></span></p>
                    <p>Line : <span id="items_lineChinese"></span></p>
                    <p>Facebook page : <span id="items_facebookPageChinese"></span></p>
                    <p>Website : <span id="items_wwwChinese"></span></p>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">รายละเอียดที่น่าสนใจ</span></h3></p>
                    <p><span id="detail_textChinese1"></span></p>
                    <div class ="row" id="photo_detail_textChinese1">
                        <!-- DATA -->
                    </div>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">การเดินทาง</span></h3></p>
                    <p><span id="detail_textChinese2"></span></p>
                    <div class ="row" id="photo_detail_textChinese2">
                        <!-- DATA -->
                    </div>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">ระดับที่พัก</span></h3></p>
                    <p><span id="HotelAccommodationChinese"></span></p>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">สิ่งอำนวยความสะดวก</span></h3></p>
                    <div id="FacilitiesChinese">
                        <!-- DATA -->
                    </div>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">รายละเอียดตัวอย่างประเภทห้องพัก</span></h3></p>
                    <div class ="row" id="ProductChinese">
                        <!-- DATA -->
                    </div>
                </div>
            </div>
        </div>
        <div role="tabpanel" id="tab-4" class="tab-pane">
            <div class="panel-body">
                <div class="ibox-content">
                    <img class="d-block w-100" src="<?php echo $host;?>assets/img/p3.jpg" alt="" id="coverItem_paths_Laos">  
                </div>
                <div class="ibox-content">
                    <hr class="hr-line-solid">
                    <h2><span id="itmes_topicLaos"></span></h2>
                    <p><span id="category_Laos"></span></p>
                    <p><h3><span id="">เวลาเปิด-ปิด</span></h3></p>
                    <p><span id="item_Open_Close_data_Laos">เปิดให้บริการทุกวัน  17:00 - 03:00 น.</span></p>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">สิ่งที่จะได้พบ</span></h3></p>
                    <p><span id="detail_textLaos0"></span></p>
                    <div class="">
                        <div class="owl-carousel owl-theme photo_detail_textLaos0 photo_detail_text" id="photo_detail_textLaos0">
                            <!-- DATA -->
                        </div> 
                    </div>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">ตำแหน่งใกล้เคียง</span></h3></p>
                    <div>
                        <div id="map"></div>
                    </div>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">ติดต่อ</span></h3></p>
                    <p>ที่อยู่ : <span id="items_contactLaos"></span></p>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">เบอร์โทร</span></h3></p>                                                                              
                    <p><span id="items_phoneLaos"></span></p>   
                    <hr class="hr-line-solid">                                                                            
                    <p><h3><span id="">การติดต่อทาง สังคมออนไลน์</span></h3></p>
                    <p>E-mail : <span id="items_emailLaos"></span></p>
                    <p>Line : <span id="items_lineLaos"></span></p>
                    <p>Facebook page : <span id="items_facebookPageLaos"></span></p>
                    <p>Website : <span id="items_wwwLaos"></span></p>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">รายละเอียดที่น่าสนใจ</span></h3></p>
                    <p><span id="detail_textLaos1"></span></p>
                    <div class ="row" id="photo_detail_textLaos1">
                        <!-- DATA -->
                    </div>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">การเดินทาง</span></h3></p>
                    <p><span id="detail_textLaos2"></span></p>
                    <div class ="row" id="photo_detail_textLaos2">
                        <!-- DATA -->
                    </div>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">ระดับที่พัก</span></h3></p>
                    <p><span id="HotelAccommodationLaos"></span></p>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">สิ่งอำนวยความสะดวก</span></h3></p>
                    <div id="FacilitiesLaos">
                        <!-- DATA -->
                    </div>
                    <hr class="hr-line-solid">
                    <p><h3><span id="">รายละเอียดตัวอย่างประเภทห้องพัก</span></h3></p>
                    <div class ="row" id="ProductLaos">
                        <!-- DATA -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>