<div class="modal fade ProvinceGroupModal" id="ProvinceGroupModal" tabindex="-1" role="dialog" aria-labelledby="ProvinceGroupModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ProvinceGroupModalLabel">Add Province Group</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <label for="thTravelPlaceDescription" class="col-sm-4 col-xs-4 col-form-label">ชื่อกลุ่มจังหวัด<br>ภาษาไทย</label>
                    <div class="col-sm-6 col-xs-6">
                        <input type="test" placeholder="ชื่อกลุ่มจังหวัด ภาษาไทย" class="form-control form-control-input flat-input " id="pg_thai">
                        <span class="note notifi" id="notifi_pg_thai"><span>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="thTravelPlaceDescription" class="col-sm-4 col-xs-4 col-form-label">ชื่อกลุ่มจังหวัด<br>ภาษาอังกฤษ</label>
                    <div class="col-sm-6 col-xs-6">
                        <input type="text" placeholder="ชื่อกลุ่มจังหวัด ภาษาอังกฤษ" class="form-control form-control-input flat-input " id="pg_english">

                    </div>
                </div>
                <div class="form-group row">
                    <label for="thTravelPlaceDescription" class="col-sm-4 col-xs-4 col-form-label">ชื่อกลุ่มจังหวัด<br>ภาษาจีน</label>
                    <div class="col-sm-6 col-xs-6">
                        <input type="text" placeholder="ชื่อกลุ่มจังหวัด ภาษาจีน" class="form-control form-control-input flat-input " id="pg_chinese">

                    </div>
                </div>
                <div class="form-group row">
                    <label for="thTravelPlaceDescription" class="col-sm-4 col-xs-4 col-form-label">ชื่อกลุ่มจังหวัด<br>ภาษาลาว</label>
                    <div class="col-sm-6 col-xs-6">
                        <input type="text" placeholder="ชื่อกลุ่มจังหวัด ภาษาลาว" class="form-control form-control-input flat-input " id="pg_laos">

                    </div>

                </div>
                <div class="form-group row">
                    <label for="thTravelPlaceDescription" class="col-sm-4 col-xs-4 col-form-label">กลุ่มจังหวัด</label>
                    <div class="col-sm-6 col-xs-6">
                        <select data-placeholder="Choose a Province..." multiple class="chosen-select ProvinceSelection" tabindex="1" id="ProvinceSelection"></select>
                        <span class="note notifi" id="notifi_ProvinceSelection"><span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col">
                        <span class="note notifi" id="notifi_error_save_ProvinceGroup"><span>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-success" type="button" id="btn_save_ProvinceGroup">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>