<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Content | Management System</title>

    <?php $this->load->view('Html/include/Header_css.php');?>  

</head>
<body>
    <!-- Preloader -->
    <div class="preloader" id="preloader">
        <div class="status" id="status"></div>
    </div>
    <div id="wrapper">
        <!-- SideMenu -->
        <?php $this->load->view('Html/include/SideMenu.php');?>
        <!-- End of SideMenu -->

        <div id="page-wrapper" class="gray-bg">
            <!-- Header -->
            <?php $this->load->view('Html/include/Header.php');?>
            <!-- End of Header -->

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2><?=$this->lang->line("sidebar_menu_MediaAndAdvertising");?></h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dasta_thailand/html/Main">Home</a></li>
                        <li class="breadcrumb-item"><?=$this->lang->line("sidebar_menu_MediaAndAdvertising");?></li>
                        <li class="breadcrumb-item active"><a href="/dasta_thailand/html/MediaAndAdvertising/VideoContent">วิดีโอคอนเทนต์ที่น่าสนใจ</a> </li>
                        <li class="breadcrumb-item active"><strong>เพิ่มวิดีโอคอนเทนต์ที่น่าสนใจ</strong></li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="ibox ">
                                        <div class="ibox-title  back-change">
                                            <h5>ข้อมูลวีดีโอ</h5>
                                        </div>
                                        <div class="ibox-content">
                                            <div class="row">
                                                <div class="col-md-6">

                                                    <div class="tabs-container">
                                                        <ul class="nav nav-tabs" role="tablist">
                                                            <li>
                                                                <a class="nav-link active" data-toggle="tab" href="#tab-1" id="tabTH"> <?=$this->lang->line("lang_th");?></a>
                                                            </li>
                                                            <li>
                                                                <a class="nav-link" data-toggle="tab" href="#tab-2" id="tabENG"><?=$this->lang->line("lang_en");?></a>
                                                            </li>
                                                            <li>
                                                                <a class="nav-link" data-toggle="tab" href="#tab-3" id="tabLAOS"><?=$this->lang->line("lang_lao");?></a>
                                                            </li>
                                                            <li>
                                                                <a class="nav-link" data-toggle="tab" href="#tab-4" id="tabCHN"><?=$this->lang->line("lang_chn");?></a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content" style="margin-top: 10px;">
                                                            
                                                            <div role="tabpanel" id="tab-1" class="tab-pane active">
                                                            
                                                                <div class="form-group  row ">
                                                                    <label class="col-sm-4 col-form-label">ชื่อวีดีโอ</label>
                                                                    <div class="col-sm-8">
                                                                        <input type="text" placeholder="ภาษาไทย" class="form-control" id="videoContent_textThai">
                                                                        <span class="note notifi" id="notifi_videoContent_textThai"></span>
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>
                                                            <div role="tabpanel" id="tab-2" class="tab-pane">
                                                                <div class="form-group  row">
                                                                    <label class="col-sm-4 col-form-label">ชื่อวีดีโอ</label>
                                                                    <div class="col-sm-8">
                                                                        <input type="text" placeholder="ภาษาอังกฤษ" class="form-control" id="videoContent_textEnglish">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div role="tabpanel" id="tab-3" class="tab-pane">
                                                                <div class="form-group  row">
                                                                    <label class="col-sm-4 col-form-label">ชื่อวีดีโอ</label>
                                                                    <div class="col-sm-8">
                                                                        <input type="text" placeholder="ภาษาลาว" class="form-control" id="videoContent_textLaos">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div role="tabpanel" id="tab-4" class="tab-pane">
                                                                <div class="form-group  row">
                                                                    <label class="col-sm-4 col-form-label">ชื่อวีดีโอ</label>
                                                                    <div class="col-sm-8">
                                                                        <input type="text" placeholder="ภาษาจีน" class="form-control" id="videoContent_textChinese">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>

                                                    <div class="form-group  row">
                                                        <label class="col-sm-4 col-form-label">link youtube</label>
                                                        <div class="col-sm-8">
                                                            <input type="text" placeholder="www.youtube" class="form-control link-youtube" id="link_youtube">
                                                            <span class="note notifi" id="notifi_link_youtube"></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group  row">
                                                        <label class="col-sm-4 col-form-label">ที่ตั้งประเทศ</label>
                                                        <div class="col-sm-8">
                                                            <select data-placeholder="Choose a Country..." class="chosen-select country" tabindex="1" id="country">
                                                            </select>
                                                            <span class="note notifi" id="notifi_country"></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group  row">
                                                        <label class="col-sm-4 col-form-label">ที่ตั้งจังหวัด</label>
                                                        <div class="col-sm-8">
                                                            <select data-placeholder="Choose a Provinces..." class="chosen-select provinces" tabindex="1" id="provinces" disabled>
                                                            </select>
                                                            <span class="note notifi" id="notifi_provinces"></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group  row">
                                                        <label class="col-sm-4 col-form-label">ที่ตั้งอำเภอ</label>
                                                        <div class="col-sm-8">
                                                            <select data-placeholder="Choose a Districts..." class="chosen-select districts" tabindex="1" id="districts" disabled>
                                                            </select>
                                                            <span class="note notifi" id="notifi_districts"></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group  row">
                                                        <label class="col-sm-4 col-form-label">หมวดหมู่สถานที่</label>
                                                        <div class="col-sm-8">
                                                            <select data-placeholder="Choose a Category..." class="chosen-select namecategory" tabindex="1" id="namecategory">
                                                            </select>
                                                            <span class="note notifi" id="notifi_namecategory"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <h4>ตัวอย่างวีดีโอ</h4>
                                                    <div class="form-group row">
                                                        <div class="col-sm-12">
                                                            <div >
                                                                <iframe width="420" height="290" id="player" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture">
                                                                    [ video ]
                                                                </iframe>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-right">
                            <div class="col">
                                <span class="note notifi" id="notifi_error_save"></span>
                            </div>
                            <button class="btn btn-primary btn-xl" id="save">บันทึก</button>
                            
                        </div>

                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php $this->load->view('Html/include/Footer.php');?>
            <!-- End of Footer -->
        </div>  
    </div>
        
    <?php $this->load->view('Html/include/Footer_js.php');?>
    <script>
        var host = "<?php echo $host;?>html";
        var table;
        var map;
        var cropper;
        var gmarkers = [];
        var CropData,CropBoxData,CanvasData;
        var MenuName = "VideoContent";
        var TypeMenu = "MediaAndAdvertising";
        var UseGoogleMap = '<?php echo $ENVIRONMENT?>';

    </script>
   
    <script src="<?php echo $host;?>assets/js/custom_js/GoogleMaps.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/GlobalFunction.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/LocationManagement.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/MediaAndAdvertising/VideoContentAddView.js"></script>
    
    

</body>

</html>