<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Content | Management System</title>
    <?php $this->load->view('Html/include/Header_css.php');?>  
</head>
<body>
    <!-- Preloader -->
    <div class="preloader" id="preloader">
        <div class="status" id="status"></div>
    </div>
    <div id="wrapper">
        <!-- SideMenu -->
        <?php $this->load->view('Html/include/SideMenu.php');?>
        <!-- End of SideMenu -->
        <div id="page-wrapper" class="gray-bg">
            <!-- Header -->
            <?php $this->load->view('Html/include/Header.php');?>
            <!-- End of Header -->

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2><?=$this->lang->line("sidebar_menu_MediaAndAdvertising");?></h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="../Main">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <?=$this->lang->line("sidebar_menu_MediaAndAdvertising");?>
                        </li>
                        <li class="breadcrumb-item active">
                            <a href="/dasta_thailand/html/MediaAndAdvertising/CarRentAds">โฆษณาตั๋วกิจกรรม</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong>แก้ไขรายละเอียดโฆษณาตั๋วกิจกรรม</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">

                    <div class="col-lg-12">
                        <div class="ibox ">
                            <div class="ibox-title  back-change">
                                <h5>ข่าวสารประชาสัมพันธ์</h5>
                            </div>
                            <div class="ibox-content">
                                <p><?=$this->lang->line("cover_photo");?></p>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="image-crop" >
                                            <img class="image-crop-img" src="<?php echo $host;?>assets/img/uploadfile/<?php echo $data_item[0]->tempImageCover_paths;?>" id="image_crop_img" style="width: 590px; height: 314px;">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <h4><?=$this->lang->line("preview_pictures");?></h4>
                                        <div class="form-group  row">
                                            <div class="img-preview img-preview-sm"></div>
                                            <div class="" style="margin-left: 5px;">
                                                <button type="button" class="btn btn-primary btn-reset-crop" id="btn_reset_crop" data-method="reset" title="Reset">
                                                    <span class="fa fa-refresh"></span>
                                                    </span>
                                                </button>
                                            </div>
                                        </div>
                                        <h4><?=$this->lang->line("cover_photo");?></h4>
                                        <p>
                                            <?=$this->lang->line("adjustments_on_pictures");?>
                                        </p>
                                        <div class="btn-group">
                                            <label title="Upload image file" for="inputImage" class="btn btn-primary">
                                                <input type="file" accept="image/jpg,image/png,image/jpeg" name="file" id="inputImage" class="hide"> Upload new image
                                                <input type="hidden" name="temp_inputImage" id="temp_inputImage"> 
                                            </label>
                                        </div>
                                        <label style="color: red;">* รูปภาพควรมีอัตราส่วน 16:9, กว้าง x ยาว ไม่เกิน 1920 X 1080  pixels และขนาดไม่เกิน 2 MB.</label>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="ibox ">
                            <div class="ibox-content">

                                <div class="tabs-container">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li>
                                            <a class="nav-link active" data-toggle="tab" href="#tab-1" id="tabTH"> <?=$this->lang->line("lang_th");?></a>
                                        </li>
                                        <li>
                                            <a class="nav-link" data-toggle="tab" href="#tab-2" id="tabENG"><?=$this->lang->line("lang_en");?></a>
                                        </li>
                                        <li>
                                            <a class="nav-link" data-toggle="tab" href="#tab-3" id="tabLAOS"><?=$this->lang->line("lang_lao");?></a>
                                        </li>
                                        <li>
                                            <a class="nav-link" data-toggle="tab" href="#tab-4" id="tabCHN"><?=$this->lang->line("lang_chn");?></a>
                                        </li>
                                    </ul>
                                    <div class="tab-content" style="margin-top: 10px;">
                                        
                                        <div role="tabpanel" id="tab-1" class="tab-pane active">
                                            <div class="form-group  row">
                                                <label class="col-sm-2 col-form-label">ชื่อป้ายโฆษณา</label>
                                                <div class="col-sm-10">
                                                    <input type="" placeholder="ภาษาไทย" class="form-control" id="mediaAndAdvertising_nameThai" value ="<?php echo $data_item[0]->mediaAndAdvertising_nameThai;?>">
                                                    <span class="note notifi" id="notifi_mediaAndAdvertising_nameThai"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" id="tab-2" class="tab-pane">
                                            <div class="form-group  row">
                                                <label class="col-sm-2 col-form-label">ชื่อป้ายโฆษณา</label>
                                                <div class="col-sm-10">
                                                    <input type="text" placeholder="ภาษาอังกฤษ" class="form-control" id="mediaAndAdvertising_nameEnglish" value ="<?php echo $data_item[0]->mediaAndAdvertising_nameEnglish;?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" id="tab-3" class="tab-pane">
                                            <div class="form-group  row">
                                                <label class="col-sm-2 col-form-label">ชื่อป้ายโฆษณา</label>
                                                <div class="col-sm-10">
                                                    <input type="text" placeholder="ภาษาลาว" class="form-control" id="mediaAndAdvertising_nameLaos" value ="<?php echo $data_item[0]->mediaAndAdvertising_nameLaos;?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" id="tab-4" class="tab-pane">
                                            <div class="form-group  row">
                                                <label class="col-sm-2 col-form-label">ชื่อป้ายโฆษณา</label>
                                                <div class="col-sm-10">
                                                    <input type="text" placeholder="ภาษาจีน" class="form-control" id="mediaAndAdvertising_nameChinese" value ="<?php echo $data_item[0]->mediaAndAdvertising_nameChinese;?>">
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <!-- <div class="form-group  row">
                                    <label class="col-sm-2 col-form-label">ที่ตั้งประเทศ</label>
                                    <div class="col-sm-10">
                                        <select data-placeholder="Choose a Country..." class="chosen-select " tabindex="1" id="country">
                                        </select>
                                        <span class="note notifi" id="notifi_country"></span>
                                    </div>
                                </div>
                                <div class="form-group  row">
                                    <label class="col-sm-2 col-form-label">ที่ตั้งจังหวัด</label>
                                    <div class="col-sm-10">
                                        <select data-placeholder="Choose a Provinces..." class="chosen-select " tabindex="1" id="provinces" disabled>
                                        </select>
                                        <span class="note notifi" id="notifi_provinces"></span>
                                    </div>
                                </div> -->
                                <!-- <div class="form-group  row">
                                    <label class="col-sm-2 col-form-label">ที่ตั้งอำเภอ</label>
                                    <div class="col-sm-10">
                                        <select data-placeholder="Choose a Districts..." class="chosen-select " tabindex="1" id="districts" disabled>
                                        </select>
                                        <span class="note notifi" id="notifi_districts"></span>
                                    </div>
                                </div> -->
                                <div class="form-group  row">
                                    <label class="col-sm-2 col-form-label">บริษัท</label>
                                    <div class="col-sm-10">
                                        <select data-placeholder="Choose a Country..." class="chosen-select " tabindex="1" id="business_id">
                                        </select>
                                        <span class="note notifi" id="notifi_business_id"></span>
                                    </div>
                                </div>
                                
                                
                                <div class="form-group  row">
                                    <div class=" col-sm-2 ">
                                        <div class="row">
                                            <label class="col-sm-12 col-form-label">ช่วงเวลาที่จะให้แสดง</label>
                                            <p class="col-sm-12 col-form-label" style="font-size: smaller;top: -15px;">* เลือกช่วงเวลาในการแสดงโฆษณานี้</p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-10 col-form-label">
                                                <div class="form-check-inline i-checks">
                                                    <label class="form-check-label">
                                                        <input type="radio" class="form-check-input set_time_period" value="1" name="set_time_period" id="set_time_period"><i></i> กำหนดช่วงเวลา
                                                        </label>
                                                </div>
                                                <div class="form-check-inline i-checks">
                                                    <label class="form-check-label">
                                                        <input type="radio" class="form-check-input set_time_period" value="0" name="set_time_period" id="un_set_time_period" checked><i></i> ไม่กำหนดช่วงเวลา
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-8 col-form-label">
                                                <div class="form-group" id="data_5">
                                                    <div class="input-daterange input-group" id="datepicker">
                                                        <input type="text" class="form-control-sm form-control time_period" name="start" id="start" value="" disabled/>
                                                        <span class="input-group-addon">ถึง</span>
                                                        <input type="text" class="form-control-sm form-control time_period" name="end" id="end" value="" disabled/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                               
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="ibox ">
                            <div class="">
                                <div class="text-right">
                                    <div class="col">
                                        <span class="note notifi" id="notifi_error_save"></span>
                                    </div>
                                    <button class="btn btn-primary btn-xl" id="save">บันทึก</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
           
            <!-- Footer -->
            <?php $this->load->view('Html/include/Footer.php');?>
            <!-- End of Footer -->
        </div>
    </div>
    <?php $this->load->view('Html/include/Footer_js.php');?>
    <script>
        var host = "<?php echo $host;?>html"
        var table;
        var MenuName = "CarRentAds";
        
        var UseGoogleMap = '<?php echo $ENVIRONMENT?>';
        var cropper;
        var crop_img = false;
        var CropData = JSON.parse( '<?php echo str_replace("\\t", "",$data_item[0]->tempImageCover_CropData);?>');
        var CropBoxData = JSON.parse('<?php echo str_replace("\\t", "",$data_item[0]->tempImageCover_CropBoxData);?>');
        var CanvasData = JSON.parse('<?php echo str_replace("\\t", "",$data_item[0]->tempImageCover_CanvasData);?>');
        
        var mediaAndAdvertising_id = <?php echo $data_item[0]->mediaAndAdvertising_id;?>;

        
        var business_id = <?php echo $data_item[0]->Business_business_id;?>;

        var mediaAndAdvertising_time_period_start = "<?php echo $data_item[0]->mediaAndAdvertising_time_period_start;?>";
        var mediaAndAdvertising_time_period_end = "<?php echo $data_item[0]->mediaAndAdvertising_time_period_end;?>";

    </script>
    <script src="<?php echo $host;?>assets/js/custom_js/GoogleMaps.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/GlobalFunction.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/LocationManagement.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/MediaAndAdvertising/CarRentAdsEditView.js"></script>
    <!-- <script src="<?php echo $host;?>assets/js/custom_js/CustomsDataTable.js"></script> -->
            
</body>

</html>