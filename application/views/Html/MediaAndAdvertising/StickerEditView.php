<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Content | Management System</title>
    <?php $this->load->view('Html/include/Header_css.php');?> 

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" crossorigin="anonymous">
    <link href="<?php echo $host;?>assets/bootstrap-fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $host;?>assets/bootstrap-fileinput/themes/explorer-fas/theme.css" media="all" rel="stylesheet" type="text/css"/>
    <style>
        .kv-file-upload{
            color:#fff !important;
            visibility: hidden !important;
        }
    </style>
</head>
<body>
    <!-- Preloader -->
    <div class="preloader" id="preloader">
        <div class="status" id="status"></div>
    </div>
    <div id="wrapper">
        <!-- SideMenu -->
        <?php $this->load->view('Html/include/SideMenu.php');?>
        <!-- End of SideMenu -->
        <div id="page-wrapper" class="gray-bg">
            <!-- Header -->
            <?php $this->load->view('Html/include/Header.php');?>
            <!-- End of Header -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2><?=$this->lang->line("sidebar_menu_MediaAndAdvertising");?></h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../Main">Home</a></li>
                        <li class="breadcrumb-item"><?=$this->lang->line("sidebar_menu_MediaAndAdvertising");?></li>
                        <li class="breadcrumb-item"><a href="/dasta_thailand/html/MediaAndAdvertising/Sticker">สติ๊กเกอร์</a></li>
                        <li class="breadcrumb-item active"><strong>แก้ไขสติ๊กเกอร์</strong></li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox ">
                            <div class="ibox-content">
                                
                                <div class="tabs-container">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li>
                                            <a class="nav-link active" data-toggle="tab" href="#tab-1" id="tabTH"> <?=$this->lang->line("lang_th");?></a>
                                        </li>
                                        <li>
                                            <a class="nav-link" data-toggle="tab" href="#tab-2" id="tabENG"><?=$this->lang->line("lang_en");?></a>
                                        </li>
                                        <li>
                                            <a class="nav-link" data-toggle="tab" href="#tab-3" id="tabLAOS"><?=$this->lang->line("lang_lao");?></a>
                                        </li>
                                        <li>
                                            <a class="nav-link" data-toggle="tab" href="#tab-4" id="tabCHN"><?=$this->lang->line("lang_chn");?></a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class=" ">
                                            <div class="ibox-title  back-change">
                                                <h5>รายละเอียดสติ๊กเกอร์</h5>
                                            </div>
                                        </div>
                                   
                                        <div role="tabpanel" id="tab-1" class="tab-pane active">
                                            <div class="ibox-content">
                                                <div class="form-group  row">
                                                    <label class="col-sm-2 col-form-label">ชื่อสติ๊กเกอร์</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" placeholder="ชื่อสติ๊กเกอร์ ภาษาไทย" class="form-control sticker_name" id="sticker_Thai" value ="<?php echo $data_item[0]->sticker_Thai;?>">
                                                        <span class="note notifi" id="notifi_sticker_Thai"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" id="tab-2" class="tab-pane">
                                            <div class="ibox-content">
                                                <div class="form-group  row">
                                                    <label class="col-sm-2 col-form-label">ชื่อสติ๊กเกอร์</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" placeholder="ชื่อสติ๊กเกอร์ ภาษาอังกฤษ" class="form-control sticker_name" id="sticker_English" value ="<?php echo $data_item[0]->sticker_English;?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" id="tab-3" class="tab-pane">
                                            <div class="ibox-content">
                                                <div class="form-group  row">
                                                    <label class="col-sm-2 col-form-label">ชื่อสติ๊กเกอร์</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" placeholder="ชื่อสติ๊กเกอร์ ภาษาลาว" class="form-control sticker_name" id="sticker_Laos" value ="<?php echo $data_item[0]->sticker_Laos;?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" id="tab-4" class="tab-pane">
                                            <div class="ibox-content">
                                                <div class="form-group  row">
                                                    <label class="col-sm-2 col-form-label">ชื่อสติ๊กเกอร์</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" placeholder="ชื่อสติ๊กเกอร์ ภาษาจีน" class="form-control sticker_name" id="sticker_Chinese" value ="<?php echo $data_item[0]->sticker_Chinese;?>">
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="ibox-content">
                                    <div class="form-group  row">
                                        <label class="col-sm-2 col-form-label">หมวดหมู่สติ๊กเกอร์</label>
                                        <div class="col-sm-10">
                                            <select data-placeholder="Choose a Country..." class="chosen-select" tabindex="1" id="sticker_type"></select>
                                            <span class="note notifi" id="notifi_sticker_type"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="ibox ">
                                        <div class="ibox-content">
                                            <div class="form-group">
                                                <div class="file-loading">
                                                    <label>Preview File Icon</label>
                                                    <input id="fileSticker" type="file" name="fileSticker[]" multiple class="file" data-overwrite-initial="false" data-min-file-count="2" data-theme="fas">
                                                </div>
                                                <div id="errorBlock" class="help-block"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="text-right">
                                    <div class="col">
                                        <span class="note notifi" id="notifi_error_save"></span>
                                    </div>
                                    <button class="btn btn-primary btn-xl" id="save">บันทึก</button>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <!-- Footer -->
            <?php $this->load->view('Html/include/Footer.php');?>
            <!-- End of Footer -->
        </div>
    </div>
    <?php $this->load->view('Html/include/Footer_js.php');?>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="<?php echo $host;?>assets/bootstrap-fileinput/js/plugins/piexif.js" type="text/javascript"></script>
    <script src="<?php echo $host;?>assets/bootstrap-fileinput/js/plugins/sortable.js" type="text/javascript"></script>
    <script src="<?php echo $host;?>assets/bootstrap-fileinput/js/fileinput.js" type="text/javascript"></script>
    <script src="<?php echo $host;?>assets/bootstrap-fileinput/js/locales/fr.js" type="text/javascript"></script>
    <script src="<?php echo $host;?>assets/bootstrap-fileinput/js/locales/es.js" type="text/javascript"></script>
    <script src="<?php echo $host;?>assets/bootstrap-fileinput/themes/fas/theme.js" type="text/javascript"></script>
    <script src="<?php echo $host;?>assets/bootstrap-fileinput/themes/explorer-fas/theme.js" type="text/javascript"></script>

    <script>
        var host = "<?php echo $host;?>html"
        var table;
        var MenuName = "Sticker";
        var typepage = "MediaAndAdvertising";
        var type = "saveEditSticker";

        var UseGoogleMap = '<?php echo $ENVIRONMENT?>';

        var val_sticker_id = <?php echo $data_item[0]->sticker_id;?>;
        var sticker_type = <?php echo $data_item[0]->StickerType_stickerType_id;?>;

        var initialPreview = JSON.parse('<?php echo escapeJsonString(json_encode($data_item[0]->initialPreview)) ?>');
        var initialPreviewConfig = JSON.parse('<?php echo escapeJsonString(json_encode($data_item[0]->initialPreviewConfig)) ?>');

        $("#fileSticker").fileinput({
            theme: 'fas',
            showUpload: false,
            minFileCount: 1,
            validateInitialCount: true,
            showCaption: false,
            browseClass: "btn btn-primary btn-lg",
            allowedFileExtensions: ['jpg', 'png', 'jpeg'],
            previewFileIcon: "<i class='glyphicon glyphicon-king'></i>",
            overwriteInitial: false,
            initialPreviewAsData: true,
            uploadExtraData: function() { 
                var out = {
                    'sticker_id': val_sticker_id
                }
                return out;
            },
            uploadUrl: '/dasta_thailand/html/MediaAndAdvertising/Sticker/savePhotoSticker',

            initialPreview: initialPreview,
            initialPreviewConfig: initialPreviewConfig
        })
        .on('fileuploaded', function(event, previewId, index, fileId) {
            console.log('File Uploaded', 'ID: ' + fileId + ', Thumb ID: ' + previewId);
        })
        .on('fileuploaderror', function(event, data, msg) {
            console.log('File Upload Error', 'ID: ' + data.fileId + ', Thumb ID: ' + data.previewId);
        })
        .on('filebatchuploadcomplete', function(event, preview, config, tags, extraData) {
            console.log('File Batch Uploaded', preview, config, tags, extraData);
            window.location.href ='/dasta_thailand/html/'+typepage+'/'+MenuName;
        })
        .on('filebeforedelete', function() {
            var aborted = !window.confirm('Are you sure you want to delete this file?');
            return aborted;
        })
        .on('filedeleted', function(event, key, jqXHR, data) {
            console.log('Key = ' + key);
        })
        // .on('filepreajax', function(event, previewId, index) {
        //     console.log('File pre ajax triggered');
        // });
        
    </script>

    <script src="<?php echo $host;?>assets/js/custom_js/GoogleMaps.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/GlobalFunction.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/LocationManagement.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/MediaAndAdvertising/StickerEditView.js"></script>
    <!-- <script src="<?php echo $host;?>assets/js/custom_js/CustomsDataTable.js"></script> -->
       
</body>

</html>
<?php
function escapeJsonString($value) { # list from www.json.org: (\b backspace, \f formfeed)
    $escapers = array("\\", "/", "\"", "\n", "\r", "\t", "\x08", "\x0c");
    $replacements = array("\\\\", "\\/", "\\\"", "\\n", "\\r", "\\t", "\\f", "\\b");
    $result = str_replace($escapers, $replacements, $value);
    return $result;
}
?>