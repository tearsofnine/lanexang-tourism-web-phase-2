<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Content | Management System</title>
    <?php $this->load->view('Html/include/Header_css.php');?>  
</head>
<body>
    <!-- Preloader -->
    <div class="preloader" id="preloader">
        <div class="status" id="status"></div>
    </div>
    <div id="wrapper">
        <!-- SideMenu -->
        <?php $this->load->view('Html/include/SideMenu.php');?>
        <!-- End of SideMenu -->
        <div id="page-wrapper" class="gray-bg">
            <!-- Header -->
            <?php $this->load->view('Html/include/Header.php');?>
            <!-- End of Header -->

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2><?=$this->lang->line("sidebar_menu_MediaAndAdvertising");?></h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="../Main">Home</a></li>
                        <li class="breadcrumb-item"><?=$this->lang->line("sidebar_menu_MediaAndAdvertising");?></li>
                        <li class="breadcrumb-item active"><strong>โฆษณาแพคเกจทัวร์</strong></li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- SearchBox -->
                        <?php $this->load->view('Html/include/SearchBox.php');?>
                        <!-- End of SearchBox -->

                        <div class="text-right">
                            <a href="./PackageToursAds/addPackageToursAds" class="btn btn-primary btn-xs">เพิ่มโฆษณาแพคเกจทัวร์</a>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="ibox">
                                    <table class="footable table table-hover" data-page-size="8" data-filter=#filter style="width:100%" id="PackageToursAdsTable">
                                        <thead>
                                            <tr>
                                                <th class="th-item-table">
                                                    <span class="float-right">
                                                        <strong><span id="countData"></span></strong> <?=$this->lang->line("list");?>
                                                        </span>
                                                    <h5 style="font-size: 14px;">โฆษณาแพคเกจทัวร์</h5>                             
                                                </th>
                                               
                                                <th>ชื่อ</th>
                                                <th>แพคเกจทัวร์ที่เลือก</th>
                                                <th>ขอบเขตทัวร์</th>
                                                <th>ประเภททัวร์</th>
                                                <th>บริษัท</th>
                                                <th>ช่วงเวลาที่จะให้แสดง</th>
                                                <th>วันที่</th>
                                            </tr>
                                        </thead>
                                        <tbody>                                                                         
                                                                                       
                                        </tbody>
                                    </table>
                                   
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php $this->load->view('Html/include/Footer.php');?>
            <!-- End of Footer -->
        </div>
    </div>
    <?php $this->load->view('Html/include/Footer_js.php');?>
    <script>
        var host = "<?php echo $host;?>html"
        var table;
        var MenuName = "PackageToursAds";
        var TypeMenu = "MediaAndAdvertising";
        var UseGoogleMap = '<?php echo $ENVIRONMENT?>';
    </script>
    <script src="<?php echo $host;?>assets/js/custom_js/GoogleMaps.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/GlobalFunction.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/LocationManagement.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/MediaAndAdvertising/PackageToursAdsView.js"></script>
    <!-- <script src="<?php echo $host;?>assets/js/custom_js/CustomsDataTable.js"></script> -->
       
</body>

</html>