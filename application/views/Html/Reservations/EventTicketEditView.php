<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Content | Management System</title>

    <?php $this->load->view('Html/include/Header_css.php');?>  

</head>
<body>
    <!-- Preloader -->
    <div class="preloader" id="preloader">
        <div class="status" id="status"></div>
    </div>
    <div id="wrapper">
        <!-- SideMenu -->
        <?php $this->load->view('Html/include/SideMenu.php');?>
        <!-- End of SideMenu -->

        <div id="page-wrapper" class="gray-bg">
            <!-- Header -->
            <?php $this->load->view('Html/include/Header.php');?>
            <!-- End of Header -->

            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2><?=$this->lang->line("sidebar_menu_Reservations");?></h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="/dasta_thailand/html/Main">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <?=$this->lang->line("sidebar_menu_Reservations");?>
                        </li>
                        <li class="breadcrumb-item active">
                            <a href="/dasta_thailand/html/Reservations/EventTicket"><?=$this->lang->line("sidebar_menu_EventTicket");?></a>
                        </li>
                        <li class="breadcrumb-item active">
                        <strong>แก้ไข<?=$this->lang->line("sidebar_menu_EventTicket");?> [ <?php echo $data_item[0]->itmes_topicThai;?> ]</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
            
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="tabs-container">
                            <ul class="nav nav-tabs" role="tablist">
                                <li>
                                    <a class="nav-link active" data-toggle="tab" href="#tab-1" id="tabTH"><?=$this->lang->line("lang_th");?></a>
                                </li>
                                <li>
                                    <a class="nav-link" data-toggle="tab" href="#tab-2" id="tabENG"><?=$this->lang->line("lang_en");?></a>
                                </li>
                                <li>
                                    <a class="nav-link" data-toggle="tab" href="#tab-3" id="tabLAOS"><?=$this->lang->line("lang_lao");?></a>
                                </li>
                                <li>
                                    <a class="nav-link" data-toggle="tab" href="#tab-4" id="tabCHN"><?=$this->lang->line("lang_chn");?></a>
                                </li>
                            </ul>
                            <div id="summernote"></div>
                            <div class="tab-content">
                                <!--tab-1 Thai -->
                                <div role="tabpanel" id="tab-1" class="tab-pane active">
                                    <div class="">
                                        <div class="row">
                                            <div class="col-lg-12"><!-- รูปภาพหรือวีดีโอหน้าปก -->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รูปภาพหรือวีดีโอหน้าปก</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <h2>
                                                            ภาพหน้าปก
                                                        </h2>
                                                        <p>สามารถเพิ่มกี่รูปก็ได้</p>
                                                        <div class="form-group  row">
                                                            <div class="col-sm-12">
                                                                <div class="owl-carousel owl-theme image_box_event_ticket" id="image_box_event_ticket">
                                                                 
                                                                </div>
                                                                <div class="text-left" style="margin-top: 10px;">
                                                                    <button class="btn btn-primary btn-xs add_image_field-Reservations" name="image_EventTicket">เพิ่มรูปภาพ</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12"> <!-- ลิ้งวีดีโอ youtube -->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>วีดีโอ (ถ้ามี)</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ลิ้งวีดีโอ youtube</label>
                                                            <div class="col-sm-10">
                                                                <input type="" placeholder="www.youtube" class="form-control" id="coverItem_url" value ="<?php echo $data_item[0]->CoverItems[0]->coverItem_url;?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12"> <!-- รายละเอียดราคา-->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดราคา</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ราคาผู้ใหญ่</label>
                                                            <div class="col-sm-10">
                                                                <input type="number" placeholder="ราคาผู้ใหญ่" class="form-control-sm form-control typenumberEventTicketPrice" min="0" max="10" id="items_priceguestAdult" value ="<?php echo $data_item[0]->items_priceguestAdult;?>">
                                                                <span class="note notifi" id="notifi_items_priceguestAdult"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ราคาเด็ก</label>
                                                            <div class="col-sm-10">
                                                                <input type="number" placeholder="ราคาเด็ก" class="form-control-sm form-control typenumberEventTicketPrice" min="0" max="10" id="items_priceguestChild" value ="<?php echo $data_item[0]->items_priceguestChild;?>">
                                                                <span class="note notifi" id="notifi_items_priceguestChild"></span>
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12"> <!-- รายละเอียด<?=$this->lang->line("sidebar_menu_EventTicket");?> Thai-->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียด<?=$this->lang->line("sidebar_menu_EventTicket");?></h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ชื่อ<?=$this->lang->line("sidebar_menu_EventTicket");?></label>
                                                            <div class="col-sm-10">
                                                                <input type="text" placeholder="ชื่อ<?=$this->lang->line("sidebar_menu_EventTicket");?>" class="form-control" id="itmes_topicThai" value ="<?php echo $data_item[0]->itmes_topicThai;?>">
                                                                <span class="note notifi" id="notifi_itmes_topicThai"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ประเภท<?=$this->lang->line("sidebar_menu_EventTicket");?></label>
                                                            <div class="col-sm-10">
                                                                <select data-placeholder="Choose a Country..." class="chosen-select namesubcategory" tabindex="1" id="namesubcategory">
                                                                </select>
                                                                <span class="note notifi" id="notifi_namesubcategory"></span>
                                                            </div>
                                                        </div>  
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="">
                                                            <h4>ไฮไลต์กิจกรรม</h4>
                                                        </div>
                                                        
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">รายละเอียด</label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด ไฮไลต์กิจกรรม" rows="5" id="items_highlightsThai"><?php echo $data_item[0]->items_highlightsThai;?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12"> <!-- ข้อมูลสถานที่ Thai-->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>ข้อมูลสถานที่</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ที่ตั้งประเทศ</label>
                                                            <div class="col-sm-10">
                                                                <select data-placeholder="Choose a Country..." class="chosen-select country" tabindex="1" id="country">
                                                                </select>
                                                                <span class="note notifi" id="notifi_country"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ที่ตั้งจังหวัด</label>
                                                            <div class="col-sm-10">
                                                                <select data-placeholder="Choose a Provinces..." class="chosen-select provinces" tabindex="1" id="provinces" disabled>
                                                                </select>
                                                                <span class="note notifi" id="notifi_provinces"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ที่ตั้งอำเภอ</label>
                                                            <div class="col-sm-10">
                                                                <select data-placeholder="Choose a Districts..." class="chosen-select districts" tabindex="1" id="districts" disabled>
                                                                </select>
                                                                <span class="note notifi" id="notifi_districts"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ที่ตั้งตำบล</label>
                                                            <div class="col-sm-10">
                                                                <select data-placeholder="Choose a Subdistricts..." class="chosen-select subdistricts" tabindex="1" id="subdistricts" disabled>
                                                                </select>
                                                                <span class="note notifi" id="notifi_subdistricts"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 col-form-label">ที่อยู่</label>
                                                            <div class="col-sm-10">
                                                                <textarea rows="4" class="form-control form-control-input flat-input contact" placeholder="รายละเอียดสถานที่ติดต่อ เช่น บ้านเลขที่" id="items_contactThai"><?php echo $data_item[0]->items_contactThai;?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12"><!-- ข้อมูลการติดต่อ -->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดการเปิด-ปิด</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">เลือกวันเปิด-ปิด</label>
                                                            <div class="col-sm-10">
                                                                <select data-placeholder="เลือกวันเปิด-ปิด" class="chosen-select-dayofweek" tabindex="1" multiple id="dayofweek">
                                                                </select>
                                                                <span class="note notifi" id="notifi_dayofweek"></span>
                                                            </div>
                                                        </div>
                                                        <h5>
                                                            เพิ่มรายละเอียดช่วงเวลาทำการ
                                                        </h5>
                                                        <div class="col-lg-12" id="create_field_timepicker">
                                                            <div class="" id="box_timepicker">
                                                            
                                                            </div>
                                                            <div class="text-left" style="margin-top: 10px;">
                                                                <button class="btn btn-primary btn-xs add_box_field_timepicker" name="timepicker">เพิ่มช่วงเวลา</button>
                                                            </div>
                                                        </div>
                                                        

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12"> <!-- GPS Thai-->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>ตำแหน่ง GPS</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <div id="map"></div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ละติจูล</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" placeholder="latitude e.g. 13.7244416" class="form-control latitude" id="items_latitude" maxlength="18" value ="<?php echo $data_item[0]->items_latitude;?>">
                                                                <span class="note notifi" id="notifi_items_latitude"></span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ลองจิจูล</label>
                                                            <div class="col-sm-10">
                                                                <input type="text" placeholder="longitude e.g. 100.3529128" class="form-control longitude" id="items_longitude" maxlength="18" value ="<?php echo $data_item[0]->items_longitude;?>">
                                                                <span class="note notifi" id="notifi_items_longitude"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12"><!-- รายละเอียดสิ่งที่จะได้พบ Thai-->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดสิ่งที่จะได้พบ</h5>
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">เกริ่นนำรายละเอียด</label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียดสิ่งที่จะได้พบ" rows="5" id="topicdetail_id_1_Thai"></textarea>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12"><!-- เลือกรูปภาพประกอบสถานที่ -->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>เลือกรูปภาพประกอบสถานที่</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <div class="col-sm-12">
                                                                <div class="owl-carousel owl-theme image_box_location_illustrations" id="image_box_location_illustrations">
                                                                 
                                                                </div>
                                                                <div class="text-left" style="margin-top: 10px;">
                                                                    <button class="btn btn-primary btn-xs add_image_field-Reservations" name="image_LocationIllustrations">เพิ่มรูปภาพ</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12"><!-- รายละเอียดเงื่อนไขในการจอง Thai-->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดเงื่อนไขในการจอง</h5>
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">วิธีการใช้บัตร<?=$this->lang->line("sidebar_menu_EventTicket");?>
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด วิธีการใช้บัตร<?=$this->lang->line("sidebar_menu_EventTicket");?>" rows="4" id="bookingConditioncol_detailThai_id_8"><?php echo $data_item[0]->BookingCondition[0]->bookingConditioncol_detailThai;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ข้อกำหนดและเงื่อนไข
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด ข้อกำหนดและเงื่อนไข" rows="4" id="bookingConditioncol_detailThai_id_9"><?php echo $data_item[0]->BookingCondition[1]->bookingConditioncol_detailThai;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">เวลาทำการ
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด เวลาทำการ" rows="4" id="bookingConditioncol_detailThai_id_10"><?php echo $data_item[0]->BookingCondition[2]->bookingConditioncol_detailThai;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">นโยบายการยกเลิกการจอง
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด นโยบายการยกเลิกการจอง" rows="4" id="bookingConditioncol_detailThai_id_11"><?php echo $data_item[0]->BookingCondition[3]->bookingConditioncol_detailThai;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">เรท ราคา
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด เรท ราคา" rows="4" id="bookingConditioncol_detailThai_id_12"><?php echo $data_item[0]->BookingCondition[4]->bookingConditioncol_detailThai;?></textarea>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <!--tab-2 English -->
                                <div role="tabpanel" id="tab-2" class="tab-pane">
                                    <div class="">
                                        <div class="row">
                                            <div class="col-lg-12"> <!-- รายละเอียด<?=$this->lang->line("sidebar_menu_EventTicket");?> English-->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียด<?=$this->lang->line("sidebar_menu_EventTicket");?></h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ชื่อ<?=$this->lang->line("sidebar_menu_EventTicket");?></label>
                                                            <div class="col-sm-10">
                                                                <input type="text" placeholder="ชื่อ<?=$this->lang->line("sidebar_menu_EventTicket");?>" class="form-control" id="itmes_topicEnglish" value ="<?php echo $data_item[0]->itmes_topicEnglish;?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="">
                                                            <h4>ไฮไลต์กิจกรรม</h4>
                                                        </div>
                                                        
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">รายละเอียด</label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด ไฮไลต์กิจกรรม" rows="5" id="items_highlightsEnglish"><?php echo $data_item[0]->items_highlightsEnglish;?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12"> <!-- ข้อมูลสถานที่ English-->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>ข้อมูลสถานที่</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 col-form-label">ที่อยู่</label>
                                                            <div class="col-sm-10">
                                                                <textarea rows="4" class="form-control form-control-input flat-input contact" placeholder="รายละเอียดสถานที่ติดต่อ เช่น บ้านเลขที่" id="items_contactEnglish"><?php echo $data_item[0]->items_contactEnglish;?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12"><!-- รายละเอียดสิ่งที่จะได้พบ English-->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดสิ่งที่จะได้พบ</h5>
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">เกริ่นนำรายละเอียด</label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียดสิ่งที่จะได้พบ" rows="5" id="topicdetail_id_1_English"></textarea>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12"><!-- เลือกรูปภาพประกอบสถานที่ -->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>เลือกรูปภาพประกอบสถานที่</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <div class="col-sm-12">
                                                                <div class="owl-carousel owl-theme image_box_location_illustrations_English" id="image_box_location_illustrations_English">
                                                                 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12"><!-- รายละเอียดเงื่อนไขในการจอง English-->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดเงื่อนไขในการจอง</h5>
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">วิธีการใช้บัตร<?=$this->lang->line("sidebar_menu_EventTicket");?>
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด วิธีการใช้บัตร<?=$this->lang->line("sidebar_menu_EventTicket");?>" rows="4" id="bookingConditioncol_detailEnglish_id_8"><?php echo $data_item[0]->BookingCondition[0]->bookingConditioncol_detailEnglish;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ข้อกำหนดและเงื่อนไข
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด ข้อกำหนดและเงื่อนไข" rows="4" id="bookingConditioncol_detailEnglish_id_9"><?php echo $data_item[0]->BookingCondition[1]->bookingConditioncol_detailEnglish;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">เวลาทำการ
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด เวลาทำการ" rows="4" id="bookingConditioncol_detailEnglish_id_10"><?php echo $data_item[0]->BookingCondition[2]->bookingConditioncol_detailEnglish;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">นโยบายการยกเลิกการจอง
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด นโยบายการยกเลิกการจอง" rows="4" id="bookingConditioncol_detailEnglish_id_11"><?php echo $data_item[0]->BookingCondition[2]->bookingConditioncol_detailEnglish;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">เรท ราคา
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด เรท ราคา" rows="4" id="bookingConditioncol_detailEnglish_id_12"><?php echo $data_item[0]->BookingCondition[4]->bookingConditioncol_detailEnglish;?></textarea>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--tab-3 Laos -->
                                <div role="tabpanel" id="tab-3" class="tab-pane">
                                    <div class="">
                                        <div class="row">
                                            <div class="col-lg-12"> <!-- รายละเอียด<?=$this->lang->line("sidebar_menu_EventTicket");?> Laos-->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียด<?=$this->lang->line("sidebar_menu_EventTicket");?></h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ชื่อ<?=$this->lang->line("sidebar_menu_EventTicket");?></label>
                                                            <div class="col-sm-10">
                                                                <input type="text" placeholder="ชื่อ<?=$this->lang->line("sidebar_menu_EventTicket");?>" class="form-control" id="itmes_topicLaos" value ="<?php echo $data_item[0]->itmes_topicLaos;?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="">
                                                            <h4>ไฮไลต์กิจกรรม</h4>
                                                        </div>
                                                        
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">รายละเอียด</label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด ไฮไลต์กิจกรรม" rows="5" id="items_highlightsLaos"><?php echo $data_item[0]->items_highlightsLaos;?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12"> <!-- ข้อมูลสถานที่ Laos-->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>ข้อมูลสถานที่</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 col-form-label">ที่อยู่</label>
                                                            <div class="col-sm-10">
                                                                <textarea rows="4" class="form-control form-control-input flat-input contact" placeholder="รายละเอียดสถานที่ติดต่อ เช่น บ้านเลขที่" id="items_contactLaos"><?php echo $data_item[0]->items_contactLaos;?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12"><!-- รายละเอียดสิ่งที่จะได้พบ Laos-->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดสิ่งที่จะได้พบ</h5>
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">เกริ่นนำรายละเอียด</label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียดสิ่งที่จะได้พบ" rows="5" id="topicdetail_id_1_Laos"></textarea>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12"><!-- เลือกรูปภาพประกอบสถานที่ -->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>เลือกรูปภาพประกอบสถานที่</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <div class="col-sm-12">
                                                                <div class="owl-carousel owl-theme image_box_location_illustrations_Laos" id="image_box_location_illustrations_Laos">
                                                                 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12"><!-- รายละเอียดเงื่อนไขในการจอง Laos-->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดเงื่อนไขในการจอง</h5>
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">วิธีการใช้บัตร<?=$this->lang->line("sidebar_menu_EventTicket");?>
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด วิธีการใช้บัตร<?=$this->lang->line("sidebar_menu_EventTicket");?>" rows="4" id="bookingConditioncol_detailLaos_id_8"><?php echo $data_item[0]->BookingCondition[0]->bookingConditioncol_detailLaos;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ข้อกำหนดและเงื่อนไข
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด ข้อกำหนดและเงื่อนไข" rows="4" id="bookingConditioncol_detailLaos_id_9"><?php echo $data_item[0]->BookingCondition[1]->bookingConditioncol_detailLaos;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">เวลาทำการ
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด เวลาทำการ" rows="4" id="bookingConditioncol_detailLaos_id_10"><?php echo $data_item[0]->BookingCondition[2]->bookingConditioncol_detailLaos;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">นโยบายการยกเลิกการจอง
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด นโยบายการยกเลิกการจอง" rows="4" id="bookingConditioncol_detailLaos_id_11"><?php echo $data_item[0]->BookingCondition[3]->bookingConditioncol_detailLaos;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">เรท ราคา
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด เรท ราคา" rows="4" id="bookingConditioncol_detailLaos_id_12"><?php echo $data_item[0]->BookingCondition[4]->bookingConditioncol_detailLaos;?></textarea>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--tab-4 Chinese -->
                                <div role="tabpanel" id="tab-4" class="tab-pane">
                                    <div class="">
                                        <div class="row">
                                            <div class="col-lg-12"> <!-- รายละเอียด<?=$this->lang->line("sidebar_menu_EventTicket");?> Chinese-->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียด<?=$this->lang->line("sidebar_menu_EventTicket");?></h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ชื่อ<?=$this->lang->line("sidebar_menu_EventTicket");?></label>
                                                            <div class="col-sm-10">
                                                                <input type="text" placeholder="ชื่อ<?=$this->lang->line("sidebar_menu_EventTicket");?>" class="form-control" id="itmes_topicChinese" value ="<?php echo $data_item[0]->itmes_topicChinese;?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="">
                                                            <h4>ไฮไลต์กิจกรรม</h4>
                                                        </div>
                                                        
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">รายละเอียด</label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด ไฮไลต์กิจกรรม" rows="5" id="items_highlightsChinese"><?php echo $data_item[0]->items_highlightsChinese;?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12"> <!-- ข้อมูลสถานที่ Chinese-->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>ข้อมูลสถานที่</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group row">
                                                            <label class="col-sm-2 col-form-label">ที่อยู่</label>
                                                            <div class="col-sm-10">
                                                                <textarea rows="4" class="form-control form-control-input flat-input contact" placeholder="รายละเอียดสถานที่ติดต่อ เช่น บ้านเลขที่" id="items_contactChinese"><?php echo $data_item[0]->items_contactChinese;?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12"><!-- รายละเอียดสิ่งที่จะได้พบ Chinese-->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดสิ่งที่จะได้พบ</h5>
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">เกริ่นนำรายละเอียด</label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียดสิ่งที่จะได้พบ" rows="5" id="topicdetail_id_1_Chinese"></textarea>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12"><!-- เลือกรูปภาพประกอบสถานที่ -->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>เลือกรูปภาพประกอบสถานที่</h5>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <div class="col-sm-12">
                                                                <div class="owl-carousel owl-theme image_box_location_illustrations_Chinese" id="image_box_location_illustrations_Chinese">
                                                                 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12"><!-- รายละเอียดเงื่อนไขในการจอง Chinese-->
                                                <div class="ibox ">
                                                    <div class="ibox-title  back-change">
                                                        <h5>รายละเอียดเงื่อนไขในการจอง</h5>
                                                        <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                                <i class="fa fa-chevron-up"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-content">
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">วิธีการใช้บัตร<?=$this->lang->line("sidebar_menu_EventTicket");?>
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด วิธีการใช้บัตร<?=$this->lang->line("sidebar_menu_EventTicket");?>" rows="4" id="bookingConditioncol_detailChinese_id_8"><?php echo $data_item[0]->BookingCondition[0]->bookingConditioncol_detailChinese;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">ข้อกำหนดและเงื่อนไข
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด ข้อกำหนดและเงื่อนไข" rows="4" id="bookingConditioncol_detailChinese_id_9"><?php echo $data_item[0]->BookingCondition[1]->bookingConditioncol_detailChinese;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">เวลาทำการ
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด เวลาทำการ" rows="4" id="bookingConditioncol_detailChinese_id_10"><?php echo $data_item[0]->BookingCondition[2]->bookingConditioncol_detailChinese;?></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">นโยบายการยกเลิกการจอง
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด นโยบายการยกเลิกการจอง" rows="4" id="bookingConditioncol_detailChinese_id_11"><?php echo $data_item[0]->BookingCondition[3]->bookingConditioncol_detailChinese;?></textarea>
                                                            </div>
                                                        </div>
                                                         <div class="form-group  row">
                                                            <label class="col-sm-2 col-form-label">เรท ราคา
                                                                <br>
                                                                <p>รายละเอียด</p>
                                                            </label>
                                                            <div class="col-sm-10">
                                                                <textarea class="form-control" placeholder="รายละเอียด เรท ราคา" rows="4" id="bookingConditioncol_detailChinese_id_12"><?php echo $data_item[0]->BookingCondition[4]->bookingConditioncol_detailChinese;?></textarea>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="text-right">
                                    <div class="col">
                                        <span class="note notifi" id="notifi_error_save"></span>
                                    </div>
                                    <button class="btn btn-primary btn-xl" id="save">บันทึก</button>
                                    
                                </div>
                                <br>
                            </div>
                        </div> 
                    </div> 
                </div> 
            </div> 

            <!-- Footer -->
            <?php $this->load->view('Html/include/Footer.php');?>
            <!-- End of Footer -->
        </div>  
    </div>
        
    <?php $this->load->view('Html/include/Footer_js.php');?>
    <script>
        var host = "<?php echo $host;?>html";
        var items_id = <?php echo $data_item[0]->items_id;?>;
        var table;
        var map;
        var cropper;
        var gmarkers = [];
        var CropData,CropBoxData,CanvasData;
        var MenuName = "EventTicket";
        var TypeMenu = "Reservations";
        var UseGoogleMap = '<?php echo $ENVIRONMENT?>';

        var box_cover_image_event_ticket = 0;
        var count_box_cover_image_event_ticket = <?php echo count($data_item[0]->CoverItems);?>;

        var box_cover_image_location_illustrations=0;
        var count_box_cover_image_location_illustrations=<?php echo count($data_item[0]->Detail[0]->Photo);?>;
        
        var box_field_timepicker = 0;
        var count_box_field_timepicker =<?php echo count($data_item[0]->TimePeriod);?>;

        var country_id = <?php echo $data_item[0]->country_id;?>;
        var provinces_id = <?php echo $data_item[0]->provinces_id;?>;
        var districts_id = <?php echo $data_item[0]->districts_id;?>;
        var subdistricts_id = <?php echo $data_item[0]->subdistricts_id;?>;
        
        var subcategory_id = <?php echo $data_item[0]->subcategory_id;?>;
        
        var CoverItems = JSON.parse("<?php echo escapeJsonString(json_encode($data_item[0]->CoverItems)) ?>");
        var PhotoDetail = JSON.parse("<?php echo escapeJsonString(json_encode($data_item[0]->Detail[0]->Photo)) ?>");
        var Detail = JSON.parse("<?php echo escapeJsonString(json_encode($data_item[0]->Detail[0])) ?>");
        var TimePeriod = JSON.parse("<?php echo escapeJsonString(json_encode($data_item[0]->TimePeriod)) ?>");

        var dayOCVal = '<?php echo $data_item[0]->TimePeriod[0]->item_dayOpen.($data_item[0]->TimePeriod[0]->item_dayOpen == $data_item[0]->TimePeriod[0]->item_dayClose?'':','.$data_item[0]->TimePeriod[0]->item_dayClose);?>';
        var str_array = dayOCVal.split(',');
        var type_page = "html";
    </script>
    <script src="<?php echo $host;?>assets/js/custom_js/GoogleMaps.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/GlobalFunction.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/LocationManagement.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/Reservations/EventTicketEditView.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/DateManagement.js"></script>
    

</body>

</html>
<?php
function escapeJsonString($value) { # list from www.json.org: (\b backspace, \f formfeed)
    $escapers = array("\\", "/", "\"", "\n", "\r", "\t", "\x08", "\x0c");
    $replacements = array("\\\\", "\\/", "\\\"", "\\n", "\\r", "\\t", "\\f", "\\b");
    $result = str_replace($escapers, $replacements, $value);
    return $result;
}
?>