<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="block m-t-xs font-bold"><?php echo $this->session->userdata("user_firstName")." ".$this->session->userdata("user_lastName");?></span>
                        <span class="text-muted text-xs block">
                            <?php 
                            switch (intval($this->session->userdata("UserRole_userRole_id"))) {
                                case 1:
                                    echo "Super Administrator";
                                    break;
                                case 2:
                                    echo "Administrator Manager";
                                    break;
                                case 3:
                                    echo "Administrator";
                                    break;
                            }
                            
                            ?>
                        </span>
                        <span class="text-muted text-xs block">menu<b class="caret"></b></span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li>
                            <a class="dropdown-item" data-toggle="modal" data-target="#EditInfomationModal" href="#" id="EditInfomation"><?=$this->lang->line("text_edit_infor");?></a>
                        </li>
                        <li>
                            <a class="dropdown-item" data-toggle="modal" data-target="#ChangePasswordModal" href="#" id="ChangePassword"><?=$this->lang->line("changePassword");?></a>
                        </li>
                        <li>
                            <a class="dropdown-item" href="/dasta_thailand/html/Login/logout"><?=$this->lang->line("Logout");?></a>
                        </li>
                    </ul>
                </div>
                <div class="logo-element">
                    Menu+
                </div>
            </li>
            <li id="sidebar_Index">
                <a href="/dasta_thailand/html/Main">
                    <i class="fa fa-th-large"></i>
                    <span class="nav-label"><?=$this->lang->line("sidebar_menu_main");?></span>
                </a>
            </li>
            <li id="sidebar_ProgramTour">
                <a href="/dasta_thailand/html/ProgramTour">
                    <i class="fa fa-globe"></i>
                    <span class="nav-label"><?=$this->lang->line("sidebar_menu_ProgramTour");?></span>
                    </span>
                </a>
            </li>
            <li id="sidebar_Attractions">
                <a href="/dasta_thailand/html/Items/Attractions">
                    <i class="fa fa-camera-retro"></i>
                    <span class="nav-label"><?=$this->lang->line("sidebar_menu_Attractions");?></span>
                </a>
            </li>
            <li id="sidebar_Restaurant">
                <a href="/dasta_thailand/html/Items/Restaurant">
                    <i class="fa fa-cutlery"></i>
                    <span class="nav-label"><?=$this->lang->line("sidebar_menu_Restaurant");?></span>
                </a>
            </li>
            <li id="sidebar_Shopping">
                <a href="/dasta_thailand/html/Items/Shopping">
                    <i class="fa fa-gift"></i>
                    <span class="nav-label"><?=$this->lang->line("sidebar_menu_Shopping");?></span>
                </a>
            </li>
            <li id="sidebar_Hotel">
                <a href="/dasta_thailand/html/Items/Hotel">
                    <i class="fa fa-hotel"></i>
                    <span class="nav-label"><?=$this->lang->line("sidebar_menu_Hotel");?></span>
                </a>
            </li>
            <li id="sidebar_ImportantContactPlaces">
                <a href="/dasta_thailand/html/Items/ImportantContactPlaces">
                    <i class="fa fa-bank"></i>
                    <span class="nav-label"><?=$this->lang->line("sidebar_menu_ImportantContactPlaces");?></span>
                </a>
            </li>

            <li id="sidebar_MediaAndAdvertising_li">
                <a href="" id="sidebar_MediaAndAdvertising_a">
                    <i class="fa fa-area-chart"></i>
                    <span class="nav-label"><?=$this->lang->line("sidebar_menu_MediaAndAdvertising");?></span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse" id="sidebar_MediaAndAdvertising_ul">
                    <li id="sidebar_VideoContent">
                        <a href="/dasta_thailand/html/MediaAndAdvertising/VideoContent"><?=$this->lang->line("sidebar_menu_VideoContent");?></a>
                    </li>
                    <li id="sidebar_Sticker">
                        <a href="/dasta_thailand/html/MediaAndAdvertising/Sticker"><?=$this->lang->line("sidebar_menu_Sticker");?></a>
                    </li>
                    <li id="sidebar_Coupon">
                        <a href="/dasta_thailand/html/MediaAndAdvertising/Coupon"><?=$this->lang->line("sidebar_menu_Coupon");?></a>
                    </li>
                    <li id="sidebar_CoverProgramTourOfficial">
                        <a href="/dasta_thailand/html/MediaAndAdvertising/CoverProgramTourOfficial"><?=$this->lang->line("sidebar_menu_CoverProgramTourOfficial");?></a>
                    </li>
                    <li id="sidebar_News">
                        <a href="/dasta_thailand/html/MediaAndAdvertising/News"><?=$this->lang->line("sidebar_menu_News");?></a>
                    </li>
                    <li id="sidebar_CarRentAds">
                        <a href="/dasta_thailand/html/MediaAndAdvertising/CarRentAds"><?=$this->lang->line("sidebar_menu_CarRentAds");?></a>
                    </li>
                    <li id="sidebar_PackageToursAds">
                        <a href="/dasta_thailand/html/MediaAndAdvertising/PackageToursAds"><?=$this->lang->line("sidebar_menu_PackageToursAds");?></a>
                    </li>
                    <li id="sidebar_EventTicketAds">
                        <a href="/dasta_thailand/html/MediaAndAdvertising/EventTicketAds"><?=$this->lang->line("sidebar_menu_EventTicketAds");?></a>
                    </li>
                    <li id="sidebar_HotelAds">
                        <a href="/dasta_thailand/html/MediaAndAdvertising/HotelAds"><?=$this->lang->line("sidebar_menu_HotelAds");?></a>
                    </li>
                </ul>
            </li>
            
            <li id="sidebar_Reservations_li">
                <a href="" id="sidebar_Reservations_a">
                    <i class="fa fa-calendar-plus-o"></i>
                    <span class="nav-label"><?=$this->lang->line("sidebar_menu_Reservations");?></span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                    <li id="sidebar_PackageTours">
                        <a href="/dasta_thailand/html/Reservations/PackageTours"><?=$this->lang->line("sidebar_menu_PackageTours");?></a>
                    </li>
                    <li id="sidebar_EventTicket">
                        <a href="/dasta_thailand/html/Reservations/EventTicket"><?=$this->lang->line("sidebar_menu_EventTicket");?></a>
                    </li>
                    <li id="sidebar_CarRent">
                        <a href="/dasta_thailand/html/Reservations/CarRent"><?=$this->lang->line("sidebar_menu_CarRent");?></a>
                    </li>
                </ul>
            </li>
            
           
            <li id="sidebar_Booking_li">
                <a href="" id="sidebar_Booking_a">
                    <i class="fa fa-calendar-plus-o"></i>
                        <span class="nav-label"><?=$this->lang->line("sidebar_menu_Booking");?></span>
                        <span class="label label-warning float-right">6</span>
                     <!-- <span class="fa arrow"></span> -->
                </a>
                <ul class="nav nav-second-level collapse">
                    <li id="sidebar_BookingPackageTours">
                        <a href="/dasta_thailand/html/BookingReservations/BookingPackageTours"><?=$this->lang->line("sidebar_menu_BookingPackageTours");?></a>
                    </li>
                    <li id="sidebar_BookingEventTicket">
                        <a href="/dasta_thailand/html/BookingReservations/BookingEventTicket"><?=$this->lang->line("sidebar_menu_BookingEventTicket");?></a>
                    </li>
                    <li id="sidebar_BookingCarRent">
                        <a href="/dasta_thailand/html/BookingReservations/BookingCarRent"><?=$this->lang->line("sidebar_menu_BookingCarRent");?></a>
                    </li>
                </ul>
            </li> 
            <li id="sidebar_Approval_li">
                <a href="" id="sidebar_Approval_a">
                    <i class="fa fa-cube"></i>
                    <span class="nav-label"><?=$this->lang->line("sidebar_menu_Approval");?></span>
                    <?php 
                        if(intval($Approval_Item["All"]) == 0)
                            echo "<span class=\"fa arrow\" id=\"ApprovalAllWarning\"></span>";
                        else
                            echo "<span class=\"label label-warning float-right\" id=\"ApprovalAllWarning\">".$Approval_Item["All"]."</span>";
                        
                    ?>
                    </span>
                </a>
                <ul class="nav nav-second-level collapse" id="sidebar_Approval_ui">
                    <li id="sidebar_ApprovalProgramTour">
                        <a href="/dasta_thailand/html/Approval/ApprovalProgramTour"><?=$this->lang->line("sidebar_menu_ApprovalProgramTour");?>
                            <span class="float-right" id="ApprovalProgramTour"><?php echo (intval($Approval_Item["ProgramTour"]) == 0?"":$Approval_Item["ProgramTour"]);?></span>
                        </a>
                    </li>
                    <li id="sidebar_ApprovalAttractions">
                        <a href="/dasta_thailand/html/Approval/ApprovalAttractions"><?=$this->lang->line("sidebar_menu_ApprovalAttractions");?>
                            <span class="float-right" id="ApprovalAttractions"><?php echo (intval($Approval_Item["Attractions"]) == 0?"":$Approval_Item["Attractions"]);?></span>
                        </a>
                    </li>
                    <li id="sidebar_ApprovalRestaurant">
                        <a href="/dasta_thailand/html/Approval/ApprovalRestaurant"><?=$this->lang->line("sidebar_menu_ApprovalRestaurant");?>
                            <span class="float-right" id="ApprovalRestaurant"><?php echo (intval($Approval_Item["Restaurant"]) == 0?"":$Approval_Item["Restaurant"]);?></span>
                        </a>
                    </li>
                    <li id="sidebar_ApprovalShopping">
                        <a href="/dasta_thailand/html/Approval/ApprovalShopping"><?=$this->lang->line("sidebar_menu_ApprovalShopping");?>
                            <span class="float-right" id="ApprovalShopping"><?php echo (intval($Approval_Item["Shopping"]) == 0?"":$Approval_Item["Shopping"]);?></span>
                        </a>
                    </li>
                    <li id="sidebar_ApprovalHotel">
                        <a href="/dasta_thailand/html/Approval/ApprovalHotel"><?=$this->lang->line("sidebar_menu_ApprovalHotel");?>
                            <span class="float-right" id="ApprovalHotel"><?php echo (intval($Approval_Item["Hotel"]) == 0?"":$Approval_Item["Hotel"]);?></span>
                        </a>
                    </li>
                    <li id="sidebar_ApprovalPackageTours">
                        <a href="/dasta_thailand/html/Approval/ApprovalPackageTours"><?=$this->lang->line("sidebar_menu_ApprovalPackageTours");?>
                            <span class="float-right" id="ApprovalPackageTours"><?php echo (intval($Approval_Item["PackageTours"]) == 0?"":$Approval_Item["PackageTours"]);?></span>
                        </a>
                    </li>
                    <li id="sidebar_ApprovalEventTicket">
                        <a href="/dasta_thailand/html/Approval/ApprovalEventTicket"><?=$this->lang->line("sidebar_menu_ApprovalEventTicket");?>
                            <span class="float-right" id="ApprovalEventTicket"><?php echo (intval($Approval_Item["EventTicket"]) == 0?"":$Approval_Item["EventTicket"]);?></span>
                        </a>
                    </li>
                    <li id="sidebar_ApprovalCarRent">
                        <a href="/dasta_thailand/html/Approval/ApprovalCarRent"><?=$this->lang->line("sidebar_menu_ApprovalCarRent");?>
                            <span class="float-right" id="ApprovalCarRent"><?php echo (intval($Approval_Item["CarRent"]) == 0?"":$Approval_Item["CarRent"]);?></span>
                        </a>
                    </li>
                </ul>
            </li>
            <li id="sidebar_Notifications">
                <a href="/dasta_thailand/html/Notifications">
                    <i class="fa fa-comments"></i>
                    <span class="nav-label"><?=$this->lang->line("sidebar_menu_Notifications");?></span>
                    <?php 
                        if(intval($UnReadMessages[0]->count_read) == 0)
                            echo "<span class=\"float-right UnReadMessages\" id=\"\" name=\"UnReadMessages\"></span>";
                        else
                            echo "<span class=\"float-right UnReadMessages label label-warning \" id=\"\" name=\"UnReadMessages\">".$UnReadMessages[0]->count_read."</span>";
                        
                    ?>
                </a>
            </li>
            <li id="sidebar_PermissionGroup_li">
                <a href="" id ="sidebar_PermissionGroup_a">
                    <i class="fa fa-id-card-o"></i>
                    <span class="nav-label">สิทธิ์การใช้งาน</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level collapse">
                <?php 
                    if($this->session->userdata('UserRole_userRole_id') == 1)
                        echo '
                            <li  id="sidebar_UserManagement">
                                <a href="/dasta_thailand/html/UserManagement">จัดการผู้ใช้งาน</a>
                            </li>
                            <li id="sidebar_ProvinceGroup">
                                <a href="/dasta_thailand/html/ProvinceGroup">จัดการกลุ่มจังหวัด</a>
                            </li>';
                    if($this->session->userdata('UserRole_userRole_id') == 2)
                        echo '
                        <li id="sidebar_UserManagement">
                            <a href="/dasta_thailand/html/UserManagement">จัดการผู้ใช้งาน</a>
                        </li>';
                ?>
                    <!-- <li>
                        <a href="">ตรวจสอบการใช้งานระบบ</a>
                    </li> -->
                </ul>
            </li>
           
            <!-- <li>
                <a href="#">
                    <i class="fa fa-credit-card"></i>
                    <span class="nav-label">การบริหารผู้ใช้งาน</span>
                </a>
            </li> -->
        </ul>
    </div>
</nav>
<?php include('ChangePasswordModal.php');?>  
<?php include('EditInfomationModal.php');?>  