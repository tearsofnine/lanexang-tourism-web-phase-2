<!-- Mainly scripts -->
<script src="<?php echo $host;?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo $host;?>assets/js/popper.min.js"></script>
<script src="<?php echo $host;?>assets/js/bootstrap.min.js"></script>

<script src="<?php echo $host;?>assets/js/jquery.magnific-popup.min.js"></script>

<!-- FooTable -->
<!-- <script src="<?php echo $host;?>assets/js/plugins/footable/footable.all.min.js"></script> -->

<!-- Mainly scripts -->

 <!-- <script src="<?php echo $host;?>assets/js/jquery-3.1.1.min.js"></script> -->
<!--<script src="<?php echo $host;?>assets/js/popper.min.js"></script>
<script src="<?php echo $host;?>assets/js/bootstrap.min.js"></script> -->



<!-- Custom and plugin javascript -->
<script src="<?php echo $host;?>assets/js/inspinia.js"></script>
<script src="<?php echo $host;?>assets/js/plugins/pace/pace.min.js"></script>
<script src="<?php echo $host;?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- SUMMERNOTE -->
<script src="<?php echo $host;?>assets/js/plugins/summernote/summernote-bs4.js"></script>

<!-- Chosen -->
<script src="<?php echo $host;?>assets/js/plugins/chosen/chosen.jquery.js"></script>

<!-- JSKnob -->
<script src="<?php echo $host;?>assets/js/plugins/jsKnob/jquery.knob.js"></script>

<!-- Input Mask-->
<script src="<?php echo $host;?>assets/js/plugins/jasny/jasny-bootstrap.min.js"></script>

<!-- Data picker -->
<script src="<?php echo $host;?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>

<!-- NouSlider -->
<script src="<?php echo $host;?>assets/js/plugins/nouslider/jquery.nouislider.min.js"></script>

<!-- Switchery -->
<script src="<?php echo $host;?>assets/js/plugins/switchery/switchery.js"></script>

<!-- IonRangeSlider -->
<script src="<?php echo $host;?>assets/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

<!-- iCheck -->
<script src="<?php echo $host;?>assets/js/plugins/iCheck/icheck.min.js"></script>

<!-- MENU -->
<script src="<?php echo $host;?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

<!-- Color picker -->
<script src="<?php echo $host;?>assets/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

<!-- Clock picker -->
<script src="<?php echo $host;?>assets/js/plugins/clockpicker/clockpicker.js"></script>

<!-- Image cropper -->
<script src="<?php echo $host;?>assets/js/plugins/cropper/cropper.js"></script>

<!-- Date range use moment.js same as full calendar plugin -->
<script src="<?php echo $host;?>assets/js/plugins/fullcalendar/moment.min.js"></script>

<!-- Date range picker -->
<script src="<?php echo $host;?>assets/js/plugins/daterangepicker/daterangepicker.js"></script>

<!-- Select2 -->
<script src="<?php echo $host;?>assets/js/plugins/select2/select2.full.min.js"></script>

<!-- TouchSpin -->
<script src="<?php echo $host;?>assets/js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

<!-- Tags Input -->
<script src="<?php echo $host;?>assets/js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

<!-- Dual Listbox -->
<script src="<?php echo $host;?>assets/js/plugins/dualListbox/jquery.bootstrap-duallistbox.js"></script>


<!-- Custom and plugin javascript -->
<script src="<?php echo $host;?>assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<!-- <script src="<?php echo $host;?>assets/js/moment.js"></script> -->

<script src="<?php echo $host;?>assets/js/plugins/pickadate/picker.js"></script>
<script src="<?php echo $host;?>assets/js/plugins/pickadate/picker.date.js"></script>
<script src="<?php echo $host;?>assets/js/plugins/pickadate/legacy.js"></script>
<script src="<?php echo $host;?>assets/js/plugins/pickadate/picker.time.js"></script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDrdQG5bfvPftYnMMdd8dOdgcl1GvODN6U&callback=initMap"type="text/javascript"></script>

<script src="<?php echo $host;?>assets/js/plugins/owlcarousel/owl.carousel.min.js"></script>

<!-- chosen-order -->
<script src="<?php echo $host;?>assets/js/plugins/chosenorder/chosen.order.jquery.min.js"></script>

<script src="<?php echo $host;?>assets/js/custom_js/NotificationsEvent.js"></script>

<!-- Insert these scripts at the bottom of the HTML, but before you use any Firebase services -->

<!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-app.js"></script>

<!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
<script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-analytics.js"></script>

<!-- Add Firebase products that you want to use -->
<script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-firestore.js"></script>

<script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-messaging.js"></script>

<script src="<?php echo $host;?>assets/js/custom_js/firebase-init.js"></script>
