<div class="modal fade bd-example-modal-xl" id="ViewItemsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" style="color: #1AB394;" id="header_view_modal">ViewItemsModal</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            
            <div class="col-sm-12 col-xs-12">
                <div class="ibox selected">
                    <div class="ibox-content">
                        <div class="tab-content">
                            <div id="" class="">
                                <div class="client-detail">
                                    <div class="full-height-scroll" id="items_content">
                                        <?php
                                            switch ($ModalView) {
                                                case 'ProgramTour':
                                                    $this->load->view('Html/ModalView/ModalViewProgramTour.php');
                                                break;
                                                case 'Attractions':
                                                    $this->load->view('Html/ModalView/ModalViewAttractions.php');
                                                break;
                                                case 'Hotel':
                                                    $this->load->view('Html/ModalView/ModalViewHotel.php');
                                                break;
                                                case 'ImportantContactPlaces':
                                                    $this->load->view('Html/ModalView/ModalViewImportantContactPlaces.php');
                                                break;
                                                case 'Restaurant':
                                                    $this->load->view('Html/ModalView/ModalViewRestaurant.php');
                                                break;
                                                case 'Shopping':
                                                    $this->load->view('Html/ModalView/ModalViewShopping.php');
                                                break;
                                                case 'CarRent':
                                                    $this->load->view('Html/ModalView/ModalViewCarRent.php');
                                                break;
                                                case 'EventTicket':
                                                    $this->load->view('Html/ModalView/ModalViewEventTicket.php');
                                                break;
                                                case 'PackageTours':
                                                    $this->load->view('Html/ModalView/ModalViewPackageTours.php');
                                                break;
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <div class="row">
                   
                    <div class="row">
                        <div class="col-md-6" >
                            <button class="btn btn-primary" type="button" data-dismiss="modal">Cancel</button>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>