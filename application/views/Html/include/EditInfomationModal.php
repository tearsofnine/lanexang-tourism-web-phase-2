<div class="modal fade" id="EditInfomationModal" tabindex="-1" role="dialog" aria-labelledby="EditInfomationModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="EditInfomationModalLabel"><?= $this->lang->line("text_edit_infor"); ?></h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <label for="thTravelPlaceDescription" class="col-sm-4 col-xs-4 col-form-label">ชื่อ</label>
                    <div class="col-sm-6 col-xs-6">
                        <input type="text" placeholder="ชื่อ" class="form-control form-control-input flat-input " id="info_user_firstName" value="<?=$this->session->userdata("user_firstName")?>">
                        <span class="note notifi" id="notifi_info_user_firstName"><span>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="thTravelPlaceDescription" class="col-sm-4 col-xs-4 col-form-label">นามสกุล</label>
                    <div class="col-sm-6 col-xs-6">
                        <input type="text" placeholder="นามสกุล" class="form-control form-control-input flat-input " id="info_user_lastName"  value="<?=$this->session->userdata("user_lastName")?>">
                        <span class="note notifi" id="notifi_info_user_lastName"><span>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="thTravelPlaceDescription" class="col-sm-4 col-xs-4 col-form-label">เพศ</label>
                    <div class="col-sm-6 col-xs-6">
                        <select class="form-control chosen-select" tabindex="1" id="info_user_gender">
                            <option value="">เพศ</option>
                            <option value="Male" <?php echo ($this->session->userdata("user_gender") == "Male"?"selected":"")?> >ชาย</option>
                            <option value="Female" <?php echo ($this->session->userdata("user_gender") == "Female"?"selected":"")?>>หญิง</option>
                        </select>
                        <span class="note notifi" id="notifi_info_user_gender"><span>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="thTravelPlaceDescription" class="col-sm-4 col-xs-4 col-form-label">อายุ</label>
                    <div class="col-sm-6 col-xs-6">
                        <input type="number" placeholder="" class="form-control form-control-input flat-input " id="info_user_age" min="0" max="99" value="<?=$this->session->userdata("user_age")?>">
                        <span class="note notifi" id="notifi_info_user_age"><span>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="thTravelPlaceDescription" class="col-sm-4 col-xs-4 col-form-label">เบอร์โทรศัพท์</label>
                    <div class="col-sm-6 col-xs-6">
                        <input type="text" placeholder="" class="form-control form-control-input flat-input telephone" id="info_user_phone" maxlength="10" value="<?=$this->session->userdata("user_phone")?>">
                        <span class="note notifi" id="notifi_info_user_phone"><span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col">
                        <span class="note notifi" id="notifi_info_error_save_info"><span>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-success" type="button" id="btn_save_info">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>