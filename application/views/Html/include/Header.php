<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#">
                <i class="fa fa-bars"></i>
            </a>
        </div>
        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                    <i class="fa fa-envelope"></i>
                    <?php 
                        if(intval($UnReadMessages[0]->count_read) == 0)
                            echo "<span class=\"UnReadMessages\" id=\"\" name=\"UnReadMessages\"></span>";
                        else
                            echo "<span class=\"label label-warning UnReadMessages\" id=\"\" name=\"UnReadMessages\">".$UnReadMessages[0]->count_read."</span>";
                        
                    ?>
                    <!-- <span class="label label-warning">16</span> -->
                </a>
                <div id="main_notifications_table">
                    <ul class="dropdown-menu dropdown-messages dropdown-menu-right" id="notifications_table">
                    </ul>
                </div>
            </li>
            <li>
                <a href="/dasta_thailand/html/Login/logout">
                    <i class="fa fa-sign-out"></i> <?=$this->lang->line("Logout");?>
                </a>
            </li>
        </ul>
    </nav>
</div>