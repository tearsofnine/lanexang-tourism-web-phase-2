<div class="row">
    <div class="col-lg-8">
        <div class="ibox">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h5 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" id="SearchBoxText">ค้นหาแหล่งท่องเที่ยวในระบบ
                                <span class="float-right">
                                    <i class="fa fa-chevron-down"></i>
                                </span>
                            </a>
                        </h5>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="ibox-content">
                                <div class="form-group  row">
                                    <label class="col-sm-2 col-form-label">ประเภท</label>
                                    <div class="col-sm-10">
                                        <select data-placeholder="Choose a Country..." class="chosen-select namesubcategory" tabindex="-1" id="namesubcategory">
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group  row">
                                    <label class="col-sm-2 col-form-label">ระยะวัน</label>
                                    <div class="col-sm-10">
                                        <select data-placeholder="Choose a Num Dates ..." class="chosen-select" tabindex="-1" id="NumDatesTrip">
                                           
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group  row">
                                    <label class="col-sm-2 col-form-label">เลือกขอบเขตแพคเกจทัวร์</label>
                                    <div class="col-sm-10">
                                        <select data-placeholder="เลือกขอบเขตจังหวัด" class="chosen-select-province-boundary" tabindex="1" multiple id="province_boundary">
                                        </select>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <button class="btn btn-danger btn-sm btn_clear" id="btn_search_clear">ล้างข้อมูล </button>
                                    <button class="btn btn-outline btn-primary btn-sm" id="btn_search">ค้นหา </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="input-group">
            <input type="text" id="CustomSearchBox" placeholder="Search" class="form-control-sm form-control" style="height: 48px">
        </div>
    </div>
</div>