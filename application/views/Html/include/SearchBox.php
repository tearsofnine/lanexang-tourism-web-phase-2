<div class="row">
    <div class="col-lg-8">
        <div class="ibox">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h5 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" id="SearchBoxText">ค้นหาแหล่งท่องเที่ยวในระบบ
                                <span class="float-right">
                                    <i class="fa fa-chevron-down"></i>
                                </span>
                            </a>
                        </h5>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="ibox-content">
                                <div class="form-group  row">
                                    <label class="col-sm-2 col-form-label"><?=$this->lang->line("category");?></label>
                                    <div class="col-sm-10">
                                        <select data-placeholder="Choose a Country..." class="chosen-select namesubcategory" tabindex="-1" id="namesubcategory">
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group  row">
                                    <label class="col-sm-2 col-form-label"><?=$this->lang->line("country");?></label>
                                    <div class="col-sm-10">
                                        <select data-placeholder="Choose a Country..." class="chosen-select country" tabindex="-1" id="country">
                                           
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group  row">
                                    <label class="col-sm-2 col-form-label"><?=$this->lang->line("province");?></label>
                                    <div class="col-sm-10">
                                        <select data-placeholder="Choose a Provinces..." class="chosen-select provinces" tabindex="-1" id="provinces" disabled>
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group  row">
                                    <label class="col-sm-2 col-form-label"><?=$this->lang->line("district");?></label>
                                    <div class="col-sm-10">
                                        <select data-placeholder="Choose a Districts..." class="chosen-select districts" tabindex="-1" id ="districts" disabled>
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <button class="btn btn-danger btn-sm btn_clear" id="btn_search_clear"><?=$this->lang->line("clear_data");?> </button>
                                    <button class="btn btn-outline btn-primary btn-sm" id="btn_search"><?=$this->lang->line("search");?> </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="input-group">
            <input type="text" id="CustomSearchBox" placeholder="Search" class="form-control-sm form-control" style="height: 48px">
        </div>
    </div>
</div>