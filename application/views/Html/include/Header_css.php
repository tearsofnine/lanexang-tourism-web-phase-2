<!-- <link href="<?php echo $host;?>assets/css/bootstrap.min.css" rel="stylesheet"> -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<link href="<?php echo $host;?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">

<!-- <meta http-equiv="refresh" content="10" /> -->

<link href="<?php echo $host;?>assets/css/magnific-popup.css" rel="stylesheet">

<link href="<?php echo $host;?>assets/css/plugins/summernote/summernote-bs4.css" rel="stylesheet">
<!-- FooTable -->
<link href="<?php echo $host;?>assets/css/plugins/footable/footable.core.css" rel="stylesheet">
<link href="<?php echo $host;?>assets/css/plugins/iCheck/custom.css" rel="stylesheet">

<link href="<?php echo $host;?>assets/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">

<link href="<?php echo $host;?>assets/css/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">

<link href="<?php echo $host;?>assets/css/plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet">

<link href="<?php echo $host;?>assets/css/plugins/cropper/cropper.css" rel="stylesheet">

<link href="<?php echo $host;?>assets/css/plugins/switchery/switchery.css" rel="stylesheet">

<link href="<?php echo $host;?>assets/css/plugins/jasny/jasny-bootstrap.min.css" rel="stylesheet">

<link href="<?php echo $host;?>assets/css/plugins/nouslider/jquery.nouislider.css" rel="stylesheet">

<link href="<?php echo $host;?>assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

<link href="<?php echo $host;?>assets/css/plugins/ionRangeSlider/ion.rangeSlider.css" rel="stylesheet">
<link href="<?php echo $host;?>assets/css/plugins/ionRangeSlider/ion.rangeSlider.skinFlat.css" rel="stylesheet">

<link href="<?php echo $host;?>assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

<link href="<?php echo $host;?>assets/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">

<link href="<?php echo $host;?>assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">

<link href="<?php echo $host;?>assets/css/plugins/select2/select2.min.css" rel="stylesheet">

<link href="<?php echo $host;?>assets/css/plugins/touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet">

<link href="<?php echo $host;?>assets/css/plugins/dualListbox/bootstrap-duallistbox.min.css" rel="stylesheet">

<link href="<?php echo $host;?>assets/css/animate.css" rel="stylesheet">
<link href="<?php echo $host;?>assets/css/style.css" rel="stylesheet">

<!-- dataTables css -->
<link href="<?php echo $host;?>assets/css/jquery.dataTables.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">


<link rel="stylesheet" href="<?php echo $host;?>assets/js/plugins/pickadate/themes/classic.css" >
<link rel="stylesheet" href="<?php echo $host;?>assets/js/plugins/pickadate/themes/classic.date.css" >
<link rel="stylesheet" href="<?php echo $host;?>assets/js/plugins/pickadate/themes/classic.time.css" >

<link rel="stylesheet" href="<?php echo $host;?>assets/css/plugins/owlcarousel/owl.carousel.min.css" >
<link rel="stylesheet" href="<?php echo $host;?>assets/css/plugins/owlcarousel/owl.theme.default.min.css" >