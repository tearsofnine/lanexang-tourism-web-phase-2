<div class="col-md-1 forum-info">
    <a href="" class="forum-item-title">
        <small class="views-number text-info" id="state_all"></small>
    </a>
    <div>
        <h4 class="text-info"><?=$this->lang->line("ApprovalStateBar_All");?></h4>
    </div>
</div>
<div class="col-md-1 forum-info">
    <a href="" class="forum-item-title">
        <small style="color: #1AB394;" class="views-number"  id="state_approved"></small>
    </a>
    <div>
        <h4 style="color: #1AB394;"><?=$this->lang->line("ApprovalStateBar_Approved");?></h4>
    </div>
</div>
<div class="col-md-1 forum-info">
    <a href="" class="forum-item-title">
        <small class="views-number text-warning" id="state_wait"></small>
    </a>
    <div>
        <h4 class="text-warning"><?=$this->lang->line("ApprovalStateBar_Pending");?></h4>
    </div>
</div>
<div class="col-md-1 forum-info">
    <a href="" class="forum-item-title">
        <small class="views-number text-danger"  id="state_refuse" ></small>
    </a>
    <div>
        <h4 class="text-danger"><?=$this->lang->line("ApprovalStateBar_Not");?></h4>
    </div>
</div>