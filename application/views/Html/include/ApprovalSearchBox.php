
<div class="form-group  row">
    <div class="col-sm-2">
        <button type="button" id="btn_refresh" class="btn btn-white btn-sm" >
            <i class="fa fa-refresh"></i> รีเฟรช</button>
    </div>
    <div class="col-sm-6">
        <select data-placeholder="ค้นหาการอนุมัติ..." class="chosen-select" tabindex="1" id="find_approval">
            <option value="0">--เลือก--</option>
            <option value="1">ที่อนุมัติในระบบ</option>
            <option value="0">ที่รอการอนุมัติ</option>
            <option value="2">ที่ไม่ผ่านการอนุมัติ</option>
        </select>
    </div>
    <div class="col-sm-4">
        <input type="text" placeholder="<?=$this->lang->line("search");?>.." class="form-control-sm form-control" id="CustomSearchBox">
    </div>
</div>
