<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Content | Management System</title>

    <link href="<?php echo $host;?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $host;?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo $host;?>assets/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="<?php echo $host;?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo $host;?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo $host;?>assets/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <div style="text-align: center;">
                    <img src="<?php echo $host;?>assets/img/Group 1916.png" style="width: 100%; " class="img">
                  </div>
            </div>
            <h3></h3>
            <p>Content Management System
                <!--Continually expanded and constantly improved Inspinia Admin Them (IN+)-->
            </p>
            <p> เส้นทางท่องเที่ยว สี่เหลี่ยมวัฒนธรรมล้านช้าง</p>
            <p>Login in. To see it in action.</p>
            <form class="m-t login-form" role="form" method="post">
                <div class="form-group">
                    <p class="text-center"><span class="note " id="notification"></span></p>
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" placeholder="Username" required="" name="input_email" id="input_email">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password" required="" name="input_password" id="input_password">
                </div>
                <div class="form-group row">
                    <label for="1" class="col-sm-3 col-xs-3 col-form-label"></label>
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="radio_lang" id="radio_lang" value="TH" checked="true">
                        <label class="form-check-label" style="padding-right: 20px;" for="thai"><img src="<?php echo $host;?>assets/img/thai.png" title="TH" alt="TH"> Thai</label>

                        <input class="form-check-input" type="radio" name="radio_lang" id="radio_lang" value="EN" >
                        <label class="form-check-label" for="english"><img src="<?php echo $host;?>assets/img/EN.png" title="EN" alt="EN"> English</label> &nbsp;
                        
                      </div>
                  </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

                <a href="#" data-toggle="modal" data-target="#ModalForgot"><small>Forgot password?</small></a>
                <!-- <p class="text-muted text-center"><small>Do not have an account?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="register.html">Create an account</a> -->
            </form>
            <p class="m-t"> <small>องค์การบริหารการพัฒนาพื้นที่พิเศษเพื่อการท่องเที่ยวอย่างยั่งยืน (องค์การมหาชน). All Rights Reserved. &copy; 2019-2020</small> </p>
        </div>
    </div>
  <!--ModalForgotpassword-->
  <div class="modal inmodal" id="ModalForgot" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Forgot password</h4>
            </div>
            <div class="modal-body">
                <div class="modal-body">ระบบจะทำการส่งรหัสผ่านใหม่ ไปยัง email ที่คุณลงทะเบียนไว้
                  </div>
                <div class="form-group inner-addon left-addon">
                  <input class="form-control flat-input-underline" placeholder="กรอกชื่อผู้ใช้งาน" name="user_forget_password" type="text"
                    autofocus="">
                </div>
              </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

    <!-- Mainly scripts -->
    <script src="<?php echo $host;?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo $host;?>assets/js/popper.min.js"></script>
    <script src="<?php echo $host;?>assets/js/bootstrap.js"></script>

    <script src="<?php echo $host;?>assets/js/custom_js/NotificationsEvent.js"></script>

     <!-- Insert these scripts at the bottom of the HTML, but before you use any Firebase services -->

    <!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-app.js"></script>

    <!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
    <script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-analytics.js"></script>

    <!-- Add Firebase products that you want to use -->
    <script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-firestore.js"></script>

    <script src="https://www.gstatic.com/firebasejs/7.22.1/firebase-messaging.js"></script>

    <script src="<?php echo $host;?>assets/js/custom_js/firebase-init.js"></script>

    <script src="<?php echo $host;?>assets/js/custom_js/Login.js"></script>

</body>

</html>
