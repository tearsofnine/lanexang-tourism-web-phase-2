<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Content | Management System</title>
    <?php $this->load->view('Html/include/Header_css.php');?>  
</head>
<body>
    <!-- Preloader -->
    <div class="preloader" id="preloader">
        <div class="status" id="status"></div>
    </div>
    <div id="wrapper">
        <!-- SideMenu -->
        <?php $this->load->view('Html/include/SideMenu.php');?>
        <!-- End of SideMenu -->
        <div id="page-wrapper" class="gray-bg">
            <!-- Header -->
            <?php $this->load->view('Html/include/Header.php');?>
            <!-- End of Header -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2><?=$this->lang->line("notifications");?></h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="../Main">Home</a>
                        </li>
                        <li class="breadcrumb-item active">
                            <strong><?=$this->lang->line("notifications");?></strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="ibox ">
                            <!-- <div class="ibox-title">
                            </div> -->
                            <div class="ibox-content">
                                <div class="project-list">
                                    <table class="footable table table-hover nowrap" data-page-size="8" data-filter=#filter style="width:100%" id="ListChatTable">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5" >
                        <div class="ibox selected">
                            <div class="ibox-content">
                                <div class="tab-content">
                                    <div class="client-detail">
                                        <div class="slimScroll" id="messagelist">

                                            <div class="ibox-content-head">
                                                <div id="main_chat_table">
                                                    <div class="chat-activity-list" id="chat_table">

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                      
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="ibox-content">
                            <div class="tab-content">
                                <div class="footer ">
                                    <div class="row">
                                        <div class="col-lg-12 ">
                                            <div class="chat-form">
                                                <div class="input-group">
                                                    <input type="text" class="form-control " placeholder="Write your message…" id="message"> &nbsp; &nbsp;
                                                    <button class="btn btn-info btn-circle" type="button"><i class="fa fa-paper-plane" id="send_message"></i></button>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                       
                        
                    </div>
                </div>
            </div>
            <br>

            <!-- Footer -->
            <?php $this->load->view('Html/include/Footer.php');?>
            <!-- End of Footer -->
             <!-- ViewItemsModal -->
             <?php $this->load->view('Html/include/ViewItemsModal.php');?>
            <!-- End of ViewItemsModal -->
        </div>
    </div>
    <?php $this->load->view('Html/include/Footer_js.php');?>
    <script>
        var host = "<?php echo $host;?>html"
        var table;
        var MenuName = "Notifications";
        var TypeMenu = "Notifications";
        var UseGoogleMap = '<?php echo $ENVIRONMENT?>';

        var user_id ="<?php echo $this->session->userdata("user_id");?>"
        var select_rows_data = 0;

    </script>
    <script src="<?php echo $host;?>assets/js/custom_js/GoogleMaps.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/GlobalFunction.js"></script>
    <script src="<?php echo $host;?>assets/js/custom_js/NotificationsView.js"></script>
</body>

</html>