// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/7.22.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.22.1/firebase-messaging.js');
// importScripts('/dasta_thailand/assets/js/custom_js/NotificationsEvent.js');

// Initialize the Firebase app in the service worker by passing in
// your app's Firebase config object.
// https://firebase.google.com/docs/web/setup#config-object
var config = {
	// apiKey: "AIzaSyCV1zm-pN-u_uPX3mU4kS7TQ4_A0UZAdFg",
    // authDomain: "lanexang-info.firebaseapp.com",
    // databaseURL: "https://lanexang-info.firebaseio.com",
    // projectId: "lanexang-info",
    // storageBucket: "lanexang-info.appspot.com",
    // messagingSenderId: "599248162658",
    // appId: "1:599248162658:web:4c5cbbd24654e03f6fa63b",
    // measurementId: "G-SETZPRY9PW"
    apiKey: "AIzaSyBQTsq90u5Yo1GKvzic1ZS3vHcmV9OjKZs",
    authDomain: "lanexang-info-ca0d2.firebaseapp.com",
    databaseURL: "https://lanexang-info-ca0d2.firebaseio.com",
    projectId: "lanexang-info-ca0d2",
    storageBucket: "lanexang-info-ca0d2.appspot.com",
    messagingSenderId: "247075743210",
    appId: "1:247075743210:web:3fb23ca77d7b8da9bc7e93",
    measurementId: "G-MZS281YXT5"
};
firebase.initializeApp(config);

const messaging = firebase.messaging();

// If you would like to customize notifications that are received in the
// background (Web app is closed or not in browser focus) then you should
// implement this optional method.
// [START background_handler]
// messaging.setBackgroundMessageHandler(function(payload) {
//   console.log('[firebase-messaging-sw.js] Received background message ', payload);
//   // Customize notification here
//   var obj = JSON.parse( payload.data.data);
//   const notificationTitle = 'New Message';
//   const notificationOptions = {
//     body: obj.body,
//     icon: '/dasta_thailand/assets/img/firebase-logo.png'
    
//   };
//   // updateNotifications()

//   return self.registration.showNotification(notificationTitle,
//     notificationOptions);
// });
// [END background_handler]

